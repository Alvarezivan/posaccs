﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class proveedorescat
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(proveedorescat))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl3 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage13 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.txMailProp = New DevExpress.XtraEditors.TextEdit()
        Me.txRadioProp = New DevExpress.XtraEditors.TextEdit()
        Me.txTelProp = New DevExpress.XtraEditors.TextEdit()
        Me.txResponsable = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.Button10 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.txEmail = New DevExpress.XtraEditors.TextEdit()
        Me.txObserva = New DevExpress.XtraEditors.TextEdit()
        Me.txWeb = New DevExpress.XtraEditors.TextEdit()
        Me.txRfc = New DevExpress.XtraEditors.TextEdit()
        Me.txNextel = New DevExpress.XtraEditors.TextEdit()
        Me.txFax = New DevExpress.XtraEditors.TextEdit()
        Me.txTel2 = New DevExpress.XtraEditors.TextEdit()
        Me.txTel1 = New DevExpress.XtraEditors.TextEdit()
        Me.txCalle = New DevExpress.XtraEditors.TextEdit()
        Me.cbCP = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbColonia = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbCiudad = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txNomCorto = New DevExpress.XtraEditors.TextEdit()
        Me.cbRazon = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.chkCopiar = New DevExpress.XtraEditors.CheckEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.chkFletePaga = New DevExpress.XtraEditors.CheckEdit()
        Me.chkactivo = New System.Windows.Forms.CheckBox()
        Me.txConvTrans = New DevExpress.XtraEditors.TextEdit()
        Me.cbTransporte = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txCuentaCont = New DevExpress.XtraEditors.TextEdit()
        Me.txEmailPagos = New DevExpress.XtraEditors.TextEdit()
        Me.txContactoPago = New DevExpress.XtraEditors.TextEdit()
        Me.txLimCrPro = New DevExpress.XtraEditors.TextEdit()
        Me.txLimCrInt = New DevExpress.XtraEditors.TextEdit()
        Me.nudDesc3 = New DevExpress.XtraEditors.SpinEdit()
        Me.nudDesc2 = New DevExpress.XtraEditors.SpinEdit()
        Me.nudDesc1 = New DevExpress.XtraEditors.SpinEdit()
        Me.nudPlazo3 = New DevExpress.XtraEditors.SpinEdit()
        Me.nudPlazo2 = New DevExpress.XtraEditors.SpinEdit()
        Me.nudPlazo1 = New DevExpress.XtraEditors.SpinEdit()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.txClabe6 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta6 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco6 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txClabe5 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta5 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco5 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txClabe4 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta4 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txClabe3 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta3 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txClabe2 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta2 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txClabe1 = New DevExpress.XtraEditors.TextEdit()
        Me.txCuenta1 = New DevExpress.XtraEditors.TextEdit()
        Me.cbBanco1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.chkTodos = New DevExpress.XtraEditors.CheckEdit()
        Me.cbProv = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Button24 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button22 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl13 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl3.SuspendLayout()
        Me.XtraTabPage13.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.txMailProp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txRadioProp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTelProp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txResponsable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.txEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txObserva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txWeb.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNextel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txFax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTel2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTel1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNomCorto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbRazon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.chkCopiar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFletePaga.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txConvTrans.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTransporte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuentaCont.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txEmailPagos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txContactoPago.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLimCrPro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLimCrInt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDesc3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDesc2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDesc1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPlazo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPlazo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPlazo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.txClabe6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCuenta1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbBanco1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbProv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl13.SuspendLayout()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.XtraTabControl1.Appearance.Options.UseBackColor = True
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(781, 683)
        Me.XtraTabControl1.TabIndex = 6
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.XtraTabControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(775, 655)
        Me.XtraTabPage2.Text = "Catalogos"
        '
        'XtraTabControl3
        '
        Me.XtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl3.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl3.Name = "XtraTabControl3"
        Me.XtraTabControl3.SelectedTabPage = Me.XtraTabPage13
        Me.XtraTabControl3.Size = New System.Drawing.Size(775, 655)
        Me.XtraTabControl3.TabIndex = 0
        Me.XtraTabControl3.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage13, Me.XtraTabPage1})
        '
        'XtraTabPage13
        '
        Me.XtraTabPage13.Appearance.PageClient.BackColor = System.Drawing.Color.LightSeaGreen
        Me.XtraTabPage13.Appearance.PageClient.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.XtraTabPage13.Appearance.PageClient.Options.UseBackColor = True
        Me.XtraTabPage13.Controls.Add(Me.GroupControl7)
        Me.XtraTabPage13.Controls.Add(Me.Button10)
        Me.XtraTabPage13.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage13.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage13.Name = "XtraTabPage13"
        Me.XtraTabPage13.Size = New System.Drawing.Size(769, 627)
        Me.XtraTabPage13.Text = "ABC Proveedores"
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.txMailProp)
        Me.GroupControl7.Controls.Add(Me.txRadioProp)
        Me.GroupControl7.Controls.Add(Me.txTelProp)
        Me.GroupControl7.Controls.Add(Me.txResponsable)
        Me.GroupControl7.Controls.Add(Me.LabelControl8)
        Me.GroupControl7.Controls.Add(Me.LabelControl7)
        Me.GroupControl7.Controls.Add(Me.LabelControl6)
        Me.GroupControl7.Controls.Add(Me.LabelControl5)
        Me.GroupControl7.Location = New System.Drawing.Point(0, 530)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(583, 95)
        Me.GroupControl7.TabIndex = 134
        Me.GroupControl7.Text = "Director General (opcional)"
        '
        'txMailProp
        '
        Me.txMailProp.Location = New System.Drawing.Point(367, 72)
        Me.txMailProp.Name = "txMailProp"
        Me.txMailProp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txMailProp.Properties.MaxLength = 100
        Me.txMailProp.Size = New System.Drawing.Size(182, 20)
        Me.txMailProp.TabIndex = 3
        '
        'txRadioProp
        '
        Me.txRadioProp.Location = New System.Drawing.Point(366, 31)
        Me.txRadioProp.Name = "txRadioProp"
        Me.txRadioProp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txRadioProp.Properties.MaxLength = 25
        Me.txRadioProp.Size = New System.Drawing.Size(183, 20)
        Me.txRadioProp.TabIndex = 2
        '
        'txTelProp
        '
        Me.txTelProp.Location = New System.Drawing.Point(99, 67)
        Me.txTelProp.Name = "txTelProp"
        Me.txTelProp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTelProp.Properties.MaxLength = 15
        Me.txTelProp.Size = New System.Drawing.Size(183, 20)
        Me.txTelProp.TabIndex = 1
        '
        'txResponsable
        '
        Me.txResponsable.Location = New System.Drawing.Point(99, 31)
        Me.txResponsable.Name = "txResponsable"
        Me.txResponsable.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txResponsable.Properties.MaxLength = 250
        Me.txResponsable.Size = New System.Drawing.Size(183, 20)
        Me.txResponsable.TabIndex = 0
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(333, 75)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(18, 13)
        Me.LabelControl8.TabIndex = 3
        Me.LabelControl8.Text = "mail"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(333, 34)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(27, 13)
        Me.LabelControl7.TabIndex = 2
        Me.LabelControl7.Text = "Radio"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(32, 70)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl6.TabIndex = 1
        Me.LabelControl6.Text = "Teléfono"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(32, 34)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl5.TabIndex = 0
        Me.LabelControl5.Text = "Responsable"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(591, 564)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(130, 46)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "&Guardar "
        '
        'GroupControl3
        '
        Me.GroupControl3.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.GroupControl3.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.GroupControl3.Appearance.Options.UseBackColor = True
        Me.GroupControl3.Controls.Add(Me.txEmail)
        Me.GroupControl3.Controls.Add(Me.txObserva)
        Me.GroupControl3.Controls.Add(Me.txWeb)
        Me.GroupControl3.Controls.Add(Me.txRfc)
        Me.GroupControl3.Controls.Add(Me.txNextel)
        Me.GroupControl3.Controls.Add(Me.txFax)
        Me.GroupControl3.Controls.Add(Me.txTel2)
        Me.GroupControl3.Controls.Add(Me.txTel1)
        Me.GroupControl3.Controls.Add(Me.txCalle)
        Me.GroupControl3.Controls.Add(Me.cbCP)
        Me.GroupControl3.Controls.Add(Me.cbColonia)
        Me.GroupControl3.Controls.Add(Me.cbCiudad)
        Me.GroupControl3.Controls.Add(Me.cbEstado)
        Me.GroupControl3.Controls.Add(Me.txNomCorto)
        Me.GroupControl3.Controls.Add(Me.cbRazon)
        Me.GroupControl3.Controls.Add(Me.Label69)
        Me.GroupControl3.Controls.Add(Me.Label47)
        Me.GroupControl3.Controls.Add(Me.Label40)
        Me.GroupControl3.Controls.Add(Me.Label41)
        Me.GroupControl3.Controls.Add(Me.Label54)
        Me.GroupControl3.Controls.Add(Me.Label42)
        Me.GroupControl3.Controls.Add(Me.Label68)
        Me.GroupControl3.Controls.Add(Me.Label67)
        Me.GroupControl3.Controls.Add(Me.Label43)
        Me.GroupControl3.Controls.Add(Me.Label66)
        Me.GroupControl3.Controls.Add(Me.Label65)
        Me.GroupControl3.Controls.Add(Me.Label64)
        Me.GroupControl3.Controls.Add(Me.Label63)
        Me.GroupControl3.Controls.Add(Me.Label62)
        Me.GroupControl3.Controls.Add(Me.Label61)
        Me.GroupControl3.Location = New System.Drawing.Point(4, 4)
        Me.GroupControl3.LookAndFeel.SkinName = "Money Twins"
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(717, 198)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Datos Generales"
        '
        'txEmail
        '
        Me.txEmail.Location = New System.Drawing.Point(471, 164)
        Me.txEmail.Name = "txEmail"
        Me.txEmail.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txEmail.Properties.MaxLength = 50
        Me.txEmail.Size = New System.Drawing.Size(237, 20)
        Me.txEmail.TabIndex = 14
        '
        'txObserva
        '
        Me.txObserva.Location = New System.Drawing.Point(295, 163)
        Me.txObserva.Name = "txObserva"
        Me.txObserva.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txObserva.Properties.MaxLength = 50
        Me.txObserva.Size = New System.Drawing.Size(170, 20)
        Me.txObserva.TabIndex = 13
        '
        'txWeb
        '
        Me.txWeb.Location = New System.Drawing.Point(131, 164)
        Me.txWeb.Name = "txWeb"
        Me.txWeb.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txWeb.Properties.MaxLength = 100
        Me.txWeb.Size = New System.Drawing.Size(158, 20)
        Me.txWeb.TabIndex = 12
        '
        'txRfc
        '
        Me.txRfc.Location = New System.Drawing.Point(4, 164)
        Me.txRfc.Name = "txRfc"
        Me.txRfc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txRfc.Properties.MaxLength = 20
        Me.txRfc.Size = New System.Drawing.Size(121, 20)
        Me.txRfc.TabIndex = 11
        '
        'txNextel
        '
        Me.txNextel.Location = New System.Drawing.Point(558, 123)
        Me.txNextel.Name = "txNextel"
        Me.txNextel.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNextel.Properties.MaxLength = 20
        Me.txNextel.Size = New System.Drawing.Size(150, 20)
        Me.txNextel.TabIndex = 10
        '
        'txFax
        '
        Me.txFax.Location = New System.Drawing.Point(394, 123)
        Me.txFax.Name = "txFax"
        Me.txFax.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txFax.Properties.MaxLength = 10
        Me.txFax.Size = New System.Drawing.Size(158, 20)
        Me.txFax.TabIndex = 9
        '
        'txTel2
        '
        Me.txTel2.Location = New System.Drawing.Point(199, 123)
        Me.txTel2.Name = "txTel2"
        Me.txTel2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTel2.Properties.MaxLength = 10
        Me.txTel2.Size = New System.Drawing.Size(189, 20)
        Me.txTel2.TabIndex = 8
        '
        'txTel1
        '
        Me.txTel1.Location = New System.Drawing.Point(4, 123)
        Me.txTel1.Name = "txTel1"
        Me.txTel1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTel1.Properties.MaxLength = 10
        Me.txTel1.Size = New System.Drawing.Size(189, 20)
        Me.txTel1.TabIndex = 7
        '
        'txCalle
        '
        Me.txCalle.Location = New System.Drawing.Point(471, 82)
        Me.txCalle.Name = "txCalle"
        Me.txCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCalle.Properties.MaxLength = 80
        Me.txCalle.Size = New System.Drawing.Size(237, 20)
        Me.txCalle.TabIndex = 6
        '
        'cbCP
        '
        Me.cbCP.Location = New System.Drawing.Point(387, 82)
        Me.cbCP.Name = "cbCP"
        Me.cbCP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCP.Size = New System.Drawing.Size(78, 20)
        Me.cbCP.TabIndex = 5
        '
        'cbColonia
        '
        Me.cbColonia.Location = New System.Drawing.Point(256, 82)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbColonia.Size = New System.Drawing.Size(125, 20)
        Me.cbColonia.TabIndex = 4
        '
        'cbCiudad
        '
        Me.cbCiudad.Location = New System.Drawing.Point(129, 82)
        Me.cbCiudad.Name = "cbCiudad"
        Me.cbCiudad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCiudad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCiudad.Size = New System.Drawing.Size(121, 20)
        Me.cbCiudad.TabIndex = 3
        '
        'cbEstado
        '
        Me.cbEstado.Location = New System.Drawing.Point(3, 82)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(120, 20)
        Me.cbEstado.TabIndex = 2
        '
        'txNomCorto
        '
        Me.txNomCorto.Location = New System.Drawing.Point(359, 43)
        Me.txNomCorto.Name = "txNomCorto"
        Me.txNomCorto.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNomCorto.Properties.MaxLength = 50
        Me.txNomCorto.Size = New System.Drawing.Size(168, 20)
        Me.txNomCorto.TabIndex = 1
        '
        'cbRazon
        '
        Me.cbRazon.Location = New System.Drawing.Point(3, 42)
        Me.cbRazon.Name = "cbRazon"
        Me.cbRazon.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbRazon.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbRazon.Properties.MaxLength = 80
        Me.cbRazon.Size = New System.Drawing.Size(322, 20)
        Me.cbRazon.TabIndex = 0
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(359, 27)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(74, 13)
        Me.Label69.TabIndex = 68
        Me.Label69.Text = "Nombre Corto"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(1, 27)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(67, 13)
        Me.Label47.TabIndex = 17
        Me.Label47.Text = "Razon Social"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(557, 107)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(52, 13)
        Me.Label40.TabIndex = 86
        Me.Label40.Text = "Nextel ID"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(394, 107)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(25, 13)
        Me.Label41.TabIndex = 84
        Me.Label41.Text = "Fax"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(136, 148)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(52, 13)
        Me.Label54.TabIndex = 106
        Me.Label54.Text = "Sitio Web"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(199, 107)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(49, 13)
        Me.Label42.TabIndex = 82
        Me.Label42.Text = "Teléfono"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(4, 65)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(40, 13)
        Me.Label68.TabIndex = 70
        Me.Label68.Text = "Estado"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(126, 65)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(40, 13)
        Me.Label67.TabIndex = 72
        Me.Label67.Text = "Ciudad"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(4, 107)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(49, 13)
        Me.Label43.TabIndex = 80
        Me.Label43.Text = "Teléfono"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(265, 65)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(42, 13)
        Me.Label66.TabIndex = 74
        Me.Label66.Text = "Colonia"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(387, 65)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(72, 13)
        Me.Label65.TabIndex = 76
        Me.Label65.Text = "Código Postal"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(525, 65)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(78, 13)
        Me.Label64.TabIndex = 78
        Me.Label64.Text = "Calle y número"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(4, 148)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(27, 13)
        Me.Label63.TabIndex = 88
        Me.Label63.Text = "RFC"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(349, 149)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(78, 13)
        Me.Label62.TabIndex = 90
        Me.Label62.Text = "Observaciones"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(554, 149)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(73, 13)
        Me.Label61.TabIndex = 92
        Me.Label61.Text = "Email Principal"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Appearance.BackColor = System.Drawing.Color.White
        Me.GroupControl2.Appearance.Options.UseBackColor = True
        Me.GroupControl2.Controls.Add(Me.GroupControl5)
        Me.GroupControl2.Controls.Add(Me.GroupControl4)
        Me.GroupControl2.Location = New System.Drawing.Point(2, 209)
        Me.GroupControl2.LookAndFeel.SkinName = "Blue"
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(719, 310)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Datos para Tesorería(opcional)"
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.chkCopiar)
        Me.GroupControl5.Controls.Add(Me.chkFletePaga)
        Me.GroupControl5.Controls.Add(Me.chkactivo)
        Me.GroupControl5.Controls.Add(Me.txConvTrans)
        Me.GroupControl5.Controls.Add(Me.cbTransporte)
        Me.GroupControl5.Controls.Add(Me.txCuentaCont)
        Me.GroupControl5.Controls.Add(Me.txEmailPagos)
        Me.GroupControl5.Controls.Add(Me.txContactoPago)
        Me.GroupControl5.Controls.Add(Me.txLimCrPro)
        Me.GroupControl5.Controls.Add(Me.txLimCrInt)
        Me.GroupControl5.Controls.Add(Me.nudDesc3)
        Me.GroupControl5.Controls.Add(Me.nudDesc2)
        Me.GroupControl5.Controls.Add(Me.nudDesc1)
        Me.GroupControl5.Controls.Add(Me.nudPlazo3)
        Me.GroupControl5.Controls.Add(Me.nudPlazo2)
        Me.GroupControl5.Controls.Add(Me.nudPlazo1)
        Me.GroupControl5.Controls.Add(Me.Label3)
        Me.GroupControl5.Controls.Add(Me.Label38)
        Me.GroupControl5.Controls.Add(Me.Label53)
        Me.GroupControl5.Controls.Add(Me.Label50)
        Me.GroupControl5.Controls.Add(Me.Label48)
        Me.GroupControl5.Controls.Add(Me.Label52)
        Me.GroupControl5.Controls.Add(Me.Label60)
        Me.GroupControl5.Controls.Add(Me.Label56)
        Me.GroupControl5.Controls.Add(Me.Label59)
        Me.GroupControl5.Location = New System.Drawing.Point(358, 20)
        Me.GroupControl5.LookAndFeel.SkinName = "Money Twins"
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(357, 255)
        Me.GroupControl5.TabIndex = 4
        Me.GroupControl5.Text = "Descuentos y Plazos Pactados de Pago"
        '
        'chkCopiar
        '
        Me.chkCopiar.Location = New System.Drawing.Point(99, 227)
        Me.chkCopiar.MenuManager = Me.BarManager1
        Me.chkCopiar.Name = "chkCopiar"
        Me.chkCopiar.Properties.Caption = "Copiar datos"
        Me.chkCopiar.Size = New System.Drawing.Size(97, 19)
        Me.chkCopiar.TabIndex = 16
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem1})
        Me.BarManager1.MaxItemId = 1
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(781, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 683)
        Me.barDockControlBottom.Size = New System.Drawing.Size(781, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 683)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(781, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 683)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Enlazar Marcas-Proveedores"
        Me.BarButtonItem1.Id = 0
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'chkFletePaga
        '
        Me.chkFletePaga.Location = New System.Drawing.Point(202, 203)
        Me.chkFletePaga.Name = "chkFletePaga"
        Me.chkFletePaga.Properties.Caption = "Flete lo paga el proveedor"
        Me.chkFletePaga.Size = New System.Drawing.Size(150, 19)
        Me.chkFletePaga.TabIndex = 17
        '
        'chkactivo
        '
        Me.chkactivo.AutoSize = True
        Me.chkactivo.Location = New System.Drawing.Point(9, 229)
        Me.chkactivo.Name = "chkactivo"
        Me.chkactivo.Size = New System.Drawing.Size(56, 17)
        Me.chkactivo.TabIndex = 15
        Me.chkactivo.Text = "Activo"
        Me.chkactivo.UseVisualStyleBackColor = True
        '
        'txConvTrans
        '
        Me.txConvTrans.Location = New System.Drawing.Point(204, 169)
        Me.txConvTrans.Name = "txConvTrans"
        Me.txConvTrans.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txConvTrans.Properties.MaxLength = 50
        Me.txConvTrans.Size = New System.Drawing.Size(148, 20)
        Me.txConvTrans.TabIndex = 14
        '
        'cbTransporte
        '
        Me.cbTransporte.Location = New System.Drawing.Point(204, 132)
        Me.cbTransporte.Name = "cbTransporte"
        Me.cbTransporte.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTransporte.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTransporte.Properties.MaxLength = 50
        Me.cbTransporte.Size = New System.Drawing.Size(148, 20)
        Me.cbTransporte.TabIndex = 13
        '
        'txCuentaCont
        '
        Me.txCuentaCont.Location = New System.Drawing.Point(9, 203)
        Me.txCuentaCont.Name = "txCuentaCont"
        Me.txCuentaCont.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuentaCont.Properties.MaxLength = 50
        Me.txCuentaCont.Size = New System.Drawing.Size(185, 20)
        Me.txCuentaCont.TabIndex = 12
        '
        'txEmailPagos
        '
        Me.txEmailPagos.Location = New System.Drawing.Point(9, 169)
        Me.txEmailPagos.Name = "txEmailPagos"
        Me.txEmailPagos.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txEmailPagos.Properties.MaxLength = 50
        Me.txEmailPagos.Size = New System.Drawing.Size(185, 20)
        Me.txEmailPagos.TabIndex = 11
        '
        'txContactoPago
        '
        Me.txContactoPago.Location = New System.Drawing.Point(9, 132)
        Me.txContactoPago.Name = "txContactoPago"
        Me.txContactoPago.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txContactoPago.Properties.MaxLength = 100
        Me.txContactoPago.Size = New System.Drawing.Size(185, 20)
        Me.txContactoPago.TabIndex = 10
        '
        'txLimCrPro
        '
        Me.txLimCrPro.EditValue = "0"
        Me.txLimCrPro.Location = New System.Drawing.Point(204, 84)
        Me.txLimCrPro.Name = "txLimCrPro"
        Me.txLimCrPro.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txLimCrPro.Properties.Mask.EditMask = "c"
        Me.txLimCrPro.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txLimCrPro.Properties.MaxLength = 100
        Me.txLimCrPro.Size = New System.Drawing.Size(148, 20)
        Me.txLimCrPro.TabIndex = 9
        '
        'txLimCrInt
        '
        Me.txLimCrInt.EditValue = "0"
        Me.txLimCrInt.Location = New System.Drawing.Point(204, 41)
        Me.txLimCrInt.Name = "txLimCrInt"
        Me.txLimCrInt.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txLimCrInt.Properties.Mask.EditMask = "c"
        Me.txLimCrInt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txLimCrInt.Properties.MaxLength = 100
        Me.txLimCrInt.Size = New System.Drawing.Size(148, 20)
        Me.txLimCrInt.TabIndex = 8
        '
        'nudDesc3
        '
        Me.nudDesc3.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudDesc3.Location = New System.Drawing.Point(131, 92)
        Me.nudDesc3.Name = "nudDesc3"
        Me.nudDesc3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudDesc3.Properties.MaxValue = New Decimal(New Integer() {9999, 0, 0, 131072})
        Me.nudDesc3.Size = New System.Drawing.Size(52, 20)
        Me.nudDesc3.TabIndex = 7
        '
        'nudDesc2
        '
        Me.nudDesc2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudDesc2.Location = New System.Drawing.Point(131, 66)
        Me.nudDesc2.Name = "nudDesc2"
        Me.nudDesc2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudDesc2.Properties.MaxValue = New Decimal(New Integer() {9999, 0, 0, 131072})
        Me.nudDesc2.Size = New System.Drawing.Size(52, 20)
        Me.nudDesc2.TabIndex = 5
        '
        'nudDesc1
        '
        Me.nudDesc1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudDesc1.Location = New System.Drawing.Point(131, 41)
        Me.nudDesc1.Name = "nudDesc1"
        Me.nudDesc1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudDesc1.Properties.MaxValue = New Decimal(New Integer() {9999, 0, 0, 131072})
        Me.nudDesc1.Size = New System.Drawing.Size(52, 20)
        Me.nudDesc1.TabIndex = 3
        '
        'nudPlazo3
        '
        Me.nudPlazo3.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudPlazo3.Location = New System.Drawing.Point(28, 92)
        Me.nudPlazo3.Name = "nudPlazo3"
        Me.nudPlazo3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudPlazo3.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudPlazo3.Size = New System.Drawing.Size(60, 20)
        Me.nudPlazo3.TabIndex = 6
        '
        'nudPlazo2
        '
        Me.nudPlazo2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudPlazo2.Location = New System.Drawing.Point(28, 66)
        Me.nudPlazo2.Name = "nudPlazo2"
        Me.nudPlazo2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudPlazo2.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudPlazo2.Size = New System.Drawing.Size(60, 20)
        Me.nudPlazo2.TabIndex = 4
        '
        'nudPlazo1
        '
        Me.nudPlazo1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudPlazo1.Location = New System.Drawing.Point(28, 41)
        Me.nudPlazo1.Name = "nudPlazo1"
        Me.nudPlazo1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nudPlazo1.Properties.MaxValue = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudPlazo1.Size = New System.Drawing.Size(60, 20)
        Me.nudPlazo1.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(201, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 13)
        Me.Label3.TabIndex = 136
        Me.Label3.Text = "Limite Crédito Proveedor"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(220, 111)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(60, 13)
        Me.Label38.TabIndex = 110
        Me.Label38.Text = "Transporte"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(6, 190)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(88, 13)
        Me.Label53.TabIndex = 108
        Me.Label53.Text = "Cuenta Contable"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(6, 117)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(83, 13)
        Me.Label50.TabIndex = 116
        Me.Label50.Text = "Contacto Pagos"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(6, 153)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(63, 13)
        Me.Label48.TabIndex = 0
        Me.Label48.Text = "Email Pagos"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(201, 154)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(108, 13)
        Me.Label52.TabIndex = 112
        Me.Label52.Text = "Convenio Transporte"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(31, 25)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(55, 13)
        Me.Label60.TabIndex = 0
        Me.Label60.Text = "Plazo Días"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(128, 25)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(58, 13)
        Me.Label56.TabIndex = 1
        Me.Label56.Text = "Descuento"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(201, 27)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(111, 13)
        Me.Label59.TabIndex = 96
        Me.Label59.Text = "Limite Crédito Interno"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.txClabe6)
        Me.GroupControl4.Controls.Add(Me.txCuenta6)
        Me.GroupControl4.Controls.Add(Me.cbBanco6)
        Me.GroupControl4.Controls.Add(Me.txClabe5)
        Me.GroupControl4.Controls.Add(Me.txCuenta5)
        Me.GroupControl4.Controls.Add(Me.cbBanco5)
        Me.GroupControl4.Controls.Add(Me.txClabe4)
        Me.GroupControl4.Controls.Add(Me.txCuenta4)
        Me.GroupControl4.Controls.Add(Me.cbBanco4)
        Me.GroupControl4.Controls.Add(Me.txClabe3)
        Me.GroupControl4.Controls.Add(Me.txCuenta3)
        Me.GroupControl4.Controls.Add(Me.cbBanco3)
        Me.GroupControl4.Controls.Add(Me.txClabe2)
        Me.GroupControl4.Controls.Add(Me.txCuenta2)
        Me.GroupControl4.Controls.Add(Me.cbBanco2)
        Me.GroupControl4.Controls.Add(Me.txClabe1)
        Me.GroupControl4.Controls.Add(Me.txCuenta1)
        Me.GroupControl4.Controls.Add(Me.cbBanco1)
        Me.GroupControl4.Controls.Add(Me.Label2)
        Me.GroupControl4.Controls.Add(Me.Label86)
        Me.GroupControl4.Controls.Add(Me.Label1)
        Me.GroupControl4.Location = New System.Drawing.Point(0, 20)
        Me.GroupControl4.LookAndFeel.SkinName = "Money Twins"
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(355, 222)
        Me.GroupControl4.TabIndex = 3
        Me.GroupControl4.Text = "Datos Bancarios para Pagos"
        '
        'txClabe6
        '
        Me.txClabe6.Location = New System.Drawing.Point(236, 187)
        Me.txClabe6.Name = "txClabe6"
        Me.txClabe6.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe6.Properties.MaxLength = 100
        Me.txClabe6.Size = New System.Drawing.Size(106, 20)
        Me.txClabe6.TabIndex = 17
        '
        'txCuenta6
        '
        Me.txCuenta6.Location = New System.Drawing.Point(133, 187)
        Me.txCuenta6.Name = "txCuenta6"
        Me.txCuenta6.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta6.Properties.MaxLength = 100
        Me.txCuenta6.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta6.TabIndex = 16
        '
        'cbBanco6
        '
        Me.cbBanco6.Location = New System.Drawing.Point(6, 187)
        Me.cbBanco6.Name = "cbBanco6"
        Me.cbBanco6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco6.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco6.Properties.MaxLength = 50
        Me.cbBanco6.Size = New System.Drawing.Size(121, 20)
        Me.cbBanco6.TabIndex = 15
        '
        'txClabe5
        '
        Me.txClabe5.Location = New System.Drawing.Point(236, 161)
        Me.txClabe5.Name = "txClabe5"
        Me.txClabe5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe5.Properties.MaxLength = 100
        Me.txClabe5.Size = New System.Drawing.Size(106, 20)
        Me.txClabe5.TabIndex = 14
        '
        'txCuenta5
        '
        Me.txCuenta5.Location = New System.Drawing.Point(133, 161)
        Me.txCuenta5.Name = "txCuenta5"
        Me.txCuenta5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta5.Properties.MaxLength = 100
        Me.txCuenta5.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta5.TabIndex = 13
        '
        'cbBanco5
        '
        Me.cbBanco5.Location = New System.Drawing.Point(6, 161)
        Me.cbBanco5.Name = "cbBanco5"
        Me.cbBanco5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco5.Properties.MaxLength = 100
        Me.cbBanco5.Size = New System.Drawing.Size(121, 20)
        Me.cbBanco5.TabIndex = 12
        '
        'txClabe4
        '
        Me.txClabe4.Location = New System.Drawing.Point(236, 130)
        Me.txClabe4.Name = "txClabe4"
        Me.txClabe4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe4.Properties.MaxLength = 100
        Me.txClabe4.Size = New System.Drawing.Size(106, 20)
        Me.txClabe4.TabIndex = 11
        '
        'txCuenta4
        '
        Me.txCuenta4.Location = New System.Drawing.Point(133, 130)
        Me.txCuenta4.Name = "txCuenta4"
        Me.txCuenta4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta4.Properties.MaxLength = 100
        Me.txCuenta4.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta4.TabIndex = 10
        '
        'cbBanco4
        '
        Me.cbBanco4.Location = New System.Drawing.Point(6, 130)
        Me.cbBanco4.Name = "cbBanco4"
        Me.cbBanco4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco4.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco4.Properties.MaxLength = 50
        Me.cbBanco4.Size = New System.Drawing.Size(121, 20)
        Me.cbBanco4.TabIndex = 9
        '
        'txClabe3
        '
        Me.txClabe3.Location = New System.Drawing.Point(236, 101)
        Me.txClabe3.Name = "txClabe3"
        Me.txClabe3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe3.Properties.MaxLength = 100
        Me.txClabe3.Size = New System.Drawing.Size(106, 20)
        Me.txClabe3.TabIndex = 8
        '
        'txCuenta3
        '
        Me.txCuenta3.Location = New System.Drawing.Point(133, 101)
        Me.txCuenta3.Name = "txCuenta3"
        Me.txCuenta3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta3.Properties.MaxLength = 100
        Me.txCuenta3.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta3.TabIndex = 7
        '
        'cbBanco3
        '
        Me.cbBanco3.Location = New System.Drawing.Point(6, 101)
        Me.cbBanco3.Name = "cbBanco3"
        Me.cbBanco3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco3.Properties.MaxLength = 50
        Me.cbBanco3.Size = New System.Drawing.Size(121, 20)
        Me.cbBanco3.TabIndex = 6
        '
        'txClabe2
        '
        Me.txClabe2.Location = New System.Drawing.Point(236, 69)
        Me.txClabe2.Name = "txClabe2"
        Me.txClabe2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe2.Properties.MaxLength = 100
        Me.txClabe2.Size = New System.Drawing.Size(106, 20)
        Me.txClabe2.TabIndex = 5
        '
        'txCuenta2
        '
        Me.txCuenta2.Location = New System.Drawing.Point(133, 69)
        Me.txCuenta2.Name = "txCuenta2"
        Me.txCuenta2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta2.Properties.MaxLength = 100
        Me.txCuenta2.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta2.TabIndex = 4
        '
        'cbBanco2
        '
        Me.cbBanco2.Location = New System.Drawing.Point(6, 69)
        Me.cbBanco2.Name = "cbBanco2"
        Me.cbBanco2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco2.Properties.MaxLength = 50
        Me.cbBanco2.Size = New System.Drawing.Size(121, 20)
        Me.cbBanco2.TabIndex = 3
        '
        'txClabe1
        '
        Me.txClabe1.Location = New System.Drawing.Point(236, 39)
        Me.txClabe1.Name = "txClabe1"
        Me.txClabe1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe1.Properties.MaxLength = 100
        Me.txClabe1.Size = New System.Drawing.Size(106, 20)
        Me.txClabe1.TabIndex = 2
        '
        'txCuenta1
        '
        Me.txCuenta1.Location = New System.Drawing.Point(133, 39)
        Me.txCuenta1.Name = "txCuenta1"
        Me.txCuenta1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCuenta1.Properties.MaxLength = 100
        Me.txCuenta1.Size = New System.Drawing.Size(97, 20)
        Me.txCuenta1.TabIndex = 1
        '
        'cbBanco1
        '
        Me.cbBanco1.Location = New System.Drawing.Point(5, 39)
        Me.cbBanco1.Name = "cbBanco1"
        Me.cbBanco1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbBanco1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbBanco1.Properties.MaxLength = 50
        Me.cbBanco1.Size = New System.Drawing.Size(122, 20)
        Me.cbBanco1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(235, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 131
        Me.Label2.Text = "Cuenta Clabe"
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(10, 22)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(36, 13)
        Me.Label86.TabIndex = 12
        Me.Label86.Text = "Banco"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(146, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "Cuenta/Sucursal"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl13)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(769, 627)
        Me.XtraTabPage1.Text = "Listado de Proveedores"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.RadioGroup1)
        Me.GroupControl1.Controls.Add(Me.chkTodos)
        Me.GroupControl1.Controls.Add(Me.cbProv)
        Me.GroupControl1.Controls.Add(Me.Button24)
        Me.GroupControl1.Controls.Add(Me.Button22)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(769, 58)
        Me.GroupControl1.TabIndex = 132
        '
        'RadioGroup1
        '
        Me.RadioGroup1.EditValue = CType(1, Short)
        Me.RadioGroup1.Location = New System.Drawing.Point(387, 30)
        Me.RadioGroup1.MenuManager = Me.BarManager1
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(1, Short), "Activos"), New DevExpress.XtraEditors.Controls.RadioGroupItem(CType(0, Short), "Inactivos")})
        Me.RadioGroup1.Size = New System.Drawing.Size(151, 26)
        Me.RadioGroup1.TabIndex = 13
        '
        'chkTodos
        '
        Me.chkTodos.Location = New System.Drawing.Point(279, 33)
        Me.chkTodos.MenuManager = Me.BarManager1
        Me.chkTodos.Name = "chkTodos"
        Me.chkTodos.Properties.Caption = "Mostrar todos"
        Me.chkTodos.Size = New System.Drawing.Size(102, 19)
        Me.chkTodos.TabIndex = 12
        '
        'cbProv
        '
        Me.cbProv.Location = New System.Drawing.Point(5, 30)
        Me.cbProv.MenuManager = Me.BarManager1
        Me.cbProv.Name = "cbProv"
        Me.cbProv.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbProv.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbProv.Size = New System.Drawing.Size(270, 20)
        Me.cbProv.TabIndex = 11
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(654, 33)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(104, 23)
        Me.Button24.TabIndex = 10
        Me.Button24.Text = "Imprimir Exportar"
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(544, 33)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(104, 23)
        Me.Button22.TabIndex = 9
        Me.Button22.Text = "Mostrar"
        '
        'GroupControl13
        '
        Me.GroupControl13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl13.Controls.Add(Me.GridControl5)
        Me.GroupControl13.Location = New System.Drawing.Point(0, 59)
        Me.GroupControl13.Name = "GroupControl13"
        Me.GroupControl13.Size = New System.Drawing.Size(744, 571)
        Me.GroupControl13.TabIndex = 131
        '
        'GridControl5
        '
        Me.GridControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl5.Location = New System.Drawing.Point(2, 21)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(740, 548)
        Me.GridControl5.TabIndex = 5
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView5.OptionsBehavior.Editable = False
        Me.GridView5.OptionsFind.AlwaysVisible = True
        Me.GridView5.OptionsView.ColumnAutoWidth = False
        Me.GridView5.OptionsView.RowAutoHeight = True
        Me.GridView5.OptionsView.ShowAutoFilterRow = True
        Me.GridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.GridView5.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'proveedorescat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(781, 683)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "proveedorescat"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Control de Proveedores"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl3.ResumeLayout(False)
        Me.XtraTabPage13.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.txMailProp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txRadioProp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTelProp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txResponsable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.txEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txObserva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txWeb.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNextel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txFax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTel2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTel1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNomCorto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbRazon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.chkCopiar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFletePaga.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txConvTrans.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTransporte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuentaCont.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txEmailPagos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txContactoPago.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLimCrPro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLimCrInt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDesc3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDesc2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDesc1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPlazo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPlazo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPlazo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.txClabe6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCuenta1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbBanco1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkTodos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbProv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl13.ResumeLayout(False)
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabControl3 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage13 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txMailProp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txRadioProp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTelProp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txResponsable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Button10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txObserva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txWeb As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txNextel As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txFax As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTel2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTel1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbCP As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbColonia As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbCiudad As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txNomCorto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbRazon As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkFletePaga As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txConvTrans As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbTransporte As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txCuentaCont As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txEmailPagos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txContactoPago As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txLimCrPro As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txLimCrInt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nudDesc3 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nudDesc2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nudDesc1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nudPlazo3 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nudPlazo2 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nudPlazo1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txClabe6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco6 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txClabe5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco5 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txClabe4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txClabe3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txClabe2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txClabe1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCuenta1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbBanco1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Button24 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button22 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl13 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents chkactivo As System.Windows.Forms.CheckBox
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents chkCopiar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkTodos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cbProv As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
End Class
