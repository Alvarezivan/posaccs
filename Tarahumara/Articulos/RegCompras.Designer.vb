﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RegCompras
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RegCompras))
        Me.chkreingreso = New DevExpress.XtraEditors.CheckEdit()
        Me.bsalir = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.bpreguardar = New DevExpress.XtraEditors.SimpleButton()
        Me.bguardar = New DevExpress.XtraEditors.SimpleButton()
        Me.gc1 = New DevExpress.XtraEditors.GroupControl()
        Me.tbcodigo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.tbcantidad = New DevExpress.XtraEditors.TextEdit()
        Me.bingresar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.tbtotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.tbflete = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.tbdesc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.tbiva = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.tbsubtotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.dtllegada = New System.Windows.Forms.DateTimePicker()
        Me.dtfactura = New System.Windows.Forms.DateTimePicker()
        Me.SimpleButton9 = New DevExpress.XtraEditors.SimpleButton()
        Me.lbpass = New System.Windows.Forms.Label()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.cbtienda = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.cbrazon = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.tbpass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.cbusuario = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tbfactura = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cbproved = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.dgvcompra = New System.Windows.Forms.DataGridView()
        Me.idproducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subfamilia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.marca = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.costou = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.costot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.iva = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ivau = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Etiqueta = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.SimpleButton18 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.chkreingreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gc1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gc1.SuspendLayout()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbflete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbsubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbtienda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbrazon.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbusuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbfactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbproved.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvcompra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkreingreso
        '
        Me.chkreingreso.Enabled = False
        Me.chkreingreso.Location = New System.Drawing.Point(768, 622)
        Me.chkreingreso.Name = "chkreingreso"
        Me.chkreingreso.Properties.Caption = "Reingreso"
        Me.chkreingreso.Size = New System.Drawing.Size(75, 19)
        Me.chkreingreso.TabIndex = 84
        '
        'bsalir
        '
        Me.bsalir.Location = New System.Drawing.Point(806, 560)
        Me.bsalir.Name = "bsalir"
        Me.bsalir.Size = New System.Drawing.Size(49, 47)
        Me.bsalir.TabIndex = 83
        Me.bsalir.Text = "Salir"
        '
        'SimpleButton7
        '
        Me.SimpleButton7.Location = New System.Drawing.Point(11, 555)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(62, 22)
        Me.SimpleButton7.TabIndex = 82
        Me.SimpleButton7.Text = "Imp/Exp"
        '
        'bpreguardar
        '
        Me.bpreguardar.Enabled = False
        Me.bpreguardar.Location = New System.Drawing.Point(728, 560)
        Me.bpreguardar.Name = "bpreguardar"
        Me.bpreguardar.Size = New System.Drawing.Size(72, 47)
        Me.bpreguardar.TabIndex = 81
        Me.bpreguardar.Text = "PreGuardar"
        '
        'bguardar
        '
        Me.bguardar.Enabled = False
        Me.bguardar.Location = New System.Drawing.Point(660, 560)
        Me.bguardar.Name = "bguardar"
        Me.bguardar.Size = New System.Drawing.Size(62, 47)
        Me.bguardar.TabIndex = 80
        Me.bguardar.Text = "Guardar"
        '
        'gc1
        '
        Me.gc1.Controls.Add(Me.SimpleButton18)
        Me.gc1.Controls.Add(Me.tbcodigo)
        Me.gc1.Controls.Add(Me.LabelControl13)
        Me.gc1.Controls.Add(Me.SimpleButton3)
        Me.gc1.Controls.Add(Me.tbcantidad)
        Me.gc1.Controls.Add(Me.bingresar)
        Me.gc1.Controls.Add(Me.LabelControl15)
        Me.gc1.Enabled = False
        Me.gc1.Location = New System.Drawing.Point(1, 119)
        Me.gc1.Name = "gc1"
        Me.gc1.Size = New System.Drawing.Size(854, 64)
        Me.gc1.TabIndex = 79
        Me.gc1.Text = "Agregar Productos"
        '
        'tbcodigo
        '
        Me.tbcodigo.Location = New System.Drawing.Point(10, 37)
        Me.tbcodigo.Name = "tbcodigo"
        Me.tbcodigo.Properties.MaxLength = 25
        Me.tbcodigo.Size = New System.Drawing.Size(115, 20)
        Me.tbcodigo.TabIndex = 0
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(29, 22)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(73, 13)
        Me.LabelControl13.TabIndex = 26
        Me.LabelControl13.Text = "Ingrese Codigo"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(411, 34)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(151, 23)
        Me.SimpleButton3.TabIndex = 32
        Me.SimpleButton3.Text = "Borrar Renglon Seleccionado"
        '
        'tbcantidad
        '
        Me.tbcantidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbcantidad.Location = New System.Drawing.Point(144, 37)
        Me.tbcantidad.Name = "tbcantidad"
        Me.tbcantidad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcantidad.Properties.Appearance.Options.UseFont = True
        Me.tbcantidad.Properties.Mask.EditMask = "n0"
        Me.tbcantidad.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbcantidad.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbcantidad.ShowToolTips = False
        Me.tbcantidad.Size = New System.Drawing.Size(87, 20)
        Me.tbcantidad.TabIndex = 1
        '
        'bingresar
        '
        Me.bingresar.Location = New System.Drawing.Point(261, 34)
        Me.bingresar.Name = "bingresar"
        Me.bingresar.Size = New System.Drawing.Size(75, 23)
        Me.bingresar.TabIndex = 2
        Me.bingresar.Text = "Ingresar"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(144, 22)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(87, 13)
        Me.LabelControl15.TabIndex = 30
        Me.LabelControl15.Text = "Cantidad Comprar"
        '
        'tbtotal
        '
        Me.tbtotal.Location = New System.Drawing.Point(546, 607)
        Me.tbtotal.Name = "tbtotal"
        Me.tbtotal.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.tbtotal.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtotal.Properties.Appearance.Options.UseBackColor = True
        Me.tbtotal.Properties.Appearance.Options.UseFont = True
        Me.tbtotal.Properties.Mask.EditMask = "c2"
        Me.tbtotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbtotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbtotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbtotal.Size = New System.Drawing.Size(101, 24)
        Me.tbtotal.TabIndex = 77
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl20.Location = New System.Drawing.Point(491, 608)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(49, 23)
        Me.LabelControl20.TabIndex = 78
        Me.LabelControl20.Text = "Total"
        '
        'tbflete
        '
        Me.tbflete.EditValue = "0"
        Me.tbflete.Location = New System.Drawing.Point(393, 609)
        Me.tbflete.Name = "tbflete"
        Me.tbflete.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbflete.Properties.Appearance.Options.UseFont = True
        Me.tbflete.Properties.Mask.EditMask = "d2"
        Me.tbflete.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbflete.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbflete.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbflete.Size = New System.Drawing.Size(82, 22)
        Me.tbflete.TabIndex = 75
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl19.Location = New System.Drawing.Point(340, 610)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(33, 19)
        Me.LabelControl19.TabIndex = 76
        Me.LabelControl19.Text = "Flete"
        '
        'tbdesc
        '
        Me.tbdesc.EditValue = "0"
        Me.tbdesc.Location = New System.Drawing.Point(393, 587)
        Me.tbdesc.Name = "tbdesc"
        Me.tbdesc.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbdesc.Properties.Appearance.Options.UseFont = True
        Me.tbdesc.Properties.Mask.EditMask = "d2"
        Me.tbdesc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbdesc.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbdesc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbdesc.Size = New System.Drawing.Size(82, 22)
        Me.tbdesc.TabIndex = 73
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl18.Location = New System.Drawing.Point(340, 588)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(33, 19)
        Me.LabelControl18.TabIndex = 74
        Me.LabelControl18.Text = "Desc"
        '
        'tbiva
        '
        Me.tbiva.Location = New System.Drawing.Point(222, 606)
        Me.tbiva.Name = "tbiva"
        Me.tbiva.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.tbiva.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbiva.Properties.Appearance.Options.UseBackColor = True
        Me.tbiva.Properties.Appearance.Options.UseFont = True
        Me.tbiva.Properties.Mask.EditMask = "c2"
        Me.tbiva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbiva.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbiva.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbiva.Size = New System.Drawing.Size(101, 22)
        Me.tbiva.TabIndex = 71
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl17.Location = New System.Drawing.Point(189, 609)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(27, 19)
        Me.LabelControl17.TabIndex = 72
        Me.LabelControl17.Text = "IVA"
        '
        'tbsubtotal
        '
        Me.tbsubtotal.Location = New System.Drawing.Point(74, 606)
        Me.tbsubtotal.Name = "tbsubtotal"
        Me.tbsubtotal.Properties.Appearance.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.tbsubtotal.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbsubtotal.Properties.Appearance.Options.UseBackColor = True
        Me.tbsubtotal.Properties.Appearance.Options.UseFont = True
        Me.tbsubtotal.Properties.Mask.EditMask = "c2"
        Me.tbsubtotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbsubtotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbsubtotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbsubtotal.Size = New System.Drawing.Size(101, 22)
        Me.tbsubtotal.TabIndex = 70
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl16.Location = New System.Drawing.Point(5, 609)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(63, 19)
        Me.LabelControl16.TabIndex = 69
        Me.LabelControl16.Text = "SubTotal"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.dtllegada)
        Me.GroupControl1.Controls.Add(Me.dtfactura)
        Me.GroupControl1.Controls.Add(Me.SimpleButton9)
        Me.GroupControl1.Controls.Add(Me.lbpass)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.cbtienda)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.cbrazon)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.tbpass)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.cbusuario)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Controls.Add(Me.tbfactura)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.cbproved)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(1, 1)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(854, 119)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Datos Factura"
        '
        'dtllegada
        '
        Me.dtllegada.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtllegada.Location = New System.Drawing.Point(179, 87)
        Me.dtllegada.Name = "dtllegada"
        Me.dtllegada.Size = New System.Drawing.Size(85, 21)
        Me.dtllegada.TabIndex = 3
        '
        'dtfactura
        '
        Me.dtfactura.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtfactura.Location = New System.Drawing.Point(81, 86)
        Me.dtfactura.Name = "dtfactura"
        Me.dtfactura.Size = New System.Drawing.Size(85, 21)
        Me.dtfactura.TabIndex = 2
        '
        'SimpleButton9
        '
        Me.SimpleButton9.Location = New System.Drawing.Point(625, 43)
        Me.SimpleButton9.Name = "SimpleButton9"
        Me.SimpleButton9.Size = New System.Drawing.Size(97, 65)
        Me.SimpleButton9.TabIndex = 8
        Me.SimpleButton9.Text = "Iniciar"
        '
        'lbpass
        '
        Me.lbpass.AutoSize = True
        Me.lbpass.Location = New System.Drawing.Point(694, 46)
        Me.lbpass.Name = "lbpass"
        Me.lbpass.Size = New System.Drawing.Size(0, 13)
        Me.lbpass.TabIndex = 27
        Me.lbpass.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(516, 72)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl6.TabIndex = 26
        Me.LabelControl6.Text = "Contraseña"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(179, 67)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl5.TabIndex = 25
        Me.LabelControl5.Text = "Fecha Llegada"
        '
        'cbtienda
        '
        Me.cbtienda.Location = New System.Drawing.Point(299, 88)
        Me.cbtienda.Name = "cbtienda"
        Me.cbtienda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbtienda.Size = New System.Drawing.Size(175, 20)
        Me.cbtienda.TabIndex = 5
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl12.Location = New System.Drawing.Point(299, 69)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(105, 16)
        Me.LabelControl12.TabIndex = 23
        Me.LabelControl12.Text = "Tienda/Almacen"
        '
        'cbrazon
        '
        Me.cbrazon.Location = New System.Drawing.Point(299, 43)
        Me.cbrazon.Name = "cbrazon"
        Me.cbrazon.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbrazon.Size = New System.Drawing.Size(175, 20)
        Me.cbrazon.TabIndex = 4
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl11.Location = New System.Drawing.Point(299, 24)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(81, 16)
        Me.LabelControl11.TabIndex = 21
        Me.LabelControl11.Text = "Razon Social"
        '
        'tbpass
        '
        Me.tbpass.Location = New System.Drawing.Point(498, 88)
        Me.tbpass.Name = "tbpass"
        Me.tbpass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbpass.Size = New System.Drawing.Size(86, 20)
        Me.tbpass.TabIndex = 7
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(526, 26)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Usuario"
        '
        'cbusuario
        '
        Me.cbusuario.Location = New System.Drawing.Point(498, 43)
        Me.cbusuario.Name = "cbusuario"
        Me.cbusuario.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbusuario.Properties.CaseSensitiveSearch = True
        Me.cbusuario.Size = New System.Drawing.Size(86, 20)
        Me.cbusuario.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(81, 67)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Fecha Factura"
        '
        'tbfactura
        '
        Me.tbfactura.Location = New System.Drawing.Point(10, 86)
        Me.tbfactura.Name = "tbfactura"
        Me.tbfactura.Size = New System.Drawing.Size(57, 20)
        Me.tbfactura.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(10, 67)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "No. Factura"
        '
        'cbproved
        '
        Me.cbproved.Location = New System.Drawing.Point(10, 43)
        Me.cbproved.Name = "cbproved"
        Me.cbproved.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbproved.Size = New System.Drawing.Size(255, 20)
        Me.cbproved.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(10, 24)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 16)
        Me.LabelControl1.TabIndex = 4
        Me.LabelControl1.Text = "Proveedor"
        '
        'dgvcompra
        '
        Me.dgvcompra.AllowUserToDeleteRows = False
        Me.dgvcompra.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvcompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcompra.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idproducto, Me.codigo, Me.subfamilia, Me.marca, Me.Descripcion, Me.cantidad, Me.costou, Me.costot, Me.iva, Me.ivau, Me.Etiqueta})
        Me.dgvcompra.Location = New System.Drawing.Point(11, 206)
        Me.dgvcompra.Name = "dgvcompra"
        Me.dgvcompra.ReadOnly = True
        Me.dgvcompra.Size = New System.Drawing.Size(834, 343)
        Me.dgvcompra.TabIndex = 85
        '
        'idproducto
        '
        Me.idproducto.HeaderText = "IdProducto"
        Me.idproducto.Name = "idproducto"
        Me.idproducto.ReadOnly = True
        Me.idproducto.Visible = False
        '
        'codigo
        '
        Me.codigo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.codigo.HeaderText = "Codigo"
        Me.codigo.Name = "codigo"
        Me.codigo.ReadOnly = True
        Me.codigo.Width = 65
        '
        'subfamilia
        '
        Me.subfamilia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.subfamilia.HeaderText = "SubFamilia"
        Me.subfamilia.Name = "subfamilia"
        Me.subfamilia.ReadOnly = True
        Me.subfamilia.Width = 82
        '
        'marca
        '
        Me.marca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.marca.HeaderText = "Marca"
        Me.marca.Name = "marca"
        Me.marca.ReadOnly = True
        Me.marca.Width = 61
        '
        'Descripcion
        '
        Me.Descripcion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'cantidad
        '
        Me.cantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.cantidad.HeaderText = "Cantidad"
        Me.cantidad.Name = "cantidad"
        Me.cantidad.ReadOnly = True
        Me.cantidad.Width = 75
        '
        'costou
        '
        Me.costou.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.costou.HeaderText = "Costo U"
        Me.costou.Name = "costou"
        Me.costou.ReadOnly = True
        Me.costou.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.costou.Width = 70
        '
        'costot
        '
        Me.costot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.costot.HeaderText = "Costo T"
        Me.costot.Name = "costot"
        Me.costot.ReadOnly = True
        Me.costot.Width = 69
        '
        'iva
        '
        Me.iva.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.iva.HeaderText = "IVA"
        Me.iva.Name = "iva"
        Me.iva.ReadOnly = True
        Me.iva.Visible = False
        '
        'ivau
        '
        Me.ivau.HeaderText = "Ivau"
        Me.ivau.Name = "ivau"
        Me.ivau.ReadOnly = True
        Me.ivau.Visible = False
        '
        'Etiqueta
        '
        Me.Etiqueta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Etiqueta.HeaderText = "Etiquetas"
        Me.Etiqueta.Name = "Etiqueta"
        Me.Etiqueta.ReadOnly = True
        Me.Etiqueta.Text = "Etiqueta"
        Me.Etiqueta.Width = 58
        '
        'SimpleButton18
        '
        Me.SimpleButton18.Location = New System.Drawing.Point(727, 34)
        Me.SimpleButton18.Name = "SimpleButton18"
        Me.SimpleButton18.Size = New System.Drawing.Size(115, 23)
        Me.SimpleButton18.TabIndex = 92
        Me.SimpleButton18.Text = "Buscar Articulo"
        '
        'RegCompras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(857, 645)
        Me.Controls.Add(Me.dgvcompra)
        Me.Controls.Add(Me.chkreingreso)
        Me.Controls.Add(Me.bsalir)
        Me.Controls.Add(Me.SimpleButton7)
        Me.Controls.Add(Me.bpreguardar)
        Me.Controls.Add(Me.bguardar)
        Me.Controls.Add(Me.gc1)
        Me.Controls.Add(Me.tbtotal)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.tbflete)
        Me.Controls.Add(Me.LabelControl19)
        Me.Controls.Add(Me.tbdesc)
        Me.Controls.Add(Me.LabelControl18)
        Me.Controls.Add(Me.tbiva)
        Me.Controls.Add(Me.LabelControl17)
        Me.Controls.Add(Me.tbsubtotal)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "RegCompras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registro de Compras"
        CType(Me.chkreingreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gc1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gc1.ResumeLayout(False)
        Me.gc1.PerformLayout()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbflete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbsubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.cbtienda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbrazon.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbusuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbfactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbproved.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvcompra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkreingreso As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents bsalir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bpreguardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bguardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents gc1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbcantidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents bingresar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbtotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbflete As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbdesc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbiva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbsubtotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents cbtienda As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbrazon As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbpass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbusuario As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbfactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbproved As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbpass As System.Windows.Forms.Label
    Friend WithEvents SimpleButton9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dgvcompra As System.Windows.Forms.DataGridView
    Public WithEvents tbcodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents dtllegada As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtfactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents idproducto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents subfamilia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents marca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents costou As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents costot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents iva As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ivau As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Etiqueta As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents SimpleButton18 As DevExpress.XtraEditors.SimpleButton
End Class
