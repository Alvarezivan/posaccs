﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Image
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns

Public Class AjusteAlmacen

    Private Sub EntradaAlmacen_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Entrada de Almacen] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        RegistraAcceso(conexion, "Entrada Almacen [EntradaAlmacen.frm]")
        Dim qry As String = "select max(numero) from almacen"
        Dim numero As String = bdBase.bdExecute(conexion, qry)
        If numero = "" Then
            numero = "0"
        End If
        lbentrada.Text = Val(numero) + 1
        lbfecha.Text = Now.Date
        llenacombos()
        llenacombomanual()
        cbtipo.DropDownStyle = ComboBoxStyle.DropDownList
        Splash(False)

    End Sub

    Private Sub llenacombos()
        Splash(True)
        Dim qry As String = "SELECT numero, nombre from tiendas where borrada=0 ORDER BY nombre"
        LlenaCombobox(cbtienda, qry, "nombre", "numero", conexion)
        Splash(False)
    End Sub

    Private Sub llenacombomanual()

        Dim dt As DataTable = New DataTable("Tipos")

        dt.Columns.Add("ID")
        dt.Columns.Add("Tipo")
        Dim dr As DataRow

        dr = dt.NewRow()
        dr("ID") = "1"
        dr("Tipo") = "ENTRADA"
        dt.Rows.Add(dr)

        dr = dt.NewRow()
        dr("ID") = "2"
        dr("Tipo") = "SALIDA"
        dt.Rows.Add(dr)

        cbtipo.DataSource = dt
        cbtipo.ValueMember = "ID"
        cbtipo.DisplayMember = "Tipo"
    End Sub


End Class