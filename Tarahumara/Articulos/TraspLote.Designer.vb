﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Trasplote
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Trasplote))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cbproved = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.tbfactura = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.cbtienda = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.rbtodos = New System.Windows.Forms.RadioButton()
        Me.rbptras = New System.Windows.Forms.RadioButton()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.Cbarticulo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.chktodos = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.chkmismacant = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcodigo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcantidad = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.chkvalidar = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Folio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.tbcomenta = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.cbproved.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbfactura.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbtienda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cbarticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chktodos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkmismacant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkvalidar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcomenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Proveedor"
        '
        'cbproved
        '
        Me.cbproved.Location = New System.Drawing.Point(12, 31)
        Me.cbproved.Name = "cbproved"
        Me.cbproved.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbproved.Size = New System.Drawing.Size(158, 20)
        Me.cbproved.TabIndex = 0
        '
        'tbfactura
        '
        Me.tbfactura.Location = New System.Drawing.Point(176, 31)
        Me.tbfactura.Name = "tbfactura"
        Me.tbfactura.Size = New System.Drawing.Size(87, 20)
        Me.tbfactura.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(176, 12)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "No. Factura"
        '
        'cbtienda
        '
        Me.cbtienda.Location = New System.Drawing.Point(290, 31)
        Me.cbtienda.Name = "cbtienda"
        Me.cbtienda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbtienda.Size = New System.Drawing.Size(158, 20)
        Me.cbtienda.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(348, 12)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Tienda"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(290, 57)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Appearance.BackColor = System.Drawing.Color.White
        Me.RadioGroup1.Properties.Appearance.Options.UseBackColor = True
        Me.RadioGroup1.Size = New System.Drawing.Size(158, 23)
        Me.RadioGroup1.TabIndex = 6
        '
        'rbtodos
        '
        Me.rbtodos.AutoSize = True
        Me.rbtodos.BackColor = System.Drawing.Color.White
        Me.rbtodos.Location = New System.Drawing.Point(294, 60)
        Me.rbtodos.Name = "rbtodos"
        Me.rbtodos.Size = New System.Drawing.Size(54, 17)
        Me.rbtodos.TabIndex = 3
        Me.rbtodos.TabStop = True
        Me.rbtodos.Text = "Todos"
        Me.rbtodos.UseVisualStyleBackColor = False
        '
        'rbptras
        '
        Me.rbptras.AutoSize = True
        Me.rbptras.BackColor = System.Drawing.Color.White
        Me.rbptras.Location = New System.Drawing.Point(354, 60)
        Me.rbptras.Name = "rbptras"
        Me.rbptras.Size = New System.Drawing.Size(92, 17)
        Me.rbptras.TabIndex = 4
        Me.rbptras.TabStop = True
        Me.rbptras.Text = "Por Traspasar"
        Me.rbptras.UseVisualStyleBackColor = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(512, 12)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl4.TabIndex = 10
        Me.LabelControl4.Text = "Articulo"
        '
        'Cbarticulo
        '
        Me.Cbarticulo.Location = New System.Drawing.Point(454, 31)
        Me.Cbarticulo.Name = "Cbarticulo"
        Me.Cbarticulo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Cbarticulo.Size = New System.Drawing.Size(158, 20)
        Me.Cbarticulo.TabIndex = 5
        '
        'chktodos
        '
        Me.chktodos.Location = New System.Drawing.Point(557, 58)
        Me.chktodos.Name = "chktodos"
        Me.chktodos.Properties.Caption = "Todos"
        Me.chktodos.Size = New System.Drawing.Size(55, 19)
        Me.chktodos.TabIndex = 6
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(640, 24)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 27)
        Me.SimpleButton1.TabIndex = 8
        Me.SimpleButton1.Text = "Buscar"
        '
        'chkmismacant
        '
        Me.chkmismacant.Location = New System.Drawing.Point(638, 57)
        Me.chkmismacant.Name = "chkmismacant"
        Me.chkmismacant.Properties.Caption = "Misma Cantidad"
        Me.chkmismacant.Size = New System.Drawing.Size(100, 19)
        Me.chkmismacant.TabIndex = 7
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(12, 87)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(188, 13)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Ingresar por medio de Lector de Barras"
        '
        'tbcodigo
        '
        Me.tbcodigo.Location = New System.Drawing.Point(12, 106)
        Me.tbcodigo.Name = "tbcodigo"
        Me.tbcodigo.Size = New System.Drawing.Size(188, 20)
        Me.tbcodigo.TabIndex = 10
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(217, 87)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(101, 13)
        Me.LabelControl6.TabIndex = 17
        Me.LabelControl6.Text = "Cantidad a traspasar"
        '
        'tbcantidad
        '
        Me.tbcantidad.Location = New System.Drawing.Point(217, 106)
        Me.tbcantidad.Name = "tbcantidad"
        Me.tbcantidad.Size = New System.Drawing.Size(101, 20)
        Me.tbcantidad.TabIndex = 11
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(336, 103)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 12
        Me.SimpleButton2.Text = "Ingresar"
        '
        'chkvalidar
        '
        Me.chkvalidar.Location = New System.Drawing.Point(747, 32)
        Me.chkvalidar.Name = "chkvalidar"
        Me.chkvalidar.Properties.Caption = "Validar"
        Me.chkvalidar.Size = New System.Drawing.Size(60, 19)
        Me.chkvalidar.TabIndex = 9
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(512, 103)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton3.TabIndex = 13
        Me.SimpleButton3.Text = "Validar"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(618, 103)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(85, 23)
        Me.SimpleButton4.TabIndex = 15
        Me.SimpleButton4.Text = "Validad Sobrantes"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton5.Appearance.Options.UseFont = True
        Me.SimpleButton5.Location = New System.Drawing.Point(709, 103)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(86, 23)
        Me.SimpleButton5.TabIndex = 16
        Me.SimpleButton5.Text = "Validad Faltantes"
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(12, 142)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(795, 294)
        Me.GridControl2.TabIndex = 43
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Folio})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView2.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowRowSizing = True
        Me.GridView2.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView2.OptionsFind.AlwaysVisible = True
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'Folio
        '
        Me.Folio.Caption = "Folio"
        Me.Folio.Name = "Folio"
        Me.Folio.Visible = True
        Me.Folio.VisibleIndex = 0
        '
        'SimpleButton6
        '
        Me.SimpleButton6.Location = New System.Drawing.Point(628, 453)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton6.TabIndex = 17
        Me.SimpleButton6.Text = "Imp / Exp"
        '
        'SimpleButton7
        '
        Me.SimpleButton7.Location = New System.Drawing.Point(732, 453)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton7.TabIndex = 18
        Me.SimpleButton7.Text = "Salir"
        '
        'tbcomenta
        '
        Me.tbcomenta.Location = New System.Drawing.Point(12, 456)
        Me.tbcomenta.Name = "tbcomenta"
        Me.tbcomenta.Properties.Appearance.BackColor = System.Drawing.Color.SkyBlue
        Me.tbcomenta.Properties.Appearance.Options.UseBackColor = True
        Me.tbcomenta.Size = New System.Drawing.Size(228, 20)
        Me.tbcomenta.TabIndex = 14
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(12, 442)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl7.TabIndex = 47
        Me.LabelControl7.Text = "Comentarios"
        '
        'Trasplote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(819, 488)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.tbcomenta)
        Me.Controls.Add(Me.SimpleButton7)
        Me.Controls.Add(Me.SimpleButton6)
        Me.Controls.Add(Me.GridControl2)
        Me.Controls.Add(Me.SimpleButton5)
        Me.Controls.Add(Me.SimpleButton4)
        Me.Controls.Add(Me.SimpleButton3)
        Me.Controls.Add(Me.chkvalidar)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.tbcantidad)
        Me.Controls.Add(Me.tbcodigo)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.chkmismacant)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.chktodos)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.Cbarticulo)
        Me.Controls.Add(Me.rbptras)
        Me.Controls.Add(Me.rbtodos)
        Me.Controls.Add(Me.RadioGroup1)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.cbtienda)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.tbfactura)
        Me.Controls.Add(Me.cbproved)
        Me.Controls.Add(Me.LabelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Trasplote"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Traspaso por Lote"
        CType(Me.cbproved.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbfactura.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbtienda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cbarticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chktodos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkmismacant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkvalidar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcomenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbproved As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents tbfactura As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbtienda As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rbtodos As System.Windows.Forms.RadioButton
    Friend WithEvents rbptras As System.Windows.Forms.RadioButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Cbarticulo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents chktodos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkmismacant As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcantidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkvalidar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Folio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbcomenta As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class
