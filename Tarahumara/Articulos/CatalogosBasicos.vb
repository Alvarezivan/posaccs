﻿
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions

Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraGrid.Views.Base


Public Class CatalogosBasicos
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                       oConfig.pBase & ";Persist Security Info=True;User ID=" & oConfig.pUsuario & _
    '                       ";password=" & oConfig.pClave

    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case RadioGroup1.SelectedIndex
            Case 0
                reloadCatalogo("Familias", "Familia")
                LabelControl2.Text = "Familias"
            Case 1
                reloadCatalogo("SubFamilias", "SubFamilia")
                LabelControl2.Text = "SubFamilias"
            Case 2
                reloadCatalogo("Temporadas", "Temporada")
                LabelControl2.Text = "Temporadas"
            Case 3
                reloadCatalogo("Clasificacion", "Clasificacion")
                LabelControl2.Text = "Clasificacion"
            Case 4
                reloadCatalogo("Marcas", "Marca")
                LabelControl2.Text = "Marcas"
        End Select
    End Sub
    
    Private Sub reloadCatalogo(ByVal tabla As String, ByVal campo As String)
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        GridControl1.DataSource = Nothing
        Dim qry As String = String.Empty
        Select Case tabla
            Case "Familias"
                qry = "SELECT         Familias.idFamilia,Familias.Familia, Familias.Estado, ISNULL(empleado.NOMBRE, 'NI') AS UsuarioAlta, ISNULL(empleado_1.NOMBRE, 'NI')  " & _
                      "   AS UsuarioModificacion, CAST(Familias.FechaAlta AS varchar) AS FechaAlta, CAST(Familias.UltimaModificacion AS varchar) AS UltimaModificacion,  " & _
                " Familias.NombreAnterior, familias.llave as LlaveUnica FROM Familias LEFT OUTER JOIN  empleado AS empleado_1 ON Familias.UsuarioModificacion = empleado_1.id LEFT OUTER JOIN " & _
                         " empleado ON Familias.UsuarioAlta = empleado.id ORDER BY Familias.Familia"
            Case "SubFamilias"
                qry = "SELECT         SubFamilias.idSubFamilia,SubFamilias.SubFamilia, SubFamilias.Estado, ISNULL(empleado.NOMBRE, 'NI') AS UsuarioAlta, ISNULL(empleado_1.NOMBRE, 'NI')  " & _
                      "   AS UsuarioModificacion, CAST(SubFamilias.FechaAlta AS varchar) AS FechaAlta, CAST(SubFamilias.UltimaModificacion AS varchar) AS UltimaModificacion,  " & _
                " SubFamilias.NombreAnterior, SubFamilias.llave as LlaveUnica FROM SubFamilias LEFT OUTER JOIN  empleado AS empleado_1 ON SubFamilias.UsuarioModificacion = empleado_1.id LEFT OUTER JOIN " & _
                         " empleado ON SubFamilias.UsuarioAlta = empleado.id ORDER BY SubFamilias.SubFamilia"

            Case "Temporadas"
                qry = "SELECT         Temporadas.idTemporada,Temporadas.Temporada, Temporadas.Estado, ISNULL(empleado.NOMBRE, 'NI') AS UsuarioAlta, ISNULL(empleado_1.NOMBRE, 'NI')  " & _
                 "   AS UsuarioModificacion, CAST(Temporadas.FechaAlta AS varchar) AS FechaAlta, CAST(Temporadas.UltimaModificacion AS varchar) AS UltimaModificacion,  " & _
           " Temporadas.NombreAnterior, Temporadas.llave as LlaveUnica FROM Temporadas LEFT OUTER JOIN  empleado AS empleado_1 ON Temporadas.UsuarioModificacion = empleado_1.id LEFT OUTER JOIN " & _
                    " empleado ON Temporadas.UsuarioAlta = empleado.id ORDER BY Temporadas.Temporada"
            Case "Clasificacion"
                qry = "SELECT         Clasificacion.IdClasificacion,Clasificacion.Clasificacion, Clasificacion.Estado, ISNULL(empleado.NOMBRE, 'NI') AS UsuarioAlta, ISNULL(empleado_1.NOMBRE, 'NI')  " & _
               "   AS UsuarioModificacion, CAST(Clasificacion.FechaAlta AS varchar) AS FechaAlta, CAST(Clasificacion.UltimaModificacion AS varchar) AS UltimaModificacion,  " & _
         " Clasificacion.NombreAnterior, Clasificacion.llave as LlaveUnica FROM Clasificacion LEFT OUTER JOIN  empleado AS empleado_1 ON Clasificacion.UsuarioModificacion = empleado_1.id LEFT OUTER JOIN " & _
                  " empleado ON Clasificacion.UsuarioAlta = empleado.id ORDER BY Clasificacion.Clasificacion"
            Case "Marcas"
                qry = "SELECT         Marcas.idMarca,Marcas.Marca, Marcas.Estado, ISNULL(empleado.NOMBRE, 'NI') AS UsuarioAlta, ISNULL(empleado_1.NOMBRE, 'NI')  " & _
              "   AS UsuarioModificacion, CAST(Marcas.FechaAlta AS varchar) AS FechaAlta, CAST(Marcas.UltimaModificacion AS varchar) AS UltimaModificacion,  " & _
        " Marcas.NombreAnterior, Marcas.llave as LlaveUnica FROM Marcas LEFT OUTER JOIN  empleado AS empleado_1 ON Marcas.UsuarioModificacion = empleado_1.id LEFT OUTER JOIN " & _
                 " empleado ON Marcas.UsuarioAlta = empleado.id ORDER BY Marcas.Marca"
        End Select
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        If Not ds Is Nothing Then
            GridControl1.DataSource = ds.Tables(0)

        Else
        End If

     
        GridView1.PopulateColumns()
        GridView1.HorzScrollVisibility = True
        GridView1.VertScrollVisibility = True
        GridView1.BestFitColumns()
        Dim riCheck As New RepositoryItemCheckEdit()
        AddHandler riCheck.EditValueChanged, AddressOf riCheck_EditValueChanged
        GridView1.Columns("Estado").ColumnEdit = riCheck


        For x As Integer = 0 To 1
            GridView1.Columns(x).OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Next
        'GridControl1.HasChildren
        GridView1.Columns(0).OptionsColumn.AllowEdit = False
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub riCheck_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        GridView1.PostEditor()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            TextBox17.Focus()
            Exit Sub
        End If
        Splash(True)
        Dim qry As String
        Dim tabla As String = LabelControl2.Text
        Dim campo As String = String.Empty
        Dim llave As String = String.Empty
        Dim CadenaInsert As String = String.Empty
        Select Case RadioGroup1.SelectedIndex
            Case 0
                campo = "Familia"
                tabla = "Familias"
                llave = "idFamilia"
                CadenaInsert = "([Familia],[llave],[Estado],[UsuarioAlta],[UsuarioModificacion])"
            Case 1
                campo = "SubFamilia"
                tabla = "SubFamilias"
                llave = "idSubFamilia"
                CadenaInsert = "([SubFamilia],[llave],[Estado],[UsuarioAlta],[UsuarioModificacion])"
            Case 2
                campo = "Temporada"
                tabla = "Temporadas"
                llave = "idTemporada"
                CadenaInsert = "([Temporada],[llave],[Estado],[UsuarioAlta],[UsuarioModificacion])"
            Case 3
                campo = "Clasificacion"
                tabla = "Clasificacion"
                llave = "idClasificacion"
                CadenaInsert = "([Clasificacion],[llave],[Estado],[UsuarioAlta],[UsuarioModificacion])"
            Case 4
                campo = "Marca"
                tabla = "Marcas"
                llave = "idMarca"
                CadenaInsert = "([Marca],[llave],[Estado],[UsuarioAlta],[UsuarioModificacion])"

        End Select

        qry = "Select " & llave & " from " & tabla & " where " & campo & " = '" & TextBox17.Text.ToString.Trim.ToUpper & "'"
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If dr2.HasRows Then
            dr2.Close()
            DevExpress.XtraEditors.XtraMessageBox.Show("Elemento ya existe!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        dr2.Close()


        qry = "Insert into " & tabla & "" & CadenaInsert & " values ('" & TextBox17.Text.ToString.Trim.ToUpper & "', newid(), 1,'" & oLogin.pUserId.ToString.Trim & "','" & oLogin.pUserId.ToString.Trim & "')"
        bdBase.bdExecute(conexion, qry)
        Splash(False)
        reloadCatalogo(tabla, campo)
        TextBox17.Text = ""
        DevExpress.XtraEditors.XtraMessageBox.Show("Elemento ha sido almacenado!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Me.Cursor = Cursors.WaitCursor
        Dim row As DataRow = GridView1.GetDataRow(e.RowHandle)
        Dim col As String = String.Empty

        Dim qry, campo, tabla, llave, anterior As String
        Dim cFecha As String = FechaHoraSvr()
        'If e.Column.Name = "colEstado" Then
        '    col = row(1).ToString

        'Else
        '    col = e.Value.ToString

        'End If
        Dim opci As Integer = IIf(row.Item("Estado"), 1, 0)
        col = row(1)

        Select Case RadioGroup1.SelectedIndex
            Case 0
                'busco el nombre actual que sera modificado
                campo = "Familia"
                tabla = "Familias"
                llave = "idFamilia"
                qry = "Select " & campo & " from " & tabla & " where " & llave & " = '" & row.Item("idfamilia") & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    anterior = dr2("Familia").ToString.Trim

                Else
                    anterior = "No localizado"
                End If
                dr2.Close()
                qry = "update  familias set familia='" & col.Trim.ToUpper & "',UsuarioModificacion='" & oLogin.pUserId.ToString.Trim & _
                    "', [UltimaModificacion]=getdate(), [NombreAnterior]='" & anterior & "', estado='" & opci & "' where idfamilia='" & row.Item("idfamilia") & "'"
           
            Case 1
                'busco el nombre actual que sera modificado
                campo = "SubFamilia"
                tabla = "SubFamilias"
                llave = "idSubFamilia"
                qry = "Select " & campo & " from " & tabla & " where " & llave & " = '" & row.Item("idSubFamilia") & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    anterior = dr2("SubFamilia").ToString.Trim
                Else
                    anterior = "No localizado"
                End If
                dr2.Close()
                qry = "update  Subfamilias set Subfamilia='" & col.Trim.ToUpper & "',UsuarioModificacion='" & oLogin.pUserId.ToString.Trim & _
                    "', [UltimaModificacion]=getdate(), [NombreAnterior]='" & anterior & "', estado='" & opci & "' where idSubfamilia='" & row.Item("idSubFamilia") & "'"

            Case 2
                campo = "Temporada"
                tabla = "Temporadas"
                llave = "idTemporada"
                qry = "Select " & campo & " from " & tabla & " where " & llave & " = '" & row.Item("idTemporada") & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    anterior = dr2("Temporada").ToString.Trim
                Else
                    anterior = "No localizado"
                End If
                dr2.Close()
                qry = "update  Temporadas set Temporada='" & col.Trim.ToUpper & "',UsuarioModificacion='" & oLogin.pUserId.ToString.Trim & _
                    "', [UltimaModificacion]=getdate(), [NombreAnterior]='" & anterior & "', estado='" & opci & "' where idTemporada='" & row.Item("idTemporada") & "'"

            Case 3
                campo = "Clasificacion"
                tabla = "Clasificacion"
                llave = "idClasificacion"
                qry = "Select " & campo & " from " & tabla & " where " & llave & " = '" & row.Item("idClasificacion") & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    anterior = dr2("Clasificacion").ToString.Trim
                Else
                    anterior = "No localizado"
                End If
                dr2.Close()
                qry = "update  Clasificacion set Clasificacion='" & col.Trim.ToUpper & "',UsuarioModificacion='" & oLogin.pUserId.ToString.Trim & _
                    "', [UltimaModificacion]=getdate(), [NombreAnterior]='" & anterior & "', estado='" & opci & "' where idClasificacion='" & row.Item("idClasificacion") & "'"
           
            Case 4
                campo = "Marca"
                tabla = "Marcas"
                llave = "idMarca"
                qry = "Select " & campo & " from " & tabla & " where " & llave & " = '" & row.Item("idMarca") & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    anterior = dr2("Marca").ToString.Trim
                Else
                    anterior = "No localizado"
                End If
                dr2.Close()
                qry = "update  Marcas set Marca='" & col.Trim.ToUpper & "',UsuarioModificacion='" & oLogin.pUserId.ToString.Trim & _
                    "', [UltimaModificacion]=getdate(), [NombreAnterior]='" & anterior & "', estado='" & opci & "' where idClasificacion='" & row.Item("idMarca") & "'"
        End Select

        bdBase.bdExecute(conexion, qry, False, False)
        reloadCatalogo(tabla, campo)
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)

            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub CatalogosBasicos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        RegistraAcceso(conexion, "Productos, Catálogos básicos [CatálogosBasicos.frm]")
    End Sub

    Private Sub GroupControl3_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles GroupControl3.Paint

    End Sub
End Class