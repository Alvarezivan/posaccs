﻿Imports System.Data

Imports System.Data.SqlClient

Imports System.Drawing.Image

Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Controls
Public Class RptProductos

    Private Sub RptProductos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Splash(True)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Reporte de Productos] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        RegistraAcceso(conexion, "Reporte Productos [RptProductos.frm]")
        llenareporte()
        Splash(False)

    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Splash(True)



        Dim qry As String = "SELECT idproducto from skus where sku = '" & tbsku.Text.Trim & "'"
        Dim id As Integer = -1
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr("idproducto")) Then id = dr("idproducto").ToString.Trim
            End If

        End If
        dr.Close()

        If id <> -1 Then

            qry = "SELECT P.idProducto, P.Codigo, S.subFamilia,M.Marca,P.Descripcion,  F.Familia,sum(ex.cantidad) as Existencias, " & _
                "max(fac.fecha) as UltimaCompra,p.Costo,p.Precio, P.Resurtible, P.procedencia as Procedencia,  P.FechaAlta,  P.UltimaModificacion, " & _
                "pr.nombre as Proveedor,T.Temporada,P.Observaciones   FROM  Productos p INNER JOIN  Familias F ON P.idFamilia = F.idFamilia INNER JOIN " & _
                "SubFamilias s ON P.idSubFamilia = S.idSubFamilia INNER JOIN  Marcas m ON P.idMarca = M.idMarca INNER JOIN " & _
                "Temporadas t ON P.idTemporada = T.idTemporada  left join existen ex on p.idproducto = ex.idProducto left join facturadetalle " & _
                "fac on p.idproducto = fac.idproducto inner join proveed pr on pr.numero = p.idproveedor where p.idproducto = " & id & " group by P.idProducto, P.Codigo, S.subFamilia, " & _
                "P.Descripcion, M.Marca, F.Familia,  T.Temporada, P.Observaciones,    P.Precio, P.COSTO,PR.NOMBRE , P.Resurtible,  P.FechaAlta, " & _
                "P.UltimaModificacion, p.procedencia "


            Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
            GridControl2.DataSource = dr2.Tables(0)
            GridView2.PopulateColumns()
            'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView2.HorzScrollVisibility = True
            GridView2.VertScrollVisibility = True
            GridView2.BestFitColumns()
            GridView2.Columns("idProducto").Visible = False

            lbco.Visible = True
            lbcod.Visible = True
            lbcod.Text = tbsku.Text.Trim

        Else

            MsgBox("Codigo no localizado", MsgBoxStyle.Exclamation)

        End If

        Splash(False)
    End Sub

    Private Sub llenareporte()
        Splash(True)

        Dim qry As String = ""
        qry = "SELECT P.idProducto, P.Codigo, S.subFamilia,M.Marca,P.Descripcion,  F.Familia,isnull(sum(ex.cantidad),0) as Existencias, " & _
            "max(fac.fecha) as UltimaCompra,p.Costo,p.Precio,P.Depurado , P.Resurtible, P.Procedencia as Procedencia,  P.FechaAlta,  P.UltimaModificacion, " & _
            "pr.nombre as Proveedor,T.Temporada,P.Observaciones   FROM  Productos p INNER JOIN  Familias F ON P.idFamilia = F.idFamilia INNER JOIN " & _
            "SubFamilias s ON P.idSubFamilia = S.idSubFamilia INNER JOIN  Marcas m ON P.idMarca = M.idMarca INNER JOIN " & _
            "Temporadas t ON P.idTemporada = T.idTemporada  left join existen ex on p.idproducto = ex.idProducto left join facturadetalle " & _
            "fac on p.idproducto = fac.idproducto inner join proveed pr on pr.numero = p.idproveedor   group by P.idProducto, P.Codigo, S.subFamilia, " & _
            "P.Descripcion, M.Marca, F.Familia,  T.Temporada, P.Observaciones,    P.Precio, P.COSTO,PR.NOMBRE , P.Resurtible,  P.FechaAlta, " & _
            "P.UltimaModificacion, p.procedencia, p.depurado "


        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.HorzScrollVisibility = True
        GridView2.VertScrollVisibility = True
        GridView2.BestFitColumns()
        GridView2.Columns("idProducto").Visible = False
        Splash(False)

    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl2)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        lbco.Visible = False
        lbcod.Visible = False
        llenareporte()
    End Sub



    Private Sub GridControl2_Click(sender As System.Object, e As System.EventArgs) Handles GridControl2.DoubleClick

        If chketiq.Checked Then

            idcod = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "idProducto")
            can = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "Existencias")
            cod = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "Codigo")
            busq = True
            Etiquetas.Show()
        End If



    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        lbanalisis.Visible = True
        Dim cuantos As Integer = GridView2.RowCount
        Dim x As Integer

        For x = 0 To cuantos
            lbanalisis.Text = "Analizando... " & x & " / " & cuantos

            Dim cant As Integer = 0
            Dim fecha As Date = Today
            Dim id As Double

            cant = GridView2.GetRowCellValue(x, "Existencias")

            If IsDBNull(GridView2.GetRowCellValue(x, "UltimaCompra")) Then
            Else
                fecha = GridView2.GetRowCellValue(x, "UltimaCompra")
            End If

            id = GridView2.GetRowCellValue(x, "idProducto")
            Dim dif As Long = DateDiff(DateInterval.Day, fecha, Today)

            If cant = 0 And dif > 365 Then
                Dim qry As String = "update productos set depurado = 1 where idproducto ='" & id & "'"
                bdBase.bdExecute(conexion, qry)
            Else
                Dim qry As String = "update productos set depurado = 0 where idproducto ='" & id & "'"
                bdBase.bdExecute(conexion, qry)
            End If
        Next
        MsgBox("Productos analizados con Exito", MsgBoxStyle.Information)
        llenareporte()
    End Sub

    Private Sub GridView2_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView2.RowStyle
        If (e.RowHandle >= 0) Then

            Dim r As Boolean = GridView2.GetRowCellValue(e.RowHandle, "Resurtible")

            If Not r Then
                e.Appearance.ForeColor = Color.Red
            End If

        End If
    End Sub

    Private Sub GridControl2_Click_1(sender As System.Object, e As System.EventArgs) Handles GridControl2.Click

        Dim c As Integer = GridView2.FocusedColumn.ColumnHandle

        Dim r As Boolean = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "Resurtible")
        Dim d As Boolean = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "Depurado")
        Dim id As Integer = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "idProducto")
        Dim qry As String = ""

        If c = 11 Then
            If r = False Then
                GridView2.SetRowCellValue(GridView2.FocusedRowHandle, "Resurtible", True)
                qry = "update productos set resurtible = 1 where idproducto = " & id
                bdBase.bdExecute(conexion, qry)
            End If

            If r = True Then
                GridView2.SetRowCellValue(GridView2.FocusedRowHandle, "Resurtible", False)
                qry = "update productos set resurtible = 0 where idproducto = " & id
                bdBase.bdExecute(conexion, qry)
            End If
        End If

        If c = 10 Then
            If d = True Then
                GridView2.SetRowCellValue(GridView2.FocusedRowHandle, "Depurado", False)
                qry = "update productos set depurado = 0 where idproducto = " & id
                bdBase.bdExecute(conexion, qry)
            End If

            If d = False Then
                GridView2.SetRowCellValue(GridView2.FocusedRowHandle, "Depurado", True)
                qry = "update productos set depurado = 1 where idproducto = " & id
                bdBase.bdExecute(conexion, qry)
            End If
        End If
    End Sub
End Class