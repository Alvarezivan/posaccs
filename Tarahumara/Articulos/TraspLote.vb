﻿
Imports System.Data

Imports System.Data.SqlClient

Imports System.Drawing.Image

Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns

Public Class Trasplote

    Private Sub Trasplotes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Traspaso por Lote] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre


        RegistraAcceso(conexion, "TraspasoporLote [TraspLote.frm]")

        Splash(False)


    End Sub
End Class