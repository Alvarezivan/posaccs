﻿Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class Etiquetas

    Private Sub Etiquetas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If busq Then

            tbexis.Text = can
            tbcodigo.Text = cod
            tbcompra.Text = 0
            tbimprimir.Text = can
            tbimprimir.Focus()
        Else
            tbexis.Text = 0
            tbcodigo.Text = cod
            tbcompra.Text = can
            tbimprimir.Text = can + Val(tbexis.Text)
            tbimprimir.Focus()
        End If

        busq = False
        tbimprimir.Focus()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

            Windows.Forms.Cursor.Current = Cursors.WaitCursor

        Dim idProducto As Integer
        Dim descri As String
        Dim precio As Decimal

        Dim qry As String

        qry = " Select p.idproducto, RTRIM(LTRIM(sf.SubFamilia)) + ' ' + RTRIM(LTRIM(p.descripcion)) + ' ' + RTRIM(LTRIM( m.Marca)) as Descrip, p.Precio from productos p inner join subfamilias sf ON p.idsubfamilia = sf.idsubfamilia " & _
             " inner join marcas m ON p.idmarca = m.idmarca inner join skus s on s.idproducto = p.idproducto  where s.sku =  '" & tbcodigo.Text.Trim & "' "

        Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr1 Is Nothing Then
            If dr1.HasRows Then
                dr1.Read()
                If Not IsDBNull(dr1("idproducto")) Then idProducto = dr1("idproducto").ToString.Trim
                If Not IsDBNull(dr1("Descrip")) Then descri = dr1("Descrip").ToString.Trim
                If Not IsDBNull(dr1("Precio")) Then precio = dr1("Precio").ToString.Trim
            End If
        End If

        Dim ds As New DataSet
        Dim dt As New DataTable
        dt.Columns.Add("ID", GetType(Integer))
        dt.Columns("ID").AutoIncrement = True
        dt.Columns("ID").AutoIncrementSeed = 1
        dt.Columns.Add("id", GetType(String))
        dt.Columns.Add("DESCRI", GetType(String))
        dt.Columns.Add("Precio", GetType(Decimal))
        dt.Columns.Add("barcodeR", GetType(String))
        dt.PrimaryKey = New DataColumn() {dt.Columns("ID")}
        ds.Tables.Add(dt)


        Dim cuantas As Integer

        cuantas = tbimprimir.Text.Trim

        For i As Integer = 1 To cuantas

            Dim row As DataRow = dt.NewRow

            row("id") = idProducto
            Dim descripcion As String
            If descri.Length > 37 Then
                descripcion = descri
                descripcion = descripcion.Substring(0, 37)
                'row("DESCRI") = renglon.Cells("Precio").Value.ToString.Trim()
            Else
                '"0123456789012345678901234567890123456" '
                row("DESCRI") = descri
            End If
            row("Precio") = FormatNumber(precio, 2)
            row("barcodeR") = getBarcode(idProducto)

            dt.Rows.Add(row)
        Next
        ReportViewer1.Reset()
        Dim reporte As String = String.Empty
        Dim params As ReportParameter() = New ReportParameter(0) {}


        reporte = "c:\elc.rdlc"
        params = New ReportParameter(1) {}
        params(0) = New ReportParameter("id", 1)
        params(1) = New ReportParameter("logotipo", "file:///" & "C:\Saz\logos\logo.jpg")

        ' Crear reporte
        ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
        ReportViewer1.LocalReport.ReportPath = reporte
        'ReportViewer1.RefreshReport()

        ReportViewer1.LocalReport.EnableExternalImages = True

        ReportViewer1.LocalReport.SetParameters(params)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", ds.Tables(0)))
        ReportViewer1.DocumentMapCollapsed = True
        ReportViewer1.RefreshReport()
    End Sub
End Class