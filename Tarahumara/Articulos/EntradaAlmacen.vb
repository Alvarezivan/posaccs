﻿
Imports System.Data

Imports System.Data.SqlClient

Imports System.Drawing.Image

Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns

Public Class EntradaAlmacen


    Private Sub EntradaAlmacen_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load


        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Entrada de Almacen] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre


        RegistraAcceso(conexion, "Entrada Almacen [EntradaAlmacen.frm]")

        lbentrada.Text = ""
        lbfecha.Text = ""



        Splash(False)

    End Sub

    Private Sub llenacombos()
        Splash(True)
        Dim qry As String = "SELECT numero, nombre from tiendas where borrada=0 ORDER BY nombre"
        LlenaCombobox(cbtienda, qry, "nombre", "numero", conexion)
      


        Splash(False)


    End Sub


End Class