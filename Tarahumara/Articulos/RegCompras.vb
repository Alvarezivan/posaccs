﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing

Imports System.IO
Imports System.Drawing.Drawing2D

Public Class RegCompras
    Dim subtotal As Decimal
    Dim iva1 As Decimal
    Dim razon As Integer
    Dim prefactura As Boolean = False
    Dim factura As Boolean
    Dim idf As Integer = -1
    Dim lleno As Boolean = False

    Private Sub RegistroCompras_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Registro de Compras] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        llenacombos()
        FillCombobox("nombre", My.Settings.TiendaActual, cbtienda, "Tiendas", "numero")
        ObtieneNumeroRazon(My.Settings.TiendaActual)
        FillCombobox("nombre", regresarazon, cbrazon, "Razones", "id")
        FillCombobox("nombre", oLogin.pUserId, cbusuario, "empleado", "numero")
        cbproved.SelectedIndex = 0
        tbsubtotal.Properties.ReadOnly = True
        tbiva.Properties.ReadOnly = True
        tbdesc.Properties.ReadOnly = True
        tbflete.Properties.ReadOnly = True
        tbtotal.Properties.ReadOnly = True
        dtfactura.Value = Now.Date
        dtllegada.Value = Now.Date


        Splash(False)
    End Sub

    Private Sub SimpleButton1_Click_1(sender As System.Object, e As System.EventArgs)
        gc1.Visible = True
    End Sub


    Private Sub llenacombos()

        Splash(True)

        Dim qry As String = "SELECT numero, RazonSocial from proveed where activo=1 ORDER BY RazonSocial"
        LlenaCombobox(cbproved, qry, "RazonSocial", "numero", conexion)
        qry = "SELECT id, Nombre from razones ORDER BY nombre"
        LlenaCombobox(cbrazon, qry, "nombre", "id", conexion)
        qry = "SELECT numero, nombre from Tiendas where borrada = 0 ORDER BY nombre"
        LlenaCombobox(cbtienda, qry, "Nombre", "Numero", conexion)
        qry = "SELECT numero, nombre from empleado where borrado = 0 ORDER BY nombre"
        LlenaCombobox(cbusuario, qry, "nombre", "numero", conexion)


        Splash(False)

    End Sub



    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs)

        bguardar.Enabled = True
        bpreguardar.Enabled = True
        gc1.Visible = True
    End Sub

    Private Function ValidaCombos() As Boolean
        Dim regresa As Boolean = False
        Dim oInfo As cInfoCombo


        oInfo = CType(Me.cbproved.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            MsgBox("No ha seleccionado al proveedor", MsgBoxStyle.Information)
            Exit Function
        End If
        oInfo = CType(Me.cbrazon.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            MsgBox("No ha seleccionado la Razon Social", MsgBoxStyle.Information)
            Exit Function
        End If

        oInfo = CType(Me.cbtienda.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            MsgBox("No ha seleccionado la Tienda", MsgBoxStyle.Information)
            Exit Function
        End If
        Return regresa
    End Function


    Private Sub SimpleButton9_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton9.Click

        Dim idfg As Integer = -1
        Dim qry As String
        qry = "Select id from facturas where factura = '" & tbfactura.Text.Trim & "' and idproveedor = '" & CType(cbproved.SelectedItem, cInfoCombo).ID & "'"
        Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr1 Is Nothing Then
            If dr1.HasRows Then
                dr1.Read()
                If Not IsDBNull(dr1("id")) Then idfg = dr1("id").ToString.Trim
            End If
        End If
        dr1.Close()
        If idfg <> -1 Then
            MsgBox("La factura ya existe", MsgBoxStyle.Information)
            Exit Sub
        Else
            Dim regresa As Boolean = True
            If cbusuario.SelectedIndex = -1 Then
                MsgBox("No hay usuario seleccionado", MsgBoxStyle.Information)
                cbusuario.Focus()
            Else
                qry = "Select password from empleado where numero = '" & CType(cbusuario.SelectedItem, cInfoCombo).ID & "'"

                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

                If Not dr Is Nothing Then

                    If dr.HasRows Then
                        dr.Read()
                        If Not IsDBNull(dr("password")) Then lbpass.Text = dr("password").ToString.Trim
                    End If

                End If


                dr.Close()

                If tbpass.Text.Trim.ToUpper <> lbpass.Text.Trim Then

                    MsgBox("La contraseña es incorrecta", MsgBoxStyle.Information)
                    tbpass.Text = ""
                End If
                If tbpass.Text.Trim.ToUpper = lbpass.Text.Trim Then


                    If regresa = True Then
                        If tbfactura.Text = "" Then
                            MsgBox("No ha ingresado el Numero de Factura", MsgBoxStyle.Information)

                            tbpass.Text = ""
                            tbfactura.Focus()

                        Else
                            bguardar.Enabled = True
                            bpreguardar.Enabled = True
                            SimpleButton9.Enabled = False
                            gc1.Enabled = True
                            chkreingreso.Enabled = True
                            cbproved.Properties.ReadOnly = True
                            tbfactura.Properties.ReadOnly = True
                            dtfactura.Enabled = False
                            dtllegada.Enabled = False
                            cbrazon.Properties.ReadOnly = True
                            cbtienda.Properties.ReadOnly = True
                            cbusuario.Properties.ReadOnly = True
                            tbpass.Properties.ReadOnly = True
                            tbcodigo.Focus()

                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub SimpleButton8_Click(sender As System.Object, e As System.EventArgs) Handles bsalir.Click

        Me.Close()

    End Sub

    Private Sub TextEdit7_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles tbcodigo.LostFocus
        If tbcodigo.Text <> "" Then
            Dim qry As String

            qry = "Select  skus.idproducto  from skus where sku = '" & tbcodigo.Text.Trim & "'"
            Try
                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not dr Is Nothing Then

                    If dr.HasRows Then
                        dr.Read()
                        If Not IsDBNull(dr("idproducto")) Then producto = dr("idproducto").ToString.Trim
                        existe = True
                        Dim myForm2 As New Productos
                        myForm2.tbcodigo.Text = Me.tbcodigo.Text
                        tbcantidad.Focus()
                        myForm2.Show()

                        'tbcosto.Text = FormatCurrency(tbcosto.Text) 

                    Else
                        Dim myForm2 As New Productos
                        myForm2.tbcodigo.Text = Me.tbcodigo.Text
                        tbcantidad.Focus()
                        myForm2.Show()
                    End If

                End If
                dr.Close()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles bingresar.Click

        If tbcantidad.Text = "" Then
            MsgBox("No a determinado una cantidad", MsgBoxStyle.Information)
            tbcantidad.Focus()
        ElseIf tbcodigo.Text = "" Then

            MsgBox("No hay codigo asignado", MsgBoxStyle.Information)
            tbcodigo.Focus()
        ElseIf Val(tbcantidad.Text) <= 0 Then

            MsgBox("La cantidad no puede ser 0 o menor", MsgBoxStyle.Information)
            tbcantidad.Focus()
        Else
            Dim qry As String = "Select  skus.idproducto  from skus where sku = '" & tbcodigo.Text.Trim & "'"
            Try
                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not dr Is Nothing Then

                    If dr.HasRows Then
                        dr.Read()
                        If Not IsDBNull(dr("idproducto")) Then producto = dr("idproducto").ToString.Trim
                    End If
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            cargaproducto(producto)
            totales()
            numfila()
            tbcodigo.Text = ""
            tbcantidad.Text = ""
            tbcodigo.Focus()
            etiquetaauto()
        End If

    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        Dim r As Integer = dgvcompra.RowCount - 1

        If dgvcompra.CurrentRow.Index = r Then

            MsgBox("No puede borrar esta fila", MsgBoxStyle.Information)
        Else
            dgvcompra.Rows.Remove(dgvcompra.CurrentRow)
            totales()
            numfila()
        End If
    End Sub

    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkreingreso.CheckedChanged
        If chkreingreso.Checked Then
            tbtotal.Text = subtotal
            tbiva.Text = ""
        Else
            tbtotal.Text = subtotal + iva1
            tbiva.Text = iva1
        End If


    End Sub

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        FuncKeysModule(e.KeyCode)
    End Sub

    Public Sub FuncKeysModule(ByVal value As Keys)
        Select Case value
            Case Keys.Enter
                tbcantidad.Focus()
                Exit Select
        End Select
    End Sub

    Private Sub SimpleButton6_Click(sender As System.Object, e As System.EventArgs) Handles bpreguardar.Click
        VerificarPre()

Dim qry As String
        If prefactura = True Then
            qry = "delete prefacturas where id = '" & idf & "'"
            bdBase.bdExecute(conexion, qry)
            qry = "delete prefacturadetalle where idfactura = '" & idf & "'"
            bdBase.bdExecute(conexion, qry)
        End If
        factura = False
        Guardafactura()
    End Sub

    Private Sub Guardadetalle()

        Dim qry As String
        Dim query As String
        If factura = False Then
            qry = " Select id from prefacturas where factura = '" & tbfactura.Text.Trim & "' and idproveedor = '" & CType(cbproved.SelectedItem, cInfoCombo).ID & "'"
        Else
            qry = " Select id from facturas where factura = '" & tbfactura.Text.Trim & "' and idproveedor = '" & CType(cbproved.SelectedItem, cInfoCombo).ID & "'"
        End If
        Dim idf As Integer = -1
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr("id")) Then idf = dr("id").ToString.Trim
            End If

        End If
        dr.Close()


        Dim i As Integer
        Dim y As Integer = dgvcompra.Rows.Count - 1
        If y > 0 Then
            For i = 0 To y - 1
                Dim idproducto As Integer = dgvcompra.Item(0, i).Value.ToString.Trim
                Dim cantidad As Integer = dgvcompra.Item(5, i).Value
                Dim costo As Decimal = dgvcompra.Item(6, i).Value
                Dim iva As Integer = dgvcompra.Item(9, i).Value
                If factura = False Then
                    query = "insert into Prefacturadetalle (factura,idproveedor,fecha,idrazon," & _
                    "idtienda,idproducto,cantidad,costo,iva,id,llave,idfactura) values ('" & tbfactura.Text.Trim & "','" & _
                 CType(cbproved.SelectedItem, cInfoCombo).ID & "','" & Format(dtfactura.Value, "yyyyMMdd") & _
                 "','" & CType(cbrazon.SelectedItem, cInfoCombo).ID & "','" & CType(cbtienda.SelectedItem, cInfoCombo).ID & "','" & _
                  idproducto & "','" & cantidad & "','" & costo & "','" & iva & "'," & _
                 "(select isnull(max(isnull(id,0)),0) + 1 from prefacturadetalle), newid()," & idf & ")"

                Else
                    GuardaExistencias(idproducto, cantidad, CType(cbtienda.SelectedItem, cInfoCombo).ID)
                    query = "insert into facturadetalle (factura,idproveedor,fecha,idrazon," & _
                    "idtienda,idproducto,cantidad,costo,iva,id,llave,idfactura) values ('" & tbfactura.Text.Trim & "','" & _
                 CType(cbproved.SelectedItem, cInfoCombo).ID & "','" & Format(dtfactura.Value, "yyyyMMdd") & _
                 "','" & CType(cbrazon.SelectedItem, cInfoCombo).ID & "','" & CType(cbtienda.SelectedItem, cInfoCombo).ID & "','" & _
                  idproducto & "','" & cantidad & "','" & costo & "','" & iva & "'," & _
                 "(select isnull(max(isnull(id,0)),0) + 1 from facturadetalle), newid()," & idf & ")"
                End If
                bdBase.bdExecute(conexion, query)
            Next
        End If

    End Sub

    Private Sub tbfactura_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles tbfactura.LostFocus
        VerificarPre()
        If idf <> -1 And lleno = False Then
            LlenaPre(idf)
            numfila()
        End If
    End Sub

    Private Sub LlenaPre(ByVal idfactura)
        Dim id As Integer
        Dim qry As String = " Select idproducto from prefacturadetalle where idfactura = '" & idfactura & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                While dr.Read()
                    If Not IsDBNull(dr("idproducto")) Then id = dr("idproducto").ToString.Trim
                    cargaproductopre(id)
                End While
            End If
        End If
        dr.Close()
        totales()
        lleno = True
    End Sub

    Private Sub cargaproducto(ByVal idproducto)
        Dim qry As String

        qry = " Select p.idproducto, p.Codigo, f.Familia, sf.SubFamilia, m.Marca, " & _
            "p.Costo2, p.Iva, p.descripcion from productos p inner join familias f " & _
            "ON p.idfamilia = f.idfamilia inner join subfamilias sf ON p.idsubfamilia = sf.idsubfamilia " & _
             "inner join marcas m ON p.idmarca = m.idmarca where p.idproducto = '" & idproducto & "' "

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                dgvcompra.Rows.Add(dr("idproducto").ToString.Trim, dr("codigo").ToString.Trim, dr("subfamilia").ToString.Trim, _
                                       dr("marca").ToString.Trim, dr("descripcion").ToString.Trim, tbcantidad.Text.Trim, dr("costo2").ToString.Trim, Val(dr("costo2").ToString.Trim) * Val(tbcantidad.Text.Trim), (Val(dr("costo2").ToString.Trim) * Val(tbcantidad.Text.Trim)) * (Val(dr("iva").ToString.Trim) / 100), dr("iva").ToString.Trim)
            End If

        End If
        dr.Close()
    End Sub

    Private Sub totales()

        Dim suma As Decimal = 0.0
        For Each row As DataGridViewRow In dgvcompra.Rows
            suma = suma + CDec(row.Cells("costot").Value)
        Next

        subtotal = suma
        tbsubtotal.Text = CStr(suma)

        Dim suma1 As Decimal = 0.0
        For Each row As DataGridViewRow In dgvcompra.Rows
            suma1 = suma1 + CDec(row.Cells("iva").Value)
        Next

        iva1 = suma1
        tbiva.Text = CStr(suma1)

        If chkreingreso.Checked Then
            tbtotal.Text = Val(CStr(suma))
        Else
            tbtotal.Text = Val(CStr(suma)) + Val(CStr(suma1))
        End If
    End Sub

    Private Sub cargaproductopre(ByVal idproducto)
        Dim qry As String

        qry = " Select pfd.idproducto,pfd.cantidad, p.Codigo, p.descripcion, sf.SubFamilia, m.Marca, " & _
            "pfd.Costo, pfd.Iva from prefacturadetalle pfd inner join productos p on pfd.idproducto = p.idproducto inner join familias f " & _
            "ON p.idfamilia = f.idfamilia inner join subfamilias sf ON p.idsubfamilia = sf.idsubfamilia " & _
             "inner join marcas m ON p.idmarca = m.idmarca  where pfd.idproducto = '" & idproducto & "'"

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                dgvcompra.Rows.Add(dr("idproducto").ToString.Trim, dr("codigo").ToString.Trim, dr("subfamilia").ToString.Trim, dr("marca").ToString.Trim, dr("descripcion").ToString.Trim, _
                                       dr("cantidad").ToString.Trim, dr("costo").ToString.Trim, Val(dr("costo").ToString.Trim) * Val(dr("cantidad").ToString.Trim), (Val(dr("costo").ToString.Trim) * Val(dr("cantidad").ToString.Trim)) * (Val(dr("iva").ToString.Trim) / 100), dr("iva").ToString.Trim)
            End If

        End If
        dr.Close()
    End Sub
    Private Sub Guardafactura()
        Dim qry As String
        Dim reingreso As Integer
        Dim plazo As Integer = 0
        If chkreingreso.Checked Then
            reingreso = 1
        Else
            reingreso = 0
        End If
        qry = " Select plazodias from proveed where numero = " & CType(cbproved.SelectedItem, cInfoCombo).ID
        Try
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    If Not IsDBNull(dr("PLAZODIAS")) Then plazo = dr("PLAZODIAS").ToString.Trim
                End If

            End If
            dr.Close()

            If factura = False Then
                qry = "insert into Prefacturas (factura,idproveedor,fechaingreso,fechafactura,fechallegada,idrazon," & _
                    "idtienda,idusuario,flete,Descuento,reingreso,plazo,vence,status,id,llave) values ('" & tbfactura.Text.Trim & "','" & _
                 CType(cbproved.SelectedItem, cInfoCombo).ID & "', getdate(), '" & Format(dtfactura.Value, "yyyyMMdd") & "','" & Format(dtllegada.Value, "yyyyMMdd") & _
                 "','" & CType(cbrazon.SelectedItem, cInfoCombo).ID & "','" & CType(cbtienda.SelectedItem, cInfoCombo).ID & "','" & CType(cbusuario.SelectedItem, cInfoCombo).ID & _
                 "','" & tbflete.Text.Trim & "','" & tbdesc.Text.Trim & "','" & reingreso & "','" & plazo & "','" & Format(dtllegada.Value, "yyyyMMdd") + plazo & "', 0," & _
                 "(select isnull(max(isnull(id,0)),0) + 1 from prefacturas), newid())"
            Else
                qry = "insert into facturas (factura,idproveedor,fechaingreso,fechafactura,fechallegada,idrazon," & _
                    "idtienda,idusuario,flete,Descuento,reingreso,plazo,vence,status,id,llave) values ('" & tbfactura.Text.Trim & "','" & _
                 CType(cbproved.SelectedItem, cInfoCombo).ID & "', getdate(), '" & Format(dtfactura.Value, "yyyyMMdd") & "','" & Format(dtllegada.Value, "yyyyMMdd") & _
                 "','" & CType(cbrazon.SelectedItem, cInfoCombo).ID & "','" & CType(cbtienda.SelectedItem, cInfoCombo).ID & "','" & CType(cbusuario.SelectedItem, cInfoCombo).ID & _
                 "','" & tbflete.Text.Trim & "','" & tbdesc.Text.Trim & "','" & reingreso & "','" & plazo & "','" & Format(dtllegada.Value, "yyyyMMdd") + plazo & "', 0," & _
                 "(select isnull(max(isnull(id,0)),0) + 1 from prefacturas), newid())"
            End If

            bdBase.bdExecute(conexion, qry)
            Guardadetalle()
            MsgBox("Guardado con Exito", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try


    End Sub

    Private Sub bguardar_Click(sender As System.Object, e As System.EventArgs) Handles bguardar.Click

        Dim qry As String
        If prefactura = True Then
            qry = "delete prefacturas where id = '" & idf & "'"
            bdBase.bdExecute(conexion, qry)
            qry = "delete prefacturadetalle where idfactura = '" & idf & "'"
            bdBase.bdExecute(conexion, qry)
        End If
        factura = True
        Guardafactura()
        prefactura = False
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvcompra.CellContentClick
        Dim r As Integer = dgvcompra.RowCount - 1
        If dgvcompra.CurrentRow.Index = r Then
            MsgBox("No hay producto en esta fila", MsgBoxStyle.Information)
        Else
            If e.ColumnIndex = 10 Then
                Dim row As Integer = e.RowIndex
                idcod = dgvcompra.Item(0, row).Value.ToString.Trim()
                cod = dgvcompra.Item(1, row).Value.ToString.Trim()
                can = dgvcompra.Item(5, row).Value.ToString.Trim()
                Etiquetas.Show()
            End If
        End If
    End Sub

    Private Sub numfila()
        'Mostrar el número de filas de un DataGridView..
        If dgvcompra.Rows.Count > 0 Then
            For r As Integer = 0 To dgvcompra.Rows.Count - 1
                Me.dgvcompra.Rows(r).HeaderCell.Value = (r + 1).ToString()
            Next
        End If
        dgvcompra.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders)
    End Sub

    Private Sub GuardaExistencias(ByVal Id As Integer, ByVal ca As Integer, ByVal ti As Integer)
        Dim qry As String
        qry = "Select  cantidad from  existen where IdProducto ='" & Id & "' and tienda='" & ti & "'"
        Dim edr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If edr.HasRows Then
            qry = "update existen set cantidad=(ISNULL(cantidad,0)+" & ca & ") where tienda='" & ti & "' and IdProducto ='" & Id & "'"
        Else
            qry = "insert into existen (IdProducto,TIENDA,cantidad,transito,llave)values('" & Id & "','" & ti & "','" & ca & "',0, newid())"
        End If

        bdBase.bdExecute(conexion, qry)
    End Sub

    Private Sub VerificarPre()
        Dim qry As String = " Select id from prefacturas where factura = '" & tbfactura.Text.Trim & "' and idproveedor = '" & CType(cbproved.SelectedItem, cInfoCombo).ID & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr("id")) Then idf = dr("id").ToString.Trim
                prefactura = True
            End If
        End If
        dr.Close()
    End Sub

    Private Sub etiquetaauto()

        Dim r As Integer = dgvcompra.RowCount - 2
        
        Dim row As Integer = r
                idcod = dgvcompra.Item(0, row).Value.ToString.Trim()
                cod = dgvcompra.Item(1, row).Value.ToString.Trim()
                can = dgvcompra.Item(5, row).Value.ToString.Trim()
                Etiquetas.Show()
    End Sub

    Private Sub SimpleButton18_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton18.Click
        Dim rptpro As New RptProductosPV
        If rptpro.ShowDialog() = DialogResult.OK Then
            tbcodigo.Text = CodigoRPTPV
            tbcodigo.Focus()
        End If
    End Sub
End Class