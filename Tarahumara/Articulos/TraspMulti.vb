﻿
Imports System.Data

Imports System.Data.SqlClient

Imports System.Drawing.Image

Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns

Public Class Traspmulti

    Private Sub Traspmulti_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Traspaso Multi Tienda] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre


        RegistraAcceso(conexion, "Traspasomultitienda [TraspMulti.frm]")

        Splash(False)

    End Sub
End Class