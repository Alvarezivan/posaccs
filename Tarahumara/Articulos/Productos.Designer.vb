﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Productos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Productos))
        Me.tbcodigo = New DevExpress.XtraEditors.TextEdit()
        Me.tbcosto = New DevExpress.XtraEditors.TextEdit()
        Me.cbsubfamilia = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbmarca = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbprovedor = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbfamilia = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbtemporada = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.chresur = New DevExpress.XtraEditors.CheckEdit()
        Me.chbasico = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.Sku = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvsku = New System.Windows.Forms.DataGridView()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.bguardar = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.bactcatalogos = New DevExpress.XtraEditors.SimpleButton()
        Me.tbdescrip = New DevExpress.XtraEditors.TextEdit()
        Me.tbobser = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.bimagen = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcosto2 = New DevExpress.XtraEditors.TextEdit()
        Me.tbprecio = New DevExpress.XtraEditors.TextEdit()
        Me.tbprecio2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.tbresultado = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.tbiva = New DevExpress.XtraEditors.TextEdit()
        Me.tbdesc2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.tbdesc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.lbactual2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbpactual = New DevExpress.XtraEditors.LabelControl()
        Me.tbdlls = New DevExpress.XtraEditors.TextEdit()
        Me.chkdlls = New System.Windows.Forms.CheckBox()
        Me.tbfactor = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.cbclasificacion = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.tbprocedencia = New DevExpress.XtraEditors.TextEdit()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcosto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbsubfamilia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbmarca.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbprovedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbfamilia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbtemporada.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chresur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chbasico.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvsku, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbdescrip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbobser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcosto2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbprecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbprecio2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbresultado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbiva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbdesc2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.tbdlls.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbfactor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbclasificacion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbprocedencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbcodigo
        '
        Me.tbcodigo.Location = New System.Drawing.Point(6, 29)
        Me.tbcodigo.Name = "tbcodigo"
        Me.tbcodigo.Size = New System.Drawing.Size(100, 20)
        Me.tbcodigo.TabIndex = 0
        '
        'tbcosto
        '
        Me.tbcosto.Location = New System.Drawing.Point(5, 71)
        Me.tbcosto.Name = "tbcosto"
        Me.tbcosto.Properties.Mask.EditMask = "c"
        Me.tbcosto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbcosto.Size = New System.Drawing.Size(83, 20)
        Me.tbcosto.TabIndex = 3
        '
        'cbsubfamilia
        '
        Me.cbsubfamilia.Location = New System.Drawing.Point(233, 29)
        Me.cbsubfamilia.Name = "cbsubfamilia"
        Me.cbsubfamilia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbsubfamilia.Size = New System.Drawing.Size(100, 20)
        Me.cbsubfamilia.TabIndex = 2
        '
        'cbmarca
        '
        Me.cbmarca.Location = New System.Drawing.Point(350, 29)
        Me.cbmarca.Name = "cbmarca"
        Me.cbmarca.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbmarca.Size = New System.Drawing.Size(100, 20)
        Me.cbmarca.TabIndex = 3
        '
        'cbprovedor
        '
        Me.cbprovedor.Location = New System.Drawing.Point(112, 100)
        Me.cbprovedor.Name = "cbprovedor"
        Me.cbprovedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbprovedor.Size = New System.Drawing.Size(147, 20)
        Me.cbprovedor.TabIndex = 6
        '
        'cbfamilia
        '
        Me.cbfamilia.Location = New System.Drawing.Point(120, 29)
        Me.cbfamilia.Name = "cbfamilia"
        Me.cbfamilia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbfamilia.Size = New System.Drawing.Size(100, 20)
        Me.cbfamilia.TabIndex = 1
        '
        'cbtemporada
        '
        Me.cbtemporada.Location = New System.Drawing.Point(465, 29)
        Me.cbtemporada.Name = "cbtemporada"
        Me.cbtemporada.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbtemporada.Size = New System.Drawing.Size(100, 20)
        Me.cbtemporada.TabIndex = 4
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(6, 10)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl1.TabIndex = 8
        Me.LabelControl1.Text = "Código"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(233, 10)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl2.TabIndex = 9
        Me.LabelControl2.Text = "Sub Familia"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(350, 10)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Marca"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(112, 81)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(50, 13)
        Me.LabelControl4.TabIndex = 11
        Me.LabelControl4.Text = "Proveedor"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(121, 10)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Familia"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(465, 10)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl6.TabIndex = 13
        Me.LabelControl6.Text = "Temporada"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(7, 52)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl7.TabIndex = 14
        Me.LabelControl7.Text = "Costo 1"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(99, 52)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "Publico 1"
        '
        'chresur
        '
        Me.chresur.EditValue = True
        Me.chresur.Location = New System.Drawing.Point(348, 249)
        Me.chresur.Name = "chresur"
        Me.chresur.Properties.Caption = "Resurtible"
        Me.chresur.Size = New System.Drawing.Size(75, 19)
        Me.chresur.TabIndex = 12
        '
        'chbasico
        '
        Me.chbasico.Location = New System.Drawing.Point(271, 249)
        Me.chbasico.Name = "chbasico"
        Me.chbasico.Properties.Caption = "Basico"
        Me.chbasico.Size = New System.Drawing.Size(62, 19)
        Me.chbasico.TabIndex = 11
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(401, 406)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl9.TabIndex = 19
        Me.LabelControl9.Text = "Imagen"
        '
        'Sku
        '
        Me.Sku.HeaderText = "Sku"
        Me.Sku.Name = "Sku"
        '
        'dgvsku
        '
        Me.dgvsku.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvsku.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sku})
        Me.dgvsku.Location = New System.Drawing.Point(7, 250)
        Me.dgvsku.Name = "dgvsku"
        Me.dgvsku.Size = New System.Drawing.Size(252, 150)
        Me.dgvsku.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(7, 406)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl10.TabIndex = 21
        Me.LabelControl10.Text = "Códigos Asociados"
        '
        'bguardar
        '
        Me.bguardar.Location = New System.Drawing.Point(465, 359)
        Me.bguardar.Name = "bguardar"
        Me.bguardar.Size = New System.Drawing.Size(117, 66)
        Me.bguardar.TabIndex = 14
        Me.bguardar.Text = "&Guardar"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.LabelControl11.Location = New System.Drawing.Point(13, 52)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(55, 11)
        Me.LabelControl11.TabIndex = 22
        Me.LabelControl11.Text = "<F5> Buscar"
        '
        'bactcatalogos
        '
        Me.bactcatalogos.Location = New System.Drawing.Point(461, 271)
        Me.bactcatalogos.Name = "bactcatalogos"
        Me.bactcatalogos.Size = New System.Drawing.Size(117, 40)
        Me.bactcatalogos.TabIndex = 15
        Me.bactcatalogos.Text = "Actualizar Catalogos"
        '
        'tbdescrip
        '
        Me.tbdescrip.Location = New System.Drawing.Point(6, 147)
        Me.tbdescrip.Name = "tbdescrip"
        Me.tbdescrip.Size = New System.Drawing.Size(253, 20)
        Me.tbdescrip.TabIndex = 7
        '
        'tbobser
        '
        Me.tbobser.Location = New System.Drawing.Point(6, 194)
        Me.tbobser.Name = "tbobser"
        Me.tbobser.Size = New System.Drawing.Size(253, 20)
        Me.tbobser.TabIndex = 8
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(6, 128)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl12.TabIndex = 26
        Me.LabelControl12.Text = "Descripción"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(6, 175)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl13.TabIndex = 27
        Me.LabelControl13.Text = "Observaciones"
        '
        'bimagen
        '
        Me.bimagen.Location = New System.Drawing.Point(255, 406)
        Me.bimagen.Name = "bimagen"
        Me.bimagen.Size = New System.Drawing.Size(113, 23)
        Me.bimagen.TabIndex = 13
        Me.bimagen.Text = "Seleccionar imagen..."
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(7, 108)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl14.TabIndex = 30
        Me.LabelControl14.Text = "Costo 2"
        '
        'tbcosto2
        '
        Me.tbcosto2.Location = New System.Drawing.Point(7, 127)
        Me.tbcosto2.Name = "tbcosto2"
        Me.tbcosto2.Properties.Mask.EditMask = "c"
        Me.tbcosto2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbcosto2.Size = New System.Drawing.Size(83, 20)
        Me.tbcosto2.TabIndex = 4
        '
        'tbprecio
        '
        Me.tbprecio.Location = New System.Drawing.Point(99, 71)
        Me.tbprecio.Name = "tbprecio"
        Me.tbprecio.Properties.Mask.EditMask = "c"
        Me.tbprecio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbprecio.Size = New System.Drawing.Size(83, 20)
        Me.tbprecio.TabIndex = 5
        '
        'tbprecio2
        '
        Me.tbprecio2.Location = New System.Drawing.Point(99, 127)
        Me.tbprecio2.Name = "tbprecio2"
        Me.tbprecio2.Properties.Mask.EditMask = "c"
        Me.tbprecio2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbprecio2.Size = New System.Drawing.Size(83, 20)
        Me.tbprecio2.TabIndex = 6
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(99, 108)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl15.TabIndex = 33
        Me.LabelControl15.Text = "Publico 2"
        '
        'tbresultado
        '
        Me.tbresultado.Location = New System.Drawing.Point(607, 435)
        Me.tbresultado.Name = "tbresultado"
        Me.tbresultado.Size = New System.Drawing.Size(72, 20)
        Me.tbresultado.TabIndex = 34
        Me.tbresultado.Visible = False
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(264, 77)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl16.TabIndex = 35
        Me.LabelControl16.Text = "IVA (%)"
        '
        'tbiva
        '
        Me.tbiva.Location = New System.Drawing.Point(264, 96)
        Me.tbiva.Name = "tbiva"
        Me.tbiva.Properties.Mask.EditMask = "P"
        Me.tbiva.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbiva.Size = New System.Drawing.Size(39, 20)
        Me.tbiva.TabIndex = 9
        '
        'tbdesc2
        '
        Me.tbdesc2.Location = New System.Drawing.Point(192, 127)
        Me.tbdesc2.Name = "tbdesc2"
        Me.tbdesc2.Properties.Mask.EditMask = "P"
        Me.tbdesc2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbdesc2.Size = New System.Drawing.Size(60, 20)
        Me.tbdesc2.TabIndex = 8
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(192, 108)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl17.TabIndex = 39
        Me.LabelControl17.Text = "Desc 2 (%)"
        '
        'tbdesc
        '
        Me.tbdesc.Location = New System.Drawing.Point(192, 71)
        Me.tbdesc.Name = "tbdesc"
        Me.tbdesc.Properties.Mask.EditMask = "P"
        Me.tbdesc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbdesc.Size = New System.Drawing.Size(60, 20)
        Me.tbdesc.TabIndex = 7
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(192, 52)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl18.TabIndex = 38
        Me.LabelControl18.Text = "Desc 1 (%)"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.lbactual2)
        Me.GroupControl1.Controls.Add(Me.lbpactual)
        Me.GroupControl1.Controls.Add(Me.tbdlls)
        Me.GroupControl1.Controls.Add(Me.chkdlls)
        Me.GroupControl1.Controls.Add(Me.tbfactor)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.tbdesc2)
        Me.GroupControl1.Controls.Add(Me.tbcosto)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.tbdesc)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.tbcosto2)
        Me.GroupControl1.Controls.Add(Me.tbiva)
        Me.GroupControl1.Controls.Add(Me.tbprecio)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.tbprecio2)
        Me.GroupControl1.Location = New System.Drawing.Point(273, 74)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(309, 165)
        Me.GroupControl1.TabIndex = 9
        Me.GroupControl1.Text = "Precios"
        '
        'lbactual2
        '
        Me.lbactual2.Location = New System.Drawing.Point(146, 148)
        Me.lbactual2.Name = "lbactual2"
        Me.lbactual2.Size = New System.Drawing.Size(35, 13)
        Me.lbactual2.TabIndex = 42
        Me.lbactual2.Text = "Pactual"
        '
        'lbpactual
        '
        Me.lbpactual.Location = New System.Drawing.Point(144, 93)
        Me.lbpactual.Name = "lbpactual"
        Me.lbpactual.Size = New System.Drawing.Size(35, 13)
        Me.lbpactual.TabIndex = 41
        Me.lbpactual.Text = "Pactual"
        '
        'tbdlls
        '
        Me.tbdlls.Location = New System.Drawing.Point(209, 26)
        Me.tbdlls.Name = "tbdlls"
        Me.tbdlls.Properties.Mask.EditMask = "c"
        Me.tbdlls.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbdlls.Size = New System.Drawing.Size(69, 20)
        Me.tbdlls.TabIndex = 2
        Me.tbdlls.Visible = False
        '
        'chkdlls
        '
        Me.chkdlls.AutoSize = True
        Me.chkdlls.Location = New System.Drawing.Point(157, 29)
        Me.chkdlls.Name = "chkdlls"
        Me.chkdlls.Size = New System.Drawing.Size(46, 17)
        Me.chkdlls.TabIndex = 1
        Me.chkdlls.Text = "Dllrs"
        Me.chkdlls.UseVisualStyleBackColor = True
        '
        'tbfactor
        '
        Me.tbfactor.Location = New System.Drawing.Point(81, 26)
        Me.tbfactor.Name = "tbfactor"
        Me.tbfactor.Size = New System.Drawing.Size(69, 20)
        Me.tbfactor.TabIndex = 0
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(7, 29)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(63, 13)
        Me.LabelControl19.TabIndex = 40
        Me.LabelControl19.Text = "Factor Precio"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(6, 81)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl20.TabIndex = 44
        Me.LabelControl20.Text = "Clasificacion"
        '
        'cbclasificacion
        '
        Me.cbclasificacion.Location = New System.Drawing.Point(6, 100)
        Me.cbclasificacion.Name = "cbclasificacion"
        Me.cbclasificacion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbclasificacion.Size = New System.Drawing.Size(100, 20)
        Me.cbclasificacion.TabIndex = 5
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(273, 271)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(182, 129)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 18
        Me.PictureBox1.TabStop = False
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(444, 252)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl21.TabIndex = 45
        Me.LabelControl21.Text = "Procedencia"
        '
        'tbprocedencia
        '
        Me.tbprocedencia.Location = New System.Drawing.Point(508, 245)
        Me.tbprocedencia.Name = "tbprocedencia"
        Me.tbprocedencia.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbprocedencia.Size = New System.Drawing.Size(43, 20)
        Me.tbprocedencia.TabIndex = 46
        '
        'Productos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 443)
        Me.Controls.Add(Me.tbprocedencia)
        Me.Controls.Add(Me.LabelControl21)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.cbclasificacion)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.tbresultado)
        Me.Controls.Add(Me.bimagen)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.tbobser)
        Me.Controls.Add(Me.tbdescrip)
        Me.Controls.Add(Me.bactcatalogos)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.bguardar)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.dgvsku)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.chbasico)
        Me.Controls.Add(Me.chresur)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.cbtemporada)
        Me.Controls.Add(Me.cbfamilia)
        Me.Controls.Add(Me.cbprovedor)
        Me.Controls.Add(Me.cbmarca)
        Me.Controls.Add(Me.cbsubfamilia)
        Me.Controls.Add(Me.tbcodigo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "Productos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcosto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbsubfamilia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbmarca.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbprovedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbfamilia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbtemporada.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chresur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chbasico.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvsku, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbdescrip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbobser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcosto2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbprecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbprecio2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbresultado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbiva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbdesc2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.tbdlls.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbfactor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbclasificacion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbprocedencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbcosto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbsubfamilia As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbmarca As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbprovedor As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbfamilia As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbtemporada As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chresur As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chbasico As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Sku As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvsku As System.Windows.Forms.DataGridView
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents bguardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents bactcatalogos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbdescrip As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbobser As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents bimagen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcosto2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbprecio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbprecio2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbresultado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbiva As DevExpress.XtraEditors.TextEdit
    Public WithEvents tbcodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbdesc2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbdesc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbclasificacion As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents tbfactor As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbdlls As DevExpress.XtraEditors.TextEdit
    Friend WithEvents chkdlls As System.Windows.Forms.CheckBox
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Public WithEvents tbprocedencia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbactual2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbpactual As DevExpress.XtraEditors.LabelControl
End Class
