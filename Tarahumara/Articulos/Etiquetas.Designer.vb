﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Etiquetas
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbcodigo = New System.Windows.Forms.TextBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbexis = New System.Windows.Forms.TextBox()
        Me.tbcompra = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbimprimir = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo"
        '
        'tbcodigo
        '
        Me.tbcodigo.Location = New System.Drawing.Point(17, 25)
        Me.tbcodigo.Name = "tbcodigo"
        Me.tbcodigo.ReadOnly = True
        Me.tbcodigo.Size = New System.Drawing.Size(91, 21)
        Me.tbcodigo.TabIndex = 1
        Me.tbcodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Right
        Me.ReportViewer1.Location = New System.Drawing.Point(128, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(363, 323)
        Me.ReportViewer1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 60)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Existencia"
        '
        'tbexis
        '
        Me.tbexis.Location = New System.Drawing.Point(17, 76)
        Me.tbexis.Name = "tbexis"
        Me.tbexis.ReadOnly = True
        Me.tbexis.Size = New System.Drawing.Size(91, 21)
        Me.tbexis.TabIndex = 4
        Me.tbexis.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbcompra
        '
        Me.tbcompra.Location = New System.Drawing.Point(17, 124)
        Me.tbcompra.Name = "tbcompra"
        Me.tbcompra.ReadOnly = True
        Me.tbcompra.Size = New System.Drawing.Size(91, 21)
        Me.tbcompra.TabIndex = 6
        Me.tbcompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 108)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Compra"
        '
        'tbimprimir
        '
        Me.tbimprimir.Location = New System.Drawing.Point(17, 196)
        Me.tbimprimir.Name = "tbimprimir"
        Me.tbimprimir.Size = New System.Drawing.Size(91, 21)
        Me.tbimprimir.TabIndex = 8
        Me.tbimprimir.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 180)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Total a Imprimir"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(17, 251)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(91, 60)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "&Generar Etiquetas"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Etiquetas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(491, 323)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tbimprimir)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tbcompra)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbexis)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Controls.Add(Me.tbcodigo)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Etiquetas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Etiquetas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbcodigo As System.Windows.Forms.TextBox
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbexis As System.Windows.Forms.TextBox
    Friend WithEvents tbcompra As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbimprimir As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
