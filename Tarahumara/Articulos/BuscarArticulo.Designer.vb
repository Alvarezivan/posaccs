﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BuscarArticulo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BuscarArticulo))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.tbbarcode = New DevExpress.XtraEditors.TextEdit()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbbarcode = New DevExpress.XtraEditors.LabelControl()
        Me.lbdescrip = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.lbobserva = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.lbprecio = New DevExpress.XtraEditors.LabelControl()
        Me.lbcosto = New DevExpress.XtraEditors.LabelControl()
        Me.lbprecio2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbcosto2 = New DevExpress.XtraEditors.LabelControl()
        Me.lbbasico = New DevExpress.XtraEditors.LabelControl()
        Me.lbresur = New DevExpress.XtraEditors.LabelControl()
        Me.lbiva = New DevExpress.XtraEditors.LabelControl()
        Me.lbfamilia = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lbsubfamilia = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.lbtemporada = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.lbmarca = New DevExpress.XtraEditors.LabelControl()
        Me.lbid = New DevExpress.XtraEditors.LabelControl()
        Me.lbproveed = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.bcopiar = New DevExpress.XtraEditors.SimpleButton()
        Me.lbclasificacion = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.tbbarcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(143, 42)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Barcode"
        '
        'tbbarcode
        '
        Me.tbbarcode.Location = New System.Drawing.Point(161, 24)
        Me.tbbarcode.Name = "tbbarcode"
        Me.tbbarcode.Properties.Appearance.BackColor = System.Drawing.Color.Gainsboro
        Me.tbbarcode.Properties.Appearance.Options.UseBackColor = True
        Me.tbbarcode.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
        Me.tbbarcode.Size = New System.Drawing.Size(170, 22)
        Me.tbbarcode.TabIndex = 1
        '
        'GridControl1
        '
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(10, 303)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(411, 202)
        Me.GridControl1.TabIndex = 2
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(344, 28)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 5
        Me.SimpleButton1.Text = "&Buscar"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 63)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Codigo:"
        '
        'lbbarcode
        '
        Me.lbbarcode.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbbarcode.Location = New System.Drawing.Point(55, 63)
        Me.lbbarcode.Name = "lbbarcode"
        Me.lbbarcode.Size = New System.Drawing.Size(120, 13)
        Me.lbbarcode.TabIndex = 7
        Me.lbbarcode.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'lbdescrip
        '
        Me.lbdescrip.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbdescrip.Location = New System.Drawing.Point(227, 110)
        Me.lbdescrip.Name = "lbdescrip"
        Me.lbdescrip.Size = New System.Drawing.Size(120, 13)
        Me.lbdescrip.TabIndex = 11
        Me.lbdescrip.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(227, 91)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl7.TabIndex = 10
        Me.LabelControl7.Text = "Descripción:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(236, 175)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(183, 110)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(10, 259)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl4.TabIndex = 13
        Me.LabelControl4.Text = "Básico:"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(78, 259)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(52, 13)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Resurtible:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(161, 259)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "IVA(%):"
        '
        'lbobserva
        '
        Me.lbobserva.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbobserva.Location = New System.Drawing.Point(227, 148)
        Me.lbobserva.Name = "lbobserva"
        Me.lbobserva.Size = New System.Drawing.Size(120, 13)
        Me.lbobserva.TabIndex = 17
        Me.lbobserva.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(227, 129)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(75, 13)
        Me.LabelControl10.TabIndex = 16
        Me.LabelControl10.Text = "Observaciones:"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(124, 216)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl11.TabIndex = 19
        Me.LabelControl11.Text = "Costo:"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(12, 216)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl12.TabIndex = 18
        Me.LabelControl12.Text = "Precio:"
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(124, 235)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl13.TabIndex = 21
        Me.LabelControl13.Text = "Costo2:"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(12, 235)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(39, 13)
        Me.LabelControl14.TabIndex = 20
        Me.LabelControl14.Text = "Precio2:"
        '
        'lbprecio
        '
        Me.lbprecio.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbprecio.Location = New System.Drawing.Point(57, 216)
        Me.lbprecio.Name = "lbprecio"
        Me.lbprecio.Size = New System.Drawing.Size(61, 13)
        Me.lbprecio.TabIndex = 22
        Me.lbprecio.Text = "$ xxxxxx.xx"
        '
        'lbcosto
        '
        Me.lbcosto.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbcosto.Location = New System.Drawing.Point(168, 216)
        Me.lbcosto.Name = "lbcosto"
        Me.lbcosto.Size = New System.Drawing.Size(61, 13)
        Me.lbcosto.TabIndex = 23
        Me.lbcosto.Text = "$ xxxxxx.xx"
        '
        'lbprecio2
        '
        Me.lbprecio2.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbprecio2.Location = New System.Drawing.Point(57, 235)
        Me.lbprecio2.Name = "lbprecio2"
        Me.lbprecio2.Size = New System.Drawing.Size(61, 13)
        Me.lbprecio2.TabIndex = 24
        Me.lbprecio2.Text = "$ xxxxxx.xx"
        '
        'lbcosto2
        '
        Me.lbcosto2.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbcosto2.Location = New System.Drawing.Point(168, 235)
        Me.lbcosto2.Name = "lbcosto2"
        Me.lbcosto2.Size = New System.Drawing.Size(61, 13)
        Me.lbcosto2.TabIndex = 25
        Me.lbcosto2.Text = "$ xxxxxx.xx"
        '
        'lbbasico
        '
        Me.lbbasico.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbbasico.Location = New System.Drawing.Point(50, 259)
        Me.lbbasico.Name = "lbbasico"
        Me.lbbasico.Size = New System.Drawing.Size(10, 13)
        Me.lbbasico.TabIndex = 26
        Me.lbbasico.Text = "SI"
        '
        'lbresur
        '
        Me.lbresur.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbresur.Location = New System.Drawing.Point(136, 259)
        Me.lbresur.Name = "lbresur"
        Me.lbresur.Size = New System.Drawing.Size(10, 13)
        Me.lbresur.TabIndex = 27
        Me.lbresur.Text = "SI"
        '
        'lbiva
        '
        Me.lbiva.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbiva.Location = New System.Drawing.Point(207, 259)
        Me.lbiva.Name = "lbiva"
        Me.lbiva.Size = New System.Drawing.Size(23, 13)
        Me.lbiva.TabIndex = 28
        Me.lbiva.Text = "xx%"
        '
        'lbfamilia
        '
        Me.lbfamilia.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbfamilia.Location = New System.Drawing.Point(83, 91)
        Me.lbfamilia.Name = "lbfamilia"
        Me.lbfamilia.Size = New System.Drawing.Size(120, 13)
        Me.lbfamilia.TabIndex = 30
        Me.lbfamilia.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(12, 91)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl6.TabIndex = 29
        Me.LabelControl6.Text = "Familia:"
        '
        'lbsubfamilia
        '
        Me.lbsubfamilia.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbsubfamilia.Location = New System.Drawing.Point(82, 110)
        Me.lbsubfamilia.Name = "lbsubfamilia"
        Me.lbsubfamilia.Size = New System.Drawing.Size(120, 13)
        Me.lbsubfamilia.TabIndex = 32
        Me.lbsubfamilia.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(12, 110)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl15.TabIndex = 31
        Me.LabelControl15.Text = "Sub Familia:"
        '
        'lbtemporada
        '
        Me.lbtemporada.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbtemporada.Location = New System.Drawing.Point(81, 146)
        Me.lbtemporada.Name = "lbtemporada"
        Me.lbtemporada.Size = New System.Drawing.Size(120, 13)
        Me.lbtemporada.TabIndex = 34
        Me.lbtemporada.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(12, 146)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(58, 13)
        Me.LabelControl17.TabIndex = 33
        Me.LabelControl17.Text = "Temporada:"
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(12, 127)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl18.TabIndex = 35
        Me.LabelControl18.Text = "Marca:"
        '
        'lbmarca
        '
        Me.lbmarca.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbmarca.Location = New System.Drawing.Point(81, 127)
        Me.lbmarca.Name = "lbmarca"
        Me.lbmarca.Size = New System.Drawing.Size(120, 13)
        Me.lbmarca.TabIndex = 36
        Me.lbmarca.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'lbid
        '
        Me.lbid.Location = New System.Drawing.Point(392, 80)
        Me.lbid.Name = "lbid"
        Me.lbid.Size = New System.Drawing.Size(0, 13)
        Me.lbid.TabIndex = 37
        Me.lbid.Visible = False
        '
        'lbproveed
        '
        Me.lbproveed.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbproveed.Location = New System.Drawing.Point(81, 184)
        Me.lbproveed.Name = "lbproveed"
        Me.lbproveed.Size = New System.Drawing.Size(120, 13)
        Me.lbproveed.TabIndex = 39
        Me.lbproveed.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(12, 184)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl9.TabIndex = 38
        Me.LabelControl9.Text = "Proveedor:"
        '
        'bcopiar
        '
        Me.bcopiar.Enabled = False
        Me.bcopiar.Location = New System.Drawing.Point(344, 63)
        Me.bcopiar.Name = "bcopiar"
        Me.bcopiar.Size = New System.Drawing.Size(75, 23)
        Me.bcopiar.TabIndex = 40
        Me.bcopiar.Text = "Copiar Info"
        '
        'lbclasificacion
        '
        Me.lbclasificacion.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbclasificacion.Location = New System.Drawing.Point(82, 165)
        Me.lbclasificacion.Name = "lbclasificacion"
        Me.lbclasificacion.Size = New System.Drawing.Size(120, 13)
        Me.lbclasificacion.TabIndex = 42
        Me.lbclasificacion.Text = "xxxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(12, 165)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(62, 13)
        Me.LabelControl16.TabIndex = 41
        Me.LabelControl16.Text = "Clasificacion:"
        '
        'BuscarArticulo
        '
        Me.AcceptButton = Me.SimpleButton1
        Me.Appearance.BackColor = System.Drawing.Color.White
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(431, 511)
        Me.Controls.Add(Me.lbclasificacion)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.bcopiar)
        Me.Controls.Add(Me.lbproveed)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.lbid)
        Me.Controls.Add(Me.lbmarca)
        Me.Controls.Add(Me.LabelControl18)
        Me.Controls.Add(Me.lbtemporada)
        Me.Controls.Add(Me.LabelControl17)
        Me.Controls.Add(Me.lbsubfamilia)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.lbfamilia)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.lbiva)
        Me.Controls.Add(Me.lbresur)
        Me.Controls.Add(Me.lbbasico)
        Me.Controls.Add(Me.lbcosto2)
        Me.Controls.Add(Me.lbprecio2)
        Me.Controls.Add(Me.lbcosto)
        Me.Controls.Add(Me.lbprecio)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.lbobserva)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lbdescrip)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.lbbarcode)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.tbbarcode)
        Me.Controls.Add(Me.LabelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "BuscarArticulo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Articulo"
        CType(Me.tbbarcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbbarcode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbbarcode As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbdescrip As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbobserva As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbprecio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbcosto As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbprecio2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbcosto2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbbasico As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbresur As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbiva As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbfamilia As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbsubfamilia As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbtemporada As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbmarca As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbid As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbproveed As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents bcopiar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbclasificacion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
End Class
