﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing

Imports System.IO




Public Class BuscarArticulo
    Private Sub BuscarArticulo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Buscar Articulo] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        RegistraAcceso(conexion, "Buscar [BuscarArticulos.frm]")
        limpia()

        Splash(False)

    End Sub


    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click

        Dim existe As String = Nothing
        Dim query As String

        query = " Select idProducto from skus where sku =  '" & tbbarcode.Text.Trim & "' "

        Dim dr3 As SqlDataReader = bdBase.bdDataReader(conexion, query)

        If Not dr3 Is Nothing Then
            If dr3.HasRows Then
                dr3.Read()
                If Not IsDBNull(dr3("idproducto")) Then lbid.Text = dr3("idproducto").ToString.Trim

            Else

                existe = MsgBox("Codigo no encontrado", MsgBoxStyle.Information)
                limpia()
            End If
        End If

        dr3.Close()

        If existe = Nothing Then
            buscar()
            formatos()
            bcopiar.Enabled = True
        End If

    End Sub
    Public Function Bytes2Image(ByVal bytes() As Byte) As Image
        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm
    End Function


    Private Function Bytes_Imagen(ByVal Imagen As Byte()) As Image
        Try
            'si hay imagen
            If Not Imagen Is Nothing Then
                'caturar array con memorystream hacia Bin
                Dim Bin As New MemoryStream(Imagen)
                'con el método FroStream de Image obtenemos imagen
                Dim Resultado As Image = Image.FromStream(Bin)
                'y la retornamos
                Return Resultado
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function buscar()
        Try
            Dim qry2 As String

            qry2 = "select imagen from imagenes where idarticulo = '" & lbid.Text.Trim & "' "


            Dim dsImg As SqlDataReader = bdBase.bdDataReader(conexion, qry2)
            If dsImg.HasRows Then
                dsImg.Read()
                Dim Imag As Byte()

                Imag = dsImg("imagen")
                Me.PictureBox1.Image = Bytes_Imagen(Imag)

            End If

            Dim qry As String

            qry = " Select p.Codigo, f.Familia, sf.SubFamilia, t.Temporada, m.Marca, prov.Nombre, " & _
                "p.Descripcion, p.Observaciones, p.Precio, p.Precio2,p.Costo, " & _
                "p.Costo2, p.Basico, p.Resurtible, p.Iva, cl.clasificacion from productos p inner join familias f " & _
                "ON p.idfamilia = f.idfamilia inner join subfamilias sf ON p.idsubfamilia = sf.idsubfamilia " & _
                 "inner join temporadas t ON p.idtemporada = t.idtemporada inner join marcas m ON " & _
                 "p.idmarca = m.idmarca inner join proveed prov on p.idproveedor = prov.numero inner join " & _
                 "clasificacion cl on p.idclasificacion = cl.idclasificacion  where p.idproducto =  '" & lbid.Text.Trim & "' "


            Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

            If Not dr1 Is Nothing Then
                If dr1.HasRows Then
                    dr1.Read()
                    If Not IsDBNull(dr1("Codigo")) Then lbbarcode.Text = dr1("Codigo").ToString.Trim
                    If Not IsDBNull(dr1("Familia")) Then lbfamilia.Text = dr1("Familia").ToString.Trim
                    If Not IsDBNull(dr1("Subfamilia")) Then lbsubfamilia.Text = dr1("Subfamilia").ToString.Trim
                    If Not IsDBNull(dr1("Temporada")) Then lbtemporada.Text = dr1("Temporada").ToString.Trim
                    If Not IsDBNull(dr1("Marca")) Then lbmarca.Text = dr1("Marca").ToString.Trim
                    If Not IsDBNull(dr1("Descripcion")) Then lbdescrip.Text = dr1("Descripcion").ToString.Trim
                    If Not IsDBNull(dr1("Observaciones")) Then lbobserva.Text = dr1("Observaciones").ToString.Trim
                    If Not IsDBNull(dr1("Precio")) Then lbprecio.Text = dr1("Precio").ToString.Trim
                    If Not IsDBNull(dr1("Precio2")) Then lbprecio2.Text = dr1("Precio2").ToString.Trim
                    If Not IsDBNull(dr1("Costo")) Then lbcosto.Text = dr1("Costo").ToString.Trim
                    If Not IsDBNull(dr1("Costo2")) Then lbcosto2.Text = dr1("Costo2").ToString.Trim
                    If Not IsDBNull(dr1("Basico")) Then lbbasico.Text = dr1("Basico").ToString.Trim
                    If Not IsDBNull(dr1("Resurtible")) Then lbresur.Text = dr1("Resurtible").ToString.Trim
                    If Not IsDBNull(dr1("Iva")) Then lbiva.Text = dr1("Iva").ToString.Trim
                    If Not IsDBNull(dr1("Nombre")) Then lbproveed.Text = dr1("Nombre").ToString.Trim
                    If Not IsDBNull(dr1("Clasificacion")) Then lbclasificacion.Text = dr1("Clasificacion").ToString.Trim

                    If lbbasico.Text = True Then
                        lbbasico.Text = "SI"
                    ElseIf lbbasico.Text = False Then
                        lbbasico.Text = "NO"
                    End If

                    If lbresur.Text = True Then
                        lbresur.Text = "SI"
                    ElseIf lbresur.Text = False Then
                        lbresur.Text = "NO"
                    End If

                End If
            End If
            dr1.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Function


    Private Function formatos()
        lbcosto.Text = FormatCurrency(lbcosto.Text)
        lbcosto2.Text = FormatCurrency(lbcosto2.Text)
        lbprecio.Text = FormatCurrency(lbprecio.Text)
        lbprecio2.Text = FormatCurrency(lbprecio2.Text)
    End Function

    Private Function limpia()

        lbbarcode.Text = ""
        lbfamilia.Text = ""
        lbsubfamilia.Text = ""
        lbmarca.Text = ""
        lbtemporada.Text = ""
        lbdescrip.Text = ""
        lbobserva.Text = ""
        lbbasico.Text = ""
        lbresur.Text = ""
        lbiva.Text = ""
        lbcosto.Text = ""
        lbcosto2.Text = ""
        lbprecio.Text = ""
        lbprecio2.Text = ""
        lbproveed.Text = ""
        lbclasificacion.Text = ""
        PictureBox1.Image = Nothing

    End Function

    Private Sub bcopiar_Click(sender As System.Object, e As System.EventArgs) Handles bcopiar.Click
        Try
            Dim qry As String

            qry = " Select idfamilia,idsubfamilia,idmarca,idtemporada,idproveedor,costo,costo2,precio,precio2,basico,resurtible,iva,idclasificacion from productos where idproducto =  '" & lbid.Text.Trim & "' "


            Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

            If Not dr1 Is Nothing Then
                If dr1.HasRows Then
                    dr1.Read()
                    If Not IsDBNull(dr1("idFamilia")) Then Familia = dr1("idFamilia").ToString.Trim
                    If Not IsDBNull(dr1("idSubfamilia")) Then subfamilia = dr1("idSubfamilia").ToString.Trim
                    If Not IsDBNull(dr1("idTemporada")) Then Temporada = dr1("idTemporada").ToString.Trim
                    If Not IsDBNull(dr1("idMarca")) Then Marca = dr1("idMarca").ToString.Trim
                    If Not IsDBNull(dr1("Precio")) Then p1 = dr1("Precio").ToString.Trim
                    If Not IsDBNull(dr1("Precio2")) Then p2 = dr1("Precio2").ToString.Trim
                    If Not IsDBNull(dr1("Costo")) Then c1 = dr1("Costo").ToString.Trim
                    If Not IsDBNull(dr1("Costo2")) Then c2 = dr1("Costo2").ToString.Trim
                    If Not IsDBNull(dr1("Basico")) Then basic = dr1("Basico")
                    If Not IsDBNull(dr1("Resurtible")) Then resur = dr1("Resurtible")
                    If Not IsDBNull(dr1("Iva")) Then Iva = dr1("Iva").ToString.Trim
                    If Not IsDBNull(dr1("idproveedor")) Then Proveedor = dr1("idproveedor").ToString.Trim
                    If Not IsDBNull(dr1("idclasificacion")) Then clasificacion = dr1("idclasificacion").ToString.Trim
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        trasp = True
        System.Windows.Forms.Application.OpenForms.Item("Productos").Close()
        Productos.Show()
        Me.Close()

    End Sub
End Class