﻿
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel

Public Class proveedorescat


#Region "Inicializaciones"
    Dim regresa As Boolean = False
    Dim hayfoto As Boolean = False
    Dim valida As Boolean = False
    Dim init As Boolean = False
    Dim unid As String
    Dim nomprove As String = ""
    Dim bNuevo As Boolean = False

    Private Sub estilos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        cbCiudad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor

        Me.Text = " [Control de Proveedores] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        loadprove()

        RegistraAcceso(conexion, "Proveedores [ProveedoresCat.frm]")

        LlenaComboRazon()
        LlenaComboDx(cbProv, "numero", "RazonSocial", "proveed", conexion)

    End Sub

    Private Sub loadprove()
        LlenaCombobox(cbEstado, "Select distinct estado,codestado from sepomex order by estado", "Estado", "codestado", conexion)
        LlenaComboTransporte()
        LlenaComboBancos()



    End Sub
#End Region

#Region "movs combos"
    Private Sub cbEstado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbEstado.SelectedIndexChanged
        LlenaCombobox(cbCiudad, "Select distinct municipio,codestado from sepomex where estado='" & cbEstado.Text & "' order by municipio", "municipio", "codestado", conexion)
    End Sub

    Private Sub cbCiudad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCiudad.SelectedIndexChanged
        LlenaCombobox(cbColonia, "Select distinct colonia,codestado from sepomex where estado='" & cbEstado.Text & "' and municipio='" & cbCiudad.Text & "' order by colonia", "colonia", "codestado", conexion)
    End Sub

    Private Sub cbColonia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbColonia.SelectedIndexChanged
        LlenaCombobox(cbCP, "Select distinct cp,id from sepomex where estado='" & cbEstado.Text & _
                   "' and municipio='" & cbCiudad.Text & "' and colonia='" & cbColonia.Text & "' order by cp", "cp", "id", conexion)
    End Sub
#End Region

#Region "ABC"
    ' Guardar
    'Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        ValidaCombos()
        If regresa = False Then

            'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(209, Mensaje.Texto), IdiomaMensajes(209, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            MsgBox("Existen Campos Vacios Necesarios", MsgBoxStyle.Information)

            Exit Sub
        End If
        Cursor = Cursors.WaitCursor
        Dim qry As String = String.Empty
        If bNuevo Then
            qry = "Select nombre from proveed where nombre='" & cbRazon.SelectedItem.ToString.Trim.ToUpper & "'   "
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

            If dr.HasRows Then
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(209, Mensaje.Texto), IdiomaMensajes(209, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Sub
                'Else
                ' dr.Read()
                ' nomprove = dr("nombre").ToString.Trim
            End If
            dr.Close()
        End If
        Dim banco1 As String = String.Empty
        Dim banco2 As String = String.Empty
        Dim banco3 As String = String.Empty
        Dim banco4 As String = String.Empty
        Dim banco5 As String = String.Empty
        Dim banco6 As String = String.Empty
        If cbBanco1.SelectedIndex <> -1 Then banco1 = cbBanco1.SelectedItem.ToString.Trim
        If cbBanco2.SelectedIndex <> -1 Then banco2 = cbBanco2.SelectedItem.ToString.Trim
        If cbBanco3.SelectedIndex <> -1 Then banco3 = cbBanco3.SelectedItem.ToString.Trim
        If cbBanco4.SelectedIndex <> -1 Then banco4 = cbBanco4.SelectedItem.ToString.Trim
        If cbBanco5.SelectedIndex <> -1 Then banco5 = cbBanco5.SelectedItem.ToString.Trim
        If cbBanco6.SelectedIndex <> -1 Then banco6 = cbBanco6.SelectedItem.ToString.Trim
        Dim isactivo As Integer

        If chkactivo.Checked Then
            isactivo = 1
        Else
            isactivo = 0
        End If

        Dim sCPtmp As String = String.Empty
        If cbCP.SelectedIndex = -1 Then
            sCPtmp = cbCP.Tag
            cbCP.Tag = ""
        Else
            sCPtmp = CType(cbCP.SelectedItem, cInfoCombo).ID
        End If

        Dim regresar As String = String.Empty
        If bNuevo Then
            qry = "INSERT INTO [proveed] ([Nombre], [RazonSocial], [idDireccion], " & _
                 "[Calle], [Telefono], [Telefono2], " & _
                 "[Telefono3], [Telefono4], [Rfc], " & _
                 "[Observaciones], [Email], [limitecredito], [PlazoDias], " & _
                 "[PlazoDias2], [PlazoDias3], " & _
                 "[Descuento1], [Descuento2], [Descuento3], [sitioweb], " & _
                 "[cuentacontable], [transporte], [conveniotransporte], " & _
                 "[ContactoPagos], " & _
                 "[Emailpagos], [CuentaBancaria], [CuentaBancaria2], " & _
                  "[CuentaBancaria3], [CuentaBancaria4], [CuentaBancaria5], " & _
                  "[CuentaBancaria6], [Borrado], [FechaAlta], " & _
                  "[Banco1], [Banco2], [Banco3], [Banco4], " & _
                  "[Banco5], [Banco6], [Clabe1], [Clabe2], " & _
                  "[Clabe3], [Clabe4], [Clabe5], [Clabe6], " & _
                  "[limiteCredInt], [limiteCredProv], [FletePagaProv], " & _
                  "[Responsable], [telProp], " & _
                  "[radioProp], [mailProp],activo, llave) VALUES ('" & _
                  txNomCorto.Text.Trim.ToUpper & "', '" & cbRazon.SelectedItem.ToString.Trim.ToUpper & "', " & sCPtmp & ", '" & _
                  txCalle.Text.Trim.ToUpper & "', '" & txTel1.Text.Trim.ToUpper & "', '" & txTel2.Text.Trim.ToUpper & "', '" & _
                 txFax.Text.Trim.ToUpper & "', '" & txNextel.Text.Trim.ToUpper & "', '" & txRfc.Text.Trim.ToUpper & "', '" & _
                  txObserva.Text.Trim.ToUpper & "', '" & txEmail.Text.Trim.ToUpper & "', '" & txLimCrInt.Text.Trim & "', " & nudPlazo1.Value & ", " & _
                 nudPlazo2.Value & ", " & nudPlazo3.Value & ", " & _
                 nudDesc1.Value & ", " & nudDesc2.Value & ", " & nudDesc3.Value & ", '" & txWeb.Text.Trim.ToUpper & "', '" & _
                  txCuentaCont.Text.Trim.ToUpper & "', " & CType(cbTransporte.SelectedItem, cInfoCombo).ID & ", '" & txConvTrans.Text.Trim.ToUpper & "', '" & _
                  txContactoPago.Text.Trim.ToUpper & "', '" & _
                  txEmailPagos.Text.Trim.ToUpper & "', '" & txCuenta1.Text.Trim.ToUpper & "', '" & txCuenta2.Text.Trim.ToUpper & "', '" & _
                  txCuenta3.Text.Trim.ToUpper & "', '" & txCuenta4.Text.Trim.ToUpper & "', '" & txCuenta5.Text.Trim.ToUpper & "', '" & _
                  txCuenta6.Text.Trim.ToUpper & "', 0, GetDate(), '" & _
                  banco1 & "', '" & banco2 & "', '" & banco3 & "', '" & banco4 & "', '" & _
                  banco5 & "', '" & banco6 & "', '" & txClabe1.Text.Trim.ToUpper & "', '" & txClabe2.Text.Trim.ToUpper & "', '" & _
                  txClabe3.Text.Trim.ToUpper & "', '" & txClabe4.Text.Trim.ToUpper & "', '" & txClabe5.Text.Trim.ToUpper & "', '" & txClabe6.Text.Trim.ToUpper & "', '" & _
                  txLimCrInt.Text.Trim.ToUpper & "', '" & txLimCrPro.Text.Trim.ToUpper & "', " & IIf(chkFletePaga.Checked, 1, 0) & ", '" & _
                  txResponsable.Text.Trim.ToUpper & "', '" & txTelProp.Text.Trim & "', '" & _
                  txRadioProp.Text.Trim.ToUpper & "', '" & txMailProp.Text.Trim.ToUpper & "', '" & isactivo.ToString.Trim & "', " & _
                  "CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER)) SELECT @@IDENTITY AS id"

            regresar = bdBase.bdExecute(conexion, qry, False, True)
           
        Else

            Dim oInfoRazon As cInfoCombo = CType(cbRazon.SelectedItem, cInfoCombo)

            qry = "UPDATE [proveed] SET [Nombre] = '" & txNomCorto.Text.Trim.ToUpper & "', [RazonSocial] = '" & oInfoRazon.Text.Trim.ToUpper & "', " & _
                 "[idDireccion] = " & sCPtmp & ", [Calle] = '" & txCalle.Text.Trim.ToUpper & "', [Telefono] = '" & _
                 txTel1.Text.Trim.ToUpper & "', [Telefono2] = '" & txTel2.Text.Trim.ToUpper & "', [Telefono3] = '" & txFax.Text.Trim.ToUpper & "', " & _
                 "[Telefono4] = '" & txNextel.Text.Trim.ToUpper & "', [Rfc] = '" & txRfc.Text.Trim.ToUpper & "', [Observaciones] = '" & _
                 txObserva.Text.Trim.ToUpper & "', [Email] = '" & txEmail.Text.Trim.ToUpper & "', [limitecredito] = '" & txLimCrInt.Text.Trim & "', " & _
                 "[PlazoDias] = '" & nudPlazo1.Value & "', [PlazoDias2] = '" & nudPlazo2.Value & "', [PlazoDias3] = '" & nudPlazo3.Value & "', " & _
                 "[Descuento1] = '" & nudDesc1.Value & "', [Descuento2] = '" & nudDesc2.Value & "', [Descuento3] = '" & nudDesc3.Value & "', " & _
                 "[sitioweb] = '" & txWeb.Text.Trim.ToUpper & "', [cuentacontable] = '" & txCuentaCont.Text.Trim.ToUpper & "', [transporte] = '" & _
                 CType(cbTransporte.SelectedItem, cInfoCombo).ID & "', [conveniotransporte] = '" & txConvTrans.Text.Trim.ToUpper & "', " & _
                 "[ContactoPagos] = '" & txContactoPago.Text.Trim.ToUpper & "', " & _
                 "[Emailpagos] = '" & txEmailPagos.Text.Trim.ToUpper & "', " & _
                 "[CuentaBancaria] = '" & txCuenta1.Text.Trim.ToUpper & "', [CuentaBancaria2] = '" & txCuenta2.Text.Trim.ToUpper & "', " & _
                 "[CuentaBancaria3] = '" & txCuenta3.Text.Trim.ToUpper & "', [CuentaBancaria4] = '" & txCuenta4.Text.Trim.ToUpper & "', " & _
                 "[CuentaBancaria5] = '" & txCuenta5.Text.Trim.ToUpper & "', [CuentaBancaria6] = '" & txCuenta6.Text.Trim.ToUpper & "', " & _
                 "[Banco1] = '" & banco1 & "', [Banco2] = '" & banco2 & "', [Banco3] = '" & _
                 banco3 & "', [Banco4] = '" & banco4 & "', [Banco5] = '" & banco5 & "', [Banco6] = '" & banco6 & "', [Clabe1] = '" & txClabe1.Text.Trim.ToUpper & _
                 "', [Clabe2] = '" & txClabe2.Text.Trim.ToUpper & "', [Clabe3] = '" & txClabe3.Text.Trim.ToUpper & "', [Clabe4] = '" & txClabe4.Text.Trim.ToUpper & _
                 "', [Clabe5] = '" & txClabe5.Text.Trim.ToUpper & "', [Clabe6] = '" & txClabe6.Text.Trim.ToUpper & "', [limiteCredInt] = '" & _
                 txLimCrInt.Text.Trim.ToUpper & "', [limiteCredProv] = '" & txLimCrPro.Text.Trim.ToUpper & "', [FletePagaProv] = " & _
                 IIf(chkFletePaga.Checked, 1, 0) & "," & _
                 "[Responsable] = '" & txResponsable.Text.Trim.ToUpper & "', [telProp] = '" & _
                 txTelProp.Text.Trim & "', [radioProp] = '" & txRadioProp.Text.Trim.ToUpper & "', [mailProp] = '" & txMailProp.Text.Trim.ToUpper & _
                 "', [activo] = '" & isactivo.ToString.Trim & "'" & _
                  " WHERE numero = " & oInfoRazon.ID

            regresar = bdBase.bdExecute(conexion, qry)
        End If

        'bdBase.bdExecute(conexion, "update proveed set numero=id where numero is null")
        'LlenaCombobox(Combobox7, "Select distinct id,Nombre from proveed order by Nombre", "nombre", "id", conexion)

        Limpia()

        LlenaComboRazon()
        chkCopiar.Checked = False
        Cursor = Cursors.Default
        'If String.IsNullOrWhiteSpace(regresar) Then
        '    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(218, Mensaje.Texto), IdiomaMensajes(218, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Else
        '    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(219, Mensaje.Texto), IdiomaMensajes(219, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End If
    End Sub

    'Private Sub cbRazon_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbRazon.Validating
    Private Sub cbRazon_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbRazon.Validating
        Limpia()
        If cbRazon.SelectedIndex = -1 Then
            bNuevo = True
            Button10.Text = "&Guardar Nuevo Proveedor"

        Else
            Windows.Forms.Cursor.Current = Cursors.WaitCursor
            bNuevo = False
            Button10.Text = "&Actualizar Datos del Proveedor"
            Dim qry As String
            qry = "SELECT Nombre, RazonSocial, Calle, idDireccion, Telefono, Telefono2, Telefono3, Telefono4, Rfc, " & _
                  "Observaciones, Email, sitioweb, limiteCredInt, limiteCredProv, " & _
                  "PlazoDias, PlazoDias2, PlazoDias3, Descuento1, Descuento2, Descuento3, " & _
                  "cuentacontable, transporte, conveniotransporte, " & _
                  "FletePagaProv,  " & _
                  "ContactoPagos, Emailpagos, Banco1, CuentaBancaria, Clabe1, " & _
                  "Banco2, CuentaBancaria2, Clabe2, Banco3, CuentaBancaria3, Clabe3, " & _
                  "Banco4, cuentabancaria4, Clabe4, Banco5, cuentabancaria5, Clabe5, " & _
                  "Banco6, cuentabancaria6, Clabe6,  " & _
                  "Responsable, telProp, radioProp, mailProp, Activo " & _
                  "FROM proveed WHERE numero = " & CType(cbRazon.SelectedItem, cInfoCombo).ID

            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    If Not IsDBNull(dr("Nombre")) Then txNomCorto.Text = dr("Nombre").ToString.Trim
                    If Not IsDBNull(dr("Calle")) Then txCalle.Text = dr("Calle").ToString.Trim
                    If Not IsDBNull(dr("Telefono")) Then txTel1.Text = dr("Telefono").ToString.Trim
                    If Not IsDBNull(dr("Telefono2")) Then txTel2.Text = dr("Telefono2").ToString.Trim
                    If Not IsDBNull(dr("Telefono3")) Then txFax.Text = dr("Telefono3").ToString.Trim
                    If Not IsDBNull(dr("Telefono4")) Then txNextel.Text = dr("Telefono4").ToString.Trim
                    If Not IsDBNull(dr("Rfc")) Then txRfc.Text = dr("Rfc").ToString.Trim
                    If Not IsDBNull(dr("Observaciones")) Then txObserva.Text = dr("Observaciones").ToString.Trim
                    If Not IsDBNull(dr("Email")) Then txEmail.Text = dr("Email").ToString.Trim
                    If Not IsDBNull(dr("sitioweb")) Then txWeb.Text = dr("sitioweb").ToString.Trim
                    If Not IsDBNull(dr("limiteCredInt")) Then txLimCrInt.Text = dr("limiteCredInt").ToString.Trim
                    If Not IsDBNull(dr("limiteCredProv")) Then txLimCrPro.Text = dr("limiteCredProv").ToString.Trim
                    If Not IsDBNull(dr("PlazoDias")) Then nudPlazo1.Value = dr("PlazoDias")
                    If Not IsDBNull(dr("PlazoDias2")) Then nudPlazo2.Value = dr("PlazoDias2")
                    If Not IsDBNull(dr("PlazoDias3")) Then nudPlazo3.Value = dr("PlazoDias3")
                    If Not IsDBNull(dr("Descuento1")) Then nudDesc1.Value = dr("Descuento1")
                    If Not IsDBNull(dr("Descuento2")) Then nudDesc2.Value = dr("Descuento2")
                    If Not IsDBNull(dr("Descuento3")) Then nudDesc3.Value = dr("Descuento3")
                    If Not IsDBNull(dr("cuentacontable")) Then txCuentaCont.Text = dr("cuentacontable").ToString.Trim
                    If Not IsDBNull(dr("conveniotransporte")) Then txConvTrans.Text = dr("conveniotransporte").ToString.Trim
                    If Not IsDBNull(dr("FletePagaProv")) Then chkFletePaga.Checked = dr("FletePagaProv")
                    If Not IsDBNull(dr("ContactoPagos")) Then txContactoPago.Text = dr("ContactoPagos").ToString.Trim
                    If Not IsDBNull(dr("Emailpagos")) Then txEmailPagos.Text = dr("Emailpagos").ToString.Trim
                    If Not IsDBNull(dr("CuentaBancaria")) Then txCuenta1.Text = dr("CuentaBancaria").ToString.Trim
                    If Not IsDBNull(dr("CuentaBancaria2")) Then txCuenta2.Text = dr("CuentaBancaria2").ToString.Trim
                    If Not IsDBNull(dr("CuentaBancaria3")) Then txCuenta3.Text = dr("CuentaBancaria3").ToString.Trim
                    If Not IsDBNull(dr("cuentabancaria4")) Then txCuenta4.Text = dr("cuentabancaria4").ToString.Trim
                    If Not IsDBNull(dr("cuentabancaria5")) Then txCuenta5.Text = dr("cuentabancaria5").ToString.Trim
                    If Not IsDBNull(dr("cuentabancaria6")) Then txCuenta6.Text = dr("cuentabancaria6").ToString.Trim
                    If Not IsDBNull(dr("Clabe1")) Then txClabe1.Text = dr("Clabe1").ToString.Trim
                    If Not IsDBNull(dr("Clabe2")) Then txClabe2.Text = dr("Clabe2").ToString.Trim
                    If Not IsDBNull(dr("Clabe3")) Then txClabe3.Text = dr("Clabe3").ToString.Trim
                    If Not IsDBNull(dr("Clabe4")) Then txClabe4.Text = dr("Clabe4").ToString.Trim
                    If Not IsDBNull(dr("Clabe5")) Then txClabe5.Text = dr("Clabe5").ToString.Trim
                    If Not IsDBNull(dr("Clabe6")) Then txClabe6.Text = dr("Clabe6").ToString.Trim
                    If Not IsDBNull(dr("Responsable")) Then txResponsable.Text = dr("Responsable").ToString.Trim
                    If Not IsDBNull(dr("telProp")) Then txTelProp.Text = dr("telProp").ToString.Trim
                    If Not IsDBNull(dr("radioProp")) Then txRadioProp.Text = dr("radioProp").ToString.Trim
                    If Not IsDBNull(dr("mailProp")) Then txMailProp.Text = dr("mailProp").ToString.Trim
                    If Not IsDBNull(dr("activo")) Then chkactivo.Checked = dr("activo")
                    'Buscar datos sepomex
                    qry = "SELECT cp, colonia, municipio, estado FROM sepomex WHERE id = " & dr("idDireccion")
                    Dim drCp As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                    If Not drCp Is Nothing Then
                        If drCp.HasRows Then
                            drCp.Read()
                            BuscaEnComboDx(cbEstado, drCp("Estado").ToString.Trim)
                            BuscaEnComboDx(cbCiudad, drCp("municipio").ToString.Trim)
                            BuscaEnComboDx(cbColonia, drCp("colonia").ToString.Trim)
                            BuscaEnComboDx(cbCP, drCp("cp").ToString.Trim)
                        End If
                        drCp.Close()
                    End If
                    'Buscar combo transporte
                    BuscaEnComboDx(cbTransporte, dr("transporte"), True)

                    BuscaEnComboDx(cbBanco1, dr("Banco1").ToString.Trim)
                    BuscaEnComboDx(cbBanco2, dr("Banco2").ToString.Trim)
                    BuscaEnComboDx(cbBanco3, dr("Banco3").ToString.Trim)
                    BuscaEnComboDx(cbBanco4, dr("Banco4").ToString.Trim)
                    BuscaEnComboDx(cbBanco5, dr("Banco5").ToString.Trim)
                    BuscaEnComboDx(cbBanco6, dr("Banco6").ToString.Trim)


                End If
                dr.Close()
            End If
            Windows.Forms.Cursor.Current = Cursors.Default
            'bDentro = False
            'End If
            End If
    End Sub

    Private Sub Limpia()
        If Not chkCopiar.Checked Then
            txNomCorto.Text = ""
            txCalle.Text = ""
            txTel1.Text = ""
            txTel2.Text = ""
            txFax.Text = ""
            txNextel.Text = ""
            txRfc.Text = ""
            txObserva.Text = ""
            txEmail.Text = ""
            txWeb.Text = ""
            txLimCrInt.Text = 0
            txLimCrPro.Text = 0
            nudPlazo1.Value = 0
            nudPlazo2.Value = 0
            nudPlazo3.Value = 0
            nudDesc1.Value = 0
            nudDesc2.Value = 0
            nudDesc3.Value = 0
            txCuentaCont.Text = ""
            txConvTrans.Text = ""
            chkFletePaga.Checked = False
            'txContCompras.Text = ""
            'txMailCompras.Text = ""
            txContactoPago.Text = ""
            txEmailPagos.Text = ""
            txCuenta1.Text = ""
            txCuenta2.Text = ""
            txCuenta3.Text = ""
            txCuenta4.Text = ""
            txCuenta5.Text = ""
            txCuenta6.Text = ""
            txClabe1.Text = ""
            txClabe2.Text = ""
            txClabe3.Text = ""
            txClabe4.Text = ""
            txClabe5.Text = ""
            txClabe6.Text = ""
            'nudCapProdSemTot.Value = 0
            'nudCapProdSemRea.Value = 0
            'nudCapProdSemNos.Value = 0
            'nudCondExcelencia.Value = 0
            'nudLinProd.Value = 0
            'nudMaqSuajado.Value = 0
            'nudMaqPespunte.Value = 0
            'nudMaqMontado.Value = 0
            'txPrincProv.Text = ""
            'txPiel.Text = ""
            'txSuela.Text = ""
            'txForros.Text = ""
            'txOrnamentos.Text = ""
            'txPeriodicidad.Text = ""
            'txNuevo.Text = ""
            'txMarcas.Text = ""
            'txDescConf.Text = 0
            'txDevolCalidad.Text = 0
            'txDevolSobre.Text = 0
            'txAportaAniv.Text = 0
            txResponsable.Text = ""
            txTelProp.Text = ""
            txRadioProp.Text = ""
            txMailProp.Text = ""
            ' Buscar datos sepomex
            cbEstado.SelectedIndex = -1
            cbCiudad.SelectedIndex = -1
            cbColonia.SelectedIndex = -1
            cbCP.SelectedIndex = -1
            ' Buscar combo transporte
            cbTransporte.SelectedIndex = -1
            ' Buscar combo bancos
            cbBanco1.SelectedIndex = -1
            cbBanco2.SelectedIndex = -1
            cbBanco3.SelectedIndex = -1
            cbBanco4.SelectedIndex = -1
            cbBanco5.SelectedIndex = -1
            cbBanco6.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "Valida Combos"
    Private Function ValidaCombos() As Boolean
        Dim oInfo As cInfoCombo
        oInfo = CType(Me.cbEstado.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(220, Mensaje.Texto), IdiomaMensajes(220, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            regresa = False
            Exit Function
        End If



        oInfo = CType(Me.cbCiudad.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(222, Mensaje.Texto), IdiomaMensajes(222, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            regresa = False
            Exit Function
        End If

        If cbColonia.SelectedIndex <> -1 Then
            oInfo = CType(Me.cbColonia.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                regresa = False
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(cbColonia.Text) Then
                regresa = True
            Else
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                regresa = False
                Exit Function
            End If
        End If

        If cbCP.SelectedIndex <> -1 Then
            oInfo = CType(Me.cbCP.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(cbCP.Text) Then
                If cbCP.Text.Trim.Length = 5 Then
                    regresa = GuardaElementoSepomex()
                    'regresa = True
                Else
                    'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(225, Mensaje.Texto), IdiomaMensajes(225, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    regresa = False
                    Exit Function
                End If
            Else
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                regresa = False
                Exit Function
            End If
        End If

        If cbTransporte.SelectedIndex = -1 Then
            'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(226, Mensaje.Texto), IdiomaMensajes(226, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            regresa = False
            Exit Function
        Else
            regresa = True
        End If
    End Function

    Private Function GuardaElementoSepomex() As Boolean
        Dim regresar As Boolean = False
        If Not String.IsNullOrEmpty(cbColonia.Text.Trim) And Not String.IsNullOrEmpty(cbCP.Text.Trim) Then
            Dim sQuery As String = "INSERT INTO sepomex (cp, colonia, municipio, ciudad, estado, codEstado, codigoPais, llave) VALUES ('" & _
                                           cbCP.Text.Trim & "', '" & cbColonia.Text.Trim & "', '" & cbCiudad.Text.Trim & "', '" & cbCiudad.Text.Trim & "', '" & _
                                           cbEstado.Text.Trim & "', '" & CType(cbEstado.SelectedItem, cInfoCombo).ID & "', 'MX', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER)) SELECT @@IDENTITY AS id"
            Dim regresa As String = bdBase.bdExecute(conexion, sQuery, False, True)
            If Not String.IsNullOrEmpty(regresa) Then
                If IsNumeric(regresa) Then
                    cbCP.Tag = regresa
                    regresar = True
                Else
                    cbCP.Tag = "-1"
                End If
            End If
        End If
        Return regresar
    End Function
#End Region

#Region "Fills combos"
    'Private Sub FillCombo(ByVal elemento As String, ByVal id As String, ByVal combo As System.Windows.Forms.ComboBox, ByVal tabla As String)
    '    Dim qry As String
    '    qry = "SELECT      " & elemento & _
    '       " FROM   " & tabla & _
    '       " WHERE     (id ='" & id & "')"
    '    Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
    '    dr2.Read()
    '    Dim contador As Integer = 0
    '    For contador = 0 To combo.Items.Count - 1
    '        If combo.Items.Item(contador).ToString.Trim = dr2(elemento).ToString.Trim Then
    '            combo.SelectedIndex = contador
    '            Exit For
    '        End If
    '    Next
    'End Sub

    'Private Sub FillCombobox(ByVal elemento As String, ByVal id As String, ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal tabla As String)
    '    Dim qry As String
    '    If id = "" Or id = "-1" Then
    '    Else

    '        qry = "SELECT      " & elemento & _
    '           " FROM   " & tabla & _
    '           " WHERE     (numero ='" & id & "')"
    '        Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
    '        dr2.Read()
    '        Dim aux As String = dr2(elemento).ToString.Trim
    '        For x As Integer = 0 To combo.Properties.Items.Count
    '            combo.SelectedIndex = x
    '            If combo.Text.ToString.Trim.ToUpper = aux.ToString.Trim.ToUpper Then

    '                Exit For
    '            End If

    '        Next
    '    End If

    'End Sub
#End Region

    'Private Sub DataGridView2_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    init = True
    'End Sub

#Region "Catálogo de proveedores"
    'Private Sub Button22_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
    Private Sub Button22_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        Me.Cursor = Cursors.WaitCursor
        Dim qry As String
        Dim activos As Integer = 0
        If RadioGroup1.SelectedIndex = 0 Then activos = 1
        If chkTodos.Checked Then
            qry = "SELECT proveed.numero as numero,proveed.Nombre as MarcaPrincipal, proveed.RazonSocial, proveed.Calle, sepomex.colonia AS Colonia, sepomex.municipio AS Ciudad, sepomex.estado AS Estado, " & _
                  "sepomex.cp As CP, proveed.Telefono, proveed.Telefono2, proveed.Telefono3 AS Fax, proveed.Telefono4 AS NextelId, proveed.Rfc, " & _
                  "proveed.Observaciones, proveed.Email, proveed.sitioweb AS SitioWeb, proveed.limiteCredInt AS LimiteCreditoInterno, proveed.limiteCredProv AS " & _
                  "LimiteCreditoProveedor, proveed.PlazoDias, proveed.PlazoDias2, proveed.PlazoDias3, proveed.Descuento1, proveed.Descuento2, proveed.Descuento3, " & _
                  "proveed.cuentacontable AS CuentaContable, Transportes.Nombre AS Transporte, proveed.conveniotransporte AS ConvenioTransporte, " & _
                  "proveed.FletePagaProv AS ProveedorPagaFlete,  " & _
                  "proveed.ContactoPagos, proveed.Emailpagos, proveed.Banco1, proveed.CuentaBancaria, proveed.Clabe1, " & _
                  "proveed.Banco2, proveed.CuentaBancaria2, proveed.Clabe2, proveed.Banco3, proveed.CuentaBancaria3, proveed.Clabe3, " & _
                  "proveed.Banco4, proveed.cuentabancaria4, proveed.Clabe4, proveed.Banco5, proveed.cuentabancaria5, proveed.Clabe5, " & _
                  "proveed.Banco6, proveed.cuentabancaria6, proveed.Clabe6, " & _
                  "proveed.FechaAlta, " & _
                  "proveed.Responsable, proveed.telProp, " & _
                  "proveed.radioProp, proveed.mailProp,proveed.activo " & _
                  "FROM proveed INNER JOIN " & _
                  "sepomex ON proveed.idDireccion = sepomex.id INNER JOIN " & _
                  "Transportes ON proveed.transporte = Transportes.id WHERE Activo = " & activos & " ORDER BY proveed.Nombre"
        Else
            If cbProv.SelectedIndex = -1 Then
                Cursor = Cursors.Default
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(227, Mensaje.Texto), IdiomaMensajes(227, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            qry = "SELECT proveed.numero as numero,proveed.Nombre as MarcaPrincipal, proveed.RazonSocial, proveed.Calle, sepomex.colonia AS Colonia, sepomex.municipio AS Ciudad, sepomex.estado AS Estado, " & _
                  "sepomex.cp As CP, proveed.Telefono, proveed.Telefono2, proveed.Telefono3 AS Fax, proveed.Telefono4 AS NextelId, proveed.Rfc, " & _
                  "proveed.Observaciones, proveed.Email, proveed.sitioweb AS SitioWeb, proveed.limiteCredInt AS LimiteCreditoInterno, proveed.limiteCredProv AS " & _
                  "LimiteCreditoProveedor, proveed.PlazoDias, proveed.PlazoDias2, proveed.PlazoDias3, proveed.Descuento1, proveed.Descuento2, proveed.Descuento3, " & _
                  "proveed.cuentacontable AS CuentaContable, Transportes.Nombre AS Transporte, proveed.conveniotransporte AS ConvenioTransporte, " & _
                  "proveed.FletePagaProv AS ProveedorPagaFlete, " & _
                  "proveed.ContactoPagos, proveed.Emailpagos, proveed.Banco1, proveed.CuentaBancaria, proveed.Clabe1, " & _
                  "proveed.Banco2, proveed.CuentaBancaria2, proveed.Clabe2, proveed.Banco3, proveed.CuentaBancaria3, proveed.Clabe3, " & _
                  "proveed.Banco4, proveed.cuentabancaria4, proveed.Clabe4, proveed.Banco5, proveed.cuentabancaria5, proveed.Clabe5, " & _
                  "proveed.Banco6, proveed.cuentabancaria6, proveed.Clabe6,  " & _
                  "proveed.FechaAlta, " & _
                  "proveed.Responsable, proveed.telProp, " & _
                  "proveed.radioProp, proveed.mailProp,proveed.activo " & _
                  "FROM proveed INNER JOIN " & _
                  "sepomex ON proveed.idDireccion = sepomex.id INNER JOIN " & _
                  "Transportes ON proveed.transporte = Transportes.id WHERE proveed.numero = " & CType(cbProv.SelectedItem, cInfoCombo).ID & _
                  " AND Activo = " & activos & " ORDER BY proveed.Nombre"
        End If
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl5.DataSource = dr2.Tables(0)
        GridView5.PopulateColumns()
        GridView5.BestFitColumns()
        Cursor = Cursors.Default
    End Sub

    'Private Sub Button24_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
    Private Sub Button24_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button24.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl5)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
#End Region

    Private Sub LlenaComboRazon()
        LlenaComboDx(cbRazon, "numero", "RazonSocial", "proveed", conexion)
    End Sub

    Private Sub LlenaComboTransporte()
        LlenaComboDx(cbTransporte, "id", "nombre", "transportes", conexion)
    End Sub

    Private Sub cbTransporte_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbTransporte.Validating
        If cbTransporte.SelectedIndex = -1 Then
            If Not String.IsNullOrWhiteSpace(cbTransporte.Text.Trim) Then
                Dim sTmp As String = cbTransporte.Text.Trim.ToUpper
                Dim sQuery As String = "insert into transportes (Nombre ) values ('" & sTmp & "')"
                Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
                If String.IsNullOrWhiteSpace(regresa) Then
                    LlenaComboTransporte()
                    BuscaEnComboDx(cbTransporte, sTmp)
                End If
            End If
        End If
    End Sub

    Private Sub LlenaComboBancos()
        LlenaComboDx(cbBanco1, "numero", "Nombre", "bancosproveedores", conexion)
        LlenaComboDx(cbBanco2, "numero", "Nombre", "bancosproveedores", conexion)
        LlenaComboDx(cbBanco3, "numero", "Nombre", "bancosproveedores", conexion)
        LlenaComboDx(cbBanco4, "numero", "Nombre", "bancosproveedores", conexion)
        LlenaComboDx(cbBanco5, "numero", "Nombre", "bancosproveedores", conexion)
        LlenaComboDx(cbBanco6, "numero", "Nombre", "bancosproveedores", conexion)
    End Sub

    Private Sub cbBanco1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco1.Validating
        ValidaBanco(cbBanco1)
    End Sub

    Private Sub cbBanco2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco2.Validating
        ValidaBanco(cbBanco2)
    End Sub

    Private Sub cbBanco3_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco3.Validating
        ValidaBanco(cbBanco3)
    End Sub

    Private Sub cbBanco4_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco4.Validating
        ValidaBanco(cbBanco4)
    End Sub

    Private Sub cbBanco5_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco5.Validating
        ValidaBanco(cbBanco5)
    End Sub

    Private Sub cbBanco6_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cbBanco6.Validating
        ValidaBanco(cbBanco6)
    End Sub

    Private Sub ValidaBanco(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit)
        If combo.SelectedIndex = -1 Then
            If Not String.IsNullOrWhiteSpace(combo.Text.Trim) Then
                Dim sTmp As String = combo.Text.Trim.ToUpper
                Dim sQuery As String = "INSERT INTO bancosProveedores (NOMBRE) values ('" & sTmp & "')"
                Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
                If String.IsNullOrWhiteSpace(regresa) Then
                    LlenaComboBancos()
                    'combo.SelectedIndex = BuscaIdBanco(sTmp, combo)
                    BuscaEnComboDx(combo, sTmp)
                End If
            End If
        End If
    End Sub

    Private Sub PoneEnCero(ByVal textBox As DevExpress.XtraEditors.TextEdit)
        If String.IsNullOrEmpty(textBox.Text.Trim) Then textBox.Text = 0
    End Sub

    Private Sub txLimCrInt_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txLimCrInt.Validating
        PoneEnCero(txLimCrInt)
    End Sub

    Private Sub txLimCrPro_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txLimCrPro.Validating
        PoneEnCero(txLimCrPro)
    End Sub






    Private Sub chkCopiar_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkCopiar.CheckedChanged
        If chkCopiar.Checked Then
            bNuevo = True
            Button10.Text = "&Guardar Nuevo Proveedor"
            cbRazon.SelectedIndex = -1
        End If
    End Sub

    Private Sub chkTodos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkTodos.CheckedChanged
        cbProv.Enabled = Not chkTodos.Checked
    End Sub

   
End Class