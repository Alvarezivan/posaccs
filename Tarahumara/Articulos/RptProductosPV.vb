﻿
Imports System.Data

Imports System.Data.SqlClient

Imports System.Drawing.Image

Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Controls
Public Class RptProductosPV

    Private Sub RptProductos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Splash(True)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Reporte de Productos] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        RegistraAcceso(conexion, "Reporte Productos [RptProductos.frm]")
        llenareporte()
        Splash(False)

    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Splash(True)



        Dim qry As String = "SELECT idproducto from skus where sku = '" & tbsku.Text.Trim & "'"
        Dim id As Integer = -1
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                If Not IsDBNull(dr("idproducto")) Then id = dr("idproducto").ToString.Trim
            End If

        End If
        dr.Close()

        If id <> -1 Then

            qry = "SELECT P.idProducto, P.Codigo, S.subFamilia,M.Marca,P.Descripcion,  F.Familia,sum(ex.cantidad) as Existencias, " & _
                "max(fac.fecha) as UltimaCompra,p.Costo,p.Precio, P.Resurtible, P.basico as Procedencia,  P.FechaAlta,  P.UltimaModificacion, " & _
                "pr.nombre as Proveedor,T.Temporada,P.Observaciones   FROM  Productos p INNER JOIN  Familias F ON P.idFamilia = F.idFamilia INNER JOIN " & _
                "SubFamilias s ON P.idSubFamilia = S.idSubFamilia INNER JOIN  Marcas m ON P.idMarca = M.idMarca INNER JOIN " & _
                "Temporadas t ON P.idTemporada = T.idTemporada  left join existen ex on p.idproducto = ex.idProducto left join facturadetalle " & _
                "fac on p.idproducto = fac.idproducto inner join proveed pr on pr.numero = p.idproveedor where p.idproducto = " & id & " group by P.idProducto, P.Codigo, S.subFamilia, " & _
                "P.Descripcion, M.Marca, F.Familia,  T.Temporada, P.Observaciones,    P.Precio, P.COSTO,PR.NOMBRE , P.Resurtible,  P.FechaAlta, " & _
                "P.UltimaModificacion, p.basico "


            Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
            GridControl2.DataSource = dr2.Tables(0)
            GridView2.PopulateColumns()
            'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView2.HorzScrollVisibility = True
            GridView2.VertScrollVisibility = True
            GridView2.BestFitColumns()
            GridView2.Columns("idProducto").Visible = False

            lbco.Visible = True
            lbcod.Visible = True
            lbcod.Text = tbsku.Text.Trim

        Else

            MsgBox("Codigo no localizado", MsgBoxStyle.Exclamation)

        End If

        Splash(False)
    End Sub

    Private Sub llenareporte()
        Splash(True)

        Dim qry As String = ""
        qry = "SELECT P.idProducto, P.Codigo, S.subFamilia,M.Marca,P.Descripcion,  F.Familia,isnull(sum(ex.cantidad),0) as Existencias, " & _
            "p.Precio,  " & _
            "T.Temporada,P.Observaciones   FROM  Productos p INNER JOIN  Familias F ON P.idFamilia = F.idFamilia INNER JOIN " & _
            "SubFamilias s ON P.idSubFamilia = S.idSubFamilia INNER JOIN  Marcas m ON P.idMarca = M.idMarca INNER JOIN " & _
            "Temporadas t ON P.idTemporada = T.idTemporada  left join existen ex on p.idproducto = ex.idProducto  " & _
            "group by P.idProducto, P.Codigo, S.subFamilia, " & _
            "P.Descripcion, M.Marca, F.Familia,  T.Temporada, P.Observaciones,    P.Precio"


        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.HorzScrollVisibility = True
        GridView2.VertScrollVisibility = True
        GridView2.BestFitColumns()
        GridView2.Columns("idProducto").Visible = False


        Splash(False)

    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl2)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        lbco.Visible = False
        lbcod.Visible = False
        llenareporte()
    End Sub



    Private Sub GridControl2_Click(sender As System.Object, e As System.EventArgs) Handles GridControl2.DoubleClick

        CodigoRPTPV = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "Codigo")
        DialogResult = DialogResult.OK
    End Sub

End Class