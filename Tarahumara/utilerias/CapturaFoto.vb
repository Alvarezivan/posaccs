﻿Public Class CapturaFoto
    Dim TFoto As New cCamara

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnTomar.Click

        Static Foto As Boolean
        If Not Foto Then
            btnexaminar.Enabled = False
            TFoto = New cCamara
            Dim Activado As Boolean = True
            Dim uError As Boolean = False
            TFoto.ActivarCamara(Form1, Activado, uError)
            If Activado Then
                btnTomar.Text = "Capturar"
                Foto = True
            Else
                btnTomar.Enabled = False
            End If
            If uError Then btnExaminar.Enabled = True
        Else
            TFoto.TomarFoto(pbImagenes)
            TFoto = Nothing
            btnTomar.Text = "Tomar"
            btnExaminar.Enabled = True
            Foto = False
        End If
    End Sub
End Class