﻿'Imports Microsoft.Win32
Imports System.Data.SqlClient

Public Class cambiaPass
    Private Anterior As String = String.Empty  ' *
    Private Cambiado As Boolean = False
    Private NuevaClave As String = String.Empty
    Private FechaExp As DateTime
    Private idUsuario As Integer = -1  ' *
    Private nomUsuario As String = String.Empty
    Private leyenda As Boolean = True

#Region "Propiedades"
    Public ReadOnly Property pCambiado As Boolean
        Get
            Return Cambiado
        End Get
    End Property

    Public WriteOnly Property pAnterior As String
        Set(value As String)
            Anterior = value
        End Set
    End Property

    Public ReadOnly Property pNuevaClave As String
        Get
            Return NuevaClave
        End Get
    End Property

    Public ReadOnly Property pFechaExp As DateTime
        Get
            Return FechaExp
        End Get
    End Property

    Public WriteOnly Property pIdUsuario As Integer
        Set(value As Integer)
            idUsuario = value
        End Set
    End Property

    Public WriteOnly Property pLeyenda As Boolean
        Set(value As Boolean)
            leyenda = value
        End Set
    End Property

    Public WriteOnly Property pNomUsuario As String
        Set(value As String)
            nomUsuario = value
        End Set
    End Property
#End Region

#Region "Validaciones"
    Private Sub txActual_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txActual.Validating
        If txActual.Text.Trim = Anterior Then
            DxErrorProvider1.SetError(txActual, "")
        Else
            DxErrorProvider1.SetError(txActual, "La clave actual no coincide")
            'btGuardar.Enabled = False
            txActual.Focus()
        End If
    End Sub

    Private Sub txNueva_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txNueva.Validating
        If Not String.IsNullOrWhiteSpace(txNueva.Text.Trim) Then
            If txNueva.Text.Trim.Length >= 8 Then
                DxErrorProvider1.SetError(txNueva, "")
            Else
                DxErrorProvider1.SetError(txNueva, "La clave dede tener al menos 8 caracteres")
            End If
        Else
            DxErrorProvider1.SetError(txNueva, "No puede quedar en blanco")
            'btGuardar.Enabled = False
            txNueva.Focus()
        End If
    End Sub

    Private Sub txConfirma_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txConfirma.Validating
        If Not String.IsNullOrWhiteSpace(txConfirma.Text.Trim) Then
            If txNueva.Text.Trim = txConfirma.Text.Trim Then
                DxErrorProvider1.SetError(txConfirma, "")
                'btGuardar.Enabled = True
                'btGuardar.Focus()
            Else
                DxErrorProvider1.SetError(txConfirma, "La nueva clave no coincide con la confirmación")
                'btGuardar.Enabled = False
                txConfirma.Focus()
            End If
        Else
            DxErrorProvider1.SetError(txConfirma, "No puede quedar en blanco")
            'btGuardar.Enabled = False
            txConfirma.Focus()
        End If
    End Sub
#End Region

    Private Sub btGuardar_Click(sender As System.Object, e As System.EventArgs) Handles btGuardar.Click
        If DxErrorProvider1.HasErrors Then
            DevExpress.XtraEditors.XtraMessageBox.Show("Hay errores en sus datos, por favor corrija", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            txActual.Focus()
        Else
            Dim sQuery As String = String.Empty
            Dim bContinua As Boolean = True
            ' Checar clave anterior
            If Not leyenda Then
                sQuery = "SELECT [PASSWORD] FROM empleado WHERE id = " & oLogin.pUserId
                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        dr.Read()
                        If Anterior <> dr("PASSWORD") Then bContinua = False
                    End If
                    dr.Close()
                End If
            End If
            If bContinua Then
                ' Guardar en bases saz
                NuevaClave = txNueva.Text.Trim
                FechaExp = Now.AddDays(30)
                Dim des As New cTripleDES

                Dim regresa As String = String.Empty
                If Not leyenda Then
                    sQuery = "UPDATE empleado SET [PASSWORD] = '" & txNueva.Text.Trim & "' WHERE id = " & oLogin.pUserId
                    regresa = bdBase.bdExecute(conexion, sQuery)
                End If
                ' Guardar en tabla empleados
                If String.IsNullOrEmpty(regresa) Then
                    '' Actualiza el registro
                    'Dim hallado As Boolean = False
                    'Dim rkU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios", True)
                    'Dim Usuarios() As String = rkU.GetSubKeyNames
                    'Dim rkUser As RegistryKey
                    'For iCual As Integer = 0 To Usuarios.Length - 1
                    '    Dim rkUU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\" & Usuarios(iCual), True)
                    '    If Not String.IsNullOrEmpty(rkUU.GetValue("User")) Then
                    '        Dim sTmp As String = des.Decrypt(rkUU.GetValue("User"))
                    '        '  oLogin.pUsrNombre
                    '        If sTmp.Trim.ToLower = nomUsuario.ToLower Then
                    '            rkUser = rkUU
                    '            hallado = True
                    '            Exit For
                    '        End If
                    '    End If
                    'Next
                    'If hallado Then
                    '    rkUser.SetValue("Pwd", des.Encrypt(NuevaClave))
                    '    rkUser.SetValue("Data5", des.Encrypt(FechaExp))
                    'End If

                    Dim oDatosActiva As New datosActivacion
                    oDatosActiva.RecuperaDatosEmpleado(nomUsuario)
                    oDatosActiva.AgregaUsuario("", "", "", "", FechaExp.ToString, "", NuevaClave, oDatosActiva.pEId)

                    ' Guardar en logins
                    sQuery = "UPDATE Rlogin SET Expira = '" & String.Format("{0:yyyyMMdd HH:mm:ss}", FechaExp) & "', Clave = '" & des.Encrypt(NuevaClave) & "' WHERE Usuario = '" & nomUsuario & "'"
                    regresa = bdBase.bdExecute(des.Decrypt(My.Settings.conLogins), sQuery)
                    If Not String.IsNullOrWhiteSpace(regresa) Then
                        DevExpress.XtraEditors.XtraMessageBox.Show("Hubo un error al guardar sus datos por favor reintente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Else
                        Cambiado = True
                        Me.Close()
                    End If
                Else
                    DevExpress.XtraEditors.XtraMessageBox.Show("En este momento no se puede cambiar la clave, por favor intente más tarde", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End If
            Else
                Cambiado = False
                Me.Close()
            End If
            End If
    End Sub

    Private Sub cambiaPass_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        LabelControl1.Visible = leyenda
    End Sub
End Class