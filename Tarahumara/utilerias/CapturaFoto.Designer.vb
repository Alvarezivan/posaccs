﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CapturaFoto
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTomar = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.pbimagenes = New System.Windows.Forms.PictureBox()
        Me.btnexaminar = New System.Windows.Forms.Button()
        CType(Me.pbimagenes,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'btnTomar
        '
        Me.btnTomar.Location = New System.Drawing.Point(238, 115)
        Me.btnTomar.Name = "btnTomar"
        Me.btnTomar.Size = New System.Drawing.Size(75, 23)
        Me.btnTomar.TabIndex = 0
        Me.btnTomar.Text = "Iniciar"
        Me.btnTomar.UseVisualStyleBackColor = True
        '
        'pbimagenes
        '
        Me.pbimagenes.Location = New System.Drawing.Point(12, 12)
        Me.pbimagenes.Name = "pbimagenes"
        Me.pbimagenes.Size = New System.Drawing.Size(207, 157)
        Me.pbimagenes.TabIndex = 2
        Me.pbimagenes.TabStop = False
        '
        'btnexaminar
        '
        Me.btnexaminar.Location = New System.Drawing.Point(238, 144)
        Me.btnexaminar.Name = "btnexaminar"
        Me.btnexaminar.Size = New System.Drawing.Size(75, 23)
        Me.btnexaminar.TabIndex = 3
        Me.btnexaminar.Text = "Capturar"
        Me.btnexaminar.UseVisualStyleBackColor = True
        '
        'CapturaFoto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 179)
        Me.Controls.Add(Me.btnexaminar)
        Me.Controls.Add(Me.pbimagenes)
        Me.Controls.Add(Me.btnTomar)
        Me.Name = "CapturaFoto"
        Me.Text = "CapturaFoto"
        CType(Me.pbimagenes,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents btnTomar As System.Windows.Forms.Button
    Friend WithEvents pbimagenes As System.Windows.Forms.PictureBox
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnexaminar As System.Windows.Forms.Button
End Class
