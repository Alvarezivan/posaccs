﻿' Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
'Imports System.Data.SqlServerCe

Delegate Sub FunctionCall(ByVal param)

Public Class HuellaDigital
    Implements DPFP.Capture.EventHandler

    Private Template As DPFP.Template
    Private Verificator As DPFP.Verification.Verification
    Private Capturer As DPFP.Capture.Capture

    Private Sub Asistencia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If String.IsNullOrEmpty(conexionLocal) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(307, Mensaje.Texto), IdiomaMensajes(307, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        IdiomaTextos(Me)
        RegistraAcceso(conexionLocal, "Reloj Checador [huelladigital.frm]")

        Dim qry As String = "SELECT Numero, Nombre, PASSWORD FROM  empleado ORDER BY NOMBRE"
        LlenaCombobox(ComboBoxEdit1, qry, "Nombre", "Numero", conexion)
        Init()
        StartCapture()
        CheckForIllegalCrossThreadCalls = False

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oInfo As cInfoCombo
        oInfo = CType(ComboBoxEdit1.SelectedItem, cInfoCombo)
        Dim qry As String = "SELECT  upper(PASSWORD) as Pass FROM  empleado where numero='" & oInfo.ID.ToString & "' ORDER BY NOMBRE"
        Dim dspass As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
        dspass.Read()
        If dspass("Pass").ToString.Trim.ToUpper = TextBox1.Text.Trim.ToUpper Then

            Dim FechaSql As String
            Dim query1, qryins, origen, tipo As String

            origen = "1"
            qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
            Dim ds As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
            ds.Read()
            FechaSql = ds("fecha")
            ds.Close()

            '        query1 = "SELECT     empleado.NOMBRE,  MAX(logdia.Fecha) AS ultimaoperacion,  " & _
            '" logdia.Tienda AS Tienda, logdia.Tipo AS tipo " & _
            '" FROM         empleado INNER JOIN " & _
            '                      " logdia ON logdia.nombre = empleado.nombre AND  " & _
            '                       " CONVERT(CHAR(8), logdia.fecha , 112) = '" + Format(FechaSql, "yyyyMMdd") + "' " & _
            '" WHERE     (empleado.nombre = '" + oInfo.Text.ToString.Trim + "')  " & _
            '" GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda " & _
            '" ORDER BY  ultimaoperacion desc"
            query1 = "SELECT empleado.NOMBRE, MAX(logdia.Fecha) AS ultimaoperacion, " & _
                     "logdia.Tienda AS Tienda, logdia.Tipo AS tipo " & _
                     " FROM empleado INNER JOIN logdia ON logdia.nombre = empleado.nombre AND  " & _
                     " CONVERT(nCHAR(8), logdia.fecha , 112) = '" + Format(FechaSql, "yyyyMMdd") + "' " & _
                     " WHERE     (empleado.nombre = '" & CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.Trim & "')  " & _
                     " GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda " & _
                     " ORDER BY  ultimaoperacion desc"
            Dim thedate As Date
            Dim cfecha As String
            qry = "Select getDate() as FechaHora "
            Dim drf As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
            drf.Read()
            thedate = drf("FechaHora")
            drf.Close()
            cfecha = thedate.ToString("yyyyMMdd")
            Dim chora As String = thedate.ToString("HH:mm:ss") ' drf("FechaHora").ToString.Trim.Substring(11, 10)
            Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexionLocal, query1)
            Dim bTieneRenglones As Boolean = False
            While ds2.Read()
                bTieneRenglones = True
                If ds2("Tienda").ToString.Trim = My.Settings.Item("TiendaActual") Then

                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        'hago salida
                        tipo = "SALIDA"
                        qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave, autoriza) values ('" & _
                         oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                         oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), 0)"
                        bdBase.bdExecute(conexionLocal, qryins)
                        MessageBox.Show(IdiomaMensajes(303, Mensaje.Texto), IdiomaMensajes(303, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Me.Close()
                        Exit While
                    Else
                        Dim idSuper As Integer = 0
                        Dim ret As Integer = 0
                        If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                            Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                            ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                        End If
                        Select Case ret
                            Case 0
                                tipo = "ENTRADA"
                            Case 1
                                tipo = "ENTRADA R"
                            Case 2
                                Dim myAdmin As New Administradores
                                myAdmin.ShowDialog()
                                If myAdmin.Verificado Then
                                    tipo = "ENTRADA A"
                                    idSuper = myAdmin.idSuper
                                End If
                            Case Else
                                tipo = ""
                        End Select
                        If Not String.IsNullOrEmpty(tipo) Then
                            'hago entrada
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave, autoriza) values ('" & _
                                    oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                                    oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                            bdBase.bdExecute(conexionLocal, qryins)
                            MessageBox.Show(IdiomaMensajes(306, Mensaje.Texto), IdiomaMensajes(306, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Close()
                            Exit While
                        End If
                    End If
                Else
                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        'Tiene que salir de la tienda origen no hace nada
                        MessageBox.Show(IdiomaMensajes(305, Mensaje.Texto))
                        Me.Close()
                        Exit While
                    Else
                        Dim idSuper As Integer = 0
                        Dim ret As Integer = 0
                        If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                            Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                            ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                        End If
                        Select Case ret
                            Case 0
                                tipo = "ENTRADA"
                            Case 1
                                tipo = "ENTRADA R"
                            Case 2
                                Dim myAdmin As New Administradores
                                myAdmin.ShowDialog()
                                If myAdmin.Verificado Then
                                    tipo = "ENTRADA A"
                                    idSuper = myAdmin.idSuper
                                End If
                            Case Else
                                tipo = ""
                        End Select
                        If Not String.IsNullOrEmpty(tipo) Then
                            'hago entrada en esta tienda
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave, autoriza) values ('" & _
                             oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                             oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                            bdBase.bdExecute(conexionLocal, qryins)
                            MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)

                            Me.Close()
                            Exit While
                        End If
                    End If
                End If
            End While
            ds2.Close()
            query1 = "SELECT empleado.NOMBRE, tiendas.HoraAp, tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, " & _
                         "empleado.HoraEnt, empleado.HoraSal FROM empleado CROSS JOIN Tiendas WHERE (empleado.NOMBRE = '" & _
                         CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.Trim & "') AND (tiendas.ID = " & My.Settings.TiendaActual & ") " & _
                         "GROUP BY empleado.NOMBRE, tiendas.HoraAp, tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, " & _
                         "empleado.HoraEnt, empleado.HoraSal"
            ds2 = bdBase.bdDataReader(conexionLocal, query1)
            If Not ds2 Is Nothing Then
                If ds2.HasRows Then
                    ds2.Read()
                    If Not bTieneRenglones Then
                        Dim idSuper As Integer = 0
                        Dim ret As Integer = 0
                        If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                            Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                            ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                        End If
                        Select Case ret
                            Case 0
                                tipo = "ENTRADA"
                            Case 1
                                tipo = "ENTRADA R"
                            Case 2
                                Dim myAdmin As New Administradores
                                myAdmin.ShowDialog()
                                If myAdmin.Verificado Then
                                    tipo = "ENTRADA A"
                                    idSuper = myAdmin.idSuper
                                End If
                            Case Else
                                tipo = ""
                        End Select
                        If Not String.IsNullOrEmpty(tipo) Then
                            ' no tiene registros este día en ningun lado, es entrada a la tienda actual
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave, autoriza) values ('" & _
                                     oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                                     oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                            bdBase.bdExecute(conexionLocal, qryins)
                            MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Close()
                        End If
                    End If
                End If
                ds2.Close()
            End If
        Else
            MessageBox.Show(IdiomaMensajes(297, Mensaje.Texto), IdiomaMensajes(297, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
        dspass.Close()
    End Sub

    Public Sub OnComplete(ByVal Capture As Object, ByVal ReaderSerialNumber As String, ByVal Sample As DPFP.Sample) Implements DPFP.Capture.EventHandler.OnComplete
        Process(Sample)
    End Sub

    Public Sub OnFingerGone(ByVal Capture As Object, ByVal ReaderSerialNumber As String) Implements DPFP.Capture.EventHandler.OnFingerGone

    End Sub

    Public Sub OnFingerTouch(ByVal Capture As Object, ByVal ReaderSerialNumber As String) Implements DPFP.Capture.EventHandler.OnFingerTouch

    End Sub

    Public Sub OnReaderConnect(ByVal Capture As Object, ByVal ReaderSerialNumber As String) Implements DPFP.Capture.EventHandler.OnReaderConnect

    End Sub

    Public Sub OnReaderDisconnect(ByVal Capture As Object, ByVal ReaderSerialNumber As String) Implements DPFP.Capture.EventHandler.OnReaderDisconnect

    End Sub

    Public Sub OnSampleQuality(ByVal Capture As Object, ByVal ReaderSerialNumber As String, ByVal CaptureFeedback As DPFP.Capture.CaptureFeedback) Implements DPFP.Capture.EventHandler.OnSampleQuality
        If CaptureFeedback = DPFP.Capture.CaptureFeedback.Good Then
            MessageBox.Show(IdiomaMensajes(298, Mensaje.Texto))
        Else
            MessageBox.Show(IdiomaMensajes(299, Mensaje.Texto))
        End If
    End Sub


    Protected Overridable Sub Init()
        Try
            Capturer = New DPFP.Capture.Capture()

            If (Not Capturer Is Nothing) Then
                Capturer.EventHandler = Me ' Subscribe for capturing events.
            Else
                MessageBox.Show(IdiomaMensajes(300, Mensaje.Texto))
            End If
        Catch ex As Exception
            MessageBox.Show(IdiomaMensajes(300, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Verificator = New DPFP.Verification.Verification()
    End Sub


    Protected Sub StartCapture()
        If (Not Capturer Is Nothing) Then
            Try
                Capturer.StartCapture()
            Catch ex As Exception
                MessageBox.Show(IdiomaMensajes(300, Mensaje.Texto))
            End Try
        End If
    End Sub

    Protected Sub StopCapture()
        If (Not Capturer Is Nothing) Then
            Try
                Capturer.StopCapture()
            Catch ex As Exception
                MessageBox.Show(IdiomaMensajes(301, Mensaje.Texto))
            End Try
        End If
    End Sub

    Protected Function ConvertSampleToBitmap(ByVal Sample As DPFP.Sample) As Bitmap
        Dim convertor As New DPFP.Capture.SampleConversion()  ' Create a sample convertor.
        Dim bitmap As Bitmap = Nothing              ' TODO: the size doesn't matter
        convertor.ConvertToPicture(Sample, bitmap)        ' TODO: return bitmap as a result
        Return bitmap
    End Function

    Protected Overridable Sub Process(ByVal Sample As DPFP.Sample)
        DrawPicture(ConvertSampleToBitmap(Sample))

        ' Process the sample and create a feature set for the enrollment purpose.
        Dim features As DPFP.FeatureSet = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Verification)

        ' Check quality of the sample and start verification if it's good
        If Not features Is Nothing Then
            ' Compare the feature set with our template
            Dim resultado As DPFP.Verification.Verification.Result = New DPFP.Verification.Verification.Result()
            Dim template As DPFP.Template = New DPFP.Template
            Dim sCnn As String
            sCnn = conexion

            Try
                If ComboBoxEdit1.SelectedIndex = -1 Then
                    MsgBox(IdiomaMensajes(865, Mensaje.Texto))
                    Exit Sub

                End If
                Dim oInfo As cInfoCombo
                oInfo = CType(ComboBoxEdit1.SelectedItem, cInfoCombo)
                Dim sSel As String = String.Format("select numero,nombre,huella from empleado where numero='{0}' and huella is not null", oInfo.ID)

                Dim da As SqlDataAdapter
                Dim dt As DataTable = New DataTable



                da = New SqlDataAdapter(sSel, sCnn)
                da.Fill(dt)
                Dim nombre As String
                Dim verificado As Boolean = False

                For Each row As DataRow In dt.Rows
                    Dim huella As Byte() = Nothing
                    huella = CType(row(2), Byte())
                    'esta dato se trae de la BD 
                    template.DeSerialize(huella)
                    Try
                        Verificator.Verify(features, template, resultado)
                        If resultado.Verified Then
                            verificado = True
                            nombre = row("nombre")
                            Exit For
                        End If
                    Catch ex As Exception

                    End Try

                Next

                If verificado Then
                    RegistraEvento(oInfo)
                    'MessageBox.Show("hola!" & nombre)
                Else
                    MessageBox.Show(IdiomaMensajes(302, Mensaje.Texto))
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub DrawPicture(ByVal bmp)
        Invoke(New FunctionCall(AddressOf _DrawPicture), bmp)
    End Sub

    Private Sub _DrawPicture(ByVal bmp)
        Picture.Image = New Bitmap(bmp, Picture.Size)
    End Sub

    Protected Function ExtractFeatures(ByVal Sample As DPFP.Sample, ByVal Purpose As DPFP.Processing.DataPurpose) As DPFP.FeatureSet
        Dim extractor As New DPFP.Processing.FeatureExtraction()    ' Create a feature extractor
        Dim feedback As DPFP.Capture.CaptureFeedback = DPFP.Capture.CaptureFeedback.None
        Dim features As New DPFP.FeatureSet()
        extractor.CreateFeatureSet(Sample, Purpose, feedback, features) ' TODO: return features as a result?
        If (feedback = DPFP.Capture.CaptureFeedback.Good) Then
            Return features
        Else
            Return Nothing
        End If
    End Function

    Private Function RegistraEvento(ByVal oInfo As cInfoCombo) As Integer
        Dim FechaSql As String
        Dim query1, qryins, origen, tipo As String
        Dim qry As String
        origen = "1"
        qry = "SELECT CONVERT(CHAR(8), GETDATE() , 112) as Fecha"
        Dim ds As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
        ds.Read()
        FechaSql = ds("fecha")
        ds.Close()

        '        query1 = "SELECT     empleado.NOMBRE,  MAX(logdia.Fecha) AS ultimaoperacion,  " & _
        '" logdia.Tienda AS Tienda, logdia.Tipo AS tipo " & _
        '" FROM         empleado INNER JOIN " & _
        '                      " logdia ON logdia.nombre = empleado.nombre AND  " & _
        '                       " CONVERT(CHAR(8), logdia.fecha , 112) = '" + Format(FechaSql, "yyyyMMdd") + "' " & _
        '" WHERE     (empleado.nombre = '" + oInfo.Text.ToString.Trim + "')  " & _
        '" GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda " & _
        '" ORDER BY  ultimaoperacion desc"
        'query1 = "SELECT empleado.NOMBRE, MAX(logdia.Fecha) AS ultimaoperacion, " & _
        '             "logdia.Tienda AS Tienda, logdia.Tipo AS tipo " & _
        '             " FROM empleado INNER JOIN logdia ON logdia.nombre = empleado.nombre AND  " & _
        '             " CONVERT(nCHAR(8), logdia.fecha , 112) = '" + Format(FechaSql, "yyyyMMdd") + "' " & _
        '             " WHERE     (empleado.nombre = '" & CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.Trim & "')  " & _
        '             " GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda " & _
        '             " ORDER BY  ultimaoperacion desc"
        query1 = "SELECT TOP 1 empleado.NOMBRE, MAX(logdia.FECHA) AS ultimaoperacion, logdia.TIENDA AS Tienda, logdia.TIPO AS tipo, tiendas.HoraAp, " & _
                     "tiendas.HoraCie, tiendas.Tolerancia, Tiendas.ToleraComida, Tiendas.TiempoComida, Tiendas.Retardo, empleado.HoraEnt, empleado.HoraSal " & _
                     "FROM empleado INNER JOIN logdia ON logdia.NOMBRE = empleado.NOMBRE AND CONVERT(nCHAR(8), logdia.FECHA, 112) = '" + _
                     Format(FechaSql, "yyyyMMdd") + "' INNER JOIN tiendas ON logdia.TIENDA = tiendas.ID WHERE  (empleado.NOMBRE = '" & _
                     CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.Trim & "') GROUP BY empleado.NOMBRE, logdia.TIPO, logdia.TIENDA, tiendas.HoraAp, " & _
                     "tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, empleado.HoraEnt, empleado.HoraSal " & _
                     "ORDER BY ultimaoperacion DESC"
        Dim thedate As Date
        Dim cfecha As String
        qry = "Select getDate() as FechaHora "
        Dim drf As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
        drf.Read()
        thedate = drf("FechaHora")
        drf.Close()
        cfecha = thedate.ToString("yyyyMMdd")
        Dim chora As String = thedate.ToString("HH:mm:ss") ' drf("FechaHora").ToString.Trim.Substring(11, 10)
        Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexionLocal, query1)
        'Dim bTieneRenglones As Boolean = False
        If ds2.HasRows Then
            While ds2.Read()
                If ds2("Tienda").ToString.Trim = My.Settings.Item("TiendaActual") Then

                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        'hago salida
                        tipo = "SALIDA"
                        qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave, autoriza) values ('" & _
                         oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                         oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), 0)"
                        bdBase.bdExecute(conexionLocal, qryins)
                        MessageBox.Show(IdiomaMensajes(303, Mensaje.Texto), IdiomaMensajes(303, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Me.Close()
                        Exit While
                    Else
                        Dim idSuper As Integer = 0
                        Dim ret As Integer = 0
                        If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                            Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                            ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                        End If
                        Select Case ret
                            Case 0
                                tipo = "ENTRADA"
                            Case 1
                                tipo = "ENTRADA R"
                            Case 2
                                Dim myAdmin As New Administradores
                                myAdmin.ShowDialog()
                                If myAdmin.Verificado Then
                                    tipo = "ENTRADA A"
                                    idSuper = myAdmin.idSuper
                                End If
                            Case Else
                                tipo = ""
                        End Select
                        If Not String.IsNullOrEmpty(tipo) Then
                            'hago entrada
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave) values ('" & _
                                    oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                                    oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                            bdBase.bdExecute(conexionLocal, qryins)
                            MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.Close()
                            Exit While
                        End If
                    End If
                Else
                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        'Tiene que salir de la tienda origen no hace nada
                        MessageBox.Show(IdiomaMensajes(305, Mensaje.Texto))
                        Me.Close()
                        Exit While
                    Else
                        Dim idSuper As Integer = 0
                        Dim ret As Integer = 0
                        If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                            Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                            ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                        End If
                        Select Case ret
                            Case 0
                                tipo = "ENTRADA"
                            Case 1
                                tipo = "ENTRADA R"
                            Case 2
                                Dim myAdmin As New Administradores
                                myAdmin.ShowDialog()
                                If myAdmin.Verificado Then
                                    tipo = "ENTRADA A"
                                    idSuper = myAdmin.idSuper
                                End If
                            Case Else
                                tipo = ""
                        End Select
                        If Not String.IsNullOrEmpty(tipo) Then
                            'hago entrada en esta tienda
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave) values ('" & _
                             oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                             oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                            bdBase.bdExecute(conexionLocal, qryins)
                            MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)

                            Me.Close()
                            Exit While
                        End If
                    End If
                End If
            End While
        Else

            'If Not bTieneRenglones Then
            Dim idSuper As Integer = 0
            Dim ret As Integer = 0
            Dim sQuery As String = "SELECT empleado.HoraEnt, empleado.HoraSal, tiendas.HoraAp, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, " & _
                                   "tiendas.Retardo FROM empleado CROSS JOIN tiendas WHERE (empleado.id = " & oInfo.ID & ") AND (dbo.tiendas.ID = " & _
                                   My.Settings.Item("TiendaActual") & ")"
            Dim drX As SqlDataReader = bdBase.bdDataReader(conexionLocal, sQuery)
            If Not drX Is Nothing Then
                If drX.HasRows Then
                    drX.Read()
                    If Not IsDBNull(drX("HoraAp")) And Not IsDBNull(drX("Tolerancia")) And Not IsDBNull(drX("Retardo")) And Not IsDBNull(drX("TiempoComida")) And _
                                    Not IsDBNull(drX("HoraEnt")) And Not IsDBNull(drX("HoraSal")) And Not IsDBNull(drX("ToleraComida")) Then
                        ret = TipoEntrada(drX("HoraAp"), drX("HoraEnt"), drX("HoraSal"), drX("Tolerancia"), drX("Retardo"), drX("TiempoComida"), drX("ToleraComida"), thedate)
                    End If
                End If
                drX.Close()
            End If
            Select Case ret
                Case 0
                    tipo = "ENTRADA"
                Case 1
                    tipo = "ENTRADA R"
                Case 2
                    Dim myAdmin As New Administradores
                    myAdmin.ShowDialog()
                    If myAdmin.Verificado Then
                        tipo = "ENTRADA A"
                        idSuper = myAdmin.idSuper
                    End If
                Case Else
                    tipo = ""
            End Select
            If Not String.IsNullOrEmpty(tipo) Then
                ' no tiene registros este día en ningun lado, es entrada a la tienda actual
                qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja, llave) values ('" & _
                         oInfo.Text.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', '" & _
                         oInfo.ID & "', '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                bdBase.bdExecute(conexionLocal, qryins)
                MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
            End If
        End If
        ds2.Close()
        Return 0
    End Function

    Private Sub Asistencia_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        StopCapture()
    End Sub

    Private Sub Asistencia_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        StopCapture()
    End Sub

    Private Sub Picture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Picture.Click

    End Sub
End Class