﻿Imports System.Data.SqlClient

Public Class loginEmp
    Private Logueado As Boolean = False

    Public ReadOnly Property pLogueado As Boolean
        Get
            Return Logueado
        End Get
    End Property

    Private Sub loginEmp_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
    End Sub

    Private Sub OK_Click(sender As System.Object, e As System.EventArgs) Handles OK.Click
        If String.IsNullOrWhiteSpace(DxErrorProvider1.GetError(UsernameTextBox)) Then
            If String.IsNullOrWhiteSpace(DxErrorProvider1.GetError(PasswordTextBox)) Then
                If UsernameTextBox.Text.Trim.ToUpper = "SAZ" Then
                    Dim des As New cTripleDES
                    Dim drS As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), "SELECT admin, rptUser, rptClave FROM configuracion")
                    If Not drS Is Nothing Then
                        If drS.HasRows Then
                            drS.Read()
                            If PasswordTextBox.Text.Trim.ToUpper = drS("admin").ToString.ToUpper Then
                                Logueado = True
                                Me.Close()
                            Else
                                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(666, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else
                            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(667, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                        drS.Close()
                    Else
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(668, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else

                End If
            Else
                PasswordTextBox.Focus()
            End If
        Else
            UsernameTextBox.Focus()
        End If
    End Sub

    Private Sub PasswordTextBox_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles PasswordTextBox.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.Chr(13) Or (e.KeyChar = Microsoft.VisualBasic.Chr(9)) Then
            OK.PerformClick()
        End If
    End Sub
End Class