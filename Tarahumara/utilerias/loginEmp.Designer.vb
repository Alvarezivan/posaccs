﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class loginEmp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.OK = New DevExpress.XtraEditors.SimpleButton()
        Me.UsernameTextBox = New DevExpress.XtraEditors.TextEdit()
        Me.PasswordTextBox = New DevExpress.XtraEditors.TextEdit()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.DxErrorProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(Me.components)
        CType(Me.UsernameTextBox.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PasswordTextBox.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(12, 4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(36, 13)
        Me.LabelControl2.TabIndex = 14
        Me.LabelControl2.Text = "Usuario"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(8, 39)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(56, 13)
        Me.LabelControl3.TabIndex = 16
        Me.LabelControl3.Text = "Contraseña"
        '
        'OK
        '
        Me.OK.Location = New System.Drawing.Point(106, 19)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(126, 59)
        Me.OK.TabIndex = 18
        Me.OK.Text = "Identificarse"
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.EditValue = ""
        Me.UsernameTextBox.Location = New System.Drawing.Point(8, 19)
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.UsernameTextBox.Properties.MaxLength = 15
        Me.UsernameTextBox.Size = New System.Drawing.Size(92, 20)
        Me.UsernameTextBox.TabIndex = 15
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.EditValue = ""
        Me.PasswordTextBox.Location = New System.Drawing.Point(8, 58)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.Properties.MaxLength = 25
        Me.PasswordTextBox.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.PasswordTextBox.Size = New System.Drawing.Size(92, 20)
        Me.PasswordTextBox.TabIndex = 17
        '
        'DxErrorProvider1
        '
        Me.DxErrorProvider1.ContainerControl = Me
        '
        'loginEmp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(241, 83)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.UsernameTextBox)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "loginEmp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Empleado login"
        CType(Me.UsernameTextBox.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PasswordTextBox.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents OK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents UsernameTextBox As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PasswordTextBox As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents DxErrorProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider
End Class
