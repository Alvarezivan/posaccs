﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Drawing2D

Public Class TipoVenta

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles btnVCM.Click
        Movimiento = 1
        Dim myForm2 As New Vender
        myForm2.Show()
        Me.Close()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnVCC.Click
        Movimiento = 2
        Dim myForm2 As New Vender
        myForm2.Show()
        Me.Close()
    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles btnAC.Click
        Dim myForm2 As New Creditos
        myForm2.Show()
        Me.Close()
    End Sub

    Private Sub TipoVenta_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles btnVCM.KeyDown
        FuncKeysModule(e.KeyCode)
    End Sub
    Private Sub FuncKeysModule(ByVal value As Keys)
        Select Case value
            Case Keys.F1
                btnVCM.PerformClick()
                Exit Select
            Case Keys.F2
                btnVCC.PerformClick()
                Exit Select
            Case Keys.F3
                btnVCRC.PerformClick()
                Exit Select
            Case Keys.F4
                btnAC.PerformClick()
                Exit Select
            Case Keys.F5
                btnAPA.PerformClick()
                Exit Select
        End Select
    End Sub

    Private Sub TipoVenta_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        Me.Focus()
    End Sub

    Private Sub btnAPA_Click(sender As System.Object, e As System.EventArgs) Handles btnAPA.Click
        Dim myForm2 As New CtrlApartados
        myForm2.Show()
        Me.Close()
    End Sub

    Private Sub btnVCRC_Click(sender As System.Object, e As System.EventArgs) Handles btnVCRC.Click
        Movimiento = 3
        Dim myForm2 As New Vender
        myForm2.Show()
        Me.Close()
    End Sub
End Class