﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TipoVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAPA = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAC = New DevExpress.XtraEditors.SimpleButton()
        Me.btnVCRC = New DevExpress.XtraEditors.SimpleButton()
        Me.btnVCC = New DevExpress.XtraEditors.SimpleButton()
        Me.btnVCM = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'btnAPA
        '
        Me.btnAPA.Location = New System.Drawing.Point(49, 273)
        Me.btnAPA.Name = "btnAPA"
        Me.btnAPA.Size = New System.Drawing.Size(185, 57)
        Me.btnAPA.TabIndex = 9
        Me.btnAPA.Text = "A&partar (F5)"
        '
        'btnAC
        '
        Me.btnAC.Location = New System.Drawing.Point(49, 210)
        Me.btnAC.Name = "btnAC"
        Me.btnAC.Size = New System.Drawing.Size(185, 57)
        Me.btnAC.TabIndex = 8
        Me.btnAC.Text = "&Abono Credito (F4)"
        '
        'btnVCRC
        '
        Me.btnVCRC.Location = New System.Drawing.Point(49, 147)
        Me.btnVCRC.Name = "btnVCRC"
        Me.btnVCRC.Size = New System.Drawing.Size(185, 57)
        Me.btnVCRC.TabIndex = 7
        Me.btnVCRC.Text = "Venta &Credito Cliente (F3)"
        '
        'btnVCC
        '
        Me.btnVCC.Location = New System.Drawing.Point(49, 84)
        Me.btnVCC.Name = "btnVCC"
        Me.btnVCC.Size = New System.Drawing.Size(185, 57)
        Me.btnVCC.TabIndex = 6
        Me.btnVCC.Text = "V&enta Contado Cliente (F2)"
        '
        'btnVCM
        '
        Me.btnVCM.Location = New System.Drawing.Point(49, 21)
        Me.btnVCM.Name = "btnVCM"
        Me.btnVCM.Size = New System.Drawing.Size(185, 57)
        Me.btnVCM.TabIndex = 5
        Me.btnVCM.Text = "&Venta Contado Mostrador (F1)"
        '
        'TipoVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 349)
        Me.Controls.Add(Me.btnAPA)
        Me.Controls.Add(Me.btnAC)
        Me.Controls.Add(Me.btnVCRC)
        Me.Controls.Add(Me.btnVCC)
        Me.Controls.Add(Me.btnVCM)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "TipoVenta"
        Me.Text = "Tipo Movimiento"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAPA As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAC As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVCRC As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVCC As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnVCM As DevExpress.XtraEditors.SimpleButton
End Class
