﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cambiaPass
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txActual = New DevExpress.XtraEditors.TextEdit()
        Me.txNueva = New DevExpress.XtraEditors.TextEdit()
        Me.txConfirma = New DevExpress.XtraEditors.TextEdit()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.DxErrorProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(Me.components)
        CType(Me.txActual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNueva.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txConfirma.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(42, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(254, 40)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Su clave ha expirado. Por motivos de seguridad, debe cambiarla en este momento."
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(92, 94)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Clave Actual"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(92, 139)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "Nueva Clave"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(35, 183)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(118, 13)
        Me.LabelControl4.TabIndex = 3
        Me.LabelControl4.Text = "Confirme la Nueva Clave"
        '
        'txActual
        '
        Me.txActual.Location = New System.Drawing.Point(159, 91)
        Me.txActual.Name = "txActual"
        Me.txActual.Properties.MaxLength = 50
        Me.txActual.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txActual.Size = New System.Drawing.Size(139, 20)
        Me.txActual.TabIndex = 4
        '
        'txNueva
        '
        Me.txNueva.Location = New System.Drawing.Point(159, 136)
        Me.txNueva.Name = "txNueva"
        Me.txNueva.Properties.MaxLength = 50
        Me.txNueva.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txNueva.Size = New System.Drawing.Size(139, 20)
        Me.txNueva.TabIndex = 5
        '
        'txConfirma
        '
        Me.txConfirma.Location = New System.Drawing.Point(159, 180)
        Me.txConfirma.Name = "txConfirma"
        Me.txConfirma.Properties.MaxLength = 50
        Me.txConfirma.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txConfirma.Size = New System.Drawing.Size(139, 20)
        Me.txConfirma.TabIndex = 6
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(201, 227)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(97, 23)
        Me.btGuardar.TabIndex = 7
        Me.btGuardar.Text = "Guardar"
        '
        'DxErrorProvider1
        '
        Me.DxErrorProvider1.ContainerControl = Me
        '
        'cambiaPass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(337, 273)
        Me.Controls.Add(Me.btGuardar)
        Me.Controls.Add(Me.txConfirma)
        Me.Controls.Add(Me.txNueva)
        Me.Controls.Add(Me.txActual)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "cambiaPass"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambio de clave"
        CType(Me.txActual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNueva.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txConfirma.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txActual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txNueva As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txConfirma As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DxErrorProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider
End Class
