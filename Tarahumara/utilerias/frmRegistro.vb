﻿Imports System.Data.SqlClient

Public Class frmRegistro
    Implements DPFP.Capture.EventHandler
    '  Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    'oConfig.pBase & ";Persist Security Info=True;User ID=" & oConfig.pUsuario & _
    '";password=" & oConfig.pClave
    Private Enroller As DPFP.Processing.Enrollment

    Private Empleado As Integer = -1
    Public Property pEmpleado As Integer
        Get
            Return Empleado
        End Get
        Set(value As Integer)
            Empleado = value
        End Set
    End Property

    Protected Overrides Sub Init()
        MyBase.Init()
        Enroller = New DPFP.Processing.Enrollment()     ' Create an enrollment.
    End Sub

    Protected Overrides Sub Process(ByVal Sample As DPFP.Sample)
        MyBase.Process(Sample)
        Dim features As DPFP.FeatureSet = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Enrollment)
        If (Not features Is Nothing) Then
            Try
                Enroller.AddFeatures(features)
            Finally
                Select Case Enroller.TemplateStatus
                    Case DPFP.Processing.Enrollment.Status.Ready
                        OnTemplate(Enroller.Template)
                        StopCapture()
                        Enroller.Clear()
                        StartCapture()
                    Case DPFP.Processing.Enrollment.Status.Failed
                        Enroller.Clear()
                        StopCapture()
                        OnTemplate(Nothing)
                        StartCapture()
                End Select
            End Try
        End If
    End Sub


    Private Sub OnTemplate(ByVal template)
        Invoke(New FunctionCall(AddressOf _OnTemplate), template)
    End Sub

    Private Sub _OnTemplate(ByVal template)
        If Not template Is Nothing Then
            Dim bytes As Byte()

            Enroller.Template.Serialize(bytes)

            Dim strCon As String = conexion
            Using CN As New SqlConnection(strCon)
                CN.Open()
                Dim idemp As String = CType(ComboBoxEdit1.SelectedItem, cInfoCombo).ID
                Dim qry As String = "update empleado set huella=@ImageData where numero='" & idemp & "'"

                Dim SqlCom As New SqlCommand(qry, CN)

                SqlCom.Parameters.AddWithValue("@ImageData", bytes)


                SqlCom.ExecuteNonQuery()

                CN.Close()
                MessageBox.Show(IdiomaMensajes(664, Mensaje.Texto))
            End Using
        Else
            MessageBox.Show(IdiomaMensajes(665, Mensaje.Texto), "Fingerprint Enrollment")
        End If
    End Sub

    Private Sub frmRegistro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        LlenaCombobox(ComboBoxEdit1, "Select numero,Nombre   from empleado   order by nombre", "nombre", "numero", conexion)
        ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor

        If Empleado <> -1 Then BuscaEnComboDx(ComboBoxEdit1, Empleado, True)
    End Sub
End Class