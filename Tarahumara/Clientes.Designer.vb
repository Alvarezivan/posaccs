﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Clientes
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Clientes))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.Foto = New DevExpress.XtraEditors.GroupControl()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.tbrefcn2 = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.tbrefct2 = New System.Windows.Forms.TextBox()
        Me.tbrefcn1 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.tbrefct1 = New System.Windows.Forms.TextBox()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.tbrefn3 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbreft3 = New System.Windows.Forms.TextBox()
        Me.tbrefn2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbreft2 = New System.Windows.Forms.TextBox()
        Me.tbrefn1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbreft1 = New System.Windows.Forms.TextBox()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.chkae = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.tbcomentarios = New DevExpress.XtraEditors.MemoEdit()
        Me.tbingresos = New DevExpress.XtraEditors.TextEdit()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.ChkEstatusSocio = New DevExpress.XtraEditors.CheckEdit()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.tbinteres = New DevExpress.XtraEditors.TextEdit()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.tbdesc = New DevExpress.XtraEditors.TextEdit()
        Me.tbplazodias = New DevExpress.XtraEditors.TextEdit()
        Me.tblimite = New DevExpress.XtraEditors.TextEdit()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.bguardarsub = New System.Windows.Forms.Button()
        Me.chksubactivo = New DevExpress.XtraEditors.CheckEdit()
        Me.tbsublimite = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.tbsubnombre = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dgvsubclientes = New System.Windows.Forms.DataGridView()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.limitecredito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.activo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.dtpmodificacion = New System.Windows.Forms.DateTimePicker()
        Me.dtpalta = New System.Windows.Forms.DateTimePicker()
        Me.tbmontocontado = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.tbcalificacion = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbpagareve = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbpagarepa = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbpagarevi = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbmontocredito = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.tbnombre = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.tbcalle = New System.Windows.Forms.TextBox()
        Me.tbnumero = New System.Windows.Forms.TextBox()
        Me.dtpnacimiento = New System.Windows.Forms.DateTimePicker()
        Me.tbnumeroint = New System.Windows.Forms.TextBox()
        Me.tbcolonia = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.chkpromociones = New System.Windows.Forms.CheckBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.tbtelefono = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.tbemail = New System.Windows.Forms.TextBox()
        Me.tbciudad = New System.Windows.Forms.TextBox()
        Me.tbrfc = New System.Windows.Forms.TextBox()
        Me.tbmunicipio = New System.Windows.Forms.TextBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.tbestado = New System.Windows.Forms.TextBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.tbcp = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.RdgSocioEstatusFiltro = New DevExpress.XtraEditors.RadioGroup()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.Foto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Foto.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.chkae.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcomentarios.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbingresos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChkEstatusSocio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbinteres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbplazodias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tblimite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.chksubactivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvsubclientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.RdgSocioEstatusFiltro.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(964, 675)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.Foto)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl5)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl4)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Controls.Add(Me.DataGridView1)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl8)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl7)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(958, 647)
        Me.XtraTabPage1.Text = "Cliente Avanzado"
        '
        'Foto
        '
        Me.Foto.Controls.Add(Me.Button1)
        Me.Foto.Controls.Add(Me.PictureBox1)
        Me.Foto.Location = New System.Drawing.Point(623, 238)
        Me.Foto.Name = "Foto"
        Me.Foto.Size = New System.Drawing.Size(322, 152)
        Me.Foto.TabIndex = 123
        Me.Foto.Text = "Foto"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(179, 100)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 43)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Foto"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(2, 21)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(169, 129)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.tbrefcn2)
        Me.GroupControl5.Controls.Add(Me.Label23)
        Me.GroupControl5.Controls.Add(Me.Label24)
        Me.GroupControl5.Controls.Add(Me.tbrefct2)
        Me.GroupControl5.Controls.Add(Me.tbrefcn1)
        Me.GroupControl5.Controls.Add(Me.Label25)
        Me.GroupControl5.Controls.Add(Me.Label26)
        Me.GroupControl5.Controls.Add(Me.tbrefct1)
        Me.GroupControl5.Location = New System.Drawing.Point(361, 336)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(249, 127)
        Me.GroupControl5.TabIndex = 123
        Me.GroupControl5.Text = "Referencias Comerciales"
        '
        'tbrefcn2
        '
        Me.tbrefcn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrefcn2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefcn2.Location = New System.Drawing.Point(9, 79)
        Me.tbrefcn2.MaxLength = 50
        Me.tbrefcn2.Name = "tbrefcn2"
        Me.tbrefcn2.Size = New System.Drawing.Size(134, 21)
        Me.tbrefcn2.TabIndex = 93
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(159, 63)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(74, 13)
        Me.Label23.TabIndex = 94
        Me.Label23.Text = "Limite Credito"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(10, 64)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(45, 13)
        Me.Label24.TabIndex = 92
        Me.Label24.Text = "Nombre"
        '
        'tbrefct2
        '
        Me.tbrefct2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefct2.Location = New System.Drawing.Point(154, 78)
        Me.tbrefct2.MaxLength = 10
        Me.tbrefct2.Name = "tbrefct2"
        Me.tbrefct2.Size = New System.Drawing.Size(85, 21)
        Me.tbrefct2.TabIndex = 95
        '
        'tbrefcn1
        '
        Me.tbrefcn1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrefcn1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefcn1.Location = New System.Drawing.Point(9, 40)
        Me.tbrefcn1.MaxLength = 50
        Me.tbrefcn1.Name = "tbrefcn1"
        Me.tbrefcn1.Size = New System.Drawing.Size(134, 21)
        Me.tbrefcn1.TabIndex = 85
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(159, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(74, 13)
        Me.Label25.TabIndex = 90
        Me.Label25.Text = "Limite Credito"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(10, 25)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 13)
        Me.Label26.TabIndex = 84
        Me.Label26.Text = "Nombre "
        '
        'tbrefct1
        '
        Me.tbrefct1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefct1.Location = New System.Drawing.Point(154, 39)
        Me.tbrefct1.MaxLength = 10
        Me.tbrefct1.Name = "tbrefct1"
        Me.tbrefct1.Size = New System.Drawing.Size(85, 21)
        Me.tbrefct1.TabIndex = 91
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.tbrefn3)
        Me.GroupControl4.Controls.Add(Me.Label13)
        Me.GroupControl4.Controls.Add(Me.Label17)
        Me.GroupControl4.Controls.Add(Me.tbreft3)
        Me.GroupControl4.Controls.Add(Me.tbrefn2)
        Me.GroupControl4.Controls.Add(Me.Label9)
        Me.GroupControl4.Controls.Add(Me.Label12)
        Me.GroupControl4.Controls.Add(Me.tbreft2)
        Me.GroupControl4.Controls.Add(Me.tbrefn1)
        Me.GroupControl4.Controls.Add(Me.Label10)
        Me.GroupControl4.Controls.Add(Me.Label11)
        Me.GroupControl4.Controls.Add(Me.tbreft1)
        Me.GroupControl4.Location = New System.Drawing.Point(361, 185)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(249, 145)
        Me.GroupControl4.TabIndex = 122
        Me.GroupControl4.Text = "Referencias Personales"
        '
        'tbrefn3
        '
        Me.tbrefn3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrefn3.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefn3.Location = New System.Drawing.Point(9, 118)
        Me.tbrefn3.MaxLength = 50
        Me.tbrefn3.Name = "tbrefn3"
        Me.tbrefn3.Size = New System.Drawing.Size(134, 21)
        Me.tbrefn3.TabIndex = 97
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(159, 102)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(47, 13)
        Me.Label13.TabIndex = 98
        Me.Label13.Text = "Telefono"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(10, 103)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 96
        Me.Label17.Text = "Nombre"
        '
        'tbreft3
        '
        Me.tbreft3.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbreft3.Location = New System.Drawing.Point(154, 117)
        Me.tbreft3.MaxLength = 10
        Me.tbreft3.Name = "tbreft3"
        Me.tbreft3.Size = New System.Drawing.Size(85, 21)
        Me.tbreft3.TabIndex = 99
        '
        'tbrefn2
        '
        Me.tbrefn2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrefn2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefn2.Location = New System.Drawing.Point(9, 79)
        Me.tbrefn2.MaxLength = 50
        Me.tbrefn2.Name = "tbrefn2"
        Me.tbrefn2.Size = New System.Drawing.Size(134, 21)
        Me.tbrefn2.TabIndex = 93
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(159, 63)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(47, 13)
        Me.Label9.TabIndex = 94
        Me.Label9.Text = "Telefono"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(10, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 92
        Me.Label12.Text = "Nombre"
        '
        'tbreft2
        '
        Me.tbreft2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbreft2.Location = New System.Drawing.Point(154, 78)
        Me.tbreft2.MaxLength = 10
        Me.tbreft2.Name = "tbreft2"
        Me.tbreft2.Size = New System.Drawing.Size(85, 21)
        Me.tbreft2.TabIndex = 95
        '
        'tbrefn1
        '
        Me.tbrefn1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrefn1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrefn1.Location = New System.Drawing.Point(9, 40)
        Me.tbrefn1.MaxLength = 50
        Me.tbrefn1.Name = "tbrefn1"
        Me.tbrefn1.Size = New System.Drawing.Size(134, 21)
        Me.tbrefn1.TabIndex = 85
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(159, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(47, 13)
        Me.Label10.TabIndex = 90
        Me.Label10.Text = "Telefono"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(10, 25)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(47, 13)
        Me.Label11.TabIndex = 84
        Me.Label11.Text = "Nombre "
        '
        'tbreft1
        '
        Me.tbreft1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbreft1.Location = New System.Drawing.Point(154, 39)
        Me.tbreft1.MaxLength = 10
        Me.tbreft1.Name = "tbreft1"
        Me.tbreft1.Size = New System.Drawing.Size(85, 21)
        Me.tbreft1.TabIndex = 91
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.chkae)
        Me.GroupControl3.Controls.Add(Me.TextEdit1)
        Me.GroupControl3.Controls.Add(Me.Label18)
        Me.GroupControl3.Controls.Add(Me.Label28)
        Me.GroupControl3.Controls.Add(Me.tbcomentarios)
        Me.GroupControl3.Controls.Add(Me.tbingresos)
        Me.GroupControl3.Controls.Add(Me.Label27)
        Me.GroupControl3.Controls.Add(Me.ChkEstatusSocio)
        Me.GroupControl3.Controls.Add(Me.Button35)
        Me.GroupControl3.Controls.Add(Me.tbinteres)
        Me.GroupControl3.Controls.Add(Me.Label16)
        Me.GroupControl3.Controls.Add(Me.tbdesc)
        Me.GroupControl3.Controls.Add(Me.tbplazodias)
        Me.GroupControl3.Controls.Add(Me.tblimite)
        Me.GroupControl3.Controls.Add(Me.Label19)
        Me.GroupControl3.Controls.Add(Me.Label20)
        Me.GroupControl3.Controls.Add(Me.Label21)
        Me.GroupControl3.Location = New System.Drawing.Point(9, 477)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(601, 138)
        Me.GroupControl3.TabIndex = 128
        Me.GroupControl3.Text = "Datos del Credito"
        '
        'chkae
        '
        Me.chkae.Location = New System.Drawing.Point(347, 106)
        Me.chkae.Name = "chkae"
        Me.chkae.Properties.Caption = "Activar Efectivo"
        Me.chkae.Size = New System.Drawing.Size(99, 19)
        Me.chkae.TabIndex = 135
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(346, 84)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Mask.EditMask = "c"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit1.Properties.ReadOnly = True
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 134
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(346, 70)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 13)
        Me.Label18.TabIndex = 133
        Me.Label18.Text = "Monto Efectivo"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(10, 82)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(68, 13)
        Me.Label28.TabIndex = 132
        Me.Label28.Text = "Comentarios"
        '
        'tbcomentarios
        '
        Me.tbcomentarios.Location = New System.Drawing.Point(84, 80)
        Me.tbcomentarios.Name = "tbcomentarios"
        Me.tbcomentarios.Size = New System.Drawing.Size(256, 48)
        Me.tbcomentarios.TabIndex = 131
        Me.tbcomentarios.UseOptimizedRendering = True
        '
        'tbingresos
        '
        Me.tbingresos.Location = New System.Drawing.Point(381, 39)
        Me.tbingresos.Name = "tbingresos"
        Me.tbingresos.Properties.Mask.EditMask = "c"
        Me.tbingresos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbingresos.Size = New System.Drawing.Size(100, 20)
        Me.tbingresos.TabIndex = 129
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(381, 25)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(47, 13)
        Me.Label27.TabIndex = 128
        Me.Label27.Text = "Ingresos"
        '
        'ChkEstatusSocio
        '
        Me.ChkEstatusSocio.Location = New System.Drawing.Point(487, 57)
        Me.ChkEstatusSocio.Name = "ChkEstatusSocio"
        Me.ChkEstatusSocio.Properties.Caption = "Activo"
        Me.ChkEstatusSocio.Size = New System.Drawing.Size(56, 19)
        Me.ChkEstatusSocio.TabIndex = 127
        '
        'Button35
        '
        Me.Button35.Enabled = False
        Me.Button35.Location = New System.Drawing.Point(506, 82)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(85, 46)
        Me.Button35.TabIndex = 111
        Me.Button35.Text = "Actualizar"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'tbinteres
        '
        Me.tbinteres.Location = New System.Drawing.Point(107, 39)
        Me.tbinteres.Name = "tbinteres"
        Me.tbinteres.Properties.Mask.EditMask = "P"
        Me.tbinteres.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbinteres.Size = New System.Drawing.Size(65, 20)
        Me.tbinteres.TabIndex = 125
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(112, 25)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(41, 13)
        Me.Label16.TabIndex = 124
        Me.Label16.Text = "Interés"
        '
        'tbdesc
        '
        Me.tbdesc.Location = New System.Drawing.Point(275, 39)
        Me.tbdesc.Name = "tbdesc"
        Me.tbdesc.Properties.Mask.EditMask = "n"
        Me.tbdesc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbdesc.Size = New System.Drawing.Size(100, 20)
        Me.tbdesc.TabIndex = 121
        '
        'tbplazodias
        '
        Me.tbplazodias.Location = New System.Drawing.Point(178, 39)
        Me.tbplazodias.Name = "tbplazodias"
        Me.tbplazodias.Properties.Mask.EditMask = "n0"
        Me.tbplazodias.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbplazodias.Size = New System.Drawing.Size(91, 20)
        Me.tbplazodias.TabIndex = 120
        '
        'tblimite
        '
        Me.tblimite.Location = New System.Drawing.Point(5, 39)
        Me.tblimite.Name = "tblimite"
        Me.tblimite.Properties.Mask.EditMask = "c"
        Me.tblimite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tblimite.Size = New System.Drawing.Size(96, 20)
        Me.tblimite.TabIndex = 119
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(278, 25)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(60, 13)
        Me.Label19.TabIndex = 116
        Me.Label19.Text = "Descuento "
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(181, 25)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(91, 13)
        Me.Label20.TabIndex = 114
        Me.Label20.Text = "Plazo Dias (Ex. Int)"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(5, 25)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 112
        Me.Label21.Text = "Limite Crédito"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.bguardarsub)
        Me.GroupControl2.Controls.Add(Me.chksubactivo)
        Me.GroupControl2.Controls.Add(Me.tbsublimite)
        Me.GroupControl2.Controls.Add(Me.Label31)
        Me.GroupControl2.Controls.Add(Me.tbsubnombre)
        Me.GroupControl2.Controls.Add(Me.Label22)
        Me.GroupControl2.Controls.Add(Me.dgvsubclientes)
        Me.GroupControl2.Location = New System.Drawing.Point(623, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(324, 223)
        Me.GroupControl2.TabIndex = 126
        Me.GroupControl2.Text = "Sub Clientes"
        '
        'bguardarsub
        '
        Me.bguardarsub.Location = New System.Drawing.Point(241, 187)
        Me.bguardarsub.Name = "bguardarsub"
        Me.bguardarsub.Size = New System.Drawing.Size(75, 23)
        Me.bguardarsub.TabIndex = 23
        Me.bguardarsub.Text = "Guardar"
        Me.bguardarsub.UseVisualStyleBackColor = True
        '
        'chksubactivo
        '
        Me.chksubactivo.Location = New System.Drawing.Point(248, 168)
        Me.chksubactivo.Name = "chksubactivo"
        Me.chksubactivo.Properties.Caption = "Activo"
        Me.chksubactivo.Size = New System.Drawing.Size(54, 19)
        Me.chksubactivo.TabIndex = 127
        '
        'tbsublimite
        '
        Me.tbsublimite.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbsublimite.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbsublimite.Location = New System.Drawing.Point(160, 189)
        Me.tbsublimite.MaxLength = 50
        Me.tbsublimite.Name = "tbsublimite"
        Me.tbsublimite.Size = New System.Drawing.Size(71, 21)
        Me.tbsublimite.TabIndex = 125
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(157, 174)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(74, 13)
        Me.Label31.TabIndex = 124
        Me.Label31.Text = "Limite Credito"
        '
        'tbsubnombre
        '
        Me.tbsubnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbsubnombre.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbsubnombre.Location = New System.Drawing.Point(4, 189)
        Me.tbsubnombre.MaxLength = 50
        Me.tbsubnombre.Name = "tbsubnombre"
        Me.tbsubnombre.Size = New System.Drawing.Size(144, 21)
        Me.tbsubnombre.TabIndex = 114
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(5, 174)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(93, 13)
        Me.Label22.TabIndex = 113
        Me.Label22.Text = "Nombre Completo"
        '
        'dgvsubclientes
        '
        Me.dgvsubclientes.AllowUserToAddRows = False
        Me.dgvsubclientes.AllowUserToDeleteRows = False
        Me.dgvsubclientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvsubclientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvsubclientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvsubclientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.id, Me.nombre, Me.limitecredito, Me.activo})
        Me.dgvsubclientes.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvsubclientes.Location = New System.Drawing.Point(2, 21)
        Me.dgvsubclientes.Name = "dgvsubclientes"
        Me.dgvsubclientes.ReadOnly = True
        Me.dgvsubclientes.Size = New System.Drawing.Size(320, 147)
        Me.dgvsubclientes.TabIndex = 123
        '
        'id
        '
        Me.id.HeaderText = "Id"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        Me.id.Width = 42
        '
        'nombre
        '
        Me.nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.nombre.HeaderText = "Nombre"
        Me.nombre.Name = "nombre"
        Me.nombre.ReadOnly = True
        '
        'limitecredito
        '
        Me.limitecredito.HeaderText = "Limite Credito"
        Me.limitecredito.Name = "limitecredito"
        Me.limitecredito.ReadOnly = True
        Me.limitecredito.Width = 97
        '
        'activo
        '
        Me.activo.HeaderText = "Activo"
        Me.activo.Name = "activo"
        Me.activo.ReadOnly = True
        Me.activo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.activo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.activo.Width = 62
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.TextBox1)
        Me.GroupControl1.Controls.Add(Me.Label15)
        Me.GroupControl1.Controls.Add(Me.dtpmodificacion)
        Me.GroupControl1.Controls.Add(Me.dtpalta)
        Me.GroupControl1.Controls.Add(Me.tbmontocontado)
        Me.GroupControl1.Controls.Add(Me.Label29)
        Me.GroupControl1.Controls.Add(Me.tbcalificacion)
        Me.GroupControl1.Controls.Add(Me.Label14)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.Label6)
        Me.GroupControl1.Controls.Add(Me.tbpagareve)
        Me.GroupControl1.Controls.Add(Me.Label5)
        Me.GroupControl1.Controls.Add(Me.tbpagarepa)
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.tbpagarevi)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.tbmontocredito)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Location = New System.Drawing.Point(623, 396)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(322, 244)
        Me.GroupControl1.TabIndex = 125
        Me.GroupControl1.Text = "Historial Cliente"
        '
        'TextBox1
        '
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(181, 115)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 21)
        Me.TextBox1.TabIndex = 146
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(5, 124)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(111, 13)
        Me.Label15.TabIndex = 145
        Me.Label15.Text = "Monto Interes Pagado"
        '
        'dtpmodificacion
        '
        Me.dtpmodificacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpmodificacion.Location = New System.Drawing.Point(182, 142)
        Me.dtpmodificacion.Name = "dtpmodificacion"
        Me.dtpmodificacion.Size = New System.Drawing.Size(99, 21)
        Me.dtpmodificacion.TabIndex = 144
        '
        'dtpalta
        '
        Me.dtpalta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpalta.Location = New System.Drawing.Point(181, 28)
        Me.dtpalta.Name = "dtpalta"
        Me.dtpalta.Size = New System.Drawing.Size(99, 21)
        Me.dtpalta.TabIndex = 113
        '
        'tbmontocontado
        '
        Me.tbmontocontado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmontocontado.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmontocontado.Location = New System.Drawing.Point(181, 89)
        Me.tbmontocontado.MaxLength = 50
        Me.tbmontocontado.Name = "tbmontocontado"
        Me.tbmontocontado.Size = New System.Drawing.Size(100, 21)
        Me.tbmontocontado.TabIndex = 143
        Me.tbmontocontado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(5, 97)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(131, 13)
        Me.Label29.TabIndex = 142
        Me.Label29.Text = "Monto Comprado Contado"
        '
        'tbcalificacion
        '
        Me.tbcalificacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalificacion.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcalificacion.Location = New System.Drawing.Point(181, 169)
        Me.tbcalificacion.MaxLength = 50
        Me.tbcalificacion.Name = "tbcalificacion"
        Me.tbcalificacion.Size = New System.Drawing.Size(100, 21)
        Me.tbcalificacion.TabIndex = 141
        Me.tbcalificacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(5, 177)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(62, 13)
        Me.Label14.TabIndex = 140
        Me.Label14.Text = "Calificacion"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(5, 149)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 13)
        Me.Label7.TabIndex = 138
        Me.Label7.Text = "Ultima modificacion "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(232, 198)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 137
        Me.Label6.Text = "Vencidos"
        '
        'tbpagareve
        '
        Me.tbpagareve.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpagareve.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpagareve.Location = New System.Drawing.Point(228, 215)
        Me.tbpagareve.MaxLength = 50
        Me.tbpagareve.Name = "tbpagareve"
        Me.tbpagareve.Size = New System.Drawing.Size(62, 21)
        Me.tbpagareve.TabIndex = 136
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(166, 197)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 135
        Me.Label5.Text = "Pagados"
        '
        'tbpagarepa
        '
        Me.tbpagarepa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpagarepa.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpagarepa.Location = New System.Drawing.Point(160, 215)
        Me.tbpagarepa.MaxLength = 50
        Me.tbpagarepa.Name = "tbpagarepa"
        Me.tbpagarepa.Size = New System.Drawing.Size(62, 21)
        Me.tbpagarepa.TabIndex = 134
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(101, 198)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 133
        Me.Label4.Text = "Vigente"
        '
        'tbpagarevi
        '
        Me.tbpagarevi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbpagarevi.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbpagarevi.Location = New System.Drawing.Point(92, 215)
        Me.tbpagarevi.MaxLength = 50
        Me.tbpagarevi.Name = "tbpagarevi"
        Me.tbpagarevi.Size = New System.Drawing.Size(62, 21)
        Me.tbpagarevi.TabIndex = 132
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 223)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 131
        Me.Label3.Text = "Pagares"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 129
        Me.Label2.Text = "Fecha Alta "
        '
        'tbmontocredito
        '
        Me.tbmontocredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmontocredito.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmontocredito.Location = New System.Drawing.Point(181, 60)
        Me.tbmontocredito.MaxLength = 50
        Me.tbmontocredito.Name = "tbmontocredito"
        Me.tbmontocredito.Size = New System.Drawing.Size(100, 21)
        Me.tbmontocredito.TabIndex = 128
        Me.tbmontocredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 13)
        Me.Label1.TabIndex = 128
        Me.Label1.Text = "Monto Comprado a Credito"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(9, 97)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(601, 80)
        Me.DataGridView1.TabIndex = 122
        '
        'GroupControl8
        '
        Me.GroupControl8.Controls.Add(Me.Label30)
        Me.GroupControl8.Controls.Add(Me.tbid)
        Me.GroupControl8.Controls.Add(Me.tbnombre)
        Me.GroupControl8.Controls.Add(Me.Label45)
        Me.GroupControl8.Controls.Add(Me.tbcalle)
        Me.GroupControl8.Controls.Add(Me.tbnumero)
        Me.GroupControl8.Controls.Add(Me.dtpnacimiento)
        Me.GroupControl8.Controls.Add(Me.tbnumeroint)
        Me.GroupControl8.Controls.Add(Me.tbcolonia)
        Me.GroupControl8.Controls.Add(Me.Label46)
        Me.GroupControl8.Controls.Add(Me.Label57)
        Me.GroupControl8.Controls.Add(Me.Label47)
        Me.GroupControl8.Controls.Add(Me.Label56)
        Me.GroupControl8.Controls.Add(Me.Label48)
        Me.GroupControl8.Controls.Add(Me.chkpromociones)
        Me.GroupControl8.Controls.Add(Me.Label55)
        Me.GroupControl8.Controls.Add(Me.tbtelefono)
        Me.GroupControl8.Controls.Add(Me.Label54)
        Me.GroupControl8.Controls.Add(Me.tbemail)
        Me.GroupControl8.Controls.Add(Me.tbciudad)
        Me.GroupControl8.Controls.Add(Me.tbrfc)
        Me.GroupControl8.Controls.Add(Me.tbmunicipio)
        Me.GroupControl8.Controls.Add(Me.Label49)
        Me.GroupControl8.Controls.Add(Me.tbestado)
        Me.GroupControl8.Controls.Add(Me.Label50)
        Me.GroupControl8.Controls.Add(Me.tbcp)
        Me.GroupControl8.Controls.Add(Me.Label51)
        Me.GroupControl8.Controls.Add(Me.Label53)
        Me.GroupControl8.Controls.Add(Me.Label52)
        Me.GroupControl8.Location = New System.Drawing.Point(9, 184)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(345, 279)
        Me.GroupControl8.TabIndex = 121
        Me.GroupControl8.Text = "Datos Generales"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(203, 196)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(65, 13)
        Me.Label30.TabIndex = 111
        Me.Label30.Text = "Num Cliente"
        '
        'tbid
        '
        Me.tbid.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbid.Location = New System.Drawing.Point(206, 212)
        Me.tbid.MaxLength = 5
        Me.tbid.Name = "tbid"
        Me.tbid.ReadOnly = True
        Me.tbid.Size = New System.Drawing.Size(85, 21)
        Me.tbid.TabIndex = 112
        '
        'tbnombre
        '
        Me.tbnombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnombre.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnombre.Location = New System.Drawing.Point(9, 40)
        Me.tbnombre.MaxLength = 50
        Me.tbnombre.Name = "tbnombre"
        Me.tbnombre.Size = New System.Drawing.Size(178, 21)
        Me.tbnombre.TabIndex = 85
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(8, 235)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(62, 13)
        Me.Label45.TabIndex = 92
        Me.Label45.Text = "Nacimiento"
        '
        'tbcalle
        '
        Me.tbcalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalle.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcalle.Location = New System.Drawing.Point(193, 40)
        Me.tbcalle.MaxLength = 50
        Me.tbcalle.Name = "tbcalle"
        Me.tbcalle.Size = New System.Drawing.Size(141, 21)
        Me.tbcalle.TabIndex = 87
        '
        'tbnumero
        '
        Me.tbnumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnumero.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnumero.Location = New System.Drawing.Point(9, 83)
        Me.tbnumero.MaxLength = 10
        Me.tbnumero.Name = "tbnumero"
        Me.tbnumero.Size = New System.Drawing.Size(62, 21)
        Me.tbnumero.TabIndex = 89
        '
        'dtpnacimiento
        '
        Me.dtpnacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpnacimiento.Location = New System.Drawing.Point(9, 251)
        Me.dtpnacimiento.Name = "dtpnacimiento"
        Me.dtpnacimiento.Size = New System.Drawing.Size(99, 21)
        Me.dtpnacimiento.TabIndex = 93
        '
        'tbnumeroint
        '
        Me.tbnumeroint.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnumeroint.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbnumeroint.Location = New System.Drawing.Point(77, 83)
        Me.tbnumeroint.MaxLength = 10
        Me.tbnumeroint.Name = "tbnumeroint"
        Me.tbnumeroint.Size = New System.Drawing.Size(67, 21)
        Me.tbnumeroint.TabIndex = 95
        '
        'tbcolonia
        '
        Me.tbcolonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcolonia.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcolonia.Location = New System.Drawing.Point(158, 83)
        Me.tbcolonia.MaxLength = 50
        Me.tbcolonia.Name = "tbcolonia"
        Me.tbcolonia.Size = New System.Drawing.Size(176, 21)
        Me.tbcolonia.TabIndex = 97
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(14, 154)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(47, 13)
        Me.Label46.TabIndex = 90
        Me.Label46.Text = "Telefono"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.Location = New System.Drawing.Point(10, 25)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(93, 13)
        Me.Label57.TabIndex = 84
        Me.Label57.Text = "Nombre Completo"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(101, 154)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(33, 13)
        Me.Label47.TabIndex = 108
        Me.Label47.Text = "Email"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.Location = New System.Drawing.Point(193, 25)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(31, 13)
        Me.Label56.TabIndex = 86
        Me.Label56.Text = "Calle"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(12, 198)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(24, 13)
        Me.Label48.TabIndex = 106
        Me.Label48.Text = "RFC"
        '
        'chkpromociones
        '
        Me.chkpromociones.AutoSize = True
        Me.chkpromociones.Checked = True
        Me.chkpromociones.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkpromociones.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkpromociones.Location = New System.Drawing.Point(218, 258)
        Me.chkpromociones.Name = "chkpromociones"
        Me.chkpromociones.Size = New System.Drawing.Size(122, 17)
        Me.chkpromociones.TabIndex = 110
        Me.chkpromociones.Text = "Recibe Promociones"
        Me.chkpromociones.UseVisualStyleBackColor = True
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(7, 68)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(64, 13)
        Me.Label55.TabIndex = 88
        Me.Label55.Text = "Número Ext."
        '
        'tbtelefono
        '
        Me.tbtelefono.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtelefono.Location = New System.Drawing.Point(9, 169)
        Me.tbtelefono.MaxLength = 10
        Me.tbtelefono.Name = "tbtelefono"
        Me.tbtelefono.Size = New System.Drawing.Size(85, 21)
        Me.tbtelefono.TabIndex = 91
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(80, 68)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(63, 13)
        Me.Label54.TabIndex = 94
        Me.Label54.Text = "Número Int."
        '
        'tbemail
        '
        Me.tbemail.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbemail.Location = New System.Drawing.Point(100, 169)
        Me.tbemail.MaxLength = 50
        Me.tbemail.Name = "tbemail"
        Me.tbemail.Size = New System.Drawing.Size(198, 21)
        Me.tbemail.TabIndex = 109
        '
        'tbciudad
        '
        Me.tbciudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbciudad.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbciudad.Location = New System.Drawing.Point(10, 127)
        Me.tbciudad.MaxLength = 50
        Me.tbciudad.Name = "tbciudad"
        Me.tbciudad.Size = New System.Drawing.Size(109, 21)
        Me.tbciudad.TabIndex = 99
        '
        'tbrfc
        '
        Me.tbrfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfc.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfc.Location = New System.Drawing.Point(10, 212)
        Me.tbrfc.MaxLength = 15
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Size = New System.Drawing.Size(85, 21)
        Me.tbrfc.TabIndex = 107
        '
        'tbmunicipio
        '
        Me.tbmunicipio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmunicipio.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbmunicipio.Location = New System.Drawing.Point(125, 127)
        Me.tbmunicipio.MaxLength = 50
        Me.tbmunicipio.Name = "tbmunicipio"
        Me.tbmunicipio.Size = New System.Drawing.Size(118, 21)
        Me.tbmunicipio.TabIndex = 101
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.Location = New System.Drawing.Point(101, 196)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(71, 13)
        Me.Label49.TabIndex = 104
        Me.Label49.Text = "Código Postal"
        '
        'tbestado
        '
        Me.tbestado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbestado.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbestado.Location = New System.Drawing.Point(249, 127)
        Me.tbestado.MaxLength = 50
        Me.tbestado.Name = "tbestado"
        Me.tbestado.Size = New System.Drawing.Size(85, 21)
        Me.tbestado.TabIndex = 103
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(249, 112)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(39, 13)
        Me.Label50.TabIndex = 102
        Me.Label50.Text = "Estado"
        '
        'tbcp
        '
        Me.tbcp.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcp.Location = New System.Drawing.Point(104, 212)
        Me.tbcp.MaxLength = 5
        Me.tbcp.Name = "tbcp"
        Me.tbcp.Size = New System.Drawing.Size(85, 21)
        Me.tbcp.TabIndex = 105
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(125, 112)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(54, 13)
        Me.Label51.TabIndex = 100
        Me.Label51.Text = "Municipio"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(161, 68)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(43, 13)
        Me.Label53.TabIndex = 96
        Me.Label53.Text = "Colonia"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label52.Location = New System.Drawing.Point(15, 112)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(40, 13)
        Me.Label52.TabIndex = 98
        Me.Label52.Text = "Ciudad"
        '
        'GroupControl7
        '
        Me.GroupControl7.Controls.Add(Me.RdgSocioEstatusFiltro)
        Me.GroupControl7.Controls.Add(Me.Button36)
        Me.GroupControl7.Controls.Add(Me.TextBox8)
        Me.GroupControl7.Controls.Add(Me.Label8)
        Me.GroupControl7.Location = New System.Drawing.Point(9, 9)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(601, 82)
        Me.GroupControl7.TabIndex = 120
        Me.GroupControl7.Text = "Búsqueda Individual"
        '
        'RdgSocioEstatusFiltro
        '
        Me.RdgSocioEstatusFiltro.Location = New System.Drawing.Point(296, 24)
        Me.RdgSocioEstatusFiltro.Name = "RdgSocioEstatusFiltro"
        Me.RdgSocioEstatusFiltro.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Activos"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inactivos"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ambos")})
        Me.RdgSocioEstatusFiltro.Size = New System.Drawing.Size(210, 24)
        Me.RdgSocioEstatusFiltro.TabIndex = 22
        '
        'Button36
        '
        Me.Button36.Location = New System.Drawing.Point(296, 50)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(75, 23)
        Me.Button36.TabIndex = 16
        Me.Button36.Text = "Buscar"
        Me.Button36.UseVisualStyleBackColor = True
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(20, 46)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(271, 21)
        Me.TextBox8.TabIndex = 19
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 31)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(103, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Búsqueda Individual"
        '
        'Clientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(964, 675)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Clientes"
        Me.Text = "Clientes"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.Foto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Foto.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.chkae.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcomentarios.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbingresos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChkEstatusSocio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbinteres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbdesc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbplazodias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tblimite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.chksubactivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvsubclientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.RdgSocioEstatusFiltro.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents dgvsubclientes As System.Windows.Forms.DataGridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbcalificacion As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbpagareve As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbpagarepa As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbpagarevi As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbmontocredito As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents tbcalle As System.Windows.Forms.TextBox
    Friend WithEvents tbnumero As System.Windows.Forms.TextBox
    Friend WithEvents dtpnacimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbnumeroint As System.Windows.Forms.TextBox
    Friend WithEvents tbcolonia As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents tbtelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents tbemail As System.Windows.Forms.TextBox
    Friend WithEvents tbciudad As System.Windows.Forms.TextBox
    Friend WithEvents tbrfc As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents tbestado As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents tbcp As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents RdgSocioEstatusFiltro As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbrefn3 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tbreft3 As System.Windows.Forms.TextBox
    Friend WithEvents tbrefn2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbreft2 As System.Windows.Forms.TextBox
    Friend WithEvents tbrefn1 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbreft1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ChkEstatusSocio As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents tbinteres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents tbdesc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbplazodias As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tblimite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents chkpromociones As System.Windows.Forms.CheckBox
    Friend WithEvents tbmunicipio As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents tbrefcn2 As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents tbrefct2 As System.Windows.Forms.TextBox
    Friend WithEvents tbrefcn1 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents tbrefct1 As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents tbcomentarios As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents tbingresos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents tbmontocontado As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents limitecredito As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents activo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dtpmodificacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpalta As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkae As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents bguardarsub As System.Windows.Forms.Button
    Friend WithEvents chksubactivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbsublimite As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents tbsubnombre As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Foto As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
End Class
