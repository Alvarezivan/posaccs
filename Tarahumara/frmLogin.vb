﻿Imports System.IO
Imports System.Data.SqlClient
'Imports Microsoft.Win32
Imports DevExpress.XtraSplashScreen
Imports Updater
Imports System.Net.NetworkInformation
Imports System.Text

'Imports Ionic.Zip
Imports System.Xml
Imports System.Reflection

Public Class frmLogin

#Region "Inicializaciones"

    Private Sub frmLogin_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not oLogin.pLoginStatus Then Application.Exit()
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Bitacora.SaveTextToFile("Inicia frmLogin.Load", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        If Not Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\Saz") Then _
            Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) & "\Saz")
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        Dim dtExcel As New DataTable
        dtExcel.TableName = "Mensajes"
        dtExcel.Columns.Add("Formulario")
        dtExcel.Columns.Add("Numero")
        dtExcel.Columns.Add("Titulo Espanol")
        dtExcel.Columns.Add("Titulo Ingles")
        dtExcel.Columns.Add("Texto Espanol")
        dtExcel.Columns.Add("Texto Ingles")
        Dim ensamblado As Assembly
        ensamblado = Assembly.GetExecutingAssembly()
        Dim xml As Stream
        xml = ensamblado.GetManifestResourceStream("Tarahumara.Mensajes.xml")
        dtExcel.ReadXml(xml)

        dvMensajes = New DataView(dtMensajes)
        If My.Settings.AutoUpdate Then
            Try
                Dim pingSender As New Ping
                Dim options As New PingOptions
                options.DontFragment = True
                Dim data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                Dim buffer() As Byte = Encoding.ASCII.GetBytes(data)
                Dim timeout As Integer = 120
                Dim reply As PingReply = pingSender.Send("merlot.dnsalias.com", timeout, buffer, options)
                If reply.Status = IPStatus.Success Then
                    AutoUpdater.Start("ftp://merlot.dnsalias.com/instaladores/Recordatorios/AppCast.xml", True)
                End If
            Catch ex As Exception
            End Try
        End If
        ' Migrar settings si es necesario
        If My.Settings.UpgradeSettings Then
            My.Settings.Upgrade()
            My.Settings.UpgradeSettings = False
            My.Settings.Save()
        End If
        ' 
        ' Leer datos del registro
        If Len(My.Settings.idEmpresa.ToString.Trim) > 0 Then
            txNoEmpresa.Text = My.Settings.idEmpresa
        Else
            txNoEmpresa.Text = ""
        End If
        UsernameTextBox.Text = My.Settings.ultimoUsuario
        ' Bitacora.SaveTextToFile("Termina frmLogin.Load", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub
#End Region

    'Public Function validaimagen()
    '    ' Bitacora.SaveTextToFile("Inicia validaimagen", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    Dim valid As Boolean
    '    If Not File.Exists("C:\saz\logos\logo.jpg") Then
    '        If DevExpress.XtraEditors.XtraMessageBox.Show("No se encuentra imagen de la empresa desea agregarla ?", "Confirmacion", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
    '            Dim busqueda As New OpenFileDialog
    '            If busqueda.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                Try
    '                    ' Mover el fichero.si existe lo sobreescribe
    '                    My.Computer.FileSystem.CopyFile(busqueda.FileName, "C:\saz\logos\" & "logo.jpg", True)
    '                    ' errores
    '                Catch ex As Exception
    '                    ' MsgBox(ex.Message.ToString, MsgBoxStyle.Critical)
    '                End Try
    '            End If
    '        End If
    '    End If
    '    ' Bitacora.SaveTextToFile("termina validaimagen", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    Return valid
    'End Function

    'Private Sub CreaTablas(conexion As String)
    '    ' Bitacora.SaveTextToFile("Inicia CreaTablas", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    If Not File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\inicial.sql") Then
    '        ' Descomprimir scripts
    '        If Not File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\scripts.zip") Then
    '            OpenFileDialog1.Title = "Seleccione el archivo de scripts"
    '            OpenFileDialog1.InitialDirectory = Application.StartupPath
    '            OpenFileDialog1.ShowDialog()
    '            'OpenFileDialog1.FileName = "scripts.zip"
    '            File.Copy(OpenFileDialog1.FileName, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\scripts.zip")
    '        End If
    '        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\scripts.zip") Then
    '            Using zip As ZipFile = ZipFile.Read(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\scripts.zip")
    '                AddHandler zip.ExtractProgress, New EventHandler(Of ExtractProgressEventArgs)(AddressOf Me.zip_ExtractProgress)
    '                zip.ExtractAll(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz", Ionic.Zip.ExtractExistingFileAction.OverwriteSilently)
    '            End Using
    '            Dim regresa As String = String.Empty
    '            ' Comprobar si hay que ejecutar scripts
    '            Dim sQuery As String = "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleado') " & _
    '                                   "BEGIN SELECT 1 AS Existe END ELSE BEGIN SELECT 0 AS Existe END"
    '            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
    '            If Not IsNothing(dr) Then
    '                If dr.HasRows Then
    '                    dr.Read()
    '                    If dr("Existe") = 0 Then
    '                        lblMensaje.Text = "Ejecutando scripts"
    '                        GroupControl2.Text = "Creando tablas..."
    '                        GroupControl2.Refresh()
    '                        regresa = bdBase.bdExecute(conexion, File.OpenText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\inicial.sql").ReadToEnd(), , , , False)
    '                        If String.IsNullOrWhiteSpace(regresa) Then
    '                            GroupControl2.Text = "Creando datos iniciales..."
    '                            GroupControl2.Refresh()
    '                            regresa = bdBase.bdExecute(conexion, File.OpenText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\datos.sql").ReadToEnd(), , , , False)
    '                            If String.IsNullOrWhiteSpace(regresa) Then
    '                                GroupControl2.Text = "Creando xml compras..."
    '                                GroupControl2.Refresh()
    '                                regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\xmlcompras.sql")
    '                                If String.IsNullOrWhiteSpace(regresa) Then
    '                                    GroupControl2.Text = "Creando funciones..."
    '                                    GroupControl2.Refresh()
    '                                    regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\funcionexisten.sql")
    '                                    If String.IsNullOrWhiteSpace(regresa) Then
    '                                        GroupControl2.Text = "Creando xml compras 2..."
    '                                        GroupControl2.Refresh()
    '                                        regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\xmlcompras2.sql")
    '                                        If String.IsNullOrWhiteSpace(regresa) Then
    '                                            GroupControl2.Text = "Creando xml pedido..."
    '                                            GroupControl2.Refresh()
    '                                            regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\xmlpedido.sql")
    '                                            If String.IsNullOrWhiteSpace(regresa) Then
    '                                                GroupControl2.Text = "Creando xml ventas..."
    '                                                GroupControl2.Refresh()
    '                                                regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\ventasxml.sql")
    '                                                If String.IsNullOrWhiteSpace(regresa) Then
    '                                                    GroupControl2.Text = "Creando datos sepomex..."
    '                                                    GroupControl2.Refresh()
    '                                                    regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\sepomex.sql")
    '                                                    If Not String.IsNullOrWhiteSpace(regresa) Then
    '                                                        BorraScripts()
    '                                                        MessageBox.Show("Error creando datos sepomex, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                                    Else
    '                                                        ' Actualizacion de vistas y SPs
    '                                                        GroupControl2.Text = "Creando vistas..."
    '                                                        GroupControl2.Refresh()
    '                                                        regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\vistas.sql")
    '                                                        If Not String.IsNullOrWhiteSpace(regresa) Then
    '                                                            MessageBox.Show("Error creando vistas iniciales, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                                        End If
    '                                                        GroupControl2.Text = "Creando SPs..."
    '                                                        GroupControl2.Refresh()
    '                                                        regresa = bdBase.bdExecute(conexion, File.OpenText(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\sps.sql").ReadToEnd(), , , , False)
    '                                                        If Not String.IsNullOrWhiteSpace(regresa) Then
    '                                                            MessageBox.Show("Error creando SPs iniciales, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                                        End If
    '                                                        GroupControl2.Text = "Creando triggers..."
    '                                                        GroupControl2.Refresh()
    '                                                        regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\triggers.sql")
    '                                                        If Not String.IsNullOrWhiteSpace(regresa) Then
    '                                                            MessageBox.Show("Error creando triggers, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                                        End If
    '                                                    End If
    '                                                Else
    '                                                    MessageBox.Show("Error creando sp iniciales de venta, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                                End If
    '                                            Else
    '                                                MessageBox.Show("Error creando sp iniciales, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                            End If
    '                                        Else
    '                                            MessageBox.Show("Error creando xm compras2, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                        End If
    '                                    Else
    '                                        MessageBox.Show("Error creando funciones iniciales, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                    End If
    '                                Else
    '                                    MessageBox.Show("Error creando xml compras, por favor contacte a soporte y notifique este error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                                End If
    '                            End If
    '                        End If
    '                        dr.Close()
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Else
    '        If Not File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\ventasxml.sql") Then
    '            ' Elimina scripts
    '            Dim dirE As DirectoryInfo = New DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz")
    '            Dim archivosE() As FileInfo = dirE.GetFiles
    '            For Each archivito As FileInfo In archivosE
    '                If archivito.Extension.ToLower = ".sql" Then File.Delete(archivito.FullName)
    '            Next
    '            ' Descomprime
    '            CreaTablas(conexion)
    '        End If
    '    End If
    '    ' Bitacora.SaveTextToFile("Termina CreaTablas", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    'End Sub

    'Private Sub zip_ExtractProgress(ByVal sender As Object, ByVal e As ExtractProgressEventArgs)
    '    ' Bitacora.SaveTextToFile("Descomprimiendo " & e.CurrentEntry.FileName, Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    If (e.EventType = Ionic.Zip.ZipProgressEventType.Extracting_AfterExtractEntry) Then
    '        GroupControl2.Text = "Descomprimiendo " & e.CurrentEntry.FileName
    '        GroupControl2.Refresh()
    '    ElseIf (e.EventType = ZipProgressEventType.Extracting_BeforeExtractAll) Then
    '        '' do nothing
    '    End If
    'End Sub

#Region "Validaciones"
    Private Sub UsernameTextBox_GotFocus(sender As Object, e As System.EventArgs) Handles UsernameTextBox.GotFocus
        UsernameTextBox.SelectAll()
    End Sub

    Private Sub UsernameTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles UsernameTextBox.Validating
        If Not String.IsNullOrWhiteSpace(UsernameTextBox.Text) Then
            If UsernameTextBox.Text.Trim = "soporte@elsaz.com" Then
                txNoEmpresa.Visible = True
                LabelControl1.Visible = True
            Else
                txNoEmpresa.Visible = False
                LabelControl1.Visible = False
                My.Settings.ultimoUsuario = UsernameTextBox.Text.Trim
                My.Settings.Save()
            End If
            DxErrorProvider1.SetError(UsernameTextBox, "")
        Else
            DxErrorProvider1.SetError(UsernameTextBox, "Debe escribir una dirección de correo electrónico válida")
            UsernameTextBox.Focus()
        End If
    End Sub

    Private Sub PasswordTextBox_GotFocus(sender As Object, e As System.EventArgs) Handles PasswordTextBox.GotFocus
        PasswordTextBox.SelectAll()
    End Sub

    Private Sub PasswordTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles PasswordTextBox.Validating
        If Not String.IsNullOrWhiteSpace(PasswordTextBox.Text) Then
            If PasswordTextBox.Text.Trim.Length < 8 Then
                DxErrorProvider1.SetError(PasswordTextBox, "Su clave debe ser al menos de 8 caracteres")
                PasswordTextBox.Focus()
            Else
                DxErrorProvider1.SetError(PasswordTextBox, "")
            End If
        Else
            DxErrorProvider1.SetError(PasswordTextBox, "Debe escribir su clave")
            'PasswordTextBox.Focus()
        End If
    End Sub

    Private Sub txNoEmpresa_GotFocus(sender As Object, e As System.EventArgs) Handles txNoEmpresa.GotFocus
        txNoEmpresa.SelectAll()
    End Sub

    Private Sub txNoEmpresa_LostFocus(sender As Object, e As System.EventArgs) Handles txNoEmpresa.LostFocus
        My.Settings.idEmpresa = txNoEmpresa.Text
        My.Settings.Save()
    End Sub

    Private Sub txNoEmpresa_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txNoEmpresa.Validating
        If Not String.IsNullOrWhiteSpace(txNoEmpresa.Text) Then
            DxErrorProvider1.SetError(txNoEmpresa, "")
        Else
            DxErrorProvider1.SetError(txNoEmpresa, "Debe escribir un número de empresa")
            txNoEmpresa.Focus()
        End If
    End Sub
#End Region

#Region "Validación Teclas"
    Private Sub UsernameTextBox_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles UsernameTextBox.KeyUp
        Select Case e.KeyCode
            Case Keys.Enter
                PasswordTextBox.Focus()
            Case Keys.F11
                UsernameTextBox.Text = "soporte@elsaz.com"
                'Case Keys.F12
                '    BorraScripts()
                'Case Keys.Shift AndAlso Keys.Delete
                '    MsgBox("hola")
        End Select
    End Sub

    Private Sub PasswordTextBox_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles PasswordTextBox.KeyUp
        Select Case e.KeyCode
            Case Keys.Enter
                If txNoEmpresa.Visible Then txNoEmpresa.Focus() Else Ingresar()
            Case Keys.F11
                UsernameTextBox.Text = "soporte@elsaz.com"
                'Case Keys.F12
                '    BorraScripts()
        End Select
    End Sub

    Private Sub txNoEmpresa_KeyUp(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txNoEmpresa.KeyUp
        Select Case e.KeyCode
            Case Keys.Enter
                Ingresar()
            Case Keys.F11
                UsernameTextBox.Text = "soporte@elsaz.com"
                'Case Keys.F12
                '    BorraScripts()
        End Select
    End Sub

    'Private Sub BorraScripts()
    '    ' Bitacora.SaveTextToFile("Inicia BorraScripts", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    Try
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\inicial.sql")
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\datos.sql")
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\sepomex.sql")
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\vistas.sql")
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\sps.sql")
    '        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\triggers.sql")
    '    Catch ex As Exception
    '    End Try
    '    ' Bitacora.SaveTextToFile("Termina BorraScripts", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    'End Sub
#End Region

    Private Sub OK_Click(sender As System.Object, e As System.EventArgs) Handles OK.Click
        Ingresar()
    End Sub

    Private Sub Ingresar()
        ' Bitacora.SaveTextToFile("Inicia Ingresar", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim mensaje As String = "Preparado"
        If DxErrorProvider1.HasErrors Then
            DevExpress.XtraEditors.XtraMessageBox.Show("Hay errores en sus datos, por favor corrija", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            UsernameTextBox.Focus()
        Else
            Splash(True)
            lblMensaje.Text = "Intentando acceso..."
            ' Proceso de login
            Dim iRegresa As Integer = -9
            If UsernameTextBox.Text.Trim.ToLower = "soporte@elsaz.com" Then
                iRegresa = oLogin.doLoginUser(UsernameTextBox.Text.Trim, PasswordTextBox.Text.Trim, txNoEmpresa.Text.Trim)
            Else
                iRegresa = oLogin.doLoginUser(UsernameTextBox.Text.Trim, PasswordTextBox.Text.Trim)
            End If
            Select Case iRegresa
                Case -9, -1
                    Splash(False)
                    mensaje = oLogin.pMensaje
                    UsernameTextBox.Focus()
                Case -999
                    Ingresar()
                Case Else
                    ' a) sistema activado?
                    'Dim activa As New activacion
                    'mensaje = activa.pMensaje
                    'If activa.pActivo Then
                    oLogin.doLoginEmp(iRegresa)
                    If oLogin.pLoginStatus = True Then
                        'validaimagen()
                        'CreaTablas(conexion)

                        '' Checar tienda y almacen relacionado
                        'If String.IsNullOrWhiteSpace(My.Settings.TiendaActual.ToString.Trim) Then
                        '    ' Llamar a forma para seleccionar ambos
                        '    Dim selecciona As New SelTiendAlm
                        '    selecciona.ShowDialog()
                        '    If selecciona.pSinDatos Then
                        '        SplashScreenManager.CloseForm()
                        '        Exit Sub
                        '    End If
                        '    selecciona = Nothing
                        'Else
                        '    Dim bExisteTienda As Boolean = False
                        '    If Convert.ToInt16(My.Settings.TiendaActual) > 0 Then
                        '        Dim sQuery As String = "Select numero,nombre from Tiendas where Numero = " & My.Settings.TiendaActual
                        '        Dim drTA As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                        '        If Not drTA Is Nothing Then
                        '            If drTA.HasRows Then
                        '                drTA.Read()
                        '                sNombreTienda = drTA("nombre").ToString.Trim
                        '                bExisteTienda = True
                        '            End If
                        '            drTA.Close()
                        '        End If
                        '    Else
                        '        bExisteTienda = False
                        '    End If
                        '    If bExisteTienda Then
                        '        If String.IsNullOrWhiteSpace(My.Settings.almacendevoluciones) Then
                        '            ' Asignar el mismo valor que tenga la tienda
                        '            My.Settings.almacendevoluciones = My.Settings.TiendaActual
                        '            My.Settings.Save()
                        '            Splash(False)
                        '            DevExpress.XtraEditors.XtraMessageBox.Show("Se ha autoasignado el almacén de devoluciones" & vbCrLf & _
                        '                                                       "[" & sNombreTienda & "]", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        '            Splash(True)
                        '        Else
                        '            If My.Settings.almacendevoluciones <= 0 Then
                        '                ' Asignar el mismo valor que tenga la tienda
                        '                My.Settings.almacendevoluciones = My.Settings.TiendaActual
                        '                My.Settings.Save()
                        '                Splash(False)
                        '                DevExpress.XtraEditors.XtraMessageBox.Show("Se ha autoasignado el almacén de devoluciones" & vbCrLf & _
                        '                                                           "[" & sNombreTienda & "]", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        '                Splash(True)
                        '            Else
                        '                Dim sQuery As String = "Select numero,nombre from Tiendas where Numero = " & My.Settings.almacendevoluciones
                        '                Dim drTA As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                        '                If Not drTA Is Nothing Then
                        '                    If Not drTA.HasRows Then
                        '                        ' Asignar el mismo valor que tenga la tienda
                        '                        My.Settings.almacendevoluciones = My.Settings.TiendaActual
                        '                        My.Settings.Save()
                        '                        Splash(False)
                        '                        DevExpress.XtraEditors.XtraMessageBox.Show("Se ha autoasignado el almacén de devoluciones" & vbCrLf & _
                        '                                                                   "[" & sNombreTienda & "]", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        '                        Splash(True)
                        '                    End If
                        '                    drTA.Close()
                        '                End If
                        '            End If
                        '        End If
                        '        If String.IsNullOrWhiteSpace(My.Settings.CajaActual) Then
                        '            My.Settings.CajaActual = 1
                        '            My.Settings.Save()
                        '            Splash(False)
                        '            DevExpress.XtraEditors.XtraMessageBox.Show("Se ha autoasignado la caja [1]", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        '            Splash(True)
                        '        Else
                        '            If Convert.ToInt16(My.Settings.CajaActual) <= 0 Then
                        '                My.Settings.CajaActual = 1
                        '                My.Settings.Save()
                        '                Splash(False)
                        '                DevExpress.XtraEditors.XtraMessageBox.Show("Se ha autoasignado la caja [1]", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        '                Splash(True)
                        '            End If
                        '        End If
                        '    Else
                        '        'SelTiendAlm.ShowDialog()
                        '        Dim selecciona As New SelTiendAlm
                        '        selecciona.ShowDialog()
                        '        If selecciona.pSinDatos Then
                        '            Splash(False)
                        '            Exit Sub
                        '        End If
                        '        selecciona = Nothing
                        '    End If
                        'End If
                        '' Auto asistencia
                        'If Not oLogin.pUsrSuper Then
                        '    If My.Settings.huelladigital = True Then
                        '        Dim frmConfig As New HuellaDigital
                        '        Splash(False)
                        '        frmConfig.ShowDialog()
                        '        frmConfig = Nothing
                        '        Splash(True)
                        '    Else
                        '        Dim FechaSql As String
                        '        Dim query1, qryins, origen, tipo As String
                        '        origen = "1"
                        '        Dim qry As String = "SELECT CONVERT(CHAR(8), GETDATE() , 112) as Fecha"
                        '        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                        '        ds.Read()
                        '        FechaSql = ds("fecha")
                        '        ' " WHERE     (empleado.nombre = '" & oLogin.pUsrNombreC.ToString.Trim & "')  "
                        '        query1 = "SELECT     empleado.NOMBRE,  MAX(logdia.Fecha) AS ultimaoperacion,  " & _
                        '                 " logdia.Tienda AS Tienda, logdia.Tipo AS tipo, logdia.caja " & _
                        '                 " FROM         empleado INNER JOIN " & _
                        '                 " logdia ON logdia.nombre = empleado.nombre AND  " & _
                        '                 " CONVERT(CHAR(8), logdia.fecha , 112) = '" + FechaSql + "' " & _
                        '                 " WHERE     (empleado.numero = '" & oLogin.pUserId & "')  " & _
                        '                 " GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda,logdia.caja " & _
                        '                 " ORDER BY  ultimaoperacion desc"
                        '        Dim thedate As Date
                        '        Dim cfecha As String
                        '        qry = "Select getDate() as FechaHora "
                        '        Dim drf = bdBase.bdDataReader(conexion, qry)
                        '        drf.Read()
                        '        thedate = drf("FechaHora")
                        '        cfecha = thedate.ToString("yyyyMMdd")
                        '        thedate = drf("FechaHora")
                        '        Dim chora As String = thedate.ToString("HH:mm:ss")
                        '        Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexion, query1)
                        '        Try
                        '            ds2.Read()
                        '            If ds2.HasRows Then
                        '                If ds2("Tienda").ToString.Trim = My.Settings.Item("TiendaActual") Then
                        '                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        '                        If ds2("Caja").ToString.Trim <> My.Settings.CajaActual Then
                        '                            'hago salida
                        '                            'tipo = "SALIDA"
                        '                            'qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora) values ('" & _
                        '                            '         oLogin.pUsrNombreC.ToString.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & _
                        '                            '         tipo & "',getdate(),'" & chora & "')"
                        '                            'bdBase.bdExecute(conexion, qryins)
                        '                            MessageBox.Show("No puede accesar usted tiene un acceso hoy en otra tienda ó caja!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                            Application.Exit()
                        '                        End If
                        '                    Else
                        '                        'hago entrada
                        '                        tipo = "ENTRADA"
                        '                        qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja) values ('" & _
                        '                                  oLogin.pUsrNombreC.ToString.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & _
                        '                                  tipo & "',getdate(),'" & chora & "', '" & oLogin.pUserId & "', '" & My.Settings.CajaActual & "')"
                        '                        bdBase.bdExecute(conexion, qryins)
                        '                        Splash(False)
                        '                        MessageBox.Show("Su entrada ha sido registrada!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                    End If
                        '                Else
                        '                    If (ds2("tipo").ToString) = "ENTRADA" Then
                        '                        ''Tiene que salir de la tienda origen 
                        '                        'tipo = "SALIDA"
                        '                        'qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja) values ('" & _
                        '                        '         oLogin.pUsrNombreC.ToString.Trim & "','" & ds2("Tienda") & "','" & origen + "','" & _
                        '                        '         tipo & "',getdate(),'" & chora & "', '" & oLogin.pUserId & "', '" & My.Settings.CajaActual & "')"
                        '                        'bdBase.bdExecute(conexion, qryins)
                        '                        ''hago entrada en esta tienda
                        '                        'tipo = "ENTRADA"
                        '                        'qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja) values ('" & _
                        '                        '         oLogin.pUsrNombreC.ToString.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & _
                        '                        '         tipo & "',getdate(),'" & chora & "', '" & oLogin.pUserId & "', '" & My.Settings.CajaActual & "')"
                        '                        'bdBase.bdExecute(conexion, qryins)
                        '                        'MessageBox.Show("Se registró salida en otra sucursal y se dio entrada en esta tienda.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                        Splash(False)
                        '                        MessageBox.Show("Tiene registrada una entrada en otra sucursal, favor de checar esa salida primero", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                        Application.Exit()
                        '                    Else
                        '                        'hago entrada en esta tienda
                        '                        tipo = "ENTRADA"
                        '                        qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja) values ('" & _
                        '                                 oLogin.pUsrNombreC.ToString.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & _
                        '                                 tipo & "',getdate(),'" & chora & "', '" & oLogin.pUserId & "', '" & My.Settings.CajaActual & "')"
                        '                        bdBase.bdExecute(conexion, qryins)
                        '                        Splash(False)
                        '                        MessageBox.Show("Su entrada ha sido registrada!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                    End If
                        '                End If
                        '            Else
                        '                ' no tiene registros este día en ningun lado, es entrada a la tienda actual
                        '                tipo = "ENTRADA"
                        '                qryins = "INSERT INTO  [logdia]([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idEmpleado, caja) values ('" & _
                        '                          oLogin.pUsrNombreC.ToString.Trim & "','" & My.Settings.Item("TiendaActual") & "','" & origen + "','" & _
                        '                          tipo & "',getdate(),'" & chora & "', '" & oLogin.pUserId & "', '" & My.Settings.CajaActual & "')"
                        '                bdBase.bdExecute(conexion, qryins)
                        '                Splash(False)
                        '                MessageBox.Show("Su entrada ha sido registrada!", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '                Splash(True)
                        '            End If
                        '        Catch ex As Exception
                        '            'MsgBox(ex.Message)
                        '        End Try
                        '    End If
                        'End If
                        Splash(False)
                        Me.Hide()
                        'If String.IsNullOrWhiteSpace(My.Settings.Modo) Then
                        '    Dim fModo As New modo
                        '    fModo.ShowDialog()
                        '    fModo = Nothing
                        'End If

                        '-------------------------------------   

                        'InicializaCreate("MenuAIO")
                        'For Each script As String In scripts
                        '    bdBase.bdExecute(conexion, script)
                        'Next
                        'CargarPermisos()
                        '-------------------------------------

                        ' Checar cambios en lista de precios
                        'Dim sQueryP As String = "SELECT DISTINCT [avisar] FROM preciosl WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, fechaAplicacion)) >= '" & _
                        '                        String.Format("{0:yyyyMMdd}", System.DateTime.Now) & "'"
                        'Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQueryP)
                        'If Not dr Is Nothing Then
                        '    If dr.HasRows Then
                        '        dr.Read()
                        '        If dr("avisar") Then
                        '            Dim forma As New PPC
                        '            forma.pGerente = True
                        '            forma.WindowState = FormWindowState.Maximized
                        '            forma.ShowDialog()
                        '            forma = Nothing
                        '        End If
                        '    End If
                        '    dr.Close()
                        'End If

                        'If Not My.Settings.puestosArreglados Then ArreglaPuestos()

                        'If oLogin.pUserId = -1 Then
                        Form1.Show()
                        'Else
                        '    Select Case My.Settings.Modo
                        '        Case "Todo"
                        '            Form1.Show()
                        '        Case "Caja"
                        '            Form2.Show()
                        '        Case "Central"
                        '            Form3.Show()
                        '    End Select
                        'End If
                    Else
                        Splash(False)
                        mensaje = oLogin.pMensaje
                        UsernameTextBox.Focus()
                    End If
                    'Else
                    '    OK.Enabled = False
                    'End If
            End Select
        End If
        Splash(False)
        If mensaje = "Esperando activación" Then
            OK.Enabled = False
        Else
            OK.Enabled = True
        End If
        lblMensaje.Text = mensaje
    End Sub

    'Private Sub CargarPermisos()
    '    ' Bitacora.SaveTextToFile("Inicia CargarPermisos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    '    Dim sql As String = "SELECT id FROM MenuAIO"
    '    Dim dtMenu As DataTable = bdBase.dbDataTable(conexion, sql)
    '    Dim view As New DataView(dtMenu)

    '    Dim filter As String = ""
    '    Dim id As String
    '    Dim titulo As String
    '    Dim nivel As String
    '    Dim parent_id As String

    '    Dim instruccion As String
    '    Dim imagen As String
    '    Dim permiso As String
    '    Dim tipo As String

    '    Dim ensamblado As Assembly
    '    Dim xml As Stream
    '    ensamblado = Assembly.GetExecutingAssembly()
    '    xml = ensamblado.GetManifestResourceStream("AllInOne.MenuAIO.xml")

    '    Dim xmlReader As XmlTextReader = New XmlTextReader(xml)
    '    While (xmlReader.ReadToFollowing("id"))
    '        id = xmlReader.ReadString()
    '        filter = "id='{0}'"
    '        filter = String.Format(filter, id)
    '        view.RowFilter = filter
    '        If (view.Count = 0) Then
    '            xmlReader.ReadToNextSibling("Titulo")
    '            titulo = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("Instruccion")
    '            instruccion = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("Nivel")
    '            nivel = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("ParentId")
    '            parent_id = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("imagen")
    '            imagen = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("permiso")
    '            permiso = xmlReader.ReadString()
    '            xmlReader.ReadToNextSibling("tipo")
    '            tipo = xmlReader.ReadString()
    '            sql = "INSERT INTO MenuAIO (id, Titulo, Nivel, ParentId, Instruccion) " & _
    '                  "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}') "
    '            sql = String.Format(sql, id, titulo, nivel, parent_id, instruccion)
    '            bdBase.bdExecute(conexion, sql)
    '        End If
    '    End While
    '    xmlReader.Close()
    '    ' Bitacora.SaveTextToFile("Termina CargarPermisos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    'End Sub

#Region "text box select"
    'Private Sub ResetEnterFlag()
    '    enter = False
    'End Sub

    'Private Sub UsernameTextBox_Enter(sender As Object, e As System.EventArgs) Handles UsernameTextBox.Enter
    '    enter = True
    '    BeginInvoke(New MethodInvoker(AddressOf ResetEnterFlag))
    'End Sub

    'Private Sub UsernameTextBox_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles UsernameTextBox.MouseUp
    '    If needSelect Then
    '        Dim textBx As DevExpress.XtraEditors.TextEdit = CType(sender, DevExpress.XtraEditors.TextEdit)
    '        textBx.SelectAll()
    '    End If
    'End Sub

    'Private Sub UsernameTextBox_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles UsernameTextBox.MouseDown
    '    needSelect = True
    'End Sub
#End Region

    'Private Sub PictureBox1_DoubleClick(sender As Object, e As System.EventArgs) Handles PictureBox1.DoubleClick
    '    GroupControl2.Text = "eliminando scripts..."
    '    GroupControl2.Refresh()
    '    Dim dirE As DirectoryInfo = New DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz")
    '    Dim archivosE() As FileInfo = dirE.GetFiles
    '    For Each archivito As FileInfo In archivosE
    '        If archivito.Extension.ToLower = ".sql" Then File.Delete(archivito.FullName)
    '    Next
    '    GroupControl2.Text = "Scripts eliminados. Preparado."
    '    GroupControl2.Refresh()
    'End Sub

    'Private Sub ArreglaPuestos()
    '    ' Vemos si es necesario arreglar
    '    Dim sQuery As String = "SELECT * FROM deptopuesto WHERE Departamento = 'Cajero'"
    '    Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
    '    If Not dr Is Nothing Then
    '        If dr.HasRows Then
    '            dr.Read()
    '            If dr("id") = 1 Then
    '                ' Borrar la tabla deptopuesto actual
    '                sQuery = "TRUNCATE TABLE deptopuesto"
    '                Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
    '                If String.IsNullOrEmpty(regresa) Then
    '                    sQuery = "SET IDENTITY_INSERT deptoPuesto ON " & vbCrLf
    '                    sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(0 AS Numeric(18, 0)), N'Cajero') " & vbCrLf
    '                    sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(1 AS Numeric(18, 0)), N'Vendedor') " & vbCrLf
    '                    sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(2 AS Numeric(18, 0)), N'Usuario/Admin') " & vbCrLf
    '                    sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(3 AS Numeric(18, 0)), N'Supervisor') " & vbCrLf
    '                    sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(4 AS Numeric(18, 0)), N'Comprador') " & vbCrLf
    '                    sQuery += "SET IDENTITY_INSERT deptoPuesto OFF "
    '                    regresa = bdBase.bdExecute(conexion, sQuery)
    '                    If String.IsNullOrEmpty(regresa) Then
    '                        ' Arreglar puestos actuales
    '                        sQuery = "UPDATE empleado SET puesto = puesto - 1"
    '                        regresa = bdBase.bdExecute(conexion, sQuery)
    '                        If String.IsNullOrEmpty(regresa) Then
    '                            My.Settings.puestosArreglados = True
    '                            My.Settings.Save()
    '                        End If
    '                    End If
    '                End If
    '            Else
    '                My.Settings.puestosArreglados = True
    '                My.Settings.Save()
    '            End If
    '        End If
    '        dr.Close()
    '    End If
    'End Sub
End Class