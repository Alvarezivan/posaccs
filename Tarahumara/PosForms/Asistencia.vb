﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
'Imports System.Data.SqlServerCe

Public Class Asistencia

    Private Sub Asistencia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'IdiomaTextos(Me)
        'If String.IsNullOrEmpty(conexionLocal) Then
        '    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(307, Mensaje.Texto), IdiomaMensajes(307, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
        '    Exit Sub
        'End If
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        ' ' DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        RegistraAcceso(conexion, "Reloj Checador [Asistencia.frm]")
        Dim qry As String = "SELECT Numero, Nombre, PASSWORD FROM  empleado where Status = 1 ORDER BY NOMBRE"
        LlenaCombobox(ComboBox6, qry, "Nombre", "Numero", conexion)
        'LlenaCombobox(ComboBox6, qry, "Nombre", "numero", conexionlocal)
        'ComboBox6.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor

        If ComboBox6.Properties.Items.Count >= 1 Then
            ComboBox6.SelectedIndex = 0
        Else
            ComboBox6.SelectedIndex = -1
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim qry As String = "SELECT upper(PASSWORD) as Pass FROM  empleado where numero='" & CType(ComboBox6.SelectedItem, cInfoCombo).ID & "' ORDER BY NOMBRE"
        Dim dspass As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dspass.Read()
        If dspass("Pass").ToString.Trim.ToUpper = TextBox1.Text.Trim.ToUpper Then

            Dim FechaSql As String
            Dim query1, qryins, origen, tipo As String

            origen = "1"
            qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
            Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            ds.Read()


            FechaSql = ds("fecha")
            ds.Close()
            'query1 = "SELECT empleado.NOMBRE, MAX(logdia.Fecha) AS ultimaoperacion, " & _
            '         "logdia.Tienda AS Tienda, logdia.Tipo AS tipo " & _
            '         " FROM empleado INNER JOIN logdia ON logdia.nombre = empleado.nombre AND  " & _
            '         " CONVERT(nCHAR(8), logdia.fecha , 112) = '" + Format(FechaSql, "yyyyMMdd") + "' " & _
            '         " WHERE     (empleado.nombre = '" & CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "')  " & _
            '         " GROUP BY   empleado.NOMBRE, logdia.Tipo, logdia.Tienda " & _
            '         " ORDER BY  ultimaoperacion desc"
            query1 = "SELECT empleado.NOMBRE, MAX(logdia.FECHA) AS ultimaoperacion, logdia.TIENDA AS Tienda, logdia.TIPO AS tipo, tiendas.HoraAp, " & _
                     "tiendas.HoraCie, tiendas.Tolerancia, Tiendas.ToleraComida, Tiendas.TiempoComida, Tiendas.Retardo, empleado.HoraEnt, empleado.HoraSal " & _
                     "FROM empleado INNER JOIN logdia ON logdia.NOMBRE = empleado.NOMBRE AND CONVERT(nCHAR(8), logdia.FECHA, 112) = '" + _
                     Format(FechaSql, "yyyyMMdd") + "' INNER JOIN tiendas ON logdia.TIENDA = tiendas.numero WHERE  (empleado.NOMBRE = '" & _
                     CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "') GROUP BY empleado.NOMBRE, logdia.TIPO, logdia.TIENDA, tiendas.HoraAp, " & _
                     "tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, empleado.HoraEnt, empleado.HoraSal " & _
                     "ORDER BY ultimaoperacion DESC"
            Dim thedate As Date
            Dim cfecha As String
            qry = "Select getDate() as FechaHora "
            Dim drf As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            drf.Read()
            thedate = drf("FechaHora")
            cfecha = thedate.ToString("yyyyMMdd")
            'MsgBox(drf("FechaHora"))
            thedate = drf("FechaHora")
            drf.Close()
            Dim chora As String = thedate.ToString("HH:mm:ss") ' drf("FechaHora").ToString.Trim.Substring(11, 10)
            Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexion, query1)
            Try
                Dim bTieneRenglones As Boolean = False
                While ds2.Read
                    bTieneRenglones = True
                    If ds2("Tienda").ToString.Trim = My.Settings.Item("TiendaActual") Then
                        If (ds2("tipo").ToString.Trim).Contains("ENTRADA") Then
                            'hago salida
                            tipo = "SALIDA"
                            qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idempleado, caja, llave, autoriza) values ('" & _
                                     CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "','" & My.Settings.Item("TiendaActual") & _
                                     "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', " & CType(ComboBox6.SelectedItem, cInfoCombo).ID & _
                                     ", '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), 0)"
                            bdBase.bdExecute(conexion, qryins)
                            'MessageBox.Show(IdiomaMensajes(303, Mensaje.Texto), IdiomaMensajes(303, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                            MsgBox("Salida Registrada con exito", MsgBoxStyle.Information)
                            Me.Hide()
                            Exit While
                        Else
                            Dim idSuper As Integer = 0
                            ' Llega a tiempo ?
                            Dim ret As Integer = 0
                            If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                                Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                                ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                            End If
                            Select Case ret
                                Case 0
                                    tipo = "ENTRADA"
                                Case 1
                                    tipo = "ENTRADA R"
                                Case 2
                                    If My.Settings.autorizaEntrada Then
                                        Dim myAdmin As New Administradores
                                        myAdmin.ShowDialog()
                                        If myAdmin.Verificado Then
                                            tipo = "ENTRADA A"
                                            idSuper = myAdmin.idSuper
                                        End If
                                    Else
                                        tipo = "ENTRADA"
                                    End If
                                Case Else
                                    tipo = ""
                            End Select
                            If Not String.IsNullOrEmpty(tipo) Then
                                'hago entrada
                                qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idempleado, caja, llave, autoriza) values ('" & _
                                         CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "','" & My.Settings.Item("TiendaActual") & _
                                         "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', " & CType(ComboBox6.SelectedItem, cInfoCombo).ID & _
                                         ", '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                                bdBase.bdExecute(conexion, qryins)
                                'MessageBox.Show(IdiomaMensajes(304, Mensaje.Texto), IdiomaMensajes(304, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                                MsgBox("Entrada Registrada con Exito", MsgBoxStyle.Information)
                                Me.Hide()
                                Exit While
                            End If
                            Exit While
                        End If
                        Exit While
                    Else
                        If (ds2("tipo").ToString.Trim).Contains("ENTRADA") Then
                            'Tiene que salir de la tienda origen no hace nada
                            ''MessageBox.Show(IdiomaMensajes(305, Mensaje.Texto))
                            MsgBox("Ya Registro Entrada en otra Tienda", MsgBoxStyle.Information)
                            Me.Hide()
                            Exit While
                        Else
                            Dim idSuper As Integer = 0
                            ' Llega a tiempo ?
                            Dim ret As Integer = 0
                            If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And Not IsDBNull(ds2("TiempoComida")) And _
                                Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) And Not IsDBNull(ds2("ToleraComida")) Then
                                ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                            End If
                            Select Case ret
                                Case 0
                                    tipo = "ENTRADA"
                                Case 1
                                    tipo = "ENTRADA R"
                                Case 2
                                    If My.Settings.autorizaEntrada Then
                                        Dim myAdmin As New Administradores
                                        myAdmin.ShowDialog()
                                        If myAdmin.Verificado Then
                                            tipo = "ENTRADA A"
                                            idSuper = myAdmin.idSuper
                                        End If
                                    Else
                                        tipo = "ENTRADA"
                                    End If
                                Case Else
                                    tipo = ""
                            End Select
                            If Not String.IsNullOrEmpty(tipo) Then
                                'hago entrada en esta tienda
                                qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idempleado, caja, llave, autoriza) values ('" & _
                                         CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "','" & My.Settings.Item("TiendaActual") & _
                                         "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', " & CType(ComboBox6.SelectedItem, cInfoCombo).ID & _
                                         ", '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                                bdBase.bdExecute(conexion, qryins)
                                'MessageBox.Show(IdiomaMensajes(306, Mensaje.Texto), IdiomaMensajes(306, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                                MsgBox("Entrada Registrada con Exito", MsgBoxStyle.Information)
                                Me.Hide()
                                Exit While
                            End If
                            Exit While
                        End If
                        Exit While
                    End If
                End While
                ds2.Close()
                query1 = "SELECT empleado.NOMBRE, tiendas.HoraAp, tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, " & _
                         "empleado.HoraEnt, empleado.HoraSal FROM empleado CROSS JOIN Tiendas WHERE (empleado.NOMBRE = '" & _
                         CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "') AND (tiendas.numero = " & My.Settings.TiendaActual & ") " & _
                         "GROUP BY empleado.NOMBRE, tiendas.HoraAp, tiendas.HoraCie, tiendas.Tolerancia, tiendas.ToleraComida, tiendas.TiempoComida, tiendas.Retardo, " & _
                         "empleado.HoraEnt, empleado.HoraSal"
                ds2 = bdBase.bdDataReader(conexion, query1)
                If Not ds2 Is Nothing Then
                    If ds2.HasRows Then
                        ds2.Read()
                        If Not bTieneRenglones Then
                            ' no tiene registros este día en ningun lado, es entrada a la tienda actual
                            Dim idSuper As Integer = 0
                            ' Llega a tiempo ?
                            Dim ret As Integer = 0
                            If Not IsDBNull(ds2("HoraAp")) And Not IsDBNull(ds2("Tolerancia")) And Not IsDBNull(ds2("Retardo")) And _
                                Not IsDBNull(ds2("TiempoComida")) And Not IsDBNull(ds2("ToleraComida")) And _
                                Not IsDBNull(ds2("HoraEnt")) And Not IsDBNull(ds2("HoraSal")) Then
                                ret = TipoEntrada(ds2("HoraAp"), ds2("HoraEnt"), ds2("HoraSal"), ds2("Tolerancia"), ds2("Retardo"), ds2("TiempoComida"), ds2("ToleraComida"), thedate)
                            End If
                            Select Case ret
                                Case 0
                                    tipo = "ENTRADA"
                                Case 1
                                    tipo = "ENTRADA R"
                                Case 2
                                    If My.Settings.autorizaEntrada Then
                                        Dim myAdmin As New Administradores
                                        myAdmin.ShowDialog()
                                        If myAdmin.Verificado Then
                                            tipo = "ENTRADA A"
                                            idSuper = myAdmin.idSuper
                                        End If
                                    Else
                                        tipo = "ENTRADA"
                                    End If
                                Case Else
                                    tipo = ""
                            End Select
                            If Not String.IsNullOrEmpty(tipo) Then
                                qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idempleado, caja, llave, autoriza) values ('" & _
                                         CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "','" & My.Settings.Item("TiendaActual") & _
                                         "','" & origen + "','" & tipo & "',getdate(),'" & chora & "', " & CType(ComboBox6.SelectedItem, cInfoCombo).ID & _
                                         ", '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), " & idSuper & ")"
                                bdBase.bdExecute(conexion, qryins)
                                'MessageBox.Show(IdiomaMensajes(306, Mensaje.Texto), IdiomaMensajes(306, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                                MsgBox("Entrada Registrada con exito", MsgBoxStyle.Information)
                                Me.Hide()
                            End If
                        End If
                    Else
                        qryins = "INSERT INTO [logdia] ([NOMBRE],[Tienda],[Origen],[Tipo],fecha,hora, idempleado, caja, llave) values ('" & _
                                             CType(ComboBox6.SelectedItem, cInfoCombo).Text.Trim & "','" & My.Settings.Item("TiendaActual") & _
                                             "','" & origen + "','ENTRADA',getdate(),'" & chora & "', " & CType(ComboBox6.SelectedItem, cInfoCombo).ID & _
                                             ", '" & My.Settings.CajaActual & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                        bdBase.bdExecute(conexion, qryins)
                        'MessageBox.Show(IdiomaMensajes(306, Mensaje.Texto), IdiomaMensajes(306, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        MsgBox("Entrada Registrada con Exito", MsgBoxStyle.Information)
                        Me.Hide()
                    End If
                    ds2.Close()
                End If
            Catch ex As Exception
                If Not ex.InnerException Is Nothing Then
                    MsgBox(ex.Message & " " & ex.InnerException.StackTrace.ToString)
                Else
                    MsgBox(ex.Message)
                End If

            End Try
        Else
            'MessageBox.Show(IdiomaMensajes(297, Mensaje.Texto), IdiomaMensajes(297, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            MsgBox("La Contraseña es Incorrecta", MsgBoxStyle.Information)
            TextBox1.Text = ""
            TextBox1.Focus()

        End If
        dspass.Close()
    End Sub
End Class