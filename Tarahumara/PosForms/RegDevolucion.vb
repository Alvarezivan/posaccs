﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls

Public Class RegDevolucion

    Public combo As New System.Windows.Forms.DataGridViewComboBoxColumn
    'Public ds As New DataSet
    Dim oSocio As String
    Dim VistaPrevia As Boolean = False
    Public arreglocolores(100) As String
    Public arregloacabasos(100) As String
    Public arreglomarcas(100) As String
    Public arreglolineas(100) As String
    Dim elvendedor As Integer
    Dim vendedorname As String = ""
    Dim ChangePrice As Boolean = False
    Dim PreciosAlterados As Boolean = False
    Dim dsArticulos As New DataSet
    Dim bImprime As Boolean = False
    Dim cod As Integer

    Private Sub Devoluciones_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Registrar Devolución] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre


        RegistraAcceso(conexion, "Registrar Devolución [RegDevolucion.frm]")

        dinicial.Value = DateAdd(DateInterval.Day, -30, Now.Date)

        dfinal.Value = Now.Date

        tbcantidad.Text = 1

        dsArticulos = New DataSet
        Dim tabla As New DataTable
        tabla.Columns.Add("idproducto", GetType(String))
        tabla.Columns.Add("Cantidad", GetType(Integer))
        tabla.Columns.Add("Descripcion", GetType(String))
        tabla.Columns.Add("Precio", GetType(Decimal))
        tabla.Columns.Add("Desc", GetType(Decimal))
        tabla.Columns.Add("Importe", GetType(Decimal))
        tabla.Columns.Add("Seleccionar", GetType(Boolean))
        tabla.Columns.Add("Devuelto", GetType(String))
        tabla.Columns.Add("id", GetType(String))
        tabla.Columns.Add("MotivoDeDevolucion", GetType(Integer))
        tabla.Columns.Add("Observaciones", GetType(String))
        tabla.Columns.Add("IDR", GetType(Integer))
        tabla.Columns("IDR").AutoIncrement = True
        tabla.Columns("IDR").AutoIncrementSeed = 1
        tabla.PrimaryKey = New DataColumn() {tabla.Columns("IDR")}
        dsArticulos.Tables.Add(tabla)

        LlenaComboAsistencia(ComboBoxEdit11)
        LlenaCombobox(ComboBoxEdit2, "Select numero,ltrim(rtrim(Nombre)) as Nombre from socios order by nombre", "nombre", "numero", conexion)
        ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor

        Splash(False)
    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click

        If Not tbcodigo.Text = "" Then
            Dim query As String = " Select idProducto from skus where sku =  '" & tbcodigo.Text.Trim & "' "
            Dim dr3 As SqlDataReader = bdBase.bdDataReader(conexion, query)
            If Not dr3 Is Nothing Then
                If dr3.HasRows Then
                    dr3.Read()

                    If Not IsDBNull(dr3("idProducto")) Then cod = dr3("idProducto")

                    ingresar(cod)
                    tbcodigo.Text = ""
                    tbcodigo.Focus()

                Else
                    MsgBox("Codigo no encontrado", MsgBoxStyle.Information)
                    tbcodigo.Text = ""
                    tbcodigo.Focus()

                End If
            End If
            dr3.Close()
        End If

    End Sub

    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click
        If GridView1.RowCount > 0 Then
            MessageBox.Show(IdiomaMensajes(380, Mensaje.Texto), IdiomaMensajes(380, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        Cursor = Cursors.WaitCursor
        GridControl1.DataSource = Nothing
        If textbox1.Text.Trim.Length = 0 Then
            Exit Sub
        End If
        Dim qry As String = "SELEct status from fma where ticket ='" & textbox1.Text.Trim & "' and status=2 "
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        Dim bTieneRenglones As Boolean = False
        While dr.Read
            bTieneRenglones = True
        End While
        dr.Close()
        If bTieneRenglones Then
            MessageBox.Show(IdiomaMensajes(381, Mensaje.Texto), IdiomaMensajes(381, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        'If My.Settings.once Then
        '    qry = " SELECT  detnotas.socio as cliente,   left(detnotas.BARCODE,12) as BARCODE, detnotas.TIENDA, detnotas.fecha, ltrim(rtrim(detnotas.ESTILO)) +' '+ltrim(rtrim( colores.COLOR))+' '+ltrim(rtrim(acabados.acabado)) as Descri ,  " & _
        '               " detnotas.PUNTO,detnotas.id, detnotas.PRECIO, detnotas.empleado,detnotas.DESCUEN,  detnotas.PRECIO * (1 - detnotas.DESCUEN / 100) AS Importe,  " & _
        '               "  tiendas.NOMBRE AS NombreTienda, cAST(isnull(detnotas.devuelto,'') AS nvarchar(30))  as devuelto " & _
        '               " FROM         detnotas INNER JOIN " & _
        '               " colores ON detnotas.COLOR = colores.NUMERO INNER JOIN " & _
        '               " acabados ON detnotas.ACABADO = acabados.numero  INNER JOIN " & _
        '               " tiendas ON detnotas.TIENDA = tiendas.NUMERO " & _
        '               " WHERE     (detnotas.NUMERO = '" & TextBox1.Text.Trim & "')"
        'Else
        qry = " SELECT    detnotas.socio as cliente, detnotas.cantidad, detnotas.idproducto , detnotas.TIENDA, detnotas.fecha, ltrim(rtrim(sub.subfamilia)) +' '+ltrim(rtrim( mar.marca))+' '+ltrim(rtrim(prod.descripcion)) as Descri ,  " & _
              " detnotas.llave as id, detnotas.PRECIO, detnotas.empleado,detnotas.DESCUEN,  detnotas.PRECIO * (1 - detnotas.DESCUEN / 100) AS Importe,  " & _
              "  tiendas.NOMBRE AS NombreTienda, cAST(CASE WHEN detnotas.devuelto IS NULL THEN '' ELSE detnotas.devuelto END AS nvarchar(30))  as devuelto " & _
              " FROM         detnotas  INNER JOIN " & _
              " productos prod ON prod.idproducto = detnotas.idproducto INNER JOIN " & _
              " subfamilias sub ON sub.idsubfamilia = prod.idsubfamilia INNER JOIN " & _
              " marcas mar ON mar.idmarca = prod.idmarca INNER JOIN " & _
              " tiendas ON detnotas.TIENDA = tiendas.NUMERO " & _
              " WHERE     (detnotas.NUMERO = '" & textbox1.Text.Trim & "')"
        'End If

        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        Dim dt As DataTable = dr2.Tables(0)
        Dim x As Integer = 0
        For Each drx As DataRow In dt.Rows
            label3.Text = drx("NombreTienda").ToString.Trim.ToUpper & " " & "El día " & FormatDateTime(drx("Fecha"), DateFormat.LongDate)
            'DataGridView1.Rows.Add()
            'DataGridView1.Item(0, x).Value = drx("barcode")
            'DataGridView1.Item(1, x).Value = 1
            'DataGridView1.Item(2, x).Value = drx("Descri")
            'DataGridView1.Item(3, x).Value = drx("PUNTO")
            'DataGridView1.Item(4, x).Value = drx("Precio")
            'DataGridView1.Item(5, x).Value = drx("DESCUEN")
            Dim valor As Decimal

            valor = String.Format("{0:f2}", drx("Importe"))


            Dim renglon As DataRow = dsArticulos.Tables(0).NewRow
            renglon("idProducto") = drx("idproducto").ToString.Trim
            renglon("Cantidad") = drx("cantidad")   ' 1
            renglon("Descripcion") = drx("Descri")
            renglon("Precio") = drx("Precio")
            renglon("Desc") = drx("DESCUEN")
            renglon("Importe") = valor
            renglon("Seleccionar") = False
            renglon("Devuelto") = drx("devuelto")
            renglon("id") = drx("id")
            dsArticulos.Tables(0).Rows.Add(renglon)

            elvendedor = drx("Empleado")
            oSocio = drx("cliente")
            x += 1
        Next
        GridControl1.DataSource = dsArticulos.Tables(0)
        GridView1.PopulateColumns()
        GridView1.BestFitColumns()
        ' GridView1.Columns(0).Visible = False
        GridView1.Columns("idproducto").Visible = False
        GridView1.Columns(8).Visible = False
        GridView1.Columns(11).Visible = False

        Dim dsM As DataSet = bdBase.bdDataset(conexion, "SELECT Numero AS Numero, LTRIM(RTRIM(Nombre)) AS Nombre FROM motivos")
        Dim colCombo As New RepositoryItemLookUpEdit
        colCombo.ShowHeader = True
        colCombo.ShowFooter = False
        colCombo.DataSource = dsM.Tables(0)
        colCombo.DisplayMember = "Nombre"
        colCombo.ValueMember = "Numero"
        colCombo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        colCombo.NullText = ""

        Dim colGridColumn As DevExpress.XtraGrid.Columns.GridColumn
        colGridColumn = GridView1.Columns("MotivoDeDevolucion")
        colGridColumn.ColumnEdit = colCombo
        Dim col1 As LookUpColumnInfoCollection = colCombo.Columns
        col1.Add(New LookUpColumnInfo("Nombre", "Motivo", 0))
        col1.Add(New LookUpColumnInfo("Numero", "Numero", 0))
        colCombo.Columns(1).Visible = False
        colCombo.BestFit()
        qry = "select nombre, nombre as Usuario,numero from empleado where numero='" & elvendedor & "'"
        Dim dx As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        bTieneRenglones = False
        While dx.Read()
            vendedorname = dx("nombre").ToString.Trim.ToUpper
            bTieneRenglones = True
        End While
        dx.Close()
        If Not bTieneRenglones Then
            vendedorname = "No localizado"
        End If
        LlenaCombobox(ComboBoxEdit11, qry, "Usuario", "numero", conexion)
        ComboBoxEdit11.SelectedIndex = 0
        FillComboCE("nombre", oSocio, ComboBoxEdit2, "socios")
        ComboBoxEdit2.SelectedIndex = -1
        'Dim i As Integer
        'For i = 0 To DataGridView1.Rows.Count - 1
        '    If DataGridView1.Item(8, i).Value = "Jan  1 1900 12:00AM" Then
        '        DataGridView1.Item(8, i).Value = ""
        '        DataGridView1.Rows(i).ReadOnly = False
        '    Else
        '        DataGridView1.Rows(i).ReadOnly = True
        '    End If
        'Next
        Dim cosa As String

        For i As Integer = 0 To GridView1.RowCount - 1
            If GridView1.GetRowCellValue(i, "Devuelto") = "Jan  1 1900 12:00AM" Or GridView1.GetRowCellValue(i, "Devuelto") = "Ene  1 1900 12:00AM" Then
                cosa = GridView1.GetRowCellValue(i, "Devuelto")

                Dim dato As DataRow = dsArticulos.Tables(0).Rows.Find(GridView1.GetRowCellValue(i, "IDR"))
                If Not dato Is Nothing Then
                    dato("Devuelto") = ""
                End If
                GridControl1.DataSource = dsArticulos.Tables(0)
                GridView1.SetRowCellValue(i, "Seleccionar", False)
                'GridView1.Rows(i).ReadOnly = False
            Else
                'GridView1.Rows(i).ReadOnly = True
                'GridView1.SetRowCellValue(i, "Seleccionar", True)
            End If
            GridView1.SetRowCellValue(i, "Seleccionar", False)
        Next
       
            ComboBoxEdit2.Enabled = False
        SimpleButton5.Enabled = False


        Cursor = Cursors.Default
    End Sub

    Private Sub FillComboCE(ByVal elemento As String, ByVal id As String, ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal tabla As String)
        If id = "" Or id = "-1" Then
        Else
            Dim qry As String
            qry = "SELECT      " & elemento & _
                  " FROM   " & tabla & _
                  " WHERE     (numero ='" & id & "')"
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr2 Is Nothing Then
                If dr2.HasRows Then
                    dr2.Read()
                    Dim aux As String = dr2(elemento).ToString.Trim
                    For x As Integer = 0 To combo.Properties.Items.Count
                        combo.SelectedIndex = x
                        If combo.Text.ToString.Trim = aux.ToString.Trim Then
                            Exit For
                        End If
                    Next
                    dr2.Close()
                End If
            End If

        End If
    End Sub

    Private Sub SimpleButton6_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton6.Click
        If IsNothing(ComboBoxEdit2.SelectedItem) Then
            MessageBox.Show(IdiomaMensajes(389, Mensaje.Texto), IdiomaMensajes(389, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Cursor = Cursors.WaitCursor
        Dim fechadel, fechal, qry As String
        dsArticulos.Tables(0).Rows.Clear()
        fechadel = Date.Parse(dinicial.Value).Date.ToString("yyyyMMdd")
        fechal = Date.Parse(dfinal.Value).Date.ToString("yyyyMMdd")
        Dim ladescri As String

        ladescri = "ltrim(rtrim(sub.subfamilia)) +' '+ltrim(rtrim( mar.marca))+' '+ltrim(rtrim(prod.descripcion)) "
        
        qry = " SELECT     detnotas.idproducto, detnotas.TIENDA, detnotas.fecha," & ladescri & " as Descri ,  " & _
                "  detnotas.PRECIO, detnotas.empleado,detnotas.DESCUEN,  CAST(detnotas.PRECIO * (1 - detnotas.DESCUEN / 100) AS Decimal(16,2)) AS Importe,  " & _
                "  tiendas.NOMBRE AS NombreTienda, CAST(CASE WHEN detnotas.devuelto IS NULL THEN '' ELSE detnotas.devuelto END AS nvarchar(30))  as devuelto " & _
                " ,detnotas.llave as id FROM          detnotas  INNER JOIN " & _
              " productos prod ON prod.idproducto = detnotas.idproducto INNER JOIN " & _
              " subfamilias sub ON sub.idsubfamilia = prod.idsubfamilia INNER JOIN " & _
              " marcas mar ON mar.idmarca = prod.idmarca INNER JOIN " & _
              " tiendas ON detnotas.TIENDA = tiendas.NUMERO " & _
                " WHERE     (detnotas.socio = '" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "') and detnotas.fecha>='" & fechadel & "' and detnotas.fecha<='" & fechal & "'"
        ComboBoxEdit2.Enabled = False
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        Dim dt As DataTable = dr2.Tables(0)
        If dr2.Tables(0).Rows.Count > 0 Then
            Dim x As Integer = 0
            For Each drx As DataRow In dt.Rows
                label3.Text = drx("NombreTienda").ToString.Trim.ToUpper & " " & "El día " & FormatDateTime(drx("Fecha"), DateFormat.LongDate)
                'DataGridView1.Rows.Add()
                'DataGridView1.Item(0, x).Value = drx("barcode")
                'DataGridView1.Item(1, x).Value = 1
                'DataGridView1.Item(2, x).Value = drx("Descri")
                'DataGridView1.Item(3, x).Value = drx("PUNTO")
                'DataGridView1.Item(4, x).Value = drx("Precio")
                'DataGridView1.Item(5, x).Value = drx("DESCUEN")
                'DataGridView1.Item(6, x).Value = drx("Importe")
                'DataGridView1.Item(8, x).Value = drx("devuelto")
                'DataGridView1.Item(9, x).Value = drx("id")

                Dim renglon As DataRow = dsArticulos.Tables(0).NewRow
                renglon("idproducto") = drx("idproducto").ToString.Trim
                renglon("Cantidad") = 1
                renglon("Descripcion") = drx("Descri")
                renglon("Precio") = drx("Precio")
                renglon("Desc") = drx("DESCUEN")
                renglon("Importe") = drx("Importe")
                renglon("Seleccionar") = False
                renglon("Devuelto") = drx("devuelto")
                renglon("id") = drx("id")
                dsArticulos.Tables(0).Rows.Add(renglon)

                elvendedor = drx("Empleado")
                x += 1
            Next
            GridControl1.DataSource = dsArticulos.Tables(0)
            GridView1.PopulateColumns()
            GridView1.BestFitColumns()
            ' GridView1.Columns(0).Visible = False
            GridView1.Columns("idproducto").Visible = False
            GridView1.Columns(8).Visible = False
            GridView1.Columns(11).Visible = False

            Dim dsM As DataSet = bdBase.bdDataset(conexion, "SELECT Numero AS Numero, LTRIM(RTRIM(Nombre)) AS Nombre FROM motivos")
            Dim colCombo As New RepositoryItemLookUpEdit
            colCombo.ShowHeader = True
            colCombo.ShowFooter = False
            colCombo.DataSource = dsM.Tables(0)
            colCombo.DisplayMember = "Nombre"
            colCombo.ValueMember = "Numero"
            colCombo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            colCombo.NullText = ""

            Dim colGridColumn As DevExpress.XtraGrid.Columns.GridColumn
            colGridColumn = GridView1.Columns("MotivoDeDevolucion")
            colGridColumn.ColumnEdit = colCombo
            Dim col1 As LookUpColumnInfoCollection = colCombo.Columns
            col1.Add(New LookUpColumnInfo("Nombre", "Motivo", 0))
            col1.Add(New LookUpColumnInfo("Numero", "Numero", 0))
            colCombo.Columns(1).Visible = False
            'colCombo.BestFitMode = BestFitMode.BestFit
            'colCombo.BestFit()
            'colCombo.Columns(1).Width = 100
            qry = "select nombre from empleado where numero='" & elvendedor & "'"
            Dim dx As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            Dim bTieneRen As Boolean = False
            While dx.Read
                bTieneRen = True
                vendedorname = dx("nombre").ToString.Trim.ToUpper
            End While
            dx.Close()
            If Not bTieneRen Then
                vendedorname = "No localizado"
            End If
        End If
        Dim i As Integer
        For i = 0 To GridView1.RowCount - 1
            If GridView1.GetRowCellValue(i, "Devuelto") = "Jan  1 1900 12:00AM" Or IsDBNull(GridView1.GetRowCellValue(i, "Devuelto")) Or GridView1.GetRowCellValue(i, "Devuelto") = "Ene  1 1900 12:00AM" Then
                Dim dato As DataRow = dsArticulos.Tables(0).Rows.Find(GridView1.GetRowCellValue(i, "IDR"))
                If Not dato Is Nothing Then
                    dato("Devuelto") = ""
                End If
                dato("Seleccionar") = False
                'GridView1.SetRowCellValue(i, "Devuelto", "")
                GridControl1.DataSource = dsArticulos.Tables(0)
                'DataGridView1.Rows(i).ReadOnly = False
            Else
                'DataGridView1.Rows(i).ReadOnly = True
            End If
        Next
        GridView1.BestFitColumns()

        Cursor = Cursors.Default
    End Sub


    Private Sub ingresar(ByVal cod As Integer)
        Dim cantidad As Integer = Val(tbcantidad.Text)
        Dim cliente As Integer = -1
        Dim tienda As Integer = My.Settings.TiendaActual
        Dim caja As Integer = My.Settings.TiendaActual
        Dim bTieneRenglones As Boolean = False
        Cursor = Cursors.WaitCursor
        If tbcodigo.Text.Trim.Length = 0 Then
            Exit Sub
        End If
        Dim qry As String

        qry = " SELECT  -1 as cliente, " & cantidad & " as cantidad , " & cod & " as idproducto , " & tienda & " as tienda, getdate() as fecha , ltrim(rtrim(sub.subfamilia)) +' '+ltrim(rtrim( mar.marca))+' '+ltrim(rtrim(prod.descripcion)) as Descri ,  " & _
              " newid() as id, prod.PRECIO, " & -1 & " as empleado, 0 as DESCUEN,  prod.precio AS Importe,  " & _
              "  'Actual' AS NombreTienda, 0  as devuelto " & _
              " FROM productos prod  INNER JOIN " & _
              " subfamilias sub ON sub.idsubfamilia = prod.idsubfamilia INNER JOIN " & _
              " marcas mar ON mar.idmarca = prod.idmarca  " & _
              " WHERE (prod.idproducto = '" & cod & "')"
        'End If

        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        Dim dt As DataTable = dr2.Tables(0)
        Dim x As Integer = 0
        For Each drx As DataRow In dt.Rows
            label3.Text = drx("NombreTienda").ToString.Trim.ToUpper & " " & "El día " & FormatDateTime(drx("Fecha"), DateFormat.LongDate)
            'DataGridView1.Rows.Add()
            'DataGridView1.Item(0, x).Value = drx("barcode")
            'DataGridView1.Item(1, x).Value = 1
            'DataGridView1.Item(2, x).Value = drx("Descri")
            'DataGridView1.Item(3, x).Value = drx("PUNTO")
            'DataGridView1.Item(4, x).Value = drx("Precio")
            'DataGridView1.Item(5, x).Value = drx("DESCUEN")
            Dim valor As Decimal

            valor = String.Format("{0:f2}", drx("Importe"))


            Dim renglon As DataRow = dsArticulos.Tables(0).NewRow
            renglon("idProducto") = drx("idproducto").ToString.Trim
            renglon("Cantidad") = drx("cantidad")   ' 1
            renglon("Descripcion") = drx("Descri")
            renglon("Precio") = drx("Precio")
            renglon("Desc") = drx("DESCUEN")
            renglon("Importe") = valor
            renglon("Seleccionar") = False
            renglon("Devuelto") = drx("devuelto")
            renglon("id") = drx("id")
            dsArticulos.Tables(0).Rows.Add(renglon)

            elvendedor = drx("Empleado")
            oSocio = drx("cliente")
            x += 1
        Next
        GridControl1.DataSource = dsArticulos.Tables(0)
        GridView1.PopulateColumns()
        GridView1.BestFitColumns()
        ' GridView1.Columns(0).Visible = False
        GridView1.Columns("idproducto").Visible = False
        GridView1.Columns(8).Visible = False
        GridView1.Columns(11).Visible = False

        Dim dsM As DataSet = bdBase.bdDataset(conexion, "SELECT Numero AS Numero, LTRIM(RTRIM(Nombre)) AS Nombre FROM motivos")
        Dim colCombo As New RepositoryItemLookUpEdit
        colCombo.ShowHeader = True
        colCombo.ShowFooter = False
        colCombo.DataSource = dsM.Tables(0)
        colCombo.DisplayMember = "Nombre"
        colCombo.ValueMember = "Numero"
        colCombo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        colCombo.NullText = ""

        Dim colGridColumn As DevExpress.XtraGrid.Columns.GridColumn
        colGridColumn = GridView1.Columns("MotivoDeDevolucion")
        colGridColumn.ColumnEdit = colCombo
        Dim col1 As LookUpColumnInfoCollection = colCombo.Columns
        col1.Add(New LookUpColumnInfo("Nombre", "Motivo", 0))
        col1.Add(New LookUpColumnInfo("Numero", "Numero", 0))
        colCombo.Columns(1).Visible = False
        colCombo.BestFit()
        FillComboCE("nombre", oSocio, ComboBoxEdit2, "socios")
        ComboBoxEdit2.SelectedIndex = -1
        'Dim i As Integer
        'For i = 0 To DataGridView1.Rows.Count - 1
        '    If DataGridView1.Item(8, i).Value = "Jan  1 1900 12:00AM" Then
        '        DataGridView1.Item(8, i).Value = ""
        '        DataGridView1.Rows(i).ReadOnly = False
        '    Else
        '        DataGridView1.Rows(i).ReadOnly = True
        '    End If
        'Next
        Dim cosa As String

        For i As Integer = 0 To GridView1.RowCount - 1
            If GridView1.GetRowCellValue(i, "Devuelto") = "Jan  1 1900 12:00AM" Or GridView1.GetRowCellValue(i, "Devuelto") = "Ene  1 1900 12:00AM" Then
                cosa = GridView1.GetRowCellValue(i, "Devuelto")

                Dim dato As DataRow = dsArticulos.Tables(0).Rows.Find(GridView1.GetRowCellValue(i, "IDR"))
                If Not dato Is Nothing Then
                    dato("Devuelto") = ""
                End If
                GridControl1.DataSource = dsArticulos.Tables(0)
                GridView1.SetRowCellValue(i, "Seleccionar", False)
                'GridView1.Rows(i).ReadOnly = False
            Else
                'GridView1.Rows(i).ReadOnly = True
                'GridView1.SetRowCellValue(i, "Seleccionar", True)
            End If
            GridView1.SetRowCellValue(i, "Seleccionar", False)
        Next

        ComboBoxEdit2.Enabled = False
        SimpleButton5.Enabled = False


        Cursor = Cursors.Default

    End Sub
    
    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click

    End Sub
End Class