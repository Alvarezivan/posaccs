﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RegDevolucion
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RegDevolucion))
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.codigobarrras = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcodigo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcantidad = New DevExpress.XtraEditors.TextEdit()
        Me.ticket = New DevExpress.XtraTab.XtraTabPage()
        Me.label3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.textbox1 = New DevExpress.XtraEditors.TextEdit()
        Me.socio = New DevExpress.XtraTab.XtraTabPage()
        Me.dfinal = New System.Windows.Forms.DateTimePicker()
        Me.dinicial = New System.Windows.Forms.DateTimePicker()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit11 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Button3 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.codigobarrras.SuspendLayout()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ticket.SuspendLayout()
        CType(Me.textbox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.socio.SuspendLayout()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.XtraTabControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(618, 166)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Selección de Estilos"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Location = New System.Drawing.Point(6, 24)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.codigobarrras
        Me.XtraTabControl1.Size = New System.Drawing.Size(610, 137)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.codigobarrras, Me.ticket, Me.socio})
        '
        'codigobarrras
        '
        Me.codigobarrras.Controls.Add(Me.SimpleButton4)
        Me.codigobarrras.Controls.Add(Me.LabelControl3)
        Me.codigobarrras.Controls.Add(Me.tbcodigo)
        Me.codigobarrras.Controls.Add(Me.LabelControl2)
        Me.codigobarrras.Controls.Add(Me.tbcantidad)
        Me.codigobarrras.Name = "codigobarrras"
        Me.codigobarrras.Size = New System.Drawing.Size(604, 109)
        Me.codigobarrras.Text = "Codigo de Barras"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Location = New System.Drawing.Point(432, 48)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(95, 28)
        Me.SimpleButton4.TabIndex = 6
        Me.SimpleButton4.Text = "Agregar"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(202, 31)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(98, 16)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Código de Barras"
        '
        'tbcodigo
        '
        Me.tbcodigo.Location = New System.Drawing.Point(159, 50)
        Me.tbcodigo.Name = "tbcodigo"
        Me.tbcodigo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcodigo.Properties.Appearance.Options.UseFont = True
        Me.tbcodigo.Size = New System.Drawing.Size(193, 26)
        Me.tbcodigo.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl2.Location = New System.Drawing.Point(63, 31)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(50, 16)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "Cantidad"
        '
        'tbcantidad
        '
        Me.tbcantidad.Location = New System.Drawing.Point(60, 50)
        Me.tbcantidad.Name = "tbcantidad"
        Me.tbcantidad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbcantidad.Properties.Appearance.Options.UseFont = True
        Me.tbcantidad.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.tbcantidad.Size = New System.Drawing.Size(56, 26)
        Me.tbcantidad.TabIndex = 0
        '
        'ticket
        '
        Me.ticket.Controls.Add(Me.label3)
        Me.ticket.Controls.Add(Me.LabelControl6)
        Me.ticket.Controls.Add(Me.LabelControl5)
        Me.ticket.Controls.Add(Me.SimpleButton5)
        Me.ticket.Controls.Add(Me.LabelControl4)
        Me.ticket.Controls.Add(Me.textbox1)
        Me.ticket.Name = "ticket"
        Me.ticket.Size = New System.Drawing.Size(604, 109)
        Me.ticket.Text = "Regresar por Ticket"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(21, 81)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(12, 13)
        Me.label3.TabIndex = 12
        Me.label3.Text = "..."
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(413, 57)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(12, 16)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "..."
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(449, 26)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(87, 18)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Tienda Venta"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Location = New System.Drawing.Point(246, 46)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(95, 28)
        Me.SimpleButton5.TabIndex = 9
        Me.SimpleButton5.Text = "Buscar"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl4.Location = New System.Drawing.Point(21, 26)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(148, 16)
        Me.LabelControl4.TabIndex = 8
        Me.LabelControl4.Text = "Ingrese Numero de Ticket"
        '
        'textbox1
        '
        Me.textbox1.Location = New System.Drawing.Point(21, 48)
        Me.textbox1.Name = "textbox1"
        Me.textbox1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.textbox1.Properties.Appearance.Options.UseFont = True
        Me.textbox1.Size = New System.Drawing.Size(193, 26)
        Me.textbox1.TabIndex = 7
        '
        'socio
        '
        Me.socio.Controls.Add(Me.dfinal)
        Me.socio.Controls.Add(Me.dinicial)
        Me.socio.Controls.Add(Me.LabelControl9)
        Me.socio.Controls.Add(Me.LabelControl8)
        Me.socio.Controls.Add(Me.SimpleButton6)
        Me.socio.Controls.Add(Me.ComboBoxEdit2)
        Me.socio.Controls.Add(Me.LabelControl7)
        Me.socio.Name = "socio"
        Me.socio.Size = New System.Drawing.Size(604, 109)
        Me.socio.Text = "Regresar por Socio"
        '
        'dfinal
        '
        Me.dfinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dfinal.Location = New System.Drawing.Point(305, 53)
        Me.dfinal.Name = "dfinal"
        Me.dfinal.Size = New System.Drawing.Size(104, 21)
        Me.dfinal.TabIndex = 21
        '
        'dinicial
        '
        Me.dinicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dinicial.Location = New System.Drawing.Point(195, 53)
        Me.dinicial.Name = "dinicial"
        Me.dinicial.Size = New System.Drawing.Size(104, 21)
        Me.dinicial.TabIndex = 20
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(317, 34)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(54, 13)
        Me.LabelControl9.TabIndex = 8
        Me.LabelControl9.Text = "Fecha Final"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(195, 34)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl8.TabIndex = 7
        Me.LabelControl8.Text = "Fecha inicial"
        '
        'SimpleButton6
        '
        Me.SimpleButton6.Location = New System.Drawing.Point(468, 50)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(95, 23)
        Me.SimpleButton6.TabIndex = 6
        Me.SimpleButton6.Text = "Buscar"
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(13, 53)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(143, 20)
        Me.ComboBoxEdit2.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(13, 34)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Seleccione Socio"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(686, 21)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(95, 23)
        Me.SimpleButton1.TabIndex = 1
        Me.SimpleButton1.Text = "Vista Peliminar"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(686, 50)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(95, 23)
        Me.SimpleButton2.TabIndex = 2
        Me.SimpleButton2.Text = "Cambiar Precios"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(711, 86)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Empleado"
        '
        'ComboBoxEdit11
        '
        Me.ComboBoxEdit11.Location = New System.Drawing.Point(675, 105)
        Me.ComboBoxEdit11.Name = "ComboBoxEdit11"
        Me.ComboBoxEdit11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit11.Size = New System.Drawing.Size(119, 20)
        Me.ComboBoxEdit11.TabIndex = 4
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(686, 136)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(95, 42)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Guardar"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(12, 195)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(820, 251)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'RegDevolucion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(844, 458)
        Me.Controls.Add(Me.GridControl1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.ComboBoxEdit11)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "RegDevolucion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Devoluciones"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.codigobarrras.ResumeLayout(False)
        Me.codigobarrras.PerformLayout()
        CType(Me.tbcodigo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ticket.ResumeLayout(False)
        Me.ticket.PerformLayout()
        CType(Me.textbox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.socio.ResumeLayout(False)
        Me.socio.PerformLayout()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents codigobarrras As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ticket As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents socio As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcodigo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcantidad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textbox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit11 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Button3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents label3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dfinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dinicial As System.Windows.Forms.DateTimePicker
End Class
