﻿Imports System.Data.SqlClient


Public Class IngresoClientes


    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click

        Dim qry1 As String = "Select cbarras from socios where cbarras = '" & tbcbarras.Text.Trim & "' "
        Dim qry2 As String = "Select rfc from socios where rfc = '" & tbrfc.Text.Trim & "' "
        Dim ex, r, ex1 As Integer
        Dim id As Double
        ex = 0

        Dim da As New SqlDataAdapter(qry1, conexion)
        Dim dt As New DataTable
        da.Fill(dt)
        Dim da1 As New SqlDataAdapter(qry2, conexion)
        Dim dt1 As New DataTable
        da1.Fill(dt1)

        If tbcbarras.Text.Length > 0 Then
            If dt.Rows.Count > 0 Then
                ex = 1
            Else
                ex = 0
            End If
        End If
        If dt1.Rows.Count > 0 Then
            ex1 = 1
        Else
            ex1 = 0
        End If

        If tbnombre.Text = "" Then
            MsgBox("Debe asignar Nombre", MsgBoxStyle.Information)
            tbnombre.Focus()
        ElseIf tbrfc.Text = "" Then
            MsgBox("Debe asignar RFC", MsgBoxStyle.Information)
            tbemail.Focus()
        ElseIf tbemail.Text = "" Then
            MsgBox("Debe asignar Email", MsgBoxStyle.Information)
            tbemail.Focus()
        ElseIf tbtelefono.Text = "" Then
            MsgBox("Debe asignar Telefono", MsgBoxStyle.Information)
            tbtelefono.Focus()
        ElseIf ex = 1 Then
            MsgBox("El Codigo de Barras ya fue asignado", MsgBoxStyle.Information)
            tbcbarras.Text = ""
            tbcbarras.Focus()
        ElseIf ex1 = 1 Then
            MsgBox("El RFC ya fue asignado", MsgBoxStyle.Information)
            tbrfc.Text = ""
            tbrfc.Focus()
        Else
            If chkpromo.Checked Then
                r = 1
            Else
                r = 0
            End If

            Dim qry As String = "Insert into [socios] ([NOMBRE], [TELEFONO], [CELULAR], [CALLE],[numeroext],[numeroint], [COLONIA], [CIUDAD], [MUNICIPIO], [ESTADO], " & _
                "[CodigoPostal], [FECALTA], [RFC], [BIRTHDAY], [CBARRAS], [CORREO], [recibemails], [Representante], [llave]) values ('" & tbnombre.Text.Trim.ToUpper & "', '" _
                & tbtelefono.Text.Trim & "', '" & tbcelular.Text.Trim & "', '" & tbcalle.Text.Trim.ToUpper & "','" & tbnext.Text.Trim & "','" & tbnint.Text.Trim.ToUpper & "', '" _
                & tbcolonia.Text.Trim.ToUpper & "', '" & tbciudad.Text.Trim.ToUpper & "', '" & tbmunicipio.Text.Trim.ToUpper & "', '" & tbestado.Text.Trim.ToUpper & "', '" _
                & tbcp.Text.Trim & "', getdate(), '" & tbrfc.Text.Trim.ToUpper & "','" & dtnac.Value.ToString("yyyy/MM/dd hh:mm:ss") & "' , '" & tbcbarras.Text.Trim & "', '" _
                & tbemail.Text.Trim.ToUpper & "', " & r & ", '" & tbcontacto.Text.Trim.ToUpper & "', newid())"

            bdBase.bdExecute(conexion, qry)

            qry = "select numero from socios where nombre = '" & tbnombre.Text.Trim & "'"

            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    If Not IsDBNull(dr("numero")) Then id = dr("numero").ToString.Trim
                End If
            End If
            dr.Close()

            qry = "update socios set id = " & id & " where numero = " & id
            bdBase.bdExecute(conexion, qry)

            MsgBox("Cliente Registrado con Exito", MsgBoxStyle.Information)
            limpia()
        End If
    End Sub

    Private Sub limpia()
        tbnombre.Text = ""
        tbcalle.Text = ""
        tbnext.Text = ""
        tbnint.Text = ""
        tbcolonia.Text = ""
        tbciudad.Text = ""
        tbmunicipio.Text = ""
        tbestado.Text = ""
        tbcp.Text = ""
        tbrfc.Text = ""
        tbemail.Text = ""
        tbtelefono.Text = ""
        tbcelular.Text = ""
        tbcontacto.Text = ""
        tbcbarras.Text = ""
        dtnac.Value = Today
        chkpromo.Checked = False
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Dim r As Integer
        If chkpromo.Checked Then
            r = 1
        Else
            r = 0
        End If
        Dim qry As String = "Update [SOCIOS] set  [TELEFONO] = '" & tbtelefono.Text.Trim & "' , " & _
           " [CELULAR] = '" & tbcelular.Text.Trim & "', [CALLE] = '" & tbcalle.Text.Trim.ToUpper & "', [numeroext] = '" & tbnext.Text.Trim & "', [numeroint] = '" & tbnint.Text.Trim & "' , " & _
           " [COLONIA] = '" & tbcolonia.Text.Trim.ToUpper & "', [CIUDAD] = '" & tbciudad.Text.Trim.ToUpper & "' , [MUNICIPIO] = '" & tbmunicipio.Text.Trim.ToUpper & "', " & _
           " [ESTADO] = '" & tbestado.Text.Trim.ToUpper & "', [CodigoPostal] = '" & tbcp.Text.Trim & "', [CBARRAS] = '" & tbcbarras.Text.Trim & "', " & _
           " [CORREO] = '" & tbemail.Text.Trim.ToUpper & "', [recibemails] =  " & r & ", [representante] = '" & tbcontacto.Text.Trim.ToUpper & "' where rfc = '" & tbrfc.Text.Trim.ToUpper & "'  "

        bdBase.bdExecute(conexion, qry)
        MsgBox("Cliente Modificado con Exito", MsgBoxStyle.Information)
    End Sub

   



    'Private Sub dgv2_RowHeaderMouseClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    Dim x As Integer = e.RowIndex
    '    Dim sel As String
    '    Dim r As Boolean
    '    sel = GridControl2.Item(0, x).Value.ToString
    '    Dim qry As String = "Select [numero], [NOMBRE], [TELEFONO], [CELULAR], [CALLE],[numeroint],[numeroext], [COLONIA], [CIUDAD], [MUNICIPIO], [ESTADO], " & _
    '            "[CodigoPostal], [RFC], [BIRTHDAY], [CBARRAS], [CORREO], [recibemails], [Representante], [DireccionEnvio],isnull(limitecredito,0) as limite from socios where numero = '" & sel & "' "

    '    Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

    '    If Not dr1 Is Nothing Then
    '        If dr1.HasRows Then
    '            dr1.Read()

    '            If Not IsDBNull(dr1("numero")) Then lbnumero.Text = dr1("numero").ToString.Trim
    '            If Not IsDBNull(dr1("NOMBRE")) Then tbnombre.Text = dr1("NOMBRE").ToString.Trim
    '            If Not IsDBNull(dr1("telefono")) Then tbtelefono.Text = dr1("telefono").ToString.Trim
    '            If Not IsDBNull(dr1("celular")) Then tbcelular.Text = dr1("celular").ToString.Trim
    '            If Not IsDBNull(dr1("calle")) Then tbcalle.Text = dr1("calle").ToString.Trim
    '            If Not IsDBNull(dr1("numeroint")) Then tbnint.Text = dr1("numeroint").ToString.Trim
    '            If Not IsDBNull(dr1("numeroext")) Then tbnext.Text = dr1("numeroext").ToString.Trim
    '            If Not IsDBNull(dr1("colonia")) Then tbcolonia.Text = dr1("colonia").ToString.Trim
    '            If Not IsDBNull(dr1("ciudad")) Then tbciudad.Text = dr1("ciudad").ToString.Trim
    '            If Not IsDBNull(dr1("municipio")) Then tbmunicipio.Text = dr1("municipio").ToString.Trim
    '            If Not IsDBNull(dr1("estado")) Then tbestado.Text = dr1("estado").ToString.Trim
    '            If Not IsDBNull(dr1("codigopostal")) Then tbcp.Text = dr1("codigopostal").ToString.Trim
    '            If Not IsDBNull(dr1("rfc")) Then tbrfc.Text = dr1("rfc").ToString.Trim
    '            If Not IsDBNull(dr1("cbarras")) Then tbcbarras.Text = dr1("cbarras").ToString.Trim
    '            If Not IsDBNull(dr1("correo")) Then tbemail.Text = dr1("correo").ToString.Trim
    '            If Not IsDBNull(dr1("representante")) Then tbcontacto.Text = dr1("representante").ToString.Trim
    '            If Not IsDBNull(dr1("recibemails")) Then r = dr1("recibemails").ToString.Trim
    '            If Not IsDBNull(dr1("DireccionEnvio")) Then lbenvio.Text = dr1("DireccionEnvio").ToString.Trim
    '            If Not IsDBNull(dr1("Limite")) Then lblimite.Text = dr1("Limite").ToString.Trim
    '            dtnac.Value = dr1("Birthday")

    '            If r = True Then
    '                chkpromo.Checked = True
    '            Else
    '                chkpromo.Checked = False
    '            End If
    '        End If
    '    End If


    '    tbrfc.Properties.ReadOnly = True
    '    tbnombre.Properties.ReadOnly = True
    '    SimpleButton1.Enabled = False

    '    XtraTabControl1.SelectedTabPageIndex = 0
    '    SimpleButton3.Enabled = True
    '    SimpleButton2.Enabled = True

    'End Sub



    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles btnSeleccionar.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub IngresoClientes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        XtraTabControl1.SelectedTabPageIndex = 1

        Dim qry As String

        If Preefectivo Then
            qry = " select numero,nombre,ltrim(rtrim(calle))+','+ltrim(rtrim(NumeroExt))+'-'+rtrim(ltrim(NumeroInt)) as Direccion, " & _
                  "ciudad, municipio, estado, codigopostal, rfc, " & _
                  " Telefono, correo, cbarras from socios where activarefectivo = 1  "
        Else
            qry = " select numero,nombre,ltrim(rtrim(calle))+','+ltrim(rtrim(NumeroExt))+'-'+rtrim(ltrim(NumeroInt)) as Direccion, " & _
                  "ciudad, municipio, estado, codigopostal, rfc, " & _
                  " Telefono, correo, cbarras from socios where numero >= 0 "
        End If
        Preefectivo = False
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.HorzScrollVisibility = True
        GridView2.VertScrollVisibility = True
        GridView2.BestFitColumns()


    End Sub

    Private Sub GridControl2_Click(sender As System.Object, e As System.EventArgs) Handles GridControl2.DoubleClick

        Dim idsocio As Integer
        Dim r As Boolean
        idsocio = GridView2.GetRowCellValue(GridView2.FocusedRowHandle, "numero")

        Dim qry As String = "Select [numero], [NOMBRE], [TELEFONO], [CELULAR], [CALLE],[numeroint],[numeroext], [COLONIA], [CIUDAD], [MUNICIPIO], [ESTADO], " & _
          "[CodigoPostal], [RFC], [BIRTHDAY], [CBARRAS], [CORREO], [recibemails], [Representante], [DireccionEnvio],isnull(limitecredito,0) as limite from socios where numero = '" & idsocio & "' "

        Dim dr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr1 Is Nothing Then
            If dr1.HasRows Then
                dr1.Read()

                If Not IsDBNull(dr1("numero")) Then lbnumero.Text = dr1("numero").ToString.Trim
                If Not IsDBNull(dr1("nombre")) Then tbnombre.Text = dr1("nombre").ToString.Trim
                If Not IsDBNull(dr1("telefono")) Then tbtelefono.Text = dr1("telefono").ToString.Trim
                If Not IsDBNull(dr1("celular")) Then tbcelular.Text = dr1("celular").ToString.Trim
                If Not IsDBNull(dr1("calle")) Then tbcalle.Text = dr1("calle").ToString.Trim
                If Not IsDBNull(dr1("numeroint")) Then tbnint.Text = dr1("numeroint").ToString.Trim
                If Not IsDBNull(dr1("numeroext")) Then tbnext.Text = dr1("numeroext").ToString.Trim
                If Not IsDBNull(dr1("colonia")) Then tbcolonia.Text = dr1("colonia").ToString.Trim
                If Not IsDBNull(dr1("ciudad")) Then tbciudad.Text = dr1("ciudad").ToString.Trim
                If Not IsDBNull(dr1("municipio")) Then tbmunicipio.Text = dr1("municipio").ToString.Trim
                If Not IsDBNull(dr1("estado")) Then tbestado.Text = dr1("estado").ToString.Trim
                If Not IsDBNull(dr1("codigopostal")) Then tbcp.Text = dr1("codigopostal").ToString.Trim
                If Not IsDBNull(dr1("rfc")) Then tbrfc.Text = dr1("rfc").ToString.Trim
                If Not IsDBNull(dr1("cbarras")) Then tbcbarras.Text = dr1("cbarras").ToString.Trim
                If Not IsDBNull(dr1("correo")) Then tbemail.Text = dr1("correo").ToString.Trim
                If Not IsDBNull(dr1("representante")) Then tbcontacto.Text = dr1("representante").ToString.Trim
                If Not IsDBNull(dr1("recibemails")) Then r = dr1("recibemails").ToString.Trim
                If Not IsDBNull(dr1("direccionenvio")) Then lbenvio.Text = dr1("direccionenvio").ToString.Trim
                If Not IsDBNull(dr1("limite")) Then lblimite.Text = dr1("limite").ToString.Trim
                dtnac.Value = dr1("birthday")

                If r = True Then
                    chkpromo.Checked = True
                Else
                    chkpromo.Checked = False
                End If
            End If
        End If


        tbrfc.Properties.ReadOnly = True
        tbnombre.Properties.ReadOnly = True
        SimpleButton1.Enabled = False

        XtraTabControl1.SelectedTabPageIndex = 0
        btnSeleccionar.Enabled = True
        SimpleButton2.Enabled = True

        btnSeleccionar.PerformClick()

    End Sub
End Class