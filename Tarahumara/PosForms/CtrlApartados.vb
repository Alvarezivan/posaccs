﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Image
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns

Public Class CtrlApartados

    Private Sub CtrlApartados_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Control de Apartados] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        RegistraAcceso(conexion, "Control Apartados [CtrlApartados.frm]")


        LlenaComboAsistencia(ComboBox6)
        TextBox7.Text = "Porcentaje mínimo:" & My.Settings.PorcentajeApartados & "%"
        TextBox15.Text = 0
        TextBox10.Text = 0
        Dim qry As String = "SELECT Clave,Nombre,0.00 as Monto from formaspago where status=1 and clave<>'VC' and clave <>'NC' and tienda='" & My.Settings("TiendaActual") & "' ORDER BY nombre"
        qry = "SELECT Clave,Nombre,0.00 as Monto,Tipo,Pagare,ticket from formaspago where status=1 and clave<>'VC' and clave <>'NC' and tienda=" & My.Settings("TiendaActual") & " ORDER BY nombre"
        Dim dsl As DataSet = bdBase.bdDataset(conexion, qry)
        DataGridView2.DataSource = dsl.Tables(0)
        DataGridView2.Columns(0).Visible = False
        DataGridView2.Columns(1).ReadOnly = True
        DataGridView2.Columns(3).Visible = False
        DataGridView2.Columns(4).Visible = False
        DataGridView2.Columns(5).Visible = False
        Dim col As DataGridViewColumn = DataGridView1.Columns(2)
        ' Le establecemos un formato decimal.
        col.DefaultCellStyle.Format = "N2"
        DataGridView2.Columns(2).DefaultCellStyle.Format = "C"
        DataGridView2.Columns(0).Visible = False
        DataGridView2.AutoResizeColumns()
        DataGridView3.DataSource = dsl.Tables(0)
        DataGridView3.Columns(0).Visible = False
        DataGridView3.Columns(1).ReadOnly = True
        DataGridView3.Columns(3).Visible = False
        DataGridView3.Columns(4).Visible = False
        DataGridView3.Columns(5).Visible = False
        col = DataGridView3.Columns(2)
        ' Le establecemos un formato decimal.
        col.DefaultCellStyle.Format = "N2"
        DataGridView3.Columns(2).DefaultCellStyle.Format = "C"
        DataGridView3.AutoResizeColumns()

        Me.ReportViewer2.RefreshReport()
        TextBox2.Text = ""
        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer2.RefreshReport()

        Splash(False)
    End Sub

    Private Sub tbbarcode_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles tbbarcode.LostFocus
        Dim cuantos, posi As Integer
        Dim barcodetrim, codigo As String
        barcodetrim = tbbarcode.Text.Trim
        If barcodetrim.Contains("*") And barcodetrim.Length > 2 Then
            posi = barcodetrim.IndexOf("*")

            If Not barcodetrim.Substring(0, posi) = "" Then
                cuantos = barcodetrim.Substring(0, posi)
            Else
                cuantos = 1
            End If

            codigo = barcodetrim.Substring(posi + 1, barcodetrim.Length - (posi + 1))
        Else
            codigo = barcodetrim
            cuantos = 1
        End If
        If Not tbbarcode.Text = "" Then
            Dim query As String = " Select idProducto from skus where sku =  '" & codigo & "' "
            Dim dr3 As SqlDataReader = bdBase.bdDataReader(conexion, query)
            If Not dr3 Is Nothing Then
                If dr3.HasRows Then
                    dr3.Read()

                    If Not IsDBNull(dr3("idProducto")) Then cod = dr3("idProducto")

                    ingresar(dr3("idproducto").ToString.Trim, cuantos)
                    totales()
                    tbbarcode.Text = ""
                    tbbarcode.Focus()
                ElseIf tbbarcode.Text.Trim = "*" Or tbbarcode.Text.Trim = "/" Then

                Else
                    MsgBox("Codigo no encontrado", MsgBoxStyle.Information)
                    tbbarcode.Text = ""
                    tbbarcode.Focus()

                End If
            End If
            dr3.Close()
        End If

    End Sub
    Private Sub ingresar(codigo As String, cantidad As String)
        Dim j, k, iva, i, imp2 As Decimal
        Dim x, val2, pos As Integer
        Dim agregar As Boolean = False
        pos = DataGridView1.RowCount - 1
        For x = 0 To pos

            If DataGridView1.Item(0, x).Value = cod Then
                j = Val(DataGridView1.Item(1, x).Value)
                k = Val(DataGridView1.Item(9, x).Value)
                i = Val(DataGridView1.Item(8, x).Value)
                iva = k / j
                imp2 = i / j

                DataGridView1.Item(1, x).Value = j + cantidad
                val2 = DataGridView1.Item(1, x).Value

                DataGridView1.Item(9, x).Value = val2 * iva
                DataGridView1.Item(8, x).Value = val2 * imp2
                DataGridView1.Item(10, x).Value = Val(DataGridView1.Item(8, x).Value) + Val(DataGridView1.Item(9, x).Value)
                totales()

                agregar = True
            End If
        Next

        If agregar = False Then

            Dim qry = " Select productos.idproducto, productos.Codigo, marcas.Marca, subfamilias.SubFamilia, productos.Precio, " & _
                "productos.descripcion, productos.Iva, productos.Descuento from productos inner join marcas on productos.idmarca = marcas.idmarca " & _
                " inner join subfamilias on productos.idsubfamilia = subfamilias.idsubfamilia where idproducto =  '" & codigo & "' "
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()

                    Dim d, imp, iv, tot As Decimal
                    d = (Val(dr("precio").ToString.Trim) * Val(cantidad)) * (Val(dr("Descuento").ToString.Trim) / 100)
                    imp = (Val(dr("precio").ToString.Trim) * Val(cantidad)) - d
                    iv = imp * (Val(dr("iva").ToString.Trim) / 100)
                    tot = imp + iv

                    DataGridView1.Rows.Add(dr("idproducto").ToString.Trim, cantidad, dr("codigo").ToString.Trim, dr("Subfamilia").ToString.Trim, _
                                           dr("Marca").ToString.Trim, dr("descripcion").ToString.Trim, _
                                           Val(dr("precio").ToString.Trim), Val(dr("Descuento").ToString.Trim), imp, iv, tot)
                End If
                dr.Close()
            End If
        End If

    End Sub
    Private Sub totales()
        Dim c As Integer
        Dim t As Decimal
        Dim suma As Decimal = 0.0
        For Each row As DataGridViewRow In DataGridView1.Rows
            suma = suma + CDec(row.Cells("total").Value)
        Next

        Dim suma2 As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            suma2 = suma2 + CInt(row.Cells("cantidad").Value)
        Next

        c = suma2
        t = suma

        tbtotal.Text = t


    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        If DataGridView1.RowCount > 0 Then

            DataGridView1.Rows.Remove(DataGridView1.CurrentRow)
            totales()

        Else
            MsgBox("No hay renglones", MsgBoxStyle.Exclamation)
        End If
    End Sub
End Class