﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IngresoClientes
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(IngresoClientes))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.lblimite = New DevExpress.XtraEditors.LabelControl()
        Me.lbenvio = New DevExpress.XtraEditors.LabelControl()
        Me.lbnumero = New System.Windows.Forms.Label()
        Me.dtnac = New System.Windows.Forms.DateTimePicker()
        Me.btnSeleccionar = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.chkpromo = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcbarras = New DevExpress.XtraEditors.TextEdit()
        Me.tbcelular = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcontacto = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.tbtelefono = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.tbemail = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.tbrfc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.tbcp = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tbestado = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.tbmunicipio = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.tbnint = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.tbnext = New DevExpress.XtraEditors.TextEdit()
        Me.tbciudad = New DevExpress.XtraEditors.TextEdit()
        Me.tbcolonia = New DevExpress.XtraEditors.TextEdit()
        Me.tbcalle = New DevExpress.XtraEditors.TextEdit()
        Me.tbnombre = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.chkeditar = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Folio = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.chkpromo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcbarras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcelular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcontacto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbtelefono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbemail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbrfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbestado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbmunicipio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbnint.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbnext.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbciudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcolonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbnombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.chkeditar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(720, 399)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.lblimite)
        Me.XtraTabPage1.Controls.Add(Me.lbenvio)
        Me.XtraTabPage1.Controls.Add(Me.lbnumero)
        Me.XtraTabPage1.Controls.Add(Me.dtnac)
        Me.XtraTabPage1.Controls.Add(Me.btnSeleccionar)
        Me.XtraTabPage1.Controls.Add(Me.SimpleButton2)
        Me.XtraTabPage1.Controls.Add(Me.SimpleButton1)
        Me.XtraTabPage1.Controls.Add(Me.chkpromo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage1.Controls.Add(Me.tbcbarras)
        Me.XtraTabPage1.Controls.Add(Me.tbcelular)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage1.Controls.Add(Me.tbcontacto)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage1.Controls.Add(Me.tbtelefono)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage1.Controls.Add(Me.tbemail)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage1.Controls.Add(Me.tbrfc)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage1.Controls.Add(Me.tbcp)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage1.Controls.Add(Me.tbestado)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage1.Controls.Add(Me.tbmunicipio)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl18)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl17)
        Me.XtraTabPage1.Controls.Add(Me.tbnint)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl16)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage1.Controls.Add(Me.tbnext)
        Me.XtraTabPage1.Controls.Add(Me.tbciudad)
        Me.XtraTabPage1.Controls.Add(Me.tbcolonia)
        Me.XtraTabPage1.Controls.Add(Me.tbcalle)
        Me.XtraTabPage1.Controls.Add(Me.tbnombre)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(714, 371)
        Me.XtraTabPage1.Text = "Ingresa Cliente"
        '
        'lblimite
        '
        Me.lblimite.Location = New System.Drawing.Point(441, 210)
        Me.lblimite.Name = "lblimite"
        Me.lblimite.Size = New System.Drawing.Size(0, 13)
        Me.lblimite.TabIndex = 58
        Me.lblimite.Visible = False
        '
        'lbenvio
        '
        Me.lbenvio.Location = New System.Drawing.Point(403, 210)
        Me.lbenvio.Name = "lbenvio"
        Me.lbenvio.Size = New System.Drawing.Size(0, 13)
        Me.lbenvio.TabIndex = 57
        Me.lbenvio.Visible = False
        '
        'lbnumero
        '
        Me.lbnumero.AutoSize = True
        Me.lbnumero.Location = New System.Drawing.Point(192, 69)
        Me.lbnumero.Name = "lbnumero"
        Me.lbnumero.Size = New System.Drawing.Size(0, 13)
        Me.lbnumero.TabIndex = 56
        Me.lbnumero.Visible = False
        '
        'dtnac
        '
        Me.dtnac.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtnac.Location = New System.Drawing.Point(183, 226)
        Me.dtnac.Name = "dtnac"
        Me.dtnac.Size = New System.Drawing.Size(99, 21)
        Me.dtnac.TabIndex = 55
        '
        'btnSeleccionar
        '
        Me.btnSeleccionar.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSeleccionar.Appearance.Options.UseFont = True
        Me.btnSeleccionar.Enabled = False
        Me.btnSeleccionar.Location = New System.Drawing.Point(142, 256)
        Me.btnSeleccionar.Name = "btnSeleccionar"
        Me.btnSeleccionar.Size = New System.Drawing.Size(400, 35)
        Me.btnSeleccionar.TabIndex = 18
        Me.btnSeleccionar.Text = "Seleccionar"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Enabled = False
        Me.SimpleButton2.Location = New System.Drawing.Point(554, 267)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(82, 23)
        Me.SimpleButton2.TabIndex = 19
        Me.SimpleButton2.Text = "Modificar"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(554, 210)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(82, 39)
        Me.SimpleButton1.TabIndex = 17
        Me.SimpleButton1.Text = "Guardar Nuevo"
        '
        'chkpromo
        '
        Me.chkpromo.Location = New System.Drawing.Point(321, 230)
        Me.chkpromo.Name = "chkpromo"
        Me.chkpromo.Properties.Caption = "Recibe Promociones"
        Me.chkpromo.Size = New System.Drawing.Size(123, 19)
        Me.chkpromo.TabIndex = 16
        '
        'LabelControl13
        '
        Me.LabelControl13.Location = New System.Drawing.Point(183, 210)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl13.TabIndex = 54
        Me.LabelControl13.Text = "Fecha Nacimiento"
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(59, 210)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl12.TabIndex = 53
        Me.LabelControl12.Text = "Código de Barras"
        '
        'tbcbarras
        '
        Me.tbcbarras.Location = New System.Drawing.Point(59, 229)
        Me.tbcbarras.Name = "tbcbarras"
        Me.tbcbarras.Size = New System.Drawing.Size(112, 20)
        Me.tbcbarras.TabIndex = 14
        '
        'tbcelular
        '
        Me.tbcelular.Location = New System.Drawing.Point(323, 178)
        Me.tbcelular.Name = "tbcelular"
        Me.tbcelular.Properties.Mask.EditMask = "(999)000-0000"
        Me.tbcelular.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.tbcelular.Size = New System.Drawing.Size(103, 20)
        Me.tbcelular.TabIndex = 12
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(435, 159)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl11.TabIndex = 50
        Me.LabelControl11.Text = "Contacto"
        '
        'tbcontacto
        '
        Me.tbcontacto.Location = New System.Drawing.Point(435, 178)
        Me.tbcontacto.Name = "tbcontacto"
        Me.tbcontacto.Size = New System.Drawing.Size(155, 20)
        Me.tbcontacto.TabIndex = 13
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(323, 159)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl10.TabIndex = 48
        Me.LabelControl10.Text = "Celular"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(214, 159)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl7.TabIndex = 46
        Me.LabelControl7.Text = "Telefono"
        '
        'tbtelefono
        '
        Me.tbtelefono.Location = New System.Drawing.Point(214, 178)
        Me.tbtelefono.Name = "tbtelefono"
        Me.tbtelefono.Properties.Mask.EditMask = "(999)000-0000"
        Me.tbtelefono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
        Me.tbtelefono.Size = New System.Drawing.Size(103, 20)
        Me.tbtelefono.TabIndex = 11
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(59, 159)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(24, 13)
        Me.LabelControl6.TabIndex = 44
        Me.LabelControl6.Text = "Email"
        '
        'tbemail
        '
        Me.tbemail.Location = New System.Drawing.Point(59, 178)
        Me.tbemail.Name = "tbemail"
        Me.tbemail.Size = New System.Drawing.Size(149, 20)
        Me.tbemail.TabIndex = 10
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(519, 114)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(20, 13)
        Me.LabelControl5.TabIndex = 42
        Me.LabelControl5.Text = "RFC"
        '
        'tbrfc
        '
        Me.tbrfc.Location = New System.Drawing.Point(519, 133)
        Me.tbrfc.Name = "tbrfc"
        Me.tbrfc.Properties.Mask.EditMask = "LLLL000000"
        Me.tbrfc.Size = New System.Drawing.Size(117, 20)
        Me.tbrfc.TabIndex = 9
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(444, 114)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl4.TabIndex = 40
        Me.LabelControl4.Text = "Código Postal"
        '
        'tbcp
        '
        Me.tbcp.Location = New System.Drawing.Point(444, 133)
        Me.tbcp.Name = "tbcp"
        Me.tbcp.Properties.Mask.EditMask = "#####"
        Me.tbcp.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbcp.Size = New System.Drawing.Size(69, 20)
        Me.tbcp.TabIndex = 8
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(309, 114)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl3.TabIndex = 38
        Me.LabelControl3.Text = "Estado"
        '
        'tbestado
        '
        Me.tbestado.Location = New System.Drawing.Point(309, 133)
        Me.tbestado.Name = "tbestado"
        Me.tbestado.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbestado.Size = New System.Drawing.Size(121, 20)
        Me.tbestado.TabIndex = 7
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(179, 114)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(43, 13)
        Me.LabelControl2.TabIndex = 36
        Me.LabelControl2.Text = "Municipio"
        '
        'tbmunicipio
        '
        Me.tbmunicipio.Location = New System.Drawing.Point(179, 133)
        Me.tbmunicipio.Name = "tbmunicipio"
        Me.tbmunicipio.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbmunicipio.Size = New System.Drawing.Size(119, 20)
        Me.tbmunicipio.TabIndex = 6
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(507, 69)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(35, 13)
        Me.LabelControl18.TabIndex = 34
        Me.LabelControl18.Text = "Colonia"
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(452, 69)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(38, 13)
        Me.LabelControl17.TabIndex = 33
        Me.LabelControl17.Text = "No. Int."
        '
        'tbnint
        '
        Me.tbnint.Location = New System.Drawing.Point(450, 88)
        Me.tbnint.Name = "tbnint"
        Me.tbnint.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnint.Size = New System.Drawing.Size(46, 20)
        Me.tbnint.TabIndex = 3
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(238, 69)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(23, 13)
        Me.LabelControl16.TabIndex = 31
        Me.LabelControl16.Text = "Calle"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(400, 69)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl9.TabIndex = 24
        Me.LabelControl9.Text = "No. Ext."
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(59, 114)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl8.TabIndex = 23
        Me.LabelControl8.Text = "Ciudad"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(59, 69)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(85, 13)
        Me.LabelControl1.TabIndex = 16
        Me.LabelControl1.Text = "Nombre Completo"
        '
        'tbnext
        '
        Me.tbnext.Location = New System.Drawing.Point(398, 88)
        Me.tbnext.Name = "tbnext"
        Me.tbnext.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnext.Properties.Mask.EditMask = "00000"
        Me.tbnext.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbnext.Size = New System.Drawing.Size(46, 20)
        Me.tbnext.TabIndex = 2
        '
        'tbciudad
        '
        Me.tbciudad.Location = New System.Drawing.Point(59, 133)
        Me.tbciudad.Name = "tbciudad"
        Me.tbciudad.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbciudad.Size = New System.Drawing.Size(109, 20)
        Me.tbciudad.TabIndex = 5
        '
        'tbcolonia
        '
        Me.tbcolonia.Location = New System.Drawing.Point(507, 88)
        Me.tbcolonia.Name = "tbcolonia"
        Me.tbcolonia.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcolonia.Size = New System.Drawing.Size(129, 20)
        Me.tbcolonia.TabIndex = 4
        '
        'tbcalle
        '
        Me.tbcalle.Location = New System.Drawing.Point(238, 88)
        Me.tbcalle.Name = "tbcalle"
        Me.tbcalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbcalle.Size = New System.Drawing.Size(154, 20)
        Me.tbcalle.TabIndex = 1
        '
        'tbnombre
        '
        Me.tbnombre.Location = New System.Drawing.Point(59, 88)
        Me.tbnombre.Name = "tbnombre"
        Me.tbnombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbnombre.Size = New System.Drawing.Size(173, 20)
        Me.tbnombre.TabIndex = 0
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.chkeditar)
        Me.XtraTabPage2.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(714, 371)
        Me.XtraTabPage2.Text = "Localizar Cliente"
        '
        'chkeditar
        '
        Me.chkeditar.Location = New System.Drawing.Point(602, 345)
        Me.chkeditar.Name = "chkeditar"
        Me.chkeditar.Properties.Caption = "Editar Cliente"
        Me.chkeditar.Size = New System.Drawing.Size(92, 19)
        Me.chkeditar.TabIndex = 6
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.GridControl2)
        Me.GroupControl2.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(704, 336)
        Me.GroupControl2.TabIndex = 5
        Me.GroupControl2.Text = "Resultado Busqueda"
        '
        'GridControl2
        '
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.Location = New System.Drawing.Point(2, 21)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(700, 313)
        Me.GridControl2.TabIndex = 41
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Folio})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView2.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowRowSizing = True
        Me.GridView2.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView2.OptionsFind.AlwaysVisible = True
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'Folio
        '
        Me.Folio.Caption = "Folio"
        Me.Folio.Name = "Folio"
        Me.Folio.Visible = True
        Me.Folio.VisibleIndex = 0
        '
        'IngresoClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(720, 399)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "IngresoClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingresar, Localizar Clientes"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.chkpromo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcbarras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcelular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcontacto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbtelefono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbemail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbrfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbestado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbmunicipio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbnint.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbnext.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbciudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcolonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbnombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.chkeditar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents btnSeleccionar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents chkpromo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcbarras As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbcelular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcontacto As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbtelefono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbemail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbrfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbestado As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbmunicipio As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbnint As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbnext As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbciudad As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbcolonia As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbcalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbnombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents dtnac As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbnumero As System.Windows.Forms.Label
    Friend WithEvents lbenvio As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblimite As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkeditar As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Folio As DevExpress.XtraGrid.Columns.GridColumn
End Class
