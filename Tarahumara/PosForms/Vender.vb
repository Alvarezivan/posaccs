﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Drawing2D

Public Class Vender
    Dim cfecha As String
    Dim td1, td2, td3, td4, td5, td6, td7, td8, td9, td10, td11, td12 As String
    Dim socio As Integer = -1
    Dim cod As Integer
    Dim credito As Boolean = False
    Dim fcredito As Boolean = False
    Dim desctomanual As Boolean = False
    Dim pcre As Boolean = False

    Private Sub Vender_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        asiste()
        inicializa()
        loadvender()
        Mostrador()

        If Movimiento = 1 Then
            PCredito.Visible = False
        ElseIf Movimiento = 2 Then
            btnClientes.PerformClick()
        ElseIf Movimiento = 3 Then
            btnEfectivo.Enabled = False
            btnClientes.PerformClick()
            pcre = True
        End If


        Dim qry As String = "SELECT idplazo, nombre from plazos where activo = 1 ORDER BY nombre"
        LlenaCombobox(cbplazo, qry, "nombre", "idplazo", conexion)


        Splash(False)
        Me.ReportViewer1.RefreshReport()
        Me.ReportViewer2.RefreshReport()
    End Sub

    Private Sub PictureBox1_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox1.Click
        Dim myForm2 As New EdoCtaMon
        myForm2.Show()
    End Sub

    Private Sub PictureBox2_Click(sender As System.Object, e As System.EventArgs) Handles PictureBox2.Click
        Dim myForm2 As New GiftCard
        myForm2.Show()
    End Sub

    Private Sub TextEdit3_LostFocus(sender As System.Object, e As System.EventArgs) Handles tbbarcode.LostFocus
        Dim cuantos, posi As Integer
        Dim barcodetrim, codigo As String
        barcodetrim = tbbarcode.Text.Trim
        If barcodetrim.Contains("*") And barcodetrim.Length > 2 Then
            posi = barcodetrim.IndexOf("*")

            If Not barcodetrim.Substring(0, posi) = "" Then
                cuantos = barcodetrim.Substring(0, posi)
            Else
                cuantos = 1
            End If

            codigo = barcodetrim.Substring(posi + 1, barcodetrim.Length - (posi + 1))
        Else
            codigo = barcodetrim
            cuantos = 1
        End If
        If Not tbbarcode.Text = "" Then
            Dim query As String = " Select idProducto from skus where sku =  '" & codigo & "' "
            Dim dr3 As SqlDataReader = bdBase.bdDataReader(conexion, query)
            If Not dr3 Is Nothing Then
                If dr3.HasRows Then
                    dr3.Read()

                    If Not IsDBNull(dr3("idProducto")) Then cod = dr3("idProducto")

                    ingresar(dr3("idproducto").ToString.Trim, cuantos)
                    totales()
                    tbbarcode.Text = ""
                    tbbarcode.Focus()
                ElseIf tbbarcode.Text.Trim = "*" Or tbbarcode.Text.Trim = "/" Then

                Else
                    MsgBox("Codigo no encontrado", MsgBoxStyle.Information)
                    tbbarcode.Text = ""
                    tbbarcode.Focus()

                End If
            End If
            dr3.Close()
        End If
    End Sub

    Private Sub ingresar(codigo As String, cantidad As String)
        Dim j, i As Decimal
        Dim x, val2, pos As Integer
        Dim agregar As Boolean = False
        pos = dgvventas.RowCount - 1
        For x = 0 To pos

            If dgvventas.Item(0, x).Value = cod Then
                j = Val(dgvventas.Item(1, x).Value)

                i = Val(dgvventas.Item(6, x).Value)


                dgvventas.Item(1, x).Value = j + cantidad
                val2 = dgvventas.Item(1, x).Value

                dgvventas.Item(8, x).Value = val2 * i

                agregar = True
            End If
        Next

        If agregar = False Then

            Dim qry = " Select productos.idproducto, productos.Codigo, marcas.Marca, subfamilias.SubFamilia, productos.Precio, " & _
                "productos.descripcion, productos.Iva, productos.Descuento from productos inner join marcas on productos.idmarca = marcas.idmarca " & _
                " inner join subfamilias on productos.idsubfamilia = subfamilias.idsubfamilia where idproducto =  '" & codigo & "' "
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()

                    Dim d, imp, iv, tot As Decimal
                    d = (Val(dr("precio").ToString.Trim) * Val(cantidad)) * (Val(dr("Descuento").ToString.Trim) / 100)
                    imp = (Val(dr("precio").ToString.Trim) * Val(cantidad)) - d

                    dgvventas.Rows.Add(dr("idproducto").ToString.Trim, cantidad, dr("codigo").ToString.Trim, dr("Subfamilia").ToString.Trim, _
                                           dr("Marca").ToString.Trim, dr("descripcion").ToString.Trim, _
                                           Val(dr("precio").ToString.Trim), Val(dr("Descuento").ToString.Trim), imp, iv, tot)
                End If
                dr.Close()
            End If
        End If

    End Sub
    Private Sub totales()
        Dim c As Integer
        Dim t As Decimal
        Dim suma As Decimal = 0.0
        For Each row As DataGridViewRow In dgvventas.Rows
            suma = suma + CDec(row.Cells("Importe").Value)
        Next

        Dim suma2 As Integer = 0
        For Each row As DataGridViewRow In dgvventas.Rows
            suma2 = suma2 + CInt(row.Cells("Cantidad").Value)
        Next

        c = suma2
        t = suma

        lbtotal.Text = t
        lbcant.Text = c
        lbporpagar.Text = t - Val(lbpagos.Text)
        lbtotal.Text = FormatCurrency(lbtotal.Text)
        lbporpagar.Text = FormatCurrency(lbporpagar.Text)
        m1.Visible = True
        m3.Visible = True
        lbt.Visible = True
        lbpp.Visible = True
        lbu.Visible = True

        If credito = True Then
            lbdispact.Text = Val(lbdisp.Text) - t

            If Val(lbdispact.Text) < 0 Then
                Dim dif As Decimal = (0 - Val(lbdispact.Text))
                MsgBox("Ha excedido el disponible si continua debera pagar $ " & dif & " en otra forma de Pago", MsgBoxStyle.Information)
            End If
        End If

    End Sub

    Private Sub Form1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        FuncKeysModule(e.KeyCode)
    End Sub

    Private Sub FuncKeysModule(ByVal value As Keys)
        Select Case value
            Case Keys.F12

                If dgvventas.RowCount > 0 Then
                    Dim j, i, imp As Decimal
                    Dim x, val2 As Integer
                    x = dgvventas.RowCount - 1
                    j = Val(dgvventas.Item(1, x).Value)
                    i = Val(dgvventas.Item(8, x).Value)


                    dgvventas.Item(1, x).Value = j + 1
                    val2 = dgvventas.Item(1, x).Value
                    dgvventas.Item(8, x).Value = val2 * imp

                    totales()
                    Exit Select

                Else
                    MsgBox("No hay productos en la lista", MsgBoxStyle.Information)

                End If

            Case Keys.Enter

                If tbbarcode.Text.Trim = "*" Then
                    tbbarcode.Text = ""
                    tbbarcode.Focus()
                    Dim myForm2 As New Productos
                    myForm2.Show()
                End If

                If tbbarcode.Text.Trim = "/" Then

                    Pop1.Visible = True
                    tbbarcode.Enabled = False

                    tbbarcode.Text = ""
                    tbcod.Focus()
                End If

                If tbbarcode.Text.Length > 2 Then

                    lb1.Focus()
                    tbbarcode.Text = ""
                    tbbarcode.Focus()

                End If
                Exit Select

            Case Keys.F8
                lbefe.Visible = True
                tbefectivo.Visible = True
                tbefectivo.Focus()
                Exit Select

            Case Keys.F9
                bof.PerformClick()
                Exit Select


            Case Keys.F11
                bpagar.Focus()
                bpagar.PerformClick()
                Exit Select

            Case Keys.F10
                SimpleButton18.PerformClick()
                Exit Select

        End Select
    End Sub
    Private Sub SimpleButton6_Click(sender As System.Object, e As System.EventArgs) Handles btnClientes.Click

        Dim clientes As New IngresoClientes


        If clientes.ShowDialog() = DialogResult.OK Then

            If clientes.tbrfc.Text = "" Then

            Else
                lbnombre.Text = clientes.tbnombre.Text.Trim
                lbcliente.Text = clientes.lbnumero.Text.Trim
                lbdireccion.Text = clientes.tbcalle.Text & " " & clientes.tbnext.Text & "-" & clientes.tbnint.Text & "  " & clientes.tbciudad.Text & ", " & clientes.tbestado.Text & ", CP " & clientes.tbcp.Text
                lbtel.Text = clientes.tbtelefono.Text
                lbemail.Text = clientes.tbemail.Text
                lbrfc.Text = clientes.tbrfc.Text
                tbenvio.Text = clientes.lbenvio.Text.Trim
                bactualizar.Enabled = False
                PCredito.Visible = False
                If Movimiento = 3 Then
                    PCredito.Visible = True
                    lblc.Text = clientes.lblimite.Text.Trim
                    verificadisponible()
                    lbdispact.Text = lbdisp.Text

                    btnClientes.Enabled = False

                    If Val(lbdisp.Text) <= 0 Then
                        pbestatus.BackColor = Color.Red
                        MsgBox("Solo se pueden realizar Ventas de Contado al Cliente")
                        btenviarcre.Enabled = False
                        lbdispact.Visible = False
                    Else
                        pbestatus.BackColor = Color.Green
                        credito = True
                        Dim qry As String = "select numero, nombre from subsocios where activo = 1 and idsocio = " & socio
                        LlenaCombobox(cbsubcliente, qry, "nombre", "numero", conexion)
                    End If
                End If

            End If
        End If
    End Sub

    Private Sub btagregar_Click(sender As System.Object, e As System.EventArgs) Handles btagregar.Click

        Dim d, imp As Decimal
        Dim cant As Integer = 1

        d = (Val(tbprec.Text.Trim) * cant) * (Val(tbdesc.Text.Trim) / 100)
        imp = (Val(tbprec.Text.Trim) * cant) - d

        If tbcod.Text.Length > 1 And tbprec.Text.Length > 1 Then

            dgvventas.Rows.Add("-2", cant, tbcod.Text.Trim, -1, -1, tbdescr.Text.Trim.ToUpper, _
                                   Val(tbprec.Text.Trim), tbdesc.Text.Trim, imp)
            tbcod.Text = ""
            tbdescr.Text = ""
            tbprec.Text = ""
            tbdesc.Text = 0
            Pop1.Visible = False
            tbbarcode.Enabled = True
            totales()
        Else
            MsgBox("Datos incompletos", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub SimpleButton5_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton5.Click

        tbcod.Text = ""
        tbdescr.Text = ""
        tbprec.Text = ""
        tbdesc.Text = 0
        Pop1.Visible = False
        tbbarcode.Enabled = True
    End Sub

    Private Sub SimpleButton16_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton16.Click

        Dim myForm2 As New CtrlApartados
        myForm2.Show()

    End Sub
    Private Sub Mostrador()
        Dim qry As String = "Select [numero], [NOMBRE], [TELEFONO], [CALLE],[numeroint],[numeroext], [COLONIA], [CIUDAD], [ESTADO], " & _
                "[CodigoPostal], [RFC], [CORREO],isnull([LIMITECREDITO],0) as Limite from socios where numero = -1 "

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()

                lbnombre.Text = dr("nombre").ToString.Trim
                lbcliente.Text = dr("numero").ToString.Trim
                lbdireccion.Text = dr("calle").ToString.Trim & " " & dr("numeroext").ToString.Trim & "-" & dr("numeroint").ToString.Trim & "  " & dr("ciudad").ToString.Trim & ", " & dr("estado").ToString.Trim & ", CP " & dr("codigopostal").ToString.Trim
                lbtel.Text = dr("telefono").ToString.Trim
                lbemail.Text = dr("correo").ToString.Trim
                lbrfc.Text = dr("rfc").ToString.Trim
                lblc.Text = dr("LIMITE").ToString.Trim

            End If
        End If

        dr.Close()
    End Sub

    Private Sub asiste()
        Dim qry As String = ""
        Dim fechasql As String
        qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        ds.Read()
        fechasql = ds("fecha")
        ds.Close()

        ' Crear temporales
        qry = "CREATE TABLE [empleadoc3] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[FECHA] datetime NULL, [tipoope] nchar(14) NULL)"
        Dim regresa As String = bdBase.bdExecute(conexion, qry)
        qry = "CREATE TABLE [empleadoc2] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[ultimao] datetime NULL)"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Insertar datos 
        qry = "INSERT INTO empleadoc3 (numero, nombre, Usuario, fecha, tipoope) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], logdia.fecha, logdia.tipo FROM empleado " & _
              "inner join logdia on logdia.nombre= empleado.nombre " & _
              "WHERE logdia.TIENDA = '" & My.Settings.Item("TiendaActual") & "' AND  CONVERT(nCHAR(8), logdia.fecha , 112)= '" & fechasql & "' "
        regresa = bdBase.bdExecute(conexion, qry)
        qry = "INSERT INTO empleadoc2 (numero, nombre, Usuario, ultimao) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], MAX(logdia.fecha) from empleado " & _
              "inner join logdia on logdia.nombre = empleado.nombre and DATEPART(year, logdia.fecha)= DATEPART(year, getdate()) " & _
              "and DATEPART(month, logdia.fecha)=DATEPART(month, getdate()) and DATEPART(day, logdia.fecha)=DATEPART(day, getdate()) and logdia.tienda='" & _
              My.Settings.Item("TiendaActual") & "' group by empleado.numero, empleado.nombre, empleado.[USER]  order BY [user]"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Query final
        qry = "Select empleadoc3.numero, empleadoc3.nombre, empleadoc3.usuario From empleadoc2 " & _
              "INNER Join empleadoc3 On empleadoc2.numero= empleadoc3.numero And empleadoc2.ultimao= empleadoc3.fecha " & _
              "Where empleadoc3.tipoope like 'ENTRADA%'"
        LlenaCombobox(cbvendedor, qry, "nombre", "numero", conexion)
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc3') BEGIN DROP TABLE empleadoc3 END")
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc2') BEGIN DROP TABLE empleadoc2 END")

        ' *****************************************************
        ' **** terminado
        ' *****************************************************
        If cbvendedor.Properties.Items.Count >= 1 Then
            cbvendedor.SelectedIndex = 0
        Else
            cbvendedor.SelectedIndex = -1
        End If

        'addtabs()       
    End Sub

    Private Sub loadvender()
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)
        Me.Text = " [Registrar Venta] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre & "  " & lbtienda.Text
        RegistraAcceso(conexion, "Registrar Venta [Vender.frm]")
        btnClientes.Text = "Busqueda, Edición" & vbCrLf & "e Ingreso de Clientes"
        lbcliente.Text = ""
        lbnombre.Text = ""
        lbdireccion.Text = ""
        lbtel.Text = ""
        lbemail.Text = ""
        lbahorro.Text = ""
        tbiva.Text = 16
        tbdesc.Text = 0

        If cbvendedor.Properties.Items.Count = 0 Then
            pc1.Visible = True
            GroupControl3.Enabled = False
            XtraTabPage2.PageVisible = False

        Else
            If cbvendedor.Properties.Items.Count = 1 Then
                cbvendedor.SelectedIndex = 0
                tbbarcode.Focus()
            End If
        End If
    End Sub

    Private Sub SimpleButton3_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton3.Click

        Dim myform2 As New Asistencia

        myform2.Show()
        Me.Close()


    End Sub

    Private Sub inicializa()

        Datagridview3.DataSource = Nothing
        Dim qry As String = "SELECT Clave,Nombre,0.00 as Monto,Tipo,Pagare,ticket from formaspago where status=1 and clave<>'VC' and clave <>'NC' and tienda=" & My.Settings("TiendaActual") & " ORDER BY nombre"
        Dim dsl As DataSet = bdBase.bdDataset(conexion, qry)
        Datagridview3.DataSource = dsl.Tables(0)
        Datagridview3.Columns(0).Visible = False
        Datagridview3.Columns(3).Visible = False
        Datagridview3.Columns(1).ReadOnly = True
        Datagridview3.Columns(4).Visible = False
        Datagridview3.Columns(5).Visible = False
        Dim col As DataGridViewColumn = dgvventas.Columns(2)
        ' Le establecemos un formato decimal.
        'col.DefaultCellStyle.Format = "N2"
        Datagridview3.Columns(2).DefaultCellStyle.Format = "C"
        Datagridview3.Columns(2).ReadOnly = False
        Datagridview3.Columns(0).Visible = False
        Datagridview3.Font = New Font("Arial", 12.0, FontStyle.Regular)
        Datagridview3.AutoResizeColumns()

        'qry = "SELECT CONVERT(nCHAR( 8 ), GETDATE(), 112) as Fecha "
        'Dim ds3 As SqlDataReader = bdBase.bdDataReader(conexionLocal, qry)
        'ds3.Read()
        'cfecha = ds3("Fecha")
        'ds3.Close()
        Dim theDate As Date
        qry = "Select getDate() as FechaHora "
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dr.Read()
        theDate = dr("FechaHora")
        dr.Close()
        cfecha = theDate.ToString("yyyyMMdd")

        qry = " SELECT     tiendas.NUMERO, tiendas.NOMBRE AS Tienda, razones.NOMBRE AS RazonSocial, razones.RFC,Razones.Calle,Razones.Ciudad,Razones.Colonia, Razones.Estado,Razones.Cp, " & _
                          " Tiendas.Calle as CalleTienda, Tiendas.Ciudad as CiudadTienda,Tiendas.estado as EstadoTienda FROM         tiendas INNER JOIN " & _
                          " razones ON tiendas.RAZON = razones.ID " & _
                          " WHERE Tiendas.numero='" & My.Settings.Item("TiendaActual") & _
                          "' ORDER BY Tienda "
        Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        Dim bTieneRen As Boolean = False
        While ds2.Read
            bTieneRen = True
            'Label8.Text = ds2("RazonSocial").ToString.Trim
            lbtienda.Text = ds2("Tienda").ToString.Trim
            lbcaja.Text = My.Settings.CajaActual.ToString.Trim
            'Label21.Text = "RFC: " & ds2("RFC").ToString.Trim
            lbcajero.Text = oLogin.pUsrNombre
            MiCajero = oLogin.pUsrNombre
            If Not IsDBNull(ds2("Numero")) Then td1 = ds2("Numero")
            If Not IsDBNull(ds2("Tienda")) Then td2 = ds2("Tienda")
            If Not IsDBNull(ds2("RazonSocial")) Then td3 = ds2("RazonSocial")
            If Not IsDBNull(ds2("RFC")) Then td4 = ds2("RFC")
            If Not IsDBNull(ds2("Calle")) Then td5 = ds2("Calle")
            If Not IsDBNull(ds2("Ciudad")) Then td6 = ds2("Ciudad")
            If Not IsDBNull(ds2("Colonia")) Then td7 = ds2("Colonia")
            If Not IsDBNull(ds2("Estado")) Then td8 = ds2("Estado")
            If Not IsDBNull(ds2("CP")) Then td9 = ds2("CP")
            If Not IsDBNull(ds2("CalleTienda")) Then td10 = ds2("CalleTienda")
            If Not IsDBNull(ds2("CiudadTienda")) Then td11 = ds2("CiudadTienda")
            If Not IsDBNull(ds2("EstadoTienda")) Then td12 = ds2("EstadoTienda")
            'TextBox1.Text = ds2("Nombre").ToString.Trim & " Caja " & My.Settings.Item("CajaActual")
        End While
        ds2.Close()
        If Not bTieneRen Then
            lbcajero.Text = "Desconocido"
        End If
        Me.Cursor = Cursors.Default
        asiste()
        Me.ReportViewer1.RefreshReport()
        dgvventas.Columns(0).ReadOnly = True
        dgvventas.Columns(1).ReadOnly = True
        dgvventas.Columns(2).ReadOnly = True
        dgvventas.Columns(3).ReadOnly = True
        dgvventas.Columns(4).ReadOnly = True
        dgvventas.Columns(5).ReadOnly = True
        dgvventas.Columns(6).ReadOnly = True
        dgvventas.Columns(7).ReadOnly = True
        dgvventas.Columns(8).ReadOnly = True

        Datagridview3.Font = New Font("Arial", 12.0, FontStyle.Regular)
    End Sub

    Private Sub DataGridView4_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Datagridview3.CellValueChanged
        Dim s As Decimal = 0.0

        For Each row As DataGridViewRow In Datagridview3.Rows
            s = s + CDec(row.Cells("Monto").Value)
        Next

        If lbtotal.Text.Length > 1 Then
            lbpagos.Text = s

            Sumatoria()
        End If

    End Sub

    Private Sub Sumatoria()

        lbcambio.Text = 0
        lbporpagar.Text = CType(lbtotal.Text, Decimal) - CType(lbpagos.Text, Decimal)
        If CType(lbpagos.Text, Decimal) > CType(lbtotal.Text, Decimal) Then
            lbcambio.Text = CType(lbpagos.Text, Decimal) - CType(lbtotal.Text, Decimal)
            lbporpagar.Text = CType(lbporpagar.Text, Decimal) + CType(lbcambio.Text, Decimal)
        End If

        If CType(lbpagos.Text, Decimal) < CType(lbtotal.Text, Decimal) Then
            lbcambio.Text = 0
        End If

        If lbcambio.Text <> "" Then
            lbc.Visible = True
        End If
        lbtotal.Text = FormatCurrency(lbtotal.Text)
        lbpagos.Text = FormatCurrency(lbpagos.Text)
        lbporpagar.Text = FormatCurrency(lbporpagar.Text)
        lbcambio.Text = FormatCurrency(lbcambio.Text)
        lbp.Visible = True
        lbpp.Visible = True
        m1.Visible = True
        m2.Visible = True
        m3.Visible = True
        m4.Visible = True
    End Sub


    Private Sub SimpleButton15_Click(sender As System.Object, e As System.EventArgs) Handles bactualizar.Click
        Dim qry As String = "Update socios set DireccionEnvio = '" & tbenvio.Text.Trim.ToUpper & "' where numero = '" & lbcliente.Text.Trim & "' "
        bdBase.bdExecute(conexion, qry)
        MsgBox("Direccion Actualizada con Exito", MsgBoxStyle.Information)
        XtraTabControl2.SelectedTabPageIndex = 3
    End Sub

    Private Sub tbenvio_TextChanged(sender As Object, e As System.EventArgs) Handles tbenvio.TextChanged
        bactualizar.Enabled = True
    End Sub

    Private Sub tbefectivo_LostFocus(sender As System.Object, e As System.EventArgs) Handles tbefectivo.LostFocus

        If tbefectivo.Text = "" Then

        Else
            Dim s As Decimal = tbefectivo.Text
            If lbtotal.Text.Length > 1 Then
                lbpagos.Text = s
                lbporpagar.Text = CType(lbtotal.Text, Decimal) - s
                If CType(lbpagos.Text, Decimal) > CType(lbtotal.Text, Decimal) Then
                    lbcambio.Text = CType(lbpagos.Text, Decimal) - CType(lbtotal.Text, Decimal)
                    lbporpagar.Text = CType(lbporpagar.Text, Decimal) + CType(lbcambio.Text, Decimal)
                End If

                If CType(lbpagos.Text, Decimal) < CType(lbtotal.Text, Decimal) Then
                    lbcambio.Text = 0
                End If

                If lbcambio.Text <> "" Then
                    lbc.Visible = True
                End If
                lbtotal.Text = FormatCurrency(lbtotal.Text)
                lbpagos.Text = FormatCurrency(lbpagos.Text)
                lbporpagar.Text = FormatCurrency(lbporpagar.Text)
                lbcambio.Text = FormatCurrency(lbcambio.Text)
                lbp.Visible = True
                lbpp.Visible = True
                m1.Visible = True
                m2.Visible = True
                m3.Visible = True
                m4.Visible = True

            End If
        End If
    End Sub

    Private Sub bpagar_Click(sender As System.Object, e As System.EventArgs) Handles bpagar.Click
        If lbporpagar.Text = 0 Then
            Registraventa()
            MsgBox("Venta Registrada con Exito", MsgBoxStyle.Information)
            TipoVenta.Show()
            Me.Close()

        Else
            MsgBox("Aun no se cubre el monto a Pagar", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click

        If dgvventas.RowCount > 0 Then

            dgvventas.Rows.Remove(dgvventas.CurrentRow)
            totales()

        Else
            MsgBox("No hay renglones", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub SimpleButton17_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton17.Click
        Dim myForm2 As New Vistas
        myForm2.Show()
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        dgvventas.Columns(7).ReadOnly = False
        desctomanual = True
    End Sub

    Private Sub dgvventas_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvventas.CellValueChanged



        Dim x As Integer = e.RowIndex
        Dim y As Integer = e.ColumnIndex
        If y = 7 Then

            If rbpesos.Checked Then
                Dim c, u, im, d As Decimal
                c = Val(dgvventas.Item(1, x).Value)
                u = Val(dgvventas.Item(6, x).Value)
                d = Val(dgvventas.Item(7, x).Value)
                im = (c * u) - d
                dgvventas.Item(8, x).Value = im
                totales()
            End If

            If rbporc.Checked Then
                Dim c, u, id, d, dp As Decimal
                c = Val(dgvventas.Item(1, x).Value)
                u = Val(dgvventas.Item(6, x).Value)
                d = Val(dgvventas.Item(7, x).Value)
                dp = (c * u) * (d / 100)
                id = (c * u) - dp
                dgvventas.Item(8, x).Value = id
                totales()
            End If
        End If

    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles btnEfectivo.Click
        tbefectivo.Visible = True
        tbefectivo.Focus()
    End Sub

    Private Sub SimpleButton18_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton18.Click
        Dim rptpro As New RptProductosPV
        
        If rptpro.ShowDialog() = DialogResult.OK Then
            tbbarcode.Text = CodigoRPTPV
            tbbarcode.Focus()
        End If
    End Sub

    Private Sub Registraventa()

        Dim ticket As String = GeneraFolio()
        Dim qry As String

        Dim tienda As Integer = My.Settings.TiendaActual
        Dim caja As Integer = My.Settings.TiendaActual
        Dim vendedor As Integer = CType(cbvendedor.SelectedItem, cInfoCombo).ID
        Dim total As Decimal = lbtotal.Text
        Dim empleado As Integer = oLogin.pUserId
        Dim subcliente As Integer = 0

        If cbsubcliente.SelectedIndex = -1 Then
            subcliente = -1
        Else
            subcliente = CType(cbsubcliente.SelectedItem, cInfoCombo).ID
        End If




        Dim x As Integer
        Dim iEfectivo, iCheque, iTarjetas, iNc, iVales, iCupon, iStatus, iGiro, iNVale, iNumvale, iVisa, iCredito As Decimal
        Dim mastercard, amex, idmov, idvale, idcupon, idprecios, idnotacre, idvaleut, descto, Factura, idfactura, monut, mongen, PrestamoE, d1, d2, d3, d4, d5, d6, d7, d8, d9, d0, cf, vd, vi, gremove As Decimal
        Dim e1, e2, e3, e4, e5, e6, e7, e8, e9, e0 As Decimal
        Dim x0, x1, x2, x3, x4, x5, x6, x7, x8, x9 As Decimal
        iStatus = 0
        iEfectivo = 0
        iCheque = 0
        iNc = 0
        iVales = 0
        iTarjetas = 0
        iCupon = 0
        PrestamoE = 0
        iGiro = 0
        iCredito = 0
        vd = 0
        cf = 0
        vi = 0
        d1 = 0
        d2 = 0
        d3 = 0
        d4 = 0
        d5 = 0
        d6 = 0
        d7 = 0
        d8 = 0
        d9 = 0
        d0 = 0
        cf = 0
        vd = 0
        vi = 0
        gremove = 0
        e1 = 0
        e2 = 0
        e3 = 0
        e4 = 0
        e5 = 0
        e6 = 0
        e7 = 0
        e8 = 0
        e9 = 0
        e0 = 0
        x0 = 0
        x1 = 0
        x2 = 0
        x3 = 0
        x4 = 0
        x5 = 0
        x6 = 0
        x7 = 0
        x8 = 0
        x9 = 0
        For x = 0 To Datagridview3.Rows.Count - 1
            Select Case Datagridview3.Item(0, x).Value
                Case Is = "NC"
                    iNc = Datagridview3.Item(2, x).Value
                Case Is = "VC"
                    iVales = Datagridview3.Item(2, x).Value
                Case Is = "EF"
                    iEfectivo = Datagridview3.Item(2, x).Value
                Case Is = "CH"
                    iCheque = Datagridview3.Item(2, x).Value
                Case Is = "TC"
                    iTarjetas = Datagridview3.Item(2, x).Value
                Case Is = "CU"
                    iCupon = Datagridview3.Item(2, x).Value
                Case Is = "DE"
                    iGiro = Datagridview3.Item(2, x).Value
                Case Is = "VD"
                    vd = Datagridview3.Item(2, x).Value
                Case Is = "CF"
                    cf = Datagridview3.Item(2, x).Value
                Case Is = "VI"
                    vi = Datagridview3.Item(2, x).Value
                Case Is = "D1"
                    d1 = Datagridview3.Item(2, x).Value
                Case Is = "D2"
                    d2 = Datagridview3.Item(2, x).Value
                Case Is = "D3"
                    d3 = Datagridview3.Item(2, x).Value
                Case Is = "D4"
                    d4 = Datagridview3.Item(2, x).Value
                Case Is = "D5"
                    d5 = Datagridview3.Item(2, x).Value
                Case Is = "D6"
                    d6 = Datagridview3.Item(2, x).Value
                Case Is = "D7"
                    d7 = Datagridview3.Item(2, x).Value
                Case Is = "D8"
                    d8 = Datagridview3.Item(2, x).Value
                Case Is = "D9"
                    d9 = Datagridview3.Item(2, x).Value
                Case Is = "D0"
                    d0 = Datagridview3.Item(2, x).Value
                Case Is = "E1"
                    e1 = Datagridview3.Item(2, x).Value
                Case Is = "E2"
                    e2 = Datagridview3.Item(2, x).Value
                Case Is = "E3"
                    e3 = Datagridview3.Item(2, x).Value
                Case Is = "E4"
                    e4 = Datagridview3.Item(2, x).Value
                Case Is = "E5"
                    e5 = Datagridview3.Item(2, x).Value
                Case Is = "E6"
                    e6 = Datagridview3.Item(2, x).Value
                Case Is = "E7"
                    e7 = Datagridview3.Item(2, x).Value
                Case Is = "E8"
                    e8 = Datagridview3.Item(2, x).Value
                Case Is = "E9"
                    e9 = Datagridview3.Item(2, x).Value
                Case Is = "E0"
                    e0 = Datagridview3.Item(2, x).Value
                Case Is = "X0"
                    x0 = Datagridview3.Item(2, x).Value
                Case Is = "X1"
                    x1 = Datagridview3.Item(2, x).Value
                Case Is = "X2"
                    x2 = Datagridview3.Item(2, x).Value
                Case Is = "X3"
                    x3 = Datagridview3.Item(2, x).Value
                Case Is = "X4"
                    x4 = Datagridview3.Item(2, x).Value
                Case Is = "X5"
                    x5 = Datagridview3.Item(2, x).Value
                Case Is = "X6"
                    x6 = Datagridview3.Item(2, x).Value
                Case Is = "X7"
                    x7 = Datagridview3.Item(2, x).Value
                Case Is = "X8"
                    x8 = Datagridview3.Item(2, x).Value
                Case Is = "X9"
                    x9 = Datagridview3.Item(2, x).Value

            End Select
        Next
        For x = 0 To DataGridView4.Rows.Count - 1
            Select Case DataGridView4.Item(0, x).Value
                Case Is = "NC"
                    iNc = iNc + DataGridView4.Item(3, x).Value
                Case Is = "VC"
                    iVales = iVales + DataGridView4.Item(3, x).Value
                Case Is = "CR"
                    iCredito = iCredito + DataGridView4.Item(3, x).Value
                Case Is = "MU"
                    monut = monut + DataGridView4.Item(3, x).Value
            End Select
        Next
        If tbefectivo.Text = "" Then
            qry = "Insert Into fma (ticket,efectivo,cheques,tarjetas,nc,vales,cupon,credito,Status,fecha,tienda,caja,socio,Total,nvale,numvale,pagare,visa," & _
             "mastercard,amex,idvale,idcupon,idprecios,idnotacre,idvaleut,descto,factura,monut,mongen,PrestamoE,idfactura,d1,d2,d3,d4,d5,d6,d7,d8,d9,d0,cf,vd,vi,gremove," & _
             "e1,e2,e3,e4,e5,e6,e7,e8,e9,e0,giro,otras,aprobacion, llave, fechaope,observaciones,tipocam,subcliente)" & _
             " values ('" & ticket & "'," & iEfectivo & "," & _
             iCheque & "," & iTarjetas & "," & iNc & "," & iVales & "," & _
             iCupon & "," & iCredito & "," & iStatus & ",'" & cfecha & "'," & My.Settings.TiendaActual & "," & _
             My.Settings.CajaActual & "," & socio & "," & total & "," & iNVale & "," & _
             iNumvale & ",'" & ticket & "','" & iVisa & "','" & mastercard & "','" & amex & _
             "'," & 1 & "," & idcupon & "," & idprecios & _
             "," & idnotacre & "," & idvaleut & "," & descto & ",'" & Factura & "','" & monut & "','" & _
             mongen & "','" & _
             PrestamoE & "','" & _
             idfactura & "','" & _
             d1 & "','" & _
             d2 & "','" & _
             d3 & "','" & _
             d4 & "','" & _
             d5 & "','" & _
             d6 & "','" & _
             d7 & "','" & _
             d8 & "','" & _
             d9 & "','" & _
             d0 & "','" & _
             cf & "','" & _
             vd & "','" & _
             vi & "','" & _
             gremove & "','" & _
             e1 & "','" & _
             e2 & "','" & _
             e3 & "','" & _
             e4 & "','" & _
             e5 & "','" & _
             e6 & "','" & _
             e7 & "','" & _
             e8 & "','" & _
             e9 & "','" & _
             e0 & "','" & _
             0 & "','" & _
             0 & "','" & _
             Textbox35.Text.Trim & "', newid() ,getdate(),'" & textbox28.Text.Trim & "','" & My.Settings.tipoCambio & "','" & subcliente & "')"

            bdBase.bdExecute(conexion, qry)

        Else

            iEfectivo = tbefectivo.Text
            qry = "Insert Into fma (ticket,efectivo,cheques,tarjetas,nc,vales,cupon,credito,Status,fecha,tienda,caja,socio,Total,nvale,numvale,pagare,visa," & _
              "mastercard,amex,idvale,idcupon,idprecios,idnotacre,idvaleut,descto,factura,monut,mongen,PrestamoE,idfactura,d1,d2,d3,d4,d5,d6,d7,d8,d9,d0,cf,vd,vi,gremove," & _
              "e1,e2,e3,e4,e5,e6,e7,e8,e9,e0,giro,otras,aprobacion, llave, fechaope,observaciones,tipocam,subcliente)" & _
              " values ('" & ticket & "'," & iEfectivo & "," & _
              iCheque & "," & iTarjetas & "," & iNc & "," & iVales & "," & _
              iCupon & "," & iCredito & "," & iStatus & ",'" & cfecha & "'," & My.Settings.TiendaActual & "," & _
              My.Settings.CajaActual & "," & socio & "," & total & "," & iNVale & "," & _
              iNumvale & ",'" & ticket & "','" & iVisa & "','" & mastercard & "','" & amex & _
              "'," & 1 & "," & idcupon & "," & idprecios & _
              "," & idnotacre & "," & idvaleut & "," & descto & ",'" & Factura & "','" & monut & "','" & _
              mongen & "','" & _
              PrestamoE & "','" & _
              idfactura & "','" & _
              d1 & "','" & _
              d2 & "','" & _
              d3 & "','" & _
              d4 & "','" & _
              d5 & "','" & _
              d6 & "','" & _
              d7 & "','" & _
              d8 & "','" & _
              d9 & "','" & _
              d0 & "','" & _
              cf & "','" & _
              vd & "','" & _
              vi & "','" & _
              gremove & "','" & _
              e1 & "','" & _
              e2 & "','" & _
              e3 & "','" & _
              e4 & "','" & _
              e5 & "','" & _
              e6 & "','" & _
              e7 & "','" & _
              e8 & "','" & _
              e9 & "','" & _
              e0 & "','" & _
              0 & "','" & _
              0 & "','" & _
              Textbox35.Text.Trim & "', newid() ,getdate(),'" & textbox28.Text.Trim & "','" & My.Settings.tipoCambio & "','" & subcliente & "')"
            bdBase.bdExecute(conexion, qry)

        End If



        If fcredito Then

            Dim plazo As Integer = 0
            Dim interes As Decimal = 0



            qry = "select plazodias,tasainteres from socios where numero = " & socio

            Dim dr As SqlDataReader

            dr = bdBase.bdDataReader(conexion, qry)

            If dr.HasRows Then
                dr.Read()
                plazo = dr.Item(0).ToString
                interes = dr.Item(1).ToString
                dr.Close()
            End If

            Dim hoy As DateTime = Now.Date
            Dim fecha1 As DateTime = hoy.AddDays(plazo)
            Dim vence As String = Format(fecha1, "yyyyMMdd")

            qry = "insert into creditos (tienda,numero,cliente,subcliente,total,inicial,fecha,pares,status,empleado,vence, " & _
                "plazo,caja,socio,finiint,foraneo,pagos,secuencia,interes,corte,id,llave,Efectivo) values ( '" & tienda & "','" & ticket & "','" & _
                             socio & "','" & subcliente & "','" & total & "','" & total & "',getdate(),1,0,'" & vendedor & "','" & vence & "','" & plazo & _
                             "','" & caja & "','" & socio & "','" & vence & "','0','1','1','" & interes & "','1',(select max(id) from creditos) + 1 , newid(),0)"

            bdBase.bdExecute(conexion, qry)

        End If

        Dim i As Integer
        Dim y As Integer = dgvventas.Rows.Count - 1
        If y > 0 Then
            For i = 0 To y
                Dim idproducto As Integer = dgvventas.Item(0, i).Value.ToString.Trim
                Dim cantidad As Integer = dgvventas.Item(1, i).Value
                Dim pre As Decimal = dgvventas.Item(6, i).Value
                Dim desc As Integer = dgvventas.Item(7, i).Value

                Afectaexistencias(tienda, idproducto, cantidad)
                qry = "insert into detnotas (tienda,caja,numero,idproducto,cantidad, " & _
                    "precio,descuen,empleado,socio,fecha,llave) values ('" & tienda & "','" & caja & "','" & _
                 ticket & "','" & idproducto & _
                 "','" & cantidad & "','" & pre & "','" & _
                  desc & "','" & vendedor & "','" & socio & "','" & cfecha & "',newid())"
                bdBase.bdExecute(conexion, qry)
            Next
        End If

    End Sub


    Private Function GeneraFolio()

        Dim qry As String = String.Empty
        Dim NumeroNota As String
        Dim FechaActual As String
        qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
        Dim conexionTicketVenta As String = conexion

        Dim ds As SqlDataReader = bdBase.bdDataReader(conexionTicketVenta, qry)
        ds.Read()
        FechaActual = ds("fecha")
        ds.Close()

        'qry = "SELECT CASE WHEN ticket IS NULL THEN 0 ELSE MAX(RIGHT(CONVERT(nchar(15), ticket), 5)) END AS Consecutivo " & _
        '      " FROM fma WHERE (left(CONVERT(nchar(20), ticket), 10) = '" & _
        '      My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "')"
        '
        'qry = "SELECT CASE WHEN MAX(SUBSTRING(CONVERT(nchar(15), ticket), LEN(CONVERT(nchar(15), ticket)) - 4, 4)) IS NULL THEN 0 ELSE " & _
        '      "MAX(SUBSTRING(CONVERT(nchar(15), ticket), LEN(CONVERT(nchar(15), ticket)) - 4, 4)) END AS Consecutivo " & _
        '      "FROM fma WHERE CONVERT(nchar(10), ticket) = '" & _
        '      My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "'"
        Dim nLen As Integer = Len(My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0"))
        Dim nLen2 As Integer = nLen + 1
        qry = "SELECT CASE WHEN MAX(SUBSTRING(CONVERT(nchar, ticket), " & nLen2 & ", LEN(ticket) - " & nLen & ")) IS NULL THEN 0 ELSE " & _
              "MAX(SUBSTRING(CONVERT(nchar, ticket), " & nLen2 & ", LEN(ticket) - " & nLen & ")) END AS Consecutivo " & _
              "FROM fma WHERE ticket LIKE '" & _
              My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "%'"

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexionTicketVenta, qry)
        dr.Read()
        Dim conse As Integer
        conse = dr("Consecutivo") + 1
        dr.Close()
        NumeroNota = My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & conse.ToString.PadLeft(4, "0")
        Return NumeroNota
    End Function

    Private Sub Afectaexistencias(ByVal tienda As Integer, ByVal id As Integer, ByVal cantidad As Integer)

        Dim qry As String
        Dim dr As SqlDataReader

        qry = "select cantidad from existen where tienda = " & tienda & " and idproducto = " & id

        dr = bdBase.bdDataReader(conexion, qry)

        dr.Read()

        If dr.HasRows Then
            qry = "update existen set cantidad = (select cantidad from existen where tienda = " & tienda & " and idproducto = " & id & ") - " & cantidad & " where tienda = " & tienda & " and idproducto = " & id
            bdBase.bdExecute(conexion, qry)
        Else
            qry = "insert into existen (IdProducto,TIENDA,cantidad,transito,llave)values('" & id & "','" & tienda & "','" & 0 - cantidad & "',0, newid())"
            bdBase.bdExecute(conexion, qry)
        End If
    End Sub


    Private Sub SimpleButton11_Click(sender As System.Object, e As System.EventArgs) Handles btenviarcre.Click
        Dim disponible As Decimal
        Dim pagos As Decimal = 0


        If lbpagos.Text <> "" Then
            pagos = Val(lbpagos.Text)
        End If


        disponible = Val(tbdisponible.Text)
        If disponible < Val(tbacredito.Text) Then
            MsgBox("No tiene suficiente disponible", MsgBoxStyle.Information)
            Exit Sub
        End If
        btenviarcre.Enabled = False
        Dim tipo As String
        tipo = "CR"
        DataGridView4.Rows.Add()
        DataGridView4.Item(0, DataGridView4.RowCount - 1).Value = tipo
        DataGridView4.Item(1, DataGridView4.RowCount - 1).Value = tipo
        DataGridView4.Item(2, DataGridView4.RowCount - 1).Value = -1
        DataGridView4.Item(3, DataGridView4.RowCount - 1).Value = tbacredito.Text
        lbpagos.Text = pagos + Val(tbacredito.Text)
        Sumatoria()
        fcredito = True
    End Sub

    Private Sub SimpleButton15_Click_1(sender As System.Object, e As System.EventArgs) Handles bof.Click
        XtraTabControl1.SelectedTabPage = XtraTabPage2

        If pcre = True Then
            XtraTabControl4.SelectedTabPage = XtraTabPage11
            verificadisponible()
            If Val(tbdisponible.Text) > Val(lbporpagar.Text) Then
                tbacredito.Text = CType(lbporpagar.Text, Decimal)
            End If
        End If
    End Sub


    Private Sub verificadisponible()
        Dim bHabilita As Boolean = True
        Dim qry As String = ""
        socio = lbcliente.Text

        qry = " SELECT  CASE WHEN socios.LIMITEcredito IS NULL THEN 0 ELSE socios.LIMITEcredito END as Limite, CASE WHEN SUM(creditos.TOTAL) IS NULL THEN 0 ELSE SUM(creditos.TOTAL) END AS Saldo, " & _
                  " CASE WHEN min(creditos.VENCE) IS NULL THEN getdate()+30 ELSE min(creditos.VENCE) END AS Vence, CASE WHEN socios.PLAZODIAS IS NULL THEN 0 ELSE socios.PLAZODIAS END AS PLAZODIAS, " & _
                  "CASE WHEN socios.NUMPAGOS IS NULL THEN 0 ELSE socios.NUMPAGOS END AS NUMPAGOS, CASE WHEN socios.TasaInteres IS NULL THEN 0 ELSE socios.TasaInteres END AS TasaInteres  FROM       " & _
                  "  socios left JOIN  creditos ON socios.NUMERO = creditos.SOCIO   " & _
                  "	WHERE(socios.id = '" & socio & "') and isnull(creditos.total,0) > -1  GROUP BY socios.limitecredito, socios.PLAZODIAS, socios.NUMPAGOS, socios.TasaInteres  "
        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        ds.Read()
        tblimite.Text = ds("LIMITE")
        tbdisponible.Text = ds("LIMITE") - ds("saldo")
        lbdisp.Text = ds("LIMITE") - ds("saldo")
        ds.Close()
    End Sub
End Class