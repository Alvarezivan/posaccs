﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CorteCaja
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CorteCaja))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton8 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton7 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton6 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton13 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit5 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.ReportViewer2 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton10 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton11 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit4 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton9 = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton15 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl5 = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit6 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton14 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit7 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage18 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton16 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl6 = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit8 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit5 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton17 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit9 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage19 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl7 = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit10 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit6 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton18 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit11 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage20 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl8 = New DevExpress.XtraGrid.GridControl()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit12 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit7 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton19 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit13 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl9 = New DevExpress.XtraGrid.GridControl()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit14 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit8 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton20 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit15 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage7 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl3 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage21 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl10 = New DevExpress.XtraGrid.GridControl()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit16 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit9 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton21 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit17 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage22 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl11 = New DevExpress.XtraGrid.GridControl()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit18 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit10 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton22 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit19 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage23 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl12 = New DevExpress.XtraGrid.GridControl()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit20 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit11 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton23 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit21 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage24 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton25 = New DevExpress.XtraEditors.SimpleButton()
        Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit()
        Me.GridControl13 = New DevExpress.XtraGrid.GridControl()
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit12 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton24 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage8 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl4 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage25 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl14 = New DevExpress.XtraGrid.GridControl()
        Me.GridView14 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit22 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit13 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton26 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit23 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage26 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl15 = New DevExpress.XtraGrid.GridControl()
        Me.GridView15 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit24 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit14 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton27 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit25 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage9 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl16 = New DevExpress.XtraGrid.GridControl()
        Me.GridView16 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit15 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton28 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit27 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage10 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl17 = New DevExpress.XtraGrid.GridControl()
        Me.GridView17 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit16 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton29 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit26 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage11 = New DevExpress.XtraTab.XtraTabPage()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton31 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.ReportViewer3 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.SimpleButton30 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.XtraTabPage12 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl18 = New DevExpress.XtraGrid.GridControl()
        Me.GridView18 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit17 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton32 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit28 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage13 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton33 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl19 = New DevExpress.XtraGrid.GridControl()
        Me.GridView19 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit29 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit18 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton34 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit30 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage14 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl24 = New DevExpress.XtraGrid.GridControl()
        Me.GridView24 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridControl23 = New DevExpress.XtraGrid.GridControl()
        Me.GridView23 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl65 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit23 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton40 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit36 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl66 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage15 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl20 = New DevExpress.XtraGrid.GridControl()
        Me.GridView20 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit19 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton36 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit32 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage16 = New DevExpress.XtraTab.XtraTabPage()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl61 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit21 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton35 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl21 = New DevExpress.XtraGrid.GridControl()
        Me.GridView21 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit31 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit20 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton37 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit33 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage17 = New DevExpress.XtraTab.XtraTabPage()
        Me.SimpleButton38 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl22 = New DevExpress.XtraGrid.GridControl()
        Me.GridView22 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl62 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit34 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl63 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit22 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton39 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEdit35 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl64 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit6.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit7.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.XtraTabPage18.SuspendLayout()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit8.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit9.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage19.SuspendLayout()
        CType(Me.GridControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit10.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit11.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage20.SuspendLayout()
        CType(Me.GridControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit12.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit13.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.GridControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit14.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit15.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage7.SuspendLayout()
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl3.SuspendLayout()
        Me.XtraTabPage21.SuspendLayout()
        CType(Me.GridControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit16.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit17.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage22.SuspendLayout()
        CType(Me.GridControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit18.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit19.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage23.SuspendLayout()
        CType(Me.GridControl12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit20.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit21.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage24.SuspendLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage8.SuspendLayout()
        CType(Me.XtraTabControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl4.SuspendLayout()
        Me.XtraTabPage25.SuspendLayout()
        CType(Me.GridControl14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit22.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit23.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit23.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage26.SuspendLayout()
        CType(Me.GridControl15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit24.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit24.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit25.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit25.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage9.SuspendLayout()
        CType(Me.GridControl16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit27.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit27.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage10.SuspendLayout()
        CType(Me.GridControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit26.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit26.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage11.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage12.SuspendLayout()
        CType(Me.GridControl18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit28.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit28.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage13.SuspendLayout()
        CType(Me.GridControl19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit29.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit29.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit30.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit30.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage14.SuspendLayout()
        CType(Me.GridControl24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit23.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit36.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit36.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage15.SuspendLayout()
        CType(Me.GridControl20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit32.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit32.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage16.SuspendLayout()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit21.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit31.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit31.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit33.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit33.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage17.SuspendLayout()
        CType(Me.GridControl22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit34.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit34.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit35.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit35.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.[True]
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(1058, 567)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6, Me.XtraTabPage7, Me.XtraTabPage8, Me.XtraTabPage9, Me.XtraTabPage10, Me.XtraTabPage11, Me.XtraTabPage12, Me.XtraTabPage13, Me.XtraTabPage14, Me.XtraTabPage15, Me.XtraTabPage16, Me.XtraTabPage17})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage1.Text = "Valores en Caja"
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.ReportViewer1)
        Me.GroupControl2.Location = New System.Drawing.Point(628, 13)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(417, 475)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Ticket Corte de Caja"
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.Location = New System.Drawing.Point(2, 21)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(413, 452)
        Me.ReportViewer1.TabIndex = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.DataGridView3)
        Me.GroupControl1.Controls.Add(Me.DataGridView2)
        Me.GroupControl1.Controls.Add(Me.DataGridView1)
        Me.GroupControl1.Controls.Add(Me.GroupControl3)
        Me.GroupControl1.Location = New System.Drawing.Point(3, 13)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(619, 475)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Resumen del Día"
        '
        'DataGridView3
        '
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Location = New System.Drawing.Point(8, 159)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(601, 57)
        Me.DataGridView3.TabIndex = 4
        '
        'DataGridView2
        '
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(8, 96)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(601, 57)
        Me.DataGridView2.TabIndex = 3
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(8, 33)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(601, 57)
        Me.DataGridView1.TabIndex = 2
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.SimpleButton5)
        Me.GroupControl3.Controls.Add(Me.SimpleButton4)
        Me.GroupControl3.Controls.Add(Me.SimpleButton3)
        Me.GroupControl3.Controls.Add(Me.SimpleButton2)
        Me.GroupControl3.Controls.Add(Me.CheckEdit1)
        Me.GroupControl3.Controls.Add(Me.SimpleButton1)
        Me.GroupControl3.Controls.Add(Me.TextEdit1)
        Me.GroupControl3.Controls.Add(Me.LabelControl3)
        Me.GroupControl3.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.DateEdit1)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Location = New System.Drawing.Point(357, 222)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(257, 248)
        Me.GroupControl3.TabIndex = 1
        Me.GroupControl3.Text = "Controles"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Location = New System.Drawing.Point(41, 218)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(185, 23)
        Me.SimpleButton5.TabIndex = 11
        Me.SimpleButton5.Text = "Enviar Corte por Email"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Location = New System.Drawing.Point(41, 189)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(185, 23)
        Me.SimpleButton4.TabIndex = 10
        Me.SimpleButton4.Text = "Generar NC del Día"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Location = New System.Drawing.Point(41, 131)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(185, 23)
        Me.SimpleButton3.TabIndex = 9
        Me.SimpleButton3.Text = "Registrar Gastos de Caja"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Location = New System.Drawing.Point(41, 160)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(185, 23)
        Me.SimpleButton2.TabIndex = 8
        Me.SimpleButton2.Text = "Generar Factura del Día"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(159, 33)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "Tienda Actual"
        Me.CheckEdit1.Size = New System.Drawing.Size(93, 19)
        Me.CheckEdit1.TabIndex = 7
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(79, 100)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(100, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Mostrar Corte"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(186, 68)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(40, 20)
        Me.TextEdit1.TabIndex = 5
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(157, 76)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(22, 13)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Caja"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(51, 69)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit1.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(16, 76)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Tienda"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(51, 32)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(16, 35)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(29, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Fecha"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GroupControl5)
        Me.XtraTabPage2.Controls.Add(Me.GroupControl4)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage2.Controls.Add(Me.DateEdit3)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage2.Controls.Add(Me.ComboBoxEdit2)
        Me.XtraTabPage2.Controls.Add(Me.SimpleButton6)
        Me.XtraTabPage2.Controls.Add(Me.DateEdit2)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage2.Text = "Ventas por Cliente"
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.SimpleButton8)
        Me.GroupControl5.Controls.Add(Me.GridControl2)
        Me.GroupControl5.Location = New System.Drawing.Point(537, 99)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(499, 389)
        Me.GroupControl5.TabIndex = 8
        Me.GroupControl5.Text = "Detalle del Cliente"
        '
        'SimpleButton8
        '
        Me.SimpleButton8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton8.Location = New System.Drawing.Point(378, 361)
        Me.SimpleButton8.Name = "SimpleButton8"
        Me.SimpleButton8.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton8.TabIndex = 11
        Me.SimpleButton8.Text = "Imprimir/Exportar"
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.Location = New System.Drawing.Point(4, 24)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(480, 331)
        Me.GridControl2.TabIndex = 10
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        '
        'GroupControl4
        '
        Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl4.Controls.Add(Me.SimpleButton7)
        Me.GroupControl4.Controls.Add(Me.GridControl1)
        Me.GroupControl4.Location = New System.Drawing.Point(11, 99)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(495, 389)
        Me.GroupControl4.TabIndex = 7
        Me.GroupControl4.Text = "Cliente Global"
        '
        'SimpleButton7
        '
        Me.SimpleButton7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton7.Location = New System.Drawing.Point(380, 361)
        Me.SimpleButton7.Name = "SimpleButton7"
        Me.SimpleButton7.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton7.TabIndex = 9
        Me.SimpleButton7.Text = "Imprimir/Exportar"
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(5, 24)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(481, 331)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(244, 16)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl6.TabIndex = 6
        Me.LabelControl6.Text = "Tienda"
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = Nothing
        Me.DateEdit3.Location = New System.Drawing.Point(117, 35)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit3.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit3.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(117, 16)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl5.TabIndex = 4
        Me.LabelControl5.Text = "Al:"
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(244, 35)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit2.TabIndex = 3
        '
        'SimpleButton6
        '
        Me.SimpleButton6.Location = New System.Drawing.Point(422, 32)
        Me.SimpleButton6.Name = "SimpleButton6"
        Me.SimpleButton6.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton6.TabIndex = 2
        Me.SimpleButton6.Text = "Buscar"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(11, 35)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(11, 16)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl4.TabIndex = 0
        Me.LabelControl4.Text = "Del:"
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.SimpleButton13)
        Me.XtraTabPage3.Controls.Add(Me.DateEdit5)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage3.Controls.Add(Me.GroupControl8)
        Me.XtraTabPage3.Controls.Add(Me.GroupControl6)
        Me.XtraTabPage3.Controls.Add(Me.GroupControl7)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage3.Controls.Add(Me.DateEdit4)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage3.Controls.Add(Me.ComboBoxEdit3)
        Me.XtraTabPage3.Controls.Add(Me.SimpleButton9)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage3.Text = "Relacion de Tickets"
        '
        'SimpleButton13
        '
        Me.SimpleButton13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton13.Location = New System.Drawing.Point(942, 30)
        Me.SimpleButton13.Name = "SimpleButton13"
        Me.SimpleButton13.Size = New System.Drawing.Size(94, 23)
        Me.SimpleButton13.TabIndex = 17
        Me.SimpleButton13.Text = "Actualizar Fecha"
        '
        'DateEdit5
        '
        Me.DateEdit5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateEdit5.EditValue = Nothing
        Me.DateEdit5.Location = New System.Drawing.Point(827, 33)
        Me.DateEdit5.Name = "DateEdit5"
        Me.DateEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit5.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit5.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit5.TabIndex = 16
        '
        'LabelControl9
        '
        Me.LabelControl9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl9.Location = New System.Drawing.Point(788, 40)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl9.TabIndex = 15
        Me.LabelControl9.Text = "Fecha:"
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.ReportViewer2)
        Me.GroupControl8.Location = New System.Drawing.Point(750, 99)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(299, 389)
        Me.GroupControl8.TabIndex = 14
        Me.GroupControl8.Text = "Ticket"
        '
        'ReportViewer2
        '
        Me.ReportViewer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer2.Location = New System.Drawing.Point(2, 21)
        Me.ReportViewer2.Name = "ReportViewer2"
        Me.ReportViewer2.Size = New System.Drawing.Size(295, 366)
        Me.ReportViewer2.TabIndex = 13
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.SimpleButton10)
        Me.GroupControl6.Controls.Add(Me.GridControl3)
        Me.GroupControl6.Location = New System.Drawing.Point(386, 99)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(358, 389)
        Me.GroupControl6.TabIndex = 13
        Me.GroupControl6.Text = "Detalle del Ticket"
        '
        'SimpleButton10
        '
        Me.SimpleButton10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton10.Location = New System.Drawing.Point(237, 361)
        Me.SimpleButton10.Name = "SimpleButton10"
        Me.SimpleButton10.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton10.TabIndex = 11
        Me.SimpleButton10.Text = "Imprimir/Exportar"
        '
        'GridControl3
        '
        Me.GridControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl3.Location = New System.Drawing.Point(4, 24)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(339, 331)
        Me.GridControl3.TabIndex = 10
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl7.Controls.Add(Me.SimpleButton11)
        Me.GroupControl7.Controls.Add(Me.GridControl4)
        Me.GroupControl7.Location = New System.Drawing.Point(10, 99)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(370, 389)
        Me.GroupControl7.TabIndex = 12
        Me.GroupControl7.Text = "Listado Global"
        '
        'SimpleButton11
        '
        Me.SimpleButton11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton11.Location = New System.Drawing.Point(255, 361)
        Me.SimpleButton11.Name = "SimpleButton11"
        Me.SimpleButton11.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton11.TabIndex = 9
        Me.SimpleButton11.Text = "Imprimir/Exportar"
        '
        'GridControl4
        '
        Me.GridControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl4.Location = New System.Drawing.Point(5, 24)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(356, 331)
        Me.GridControl4.TabIndex = 0
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(141, 14)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl7.TabIndex = 11
        Me.LabelControl7.Text = "Tienda"
        '
        'DateEdit4
        '
        Me.DateEdit4.EditValue = Nothing
        Me.DateEdit4.Location = New System.Drawing.Point(14, 33)
        Me.DateEdit4.Name = "DateEdit4"
        Me.DateEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit4.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit4.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit4.TabIndex = 10
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(14, 14)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl8.TabIndex = 9
        Me.LabelControl8.Text = "Fecha:"
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(141, 33)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit3.TabIndex = 8
        '
        'SimpleButton9
        '
        Me.SimpleButton9.Location = New System.Drawing.Point(319, 30)
        Me.SimpleButton9.Name = "SimpleButton9"
        Me.SimpleButton9.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton9.TabIndex = 7
        Me.SimpleButton9.Text = "Buscar"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.SimpleButton15)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl13)
        Me.XtraTabPage4.Controls.Add(Me.GridControl5)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage4.Controls.Add(Me.DateEdit6)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage4.Controls.Add(Me.ComboBoxEdit4)
        Me.XtraTabPage4.Controls.Add(Me.SimpleButton14)
        Me.XtraTabPage4.Controls.Add(Me.DateEdit7)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage4.Text = "Productos Vendidos"
        '
        'SimpleButton15
        '
        Me.SimpleButton15.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton15.Location = New System.Drawing.Point(882, 32)
        Me.SimpleButton15.Name = "SimpleButton15"
        Me.SimpleButton15.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton15.TabIndex = 16
        Me.SimpleButton15.Text = "Imprimir/Exportar"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl13.Location = New System.Drawing.Point(514, 41)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(300, 11)
        Me.LabelControl13.TabIndex = 15
        Me.LabelControl13.Text = "* El importe se presenta redondeado o no, segun configuracion en panel."
        '
        'GridControl5
        '
        Me.GridControl5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl5.Location = New System.Drawing.Point(12, 82)
        Me.GridControl5.MainView = Me.GridView5
        Me.GridControl5.Name = "GridControl5"
        Me.GridControl5.Size = New System.Drawing.Size(1033, 406)
        Me.GridControl5.TabIndex = 14
        Me.GridControl5.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.GridControl = Me.GridControl5
        Me.GridView5.Name = "GridView5"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(245, 16)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl10.TabIndex = 13
        Me.LabelControl10.Text = "Tienda"
        '
        'DateEdit6
        '
        Me.DateEdit6.EditValue = Nothing
        Me.DateEdit6.Location = New System.Drawing.Point(118, 35)
        Me.DateEdit6.Name = "DateEdit6"
        Me.DateEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit6.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit6.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit6.TabIndex = 12
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(118, 16)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl11.TabIndex = 11
        Me.LabelControl11.Text = "Al:"
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(245, 35)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit4.TabIndex = 10
        '
        'SimpleButton14
        '
        Me.SimpleButton14.Location = New System.Drawing.Point(423, 32)
        Me.SimpleButton14.Name = "SimpleButton14"
        Me.SimpleButton14.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton14.TabIndex = 9
        Me.SimpleButton14.Text = "Buscar"
        '
        'DateEdit7
        '
        Me.DateEdit7.EditValue = Nothing
        Me.DateEdit7.Location = New System.Drawing.Point(12, 35)
        Me.DateEdit7.Name = "DateEdit7"
        Me.DateEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit7.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit7.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit7.TabIndex = 8
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(12, 16)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl12.TabIndex = 7
        Me.LabelControl12.Text = "Del:"
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.XtraTabControl2)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage5.Text = "Devoluciones"
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage18
        Me.XtraTabControl2.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabControl2.TabIndex = 0
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage18, Me.XtraTabPage19, Me.XtraTabPage20})
        '
        'XtraTabPage18
        '
        Me.XtraTabPage18.Appearance.PageClient.BackColor = System.Drawing.Color.LightBlue
        Me.XtraTabPage18.Appearance.PageClient.Options.UseBackColor = True
        Me.XtraTabPage18.Controls.Add(Me.SimpleButton16)
        Me.XtraTabPage18.Controls.Add(Me.GridControl6)
        Me.XtraTabPage18.Controls.Add(Me.LabelControl15)
        Me.XtraTabPage18.Controls.Add(Me.DateEdit8)
        Me.XtraTabPage18.Controls.Add(Me.LabelControl16)
        Me.XtraTabPage18.Controls.Add(Me.ComboBoxEdit5)
        Me.XtraTabPage18.Controls.Add(Me.SimpleButton17)
        Me.XtraTabPage18.Controls.Add(Me.DateEdit9)
        Me.XtraTabPage18.Controls.Add(Me.LabelControl17)
        Me.XtraTabPage18.Name = "XtraTabPage18"
        Me.XtraTabPage18.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage18.Text = "Estilos Devueltos a Detalle"
        '
        'SimpleButton16
        '
        Me.SimpleButton16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton16.Location = New System.Drawing.Point(877, 27)
        Me.SimpleButton16.Name = "SimpleButton16"
        Me.SimpleButton16.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton16.TabIndex = 26
        Me.SimpleButton16.Text = "Imprimir/Exportar"
        '
        'GridControl6
        '
        Me.GridControl6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl6.Location = New System.Drawing.Point(7, 63)
        Me.GridControl6.MainView = Me.GridView6
        Me.GridControl6.Name = "GridControl6"
        Me.GridControl6.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl6.TabIndex = 24
        Me.GridControl6.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.GridControl = Me.GridControl6
        Me.GridView6.Name = "GridView6"
        '
        'LabelControl15
        '
        Me.LabelControl15.Location = New System.Drawing.Point(240, 11)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl15.TabIndex = 23
        Me.LabelControl15.Text = "Tienda"
        '
        'DateEdit8
        '
        Me.DateEdit8.EditValue = Nothing
        Me.DateEdit8.Location = New System.Drawing.Point(113, 30)
        Me.DateEdit8.Name = "DateEdit8"
        Me.DateEdit8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit8.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit8.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit8.TabIndex = 22
        '
        'LabelControl16
        '
        Me.LabelControl16.Location = New System.Drawing.Point(113, 11)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl16.TabIndex = 21
        Me.LabelControl16.Text = "Al:"
        '
        'ComboBoxEdit5
        '
        Me.ComboBoxEdit5.Location = New System.Drawing.Point(240, 30)
        Me.ComboBoxEdit5.Name = "ComboBoxEdit5"
        Me.ComboBoxEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit5.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit5.TabIndex = 20
        '
        'SimpleButton17
        '
        Me.SimpleButton17.Location = New System.Drawing.Point(418, 27)
        Me.SimpleButton17.Name = "SimpleButton17"
        Me.SimpleButton17.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton17.TabIndex = 19
        Me.SimpleButton17.Text = "Buscar"
        '
        'DateEdit9
        '
        Me.DateEdit9.EditValue = Nothing
        Me.DateEdit9.Location = New System.Drawing.Point(7, 30)
        Me.DateEdit9.Name = "DateEdit9"
        Me.DateEdit9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit9.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit9.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit9.TabIndex = 18
        '
        'LabelControl17
        '
        Me.LabelControl17.Location = New System.Drawing.Point(7, 11)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl17.TabIndex = 17
        Me.LabelControl17.Text = "Del:"
        '
        'XtraTabPage19
        '
        Me.XtraTabPage19.Appearance.PageClient.BackColor = System.Drawing.Color.WhiteSmoke
        Me.XtraTabPage19.Appearance.PageClient.Options.UseBackColor = True
        Me.XtraTabPage19.Controls.Add(Me.GridControl7)
        Me.XtraTabPage19.Controls.Add(Me.LabelControl14)
        Me.XtraTabPage19.Controls.Add(Me.DateEdit10)
        Me.XtraTabPage19.Controls.Add(Me.LabelControl18)
        Me.XtraTabPage19.Controls.Add(Me.ComboBoxEdit6)
        Me.XtraTabPage19.Controls.Add(Me.SimpleButton18)
        Me.XtraTabPage19.Controls.Add(Me.DateEdit11)
        Me.XtraTabPage19.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage19.Name = "XtraTabPage19"
        Me.XtraTabPage19.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage19.Text = "Notas de Crédito Generadas"
        '
        'GridControl7
        '
        Me.GridControl7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl7.Location = New System.Drawing.Point(7, 56)
        Me.GridControl7.MainView = Me.GridView7
        Me.GridControl7.Name = "GridControl7"
        Me.GridControl7.Size = New System.Drawing.Size(1033, 406)
        Me.GridControl7.TabIndex = 32
        Me.GridControl7.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.GridControl = Me.GridControl7
        Me.GridView7.Name = "GridView7"
        '
        'LabelControl14
        '
        Me.LabelControl14.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl14.TabIndex = 31
        Me.LabelControl14.Text = "Tienda"
        '
        'DateEdit10
        '
        Me.DateEdit10.EditValue = Nothing
        Me.DateEdit10.Location = New System.Drawing.Point(113, 25)
        Me.DateEdit10.Name = "DateEdit10"
        Me.DateEdit10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit10.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit10.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit10.TabIndex = 30
        '
        'LabelControl18
        '
        Me.LabelControl18.Location = New System.Drawing.Point(113, 6)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl18.TabIndex = 29
        Me.LabelControl18.Text = "Al:"
        '
        'ComboBoxEdit6
        '
        Me.ComboBoxEdit6.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit6.Name = "ComboBoxEdit6"
        Me.ComboBoxEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit6.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit6.TabIndex = 28
        '
        'SimpleButton18
        '
        Me.SimpleButton18.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton18.Name = "SimpleButton18"
        Me.SimpleButton18.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton18.TabIndex = 27
        Me.SimpleButton18.Text = "Buscar"
        '
        'DateEdit11
        '
        Me.DateEdit11.EditValue = Nothing
        Me.DateEdit11.Location = New System.Drawing.Point(7, 25)
        Me.DateEdit11.Name = "DateEdit11"
        Me.DateEdit11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit11.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit11.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit11.TabIndex = 26
        '
        'LabelControl19
        '
        Me.LabelControl19.Location = New System.Drawing.Point(7, 6)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl19.TabIndex = 25
        Me.LabelControl19.Text = "Del:"
        '
        'XtraTabPage20
        '
        Me.XtraTabPage20.Controls.Add(Me.GridControl8)
        Me.XtraTabPage20.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage20.Controls.Add(Me.DateEdit12)
        Me.XtraTabPage20.Controls.Add(Me.LabelControl21)
        Me.XtraTabPage20.Controls.Add(Me.ComboBoxEdit7)
        Me.XtraTabPage20.Controls.Add(Me.SimpleButton19)
        Me.XtraTabPage20.Controls.Add(Me.DateEdit13)
        Me.XtraTabPage20.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage20.Name = "XtraTabPage20"
        Me.XtraTabPage20.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage20.Text = "Notas de Crédito Utilizadas"
        '
        'GridControl8
        '
        Me.GridControl8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl8.Location = New System.Drawing.Point(7, 56)
        Me.GridControl8.MainView = Me.GridView8
        Me.GridControl8.Name = "GridControl8"
        Me.GridControl8.Size = New System.Drawing.Size(1033, 406)
        Me.GridControl8.TabIndex = 32
        Me.GridControl8.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView8})
        '
        'GridView8
        '
        Me.GridView8.GridControl = Me.GridControl8
        Me.GridView8.Name = "GridView8"
        '
        'LabelControl20
        '
        Me.LabelControl20.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl20.TabIndex = 31
        Me.LabelControl20.Text = "Tienda"
        '
        'DateEdit12
        '
        Me.DateEdit12.EditValue = Nothing
        Me.DateEdit12.Location = New System.Drawing.Point(113, 25)
        Me.DateEdit12.Name = "DateEdit12"
        Me.DateEdit12.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit12.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit12.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit12.TabIndex = 30
        '
        'LabelControl21
        '
        Me.LabelControl21.Location = New System.Drawing.Point(113, 6)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl21.TabIndex = 29
        Me.LabelControl21.Text = "Al:"
        '
        'ComboBoxEdit7
        '
        Me.ComboBoxEdit7.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit7.Name = "ComboBoxEdit7"
        Me.ComboBoxEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit7.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit7.TabIndex = 28
        '
        'SimpleButton19
        '
        Me.SimpleButton19.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton19.Name = "SimpleButton19"
        Me.SimpleButton19.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton19.TabIndex = 27
        Me.SimpleButton19.Text = "Buscar"
        '
        'DateEdit13
        '
        Me.DateEdit13.EditValue = Nothing
        Me.DateEdit13.Location = New System.Drawing.Point(7, 25)
        Me.DateEdit13.Name = "DateEdit13"
        Me.DateEdit13.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit13.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit13.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit13.TabIndex = 26
        '
        'LabelControl22
        '
        Me.LabelControl22.Location = New System.Drawing.Point(7, 6)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl22.TabIndex = 25
        Me.LabelControl22.Text = "Del:"
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.GridControl9)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl23)
        Me.XtraTabPage6.Controls.Add(Me.DateEdit14)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage6.Controls.Add(Me.ComboBoxEdit8)
        Me.XtraTabPage6.Controls.Add(Me.SimpleButton20)
        Me.XtraTabPage6.Controls.Add(Me.DateEdit15)
        Me.XtraTabPage6.Controls.Add(Me.LabelControl25)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage6.Text = "Abonos a Credito"
        '
        'GridControl9
        '
        Me.GridControl9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl9.Location = New System.Drawing.Point(10, 69)
        Me.GridControl9.MainView = Me.GridView9
        Me.GridControl9.Name = "GridControl9"
        Me.GridControl9.Size = New System.Drawing.Size(1033, 419)
        Me.GridControl9.TabIndex = 40
        Me.GridControl9.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView9})
        '
        'GridView9
        '
        Me.GridView9.GridControl = Me.GridControl9
        Me.GridView9.Name = "GridView9"
        '
        'LabelControl23
        '
        Me.LabelControl23.Location = New System.Drawing.Point(243, 19)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl23.TabIndex = 39
        Me.LabelControl23.Text = "Tienda"
        '
        'DateEdit14
        '
        Me.DateEdit14.EditValue = Nothing
        Me.DateEdit14.Location = New System.Drawing.Point(116, 38)
        Me.DateEdit14.Name = "DateEdit14"
        Me.DateEdit14.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit14.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit14.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit14.TabIndex = 38
        '
        'LabelControl24
        '
        Me.LabelControl24.Location = New System.Drawing.Point(116, 19)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl24.TabIndex = 37
        Me.LabelControl24.Text = "Al:"
        '
        'ComboBoxEdit8
        '
        Me.ComboBoxEdit8.Location = New System.Drawing.Point(243, 38)
        Me.ComboBoxEdit8.Name = "ComboBoxEdit8"
        Me.ComboBoxEdit8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit8.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit8.TabIndex = 36
        '
        'SimpleButton20
        '
        Me.SimpleButton20.Location = New System.Drawing.Point(421, 35)
        Me.SimpleButton20.Name = "SimpleButton20"
        Me.SimpleButton20.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton20.TabIndex = 35
        Me.SimpleButton20.Text = "Buscar"
        '
        'DateEdit15
        '
        Me.DateEdit15.EditValue = Nothing
        Me.DateEdit15.Location = New System.Drawing.Point(10, 38)
        Me.DateEdit15.Name = "DateEdit15"
        Me.DateEdit15.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit15.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit15.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit15.TabIndex = 34
        '
        'LabelControl25
        '
        Me.LabelControl25.Location = New System.Drawing.Point(10, 19)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl25.TabIndex = 33
        Me.LabelControl25.Text = "Del:"
        '
        'XtraTabPage7
        '
        Me.XtraTabPage7.Controls.Add(Me.XtraTabControl3)
        Me.XtraTabPage7.Name = "XtraTabPage7"
        Me.XtraTabPage7.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage7.Text = "Apartados"
        '
        'XtraTabControl3
        '
        Me.XtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl3.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl3.Name = "XtraTabControl3"
        Me.XtraTabControl3.SelectedTabPage = Me.XtraTabPage21
        Me.XtraTabControl3.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabControl3.TabIndex = 0
        Me.XtraTabControl3.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage21, Me.XtraTabPage22, Me.XtraTabPage23, Me.XtraTabPage24})
        '
        'XtraTabPage21
        '
        Me.XtraTabPage21.Controls.Add(Me.GridControl10)
        Me.XtraTabPage21.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage21.Controls.Add(Me.DateEdit16)
        Me.XtraTabPage21.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage21.Controls.Add(Me.ComboBoxEdit9)
        Me.XtraTabPage21.Controls.Add(Me.SimpleButton21)
        Me.XtraTabPage21.Controls.Add(Me.DateEdit17)
        Me.XtraTabPage21.Controls.Add(Me.LabelControl28)
        Me.XtraTabPage21.Name = "XtraTabPage21"
        Me.XtraTabPage21.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage21.Text = "Nuevos y sus Anticipos"
        '
        'GridControl10
        '
        Me.GridControl10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl10.Location = New System.Drawing.Point(7, 63)
        Me.GridControl10.MainView = Me.GridView10
        Me.GridControl10.Name = "GridControl10"
        Me.GridControl10.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl10.TabIndex = 48
        Me.GridControl10.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView10})
        '
        'GridView10
        '
        Me.GridView10.GridControl = Me.GridControl10
        Me.GridView10.Name = "GridView10"
        '
        'LabelControl26
        '
        Me.LabelControl26.Location = New System.Drawing.Point(240, 10)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl26.TabIndex = 47
        Me.LabelControl26.Text = "Tienda"
        '
        'DateEdit16
        '
        Me.DateEdit16.EditValue = Nothing
        Me.DateEdit16.Location = New System.Drawing.Point(113, 29)
        Me.DateEdit16.Name = "DateEdit16"
        Me.DateEdit16.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit16.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit16.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit16.TabIndex = 46
        '
        'LabelControl27
        '
        Me.LabelControl27.Location = New System.Drawing.Point(113, 10)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl27.TabIndex = 45
        Me.LabelControl27.Text = "Al:"
        '
        'ComboBoxEdit9
        '
        Me.ComboBoxEdit9.Location = New System.Drawing.Point(240, 29)
        Me.ComboBoxEdit9.Name = "ComboBoxEdit9"
        Me.ComboBoxEdit9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit9.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit9.TabIndex = 44
        '
        'SimpleButton21
        '
        Me.SimpleButton21.Location = New System.Drawing.Point(418, 26)
        Me.SimpleButton21.Name = "SimpleButton21"
        Me.SimpleButton21.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton21.TabIndex = 43
        Me.SimpleButton21.Text = "Buscar"
        '
        'DateEdit17
        '
        Me.DateEdit17.EditValue = Nothing
        Me.DateEdit17.Location = New System.Drawing.Point(7, 29)
        Me.DateEdit17.Name = "DateEdit17"
        Me.DateEdit17.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit17.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit17.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit17.TabIndex = 42
        '
        'LabelControl28
        '
        Me.LabelControl28.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl28.TabIndex = 41
        Me.LabelControl28.Text = "Del:"
        '
        'XtraTabPage22
        '
        Me.XtraTabPage22.Controls.Add(Me.GridControl11)
        Me.XtraTabPage22.Controls.Add(Me.LabelControl29)
        Me.XtraTabPage22.Controls.Add(Me.DateEdit18)
        Me.XtraTabPage22.Controls.Add(Me.LabelControl30)
        Me.XtraTabPage22.Controls.Add(Me.ComboBoxEdit10)
        Me.XtraTabPage22.Controls.Add(Me.SimpleButton22)
        Me.XtraTabPage22.Controls.Add(Me.DateEdit19)
        Me.XtraTabPage22.Controls.Add(Me.LabelControl31)
        Me.XtraTabPage22.Name = "XtraTabPage22"
        Me.XtraTabPage22.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage22.Text = "Liquidados"
        '
        'GridControl11
        '
        Me.GridControl11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl11.Location = New System.Drawing.Point(7, 59)
        Me.GridControl11.MainView = Me.GridView11
        Me.GridControl11.Name = "GridControl11"
        Me.GridControl11.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl11.TabIndex = 56
        Me.GridControl11.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView11})
        '
        'GridView11
        '
        Me.GridView11.GridControl = Me.GridControl11
        Me.GridView11.Name = "GridView11"
        '
        'LabelControl29
        '
        Me.LabelControl29.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl29.TabIndex = 55
        Me.LabelControl29.Text = "Tienda"
        '
        'DateEdit18
        '
        Me.DateEdit18.EditValue = Nothing
        Me.DateEdit18.Location = New System.Drawing.Point(113, 25)
        Me.DateEdit18.Name = "DateEdit18"
        Me.DateEdit18.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit18.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit18.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit18.TabIndex = 54
        '
        'LabelControl30
        '
        Me.LabelControl30.Location = New System.Drawing.Point(113, 6)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl30.TabIndex = 53
        Me.LabelControl30.Text = "Al:"
        '
        'ComboBoxEdit10
        '
        Me.ComboBoxEdit10.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit10.Name = "ComboBoxEdit10"
        Me.ComboBoxEdit10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit10.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit10.TabIndex = 52
        '
        'SimpleButton22
        '
        Me.SimpleButton22.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton22.Name = "SimpleButton22"
        Me.SimpleButton22.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton22.TabIndex = 51
        Me.SimpleButton22.Text = "Buscar"
        '
        'DateEdit19
        '
        Me.DateEdit19.EditValue = Nothing
        Me.DateEdit19.Location = New System.Drawing.Point(7, 25)
        Me.DateEdit19.Name = "DateEdit19"
        Me.DateEdit19.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit19.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit19.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit19.TabIndex = 50
        '
        'LabelControl31
        '
        Me.LabelControl31.Location = New System.Drawing.Point(7, 6)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl31.TabIndex = 49
        Me.LabelControl31.Text = "Del:"
        '
        'XtraTabPage23
        '
        Me.XtraTabPage23.Controls.Add(Me.GridControl12)
        Me.XtraTabPage23.Controls.Add(Me.LabelControl32)
        Me.XtraTabPage23.Controls.Add(Me.DateEdit20)
        Me.XtraTabPage23.Controls.Add(Me.LabelControl33)
        Me.XtraTabPage23.Controls.Add(Me.ComboBoxEdit11)
        Me.XtraTabPage23.Controls.Add(Me.SimpleButton23)
        Me.XtraTabPage23.Controls.Add(Me.DateEdit21)
        Me.XtraTabPage23.Controls.Add(Me.LabelControl34)
        Me.XtraTabPage23.Name = "XtraTabPage23"
        Me.XtraTabPage23.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage23.Text = "Pagos y Abonos"
        '
        'GridControl12
        '
        Me.GridControl12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl12.Location = New System.Drawing.Point(7, 59)
        Me.GridControl12.MainView = Me.GridView12
        Me.GridControl12.Name = "GridControl12"
        Me.GridControl12.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl12.TabIndex = 56
        Me.GridControl12.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView12})
        '
        'GridView12
        '
        Me.GridView12.GridControl = Me.GridControl12
        Me.GridView12.Name = "GridView12"
        '
        'LabelControl32
        '
        Me.LabelControl32.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl32.TabIndex = 55
        Me.LabelControl32.Text = "Tienda"
        '
        'DateEdit20
        '
        Me.DateEdit20.EditValue = Nothing
        Me.DateEdit20.Location = New System.Drawing.Point(113, 25)
        Me.DateEdit20.Name = "DateEdit20"
        Me.DateEdit20.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit20.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit20.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit20.TabIndex = 54
        '
        'LabelControl33
        '
        Me.LabelControl33.Location = New System.Drawing.Point(113, 6)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl33.TabIndex = 53
        Me.LabelControl33.Text = "Al:"
        '
        'ComboBoxEdit11
        '
        Me.ComboBoxEdit11.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit11.Name = "ComboBoxEdit11"
        Me.ComboBoxEdit11.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit11.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit11.TabIndex = 52
        '
        'SimpleButton23
        '
        Me.SimpleButton23.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton23.Name = "SimpleButton23"
        Me.SimpleButton23.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton23.TabIndex = 51
        Me.SimpleButton23.Text = "Buscar"
        '
        'DateEdit21
        '
        Me.DateEdit21.EditValue = Nothing
        Me.DateEdit21.Location = New System.Drawing.Point(7, 25)
        Me.DateEdit21.Name = "DateEdit21"
        Me.DateEdit21.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit21.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit21.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit21.TabIndex = 50
        '
        'LabelControl34
        '
        Me.LabelControl34.Location = New System.Drawing.Point(7, 6)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl34.TabIndex = 49
        Me.LabelControl34.Text = "Del:"
        '
        'XtraTabPage24
        '
        Me.XtraTabPage24.Controls.Add(Me.SimpleButton25)
        Me.XtraTabPage24.Controls.Add(Me.SpinEdit1)
        Me.XtraTabPage24.Controls.Add(Me.GridControl13)
        Me.XtraTabPage24.Controls.Add(Me.LabelControl35)
        Me.XtraTabPage24.Controls.Add(Me.ComboBoxEdit12)
        Me.XtraTabPage24.Controls.Add(Me.SimpleButton24)
        Me.XtraTabPage24.Controls.Add(Me.LabelControl37)
        Me.XtraTabPage24.Name = "XtraTabPage24"
        Me.XtraTabPage24.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage24.Text = "Vencidos"
        '
        'SimpleButton25
        '
        Me.SimpleButton25.Location = New System.Drawing.Point(621, 22)
        Me.SimpleButton25.Name = "SimpleButton25"
        Me.SimpleButton25.Size = New System.Drawing.Size(132, 23)
        Me.SimpleButton25.TabIndex = 66
        Me.SimpleButton25.Text = "Cancelar Seleccionados"
        '
        'SpinEdit1
        '
        Me.SpinEdit1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.SpinEdit1.Location = New System.Drawing.Point(115, 25)
        Me.SpinEdit1.Name = "SpinEdit1"
        Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.SpinEdit1.Size = New System.Drawing.Size(100, 20)
        Me.SpinEdit1.TabIndex = 65
        '
        'GridControl13
        '
        Me.GridControl13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl13.Location = New System.Drawing.Point(7, 59)
        Me.GridControl13.MainView = Me.GridView13
        Me.GridControl13.Name = "GridControl13"
        Me.GridControl13.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl13.TabIndex = 64
        Me.GridControl13.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView13})
        '
        'GridView13
        '
        Me.GridView13.GridControl = Me.GridControl13
        Me.GridView13.Name = "GridView13"
        '
        'LabelControl35
        '
        Me.LabelControl35.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl35.TabIndex = 63
        Me.LabelControl35.Text = "Tienda"
        '
        'ComboBoxEdit12
        '
        Me.ComboBoxEdit12.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit12.Name = "ComboBoxEdit12"
        Me.ComboBoxEdit12.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit12.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit12.TabIndex = 60
        '
        'SimpleButton24
        '
        Me.SimpleButton24.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton24.Name = "SimpleButton24"
        Me.SimpleButton24.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton24.TabIndex = 59
        Me.SimpleButton24.Text = "Buscar"
        '
        'LabelControl37
        '
        Me.LabelControl37.Location = New System.Drawing.Point(10, 28)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(89, 13)
        Me.LabelControl37.TabIndex = 57
        Me.LabelControl37.Text = "Días de Generado:"
        '
        'XtraTabPage8
        '
        Me.XtraTabPage8.Controls.Add(Me.XtraTabControl4)
        Me.XtraTabPage8.Name = "XtraTabPage8"
        Me.XtraTabPage8.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage8.Text = "Vales de Cambio"
        '
        'XtraTabControl4
        '
        Me.XtraTabControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl4.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl4.Name = "XtraTabControl4"
        Me.XtraTabControl4.SelectedTabPage = Me.XtraTabPage25
        Me.XtraTabControl4.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabControl4.TabIndex = 1
        Me.XtraTabControl4.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage25, Me.XtraTabPage26})
        '
        'XtraTabPage25
        '
        Me.XtraTabPage25.Controls.Add(Me.GridControl14)
        Me.XtraTabPage25.Controls.Add(Me.LabelControl36)
        Me.XtraTabPage25.Controls.Add(Me.DateEdit22)
        Me.XtraTabPage25.Controls.Add(Me.LabelControl38)
        Me.XtraTabPage25.Controls.Add(Me.ComboBoxEdit13)
        Me.XtraTabPage25.Controls.Add(Me.SimpleButton26)
        Me.XtraTabPage25.Controls.Add(Me.DateEdit23)
        Me.XtraTabPage25.Controls.Add(Me.LabelControl39)
        Me.XtraTabPage25.Name = "XtraTabPage25"
        Me.XtraTabPage25.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage25.Text = "Generados"
        '
        'GridControl14
        '
        Me.GridControl14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl14.Location = New System.Drawing.Point(7, 63)
        Me.GridControl14.MainView = Me.GridView14
        Me.GridControl14.Name = "GridControl14"
        Me.GridControl14.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl14.TabIndex = 48
        Me.GridControl14.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView14})
        '
        'GridView14
        '
        Me.GridView14.GridControl = Me.GridControl14
        Me.GridView14.Name = "GridView14"
        '
        'LabelControl36
        '
        Me.LabelControl36.Location = New System.Drawing.Point(240, 10)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl36.TabIndex = 47
        Me.LabelControl36.Text = "Tienda"
        '
        'DateEdit22
        '
        Me.DateEdit22.EditValue = Nothing
        Me.DateEdit22.Location = New System.Drawing.Point(113, 29)
        Me.DateEdit22.Name = "DateEdit22"
        Me.DateEdit22.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit22.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit22.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit22.TabIndex = 46
        '
        'LabelControl38
        '
        Me.LabelControl38.Location = New System.Drawing.Point(113, 10)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl38.TabIndex = 45
        Me.LabelControl38.Text = "Al:"
        '
        'ComboBoxEdit13
        '
        Me.ComboBoxEdit13.Location = New System.Drawing.Point(240, 29)
        Me.ComboBoxEdit13.Name = "ComboBoxEdit13"
        Me.ComboBoxEdit13.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit13.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit13.TabIndex = 44
        '
        'SimpleButton26
        '
        Me.SimpleButton26.Location = New System.Drawing.Point(418, 26)
        Me.SimpleButton26.Name = "SimpleButton26"
        Me.SimpleButton26.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton26.TabIndex = 43
        Me.SimpleButton26.Text = "Buscar"
        '
        'DateEdit23
        '
        Me.DateEdit23.EditValue = Nothing
        Me.DateEdit23.Location = New System.Drawing.Point(7, 29)
        Me.DateEdit23.Name = "DateEdit23"
        Me.DateEdit23.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit23.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit23.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit23.TabIndex = 42
        '
        'LabelControl39
        '
        Me.LabelControl39.Location = New System.Drawing.Point(7, 10)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl39.TabIndex = 41
        Me.LabelControl39.Text = "Del:"
        '
        'XtraTabPage26
        '
        Me.XtraTabPage26.Controls.Add(Me.GridControl15)
        Me.XtraTabPage26.Controls.Add(Me.LabelControl40)
        Me.XtraTabPage26.Controls.Add(Me.DateEdit24)
        Me.XtraTabPage26.Controls.Add(Me.LabelControl41)
        Me.XtraTabPage26.Controls.Add(Me.ComboBoxEdit14)
        Me.XtraTabPage26.Controls.Add(Me.SimpleButton27)
        Me.XtraTabPage26.Controls.Add(Me.DateEdit25)
        Me.XtraTabPage26.Controls.Add(Me.LabelControl42)
        Me.XtraTabPage26.Name = "XtraTabPage26"
        Me.XtraTabPage26.Size = New System.Drawing.Size(1046, 467)
        Me.XtraTabPage26.Text = "Utilizados"
        '
        'GridControl15
        '
        Me.GridControl15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl15.Location = New System.Drawing.Point(7, 59)
        Me.GridControl15.MainView = Me.GridView15
        Me.GridControl15.Name = "GridControl15"
        Me.GridControl15.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl15.TabIndex = 56
        Me.GridControl15.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView15})
        '
        'GridView15
        '
        Me.GridView15.GridControl = Me.GridControl15
        Me.GridView15.Name = "GridView15"
        '
        'LabelControl40
        '
        Me.LabelControl40.Location = New System.Drawing.Point(240, 6)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl40.TabIndex = 55
        Me.LabelControl40.Text = "Tienda"
        '
        'DateEdit24
        '
        Me.DateEdit24.EditValue = Nothing
        Me.DateEdit24.Location = New System.Drawing.Point(113, 25)
        Me.DateEdit24.Name = "DateEdit24"
        Me.DateEdit24.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit24.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit24.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit24.TabIndex = 54
        '
        'LabelControl41
        '
        Me.LabelControl41.Location = New System.Drawing.Point(113, 6)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl41.TabIndex = 53
        Me.LabelControl41.Text = "Al:"
        '
        'ComboBoxEdit14
        '
        Me.ComboBoxEdit14.Location = New System.Drawing.Point(240, 25)
        Me.ComboBoxEdit14.Name = "ComboBoxEdit14"
        Me.ComboBoxEdit14.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit14.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit14.TabIndex = 52
        '
        'SimpleButton27
        '
        Me.SimpleButton27.Location = New System.Drawing.Point(418, 22)
        Me.SimpleButton27.Name = "SimpleButton27"
        Me.SimpleButton27.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton27.TabIndex = 51
        Me.SimpleButton27.Text = "Buscar"
        '
        'DateEdit25
        '
        Me.DateEdit25.EditValue = Nothing
        Me.DateEdit25.Location = New System.Drawing.Point(7, 25)
        Me.DateEdit25.Name = "DateEdit25"
        Me.DateEdit25.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit25.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit25.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit25.TabIndex = 50
        '
        'LabelControl42
        '
        Me.LabelControl42.Location = New System.Drawing.Point(7, 6)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl42.TabIndex = 49
        Me.LabelControl42.Text = "Del:"
        '
        'XtraTabPage9
        '
        Me.XtraTabPage9.Controls.Add(Me.GridControl16)
        Me.XtraTabPage9.Controls.Add(Me.LabelControl43)
        Me.XtraTabPage9.Controls.Add(Me.ComboBoxEdit15)
        Me.XtraTabPage9.Controls.Add(Me.SimpleButton28)
        Me.XtraTabPage9.Controls.Add(Me.DateEdit27)
        Me.XtraTabPage9.Controls.Add(Me.LabelControl45)
        Me.XtraTabPage9.Name = "XtraTabPage9"
        Me.XtraTabPage9.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage9.Text = "Gastos"
        '
        'GridControl16
        '
        Me.GridControl16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl16.Location = New System.Drawing.Point(10, 73)
        Me.GridControl16.MainView = Me.GridView16
        Me.GridControl16.Name = "GridControl16"
        Me.GridControl16.Size = New System.Drawing.Size(1033, 415)
        Me.GridControl16.TabIndex = 64
        Me.GridControl16.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView16})
        '
        'GridView16
        '
        Me.GridView16.GridControl = Me.GridControl16
        Me.GridView16.Name = "GridView16"
        '
        'LabelControl43
        '
        Me.LabelControl43.Location = New System.Drawing.Point(135, 20)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl43.TabIndex = 63
        Me.LabelControl43.Text = "Tienda"
        '
        'ComboBoxEdit15
        '
        Me.ComboBoxEdit15.Location = New System.Drawing.Point(135, 39)
        Me.ComboBoxEdit15.Name = "ComboBoxEdit15"
        Me.ComboBoxEdit15.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit15.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit15.TabIndex = 60
        '
        'SimpleButton28
        '
        Me.SimpleButton28.Location = New System.Drawing.Point(313, 36)
        Me.SimpleButton28.Name = "SimpleButton28"
        Me.SimpleButton28.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton28.TabIndex = 59
        Me.SimpleButton28.Text = "Buscar"
        '
        'DateEdit27
        '
        Me.DateEdit27.EditValue = Nothing
        Me.DateEdit27.Location = New System.Drawing.Point(10, 39)
        Me.DateEdit27.Name = "DateEdit27"
        Me.DateEdit27.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit27.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit27.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit27.TabIndex = 58
        '
        'LabelControl45
        '
        Me.LabelControl45.Location = New System.Drawing.Point(10, 20)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl45.TabIndex = 57
        Me.LabelControl45.Text = "Fecha:"
        '
        'XtraTabPage10
        '
        Me.XtraTabPage10.Controls.Add(Me.GridControl17)
        Me.XtraTabPage10.Controls.Add(Me.LabelControl44)
        Me.XtraTabPage10.Controls.Add(Me.ComboBoxEdit16)
        Me.XtraTabPage10.Controls.Add(Me.SimpleButton29)
        Me.XtraTabPage10.Controls.Add(Me.DateEdit26)
        Me.XtraTabPage10.Controls.Add(Me.LabelControl46)
        Me.XtraTabPage10.Name = "XtraTabPage10"
        Me.XtraTabPage10.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage10.Text = "Precios Modificados"
        '
        'GridControl17
        '
        Me.GridControl17.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl17.Location = New System.Drawing.Point(10, 66)
        Me.GridControl17.MainView = Me.GridView17
        Me.GridControl17.Name = "GridControl17"
        Me.GridControl17.Size = New System.Drawing.Size(1033, 415)
        Me.GridControl17.TabIndex = 70
        Me.GridControl17.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView17})
        '
        'GridView17
        '
        Me.GridView17.GridControl = Me.GridControl17
        Me.GridView17.Name = "GridView17"
        '
        'LabelControl44
        '
        Me.LabelControl44.Location = New System.Drawing.Point(135, 13)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl44.TabIndex = 69
        Me.LabelControl44.Text = "Tienda"
        '
        'ComboBoxEdit16
        '
        Me.ComboBoxEdit16.Location = New System.Drawing.Point(135, 32)
        Me.ComboBoxEdit16.Name = "ComboBoxEdit16"
        Me.ComboBoxEdit16.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit16.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit16.TabIndex = 68
        '
        'SimpleButton29
        '
        Me.SimpleButton29.Location = New System.Drawing.Point(313, 29)
        Me.SimpleButton29.Name = "SimpleButton29"
        Me.SimpleButton29.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton29.TabIndex = 67
        Me.SimpleButton29.Text = "Buscar"
        '
        'DateEdit26
        '
        Me.DateEdit26.EditValue = Nothing
        Me.DateEdit26.Location = New System.Drawing.Point(10, 32)
        Me.DateEdit26.Name = "DateEdit26"
        Me.DateEdit26.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit26.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit26.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit26.TabIndex = 66
        '
        'LabelControl46
        '
        Me.LabelControl46.Location = New System.Drawing.Point(10, 13)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl46.TabIndex = 65
        Me.LabelControl46.Text = "Fecha:"
        '
        'XtraTabPage11
        '
        Me.XtraTabPage11.Controls.Add(Me.RadioButton6)
        Me.XtraTabPage11.Controls.Add(Me.RadioButton5)
        Me.XtraTabPage11.Controls.Add(Me.GroupControl10)
        Me.XtraTabPage11.Controls.Add(Me.TextEdit3)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl49)
        Me.XtraTabPage11.Controls.Add(Me.GroupControl9)
        Me.XtraTabPage11.Controls.Add(Me.SimpleButton30)
        Me.XtraTabPage11.Controls.Add(Me.TextEdit2)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl48)
        Me.XtraTabPage11.Controls.Add(Me.RadioButton4)
        Me.XtraTabPage11.Controls.Add(Me.RadioButton3)
        Me.XtraTabPage11.Controls.Add(Me.RadioButton2)
        Me.XtraTabPage11.Controls.Add(Me.RadioButton1)
        Me.XtraTabPage11.Controls.Add(Me.LabelControl47)
        Me.XtraTabPage11.Controls.Add(Me.RadioGroup1)
        Me.XtraTabPage11.Name = "XtraTabPage11"
        Me.XtraTabPage11.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage11.Text = "Consultas/Cancelaciones"
        '
        'RadioButton6
        '
        Me.RadioButton6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.BackColor = System.Drawing.Color.White
        Me.RadioButton6.Location = New System.Drawing.Point(46, 167)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(94, 17)
        Me.RadioButton6.TabIndex = 17
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "Abono Credito"
        Me.RadioButton6.UseVisualStyleBackColor = False
        '
        'RadioButton5
        '
        Me.RadioButton5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.BackColor = System.Drawing.Color.White
        Me.RadioButton5.Location = New System.Drawing.Point(46, 144)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(104, 17)
        Me.RadioButton5.TabIndex = 16
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "Abono Apartado"
        Me.RadioButton5.UseVisualStyleBackColor = False
        '
        'GroupControl10
        '
        Me.GroupControl10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl10.Controls.Add(Me.LabelControl50)
        Me.GroupControl10.Controls.Add(Me.SimpleButton31)
        Me.GroupControl10.Controls.Add(Me.MemoEdit1)
        Me.GroupControl10.Location = New System.Drawing.Point(36, 285)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(200, 186)
        Me.GroupControl10.TabIndex = 15
        Me.GroupControl10.Text = "Cancelar"
        '
        'LabelControl50
        '
        Me.LabelControl50.Location = New System.Drawing.Point(51, 24)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(97, 13)
        Me.LabelControl50.TabIndex = 13
        Me.LabelControl50.Text = "Motivos Cancelacion"
        '
        'SimpleButton31
        '
        Me.SimpleButton31.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton31.Appearance.Options.UseFont = True
        Me.SimpleButton31.Location = New System.Drawing.Point(51, 158)
        Me.SimpleButton31.Name = "SimpleButton31"
        Me.SimpleButton31.Size = New System.Drawing.Size(94, 23)
        Me.SimpleButton31.TabIndex = 14
        Me.SimpleButton31.Text = "Cancelar"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Location = New System.Drawing.Point(17, 43)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(169, 109)
        Me.MemoEdit1.TabIndex = 12
        Me.MemoEdit1.UseOptimizedRendering = True
        '
        'TextEdit3
        '
        Me.TextEdit3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextEdit3.Location = New System.Drawing.Point(436, 26)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.ReadOnly = True
        Me.TextEdit3.Size = New System.Drawing.Size(237, 26)
        Me.TextEdit3.TabIndex = 11
        '
        'LabelControl49
        '
        Me.LabelControl49.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl49.Location = New System.Drawing.Point(300, 33)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(118, 19)
        Me.LabelControl49.TabIndex = 10
        Me.LabelControl49.Text = "Estado Actual:"
        '
        'GroupControl9
        '
        Me.GroupControl9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl9.CaptionLocation = DevExpress.Utils.Locations.Right
        Me.GroupControl9.Controls.Add(Me.ReportViewer3)
        Me.GroupControl9.Location = New System.Drawing.Point(300, 65)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(373, 423)
        Me.GroupControl9.TabIndex = 9
        '
        'ReportViewer3
        '
        Me.ReportViewer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer3.Location = New System.Drawing.Point(2, 2)
        Me.ReportViewer3.Name = "ReportViewer3"
        Me.ReportViewer3.Size = New System.Drawing.Size(350, 419)
        Me.ReportViewer3.TabIndex = 0
        '
        'SimpleButton30
        '
        Me.SimpleButton30.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton30.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton30.Appearance.Options.UseFont = True
        Me.SimpleButton30.Location = New System.Drawing.Point(87, 252)
        Me.SimpleButton30.Name = "SimpleButton30"
        Me.SimpleButton30.Size = New System.Drawing.Size(94, 23)
        Me.SimpleButton30.TabIndex = 8
        Me.SimpleButton30.Text = "Buscar"
        '
        'TextEdit2
        '
        Me.TextEdit2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextEdit2.Location = New System.Drawing.Point(36, 216)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(200, 20)
        Me.TextEdit2.TabIndex = 7
        '
        'LabelControl48
        '
        Me.LabelControl48.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl48.Location = New System.Drawing.Point(107, 197)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(57, 13)
        Me.LabelControl48.TabIndex = 6
        Me.LabelControl48.Text = "No. de Folio"
        '
        'RadioButton4
        '
        Me.RadioButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.BackColor = System.Drawing.Color.White
        Me.RadioButton4.Location = New System.Drawing.Point(46, 98)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(118, 17)
        Me.RadioButton4.TabIndex = 5
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Traspaso a Tiendas"
        Me.RadioButton4.UseVisualStyleBackColor = False
        '
        'RadioButton3
        '
        Me.RadioButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.BackColor = System.Drawing.Color.White
        Me.RadioButton3.Location = New System.Drawing.Point(46, 121)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(75, 17)
        Me.RadioButton3.TabIndex = 4
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Apartados"
        Me.RadioButton3.UseVisualStyleBackColor = False
        '
        'RadioButton2
        '
        Me.RadioButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.BackColor = System.Drawing.Color.White
        Me.RadioButton2.Location = New System.Drawing.Point(46, 75)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(142, 17)
        Me.RadioButton2.TabIndex = 3
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Devolucion/Nota Credito"
        Me.RadioButton2.UseVisualStyleBackColor = False
        '
        'RadioButton1
        '
        Me.RadioButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.BackColor = System.Drawing.Color.White
        Me.RadioButton1.Location = New System.Drawing.Point(46, 52)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(99, 17)
        Me.RadioButton1.TabIndex = 2
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Ticket de Venta"
        Me.RadioButton1.UseVisualStyleBackColor = False
        '
        'LabelControl47
        '
        Me.LabelControl47.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl47.Location = New System.Drawing.Point(89, 14)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(92, 13)
        Me.LabelControl47.TabIndex = 1
        Me.LabelControl47.Text = "Tipo de Documento"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadioGroup1.Location = New System.Drawing.Point(36, 33)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Size = New System.Drawing.Size(200, 158)
        Me.RadioGroup1.TabIndex = 0
        '
        'XtraTabPage12
        '
        Me.XtraTabPage12.Controls.Add(Me.GridControl18)
        Me.XtraTabPage12.Controls.Add(Me.LabelControl51)
        Me.XtraTabPage12.Controls.Add(Me.ComboBoxEdit17)
        Me.XtraTabPage12.Controls.Add(Me.SimpleButton32)
        Me.XtraTabPage12.Controls.Add(Me.DateEdit28)
        Me.XtraTabPage12.Controls.Add(Me.LabelControl52)
        Me.XtraTabPage12.Name = "XtraTabPage12"
        Me.XtraTabPage12.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage12.Text = "Reporte Cancelaciones"
        '
        'GridControl18
        '
        Me.GridControl18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl18.Location = New System.Drawing.Point(10, 66)
        Me.GridControl18.MainView = Me.GridView18
        Me.GridControl18.Name = "GridControl18"
        Me.GridControl18.Size = New System.Drawing.Size(1033, 415)
        Me.GridControl18.TabIndex = 76
        Me.GridControl18.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView18})
        '
        'GridView18
        '
        Me.GridView18.GridControl = Me.GridControl18
        Me.GridView18.Name = "GridView18"
        '
        'LabelControl51
        '
        Me.LabelControl51.Location = New System.Drawing.Point(135, 13)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl51.TabIndex = 75
        Me.LabelControl51.Text = "Tienda"
        '
        'ComboBoxEdit17
        '
        Me.ComboBoxEdit17.Location = New System.Drawing.Point(135, 32)
        Me.ComboBoxEdit17.Name = "ComboBoxEdit17"
        Me.ComboBoxEdit17.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit17.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit17.TabIndex = 74
        '
        'SimpleButton32
        '
        Me.SimpleButton32.Location = New System.Drawing.Point(313, 29)
        Me.SimpleButton32.Name = "SimpleButton32"
        Me.SimpleButton32.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton32.TabIndex = 73
        Me.SimpleButton32.Text = "Buscar"
        '
        'DateEdit28
        '
        Me.DateEdit28.EditValue = Nothing
        Me.DateEdit28.Location = New System.Drawing.Point(10, 32)
        Me.DateEdit28.Name = "DateEdit28"
        Me.DateEdit28.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit28.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit28.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit28.TabIndex = 72
        '
        'LabelControl52
        '
        Me.LabelControl52.Location = New System.Drawing.Point(10, 13)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl52.TabIndex = 71
        Me.LabelControl52.Text = "Fecha:"
        '
        'XtraTabPage13
        '
        Me.XtraTabPage13.Controls.Add(Me.SimpleButton33)
        Me.XtraTabPage13.Controls.Add(Me.GridControl19)
        Me.XtraTabPage13.Controls.Add(Me.LabelControl53)
        Me.XtraTabPage13.Controls.Add(Me.DateEdit29)
        Me.XtraTabPage13.Controls.Add(Me.LabelControl54)
        Me.XtraTabPage13.Controls.Add(Me.ComboBoxEdit18)
        Me.XtraTabPage13.Controls.Add(Me.SimpleButton34)
        Me.XtraTabPage13.Controls.Add(Me.DateEdit30)
        Me.XtraTabPage13.Controls.Add(Me.LabelControl55)
        Me.XtraTabPage13.Name = "XtraTabPage13"
        Me.XtraTabPage13.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage13.Text = "Ventas por Empleado"
        '
        'SimpleButton33
        '
        Me.SimpleButton33.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton33.Location = New System.Drawing.Point(880, 37)
        Me.SimpleButton33.Name = "SimpleButton33"
        Me.SimpleButton33.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton33.TabIndex = 35
        Me.SimpleButton33.Text = "Imprimir/Exportar"
        '
        'GridControl19
        '
        Me.GridControl19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl19.Location = New System.Drawing.Point(10, 73)
        Me.GridControl19.MainView = Me.GridView19
        Me.GridControl19.Name = "GridControl19"
        Me.GridControl19.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl19.TabIndex = 34
        Me.GridControl19.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView19})
        '
        'GridView19
        '
        Me.GridView19.GridControl = Me.GridControl19
        Me.GridView19.Name = "GridView19"
        '
        'LabelControl53
        '
        Me.LabelControl53.Location = New System.Drawing.Point(243, 21)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl53.TabIndex = 33
        Me.LabelControl53.Text = "Tienda"
        '
        'DateEdit29
        '
        Me.DateEdit29.EditValue = Nothing
        Me.DateEdit29.Location = New System.Drawing.Point(116, 40)
        Me.DateEdit29.Name = "DateEdit29"
        Me.DateEdit29.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit29.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit29.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit29.TabIndex = 32
        '
        'LabelControl54
        '
        Me.LabelControl54.Location = New System.Drawing.Point(116, 21)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl54.TabIndex = 31
        Me.LabelControl54.Text = "Al:"
        '
        'ComboBoxEdit18
        '
        Me.ComboBoxEdit18.Location = New System.Drawing.Point(243, 40)
        Me.ComboBoxEdit18.Name = "ComboBoxEdit18"
        Me.ComboBoxEdit18.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit18.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit18.TabIndex = 30
        '
        'SimpleButton34
        '
        Me.SimpleButton34.Location = New System.Drawing.Point(421, 37)
        Me.SimpleButton34.Name = "SimpleButton34"
        Me.SimpleButton34.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton34.TabIndex = 29
        Me.SimpleButton34.Text = "Buscar"
        '
        'DateEdit30
        '
        Me.DateEdit30.EditValue = Nothing
        Me.DateEdit30.Location = New System.Drawing.Point(10, 40)
        Me.DateEdit30.Name = "DateEdit30"
        Me.DateEdit30.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit30.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit30.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit30.TabIndex = 28
        '
        'LabelControl55
        '
        Me.LabelControl55.Location = New System.Drawing.Point(10, 21)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl55.TabIndex = 27
        Me.LabelControl55.Text = "Del:"
        '
        'XtraTabPage14
        '
        Me.XtraTabPage14.Controls.Add(Me.GridControl24)
        Me.XtraTabPage14.Controls.Add(Me.GridControl23)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl65)
        Me.XtraTabPage14.Controls.Add(Me.ComboBoxEdit23)
        Me.XtraTabPage14.Controls.Add(Me.SimpleButton40)
        Me.XtraTabPage14.Controls.Add(Me.DateEdit36)
        Me.XtraTabPage14.Controls.Add(Me.LabelControl66)
        Me.XtraTabPage14.Name = "XtraTabPage14"
        Me.XtraTabPage14.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage14.Text = "Devoluciones Canceladas"
        '
        'GridControl24
        '
        Me.GridControl24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl24.Location = New System.Drawing.Point(542, 78)
        Me.GridControl24.MainView = Me.GridView24
        Me.GridControl24.Name = "GridControl24"
        Me.GridControl24.Size = New System.Drawing.Size(477, 396)
        Me.GridControl24.TabIndex = 70
        Me.GridControl24.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView24})
        '
        'GridView24
        '
        Me.GridView24.GridControl = Me.GridControl24
        Me.GridView24.Name = "GridView24"
        '
        'GridControl23
        '
        Me.GridControl23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GridControl23.Location = New System.Drawing.Point(11, 78)
        Me.GridControl23.MainView = Me.GridView23
        Me.GridControl23.Name = "GridControl23"
        Me.GridControl23.Size = New System.Drawing.Size(496, 396)
        Me.GridControl23.TabIndex = 69
        Me.GridControl23.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView23})
        '
        'GridView23
        '
        Me.GridView23.GridControl = Me.GridControl23
        Me.GridView23.Name = "GridView23"
        '
        'LabelControl65
        '
        Me.LabelControl65.Location = New System.Drawing.Point(136, 14)
        Me.LabelControl65.Name = "LabelControl65"
        Me.LabelControl65.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl65.TabIndex = 68
        Me.LabelControl65.Text = "Tienda"
        '
        'ComboBoxEdit23
        '
        Me.ComboBoxEdit23.Location = New System.Drawing.Point(136, 33)
        Me.ComboBoxEdit23.Name = "ComboBoxEdit23"
        Me.ComboBoxEdit23.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit23.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit23.TabIndex = 67
        '
        'SimpleButton40
        '
        Me.SimpleButton40.Location = New System.Drawing.Point(314, 30)
        Me.SimpleButton40.Name = "SimpleButton40"
        Me.SimpleButton40.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton40.TabIndex = 66
        Me.SimpleButton40.Text = "Buscar"
        '
        'DateEdit36
        '
        Me.DateEdit36.EditValue = Nothing
        Me.DateEdit36.Location = New System.Drawing.Point(11, 33)
        Me.DateEdit36.Name = "DateEdit36"
        Me.DateEdit36.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit36.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit36.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit36.TabIndex = 65
        '
        'LabelControl66
        '
        Me.LabelControl66.Location = New System.Drawing.Point(11, 14)
        Me.LabelControl66.Name = "LabelControl66"
        Me.LabelControl66.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl66.TabIndex = 64
        Me.LabelControl66.Text = "Fecha:"
        '
        'XtraTabPage15
        '
        Me.XtraTabPage15.Controls.Add(Me.GridControl20)
        Me.XtraTabPage15.Controls.Add(Me.LabelControl56)
        Me.XtraTabPage15.Controls.Add(Me.ComboBoxEdit19)
        Me.XtraTabPage15.Controls.Add(Me.SimpleButton36)
        Me.XtraTabPage15.Controls.Add(Me.DateEdit32)
        Me.XtraTabPage15.Controls.Add(Me.LabelControl58)
        Me.XtraTabPage15.Name = "XtraTabPage15"
        Me.XtraTabPage15.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage15.Text = "Descuentos Manuales en Caja"
        '
        'GridControl20
        '
        Me.GridControl20.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl20.Location = New System.Drawing.Point(10, 73)
        Me.GridControl20.MainView = Me.GridView20
        Me.GridControl20.Name = "GridControl20"
        Me.GridControl20.Size = New System.Drawing.Size(1033, 401)
        Me.GridControl20.TabIndex = 34
        Me.GridControl20.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView20})
        '
        'GridView20
        '
        Me.GridView20.GridControl = Me.GridControl20
        Me.GridView20.Name = "GridView20"
        '
        'LabelControl56
        '
        Me.LabelControl56.Location = New System.Drawing.Point(136, 21)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl56.TabIndex = 33
        Me.LabelControl56.Text = "Tienda"
        '
        'ComboBoxEdit19
        '
        Me.ComboBoxEdit19.Location = New System.Drawing.Point(136, 40)
        Me.ComboBoxEdit19.Name = "ComboBoxEdit19"
        Me.ComboBoxEdit19.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit19.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit19.TabIndex = 30
        '
        'SimpleButton36
        '
        Me.SimpleButton36.Location = New System.Drawing.Point(314, 37)
        Me.SimpleButton36.Name = "SimpleButton36"
        Me.SimpleButton36.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton36.TabIndex = 29
        Me.SimpleButton36.Text = "Buscar"
        '
        'DateEdit32
        '
        Me.DateEdit32.EditValue = Nothing
        Me.DateEdit32.Location = New System.Drawing.Point(10, 40)
        Me.DateEdit32.Name = "DateEdit32"
        Me.DateEdit32.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit32.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit32.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit32.TabIndex = 28
        '
        'LabelControl58
        '
        Me.LabelControl58.Location = New System.Drawing.Point(10, 21)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl58.TabIndex = 27
        Me.LabelControl58.Text = "Fecha:"
        '
        'XtraTabPage16
        '
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit3)
        Me.XtraTabPage16.Controls.Add(Me.CheckEdit2)
        Me.XtraTabPage16.Controls.Add(Me.LabelControl61)
        Me.XtraTabPage16.Controls.Add(Me.ComboBoxEdit21)
        Me.XtraTabPage16.Controls.Add(Me.SimpleButton35)
        Me.XtraTabPage16.Controls.Add(Me.GridControl21)
        Me.XtraTabPage16.Controls.Add(Me.LabelControl57)
        Me.XtraTabPage16.Controls.Add(Me.DateEdit31)
        Me.XtraTabPage16.Controls.Add(Me.LabelControl59)
        Me.XtraTabPage16.Controls.Add(Me.ComboBoxEdit20)
        Me.XtraTabPage16.Controls.Add(Me.SimpleButton37)
        Me.XtraTabPage16.Controls.Add(Me.DateEdit33)
        Me.XtraTabPage16.Controls.Add(Me.LabelControl60)
        Me.XtraTabPage16.Name = "XtraTabPage16"
        Me.XtraTabPage16.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage16.Text = "Reporte Asistencias por Empleado"
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(434, 48)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "Todos los Empleados"
        Me.CheckEdit3.Size = New System.Drawing.Size(124, 19)
        Me.CheckEdit3.TabIndex = 39
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(274, 48)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Caption = "Todas las Tiendas"
        Me.CheckEdit2.Size = New System.Drawing.Size(110, 19)
        Me.CheckEdit2.TabIndex = 38
        '
        'LabelControl61
        '
        Me.LabelControl61.Location = New System.Drawing.Point(417, 8)
        Me.LabelControl61.Name = "LabelControl61"
        Me.LabelControl61.Size = New System.Drawing.Size(46, 13)
        Me.LabelControl61.TabIndex = 37
        Me.LabelControl61.Text = "Empleado"
        '
        'ComboBoxEdit21
        '
        Me.ComboBoxEdit21.Location = New System.Drawing.Point(417, 27)
        Me.ComboBoxEdit21.Name = "ComboBoxEdit21"
        Me.ComboBoxEdit21.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit21.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit21.TabIndex = 36
        '
        'SimpleButton35
        '
        Me.SimpleButton35.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton35.Location = New System.Drawing.Point(937, 468)
        Me.SimpleButton35.Name = "SimpleButton35"
        Me.SimpleButton35.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton35.TabIndex = 35
        Me.SimpleButton35.Text = "Imprimir/Exportar"
        '
        'GridControl21
        '
        Me.GridControl21.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl21.Location = New System.Drawing.Point(10, 73)
        Me.GridControl21.MainView = Me.GridView21
        Me.GridControl21.Name = "GridControl21"
        Me.GridControl21.Size = New System.Drawing.Size(1033, 389)
        Me.GridControl21.TabIndex = 34
        Me.GridControl21.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView21})
        '
        'GridView21
        '
        Me.GridView21.GridControl = Me.GridControl21
        Me.GridView21.Name = "GridView21"
        '
        'LabelControl57
        '
        Me.LabelControl57.Location = New System.Drawing.Point(243, 8)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl57.TabIndex = 33
        Me.LabelControl57.Text = "Tienda"
        '
        'DateEdit31
        '
        Me.DateEdit31.EditValue = Nothing
        Me.DateEdit31.Location = New System.Drawing.Point(116, 27)
        Me.DateEdit31.Name = "DateEdit31"
        Me.DateEdit31.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit31.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit31.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit31.TabIndex = 32
        '
        'LabelControl59
        '
        Me.LabelControl59.Location = New System.Drawing.Point(116, 8)
        Me.LabelControl59.Name = "LabelControl59"
        Me.LabelControl59.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl59.TabIndex = 31
        Me.LabelControl59.Text = "Al:"
        '
        'ComboBoxEdit20
        '
        Me.ComboBoxEdit20.Location = New System.Drawing.Point(243, 27)
        Me.ComboBoxEdit20.Name = "ComboBoxEdit20"
        Me.ComboBoxEdit20.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit20.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit20.TabIndex = 30
        '
        'SimpleButton37
        '
        Me.SimpleButton37.Location = New System.Drawing.Point(616, 24)
        Me.SimpleButton37.Name = "SimpleButton37"
        Me.SimpleButton37.Size = New System.Drawing.Size(115, 23)
        Me.SimpleButton37.TabIndex = 29
        Me.SimpleButton37.Text = "Mostrar Reporte"
        '
        'DateEdit33
        '
        Me.DateEdit33.EditValue = Nothing
        Me.DateEdit33.Location = New System.Drawing.Point(10, 27)
        Me.DateEdit33.Name = "DateEdit33"
        Me.DateEdit33.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit33.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit33.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit33.TabIndex = 28
        '
        'LabelControl60
        '
        Me.LabelControl60.Location = New System.Drawing.Point(10, 8)
        Me.LabelControl60.Name = "LabelControl60"
        Me.LabelControl60.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl60.TabIndex = 27
        Me.LabelControl60.Text = "Del:"
        '
        'XtraTabPage17
        '
        Me.XtraTabPage17.Controls.Add(Me.SimpleButton38)
        Me.XtraTabPage17.Controls.Add(Me.GridControl22)
        Me.XtraTabPage17.Controls.Add(Me.LabelControl62)
        Me.XtraTabPage17.Controls.Add(Me.DateEdit34)
        Me.XtraTabPage17.Controls.Add(Me.LabelControl63)
        Me.XtraTabPage17.Controls.Add(Me.ComboBoxEdit22)
        Me.XtraTabPage17.Controls.Add(Me.SimpleButton39)
        Me.XtraTabPage17.Controls.Add(Me.DateEdit35)
        Me.XtraTabPage17.Controls.Add(Me.LabelControl64)
        Me.XtraTabPage17.Name = "XtraTabPage17"
        Me.XtraTabPage17.Size = New System.Drawing.Size(1052, 495)
        Me.XtraTabPage17.Text = "Retiro de Efectivo"
        '
        'SimpleButton38
        '
        Me.SimpleButton38.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton38.Location = New System.Drawing.Point(880, 37)
        Me.SimpleButton38.Name = "SimpleButton38"
        Me.SimpleButton38.Size = New System.Drawing.Size(106, 23)
        Me.SimpleButton38.TabIndex = 44
        Me.SimpleButton38.Text = "Imprimir/Exportar"
        '
        'GridControl22
        '
        Me.GridControl22.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl22.Location = New System.Drawing.Point(10, 73)
        Me.GridControl22.MainView = Me.GridView22
        Me.GridControl22.Name = "GridControl22"
        Me.GridControl22.Size = New System.Drawing.Size(1033, 415)
        Me.GridControl22.TabIndex = 43
        Me.GridControl22.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView22})
        '
        'GridView22
        '
        Me.GridView22.GridControl = Me.GridControl22
        Me.GridView22.Name = "GridView22"
        '
        'LabelControl62
        '
        Me.LabelControl62.Location = New System.Drawing.Point(243, 21)
        Me.LabelControl62.Name = "LabelControl62"
        Me.LabelControl62.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl62.TabIndex = 42
        Me.LabelControl62.Text = "Tienda"
        '
        'DateEdit34
        '
        Me.DateEdit34.EditValue = Nothing
        Me.DateEdit34.Location = New System.Drawing.Point(116, 40)
        Me.DateEdit34.Name = "DateEdit34"
        Me.DateEdit34.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit34.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit34.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit34.TabIndex = 41
        '
        'LabelControl63
        '
        Me.LabelControl63.Location = New System.Drawing.Point(116, 21)
        Me.LabelControl63.Name = "LabelControl63"
        Me.LabelControl63.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl63.TabIndex = 40
        Me.LabelControl63.Text = "Al:"
        '
        'ComboBoxEdit22
        '
        Me.ComboBoxEdit22.Location = New System.Drawing.Point(243, 40)
        Me.ComboBoxEdit22.Name = "ComboBoxEdit22"
        Me.ComboBoxEdit22.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit22.Size = New System.Drawing.Size(141, 20)
        Me.ComboBoxEdit22.TabIndex = 39
        '
        'SimpleButton39
        '
        Me.SimpleButton39.Location = New System.Drawing.Point(421, 37)
        Me.SimpleButton39.Name = "SimpleButton39"
        Me.SimpleButton39.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton39.TabIndex = 38
        Me.SimpleButton39.Text = "Buscar"
        '
        'DateEdit35
        '
        Me.DateEdit35.EditValue = Nothing
        Me.DateEdit35.Location = New System.Drawing.Point(10, 40)
        Me.DateEdit35.Name = "DateEdit35"
        Me.DateEdit35.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit35.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit35.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit35.TabIndex = 37
        '
        'LabelControl64
        '
        Me.LabelControl64.Location = New System.Drawing.Point(10, 21)
        Me.LabelControl64.Name = "LabelControl64"
        Me.LabelControl64.Size = New System.Drawing.Size(19, 13)
        Me.LabelControl64.TabIndex = 36
        Me.LabelControl64.Text = "Del:"
        '
        'CorteCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1058, 567)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "CorteCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corte de Caja"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.DateEdit5.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        Me.XtraTabPage4.PerformLayout()
        CType(Me.GridControl5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit6.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit7.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.XtraTabPage18.ResumeLayout(False)
        Me.XtraTabPage18.PerformLayout()
        CType(Me.GridControl6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit8.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit9.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage19.ResumeLayout(False)
        Me.XtraTabPage19.PerformLayout()
        CType(Me.GridControl7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit10.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit11.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage20.ResumeLayout(False)
        Me.XtraTabPage20.PerformLayout()
        CType(Me.GridControl8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit12.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit13.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage6.ResumeLayout(False)
        Me.XtraTabPage6.PerformLayout()
        CType(Me.GridControl9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit14.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit15.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage7.ResumeLayout(False)
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl3.ResumeLayout(False)
        Me.XtraTabPage21.ResumeLayout(False)
        Me.XtraTabPage21.PerformLayout()
        CType(Me.GridControl10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit16.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit17.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage22.ResumeLayout(False)
        Me.XtraTabPage22.PerformLayout()
        CType(Me.GridControl11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit18.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit19.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage23.ResumeLayout(False)
        Me.XtraTabPage23.PerformLayout()
        CType(Me.GridControl12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit20.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit21.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage24.ResumeLayout(False)
        Me.XtraTabPage24.PerformLayout()
        CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage8.ResumeLayout(False)
        CType(Me.XtraTabControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl4.ResumeLayout(False)
        Me.XtraTabPage25.ResumeLayout(False)
        Me.XtraTabPage25.PerformLayout()
        CType(Me.GridControl14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit22.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit23.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit23.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage26.ResumeLayout(False)
        Me.XtraTabPage26.PerformLayout()
        CType(Me.GridControl15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit24.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit24.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit25.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit25.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage9.ResumeLayout(False)
        Me.XtraTabPage9.PerformLayout()
        CType(Me.GridControl16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit27.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit27.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage10.ResumeLayout(False)
        Me.XtraTabPage10.PerformLayout()
        CType(Me.GridControl17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit26.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit26.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage11.ResumeLayout(False)
        Me.XtraTabPage11.PerformLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage12.ResumeLayout(False)
        Me.XtraTabPage12.PerformLayout()
        CType(Me.GridControl18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit28.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit28.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage13.ResumeLayout(False)
        Me.XtraTabPage13.PerformLayout()
        CType(Me.GridControl19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit29.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit29.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit30.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit30.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage14.ResumeLayout(False)
        Me.XtraTabPage14.PerformLayout()
        CType(Me.GridControl24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit23.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit36.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit36.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage15.ResumeLayout(False)
        Me.XtraTabPage15.PerformLayout()
        CType(Me.GridControl20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit32.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit32.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage16.ResumeLayout(False)
        Me.XtraTabPage16.PerformLayout()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit21.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit31.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit31.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit33.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit33.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage17.ResumeLayout(False)
        Me.XtraTabPage17.PerformLayout()
        CType(Me.GridControl22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit34.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit34.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit35.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit35.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage7 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage8 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage9 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage10 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage11 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage12 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage13 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage14 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage15 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage16 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage17 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit5 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Private WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton10 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton11 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit4 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton15 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl5 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit6 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton14 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit7 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage18 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton16 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl6 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit8 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit5 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton17 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit9 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage19 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl7 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit10 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit6 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton18 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit11 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage20 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl8 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit12 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit7 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton19 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit13 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl9 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit14 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit8 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton20 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit15 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl3 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage21 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl10 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit16 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit9 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton21 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit17 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage22 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl11 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit18 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit10 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton22 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit19 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage23 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl12 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit20 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit11 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton23 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit21 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage24 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents SimpleButton25 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents GridControl13 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit12 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton24 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl4 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage25 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl14 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView14 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit22 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit13 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton26 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit23 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage26 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl15 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView15 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit24 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit14 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton27 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit25 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl16 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView16 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit15 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton28 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit27 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl17 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView17 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit16 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton29 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit26 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton31 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton30 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents GridControl18 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView18 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit17 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton32 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit28 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton33 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl19 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView19 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit29 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit18 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton34 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit30 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl20 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView20 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit19 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton36 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit32 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl61 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit21 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton35 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl21 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView21 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit31 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit20 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton37 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit33 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton38 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl22 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView22 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl62 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit34 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl63 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit22 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton39 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit35 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl64 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControl24 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView24 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridControl23 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView23 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LabelControl65 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit23 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents SimpleButton40 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit36 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl66 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ReportViewer2 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ReportViewer3 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
End Class
