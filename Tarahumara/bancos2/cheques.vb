﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen

Public Class cheques
    Dim lacuenta As Integer = 0
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                         oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '                         ";password=" & oConfig.pClave

    Private Sub CatalogoDeBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        LlenaCombobox(ComboBoxEdit2, "Select id as numero, nombre from bancoscatalogo order by nombre", "nombre", "numero", conexion)
        LlenaComboDx(cbTienda, "numero", "Nombre", "tiendas", conexion)
        LlenaComboDx(cbTipo, "numero", "RazonSocial", "proveed", conexion)
        DateEdit1.Text = Now.Date
        TextEdit2.Text = 0
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(23, funciones.Mensaje.Texto), IdiomaMensajes(23, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TextBox17.Focus()
            Exit Sub
        End If
        If ComboBoxEdit1.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(24, funciones.Mensaje.Texto), IdiomaMensajes(24, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ComboBoxEdit1.Focus()
            Exit Sub
        End If
        If ComboBoxEdit2.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(25, funciones.Mensaje.Texto), IdiomaMensajes(25, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ComboBoxEdit2.Focus()
            Exit Sub
        End If
        If TextEdit2.Text = 0 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(26, funciones.Mensaje.Texto), IdiomaMensajes(26, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TextEdit2.Focus()
            Exit Sub
        End If

        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Dim iGasto As Integer = -1
        Dim iProve As Integer = -1
        Dim iTienda As Integer = -1
        Dim tipo As Integer = 0
        Select Case RadioGroup1.SelectedIndex
            Case 0
                iProve = CType(cbTipo.SelectedItem, cInfoCombo).ID
                tipo = 3
            Case 1
                iGasto = CType(cbTipo.SelectedItem, cInfoCombo).ID
                If Not ckTodas.Checked Then iTienda = CType(cbTienda.SelectedItem, cInfoCombo).ID
                tipo = 4
        End Select
        Dim monto As String = TextEdit2.Text
        monto = monto.Replace("$", "")
        monto = monto.Replace(",", "")
        Dim sQry As String = "INSERT INTO detBanco (idCuenta, Fecha, Referencia, Concepto, Cheque, tipo, Beneficiario, idgasto, idProveedor, idTienda) VALUES (" & _
                             CType(ComboBoxEdit1.SelectedItem, cInfoCombo).ID & ", '" & String.Format("{0:yyyyMMdd}", DateEdit1.DateTime) & "', '" & _
                             txCheque.Text.Trim & "', '" & TextBox17.Text.Trim & "', '" & monto & "', " & tipo & ", '" & txBeneficiario.Text.Trim & "', " & iGasto & _
                             ", " & iProve & ", " & iTienda & ")"
        Dim regresa As String = bdBase.bdExecute(conexion, sQry)
        Dim mensaje As String = String.Empty
        If String.IsNullOrEmpty(regresa) Then
            ComboBoxEdit1.SelectedIndex = -1
            ComboBoxEdit2.SelectedIndex = -1
            TextEdit1.Text = ""
            DateEdit1.DateTime = Now.Date
            TextEdit2.Text = ""
            TextBox17.Text = ""
            txCheque.Text = ""
            txBeneficiario.Text = ""
            RadioGroup1.SelectedIndex = 0
            mensaje = IdiomaMensajes(741, funciones.Mensaje.Texto)
        Else
            mensaje = IdiomaMensajes(742, funciones.Mensaje.Texto)
        End If

        SplashScreenManager.CloseForm()
        DevExpress.XtraEditors.XtraMessageBox.Show(mensaje, IdiomaMensajes(739, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit2.SelectedIndex <> -1 Then
            LlenaCombobox(ComboBoxEdit1, "Select id as numero,cuenta as nombre from [CuentasBancarias] where idbanco='" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "'   order by cuenta", "nombre", "numero", conexion)
            ComboBoxEdit1.SelectedIndex = -1
            TextEdit1.Text = ""
        End If
    End Sub

    Private Sub ComboBoxEdit1_GotFocus(sender As Object, e As System.EventArgs) Handles ComboBoxEdit1.GotFocus
        TextEdit1.Text = ""
    End Sub

    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        If ComboBoxEdit1.SelectedIndex <> -1 Then
            Dim iqry As String = "Select id,idrazonsocial from [CuentasBancarias] where idbanco='" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "' and cuenta='" & CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.ToString.Trim & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, iqry)
            If dr.HasRows Then
                dr.Read()
                lacuenta = dr("id")
                Dim qry As String = "Select nombre from razones where id='" & dr("idrazonsocial").ToString.Trim & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    TextEdit1.Text = dr2("nombre").ToString.Trim
                Else
                    TextEdit1.Text = "error en razon social"
                End If
                dr2.Close()
            Else
                TextEdit1.Text = "error en razon social"
            End If
            dr.Close()
        End If
    End Sub

    Private Sub ckTodas_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ckTodas.CheckedChanged
        cbTienda.Enabled = Not ckTodas.Checked
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case RadioGroup1.SelectedIndex
            Case 0
                lblTipo.Text = "Proveedor"
                LlenaComboDx(cbTipo, "numero", "Nombre", "proveed", conexion)
                cbTienda.Enabled = False
                ckTodas.Enabled = False
            Case 1
                lblTipo.Text = "Gasto"
                LlenaComboDx(cbTipo, "NUMERO", "GASTO", "gastos", conexion)
                cbTienda.Enabled = False
                ckTodas.Checked = True
                ckTodas.Enabled = True
        End Select
    End Sub
End Class