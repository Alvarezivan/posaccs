﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen

Public Class CuentasBancarias
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                   oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '                   ";password=" & oConfig.pClave
    Private Sub CatalogoDeBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)
        IdiomaTextos(Me)
        ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        LlenaCombobox(ComboBoxEdit1, "Select id as numero,nombre from razones   order by nombre", "nombre", "numero", conexion)
        LlenaCombobox(ComboBoxEdit2, "Select id as numero, nombre from bancoscatalogo   order by nombre", "nombre", "numero", conexion)
        Splash(False)
        reloadCatalogo()

    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            TextBox17.Focus()
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(27, Mensaje.Texto), IdiomaMensajes(27, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        If String.IsNullOrWhiteSpace(TextEdit1.Text) Then
            TextEdit1.Focus()
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(28, Mensaje.Texto), IdiomaMensajes(28, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        If String.IsNullOrWhiteSpace(TextEdit3.Text) Then
            TextEdit3.Focus()
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(29, Mensaje.Texto), IdiomaMensajes(29, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        Splash(True)
        Dim qry As String
        Dim tabla As String = LabelControl2.Text
        Dim campo As String = String.Empty
        campo = "cuenta"
        tabla = "cuentasbancarias"
        qry = "Select id from " & tabla & " where " & campo & " = '" & TextBox17.Text.ToString.Trim.ToUpper & "'"
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If dr2.HasRows Then
            dr2.Close()
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(30, Mensaje.Texto), IdiomaMensajes(30, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        dr2.Close()

        qry = "Insert into " & tabla & " (" & campo & ",idBanco,idRazonSocial,CuentaClabe,Sucursal) values ('" & _
            TextBox17.Text.ToString.Trim.ToUpper & "','" & _
       CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "','" & _
        CType(ComboBoxEdit1.SelectedItem, cInfoCombo).ID.ToString.Trim & "','" & _
        TextEdit1.Text.ToString.Trim.ToUpper & "','" & _
        TextEdit3.Text.ToString.Trim.ToUpper & "')"

        bdBase.bdExecute(conexion, qry)

        TextBox17.Text = ""
        TextEdit1.Text = ""
        TextEdit3.Text = ""
        ComboBoxEdit1.SelectedIndex = -1
        ComboBoxEdit2.SelectedIndex = -1
        Splash(False)
        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(22, Mensaje.Texto), IdiomaMensajes(23, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
        'reloadCatalogo(tabla, campo)
        reloadCatalogo()
    End Sub
    Private Sub reloadCatalogo()
        Splash(True)

        GridControl1.DataSource = Nothing
        Dim qry As String
        qry = "SELECT        CuentasBancarias.id, BancosCatalogo.Nombre AS Banco, CuentasBancarias.Sucursal, CuentasBancarias.CuentaClabe, CuentasBancarias.Cuenta, " & _
                         " razones.NOMBRE AS RazonSocial " & _
                         " FROM            CuentasBancarias INNER JOIN " & _
                         " BancosCatalogo ON CuentasBancarias.idBanco = BancosCatalogo.id INNER JOIN " & _
                         " razones ON CuentasBancarias.idRazonSocial = razones.ID " & _
                         "  ORDER BY Banco, RazonSocial "
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        If Not ds Is Nothing Then
            GridControl1.DataSource = ds.Tables(0)
            'DataGridView2.Columns(0).Visible = False
        Else
        End If
        GridControl1.DataSource = ds.Tables(0)
        GridView1.PopulateColumns()
        GridView1.HorzScrollVisibility = True
        GridView1.VertScrollVisibility = True
        GridView1.BestFitColumns()
        For x As Integer = 0 To 1
            GridView1.Columns(x).OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Next
        'GridControl1.HasChildren
        GridView1.Columns(0).OptionsColumn.AllowEdit = False
        Splash(False)
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Splash(True)
        Dim row As DataRow = GridView1.GetDataRow(e.RowHandle)
        Dim qry, campo, tabla As String

        qry = "update  bancoscatalogo set nombre='" & e.Value.ToString.Trim.ToUpper & "' where id='" & row.Item("id") & "'"
        tabla = "bancoscatalogo"
        campo = "nombre"


        bdBase.bdExecute(conexion, qry, False, False)
        Splash(False)
        'reloadCatalogo(tabla, campo)

    End Sub
End Class