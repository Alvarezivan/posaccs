﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen

Public Class PagosaProveedor
    Dim Factura As String = String.Empty
    Dim Proveedor = String.Empty
    Dim Saldo As String = String.Empty
    Public Property pFactura As String
        Get
            Return Factura
        End Get
        Set(value As String)
            Factura = value
        End Set
    End Property
    Public Property pProveedor As String
        Get
            Return Proveedor
        End Get
        Set(value As String)
            Proveedor = value
        End Set
    End Property
    Public Property pSaldo As String
        Get
            Return Saldo
        End Get
        Set(value As String)
            Saldo = value
        End Set
    End Property
    Dim lacuenta As Integer = 0
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                         oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '                         ";password=" & oConfig.pClave

    Private Sub CatalogoDeBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        Splash(True)
        ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        LlenaCombobox(ComboBoxEdit2, "Select id as numero, nombre from bancoscatalogo order by nombre", "nombre", "numero", conexion)
        LlenaComboDx(cbTienda, "numero", "Nombre", "tiendas", conexion)
        'LlenaComboDx(cbTipo, "id", "Nombre", "proveed", conexion)
        LlenaCombobox(cbTipo, "Select  numero, nombre from proveed where numero='" & Proveedor & "' order by nombre", "nombre", "numero", conexion)
        cbTipo.SelectedIndex = 0
        txBeneficiario.Text = cbTipo.Text
        DateEdit1.Text = Now.Date
        TextEdit2.Text = Saldo
        'TextBox17.Text = "Pago a Factura No." & Factura.ToString.Trim
        Splash(False)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(613, funciones.Mensaje.Texto), IdiomaMensajes(613, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TextBox17.Focus()
            Exit Sub
        End If
        If String.IsNullOrWhiteSpace(txCheque.Text) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(614, funciones.Mensaje.Texto), IdiomaMensajes(614, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txCheque.Focus()
            Exit Sub
        End If
        If ComboBoxEdit1.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(615, funciones.Mensaje.Texto), IdiomaMensajes(615, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ComboBoxEdit1.Focus()
            Exit Sub
        End If
        If ComboBoxEdit2.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(616, funciones.Mensaje.Texto), IdiomaMensajes(616, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ComboBoxEdit2.Focus()
            Exit Sub
        End If
        If TextEdit2.Text = 0 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(617, funciones.Mensaje.Texto), IdiomaMensajes(617, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            TextEdit2.Focus()
            Exit Sub
        End If

        Splash(True)
        Dim iGasto As Integer = -1
        Dim iProve As Integer = -1
        Dim iTienda As Integer = -1
        Dim tipo As Integer = 0
        Select Case RadioGroup1.SelectedIndex
            Case 0
                iProve = CType(cbTipo.SelectedItem, cInfoCombo).ID
                tipo = 3
            Case 1
                iGasto = CType(cbTipo.SelectedItem, cInfoCombo).ID
                If Not ckTodas.Checked Then iTienda = CType(cbTienda.SelectedItem, cInfoCombo).ID
                tipo = 4
        End Select
        Dim monto As String = TextEdit2.Text
        monto = monto.Replace("$", "")
        monto = monto.Replace(",", "")
        Dim sQry As String = "INSERT INTO detBanco (idCuenta, Fecha, Referencia, Concepto, Cheque, tipo, Beneficiario, idgasto, idProveedor, idTienda) VALUES (" & _
                             CType(ComboBoxEdit1.SelectedItem, cInfoCombo).ID & ", '" & String.Format("{0:yyyyMMdd}", DateEdit1.DateTime) & "', '" & _
                             txCheque.Text.Trim & "', '" & TextBox17.Text.Trim & "', '" & monto & "', " & tipo & ", '" & txBeneficiario.Text.Trim & "', " & iGasto & _
                             ", " & iProve & ", " & iTienda & ")"
        Dim regresa As String = bdBase.bdExecute(conexion, sQry)
        Dim qry As String = "INSERT INTO [FacturaPagos]([Factura],[idProveedor] " & _
                            " ,[Fecha],[idBanco],[idCuenta],[NumeroCheque],[Monto] " & _
                            " ,[MontoOperado],[idUsuario]) values ('" & _
                                 Factura.ToString.Trim & "','" & _
                                 Proveedor.ToString.Trim & "','" & _
                                 String.Format("{0:yyyyMMdd}", DateEdit1.DateTime) & "','" & _
                                  CType(ComboBoxEdit1.SelectedItem, cInfoCombo).ID & "','" & _
        CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID & "','" & _
         txCheque.Text.Trim & "','" & _
         monto.ToString.Trim & "','" & _
        monto.ToString.Trim & "','" & _
        oLogin.pUserId & "')"



        Dim regresa2 As String = bdBase.bdExecute(conexion, qry)

        If Saldo = monto Then
            qry = "Update facturas set status=3 where factura='" & Factura.ToString.Trim & "' and proveedor='" & Proveedor.ToString.Trim & "'"
            bdBase.bdExecute(conexion, qry)
        End If
        Dim mensaje As String = String.Empty
        If String.IsNullOrEmpty(regresa) Then
            ComboBoxEdit1.SelectedIndex = -1
            ComboBoxEdit2.SelectedIndex = -1
            TextEdit1.Text = ""
            DateEdit1.DateTime = Now.Date
            TextEdit2.Text = ""
            TextBox17.Text = ""
            txCheque.Text = ""
            txBeneficiario.Text = ""
            RadioGroup1.SelectedIndex = 0
            mensaje = IdiomaMensajes(618, funciones.Mensaje.Texto)
        Else
            mensaje = IdiomaMensajes(619, funciones.Mensaje.Texto)
        End If

        Splash(False)
        DevExpress.XtraEditors.XtraMessageBox.Show(mensaje, IdiomaMensajes(617, funciones.Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub

    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit2.SelectedIndex <> -1 Then
            LlenaCombobox(ComboBoxEdit1, "Select id as numero,cuenta as nombre from [CuentasBancarias] where idbanco='" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "'   order by cuenta", "nombre", "numero", conexion)
            ComboBoxEdit1.SelectedIndex = -1
            TextEdit1.Text = ""
        End If
    End Sub

    Private Sub ComboBoxEdit1_GotFocus(sender As Object, e As System.EventArgs) Handles ComboBoxEdit1.GotFocus
        TextEdit1.Text = ""
    End Sub

    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        If ComboBoxEdit1.SelectedIndex <> -1 Then
            Dim iqry As String = "Select id,idrazonsocial from [CuentasBancarias] where idbanco='" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "' and cuenta='" & CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text.ToString.Trim & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, iqry)
            If dr.HasRows Then
                dr.Read()
                lacuenta = dr("id")
                Dim qry As String = "Select nombre from razones where id ='" & dr("idrazonsocial").ToString.Trim & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    TextEdit1.Text = dr2("nombre").ToString.Trim
                Else
                    TextEdit1.Text = "error en razon social"
                End If
                dr2.Close()
            Else
                TextEdit1.Text = "error en razon social"
            End If
            dr.Close()
        End If
    End Sub

    Private Sub ckTodas_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ckTodas.CheckedChanged
        cbTienda.Enabled = Not ckTodas.Checked
    End Sub

    Private Sub RadioGroup1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case RadioGroup1.SelectedIndex
            Case 0
                lblTipo.Text = "Proveedor"
                LlenaComboDx(cbTipo, "id", "Nombre", "proveed", conexion)
                cbTienda.Enabled = False
                ckTodas.Enabled = False
            Case 1
                lblTipo.Text = "Gasto"
                LlenaComboDx(cbTipo, "NUMERO", "GASTO", "gastos", conexion)
                cbTienda.Enabled = False
                ckTodas.Checked = True
                ckTodas.Enabled = True
        End Select
    End Sub

  

    'Private Sub TextEdit2_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles TextEdit2.Validating
    '    Dim utemp As String = TextEdit2.Text
    '    utemp = utemp.Replace("$", "")
    '    utemp = utemp.Replace(",", "")
    '    If Convert.ToDecimal(utemp) > Saldo Then
    '        TextEdit2.Text = Saldo
    '    End If
    'End Sub
End Class