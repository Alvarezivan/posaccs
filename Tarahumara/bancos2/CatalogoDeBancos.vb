﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen

Public Class CatalogoDeBancos
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                   oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '                   ";password=" & oConfig.pClave
    Private Sub CatalogoDeBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        Dim tabla As String = LabelControl2.Text
        Dim campo As String = String.Empty
        campo = "Nombre"
        tabla = "BancosCatalogo"
        reloadCatalogo(tabla, campo)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            TextBox17.Focus()
            Exit Sub
        End If
        Splash(True)
        Dim qry As String
        Dim tabla As String = LabelControl2.Text
        Dim campo As String = String.Empty
        campo = "Nombre"
        tabla = "BancosCatalogo"
        qry = "Select id from " & tabla & " where " & campo & " = '" & TextBox17.Text.ToString.Trim.ToUpper & "'"
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If dr2.HasRows Then
            dr2.Close()
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(21, Mensaje.Texto), IdiomaMensajes(21, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        dr2.Close()

        qry = "Insert into " & tabla & "(" & campo & ") values ('" & TextBox17.Text.ToString.Trim.ToUpper & "')"

        bdBase.bdExecute(conexion, qry)

        TextBox17.Text = ""
        Splash(False)
        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(22, Mensaje.Texto), IdiomaMensajes(22, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
        reloadCatalogo(tabla, campo)
    End Sub
    Private Sub reloadCatalogo(ByVal tabla As String, ByVal campo As String)
        Splash(True)

        GridControl1.DataSource = Nothing
        Dim qry As String = "Select id, " & campo & " as Nombre from " & tabla & "    order by " & campo
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        If Not ds Is Nothing Then
            GridControl1.DataSource = ds.Tables(0)
            'DataGridView2.Columns(0).Visible = False
        Else
        End If
        GridControl1.DataSource = ds.Tables(0)
        GridView1.PopulateColumns()
        GridView1.HorzScrollVisibility = True
        GridView1.VertScrollVisibility = True
        GridView1.BestFitColumns()
        For x As Integer = 0 To 1
            GridView1.Columns(x).OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Next
        'GridControl1.HasChildren
        GridView1.Columns(0).OptionsColumn.AllowEdit = False
        Splash(False)
    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Splash(True)
        Dim row As DataRow = GridView1.GetDataRow(e.RowHandle)
        Dim qry, campo, tabla As String
     
        qry = "update  bancoscatalogo set nombre='" & e.Value.ToString.Trim.ToUpper & "' where id='" & row.Item("id") & "'"
        tabla = "bancoscatalogo"
        campo = "nombre"
            

        bdBase.bdExecute(conexion, qry, False, False)
        Splash(False)
        reloadCatalogo(tabla, campo)

    End Sub
End Class