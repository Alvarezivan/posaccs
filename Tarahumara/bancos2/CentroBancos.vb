﻿Imports System.Reflection
Imports System.IO

Public Class CentroBancos

    Private Sub TileItem8_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiCatalogoBancos.ItemClick

        CatalogoDeBancos.Show()
        
    End Sub

    Private Sub TileItem7_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiCatalogoCuentasB.ItemClick

        CuentasBancarias.Show()

    End Sub

    Private Sub TileItem1_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiCheques.ItemClick

        PagosaProveedor.Show()

    End Sub

    Private Sub TileItem3_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiEstadoBancario.ItemClick

        EstadoDeCuenta.Show()

    End Sub

    Private Sub TileItem2_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiDepositos.ItemClick

        Depositos.Show()

    End Sub

    Private Sub TileItem4_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiEstadoContable.ItemClick
       
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(7, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub

    Private Sub TileItem5_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs) Handles tiCierreMensual.ItemClick

        cierremensual.Show()

    End Sub

    Private Sub CentroBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        Idioma()
    End Sub

    Private Sub CentroBancos_Layout(sender As System.Object, e As System.Windows.Forms.LayoutEventArgs) Handles MyBase.Layout
        '        Idioma()
    End Sub

    Private Sub Idioma()
        Dim item As New DevExpress.XtraEditors.TileItemFrame
        Dim imagen As Stream
        Dim ensamblado As Assembly
        ensamblado = Assembly.GetExecutingAssembly()
        If (My.Settings.idiomaES = True) Then
            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos3.png")
            'tiCheques.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos4.png")
            'tiDepositos.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos6.png")
            'tiCierreMensual.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos7.png")
            'tiCatalogoCuentasB.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos1.png")
            'tiEstadoBancario.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos5.png")
            'tiCatalogoBancos.BackgroundImage = New Bitmap(imagen)

            'imagen = ensamblado.GetManifestResourceStream("AllInOneSync.bancos2.png")
            'tiEstadoContable.BackgroundImage = New Bitmap(imagen)

        Else
            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos3.png")
            tiCheques.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos4.png")
            tiDepositos.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos6.png")
            tiCierreMensual.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos7.png")
            tiCatalogoCuentasB.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos1.png")
            tiEstadoBancario.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos5.png")
            tiCatalogoBancos.BackgroundImage = New Bitmap(imagen)

            imagen = ensamblado.GetManifestResourceStream("AllInOneSync.ibancos2.png")
            tiEstadoContable.BackgroundImage = New Bitmap(imagen)

        End If
    End Sub
End Class