﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen

Public Class EstadoDeCuenta
    Dim lacuenta As Integer = 0
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                   oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '                   ";password=" & oConfig.pClave
    Private Sub CatalogoDeBancos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)
        IdiomaTextos(Me)
        ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        LlenaCombobox(ComboBoxEdit2, "Select id as numero, nombre from bancoscatalogo   order by nombre", "nombre", "numero", conexion)
        SpinEdit1.Value = Now.Date.Year
        ComboBoxEdit3.SelectedIndex = Now.Month - 1
        Splash(False)
    End Sub

    






    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit2.SelectedIndex <> -1 Then
            LlenaCombobox(ComboBoxEdit1, "Select idbanco as numero,cuenta as nombre from [CuentasBancarias] where idbanco='" & CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "'   order by cuenta", "nombre", "numero", conexion)
            ComboBoxEdit1.SelectedIndex = -1
            TextEdit1.Text = ""
        End If

    End Sub

    Private Sub ComboBoxEdit1_GotFocus(sender As Object, e As System.EventArgs) Handles ComboBoxEdit1.GotFocus
        TextEdit1.Text = ""
    End Sub

    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        If ComboBoxEdit1.SelectedIndex <> -1 Then
            Dim iqry As String = "Select id,idrazonsocial from [CuentasBancarias] where idbanco='" & _
                CType(ComboBoxEdit2.SelectedItem, cInfoCombo).ID.ToString.Trim & "' and cuenta='" & _
                CType(ComboBoxEdit1.SelectedItem, cInfoCombo).Text & "'"

            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, iqry)
            If dr.HasRows Then
                dr.Read()
                lacuenta = dr("id")
                Dim qry As String = "Select nombre from razones where id='" & dr("idrazonsocial").ToString.Trim & "'"
                Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If dr2.HasRows Then
                    dr2.Read()
                    TextEdit1.Text = dr2("nombre").ToString.Trim
                Else
                    TextEdit1.Text = "error en razon social"
                End If
            Else
                TextEdit1.Text = "error en razon social"
            End If
        End If


    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If ComboBoxEdit1.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(32, Mensaje.Texto), IdiomaMensajes(32, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub
        End If
        If ComboBoxEdit2.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(33, Mensaje.Texto), IdiomaMensajes(33, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Exit Sub
        End If
        Splash(True)
        Dim qry As String = ""
        Dim mes As String = ""
        mes = ComboBoxEdit3.SelectedIndex + 1
        mes = mes.ToString.PadLeft(2, "0")
        qry = " SELECT detbanco.id, detbanco.Fecha, detbanco.Referencia, detbanco.Concepto, isnull(detbanco.Cheque,0) as Cheque, isnull(detbanco.Deposito,0) as Deposito, detbanco.Status, detbanco.Tipo, detbanco.Beneficiario, " & _
                       "   detbanco.FechaOperacion, 0 AS Saldo, BancosCatalogo.Nombre as Banco, CuentasBancarias.Cuenta, detbanco.Conciliado " & _
                       " FROM            detbanco INNER JOIN " & _
                       " CuentasBancarias ON detbanco.idCuenta = CuentasBancarias.id INNER JOIN " & _
                       " BancosCatalogo ON CuentasBancarias.idBanco = BancosCatalogo.id " & _
                       " where idcuenta='" & lacuenta & "' and fecha between '" & SpinEdit1.Value & mes & "01' and '" & SpinEdit1.Value & mes & _
         Convert.ToString(System.DateTime.DaysInMonth(SpinEdit1.Value, ComboBoxEdit3.SelectedIndex + 1)) & _
                       "' ORDER BY detbanco.id "
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl1.DataSource = ds.Tables(0)
        Dim isaldo As Decimal = 0
        Dim x As Integer
        For x = 0 To GridView1.RowCount - 1
            isaldo = isaldo + GridView1.GetRowCellValue(x, "Deposito")
            isaldo = isaldo - GridView1.GetRowCellValue(x, "Cheque")
            GridView1.SetRowCellValue(x, "Saldo", isaldo)
        Next
        GridView1.PopulateColumns()
        GridView1.Columns(10).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        GridView1.Columns(10).DisplayFormat.FormatString = "c"
        GridView1.Columns(0).Visible = False

        GridView1.BestFitColumns()

        Dim riConciliado As New RepositoryItemCheckEdit
        AddHandler riConciliado.EditValueChanged, AddressOf OnEditValueChanged
        GridView1.Columns("Conciliado").ColumnEdit = riConciliado

        Splash(False)
    End Sub

    Private Sub OnEditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
        gridView1.PostEditor()
    End Sub

    Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Select Case e.Column.FieldName
            Case "Conciliado"
                Dim row As DataRow = GridView1.GetDataRow(e.RowHandle)
                Dim sQuery As String = "UPDATE detbanco SET Conciliado = '" & IIf(e.Value, 1, 0) & "' WHERE id = '" & row.Item("id") & "'"
                Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
        End Select
    End Sub

    Private Sub GridView1_ShowingEditor(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles GridView1.ShowingEditor
        Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = sender
        Dim columna As DevExpress.XtraGrid.Columns.GridColumn = gv.FocusedColumn
        Select Case columna.FieldName
            Case "Conciliado"
            Case Else
                ' Deshabilita edicion en todas las demás
                e.Cancel = True
        End Select
    End Sub
End Class