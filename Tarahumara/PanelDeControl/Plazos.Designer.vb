﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Plazos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.chkactivo = New DevExpress.XtraEditors.CheckEdit()
        Me.tbminimo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.chkpermanente = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.dtfinal = New System.Windows.Forms.DateTimePicker()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.dtinicial = New System.Windows.Forms.DateTimePicker()
        Me.Vigencia = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.tbinteres = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.tbpagos = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.tblibre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tbplazo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.tbnombre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Folio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.chkactivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbminimo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.chkpermanente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbinteres.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpagos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tblibre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbplazo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbnombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(704, 374)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.chkactivo)
        Me.XtraTabPage1.Controls.Add(Me.tbminimo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage1.Controls.Add(Me.PanelControl1)
        Me.XtraTabPage1.Controls.Add(Me.SimpleButton1)
        Me.XtraTabPage1.Controls.Add(Me.tbinteres)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage1.Controls.Add(Me.tbpagos)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage1.Controls.Add(Me.tblibre)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage1.Controls.Add(Me.tbplazo)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage1.Controls.Add(Me.tbnombre)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(698, 346)
        Me.XtraTabPage1.Text = "Alta de Plazos"
        '
        'chkactivo
        '
        Me.chkactivo.EditValue = True
        Me.chkactivo.Location = New System.Drawing.Point(382, 208)
        Me.chkactivo.Name = "chkactivo"
        Me.chkactivo.Properties.Caption = "Activo"
        Me.chkactivo.Size = New System.Drawing.Size(58, 19)
        Me.chkactivo.TabIndex = 20
        '
        'tbminimo
        '
        Me.tbminimo.Location = New System.Drawing.Point(212, 204)
        Me.tbminimo.Name = "tbminimo"
        Me.tbminimo.Size = New System.Drawing.Size(136, 20)
        Me.tbminimo.TabIndex = 19
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(72, 211)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(120, 13)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Aplica en venta mayor a:"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.chkpermanente)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.dtfinal)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.dtinicial)
        Me.PanelControl1.Controls.Add(Me.Vigencia)
        Me.PanelControl1.Location = New System.Drawing.Point(486, 77)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(137, 147)
        Me.PanelControl1.TabIndex = 17
        '
        'chkpermanente
        '
        Me.chkpermanente.Location = New System.Drawing.Point(22, 110)
        Me.chkpermanente.Name = "chkpermanente"
        Me.chkpermanente.Properties.Caption = "Permanente"
        Me.chkpermanente.Size = New System.Drawing.Size(90, 19)
        Me.chkpermanente.TabIndex = 23
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(5, 82)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(9, 13)
        Me.LabelControl7.TabIndex = 22
        Me.LabelControl7.Text = "Al"
        '
        'dtfinal
        '
        Me.dtfinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtfinal.Location = New System.Drawing.Point(24, 76)
        Me.dtfinal.Name = "dtfinal"
        Me.dtfinal.Size = New System.Drawing.Size(101, 21)
        Me.dtfinal.TabIndex = 21
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(5, 46)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(13, 13)
        Me.LabelControl6.TabIndex = 20
        Me.LabelControl6.Text = "De"
        '
        'dtinicial
        '
        Me.dtinicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtinicial.Location = New System.Drawing.Point(24, 40)
        Me.dtinicial.Name = "dtinicial"
        Me.dtinicial.Size = New System.Drawing.Size(101, 21)
        Me.dtinicial.TabIndex = 19
        '
        'Vigencia
        '
        Me.Vigencia.Location = New System.Drawing.Point(5, 5)
        Me.Vigencia.Name = "Vigencia"
        Me.Vigencia.Size = New System.Drawing.Size(39, 13)
        Me.Vigencia.TabIndex = 18
        Me.Vigencia.Text = "Vigencia"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(486, 260)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(137, 46)
        Me.SimpleButton1.TabIndex = 16
        Me.SimpleButton1.Text = "Guardar"
        '
        'tbinteres
        '
        Me.tbinteres.Location = New System.Drawing.Point(369, 158)
        Me.tbinteres.Name = "tbinteres"
        Me.tbinteres.Size = New System.Drawing.Size(71, 20)
        Me.tbinteres.TabIndex = 15
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(264, 165)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(35, 13)
        Me.LabelControl5.TabIndex = 14
        Me.LabelControl5.Text = "Interes"
        '
        'tbpagos
        '
        Me.tbpagos.Location = New System.Drawing.Point(159, 158)
        Me.tbpagos.Name = "tbpagos"
        Me.tbpagos.Size = New System.Drawing.Size(71, 20)
        Me.tbpagos.TabIndex = 13
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(72, 165)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(49, 13)
        Me.LabelControl4.TabIndex = 12
        Me.LabelControl4.Text = "No. Pagos"
        '
        'tblibre
        '
        Me.tblibre.Location = New System.Drawing.Point(369, 112)
        Me.tblibre.Name = "tblibre"
        Me.tblibre.Size = New System.Drawing.Size(71, 20)
        Me.tblibre.TabIndex = 11
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(264, 119)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(84, 13)
        Me.LabelControl3.TabIndex = 10
        Me.LabelControl3.Text = "Dias Libre Interes"
        '
        'tbplazo
        '
        Me.tbplazo.Location = New System.Drawing.Point(159, 112)
        Me.tbplazo.Name = "tbplazo"
        Me.tbplazo.Size = New System.Drawing.Size(71, 20)
        Me.tbplazo.TabIndex = 9
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(72, 119)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 13)
        Me.LabelControl2.TabIndex = 8
        Me.LabelControl2.Text = "Plazo dias"
        '
        'tbnombre
        '
        Me.tbnombre.Location = New System.Drawing.Point(159, 74)
        Me.tbnombre.Name = "tbnombre"
        Me.tbnombre.Size = New System.Drawing.Size(281, 20)
        Me.tbnombre.TabIndex = 7
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(72, 81)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Nombre"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Controls.Add(Me.CheckEdit3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(698, 346)
        Me.XtraTabPage2.Text = "Reporte de Plazos"
        '
        'GridControl2
        '
        Me.GridControl2.Location = New System.Drawing.Point(3, 3)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(693, 315)
        Me.GridControl2.TabIndex = 41
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2, Me.GridView1})
        '
        'GridView2
        '
        Me.GridView2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Folio})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView2.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowRowSizing = True
        Me.GridView2.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView2.OptionsFind.AlwaysVisible = True
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'Folio
        '
        Me.Folio.Caption = "Folio"
        Me.Folio.Name = "Folio"
        Me.Folio.Visible = True
        Me.Folio.VisibleIndex = 0
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl2
        Me.GridView1.Name = "GridView1"
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(616, 324)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Caption = "Edita Plazo"
        Me.CheckEdit3.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit3.TabIndex = 42
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(698, 346)
        Me.XtraTabPage3.Text = "XtraTabPage3"
        '
        'Plazos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 374)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Plazos"
        Me.Text = "Plazos"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.chkactivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbminimo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.chkpermanente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbinteres.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpagos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tblibre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbplazo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbnombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tbpagos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tblibre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbplazo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbnombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents Vigencia As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbinteres As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbminimo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkpermanente As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtfinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtinicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkactivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Folio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
End Class
