﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Administradores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.btCancelar = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.btAceptar = New DevExpress.XtraEditors.SimpleButton()
        Me.txClave = New DevExpress.XtraEditors.TextEdit()
        Me.txUsuario = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.lblInfo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.txClave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txUsuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btCancelar
        '
        Me.btCancelar.Location = New System.Drawing.Point(92, 183)
        Me.btCancelar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btCancelar.Name = "btCancelar"
        Me.btCancelar.Size = New System.Drawing.Size(171, 66)
        Me.btCancelar.TabIndex = 16
        Me.btCancelar.Text = "Cancelar"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.btAceptar)
        Me.PanelControl1.Controls.Add(Me.txClave)
        Me.PanelControl1.Controls.Add(Me.txUsuario)
        Me.PanelControl1.Controls.Add(Me.LabelControl3)
        Me.PanelControl1.Controls.Add(Me.LabelControl2)
        Me.PanelControl1.Controls.Add(Me.lblInfo)
        Me.PanelControl1.Controls.Add(Me.LabelControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(341, 287)
        Me.PanelControl1.TabIndex = 17
        '
        'btAceptar
        '
        Me.btAceptar.Location = New System.Drawing.Point(219, 66)
        Me.btAceptar.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(100, 53)
        Me.btAceptar.TabIndex = 6
        Me.btAceptar.Text = "Aceptar"
        '
        'txClave
        '
        Me.txClave.Location = New System.Drawing.Point(79, 97)
        Me.txClave.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txClave.Name = "txClave"
        Me.txClave.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txClave.Size = New System.Drawing.Size(133, 22)
        Me.txClave.TabIndex = 5
        '
        'txUsuario
        '
        Me.txUsuario.Location = New System.Drawing.Point(79, 65)
        Me.txUsuario.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txUsuario.Name = "txUsuario"
        Me.txUsuario.Size = New System.Drawing.Size(133, 22)
        Me.txUsuario.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(17, 94)
        Me.LabelControl3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(36, 16)
        Me.LabelControl3.TabIndex = 3
        Me.LabelControl3.Text = "Clave:"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(17, 69)
        Me.LabelControl2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(48, 16)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Usuario:"
        '
        'lblInfo
        '
        Me.lblInfo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblInfo.Appearance.ForeColor = System.Drawing.Color.Red
        Me.lblInfo.Location = New System.Drawing.Point(35, 148)
        Me.lblInfo.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(260, 17)
        Me.lblInfo.TabIndex = 1
        Me.lblInfo.Text = "¡No identificado! Reintente por favor."
        Me.lblInfo.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
        Me.LabelControl1.Location = New System.Drawing.Point(31, 15)
        Me.LabelControl1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(288, 34)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Usuario SUPERVISOR, por favor coloque su dedo en el lector."
        '
        'Administradores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(341, 287)
        Me.Controls.Add(Me.btCancelar)
        Me.Controls.Add(Me.PanelControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Administradores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Administradores"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.txClave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txUsuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents btCancelar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblInfo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btAceptar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txClave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txUsuario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
End Class
