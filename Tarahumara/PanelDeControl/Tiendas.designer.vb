﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Tiendas
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Tiendas))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.Button13 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button6 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button7 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBox15 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.ChkAlmacenInventario = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.seRetardo = New DevExpress.XtraEditors.SpinEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit()
        Me.seComida = New DevExpress.XtraEditors.SpinEdit()
        Me.seTolera = New DevExpress.XtraEditors.SpinEdit()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.seTolComida = New DevExpress.XtraEditors.SpinEdit()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbAlmacenInv = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button5 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBox16 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox33 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Button8 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBox34 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox32 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox20 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox19 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox18 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox17 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextBox32 = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox27 = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox28 = New DevExpress.XtraEditors.TextEdit()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.textbox22 = New DevExpress.XtraEditors.TextEdit()
        Me.textbox23 = New DevExpress.XtraEditors.TextEdit()
        Me.textbox59 = New DevExpress.XtraEditors.TextEdit()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.ComboBox15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.ChkAlmacenInventario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.seRetardo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seComida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seTolera.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.seTolComida.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbAlmacenInv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox33.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox34.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox32.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox32.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox27.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox28.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textbox22.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textbox23.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textbox59.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(872, 640)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.GroupControl8)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl7)
        Me.XtraTabPage1.Controls.Add(Me.GroupControl6)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(866, 612)
        Me.XtraTabPage1.Text = "ABC Tiendas"
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.GridControl4)
        Me.GroupControl8.Location = New System.Drawing.Point(0, 323)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(866, 292)
        Me.GroupControl8.TabIndex = 135
        Me.GroupControl8.Text = "Tiendas/Almacenes"
        '
        'GridControl4
        '
        Me.GridControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl4.Location = New System.Drawing.Point(2, 21)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(862, 269)
        Me.GridControl4.TabIndex = 92
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView4.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsBehavior.ReadOnly = True
        Me.GridView4.OptionsCustomization.AllowRowSizing = True
        Me.GridView4.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView4.OptionsFind.AlwaysVisible = True
        Me.GridView4.OptionsView.ColumnAutoWidth = False
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        Me.GridView4.OptionsView.ShowFooter = True
        Me.GridView4.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'GroupControl7
        '
        Me.GroupControl7.Appearance.BackColor = System.Drawing.Color.Gray
        Me.GroupControl7.Appearance.Options.UseBackColor = True
        Me.GroupControl7.Controls.Add(Me.Button13)
        Me.GroupControl7.Controls.Add(Me.Button6)
        Me.GroupControl7.Controls.Add(Me.Button7)
        Me.GroupControl7.Controls.Add(Me.ComboBox15)
        Me.GroupControl7.Location = New System.Drawing.Point(0, 262)
        Me.GroupControl7.LookAndFeel.SkinName = "Caramel"
        Me.GroupControl7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(866, 62)
        Me.GroupControl7.TabIndex = 134
        Me.GroupControl7.Text = "Localización de Tiendas/Almacenes"
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(695, 27)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(136, 23)
        Me.Button13.TabIndex = 111
        Me.Button13.Text = "Imprimir/Exportar"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(525, 27)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 110
        Me.Button6.Text = "Modificar"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(444, 27)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 109
        Me.Button7.Text = "Mostrar"
        '
        'ComboBox15
        '
        Me.ComboBox15.Location = New System.Drawing.Point(10, 28)
        Me.ComboBox15.Name = "ComboBox15"
        Me.ComboBox15.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox15.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox15.Size = New System.Drawing.Size(428, 20)
        Me.ComboBox15.TabIndex = 108
        '
        'GroupControl6
        '
        Me.GroupControl6.Controls.Add(Me.ChkAlmacenInventario)
        Me.GroupControl6.Controls.Add(Me.GroupControl1)
        Me.GroupControl6.Controls.Add(Me.Label1)
        Me.GroupControl6.Controls.Add(Me.cbAlmacenInv)
        Me.GroupControl6.Controls.Add(Me.SimpleButton1)
        Me.GroupControl6.Controls.Add(Me.Button5)
        Me.GroupControl6.Controls.Add(Me.ComboBox16)
        Me.GroupControl6.Controls.Add(Me.ComboBox33)
        Me.GroupControl6.Controls.Add(Me.Button8)
        Me.GroupControl6.Controls.Add(Me.ComboBox34)
        Me.GroupControl6.Controls.Add(Me.ComboBox32)
        Me.GroupControl6.Controls.Add(Me.ComboBox20)
        Me.GroupControl6.Controls.Add(Me.ComboBox19)
        Me.GroupControl6.Controls.Add(Me.ComboBox18)
        Me.GroupControl6.Controls.Add(Me.ComboBox17)
        Me.GroupControl6.Controls.Add(Me.TextBox32)
        Me.GroupControl6.Controls.Add(Me.TextBox27)
        Me.GroupControl6.Controls.Add(Me.TextBox28)
        Me.GroupControl6.Controls.Add(Me.Label46)
        Me.GroupControl6.Controls.Add(Me.textbox22)
        Me.GroupControl6.Controls.Add(Me.textbox23)
        Me.GroupControl6.Controls.Add(Me.textbox59)
        Me.GroupControl6.Controls.Add(Me.Label78)
        Me.GroupControl6.Controls.Add(Me.Label33)
        Me.GroupControl6.Controls.Add(Me.Label35)
        Me.GroupControl6.Controls.Add(Me.Label36)
        Me.GroupControl6.Controls.Add(Me.Label37)
        Me.GroupControl6.Controls.Add(Me.Label38)
        Me.GroupControl6.Controls.Add(Me.Label39)
        Me.GroupControl6.Controls.Add(Me.Label40)
        Me.GroupControl6.Controls.Add(Me.Label41)
        Me.GroupControl6.Controls.Add(Me.Label42)
        Me.GroupControl6.Controls.Add(Me.Label43)
        Me.GroupControl6.Controls.Add(Me.Label45)
        Me.GroupControl6.Controls.Add(Me.PictureBox1)
        Me.GroupControl6.Controls.Add(Me.Label48)
        Me.GroupControl6.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControl6.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(866, 262)
        Me.GroupControl6.TabIndex = 133
        Me.GroupControl6.Text = "Datos Principales de la Tienda"
        '
        'ChkAlmacenInventario
        '
        Me.ChkAlmacenInventario.Location = New System.Drawing.Point(436, 138)
        Me.ChkAlmacenInventario.Name = "ChkAlmacenInventario"
        Me.ChkAlmacenInventario.Properties.Caption = "Misma tienda"
        Me.ChkAlmacenInventario.Size = New System.Drawing.Size(90, 19)
        Me.ChkAlmacenInventario.TabIndex = 199
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label9)
        Me.GroupControl1.Controls.Add(Me.Label3)
        Me.GroupControl1.Controls.Add(Me.seRetardo)
        Me.GroupControl1.Controls.Add(Me.Label4)
        Me.GroupControl1.Controls.Add(Me.Label10)
        Me.GroupControl1.Controls.Add(Me.TimeEdit1)
        Me.GroupControl1.Controls.Add(Me.Label8)
        Me.GroupControl1.Controls.Add(Me.TimeEdit2)
        Me.GroupControl1.Controls.Add(Me.seComida)
        Me.GroupControl1.Controls.Add(Me.seTolera)
        Me.GroupControl1.Controls.Add(Me.Label7)
        Me.GroupControl1.Controls.Add(Me.Label5)
        Me.GroupControl1.Controls.Add(Me.seTolComida)
        Me.GroupControl1.Controls.Add(Me.Label6)
        Me.GroupControl1.Location = New System.Drawing.Point(15, 182)
        Me.GroupControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(554, 72)
        Me.GroupControl1.TabIndex = 198
        Me.GroupControl1.Text = "Horarios"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 183
        Me.Label2.Text = "Hora Apertura"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.Label9.Location = New System.Drawing.Point(507, 51)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(28, 12)
        Me.Label9.TabIndex = 197
        Me.Label9.Text = "mins."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(113, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 184
        Me.Label3.Text = "Hora Cierre"
        '
        'seRetardo
        '
        Me.seRetardo.EditValue = New Decimal(New Integer() {5, 0, 0, 0})
        Me.seRetardo.Location = New System.Drawing.Point(444, 48)
        Me.seRetardo.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.seRetardo.Name = "seRetardo"
        Me.seRetardo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seRetardo.Properties.MaxLength = 3
        Me.seRetardo.Properties.MaxValue = New Decimal(New Integer() {360, 0, 0, 0})
        Me.seRetardo.Size = New System.Drawing.Size(57, 20)
        Me.seRetardo.TabIndex = 195
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.Label4.Location = New System.Drawing.Point(229, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 12)
        Me.Label4.TabIndex = 185
        Me.Label4.Text = "Entrada"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(441, 32)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 196
        Me.Label10.Text = "Tiempo retardo"
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2013, 9, 26, 9, 0, 0, 0)
        Me.TimeEdit1.Location = New System.Drawing.Point(7, 48)
        Me.TimeEdit1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TimeEdit1.Name = "TimeEdit1"
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit1.Size = New System.Drawing.Size(102, 20)
        Me.TimeEdit1.TabIndex = 180
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.Label8.Location = New System.Drawing.Point(395, 51)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 12)
        Me.Label8.TabIndex = 194
        Me.Label8.Text = "mins."
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2013, 9, 26, 21, 0, 0, 0)
        Me.TimeEdit2.Location = New System.Drawing.Point(116, 48)
        Me.TimeEdit2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TimeEdit2.Name = "TimeEdit2"
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit2.Size = New System.Drawing.Size(102, 20)
        Me.TimeEdit2.TabIndex = 181
        '
        'seComida
        '
        Me.seComida.EditValue = New Decimal(New Integer() {60, 0, 0, 0})
        Me.seComida.Location = New System.Drawing.Point(333, 48)
        Me.seComida.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.seComida.Name = "seComida"
        Me.seComida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seComida.Properties.MaxLength = 3
        Me.seComida.Properties.MaxValue = New Decimal(New Integer() {360, 0, 0, 0})
        Me.seComida.Size = New System.Drawing.Size(57, 20)
        Me.seComida.TabIndex = 184
        '
        'seTolera
        '
        Me.seTolera.EditValue = New Decimal(New Integer() {10, 0, 0, 0})
        Me.seTolera.Location = New System.Drawing.Point(231, 48)
        Me.seTolera.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.seTolera.Name = "seTolera"
        Me.seTolera.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seTolera.Properties.MaxLength = 3
        Me.seTolera.Properties.MaxValue = New Decimal(New Integer() {60, 0, 0, 0})
        Me.seTolera.Size = New System.Drawing.Size(42, 20)
        Me.seTolera.TabIndex = 182
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(330, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 13)
        Me.Label7.TabIndex = 192
        Me.Label7.Text = "Tiempo de comida"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.Label5.Location = New System.Drawing.Point(279, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 12)
        Me.Label5.TabIndex = 189
        Me.Label5.Text = "Comida"
        '
        'seTolComida
        '
        Me.seTolComida.EditValue = New Decimal(New Integer() {5, 0, 0, 0})
        Me.seTolComida.Location = New System.Drawing.Point(279, 48)
        Me.seTolComida.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.seTolComida.Name = "seTolComida"
        Me.seTolComida.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.seTolComida.Properties.MaxLength = 3
        Me.seTolComida.Properties.MaxValue = New Decimal(New Integer() {60, 0, 0, 0})
        Me.seTolComida.Size = New System.Drawing.Size(42, 20)
        Me.seTolComida.TabIndex = 183
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 7.5!)
        Me.Label6.Location = New System.Drawing.Point(244, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 12)
        Me.Label6.TabIndex = 190
        Me.Label6.Text = "Tolerancias"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(309, 142)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 182
        Me.Label1.Text = "Almacén de Inventario"
        '
        'cbAlmacenInv
        '
        Me.cbAlmacenInv.Location = New System.Drawing.Point(312, 157)
        Me.cbAlmacenInv.Name = "cbAlmacenInv"
        Me.cbAlmacenInv.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbAlmacenInv.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbAlmacenInv.Size = New System.Drawing.Size(232, 20)
        Me.cbAlmacenInv.TabIndex = 178
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(573, 182)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(126, 23)
        Me.SimpleButton1.TabIndex = 186
        Me.SimpleButton1.Text = "Control de Zonas"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(575, 231)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(124, 23)
        Me.Button5.TabIndex = 185
        Me.Button5.Text = "&Guardar"
        '
        'ComboBox16
        '
        Me.ComboBox16.Location = New System.Drawing.Point(608, 156)
        Me.ComboBox16.Name = "ComboBox16"
        Me.ComboBox16.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox16.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox16.Size = New System.Drawing.Size(207, 20)
        Me.ComboBox16.TabIndex = 179
        '
        'ComboBox33
        '
        Me.ComboBox33.Location = New System.Drawing.Point(15, 156)
        Me.ComboBox33.Name = "ComboBox33"
        Me.ComboBox33.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox33.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox33.Size = New System.Drawing.Size(232, 20)
        Me.ComboBox33.TabIndex = 177
        '
        'Button8
        '
        Me.Button8.Enabled = False
        Me.Button8.Location = New System.Drawing.Point(857, 138)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(119, 23)
        Me.Button8.TabIndex = 176
        Me.Button8.Text = "Logo.."
        Me.Button8.Visible = False
        '
        'ComboBox34
        '
        Me.ComboBox34.Location = New System.Drawing.Point(695, 107)
        Me.ComboBox34.Name = "ComboBox34"
        Me.ComboBox34.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox34.Properties.Items.AddRange(New Object() {"Tienda que vende", "Almacén o Bodega", "Almacén Virtual o Tránsito"})
        Me.ComboBox34.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox34.Size = New System.Drawing.Size(156, 20)
        Me.ComboBox34.TabIndex = 175
        '
        'ComboBox32
        '
        Me.ComboBox32.Location = New System.Drawing.Point(312, 108)
        Me.ComboBox32.Name = "ComboBox32"
        Me.ComboBox32.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox32.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox32.Size = New System.Drawing.Size(374, 20)
        Me.ComboBox32.TabIndex = 174
        '
        'ComboBox20
        '
        Me.ComboBox20.Location = New System.Drawing.Point(565, 72)
        Me.ComboBox20.Name = "ComboBox20"
        Me.ComboBox20.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox20.Size = New System.Drawing.Size(121, 20)
        Me.ComboBox20.TabIndex = 170
        '
        'ComboBox19
        '
        Me.ComboBox19.Location = New System.Drawing.Point(352, 72)
        Me.ComboBox19.Name = "ComboBox19"
        Me.ComboBox19.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox19.Size = New System.Drawing.Size(207, 20)
        Me.ComboBox19.TabIndex = 169
        '
        'ComboBox18
        '
        Me.ComboBox18.Location = New System.Drawing.Point(204, 72)
        Me.ComboBox18.Name = "ComboBox18"
        Me.ComboBox18.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox18.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox18.Size = New System.Drawing.Size(142, 20)
        Me.ComboBox18.TabIndex = 168
        '
        'ComboBox17
        '
        Me.ComboBox17.Location = New System.Drawing.Point(15, 72)
        Me.ComboBox17.Name = "ComboBox17"
        Me.ComboBox17.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox17.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox17.Size = New System.Drawing.Size(183, 20)
        Me.ComboBox17.TabIndex = 167
        '
        'TextBox32
        '
        Me.TextBox32.Location = New System.Drawing.Point(695, 37)
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox32.Size = New System.Drawing.Size(156, 20)
        Me.TextBox32.TabIndex = 166
        '
        'TextBox27
        '
        Me.TextBox27.Location = New System.Drawing.Point(312, 38)
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox27.Size = New System.Drawing.Size(374, 20)
        Me.TextBox27.TabIndex = 165
        '
        'TextBox28
        '
        Me.TextBox28.Location = New System.Drawing.Point(15, 38)
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox28.Size = New System.Drawing.Size(291, 20)
        Me.TextBox28.TabIndex = 164
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(12, 58)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(40, 13)
        Me.Label46.TabIndex = 163
        Me.Label46.Text = "Estado"
        '
        'textbox22
        '
        Me.textbox22.Location = New System.Drawing.Point(160, 108)
        Me.textbox22.Name = "textbox22"
        Me.textbox22.Properties.Mask.EditMask = "f0"
        Me.textbox22.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textbox22.Size = New System.Drawing.Size(146, 20)
        Me.textbox22.TabIndex = 173
        '
        'textbox23
        '
        Me.textbox23.Location = New System.Drawing.Point(15, 108)
        Me.textbox23.Name = "textbox23"
        Me.textbox23.Properties.Mask.EditMask = "f0"
        Me.textbox23.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textbox23.Size = New System.Drawing.Size(139, 20)
        Me.textbox23.TabIndex = 172
        '
        'textbox59
        '
        Me.textbox59.Location = New System.Drawing.Point(695, 72)
        Me.textbox59.Name = "textbox59"
        Me.textbox59.Properties.Mask.EditMask = "P2"
        Me.textbox59.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textbox59.Size = New System.Drawing.Size(156, 20)
        Me.textbox59.TabIndex = 171
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(692, 56)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(94, 13)
        Me.Label78.TabIndex = 162
        Me.Label78.Text = "Porcentaje de IVA"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(692, 21)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(96, 13)
        Me.Label33.TabIndex = 158
        Me.Label33.Text = "Número Franquicia"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(605, 141)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(31, 13)
        Me.Label35.TabIndex = 157
        Me.Label35.Text = "Zona"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(12, 141)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(150, 13)
        Me.Label36.TabIndex = 155
        Me.Label36.Text = "Almacén Tránsito Relacionado"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(692, 91)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(27, 13)
        Me.Label37.TabIndex = 154
        Me.Label37.Text = "Tipo"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(309, 93)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(67, 13)
        Me.Label38.TabIndex = 153
        Me.Label38.Text = "Razon Social"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(157, 93)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(25, 13)
        Me.Label39.TabIndex = 152
        Me.Label39.Text = "Fax"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(12, 93)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(49, 13)
        Me.Label40.TabIndex = 151
        Me.Label40.Text = "Telefono"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(562, 58)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(72, 13)
        Me.Label41.TabIndex = 148
        Me.Label41.Text = "Código Postal"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(349, 58)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(42, 13)
        Me.Label42.TabIndex = 147
        Me.Label42.Text = "Colonia"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(201, 58)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(40, 13)
        Me.Label43.TabIndex = 144
        Me.Label43.Text = "Ciudad"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(313, 23)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(78, 13)
        Me.Label45.TabIndex = 139
        Me.Label45.Text = "Calle y número"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Enabled = False
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(857, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(119, 112)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 135
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(12, 23)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(105, 13)
        Me.Label48.TabIndex = 133
        Me.Label48.Text = "Nombre de la Tienda"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GroupControl9)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(866, 612)
        Me.XtraTabPage2.Text = "Metas"
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.DataGridView1)
        Me.GroupControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl9.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(866, 612)
        Me.GroupControl9.TabIndex = 171
        Me.GroupControl9.Text = "Metas de Ventas"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.MenuText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.EnableHeadersVisualStyles = False
        Me.DataGridView1.Location = New System.Drawing.Point(2, 21)
        Me.DataGridView1.Name = "DataGridView1"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.MenuText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Size = New System.Drawing.Size(862, 589)
        Me.DataGridView1.TabIndex = 0
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.DataGridView2)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(866, 612)
        Me.XtraTabPage3.Text = "Rentabilidad"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToOrderColumns = True
        Me.DataGridView2.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView2.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.EnableHeadersVisualStyles = False
        Me.DataGridView2.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView2.Name = "DataGridView2"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!)
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.MenuText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView2.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView2.Size = New System.Drawing.Size(866, 612)
        Me.DataGridView2.TabIndex = 2
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Tiendas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 640)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Tiendas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tiendas"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.ComboBox15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.ChkAlmacenInventario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.seRetardo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seComida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seTolera.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.seTolComida.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbAlmacenInv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox33.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox34.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox32.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox32.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox27.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox28.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textbox22.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textbox23.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textbox59.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Button5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBox16 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox33 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Button8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBox34 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox32 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox20 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox19 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox18 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox17 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextBox32 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox27 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox28 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents textbox22 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textbox23 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents textbox59 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Button13 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBox15 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbAlmacenInv As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents seTolera As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents seComida As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents seTolComida As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents seRetardo As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ChkAlmacenInventario As DevExpress.XtraEditors.CheckEdit
End Class
