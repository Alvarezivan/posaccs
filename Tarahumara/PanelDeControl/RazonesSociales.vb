﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraPrinting

Public Class RazonesSociales
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '   oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '   ";password=" & oConfig.pClave
    Dim hayfoto As Boolean = False
    Dim bEsNuevo As Boolean = True

    Private Sub Empleados_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = False
    End Sub

    Private Sub Clientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        ' DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        'LlenaCombo(ComboBox6, "Select id,Nombre from empleado order by nombre", "Nombre", "id", conexion)

        LlenaCombobox(ComboBox7, "Select id,Nombre from razones order by nombre", "Nombre", "id", conexion)
        LlenaCombobox(ComboBox1, "Select distinct estado, codestado from sepomex order by estado", "Estado", "codestado", conexion)
        'Me.Cursor = Cursors.WaitCursor
        llenagrid()

        RegistraAcceso(conexion, "Utilerias, Razones Sociales [RazonesSoaciales.frm]")
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        LlenaCombobox(ComboBox2, "Select distinct municipio,codestado from sepomex where estado='" & ComboBox1.Text & "' order by municipio", "municipio", "codestado", conexion)
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        LlenaCombobox(ComboBox3, "Select distinct colonia,codestado from sepomex where estado='" & ComboBox1.Text & "' and municipio='" & ComboBox2.Text & "' order by colonia", "colonia", "codestado", conexion)
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        LlenaCombobox(ComboBox4, "Select distinct cp,id from sepomex where estado='" & ComboBox1.Text & _
                   "' and municipio='" & ComboBox2.Text & "' and colonia='" & ComboBox3.Text & "' order by cp", "cp", "id", conexion)
    End Sub

    Private Sub llenagrid()
        Dim qry As String
        qry = "SELECT        razones.id, razones.Nombre, razones.Rfc, razones.Calle, razones.Telefono, " & _
       " razones.Telefono2, razones.Contacto, razones.FechaIngreso, razones.COLONIA, razones.CIUDAD, razones.ESTADO, razones.cp " & _
" FROM            razones LEFT JOIN " & _
                         " sepomex ON razones.idColonia = sepomex.id " & _
        " WHERE(razones.Borrado = 0) " & _
" ORDER BY razones.Nombre"
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl1.DataSource = dr2.Tables(0)
        GridView1.PopulateColumns()
        GridView1.BestFitColumns()
    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Not bEsNuevo Then
            Button3.PerformClick()
            Exit Sub
        End If

        If Validation.FormIsValid(Me) Then
            If ValidaCombos() Then
                'hayfoto = False
                Dim qry As String = "Select Nombre from razones where nombre='" & TextBox2.Text.Trim & "' and borrado=0 "
                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(691, Mensaje.Texto), IdiomaMensajes(691, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        Exit Sub
                    Else
                        Cursor.Current = Cursors.WaitCursor
                        Dim sQuery As String = "INSERT INTO razones (RFC, NOMBRE, CALLE, COLONIA, CIUDAD, ESTADO, TELEFONO, FAX, CP, CONTACTO, IDCOLONIA, FECHAINGRESO, Borrado, llave) " & _
                                               "VALUES (@RFC, @NOMBRE, @CALLE, @COLONIA, @CIUDAD, @ESTADO, @TELEFONO, @FAX, @CP, @CONTACTO, @IDCOLONIA, GetDate(), 0, CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                        Dim cmd As New SqlCommand
                        cmd.CommandText = sQuery
                        cmd.Parameters.Add(New SqlParameter("RFC", SqlDbType.Char)).Value = TextBox6.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("NOMBRE", SqlDbType.VarChar)).Value = TextBox2.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("CALLE", SqlDbType.VarChar)).Value = TextBox5.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("COLONIA", SqlDbType.VarChar)).Value = ComboBox3.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("CIUDAD", SqlDbType.VarChar)).Value = ComboBox2.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("ESTADO", SqlDbType.VarChar)).Value = ComboBox1.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("TELEFONO", SqlDbType.VarChar)).Value = Textbox1.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("FAX", SqlDbType.Char)).Value = textbox7.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("CP", SqlDbType.Char)).Value = ComboBox4.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("CONTACTO", SqlDbType.VarChar)).Value = TextBox14.Text.Trim.ToUpper
                        If ComboBox4.SelectedIndex <> -1 Then
                            cmd.Parameters.Add(New SqlParameter("IDCOLONIA", SqlDbType.Decimal)).Value = CType(ComboBox4.SelectedItem, cInfoCombo).ID
                        Else
                            cmd.Parameters.Add(New SqlParameter("IDCOLONIA", SqlDbType.Decimal)).Value = ComboBox4.Tag
                            ComboBox4.Tag = ""
                        End If


                        Dim regresa As String = bdBase.bdExecute(conexion, cmd)
                        If Not String.IsNullOrWhiteSpace(regresa) Then
                            MessageBox.Show(regresa, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                        llenagrid()
                        limpia()
                        Cursor.Current = Cursors.Default
                        'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(692, Mensaje.Texto), IdiomaMensajes(692, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                End If
            End If
        End If
    End Sub

#Region "Valida Combos"
    Private Function ValidaCombos() As Boolean
        Dim regresa As Boolean = False
        Dim oInfo As cInfoCombo
        oInfo = CType(Me.ComboBox1.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(220, Mensaje.Texto), IdiomaMensajes(220, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If
        oInfo = CType(Me.ComboBox2.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(222, Mensaje.Texto), IdiomaMensajes(222, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If
        If ComboBox3.SelectedIndex <> -1 Then
            oInfo = CType(Me.ComboBox3.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(ComboBox3.Text) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        End If

        If ComboBox4.SelectedIndex <> -1 Then
            oInfo = CType(Me.ComboBox4.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(ComboBox4.Text) Then
                If ComboBox4.Text.Trim.Length = 5 Then
                    regresa = GuardaElementoSepomex()
                    'regresa = True
                Else
                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(225, Mensaje.Texto), IdiomaMensajes(225, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Return False
                    Exit Function
                End If
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        End If

        Return regresa
    End Function

    Private Function GuardaElementoSepomex() As Boolean
        Dim regresar As Boolean = False
        If Not String.IsNullOrEmpty(ComboBox3.Text.Trim) And Not String.IsNullOrEmpty(ComboBox4.Text.Trim) Then
            Dim sQuery As String = "INSERT INTO sepomex (cp, colonia, municipio, ciudad, estado, codEstado, codigoPais, llave) VALUES ('" & _
                                           ComboBox4.Text.Trim & "', '" & ComboBox3.Text.Trim & "', '" & ComboBox2.Text.Trim & "', '" & ComboBox2.Text.Trim & "', '" & _
                                           ComboBox1.Text.Trim & "', '" & CType(ComboBox1.SelectedItem, cInfoCombo).ID & "', 'MX', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER)) SELECT @@IDENTITY AS id"
            Dim regresa As String = bdBase.bdExecute(conexion, sQuery, False, True)
            If Not String.IsNullOrEmpty(regresa) Then
                If IsNumeric(regresa) Then
                    ComboBox4.Tag = regresa
                    regresar = True
                Else
                    ComboBox4.Tag = "-1"
                End If
            End If
        End If
        Return regresar
    End Function
#End Region

    Private Sub TextBox6_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim regExp As New Regex("^([A-Z|a-z|&amp;]{3,4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)(\w{2})([A|a|0-9]{1})$|^([A-Z|a-z]{4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)((\w{2})([A|a|0-9]{1})){0,3}$")
        If regExp.IsMatch(TextBox6.Text) Then
            ErrorProvider1.SetError(TextBox6, "")
            Button1.Enabled = True
        Else
            ErrorProvider1.SetError(TextBox6, "RFC no válido")
            Button1.Enabled = False
            e.Cancel = True

        End If
    End Sub

    Private Sub TextBox2_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If TextBox2.Text.Trim.Length = 0 Then
            ErrorProvider1.SetError(TextBox2, "Debe Seleccionar Nombre")
            Button1.Enabled = False
            e.Cancel = True
        Else
            ErrorProvider1.SetError(TextBox2, "")
            Button1.Enabled = True
        End If
    End Sub

    Private Sub TextBox5_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If TextBox5.Text.Trim.Length = 0 Then
            ErrorProvider1.SetError(TextBox5, "Debe Escribir Calle y Número")
            Button1.Enabled = False
            e.Cancel = True
        Else
            ErrorProvider1.SetError(TextBox5, "")
            Button1.Enabled = True
        End If
    End Sub

    Private Sub FillCombo(ByVal elemento As String, ByVal id As String, ByVal combo As ComboBox, ByVal tabla As String)
        If id = "" Or id = "-1" Or id = "-1.00" Then
        Else

            Dim qry As String
            qry = "SELECT      " & elemento & _
               " FROM   " & tabla & _
               " WHERE     (id ='" & id & "')"
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            dr2.Read()
            Dim contador As Integer = 0
            For contador = 0 To combo.Items.Count - 1
                If combo.Items.Item(contador).ToString.Trim = dr2(elemento).ToString.Trim Then
                    combo.SelectedIndex = contador
                    Exit For
                End If
            Next
        End If
    End Sub

    'Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Cursor.Current = Cursors.WaitCursor
        Button3.Enabled = True
        Dim qry As String = "Select * from razones where id='" & CType(ComboBox7.SelectedItem, cInfoCombo).ID & "'  "
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dr.Read()
        bEsNuevo = False
        If Not IsDBNull(dr("nombre")) Then TextBox2.Text = dr("nombre").Trim.ToUpper
        If Not IsDBNull(dr("calle")) Then TextBox5.Text = dr("calle").Trim.ToUpper
        If Not IsDBNull(dr("telefono")) Then Textbox1.Text = dr("telefono").Trim.ToUpper
        If Not IsDBNull(dr("fax")) Then textbox7.Text = dr("fax").Trim.ToUpper
        If Not IsDBNull(dr("rfc")) Then TextBox6.Text = dr("rfc").Trim.ToUpper
        If Not IsDBNull(dr("Contacto")) Then TextBox14.Text = dr("Contacto").Trim.ToUpper

        ' Buscar datos sepomex
        If Not IsDBNull(dr("idcolonia")) Then
            If dr("idcolonia") <> -1 Then
                qry = "SELECT cp, colonia, municipio, estado FROM sepomex WHERE id = " & dr("idcolonia")
                Dim drCp As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not drCp Is Nothing Then
                    If drCp.HasRows Then
                        drCp.Read()
                        BuscaEnComboDx(ComboBox1, drCp("Estado").ToString.Trim)
                        BuscaEnComboDx(ComboBox2, drCp("municipio").ToString.Trim)
                        BuscaEnComboDx(ComboBox3, drCp("colonia").ToString.Trim)
                        BuscaEnComboDx(ComboBox4, drCp("cp").ToString.Trim)
                    End If
                    drCp.Close()
                End If
            Else
                BuscaEnComboDx(ComboBox1, dr("Estado").ToString.Trim)
                BuscaEnComboDx(ComboBox2, dr("Ciudad").ToString.Trim)
                ComboBox3.Text = dr("Colonia")
                ComboBox4.Text = dr("cp")
            End If
        Else
            ComboBox1.SelectedIndex = -1
            ComboBox2.SelectedIndex = -1
            ComboBox3.SelectedIndex = -1
            ComboBox4.SelectedIndex = -1
        End If

        Button3.Visible = True
        Cursor.Current = Cursors.Default
    End Sub

    'Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If bEsNuevo Then
            Button1.PerformClick()
            Exit Sub
        End If

        Cursor.Current = Cursors.WaitCursor
        If Validation.FormIsValid(Me) Then
            If ValidaCombos() Then
                Dim qry As String

        

                Dim _rfc, _nombre, _calle, _telefono, _fax, _cp, _contacto As String
                Dim _colonia, _ciudad, _estado As String
                Dim _idcolonia, _id As Int64

                _id = CType(ComboBox7.SelectedItem, cInfoCombo).ID
                _rfc = TextBox6.Text.Trim.ToUpper
                _nombre = (TextBox2.Text.Trim.ToUpper)
                _calle = TextBox5.Text.Trim.ToUpper
                _colonia = ComboBox3.Text.Trim
                _ciudad = ComboBox2.Text.Trim
                _estado = ComboBox1.Text.Trim
                _telefono = Textbox1.Text.Trim.ToUpper
                _fax = textbox7.Text.Trim.ToUpper
                _cp = ComboBox4.Text.Trim
                _contacto = TextBox14.Text.Trim.ToUpper
                If ComboBox4.SelectedIndex <> -1 Then
                    _idcolonia = CType(ComboBox4.SelectedItem, cInfoCombo).ID
                Else
                    _idcolonia = ComboBox4.Tag
                    ComboBox4.Tag = ""
                End If
                qry = "update [Razones] set [RFC]='" & _rfc & "', [NOMBRE]='" & _nombre & "', [Calle]='" & _calle & "', [COLONIA]='" & _colonia & _
                         "', [CIUDAD]='" & _ciudad & "', [ESTADO]='" & _estado & "', [Telefono]='" & _telefono & "', " & _
                           "[FAX]='" & _fax & "', [CP]='" & _cp & "', [Contacto]='" & _contacto & "', [idColonia]='" & _idcolonia.ToString.Trim & "' where id='" & _id.ToString.Trim & "' "

                Dim regresa As String = bdBase.bdExecute(conexion, qry)
                If Not String.IsNullOrWhiteSpace(regresa) Then
                    'MessageBox.Show(regresa, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

                LlenaCombobox(ComboBox7, "Select distinct id,nombre from razones order by nombre", "nombre", "id", conexion)
                llenagrid()
                limpia()
                Button3.Visible = False
                'DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(693, Mensaje.Texto), IdiomaMensajes(693, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)

                Cursor.Current = Cursors.Default
            End If
        End If
        Button3.Enabled = False
    End Sub

    'Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub limpia()
        TextBox2.Text = ""
        TextBox5.Text = ""
        ComboBox1.SelectedIndex = -1
        ComboBox2.SelectedIndex = -1
        ComboBox3.SelectedIndex = -1
        ComboBox4.SelectedIndex = -1
        Textbox1.Text = ""
        textbox7.Text = ""
        TextBox6.Text = ""
        TextBox14.Text = ""
        bEsNuevo = True
    End Sub

    
    Private Sub ComboBox7_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox7.SelectedIndexChanged
        Button3.Enabled = False
    End Sub
End Class