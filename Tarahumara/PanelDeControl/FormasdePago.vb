﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions

Public Class FormasdePago

    '  Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    'oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '";password=" & oConfig.pClave

    Private Sub FormasdePago_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        IdiomaTextos(Me)
        LlenaCombo(ComboBox1, "Select Numero,Nombre from tiendas where tipo=1 and numero='" & My.Settings.Item("TiendaActual") & "'order by nombre", "Nombre", "Numero", conexion)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim flag As Boolean = False
        Dim oInfo As cInfoCombo
        oInfo = CType(ComboBox1.SelectedItem, cInfoCombo)

        Dim qry As String = "SELECT Numero,nombre as FormaPago,ISNULL(status,0) as status,ISNULL(pagare,0) as pagare, " & _
     " ISNULL(clave,0) as clave,ISNULL(vale,0) as vale,ISNULL(tipo,0) as tipo, ISNULL(ticket,0) as monex  " & _
     " FROM formaspago where tienda=" & oInfo.ID.ToString & "Order by NUMERO "
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        If ds.Tables(0).Rows.Count > 0 Then
            flag = True
            DataGridView1.DataSource = ds.Tables(0)
            With DataGridView1
                .AutoResizeRows()
            End With
        Else
            qry = "SELECT numero,nombre,ISNULL(status,0) as status,ISNULL(pagare,0) as pagare, " & _
   " ISNULL(clave,0) as clave,ISNULL(vale,0) as vale,ISNULL(tipo,0) as tipo, ISNULL(ticket,0) as monex  " & _
   " FROM formaspago where tienda=-1 Order by NUMERO "
            ds = bdBase.bdDataset(conexion, qry)
            DataGridView1.DataSource = ds.Tables(0)
            With DataGridView1
                .AutoResizeRows()
            End With
        End If

        Dim colCheck As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
        colCheck.Name = "Status"
        DataGridView1.Columns.Add(colCheck)
        colCheck.DisplayIndex = 2
        Dim colPagare As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
        colPagare.Name = "Pagare"
        DataGridView1.Columns.Add(colPagare)
        colPagare.DisplayIndex = 3
        Dim colTarjeta As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
        colTarjeta.Name = "TipoTarjeta"
        DataGridView1.Columns.Add(colTarjeta)
        colTarjeta.DisplayIndex = 4
        Dim colMonEx As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn()
        colMonEx.Name = "MonEx"
        DataGridView1.Columns.Add(colMonEx)
        colMonEx.DisplayIndex = 5
        With DataGridView1
            .AutoResizeRows()
            .Columns(0).ReadOnly = True
            .Columns(0).Visible = False
            .Columns(5).Visible = False
            .Columns(4).Visible = True
            .Columns(4).ReadOnly = True
            .Columns(2).Visible = False
            .Columns(3).Visible = False
            .Columns(6).Visible = False
            .Columns(7).Visible = False
        End With
        DataGridView1.Item(1, 0).ReadOnly = True
        DataGridView1.Item(1, 1).ReadOnly = True
        DataGridView1.Item(1, 2).ReadOnly = True
        DataGridView1.Item(1, 3).ReadOnly = True
        DataGridView1.Item(1, 4).ReadOnly = True
        DataGridView1.Item(1, 5).ReadOnly = True
        DataGridView1.Item(1, 6).ReadOnly = True
        DataGridView1.Item(1, 7).ReadOnly = True
        DataGridView1.Item(1, 8).ReadOnly = True
        DataGridView1.Item(1, 9).ReadOnly = True
        DataGridView1.Item(1, 10).ReadOnly = True

        If flag Then
            Dim x As Integer
            For x = 0 To DataGridView1.Rows.Count - 1
                If DataGridView1.Item(2, x).Value = 1 Then
                    DataGridView1.Item(8, x).Value = True
                Else
                    DataGridView1.Item(8, x).Value = False
                End If
                If DataGridView1.Item(3, x).Value = 1 Then
                    DataGridView1.Item(9, x).Value = True
                Else
                    DataGridView1.Item(9, x).Value = False
                End If
                If DataGridView1.Item(6, x).Value = 1 Then
                    DataGridView1.Item(10, x).Value = True
                Else
                    DataGridView1.Item(10, x).Value = False
                End If
                If DataGridView1.Item(7, x).Value = 1 Then
                    DataGridView1.Item(11, x).Value = True
                Else
                    DataGridView1.Item(11, x).Value = False
                End If
            Next
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim qry As String
        qry = "Delete from formaspago where tienda='" & My.Settings("TiendaActual") & "'"
        bdBase.bdExecute(conexion, qry)
        If Not String.IsNullOrEmpty(conexionLocal) Then bdBase.bdExecute(conexionLocal, qry)
        Dim x, tipo, pagare, status, monex As Integer
        For x = 0 To DataGridView1.Rows.Count - 1
            If DataGridView1.Item(1, x).Value = Nothing Then
            Else
                If DataGridView1.Item(11, x).Value = Nothing Then
                    monex = 0
                Else
                    monex = 1
                End If
                If DataGridView1.Item(10, x).Value = Nothing Then
                    tipo = 0
                Else
                    tipo = 1
                End If
                If DataGridView1.Item(9, x).Value = Nothing Then
                    pagare = 0
                Else
                    pagare = 1
                End If
                If DataGridView1.Item(8, x).Value = Nothing Then
                    status = 0
                Else
                    status = 1
                End If
                qry = "   INSERT INTO [formaspago] ([NUMERO],[NOMBRE]  ,[TIPO] ,[PAGARE] ,[TIENDA] ,[CLAVE] ,[STATUS],[TICKET], llave) values ('" & _
                DataGridView1.Item(0, x).Value.ToString.Trim & "','" & _
                DataGridView1.Item(1, x).Value.ToString.Trim & "','" & _
                tipo.ToString & "','" & _
                pagare.ToString & "','" & _
                My.Settings("TiendaActual") & "','" & _
                DataGridView1.Item(4, x).Value.ToString.Trim & "','" & status.ToString & "','" + monex.ToString + "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
                bdBase.bdExecute(conexion, qry)
            End If
        Next
        MessageBox.Show(IdiomaMensajes(663, Mensaje.Texto))
        Me.Hide()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim qry As String
        qry = "Delete from formaspago where tienda='" & My.Settings("TiendaActual") & "'"
        bdBase.bdExecute(conexion, qry)
        Me.Hide()

    End Sub
End Class