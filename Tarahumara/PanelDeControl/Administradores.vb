﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DPFP

Public Class Administradores
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '                         oConfig.pBase & ";Persist Security Info=True;User ID=" & oConfig.pUsuario & _
    '                         ";password=" & oConfig.pClave

    Public Verificado As Boolean = False
    Public idSuper As Integer = 0

    Private WithEvents enrollControl As DPFP.Gui.Enrollment.EnrollmentControl
    Private WithEvents verifyControl As DPFP.Gui.Verification.VerificationControl
    Private matcher As DPFP.Verification.Verification
    Private matchResult As DPFP.Verification.Verification.Result
    Private allReaderSerial As String = "00000000-0000-0000-0000-000000000000"

    Public template As New DPFP.Template

    Private Sub Administradores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        ' DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)

        'Dim qry As String = "SELECT Numero, Nombre, PASSWORD FROM  empleado where tipo='1' ORDER BY NOMBRE"
        'LlenaCombo(ComboBox1, qry, "Nombre", "Numero", conexion)

        If My.Settings.usarLector Then
            LabelControl2.Visible = False
            LabelControl3.Visible = False
            txUsuario.Visible = False
            txClave.Visible = False
            btAceptar.Visible = False

            matcher = New Verification.Verification()
            matchResult = New Verification.Verification.Result

            If enrollControl IsNot Nothing Then
                enrollControl.Dispose()
                enrollControl = Nothing
            End If
            If verifyControl Is Nothing Then
                CreateDPControl(verifyControl)
            End If

        Else
            LabelControl1.Text = "Usuario SUPERVISOR, por favor identifíquese"
            LabelControl2.Visible = True
            LabelControl3.Visible = True
            txUsuario.Visible = True
            txClave.Visible = True
            btAceptar.Visible = True
        End If
    End Sub

    Private Sub CreateDPControl(ByRef control As DPFP.Gui.Verification.VerificationControl)
        Try
            control = New DPFP.Gui.Verification.VerificationControl()
            control.AutoSizeMode = Windows.Forms.AutoSizeMode.GrowAndShrink
            control.Name = "verifyControl"
            control.Location = New System.Drawing.Point(110, 55)
            control.ReaderSerialNumber = allReaderSerial
            control.Visible = True
            control.Enabled = True

            Me.Controls.Add(control)
        Catch ex As Exception
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(148, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    Private Sub verifyControl_OnComplete(ByVal Control As Object, ByVal FeatureSet As DPFP.FeatureSet, ByRef EventHandlerStatus As DPFP.Gui.EventHandlerStatus) Handles verifyControl.OnComplete
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Dim sQry As String = "SELECT numero, nombre, huella FROM empleado WHERE tipo = 3 ORDER BY NOMBRE"
            Dim listo As Boolean = False
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexionLocal, sQry)
            If Not dr Is Nothing Then
                Do While dr.Read
                    If listo Then Exit Do
                    Dim huella As Byte() = Nothing
                    huella = dr("huella")
                    idSuper = dr("numero")
                    template.DeSerialize(huella)
                    matcher.Verify(FeatureSet, template, matchResult)
                    If matchResult.Verified Then
                        EventHandlerStatus = Gui.EventHandlerStatus.Success
                        'MessageBox.Show("verificado")
                        listo = True
                        Windows.Forms.Cursor.Current = Cursors.Default
                        Me.Close()
                    Else
                        EventHandlerStatus = Gui.EventHandlerStatus.Failure
                        'MessageBox.Show("no verificado")
                        lblInfo.Visible = True
                    End If
                Loop
            Else
                EventHandlerStatus = Gui.EventHandlerStatus.Failure
            End If
            Verificado = listo
        Catch ex As Exception
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(148, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        End Try
    End Sub

    Private Sub btCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        Me.Close()
    End Sub

    Private Sub btAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btAceptar.Click
        Verificado = False
        If Not String.IsNullOrWhiteSpace(txUsuario.Text.Trim) Then
            If Not String.IsNullOrWhiteSpace(txClave.Text.Trim) Then
                If txUsuario.Text.Trim.ToUpper = "SAZ" Then
                    Dim des As New cTripleDES
                    Dim drS As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), "SELECT admin, rptUser, rptClave FROM configuracion")
                    If Not drS Is Nothing Then
                        If drS.HasRows Then
                            drS.Read()
                            If txClave.Text.Trim.ToUpper = drS("admin").ToString.ToUpper Then
                                Verificado = True
                                Me.Close()
                            Else
                                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(666, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        Else
                            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(667, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                        drS.Close()
                    Else
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(668, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else
                    Dim sQuery As String = "SELECT Numero FROM empleado WHERE UPPER(LTRIM(RTRIM([User]))) = '" & txUsuario.Text.Trim.ToUpper & _
                                                           "' AND UPPER(LTRIM(RTRIM([PASSWORD]))) = '" & txClave.Text.Trim.ToUpper & "' AND tipo='3'"
                    Dim dr As SqlDataReader = bdBase.bdDataReader(conexionLocal, sQuery)
                    If Not dr Is Nothing Then
                        dr.Read()
                        Verificado = dr.HasRows
                        idSuper = dr("Numero")
                        dr.Close()
                        Me.Close()
                    End If
                End If
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(149, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(150, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class