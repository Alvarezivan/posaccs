﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.IO
Imports Microsoft.Office.Interop
Imports DevExpress.XtraEditors.Repository
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Controls
Imports DevExpress.XtraGrid.GridControl
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel


Public Class Empleados
    Dim hayfoto As Boolean = False
    Dim bEsNuevo As Boolean = True
    Dim prevPass As String = ""

#Region "Inicializaciones"
    Private Sub Empleados_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = False
    End Sub

    Private Sub Clientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        deNacimiento.Text = DateAdd(DateInterval.Year, -12, Now.Date)
        deIngreso.Text = Now.Date
        deVDel.DateTime = Now.Date
        deVAl.DateTime = DateAdd(DateInterval.Day, 5, Now.Date)
        dePDel.DateTime = DateValue("01/" & Now.Month & "/" & Now.Year)
        dePAl.DateTime = Now.Date
        deDelP.DateTime = DateValue("01/" & Now.Month & "/" & Now.Year)
        deAlP.DateTime = Now.Date
        Try
            LlenaCombobox(cbCobranza, "Select id,Nombre from zonacobranza where borrado=0 or borrado is null order by nombre", "Nombre", "id", conexion)
            LlenaCombobox(cbRuta, "Select id,Nombre from zonaventas where borrado=0 or borrado is null order by nombre", "Nombre", "id", conexion)

            LlenaCombobox(ComboBox7, "Select numero,ltrim(rtrim(Nombre)) as Nombre from empleado where borrado=0 order by nombre", "Nombre", "numero", conexion)
            LlenaCombobox(cbEstado, "Select distinct estado,codestado from sepomex order by estado", "Estado", "codestado", conexion)
            LlenaCombobox(cbTienda, "SELECT NUMERO, Nombre from tiendas ORDER BY Nombre", "Nombre", "Numero", conexion)
            LlenaCombobox(cbTiendasP, "SELECT NUMERO, Nombre from tiendas ORDER BY Nombre", "Nombre", "Numero", conexion)
            LlenaCombobox(cbTiendaPP, "SELECT NUMERO, Nombre from tiendas ORDER BY Nombre", "Nombre", "Numero", conexion)

            BuscaEnComboDx(cbTienda, My.Settings.TiendaActual, True)
        Catch ex As Exception
            DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message)
        End Try
        cbDescanso.SelectedIndex = 0

        ' Inicializar departamentos por default si fuera necesario
        Dim sQuery As String = "SELECT id FROM deptoPuesto"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        If Not dr Is Nothing Then
            If Not dr.HasRows Then
                sQuery = "SET IDENTITY_INSERT deptoPuesto ON " & vbCrLf
                sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(0 AS Numeric(18, 0)), N'Cajero') " & vbCrLf
                sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(1 AS Numeric(18, 0)), N'Vendedor') " & vbCrLf
                sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(2 AS Numeric(18, 0)), N'Usuario/Admin') " & vbCrLf
                sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(3 AS Numeric(18, 0)), N'Supervisor') " & vbCrLf
                sQuery += "INSERT deptoPuesto ([id], [Departamento]) VALUES (CAST(4 AS Numeric(18, 0)), N'Comprador') " & vbCrLf
                sQuery += "SET IDENTITY_INSERT deptoPuesto OFF "
                bdBase.bdExecute(conexion, sQuery)
            End If
            dr.Close()
        End If

        LlenaCombobox(cbDepartamento, "SELECT id, Departamento FROM deptoPuesto ORDER BY Departamento", "Departamento", "id", conexion)

        RegistraAcceso(conexion, "Utilerías, Control de usuarios [Empleados.frm]")


    End Sub
#End Region

#Region "ABC"
    Private Sub cbEstado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbEstado.SelectedIndexChanged
        LlenaCombobox(cbCiudad, "Select distinct municipio,codestado from sepomex where estado='" & cbEstado.Text & "' order by municipio", "municipio", "codestado", conexion)
    End Sub

    Private Sub cbCiudad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbCiudad.SelectedIndexChanged
        LlenaCombobox(cbColonia, "Select distinct colonia,codestado from sepomex where estado='" & cbEstado.Text & "' and municipio='" & cbCiudad.Text & "' order by colonia", "colonia", "codestado", conexion)
    End Sub

    Private Sub cbColonia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbColonia.SelectedIndexChanged
        LlenaCombobox(cbPC, "Select distinct cp,id from sepomex where estado='" & cbEstado.Text & _
                   "' and municipio='" & cbCiudad.Text & "' and colonia='" & cbColonia.Text & "' order by cp", "cp", "id", conexion)
    End Sub

    Private Sub btGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btGuardar.Click
        If Not bEsNuevo Then
            btModificar.PerformClick()
            Exit Sub
        End If
        Splash(True)
        Dim usr, pass As String
        Dim tipu As Integer
        If Validation.FormIsValid(Me) Then
            If ValidaCombos() Then

                Dim oInfo As cInfoCombo
                oInfo = CType(cbRuta.SelectedItem, cInfoCombo)
                'hayfoto = False

                Dim sComp1 As String = String.Empty
                Dim sComp2 As String = String.Empty
                If cbDepartamento.SelectedIndex <> -1 Then
                    Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                        Case 0, 2, 3, 4
                            sComp1 = "eMail = '" & txeMail.Text.Trim & "'"
                            sComp2 = txeMail.Text.Trim
                        Case 1
                            sComp1 = "[user] = '" & txUsuario.Text.Trim & "'"
                            sComp2 = txUsuario.Text.Trim
                        Case Else
                            sComp1 = "eMail = '" & txeMail.Text.Trim & "'"
                            sComp2 = txeMail.Text.Trim
                    End Select
                Else
                    sComp1 = "eMail = '" & txeMail.Text.Trim & "'"
                    sComp2 = txeMail.Text.Trim
                End If
                Dim qry As String = "SELECT Nombre FROM empleado WHERE " & sComp1
                Dim drM As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not IsNothing(drM) Then
                    If drM.HasRows Then
                        Dim msg As String = IdiomaMensajes(813, Mensaje.Texto)
                        msg = String.Format(msg, sComp2)
                        DevExpress.XtraEditors.XtraMessageBox.Show(msg, IdiomaMensajes(813, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        txeMail.Focus()
                        Exit Sub
                    End If
                    drM.Close()
                End If

                qry = "Select Nombre from empleado where nombre='" & txNombre.Text.Trim & "' and borrado=0 "
                Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                If Not dr Is Nothing Then
                    If dr.HasRows Then
                        Dim msg As String = IdiomaMensajes(814, Mensaje.Texto)
                        msg = String.Format(msg, txNombre.Text.Trim)
                        DevExpress.XtraEditors.XtraMessageBox.Show(msg, IdiomaMensajes(814, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        Exit Sub
                    Else
                        Cursor.Current = Cursors.WaitCursor
                        If ckActivo.Checked = False Then
                            If txUsuario.Text.Trim.Length = 0 Then
                                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(649, Mensaje.Texto), IdiomaMensajes(649, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)

                                Exit Sub
                            End If
                            If txClave.Text.Trim.Length = 0 Then
                                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(656, Mensaje.Texto), IdiomaMensajes(656, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                                Exit Sub
                            End If
                        Else
                            usr = "-1"
                            pass = "-1"
                        End If
                        Dim z, zc, cel, ti, dep As Integer
                        pass = txClave.Text.Trim

                        If cbDepartamento.SelectedIndex <> -1 Then
                            Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                                Case 0, 2, 3, 4
                                    usr = txeMail.Text.Trim
                                Case 1
                                    usr = txUsuario.Text.Trim
                                Case Else
                                    usr = txeMail.Text.Trim
                                    'Case 0
                                    '    usr = txeMail.Text.Trim
                                    '    'tipu = 3
                                    'Case 1
                                    '    usr = txUsuario.Text.Trim
                                    '    'tipu = 4
                                    'Case 2
                                    '    usr = txeMail.Text.Trim
                                    '    'tipu = 1
                                    'Case 3
                                    '    usr = txeMail.Text.Trim
                                    '    'tipu = 2
                                    'Case 4
                                    '    usr = txeMail.Text.Trim
                                    '    'tipu = 5
                                    'Case Else
                                    '    usr = txUsuario.Text.Trim
                                    '    'tipu = 4
                            End Select
                            tipu = CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                        Else
                            usr = txeMail.Text.Trim
                            tipu = cbDepartamento.Tag
                            cbDepartamento.Tag = ""
                            LlenaCombobox(cbDepartamento, "SELECT id, Departamento FROM deptoPuesto ORDER BY Departamento", "Departamento", "id", conexion)
                        End If


                        If cbRuta.SelectedIndex = -1 Then
                            z = -1
                        Else
                            z = CType(cbRuta.SelectedItem, cInfoCombo).ID
                        End If

                        If cbCobranza.SelectedIndex = -1 Then
                            zc = -1
                        Else
                            zc = CType(cbCobranza.SelectedItem, cInfoCombo).ID
                        End If
                        Dim oInfoTienda As cInfoCombo = CType(cbTienda.SelectedItem, cInfoCombo)
                        Dim sQuery As String = "INSERT INTO empleado ([NOMBRE], [Calle], [idColonia], [Telefono], [Telefono2], [FechaNacimiento], " & _
                                               "[FechaIngreso], [RFC], [CURP], [eMail], [IMSS], [CuentaNomina], [CuentaClabe], [Sueldo], [Puesto], [tipo], " & _
                                               "[User], [PASSWORD], [Zona], [ZonaCobranza], [ComisionVenta], [ComisionVenta2], [Tienda], [Foto], [DiaDescanso], " & _
                                               "Borrado, Estado, Ciudad, Colonia, Cp, Status, Fijo, Turno, ClaveInfonavit, compensacionDiaria, Bono, llave, " & _
                                               "HoraEnt, HoraSal) VALUES " & _
                                               "(@NOMBRE, @Calle, @idColonia, @Telefono, @Telefono2, @FechaNacimiento, @FechaIngreso, @RFC, @CURP, " & _
                                               "@eMail, @IMSS, @CuentaNomina, @CuentaClabe, @Sueldo, @Puesto, @Tipo, @Usuario, @PASSWORD, @Zona, @ZonaCobranza, " & _
                                               "@ComisionVenta, @ComisionVenta2, @Tienda, @Foto, @DiaDescanso, 0, @Estado, @Ciudad, @Colonia, @Cp, " & _
                                               "@Status, @Fijo, @Turno, @ClaveInfonavit, @compensacionDiaria, @Bono, " & _
                                               "CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER), @HoraEnt, @HoraSal) SELECT @@IDENTITY AS id"
                        Dim cmd As New SqlCommand
                        cmd.CommandText = sQuery
                        cmd.Parameters.Add(New SqlParameter("@NOMBRE", SqlDbType.VarChar)).Value = txNombre.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@Calle", SqlDbType.VarChar)).Value = txCalle.Text.Trim.ToUpper

                        If cbPC.SelectedIndex <> -1 Then
                            cmd.Parameters.Add(New SqlParameter("@idColonia", SqlDbType.BigInt)).Value = CType(cbPC.SelectedItem, cInfoCombo).ID
                        Else
                            cmd.Parameters.Add(New SqlParameter("@idColonia", SqlDbType.BigInt)).Value = cbPC.Tag
                            cbPC.Tag = ""
                        End If
                        cmd.Parameters.Add(New SqlParameter("@Telefono", SqlDbType.VarChar)).Value = txTel1.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@Telefono2", SqlDbType.VarChar)).Value = txTel2.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@FechaNacimiento", SqlDbType.DateTime)).Value = deNacimiento.DateTime
                        cmd.Parameters.Add(New SqlParameter("@FechaIngreso", SqlDbType.DateTime)).Value = deIngreso.DateTime
                        cmd.Parameters.Add(New SqlParameter("@RFC", SqlDbType.VarChar)).Value = txRfc.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@CURP", SqlDbType.VarChar)).Value = txCurp.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@IMSS", SqlDbType.VarChar)).Value = txImss.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@CuentaNomina", SqlDbType.VarChar)).Value = txNomina.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@CuentaClabe", SqlDbType.VarChar)).Value = txClabe.Text.Trim.ToUpper
                        cmd.Parameters.Add(New SqlParameter("@Sueldo", SqlDbType.Decimal)).Value = IIf(String.IsNullOrWhiteSpace(txSueldo.Text.Trim.ToUpper), 0, txSueldo.Text.Trim.ToUpper)
                        cmd.Parameters.Add(New SqlParameter("@Puesto", SqlDbType.Decimal)).Value = tipu
                        cmd.Parameters.Add(New SqlParameter("@Tipo", SqlDbType.Decimal)).Value = tipu
                        cmd.Parameters.Add(New SqlParameter("@Usuario", SqlDbType.VarChar)).Value = usr
                        cmd.Parameters.Add(New SqlParameter("@eMail", SqlDbType.VarChar)).Value = txeMail.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@PASSWORD", SqlDbType.VarChar)).Value = pass
                        cmd.Parameters.Add(New SqlParameter("@Zona", SqlDbType.Decimal)).Value = z
                        cmd.Parameters.Add(New SqlParameter("@ZonaCobranza", SqlDbType.Decimal)).Value = zc
                        cmd.Parameters.Add(New SqlParameter("@ComisionVenta", SqlDbType.Decimal)).Value = nuPComision.Value
                        cmd.Parameters.Add(New SqlParameter("@ComisionVenta2", SqlDbType.Decimal)).Value = nuCComison.Value
                        cmd.Parameters.Add(New SqlParameter("@Tienda", SqlDbType.BigInt)).Value = oInfoTienda.ID
                        cmd.Parameters.Add(New SqlParameter("@DiaDescanso", SqlDbType.BigInt)).Value = cbDescanso.SelectedIndex
                        cmd.Parameters.Add(New SqlParameter("@Estado", SqlDbType.VarChar)).Value = cbEstado.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@Ciudad", SqlDbType.VarChar)).Value = cbCiudad.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@Colonia", SqlDbType.VarChar)).Value = cbColonia.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@Cp", SqlDbType.VarChar)).Value = cbPC.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@Status", SqlDbType.Bit)).Value = ckActivo.Checked
                        cmd.Parameters.Add(New SqlParameter("@Fijo", SqlDbType.Bit)).Value = rgTipoEmp.SelectedIndex
                        cmd.Parameters.Add(New SqlParameter("@Turno", SqlDbType.Bit)).Value = rgTurno.SelectedIndex
                        cmd.Parameters.Add(New SqlParameter("@ClaveInfonavit", SqlDbType.VarChar)).Value = txInfonavit.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@compensacionDiaria", SqlDbType.VarChar)).Value = txCompensa.Text.Trim
                        cmd.Parameters.Add(New SqlParameter("@Bono", SqlDbType.VarChar)).Value = txBono.Text
                        cmd.Parameters.Add(New SqlParameter("@HoraEnt", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit1.Time)
                        cmd.Parameters.Add(New SqlParameter("@HoraSal", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit2.Time)
                        ' String.Format("{0:HH:mm:ss}", TimeEdit1.Time)
                        If hayfoto Then
                            Dim imagen As Byte()
                            imagen = Image2Bytes(Me.PictureBox1.Image)
                            cmd.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = imagen
                        Else
                            cmd.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = DBNull.Value
                        End If
                        Dim regresa As String = bdBase.bdExecute(conexion, cmd, True)
                        sQuery = "SELECT id FROM empleado WHERE UPPER([user]) = '" & usr.ToUpper & "' AND UPPER([PASSWORD]) = '" & pass.ToUpper & "'"
                        Dim drE As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                        If Not drE Is Nothing Then
                            If drE.HasRows Then
                                drE.Read()
                                regresa = drE("id")
                            End If
                            drE.Close()
                        End If
                        If Not String.IsNullOrWhiteSpace(regresa) Then
                            If IsNumeric(regresa) Then
                                ' Guarda vacaciones
                                sQuery = "INSERT INTO vacaciones (idEmpleado, Del, Al, montoPrima, compensacion) VALUES (@idEmpleado, @Del, @Al, @montoPrima, @compensacion)"
                                Dim cmdV As New SqlCommand
                                cmdV.CommandText = sQuery
                                cmdV.Parameters.Add(New SqlParameter("@idEmpleado", SqlDbType.BigInt)).Value = regresa
                                cmdV.Parameters.Add(New SqlParameter("@Del", SqlDbType.DateTime)).Value = deVDel.DateTime
                                cmdV.Parameters.Add(New SqlParameter("@Al", SqlDbType.DateTime)).Value = deVAl.DateTime
                                cmdV.Parameters.Add(New SqlParameter("@montoPrima", SqlDbType.Float)).Value = txPrima.Text.Trim
                                cmdV.Parameters.Add(New SqlParameter("@compensacion", SqlDbType.Float)).Value = txCompV.Text.Trim
                                regresa = bdBase.bdExecute(conexion, cmdV)
                                If String.IsNullOrEmpty(regresa) Then
                                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(650, Mensaje.Texto), IdiomaMensajes(650, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    limpia()
                                    LlenaCombobox(ComboBox7, "Select distinct numero,nombre from empleado order by nombre", "nombre", "numero", conexion)
                                Else
                                    cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmdV)
                                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(651, Mensaje.Texto), IdiomaMensajes(651, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                End If
                            Else
                                cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmd)
                                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(652, Mensaje.Texto), IdiomaMensajes(652, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                        End If
                        Splash(False)
                    End If
                    dr.Close()
                End If
            End If
        End If
    End Sub

#Region "Valida Combos"
    Private Function ValidaCombos() As Boolean
        Dim regresa As Boolean = False
        Dim oInfo As cInfoCombo
        oInfo = CType(Me.cbEstado.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(220, Mensaje.Texto), IdiomaMensajes(220, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If
        oInfo = CType(Me.cbCiudad.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(222, Mensaje.Texto), IdiomaMensajes(222, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If
        If cbColonia.SelectedIndex <> -1 Then
            oInfo = CType(Me.cbColonia.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(cbColonia.Text) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        End If

        oInfo = CType(Me.cbTienda.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(653, Mensaje.Texto), IdiomaMensajes(653, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If

        If cbPC.SelectedIndex <> -1 Then
            oInfo = CType(Me.cbPC.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(cbPC.Text) Then
                If cbPC.Text.Trim.Length = 5 Then
                    regresa = GuardaElementoSepomex()
                    'regresa = True
                Else
                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(225, Mensaje.Texto), IdiomaMensajes(225, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Return False
                    Exit Function
                End If
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        End If
        oInfo = CType(Me.cbRuta.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(654, Mensaje.Texto), IdiomaMensajes(654, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Return False
            Exit Function
        End If
        oInfo = CType(Me.cbCobranza.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(655, Mensaje.Texto), IdiomaMensajes(655, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Return False
            Exit Function
        End If
        'Select Case rgTipoEmp.SelectedIndex
        '    Case 0
        '        ' RadioButton1.Checked
        '        'oInfo = CType(Me.ComboBox9.SelectedItem, cInfoCombo)
        '        'If Not IsNothing(oInfo) Then
        '        '    regresa = True
        '        'Else
        '        '   DevExpress.XtraEditors.XtraMessageBox.Show("Seleccione Celula", "No puede continuar!", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        '        '    Exit Function
        '        'End If
        '    Case 1
        '        ' RadioButton2.Checked

        'End Select
        If cbDepartamento.SelectedIndex = -1 Then
            If Not String.IsNullOrEmpty(cbDepartamento.Text.Trim) Then
                Dim sQuery As String = "INSERT INTO deptoPuesto (Departamento) VALUES ('" & cbDepartamento.Text.Trim & "') SELECT @@IDENTITY AS id"
                Dim regresar As String = bdBase.bdExecute(conexion, sQuery, False, True)
                If Not String.IsNullOrEmpty(regresar) Then
                    If IsNumeric(regresar) Then
                        cbDepartamento.Tag = regresar
                        regresa = True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            cbDepartamento.Tag = ""
        End If

        'Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
        '    Case 0, 2, 3, 4

        'End Select

        Return regresa
    End Function

    Private Function GuardaElementoSepomex() As Boolean
        Dim regresar As Boolean = False
        If Not String.IsNullOrEmpty(cbColonia.Text.Trim) And Not String.IsNullOrEmpty(cbPC.Text.Trim) Then
            Dim sQuery As String = "INSERT INTO sepomex (cp, colonia, municipio, ciudad, estado, codEstado, codigoPais, llave) VALUES ('" & _
                                           cbPC.Text.Trim & "', '" & cbColonia.Text.Trim & "', '" & cbCiudad.Text.Trim & "', '" & cbCiudad.Text.Trim & "', '" & _
                                           cbEstado.Text.Trim & "', '" & CType(cbEstado.SelectedItem, cInfoCombo).ID & "', 'MX', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER)) SELECT @@IDENTITY AS id"
            Dim regresa As String = bdBase.bdExecute(conexion, sQuery, False, True)
            If Not String.IsNullOrEmpty(regresa) Then
                If IsNumeric(regresa) Then
                    cbPC.Tag = regresa
                    regresar = True
                Else
                    cbPC.Tag = "-1"
                End If
            End If
        End If
        Return regresar
    End Function
#End Region

    Private Sub btImagen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImagen.Click
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = System.Drawing.Image.FromFile(OpenFileDialog1.FileName)
            hayfoto = True
        End If
    End Sub

    Private Sub txRfc_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txRfc.Validating
        ' "^([A-Z|a-z|&amp;]{3,4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)(\w{2})([A|a|0-9]{1})$|^([A-Z|a-z]{4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)((\w{2})([A|a|0-9]{1})){0,3}$"
        Dim regExp As New Regex("^([A-Z|a-z|&amp;]{3,4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)(\w{2})([A|a|0-9]{1})$|^([A-Z|a-z]{4}\d{2}((0[1-9]|1[012])(0[1-9]|1\d|2[0-8])|(0[13456789]|1[012])(29|30)|(0[13578]|1[02])31)|([02468][048]|[13579][26])0229)$")
        If regExp.IsMatch(txRfc.Text) Then
            ErrorProvider1.SetError(txRfc, "")
            btGuardar.Enabled = True
        Else
            ErrorProvider1.SetError(txRfc, "RFC no válido")
            btGuardar.Enabled = False
            e.Cancel = True
        End If
    End Sub

    Private Sub txeMail_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txeMail.Validating
        Dim regExp As New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        If regExp.IsMatch(txeMail.Text) Then
            ErrorProvider1.SetError(txeMail, "")
            btGuardar.Enabled = True
            'txUsuario.Text = TextBox9.Text.Trim
        Else
            ErrorProvider1.SetError(txeMail, "Correo no válido")
            btGuardar.Enabled = False
            e.Cancel = True
        End If
    End Sub

    Private Sub txUsuario_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txUsuario.Validating
        If txUsuario.Text.Trim.Length = 0 Then
            ErrorProvider1.SetError(txUsuario, "Debe Seleccionar usuario")
            btGuardar.Enabled = False
            e.Cancel = True
        Else
            ErrorProvider1.SetError(txUsuario, "")
            btGuardar.Enabled = True
        End If
    End Sub

    Private Sub txClave_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txClave.Validating        
        'If txClave.Text.Trim.Length = 0 Then
        '    ErrorProvider1.SetError(txClave, "Debe Seleccionar Password")
        '    btGuardar.Enabled = False
        '    e.Cancel = True
        'Else            
        '    ErrorProvider1.SetError(txClave, "")
        '    btGuardar.Enabled = True
        '    If Not txClave.Text.Trim.Equals(prevPass) Then
        '        Dim myAdmin As New Administradores
        '        myAdmin.ShowDialog()
        '        If Not myAdmin.Verificado Then
        '            txClave.Text = prevPass
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub txNombre_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txNombre.Validating
        If txNombre.Text.Trim.Length = 0 Then
            ErrorProvider1.SetError(txNombre, "Debe Seleccionar Nombre")
            btGuardar.Enabled = False
            e.Cancel = True
        Else
            ErrorProvider1.SetError(txNombre, "")
            btGuardar.Enabled = True
        End If
    End Sub

    Private Sub txCalle_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txCalle.Validating
        If txCalle.Text.Trim.Length = 0 Then
            ErrorProvider1.SetError(txCalle, "Debe Escribir Calle y Número")
            btGuardar.Enabled = False
            e.Cancel = True
        Else
            ErrorProvider1.SetError(txCalle, "")
            btGuardar.Enabled = True
        End If
    End Sub

    Private Sub FillCombo(ByVal elemento As String, ByVal id As String, ByVal combo As ComboBox, ByVal tabla As String)
        If id = "" Or id = "-1" Or id = "-1.00" Then
        Else
            Dim qry As String
            qry = "SELECT      " & elemento & _
               " FROM   " & tabla & _
               " WHERE     (id ='" & id & "')"
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            dr2.Read()
            Dim contador As Integer = 0
            For contador = 0 To combo.Items.Count - 1
                If combo.Items.Item(contador).ToString.Trim = dr2(elemento).ToString.Trim Then
                    combo.SelectedIndex = contador
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub limpia()
        Cursor.Current = Cursors.WaitCursor
        deNacimiento.Text = DateAdd(DateInterval.Year, -12, Now.Date)
        deIngreso.Text = Now.Date
        deVDel.DateTime = Now.Date
        deVAl.DateTime = DateAdd(DateInterval.Day, 5, Now.Date)

        bEsNuevo = True
        txNombre.Text = ""
        txCalle.Text = ""
        txTel1.Text = ""
        txTel2.Text = ""
        txRfc.Text = ""
        txCurp.Text = ""
        txeMail.Text = ""
        txImss.Text = ""
        txNomina.Text = ""
        txClabe.Text = ""
        txSueldo.Text = "0"
        txUsuario.Text = ""
        txClave.Text = ""
        txCompensa.Text = "0"
        txPrima.Text = "0"
        txCompV.Text = "0"
        txBono.Text = "0"
        nuPComision.Value = 0
        nuCComison.Value = 0
        cbDepartamento.SelectedIndex = -1
        ComboBox7.SelectedIndex = -1
        btRegistraHuella.Enabled = False

        cbEstado.SelectedIndex = -1
        cbCiudad.SelectedIndex = -1
        cbColonia.SelectedIndex = -1
        cbPC.SelectedIndex = -1
        cbRuta.SelectedIndex = -1

        rgTipoEmp.SelectedIndex = 0
        rgTurno.SelectedIndex = 0

        cbCobranza.SelectedIndex = -1
        cbTienda.SelectedIndex = -1

        txeMail.Enabled = True
        txUsuario.Enabled = True
        txClave.Enabled = True

        Me.PictureBox1.Image = Nothing
        Cursor.Current = Cursors.Default
    End Sub

    Private Sub btMostrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btMostrar.Click
        Splash(True)

        If ComboBox7.SelectedIndex = -1 Then

            MsgBox("No hay empleado seleccionado", MsgBoxStyle.Exclamation)
            Exit Sub
        Else
            Me.PictureBox1.Image = Nothing

            Dim qry As String = "SELECT numero, NOMBRE, isnull(Calle,'') as Calle, isnull(idColonia,-1) as idDireccion, Telefono, isnull(Telefono2,'') as Telefono2, " & _
                                "isnull(FechaNacimiento,getdate()) as FechaNacimiento, isnull(FechaIngreso,getdate()) as FechaIngreso, " & _
                                " isnull(RFC,'') as RFC , isnull(CURP,'') as CURP, isnull(eMail,'') as eMail , isnull(IMSS,'') as imss, isnull(CuentaNomina,'') as CuentaNomina, " & _
                                "isnull(CuentaClabe,'') as CuentaClabe , isnull(Sueldo,0) as Sueldo, Puesto, Bono, " & _
                                " Foto, isnull([user],'') As usuario, isnull(PASSWORD,'') as password, isnull(Zona,0) as Zona, isnull(ZonaCobranza,0) as ZonaCobranza, " & _
                                " isnull(ComisionVenta,0) as ComisionVenta, isnull(ComisionVenta2,0) as ComisionVenta2, Borrado, FechaAlta, tipo, tienda, DiaDescanso, " & _
                                "Estado, Ciudad, Colonia, Cp, Status, Fijo, Turno, ClaveInfonavit, compensacionDiaria FROM empleado   where numero='" & _
                                CType(ComboBox7.SelectedItem, cInfoCombo).ID & "'  "
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If dr Is Nothing Then
                ComboBox7.SelectedIndex = -1
                Splash(False)
                Exit Sub
            End If
            dr.Read()
            bEsNuevo = False
            If Not IsDBNull(dr("nombre")) Then txNombre.Text = dr("nombre").Trim.ToUpper Else txNombre.Text = ""
            If Not IsDBNull(dr("calle")) Then txCalle.Text = dr("calle").Trim.ToUpper Else txCalle.Text = ""
            If Not IsDBNull(dr("telefono")) Then txTel1.Text = dr("telefono").Trim.ToUpper Else txTel1.Text = ""
            If Not IsDBNull(dr("telefono2")) Then txTel2.Text = dr("telefono2").Trim.ToUpper Else txTel2.Text = ""
            If Not IsDBNull(dr("rfc")) Then txRfc.Text = dr("rfc").Trim.ToUpper Else txRfc.Text = ""
            If Not IsDBNull(dr("curp")) Then txCurp.Text = dr("curp").Trim.ToUpper Else txCurp.Text = ""
            If Not IsDBNull(dr("email")) Then txeMail.Text = dr("email").Trim Else txeMail.Text = ""
            If Not IsDBNull(dr("imss")) Then txImss.Text = dr("imss").Trim.ToUpper Else txImss.Text = ""
            If Not IsDBNull(dr("cuentanomina")) Then txNomina.Text = dr("cuentanomina").Trim.ToUpper Else txNomina.Text = ""
            If Not IsDBNull(dr("cuentaclabe")) Then txClabe.Text = dr("cuentaclabe").Trim.ToUpper Else txClabe.Text = ""
            If Not IsDBNull(dr("sueldo")) Then txSueldo.Text = dr("sueldo").ToString.Trim.ToUpper Else txSueldo.Text = ""
            If Not IsDBNull(dr("usuario")) Then txUsuario.Text = dr("usuario").Trim.ToUpper Else txUsuario.Text = ""
            If Not IsDBNull(dr("password")) Then txClave.Text = dr("password").Trim.ToUpper Else txClave.Text = ""
            If Not IsDBNull(dr("FechaNacimiento")) Then deNacimiento.DateTime = dr("FechaNacimiento") Else deNacimiento.DateTime = Now.Date
            If Not IsDBNull(dr("FechaIngreso")) Then deIngreso.DateTime = dr("FechaIngreso") Else deIngreso.DateTime = Now.Date
            If Not IsDBNull(dr("comisionventa")) Then nuPComision.Value = dr("comisionventa") Else nuPComision.Value = 0
            If Not IsDBNull(dr("comisionventa2")) Then nuCComison.Value = dr("comisionventa2") Else nuPComision.Value = 0
            If Not IsDBNull(dr("DiaDescanso")) Then cbDescanso.SelectedIndex = dr("DiaDescanso") Else cbDescanso.SelectedIndex = 0
            If Not IsDBNull(dr("idDireccion")) Then
                If dr("idDireccion") <> -1 Then
                    ' Buscar datos sepomex
                    qry = "SELECT cp, colonia, municipio, estado FROM sepomex WHERE id = " & dr("idDireccion")
                    Dim drCp As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                    If Not drCp Is Nothing Then
                        If drCp.HasRows Then
                            drCp.Read()
                            BuscaEnComboDx(cbEstado, drCp("Estado").ToString.Trim)
                            BuscaEnComboDx(cbCiudad, drCp("municipio").ToString.Trim)
                            BuscaEnComboDx(cbColonia, drCp("colonia").ToString.Trim)
                            BuscaEnComboDx(cbPC, drCp("cp").ToString.Trim)
                        End If
                        drCp.Close()
                    End If
                Else
                    BuscaEnComboDx(cbEstado, dr("Estado").ToString.Trim)
                    BuscaEnComboDx(cbCiudad, dr("Ciudad").ToString.Trim)
                    cbColonia.Text = dr("Colonia").ToString.Trim
                    cbPC.Text = dr("Cp").ToString.Trim
                End If
            Else
                cbEstado.SelectedIndex = -1
                cbCiudad.SelectedIndex = -1
                cbColonia.SelectedIndex = -1
                cbPC.SelectedIndex = -1
            End If
            btRegistraHuella.Enabled = True
            If Not IsDBNull(dr("puesto")) Then
                'Select Case dr("puesto").ToString.Trim
                '    Case 1
                '        ' rgTipoEmp.SelectedIndex = 2
                '        cbDepartamento.SelectedIndex = 2
                '    Case 2
                '        ' rgTipoEmp.SelectedIndex = 3
                '        cbDepartamento.SelectedIndex = 3
                '    Case 3
                '        ' rgTipoEmp.SelectedIndex = 0
                '        cbDepartamento.SelectedIndex = 0
                '    Case 4
                '        ' rgTipoEmp.SelectedIndex = 1
                '        cbDepartamento.SelectedIndex = 1
                '    Case 5
                '        ' rgTipoEmp.SelectedIndex = 4
                '        cbDepartamento.SelectedIndex = 4
                '    Case Else
                '        ' rgTipoEmp.SelectedIndex = 0
                '        cbDepartamento.SelectedIndex = 0
                'End Select
                BuscaEnComboDx(cbDepartamento, dr("puesto"), True)
                Select Case dr("puesto").ToString.Trim
                    Case 0, 2, 3, 4
                        txeMail.Visible = True
                        txeMail.Enabled = True
                        txUsuario.Visible = False
                        'txUsuario.Text = ""
                    Case 1
                        txUsuario.Enabled = True
                        txClave.Properties.ReadOnly = False
                        txClave.Enabled = True
                    Case Else
                        txeMail.Visible = False
                        txUsuario.Visible = True
                        txUsuario.Enabled = True
                        If txUsuario.Text.Trim.ToLower = "mail@mail.com" Then txUsuario.Text = ""
                End Select
                If (txUsuario.Text.Trim.ToLower = "mail@mail.com") And (txeMail.Text.Trim.ToLower <> "mail@mail.com") Then
                    If txeMail.Text.Trim.Contains("@") Then
                        txUsuario.Text = txeMail.Text.Trim
                    End If
                End If
                If (txeMail.Text.Trim.ToLower = "mail@mail.com") And (txUsuario.Text.Trim.ToLower <> "mail@mail.com") Then
                    If txUsuario.Text.Trim.Contains("@") Then
                        txeMail.Text = txUsuario.Text.Trim
                    End If
                End If
            Else
                rgTipoEmp.SelectedIndex = 0
            End If
            'txeMail.Enabled = False
            'txUsuario.Enabled = False
            'txClave.Enabled = False
            If Not IsDBNull(dr("zona")) Then BuscaEnComboDx(cbRuta, dr("zona").ToString.Trim, True) Else cbRuta.SelectedIndex = -1
            If Not IsDBNull(dr("zonacobranza")) Then BuscaEnComboDx(cbCobranza, dr("zonacobranza").ToString.Trim, True) Else cbCobranza.SelectedIndex = -1
            If Not IsDBNull(dr("tienda")) Then
                BuscaEnComboDx(cbTienda, dr("tienda"), True)
            Else
                cbTienda.SelectedIndex = -1
            End If

            If Not IsDBNull(dr("Foto")) Then
                Dim img As Image = Bytes2Image(CType(dr("Foto"), Byte()))
                If img IsNot Nothing Then
                    Me.PictureBox1.Image = img
                    hayfoto = True
                    'bFoto = False
                Else
                    Me.PictureBox1.Image = Nothing
                    hayfoto = False
                End If
            End If
            If Not IsDBNull(dr("Status")) Then ckActivo.Checked = dr("Status") Else ckActivo.Checked = False
            If Not IsDBNull(dr("Fijo")) Then
                If dr("Fijo") Then rgTipoEmp.SelectedIndex = 1 Else rgTipoEmp.SelectedIndex = 0
            End If
            If Not IsDBNull(dr("Turno")) Then
                If dr("Turno") Then rgTurno.SelectedIndex = 1 Else rgTurno.SelectedIndex = 0
            End If
            If Not IsDBNull(dr("ClaveInfonavit")) Then txInfonavit.Text = dr("ClaveInfonavit") Else txInfonavit.Text = "0"
            If Not IsDBNull(dr("compensacionDiaria")) Then txCompensa.Text = dr("compensacionDiaria") Else txCompensa.Text = "0"
            If Not IsDBNull(dr("Bono")) Then txBono.Text = dr("Bono") Else txBono.Text = "0"
            ' Busca vacaciones
            qry = "SELECT * FROM vacaciones WHERE idEmpleado = " & dr("numero")
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr2 Is Nothing Then
                If dr2.HasRows Then
                    dr2.Read()
                    deVDel.DateTime = dr2("Del")
                    deVAl.DateTime = dr2("Al")
                    txPrima.Text = dr2("montoPrima")
                    txCompV.Text = dr2("compensacion")
                End If
                dr2.Close()
            End If
            prevPass = txClave.Text
        End If
        Splash(False)
    End Sub

    Private Sub btModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btModificar.Click

        hayfoto = False

        If bEsNuevo Then
            btGuardar.PerformClick()
            Exit Sub
        End If
        Splash(True)
        Dim usr, pass As String
        Dim tipu As Integer
        If Validation.FormIsValid(Me) Then
            If ValidaCombos() Then
                Dim qry As String = String.Empty
                If ckActivo.Checked = False Then
                    If txUsuario.Text.Trim.Length = 0 Then
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(649, Mensaje.Texto), IdiomaMensajes(649, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)

                        Exit Sub
                    End If
                    If txClave.Text.Trim.Length = 0 Then
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(656, Mensaje.Texto), IdiomaMensajes(656, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                        Exit Sub
                    End If
                Else
                    usr = "-1"
                    pass = "-1"
                End If
                Dim z, zc, cel, ti, dep As Integer
                pass = txClave.Text.Trim
                If cbDepartamento.SelectedIndex <> -1 Then
                    Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                        'Case 0
                        '    usr = txeMail.Text.Trim
                        '    'tipu = 3
                        'Case 1
                        '    usr = txUsuario.Text.Trim
                        '    'tipu = 4
                        'Case 2
                        '    usr = txeMail.Text.Trim
                        '    'tipu = 1
                        'Case 3
                        '    usr = txeMail.Text.Trim
                        '    'tipu = 2
                        'Case 4
                        '    usr = txeMail.Text.Trim
                        '    'tipu = 5
                        'Case Else
                        '    usr = txUsuario.Text.Trim
                        '    'tipu = 4
                        Case 0, 2, 3, 4
                            usr = txeMail.Text.Trim
                        Case 1
                            usr = txUsuario.Text.Trim
                        Case Else
                            usr = txeMail.Text.Trim
                    End Select
                    tipu = CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                Else
                    usr = txeMail.Text.Trim
                    tipu = cbDepartamento.Tag
                    cbDepartamento.Tag = ""
                End If
                'Select Case rgTipoEmp.SelectedIndex
                '    Case 0
                '        usr = txeMail.Text.Trim
                '        tipu = 3
                '    Case 1
                '        usr = txUsuario.Text.Trim
                '        tipu = 4
                '    Case 2
                '        usr = txeMail.Text.Trim
                '        tipu = 1
                '    Case 3
                '        usr = txeMail.Text.Trim
                '        tipu = 2
                '    Case 4
                '        usr = txeMail.Text.Trim
                '        tipu = 5
                'End Select
                If cbRuta.SelectedIndex = -1 Then
                    z = -1
                Else
                    z = CType(cbRuta.SelectedItem, cInfoCombo).ID
                End If

                If cbCobranza.SelectedIndex = -1 Then
                    zc = -1
                Else
                    zc = CType(cbCobranza.SelectedItem, cInfoCombo).ID
                End If
                '  
                Dim sQuery As String = "update dbo.empleado set [NOMBRE]=@NOMBRE, [Calle]=@Calle, [idColonia]= @idColonia, [Telefono]=@Telefono, [Telefono2]=@Telefono2, " & _
                                       "[FechaNacimiento]=@FechaNacimiento, [FechaIngreso]=@FechaIngreso, [RFC]=@RFC, [CURP]=@CURP, [eMail]=@eMail, [IMSS]=@IMSS, [CuentaNomina]=@CuentaNomina, " & _
                                       "[CuentaClabe]= @CuentaClabe, [Sueldo]= @Sueldo, [Puesto]=@Puesto, [tipo]=@Tipo, [User]=@Usuario, [PASSWORD]=@PASSWORD, " & _
                                       "[Zona]=@Zona, [ZonaCobranza]=@ZonaCobranza, [ComisionVenta]=@ComisionVenta, [ComisionVenta2]=@ComisionVenta2, [Tienda]=@tienda, " & _
                                       "[Foto]=@Foto, [DiaDescanso]=@DiaDescanso, [Estado]=@Estado, [Ciudad]=@Ciudad, [Colonia]=@Colonia, [Cp]=@Cp, " & _
                                       "[Status]=@Status, [Fijo]=@Fijo, [Turno]=@Turno, [ClaveInfonavit]=@ClaveInfonavit, [compensacionDiaria]=@compensacionDiaria, " & _
                                       "[Bono]=@Bono, HoraEnt = @HoraEnt, HoraSal = @HoraSal where numero =@numero"
                Dim cmd As New SqlCommand
                cmd.CommandText = sQuery
                cmd.Parameters.Add(New SqlParameter("@NOMBRE", SqlDbType.VarChar)).Value = txNombre.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@Calle", SqlDbType.VarChar)).Value = txCalle.Text.Trim.ToUpper

                If cbPC.SelectedIndex <> -1 Then
                    cmd.Parameters.Add(New SqlParameter("@idColonia", SqlDbType.BigInt)).Value = CType(cbPC.SelectedItem, cInfoCombo).ID
                Else
                    cmd.Parameters.Add(New SqlParameter("@idColonia", SqlDbType.BigInt)).Value = cbPC.Tag
                    cbPC.Tag = ""
                End If

                cmd.Parameters.Add(New SqlParameter("@Telefono", SqlDbType.VarChar)).Value = txTel1.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@Telefono2", SqlDbType.VarChar)).Value = txTel2.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@FechaNacimiento", SqlDbType.DateTime)).Value = deNacimiento.DateTime
                cmd.Parameters.Add(New SqlParameter("@FechaIngreso", SqlDbType.DateTime)).Value = deIngreso.DateTime
                cmd.Parameters.Add(New SqlParameter("@RFC", SqlDbType.VarChar)).Value = txRfc.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@CURP", SqlDbType.VarChar)).Value = txCurp.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@eMail", SqlDbType.VarChar)).Value = txeMail.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@IMSS", SqlDbType.VarChar)).Value = txImss.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@CuentaNomina", SqlDbType.VarChar)).Value = txNomina.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@CuentaClabe", SqlDbType.VarChar)).Value = txClabe.Text.Trim.ToUpper
                cmd.Parameters.Add(New SqlParameter("@Sueldo", SqlDbType.Decimal)).Value = IIf(String.IsNullOrWhiteSpace(txSueldo.Text.Trim.ToUpper), 0, txSueldo.Text.Trim.ToUpper)
                cmd.Parameters.Add(New SqlParameter("@Puesto", SqlDbType.Decimal)).Value = tipu
                cmd.Parameters.Add(New SqlParameter("@tipo", SqlDbType.Decimal)).Value = tipu
                cmd.Parameters.Add(New SqlParameter("@Usuario", SqlDbType.VarChar)).Value = usr
                cmd.Parameters.Add(New SqlParameter("@PASSWORD", SqlDbType.VarChar)).Value = pass
                cmd.Parameters.Add(New SqlParameter("@Zona", SqlDbType.Decimal)).Value = z
                cmd.Parameters.Add(New SqlParameter("@ZonaCobranza", SqlDbType.Decimal)).Value = zc
                cmd.Parameters.Add(New SqlParameter("@ComisionVenta", SqlDbType.Decimal)).Value = nuPComision.Value
                cmd.Parameters.Add(New SqlParameter("@ComisionVenta2", SqlDbType.Decimal)).Value = nuCComison.Value
                cmd.Parameters.Add(New SqlParameter("@Tienda", SqlDbType.BigInt)).Value = CType(cbTienda.SelectedItem, cInfoCombo).ID
                cmd.Parameters.Add(New SqlParameter("@numero", SqlDbType.BigInt)).Value = CType(ComboBox7.SelectedItem, cInfoCombo).ID
                cmd.Parameters.Add(New SqlParameter("@DiaDescanso", SqlDbType.BigInt)).Value = cbDescanso.SelectedIndex
                cmd.Parameters.Add(New SqlParameter("@Estado", SqlDbType.VarChar)).Value = cbEstado.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@Ciudad", SqlDbType.VarChar)).Value = cbCiudad.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@Colonia", SqlDbType.VarChar)).Value = cbColonia.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@Cp", SqlDbType.VarChar)).Value = cbPC.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@Status", SqlDbType.Bit)).Value = ckActivo.Checked
                cmd.Parameters.Add(New SqlParameter("@Fijo", SqlDbType.Bit)).Value = rgTipoEmp.SelectedIndex
                cmd.Parameters.Add(New SqlParameter("@Turno", SqlDbType.Bit)).Value = rgTurno.SelectedIndex
                cmd.Parameters.Add(New SqlParameter("@ClaveInfonavit", SqlDbType.VarChar)).Value = txInfonavit.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@compensacionDiaria", SqlDbType.VarChar)).Value = txCompensa.Text.Trim
                cmd.Parameters.Add(New SqlParameter("@Bono", SqlDbType.VarChar)).Value = txBono.Text
                cmd.Parameters.Add(New SqlParameter("@HoraEnt", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit1.Time)
                cmd.Parameters.Add(New SqlParameter("@HoraSal", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit2.Time)
                If hayfoto Then
                    Dim imagen As Byte()
                    imagen = Image2Bytes(Me.PictureBox1.Image)
                    cmd.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = imagen
                Else
                    cmd.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = DBNull.Value
                End If
                Dim regresa As String = bdBase.bdExecute(conexion, cmd)
                Splash(False)
                If String.IsNullOrWhiteSpace(regresa) Then
                    ' Guarda vacaciones
                    sQuery = "UPDATE vacaciones SET Del = @Del, Al = @Al, montoPrima = @montoPrima, compensacion = @compensacion WHERE idEmpleado = @idEmpleado"
                    Dim cmdV As New SqlCommand
                    cmdV.CommandText = sQuery
                    cmdV.Parameters.Add(New SqlParameter("@idEmpleado", SqlDbType.BigInt)).Value = CType(ComboBox7.SelectedItem, cInfoCombo).ID
                    cmdV.Parameters.Add(New SqlParameter("@Del", SqlDbType.DateTime)).Value = deVDel.DateTime
                    cmdV.Parameters.Add(New SqlParameter("@Al", SqlDbType.DateTime)).Value = deVAl.DateTime
                    cmdV.Parameters.Add(New SqlParameter("@montoPrima", SqlDbType.Float)).Value = txPrima.Text.Trim
                    cmdV.Parameters.Add(New SqlParameter("@compensacion", SqlDbType.Float)).Value = txCompV.Text.Trim
                    regresa = bdBase.bdExecute(conexion, cmdV)
                    If String.IsNullOrWhiteSpace(regresa) Then
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(657, Mensaje.Texto), IdiomaMensajes(657, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                        limpia()
                        LlenaCombobox(ComboBox7, "Select distinct numero,nombre from empleado order by nombre", "nombre", "numero", conexion)
                    Else
                        cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmdV)
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(651, Mensaje.Texto), IdiomaMensajes(651, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmd)
                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(652, Mensaje.Texto), IdiomaMensajes(652, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If
    End Sub

    Private Sub btPermisos_Click_1(sender As System.Object, e As System.EventArgs) Handles btPermisos.Click
        'permisos.ShowDialog()
    End Sub

    'Private Sub RadioGroup1_SelectedIndexChanged(sender As Object, e As System.EventArgs)
    '    If rgTipoEmp.SelectedIndex = 1 Then
    '        txUsuario.Properties.ReadOnly = False
    '        If txUsuario.Text.Contains("@") Then
    '            txUsuario.Text = txUsuario.Text.Substring(0, txUsuario.Text.IndexOf("@"))
    '        End If
    '        txClave.Text = ""
    '        txClave.Properties.ReadOnly = False
    '    Else
    '        txUsuario.Properties.ReadOnly = True
    '        'txUsuario.Text = TextBox9.Text.Trim.ToLower
    '        txClave.Text = "12345678"
    '        txClave.Properties.ReadOnly = True
    '    End If
    'End Sub

    'Private Sub cbDepartamento_LostFocus(sender As Object, e As System.EventArgs) Handles cbDepartamento.LostFocus
    '    If cbDepartamento.SelectedIndex <> -1 Then
    '        If cbDepartamento.SelectedItem Is Nothing Then Exit Sub
    '        Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
    '            Case 0, 2, 3, 4
    '                lblMail.Text = "Usuario (eMail)"
    '                txeMail.Visible = True
    '                txeMail.Enabled = False
    '                txUsuario.Visible = False
    '                txClave.Enabled = False
    '                If bEsNuevo Then
    '                    txClave.Text = "12345678"
    '                    txClave.Properties.ReadOnly = True
    '                Else
    '                    txClave.Properties.ReadOnly = False
    '                End If
    '            Case 1
    '                lblMail.Text = "Usuario"
    '                If Not txeMail.Text.Contains("@") Then txeMail.Text = "mail@mail.com"
    '                txeMail.Visible = False
    '                txUsuario.Visible = True
    '                txUsuario.Enabled = True
    '                txClave.Enabled = True
    '                txClave.Properties.ReadOnly = False
    '                If bEsNuevo Then txClave.Text = ""
    '            Case Else
    '                lblMail.Text = "Usuario (eMail)"
    '                txeMail.Visible = True
    '                txUsuario.Visible = False
    '                txeMail.Enabled = False
    '                txClave.Enabled = False
    '                If bEsNuevo Then
    '                    txClave.Text = "12345678"
    '                    txClave.Properties.ReadOnly = True
    '                Else
    '                    txClave.Properties.ReadOnly = False
    '                End If
    '        End Select
    '    Else
    '        lblMail.Text = "Usuario (eMail)"
    '        txeMail.Visible = True
    '        txUsuario.Visible = False
    '        txeMail.Enabled = False
    '        txClave.Enabled = False
    '        If bEsNuevo Then
    '            txClave.Properties.ReadOnly = True
    '        Else
    '            txClave.Text = "12345678"
    '            txClave.Properties.ReadOnly = False
    '        End If
    '    End If
    'End Sub

    Private Sub cbDepartamento_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbDepartamento.SelectedIndexChanged
        'If cbDepartamento.SelectedIndex <> -1 Then
        '    If cbDepartamento.SelectedItem Is Nothing Then Exit Sub
        '    Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
        '        Case 0, 2, 3, 4
        '            lblMail.Text = "Usuario (eMail)"
        '            txeMail.Visible = True
        '            txUsuario.Visible = False
        '            txClave.Text = "12345678"
        '            If bEsNuevo Then txClave.Properties.ReadOnly = True Else txClave.Properties.ReadOnly = False
        '        Case 1
        '            lblMail.Text = "Usuario"
        '            If Not txeMail.Text.Contains("@") Then txeMail.Text = "mail@mail.com"
        '            txeMail.Visible = False
        '            txUsuario.Visible = True
        '            txUsuario.Enabled = True
        '            txClave.Properties.ReadOnly = False
        '            txClave.Enabled = True
        '            If bEsNuevo Then txClave.Text = ""
        '        Case Else
        '            lblMail.Text = "Usuario (eMail)"
        '            txeMail.Visible = True
        '            txUsuario.Visible = False
        '            txClave.Text = "12345678"
        '            If bEsNuevo Then txClave.Properties.ReadOnly = True Else txClave.Properties.ReadOnly = False
        '    End Select
        'Else
        '    lblMail.Text = "Usuario (eMail)"
        '    txeMail.Visible = True
        '    txUsuario.Visible = False
        '    txClave.Text = "12345678"
        '    If bEsNuevo Then txClave.Properties.ReadOnly = True Else txClave.Properties.ReadOnly = False
        'End If
        If cbDepartamento.SelectedIndex <> -1 Then
            If cbDepartamento.SelectedItem Is Nothing Then Exit Sub
            Select Case CType(cbDepartamento.SelectedItem, cInfoCombo).ID
                Case 0, 2, 3, 4
                    lblMail.Text = "Usuario (eMail)"
                    txeMail.Visible = True
                    txUsuario.Visible = False
                    If bEsNuevo Then
                        txClave.Text = "12345678"
                        txClave.Properties.ReadOnly = True
                        txeMail.Enabled = True
                    Else
                        txeMail.Enabled = True
                        txClave.Enabled = False
                        txClave.Properties.ReadOnly = False
                    End If
                Case 1
                    lblMail.Text = "Usuario"
                    If Not txeMail.Text.Contains("@") Then txeMail.Text = "mail@mail.com"
                    txeMail.Visible = False
                    txUsuario.Visible = True
                    txUsuario.Enabled = True
                    txClave.Enabled = True
                    txClave.Properties.ReadOnly = False
                    If bEsNuevo Then txClave.Text = ""
                Case Else
                    lblMail.Text = "Usuario (eMail)"
                    txeMail.Visible = True
                    txUsuario.Visible = False
                    txeMail.Enabled = False
                    txClave.Enabled = False
                    If bEsNuevo Then
                        txClave.Text = "12345678"
                        txClave.Properties.ReadOnly = True
                    Else
                        txClave.Properties.ReadOnly = False
                    End If
            End Select
        Else
            lblMail.Text = "Usuario (eMail)"
            txeMail.Visible = True
            txUsuario.Visible = False
            txeMail.Enabled = False
            txClave.Enabled = False
            If bEsNuevo Then
                txClave.Properties.ReadOnly = True
            Else
                txClave.Text = "12345678"
                txClave.Properties.ReadOnly = False
            End If
        End If
        txClave.Enabled = True
    End Sub

    Private Sub txCurp_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txCurp.Validating
        If Not String.IsNullOrEmpty(txCurp.Text) Then
            Dim regExp As New Regex("^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$")
            If regExp.IsMatch(txCurp.Text) Then
                ErrorProvider1.SetError(txCurp, "")
                btGuardar.Enabled = True
            Else
                ErrorProvider1.SetError(txCurp, "CURP no válido")
                btGuardar.Enabled = False
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub deNacimiento_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles deNacimiento.Validating
        Dim diferencia As Integer = DateDiff(DateInterval.Year, deNacimiento.DateTime, Now.Date)
        If diferencia < 12 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(658, Mensaje.Texto), IdiomaMensajes(658, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            deNacimiento.Focus()
        End If
    End Sub

    Private Sub deIngreso_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles deIngreso.Validating
        deVDel.DateTime = deIngreso.DateTime
        deVAl.DateTime = DateAdd(DateInterval.Day, 5, deIngreso.DateTime)
        ' Calcular antiguedad
    End Sub
#End Region

#Region "Reporte"
    Private Sub GridView1_CustomDrawCell(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs) Handles GridView1.CustomDrawCell
        If ckFotos.Checked Then
            Dim pictureEdit As RepositoryItemPictureEdit = TryCast(GridControl1.RepositoryItems.Add("PictureEdit"), RepositoryItemPictureEdit)
            Dim column As GridColumn = New GridColumn()
            pictureEdit.CustomHeight = 150
            pictureEdit.SizeMode = PictureSizeMode.Zoom
            pictureEdit.NullText = " "
            column = GridView1.Columns("Foto")
            column.ColumnEdit = pictureEdit
            GridView1.Columns(0).Width = 150
            GridView1.RowHeight = 75
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btMostrarR.Click
        Dim qry As String = ""
        Cursor = Cursors.WaitCursor
        If ckFotos.Checked Then
            qry = " SELECT        empleado.Foto, empleado.numero as No, empleado.NOMBRE, empleado.Calle, sepomex.colonia, sepomex.municipio, sepomex.ciudad, sepomex.estado, sepomex.cp, empleado.Telefono, " & _
                  "    empleado.Telefono2, empleado.FechaNacimiento,tiendas.nombre as Tienda, empleado.FechaIngreso, empleado.RFC, empleado.CURP, empleado.eMail, empleado.IMSS,  " & _
                  " empleado.CuentaNomina, empleado.CuentaClabe, empleado.Sueldo, deptoPuesto.Departamento AS Puesto, empleado.[User], empleado.ComisionVenta, empleado.ComisionVenta2, " & _
                  "           empleado.FechaAlta, ZonaCobranza.NOMBRE AS ZonaCobranza, ZonaVentas.NOMBRE AS ZonaDeVentas " & _
                  " FROM            empleado LEFT JOIN " & _
                  " deptoPuesto ON empleado.tipo = deptoPuesto.id LEFT JOIN " & _
                  "    sepomex ON empleado.idColonia = sepomex.id LEFT JOIN " & _
                  "   ZonaCobranza ON empleado.ZonaCobranza = ZonaCobranza.id LEFT JOIN " & _
                  "  ZonaVentas ON empleado.Zona = ZonaVentas.id " & _
                                           "  left join tiendas  on tiendas.numero=empleado.tienda " & _
         " order by empleado.NOMBRE"
            Dim dr As DataSet = bdBase.bdDataset(conexion, qry)
            GridControl1.DataSource = dr.Tables(0)
            GridView1.PopulateColumns()
            GridView1.BestFitColumns()
            GridView1.Columns(0).Width = 150
            GridView1.RowHeight = 75
        Else
            qry = " SELECT     empleado.numero as No, empleado.NOMBRE, empleado.Calle, sepomex.colonia, sepomex.municipio, sepomex.ciudad, sepomex.estado, sepomex.cp, empleado.Telefono, " & _
                     "    empleado.Telefono2, empleado.FechaNacimiento,tiendas.nombre as Tienda, empleado.FechaIngreso, empleado.RFC, empleado.CURP, empleado.eMail, empleado.IMSS,  " & _
            " empleado.CuentaNomina, empleado.CuentaClabe, empleado.Sueldo, deptoPuesto.Departamento AS Puesto, empleado.[User], empleado.ComisionVenta, empleado.ComisionVenta2, " & _
              "           empleado.FechaAlta, ZonaCobranza.NOMBRE AS ZonaCobranza, ZonaVentas.NOMBRE AS ZonaDeVentas " & _
                   " FROM            empleado LEFT JOIN " & _
                   " deptoPuesto ON empleado.tipo = deptoPuesto.id LEFT JOIN " & _
                     "    sepomex ON empleado.idColonia = sepomex.id LEFT JOIN " & _
                      "   ZonaCobranza ON empleado.ZonaCobranza = ZonaCobranza.id LEFT JOIN " & _
                       "  ZonaVentas ON empleado.Zona = ZonaVentas.id " & _
                       "  left join tiendas  on tiendas.numero=empleado.tienda " & _
      " order by empleado.NOMBRE"
            Dim dr As DataSet = bdBase.bdDataset(conexion, qry)
            GridControl1.DataSource = dr.Tables(0)
            GridView1.PopulateColumns()
            GridView1.BestFitColumns()
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btImprimir.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
#End Region

#Region "Prenomina"
    Private Sub btCalcular_Click(sender As System.Object, e As System.EventArgs) Handles btCalcular.Click
        GridControl2.DataSource = Nothing
        Dim sTienda As String = String.Empty
        If Not ckTodasT.Checked Then
            If cbTiendasP.SelectedIndex = -1 Then
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(659, Mensaje.Texto), IdiomaMensajes(659, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            Else
                sTienda = " AND preNominas.Tienda = " & CType(cbTiendasP.SelectedItem, cInfoCombo).ID
            End If
        End If

        Splash(True)
        Dim sQuery As String = String.Empty

        ' Prenominas
        GridControl2.DataSource = Nothing
        sQuery = "SELECT id FROM preNominas WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, Del)) = '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & _
                 "' AND DATEADD(DD, 0, DATEDIFF(DD, 0, Al)) = '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "'"
        Dim dsP As DataSet = bdBase.bdDataset(conexion, sQuery)
        If dsP.Tables(0).Rows.Count > 0 Then
            sQuery = "SELECT preNominas.id, empleado.NOMBRE AS Empleado, preNominas.Del, preNominas.Al, preNominas.Sueldo, preNominas.Compensacion, preNominas.DiasTrabajados, " & _
                     "preNominas.HorasTrabajadas, preNominas.DiasExtras, preNominas.HorasExtras, preNominas.APagar, preNominas.PrimaVacacional, preNominas.OtrosIngresos, " & _
                     "preNominas.Deducciones, preNominas.SueldoNeto FROM preNominas INNER JOIN empleado ON preNominas.idEmpleado = empleado.id " & _
                     "WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, Del)) = '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & _
                     "' AND DATEADD(DD, 0, DATEDIFF(DD, 0, Al)) = '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "' " & sTienda
            Dim dsPP As DataSet = bdBase.bdDataset(conexion, sQuery)
            GridControl2.DataSource = dsPP.Tables(0)
            If GridView2.RowCount > 0 Then
                btExportP.Visible = True
                btGuardaPre.Visible = False
                Splash(False)
                Exit Sub
            End If
        End If

        '' Meta
        'Dim totalParesVendidos As Integer = 0
        'Dim comiTotalPares As Decimal = 0
        'If Not ckTodasT.Checked Then
        '    sQuery = "SELECT SUM(CANTIDAD) AS Canti, PRECIO FROM detnotas WHERE (DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) BETWEEN '" & _
        '             String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "') " & _
        '             "AND Tienda = " & CType(cbTiendasP.SelectedItem, cInfoCombo).ID & " GROUP BY PRECIO"
        '    Dim drC As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        '    If Not drC Is Nothing Then
        '        If drC.HasRows Then
        '            While drC.Read
        '                Dim sTemp As Decimal = drC("Canti") * drC("Precio")
        '                totalParesVendidos += sTemp
        '            End While
        '        End If
        '        drC.Close()
        '    End If
        '    sQuery = "SELECT MetaMin, comiMetaMin FROM tiendas WHERE id = " & My.Settings.TiendaActual
        '    Dim drT As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        '    If drT.HasRows Then
        '        drT.Read()
        '        If Not IsDBNull(drT("MetaMin")) Then
        '            If Not IsDBNull(drT("comiMetaMin")) Then
        '                If totalParesVendidos >= drT("MetaMin") Then
        '                    comiTotalPares = (totalParesVendidos * drT("comiMetaMin")) / 100
        '                End If
        '            End If
        '        End If
        '        drT.Close()
        '    End If
        'End If

        ' Calcular totalParesVendidos y Comision
        Dim dsT As New DataSet()
        Dim tablaT As DataTable = New DataTable()
        tablaT.Columns.Add("idTienda", GetType(Integer))
        tablaT.Columns.Add("totalParesVendidos", GetType(Integer))
        tablaT.Columns.Add("comiTotalPares", GetType(Decimal))
        dsT.Tables.Add(tablaT)
        sQuery = "SELECT id, MetaMin, comiMetaMin FROM Tiendas ORDER BY id"
        Dim drT As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        If Not drT Is Nothing Then
            If drT.HasRows Then
                While drT.Read
                    Dim totalParesVendidos As Integer = 0
                    Dim comiTotalPares As Decimal = 0
                    sQuery = "SELECT SUM(CANTIDAD) AS Canti, PRECIO FROM detnotas WHERE (DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) BETWEEN '" & _
                             String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "') " & _
                             "AND Tienda = " & drT("id") & " GROUP BY PRECIO"
                    Dim drC As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                    If Not drC Is Nothing Then
                        If drC.HasRows Then
                            While drC.Read
                                Dim sTemp As Decimal = drC("Canti") * drC("Precio")
                                totalParesVendidos += sTemp
                            End While
                        End If
                        drC.Close()
                    End If

                    If Not IsDBNull(drT("MetaMin")) Then
                        If Not IsDBNull(drT("comiMetaMin")) Then
                            If totalParesVendidos >= drT("MetaMin") Then
                                comiTotalPares = (totalParesVendidos * drT("comiMetaMin")) / 100
                            End If
                        End If
                    End If

                    Dim row As DataRow = tablaT.NewRow()
                    row("idTienda") = drT("id")
                    row("totalParesVendidos") = totalParesVendidos
                    row("comiTotalPares") = comiTotalPares
                    tablaT.Rows.Add(row)
                End While
            End If
            drT.Close()
        End If



        Dim sComp As String = String.Empty
        If Not ckTodasT.Checked Then sComp = " AND (tiendas.Numero = " & CType(cbTiendasP.SelectedItem, cInfoCombo).ID & ") "
        sQuery = "SELECT DISTINCT idEmpleado AS [NOMBRE] FROM [logdia] WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) BETWEEN '" & _
                 String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "' ORDER BY NOMBRE"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                Dim dsPre As New DataSet()
                Dim tabla As DataTable = New DataTable()
                tabla.Columns.Add("ID", GetType(Integer))               '  0
                tabla.Columns("ID").AutoIncrement = True
                tabla.Columns("ID").AutoIncrementSeed = 1
                tabla.Columns.Add("Nombre", GetType(String))            '  1
                tabla.Columns.Add("Departamento", GetType(String))      '  2
                tabla.Columns.Add("Periodo", GetType(String))           '  3
                tabla.Columns.Add("SueldoDiario", GetType(Decimal))     '  4
                tabla.Columns.Add("Compensacion", GetType(Decimal))     '  5
                tabla.Columns.Add("DiasTrabajados", GetType(Decimal))   '  6
                tabla.Columns.Add("HorasTrabajadas", GetType(Decimal))  '  7
                tabla.Columns.Add("DiasExtras", GetType(Decimal))       '  8
                tabla.Columns.Add("HorasExtras", GetType(Decimal))      '  9
                tabla.Columns.Add("ParesVendidos", GetType(Integer))    ' 10
                tabla.Columns.Add("Comisiones", GetType(Decimal))       ' 11
                tabla.Columns.Add("ComisionMeta", GetType(Decimal))     ' 12
                tabla.Columns.Add("Bono", GetType(Decimal))             ' 13
                tabla.Columns.Add("APagar", GetType(Decimal))           ' 14
                tabla.Columns.Add("PrimaVacacional", GetType(Decimal))  ' 15
                tabla.Columns.Add("OtrosIngresos", GetType(Decimal))    ' 16
                tabla.Columns.Add("Infonavit", GetType(Decimal))        ' 17
                tabla.Columns.Add("Incapacidad", GetType(Decimal))      ' 18
                tabla.Columns.Add("Deducciones", GetType(Decimal))      ' 19
                tabla.Columns.Add("SueldoNeto", GetType(Decimal))       ' 20
                tabla.Columns.Add("Deposito", GetType(Decimal))         ' 21
                tabla.Columns.Add("Efectivo", GetType(Decimal))         ' 22
                tabla.Columns.Add("Firma", GetType(String))             ' 23
                tabla.Columns.Add("idEmpleado", GetType(Integer))       ' 24
                tabla.Columns.Add("Del", GetType(Date))                 ' 25
                tabla.Columns.Add("Al", GetType(Date))                  ' 26
                tabla.Columns.Add("TipoE", GetType(Integer))            ' 27
                tabla.PrimaryKey = New DataColumn() {tabla.Columns("ID")}
                dsPre.Tables.Add(tabla)
                Do While dr.Read
                    If Not IsDBNull(dr("NOMBRE")) Then
                        sQuery = "SELECT logdia.FECHA, logdia.HORA, logdia.NOMBRE AS Empleado, deptoPuesto.Departamento, tiendas.Nombre AS Tienda, " & _
                                 "empleado.DiaDescanso, empleado.Sueldo, empleado.compensacionDiaria, vacaciones.Del, vacaciones.Al, vacaciones.montoPrima, " & _
                                 "vacaciones.compensacion, logdia.TIPO, empleado.FechaIngreso, logdia.idEmpleado, empleado.ComisionVenta, empleado.ComisionVenta2, empleado.Bono, " & _
                                 "empleado.Tipo AS TipoE, tiendas.Numero AS idTienda FROM logdia LEFT OUTER JOIN empleado ON logdia.NOMBRE = empleado.NOMBRE " & _
                                 "LEFT OUTER JOIN tiendas ON logdia.TIENDA = tiendas.NUMERO LEFT OUTER JOIN vacaciones ON empleado.id = vacaciones.idEmpleado " & _
                                 "LEFT OUTER JOIN deptoPuesto ON empleado.Puesto = deptoPuesto.id WHERE (logdia.idEmpleado = '" & dr("NOMBRE").ToString.Trim & _
                                 "') AND  (DATEADD(DD, 0, DATEDIFF(DD, 0, logdia.FECHA)) BETWEEN '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & _
                                 String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "') " & sComp & " AND (NOT (logdia.idEmpleado IS NULL)) GROUP BY logdia.FECHA, " & _
                                 "logdia.HORA, logdia.NOMBRE, deptoPuesto.Departamento, tiendas.Nombre, empleado.DiaDescanso, empleado.Sueldo, " & _
                                 "empleado.compensacionDiaria, vacaciones.Del, vacaciones.Al, vacaciones.montoPrima, vacaciones.compensacion, logdia.TIPO, " & _
                                 "empleado.FechaIngreso, logdia.idEmpleado, empleado.ComisionVenta, empleado.ComisionVenta2, empleado.Bono, empleado.Tipo, " & _
                                 "tiendas.Numero HAVING(empleado.Sueldo > 0) ORDER BY logdia.FECHA, logdia.HORA"
                        Dim ds As DataSet = bdBase.bdDataset(conexion, sQuery)
                        ' Calcular
                        Dim datos(16) As String
                        Dim horasTrabajadas As Integer = 0
                        Dim anterior As String = String.Empty
                        Dim sTipo As String = String.Empty
                        Dim borraAnterior As Boolean = False
                        Dim HorasSemana As Integer = 48
                        Dim bonusDay As Integer = 0
                        Dim antiguedad As Integer = 0
                        Dim primaVacacional As Decimal = 0
                        Dim ComisionVenta As Decimal = 0
                        Dim dBono As Decimal = 0
                        Dim sTipoE As Integer = 0
                        Dim idTienda As Integer = 0
                        For Each renglon As DataRow In ds.Tables(0).Rows
                            antiguedad = DateDiff(DateInterval.Year, renglon("FechaIngreso"), Now.Date)
                            If Not IsDBNull(renglon("montoPrima")) Then primaVacacional = renglon("montoPrima") Else primaVacacional = 0
                            datos(0) = renglon("Empleado").ToString.Trim
                            datos(1) = "Del " & String.Format("{0:dd/MMM/yyyy}", dePDel.DateTime) & " Al " & String.Format("{0:dd/MMM/yyyy}", dePAl.DateTime)
                            If Not IsDBNull(renglon("Sueldo")) Then datos(2) = renglon("Sueldo") Else datos(2) = 0
                            If Not IsDBNull(renglon("compensacionDiaria")) Then datos(3) = renglon("compensacionDiaria") Else datos(3) = 0
                            If Not IsDBNull(renglon("DiaDescanso")) Then
                                Dim iDia As Integer = 0
                                Select Case Convert.ToDateTime(renglon("FECHA")).DayOfWeek.ToString
                                    Case "Monday", "Lunes", "Lun", "Mon"
                                        iDia = 0
                                    Case "Tuesday", "Martes", "Mar", "Tue"
                                        iDia = 1
                                    Case "Wednesday", "Miércoles", "Miercoles", "Mie", "Wed"
                                        iDia = 2
                                    Case "Thursday", "Jueves", "Jue", "Thu"
                                        iDia = 3
                                    Case "Friday", "Viernes", "Vie", "Fri"
                                        iDia = 4
                                    Case "Saturday", "Sábado", "Sabado", "Sab", "Sat"
                                        iDia = 5
                                    Case "Sunday", "Domingo", "Dom", "Sun"
                                        iDia = 6
                                End Select
                                If iDia = renglon("DiaDescanso") Then
                                    If ckExtras.Checked = True Then
                                        bonusDay += 8
                                        '  MessageBox.Show(renglon("Empleado").ToString.Trim)

                                    Else
                                        bonusDay += 0
                                        ' MessageBox.Show(renglon("Empleado").ToString.Trim)

                                    End If
                                End If

                            End If
                            If Not String.IsNullOrEmpty(anterior) Then
                                If sTipo <> renglon("Tipo") Then
                                    horasTrabajadas += DateDiff(DateInterval.Hour, Convert.ToDateTime(anterior), Convert.ToDateTime(renglon("HORA")))
                                    borraAnterior = True
                                Else
                                    horasTrabajadas += 8
                                    borraAnterior = False
                                End If
                            End If
                            If Not borraAnterior Then
                                anterior = renglon("HORA")
                                sTipo = renglon("Tipo")
                            Else
                                anterior = String.Empty
                                sTipo = String.Empty
                            End If
                            datos(12) = renglon("idEmpleado")
                            datos(13) = dePDel.DateTime
                            datos(14) = dePAl.DateTime
                            If RadioGroup1.SelectedIndex = 0 Then
                                ComisionVenta = renglon("ComisionVenta")
                            Else
                                ComisionVenta = renglon("ComisionVenta2")
                            End If
                            If Not IsDBNull(renglon("Departamento")) Then datos(15) = renglon("Departamento").ToString.Trim
                            If Not IsDBNull(renglon("Bono")) Then dBono = renglon("Bono")
                            If Not IsDBNull(renglon("TipoE")) Then sTipoE = renglon("TipoE")
                            If Not IsDBNull(renglon("idTienda")) Then idTienda = renglon("idTienda")
                        Next
                        If Not String.IsNullOrEmpty(anterior) And sTipo.ToUpper = "ENTRADA" Then horasTrabajadas += 8
                        datos(4) = horasTrabajadas \ 8
                        datos(5) = horasTrabajadas
                        If ckExtras.Checked = True Then
                            If bonusDay > 0 Then
                                datos(6) = bonusDay \ 8
                            Else
                                datos(6) = 0
                            End If

                        Else
                            datos(6) = 0
                        End If

                        If ckExtras.Checked Then
                            If horasTrabajadas > HorasSemana Then
                                datos(7) = horasTrabajadas - HorasSemana
                            Else
                                datos(7) = 0
                            End If
                        Else
                            datos(7) = 0
                        End If
                        Dim sueldoPorHora As Decimal = datos(2) / 8
                        datos(8) = Math.Round(((sueldoPorHora * datos(5)) + ((sueldoPorHora * bonusDay) * 2) + datos(3) + dBono), 2)
                        datos(9) = Math.Round((sueldoPorHora * 2) * datos(7), 2)
                        datos(9) += Math.Round(datos(3) * datos(4), 2)
                        'If antiguedad >= 1 Then
                        '    If 
                        '    datos(9) += primaVacacional
                        'End If
                        datos(10) = 0
                        datos(11) = Val(datos(8)) + Val(datos(9)) - Val(datos(10))
                        Dim ParesVendidos As Integer = 0
                        Dim comisiones As Decimal = 0
                        If Not datos(0) Is Nothing Then
                            sQuery = "SELECT SUM(CANTIDAD) AS Canti, PRECIO, EMPLEADO FROM detnotas WHERE (EMPLEADO = " & datos(12) & ") AND " & _
                                 "(DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) BETWEEN '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & _
                                 String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "') GROUP BY PRECIO, EMPLEADO"
                            Dim drC As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                            If Not drC Is Nothing Then
                                If drC.HasRows Then
                                    While drC.Read
                                        Dim tmpComision As Decimal = 0
                                        If RadioGroup1.SelectedIndex = 0 Then
                                            tmpComision = drC("Canti") * drC("Precio")
                                            tmpComision = (tmpComision * ComisionVenta) / 100
                                        Else
                                            tmpComision = ComisionVenta * drC("Canti")
                                        End If
                                        ParesVendidos += drC("Canti")
                                        comisiones += tmpComision
                                    End While
                                End If
                                drC.Close()
                            End If
                            Dim qrydev As String
                            Dim sdf
                            qrydev = "SELECT SUM(CANTIDAD) AS Canti, PRECIO, EMPLEADO FROM detdev WHERE (EMPLEADO = " & datos(12) & ") AND " & _
                           "(DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) BETWEEN '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & "' AND '" & _
                           String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "') GROUP BY PRECIO, EMPLEADO"
                            Dim drdv As SqlDataReader = bdBase.bdDataReader(conexion, qrydev)
                            If Not drdv Is Nothing Then
                                If drdv.HasRows Then
                                    While drdv.Read
                                        Dim tmpComisiondev As Decimal = 0

                                        tmpComisiondev = ComisionVenta * drdv("Canti")

                                        ParesVendidos -= drdv("Canti")
                                        comisiones -= tmpComisiondev
                                    End While
                                End If
                                drdv.Close()

                            End If
                            ' Añadir renglón
                            Dim row As DataRow = tabla.NewRow()
                            row("Nombre") = datos(0)
                            row("Departamento") = datos(15)
                            row("Periodo") = datos(1)
                            row("SueldoDiario") = datos(2)
                            row("Compensacion") = datos(3)
                            row("DiasTrabajados") = datos(4)
                            row("HorasTrabajadas") = datos(5)
                            row("DiasExtras") = datos(6)
                            row("HorasExtras") = datos(7)
                            row("ParesVendidos") = ParesVendidos
                            row("Comisiones") = Math.Round(comisiones, 2)
                            Dim comisionG As Decimal = 0
                            If sTipoE = 2 Then
                                For Each ren As DataRow In dsT.Tables(0).Rows
                                    If datos(12) = idTienda Then
                                        comisionG = ren("comiTotalPares")
                                        Exit For
                                    End If
                                Next
                            End If
                            row("ComisionMeta") = Math.Round(comisionG, 2)
                            row("Bono") = dBono

                            row("APagar") = Math.Round(datos(8) + comisiones + comisionG, 2)
                            row("PrimaVacacional") = 0
                            row("OtrosIngresos") = datos(9)
                            row("Infonavit") = 0
                            row("Incapacidad") = 0
                            row("Deducciones") = datos(10)
                            row("SueldoNeto") = Math.Round(datos(11) + comisiones + comisionG, 2)
                            row("idEmpleado") = datos(12)
                            row("Del") = dePDel.DateTime
                            row("Al") = dePAl.DateTime
                            row("Deposito") = 0
                            row("Efectivo") = 0
                            tabla.Rows.Add(row)
                        End If
                        For i As Integer = 0 To 11
                            datos(i) = String.Empty
                        Next
                    End If
                Loop
                GridControl2.DataSource = dsPre.Tables(0)
                GridView2.PopulateColumns()
                GridView2.BestFitColumns()
                GridView2.Columns(24).Visible = False
                GridView2.Columns(25).Visible = False
                GridView2.Columns(26).Visible = False
                GridView2.Columns(27).Visible = False
                If GridView2.RowCount > 0 Then
                    btExportP.Visible = True
                    btGuardaPre.Visible = True
                End If
            End If
            dr.Close()
        End If
        Splash(False)
    End Sub

    Private Sub btExportP_Click(sender As System.Object, e As System.EventArgs) Handles btExportP.Click
        If GridView2.RowCount > 0 Then
            If ComponentPrinter.IsPrintingAvailable(True) Then
                Dim printer As New ComponentPrinter(GridControl2)
                printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(660, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub GridView2_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView2.CellValueChanged
        Select Case e.Column.FieldName
            Case "Deducciones", "PrimaVacacional", "Infonavit", "Incapacidad", "Extras", "OtrosIngresos", "Deposito", "Efectivo", "Firma"
                Dim valor As Decimal = Val(GridView2.GetRowCellValue(e.RowHandle, "APagar")) + Val(GridView2.GetRowCellValue(e.RowHandle, "OtrosIngresos")) + _
                                       Val(GridView2.GetRowCellValue(e.RowHandle, "PrimaVacacional")) - Val(GridView2.GetRowCellValue(e.RowHandle, "Infonavit")) - _
                                       Val(GridView2.GetRowCellValue(e.RowHandle, "Incapacidad")) - Val(GridView2.GetRowCellValue(e.RowHandle, "Deducciones")) + _
                                       Val(GridView2.GetRowCellValue(e.RowHandle, "Extras"))
                GridView2.SetRowCellValue(e.RowHandle, "SueldoNeto", Math.Round(valor, 2))
        End Select
    End Sub

    Private Sub GridView2_ShowingEditor(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles GridView2.ShowingEditor
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = sender
        Dim columna As DevExpress.XtraGrid.Columns.GridColumn = view.FocusedColumn
        Select Case columna.FieldName
            Case "Deducciones", "PrimaVacacional", "Infonavit", "Incapacidad", "Extras", "OtrosIngresos", "Deposito", "Efectivo", "Firma"
            Case Else
                e.Cancel = True
        End Select
    End Sub

    Private Sub btGuardaPre_Click(sender As System.Object, e As System.EventArgs) Handles btGuardaPre.Click
        Dim bContinua As Boolean = False
        If GridView2.RowCount > 0 Then
            Dim sQuery As String = "SELECT id FROM preNominas WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, Del)) = '" & String.Format("{0:yyyyMMdd}", dePDel.DateTime) & _
                                   "' AND DATEADD(DD, 0, DATEDIFF(DD, 0, Al)) = '" & String.Format("{0:yyyyMMdd}", dePAl.DateTime) & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
            If Not dr Is Nothing Then
                bContinua = Not dr.HasRows
                dr.Close()
            End If
            Dim sTienda As String = "-1"
            If Not ckTodasT.Checked Then
                sTienda = CType(cbTiendasP.SelectedItem, cInfoCombo).ID
            End If
            sQuery = String.Empty
            If bContinua Then
                For i As Integer = 0 To GridView2.RowCount - 1
                    Dim sDel As String = String.Format("{0:yyyyMMdd}", DateValue(GridView2.GetRowCellValue(i, "Del")))
                    Dim sAl As String = String.Format("{0:yyyyMMdd}", GridView2.GetRowCellValue(i, "Al"))
                    sQuery += "INSERT INTO preNominas ([idEmpleado], [Del], [Al], [Sueldo], [Compensacion], [DiasTrabajados], " & _
                              "[HorasTrabajadas], [DiasExtras], [HorasExtras], [APagar], [PrimaVacacional], [OtrosIngresos], [Deducciones], " & _
                              "[SueldoNeto], [tienda], [ParesVendidos], [Comisiones], [ComisionMeta], [Bono], [Infonavit], [Incapacidad], " & _
                              "[Deposito], [Efectivo]) " & _
                              "VALUES ('" & GridView2.GetRowCellValue(i, "idEmpleado") & "', '" & sDel & "', '" & _
                              sAl & "', '" & GridView2.GetRowCellValue(i, "SueldoDiario") & "', '" & GridView2.GetRowCellValue(i, "Compensacion") & _
                              "', '" & GridView2.GetRowCellValue(i, "DiasTrabajados") & "', '" & GridView2.GetRowCellValue(i, "HorasTrabajadas") & "', '" & _
                              GridView2.GetRowCellValue(i, "DiasExtras") & "', '" & GridView2.GetRowCellValue(i, "HorasExtras") & "', '" & GridView2.GetRowCellValue(i, "APagar") & _
                              "', '" & GridView2.GetRowCellValue(i, "PrimaVacacional") & "', '" & GridView2.GetRowCellValue(i, "OtrosIngresos") & "', '" & _
                              GridView2.GetRowCellValue(i, "Deducciones") & "', '" & GridView2.GetRowCellValue(i, "SueldoNeto") & "', '" & sTienda & "', '" & _
                              GridView2.GetRowCellValue(i, "ParesVendidos") & "', '" & GridView2.GetRowCellValue(i, "Comisiones") & "', '" & _
                              GridView2.GetRowCellValue(i, "ComisionMeta") & "', '" & GridView2.GetRowCellValue(i, "Bono") & "', '" & _
                              GridView2.GetRowCellValue(i, "Infonavit") & "', '" & GridView2.GetRowCellValue(i, "Incapacidad") & "', '" & _
                              GridView2.GetRowCellValue(i, "Deposito") & "', '" & GridView2.GetRowCellValue(i, "Efectivo") & "') " & vbCrLf
                Next
                If Not String.IsNullOrEmpty(sQuery) Then
                    Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
                    If String.IsNullOrEmpty(regresa) Then
                        btGuardaPre.Visible = False
                        DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(661, Mensaje.Texto), IdiomaMensajes(661, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(815, Mensaje.Texto), IdiomaMensajes(815, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        End If
    End Sub
#End Region

    Private Sub btRegistraHuella_Click(sender As System.Object, e As System.EventArgs) Handles btRegistraHuella.Click
        Dim forma As New frmRegistro
        forma.pEmpleado = CType(ComboBox7.SelectedItem, cInfoCombo).ID
        forma.ShowDialog()
        forma = Nothing
    End Sub

    Private Sub ckTodasT_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ckTodasT.CheckedChanged
        cbTiendasP.Enabled = Not ckTodasT.Checked
    End Sub

    Private Sub ckTodasP_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ckTodasP.CheckedChanged
        cbTiendaPP.Enabled = Not ckTodasP.Checked
    End Sub

    Private Sub btBuscaP_Click(sender As System.Object, e As System.EventArgs) Handles btBuscaP.Click
        GridControl3.DataSource = Nothing
        GridControl4.DataSource = Nothing
        Dim sTienda As String = "-1"
        If Not ckTodasP.Checked Then
            If cbTiendaPP.SelectedIndex = -1 Then
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(662, Mensaje.Texto), IdiomaMensajes(662, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            Else
                sTienda = CType(cbTiendaPP.SelectedItem, cInfoCombo).ID
            End If
        End If
        Dim sDel As String = String.Format("{0:yyyyMMdd}", deDelP.DateTime)
        Dim sAl As String = String.Format("{0:yyyyMMdd}", deAlP.DateTime)
        Dim sQuery As String = "SELECT preNominas.id, tiendas.Nombre, preNominas.Del, preNominas.Al FROM preNominas INNER JOIN " & _
                               "tiendas ON preNominas.tienda = tiendas.NUMERO WHERE (preNominas.tienda = " & sTienda & ") AND " & _
                               "(DATEADD(DD, 0, DATEDIFF(DD, 0, preNominas.Del)) >= '" & _
                               sDel & "') AND (DATEADD(DD, 0, DATEDIFF(DD, 0, preNominas.Al)) >= '" & sAl & "')"
        Dim ds As DataSet = bdBase.bdDataset(conexion, sQuery)
        GridControl3.DataSource = ds.Tables(0)
        GridView3.PopulateColumns()
        GridView3.BestFitColumns 
        GridView3.Columns(0).Visible = False
        If GridView3.RowCount > 0 Then lblMsj.Visible = True Else lblMsj.Visible = False
    End Sub

    Private Sub GridView3_RowClick(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles GridView3.RowClick
        Splash(True)
        GridControl4.DataSource = Nothing
        Dim sQuery As String = "SELECT tiendas.Nombre, empleado.NOMBRE AS Empleado, deptoPuesto.Departamento, preNominas.Del, preNominas.Al, preNominas.Sueldo, preNominas.Compensacion, " & _
                               "preNominas.DiasTrabajados, preNominas.HorasTrabajadas, preNominas.DiasExtras, preNominas.HorasExtras, preNominas.ParesVendidos, " & _
                               "preNominas.Comisiones, preNominas.ComisionMeta, preNominas.Bono, preNominas.APagar, preNominas.PrimaVacacional, preNominas.OtrosIngresos, " & _
                               "preNominas.Infonavit, preNominas.Incapacidad, preNominas.Deducciones, preNominas.SueldoNeto FROM preNominas INNER JOIN " & _
                               "empleado ON preNominas.idEmpleado = empleado.id INNER JOIN tiendas ON preNominas.tienda = tiendas.NUMERO INNER JOIN " & _
                               "deptoPuesto ON empleado.Puesto = deptoPuesto.id WHERE (preNominas.id = " & GridView3.GetRowCellValue(e.RowHandle, "id") & ")"
        Dim ds As DataSet = bdBase.bdDataset(conexion, sQuery)
        GridControl4.DataSource = ds.Tables(0)
        GridView4.PopulateColumns()
        GridView4.BestFitColumns()
        If GridView4.RowCount > 0 Then btExpPP.Visible = True Else btExpPP.Visible = False
        Splash(False)
    End Sub

    Private Sub btExpPP_Click(sender As System.Object, e As System.EventArgs) Handles btExpPP.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl4)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub Empleados_Layout(sender As System.Object, e As System.Windows.Forms.LayoutEventArgs) Handles MyBase.Layout
        'IdiomaTextos(Me)
    End Sub
    Private Sub txClave_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txClave.LostFocus
        
    End Sub
End Class