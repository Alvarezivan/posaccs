﻿Imports System.Windows.Forms.DataVisualization.Charting
Imports System
Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports System.Collections.Generic
Imports System.Data
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel

Public Class Zonas
    '    Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    'oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '";password=" & oConfig.pClave

    'Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
    Private Sub RadioGroup1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioGroup1.SelectedIndexChanged
        Select Case RadioGroup1.SelectedIndex
            Case 0
                reloadCatalogo("zonacobranza", "nombre")
                LabelControl2.Text = "Zona Cobranza"
            Case 1
                reloadCatalogo("zonatiendas", "nombre")
                LabelControl2.Text = "Zona Tiendas"
            Case 2
                reloadCatalogo("zonaventas", "nombre")
                LabelControl2.Text = "Zona Vendedores"
        End Select
    End Sub

    Private Sub reloadCatalogo(ByVal tabla As String, ByVal campo As String)
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        GridControl1.DataSource = Nothing
        Dim qry As String = "Select Numero, " & campo & " as Nombre from " & tabla & "    order by " & campo
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        If Not ds Is Nothing Then
            GridControl1.DataSource = ds.Tables(0)
            'DataGridView2.Columns(0).Visible = False
        Else
        End If
        GridControl1.DataSource = ds.Tables(0)
        GridView1.PopulateColumns()
        GridView1.HorzScrollVisibility = True
        GridView1.VertScrollVisibility = True
        GridView1.BestFitColumns()
        For x As Integer = 0 To 1
            GridView1.Columns(x).OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Next
        'GridControl1.HasChildren
        GridView1.Columns(0).OptionsColumn.AllowEdit = False
        Me.Cursor = Cursors.Default
    End Sub

    'Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click



        If String.IsNullOrWhiteSpace(TextBox17.Text) Then
            TextBox17.Focus()
            Exit Sub
        End If
        Dim qry As String
        Dim tabla As String = ""
        Dim campo As String = String.Empty
        'Dim CualSP As String = ""
        Dim sQuery As String = String.Empty
        Dim cmd As New SqlCommand
        Select Case RadioGroup1.SelectedIndex
            Case 0
                campo = "nombre"
                tabla = "zonacobranza"
                'vCualSP = "InsertaZonaCobranza"
                sQuery = "INSERT INTO [ZonaCobranza] ([NOMBRE], [BORRADO]) VALUES (@NOMBRE, 0) " & vbCrLf & _
                         "update ZonaCobranza set NUMERO=(select MAX(id) from ZonaCobranza) where id=(select MAX(id) from ZonaCobranza)"
            Case 1
                campo = "nombre"
                tabla = "zonatiendas"
                'CualSP = "InsertaZonaTiendas"
                sQuery = "INSERT INTO [ZonaTiendas] ([NOMBRE], [BORRADO]) VALUES (@NOMBRE, 0) " & vbCrLf & _
                         "update ZonaTiendas set NUMERO=(select MAX(id) from ZonaTiendas) where id=(select MAX(id) from ZonaTiendas)"
            Case 2
                campo = "nombre"
                tabla = "zonaventas"
                'CualSP = "InsertaZonaVentas"
                sQuery = "INSERT INTO [ZonaVentas] ([NOMBRE], [BORRADO]) VALUES (@NOMBRE, 0) " & vbCrLf & _
                         "update ZonaVentas set NUMERO=(select MAX(id) from ZonaVentas) where id=(select MAX(id) from ZonaVentas)"
        End Select

        If RadioGroup1.SelectedIndex = -1 Then

            MsgBox("Debe seleccionar el Tipo de Zona", MsgBoxStyle.Information)

        Else

            cmd.CommandText = sQuery
            cmd.Parameters.Add(New SqlParameter("NOMBRE", SqlDbType.VarChar)).Value = TextBox17.Text.ToString.Trim.ToUpper

            qry = String.Format("Select numero from {0} where {1} = '{2}'", tabla, campo, TextBox17.Text.ToString.Trim.ToUpper)
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If dr2.HasRows Then
                dr2.Close()
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(705, Mensaje.Texto), IdiomaMensajes(705, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            dr2.Close()

            'qry = "exec " & CualSP & " '" & TextBox17.Text.ToString.Trim.ToUpper & "'"
            'bdBase.bdExecute(conexion, qry)
            Dim regresa As String = bdBase.bdExecute(conexion, cmd)
            If String.IsNullOrWhiteSpace(regresa) Then
                reloadCatalogo(tabla, campo)
                TextBox17.Text = ""
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(692, Mensaje.Texto), IdiomaMensajes(692, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(706, Mensaje.Texto), IdiomaMensajes(706, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub

    'Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl1)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub Zonas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)

        RegistraAcceso(conexion, "Utilerías, Zonas [zonas.frm]")



    End Sub
End Class