﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RazonesSociales
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RazonesSociales))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Button1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBox7 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Button4 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button3 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button2 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.TextBox5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.Textbox1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBox1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBox2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.textbox7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextBox14 = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBox4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBox3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextBox2 = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.ComboBox7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextBox5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Textbox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textbox7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBox3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "imágenes (*.jpg) | *jpg"
        Me.OpenFileDialog1.Title = "Buscar Imágenes"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(2, 21)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(609, 243)
        Me.GridControl1.TabIndex = 92
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView1.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView1.OptionsCustomization.AllowRowSizing = True
        Me.GridView1.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView1.OptionsFind.AlwaysVisible = True
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(9, 290)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(613, 266)
        Me.GroupControl1.TabIndex = 93
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(432, 165)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(150, 23)
        Me.Button1.TabIndex = 103
        Me.Button1.Text = "&Guardar "
        '
        'ComboBox7
        '
        Me.ComboBox7.Location = New System.Drawing.Point(5, 29)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox7.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox7.Size = New System.Drawing.Size(373, 20)
        Me.ComboBox7.TabIndex = 104
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(399, 26)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 105
        Me.Button4.Text = "Mostrar"
        '
        'Button3
        '
        Me.Button3.Enabled = False
        Me.Button3.Location = New System.Drawing.Point(504, 26)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 106
        Me.Button3.Text = "Modificar"
        Me.Button3.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(9, 165)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(150, 23)
        Me.Button2.TabIndex = 107
        Me.Button2.Text = "&Imprimir Exportar"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl3)
        Me.PanelControl1.Controls.Add(Me.GroupControl2)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(627, 560)
        Me.PanelControl1.TabIndex = 108
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.TextBox5)
        Me.GroupControl3.Controls.Add(Me.LabelControl10)
        Me.GroupControl3.Controls.Add(Me.Button2)
        Me.GroupControl3.Controls.Add(Me.LabelControl9)
        Me.GroupControl3.Controls.Add(Me.Button1)
        Me.GroupControl3.Controls.Add(Me.Textbox1)
        Me.GroupControl3.Controls.Add(Me.LabelControl8)
        Me.GroupControl3.Controls.Add(Me.LabelControl7)
        Me.GroupControl3.Controls.Add(Me.LabelControl6)
        Me.GroupControl3.Controls.Add(Me.LabelControl5)
        Me.GroupControl3.Controls.Add(Me.ComboBox1)
        Me.GroupControl3.Controls.Add(Me.ComboBox2)
        Me.GroupControl3.Controls.Add(Me.LabelControl4)
        Me.GroupControl3.Controls.Add(Me.LabelControl3)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.textbox7)
        Me.GroupControl3.Controls.Add(Me.TextBox6)
        Me.GroupControl3.Controls.Add(Me.TextBox14)
        Me.GroupControl3.Controls.Add(Me.ComboBox4)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Controls.Add(Me.ComboBox3)
        Me.GroupControl3.Controls.Add(Me.TextBox2)
        Me.GroupControl3.Location = New System.Drawing.Point(11, 76)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(600, 192)
        Me.GroupControl3.TabIndex = 109
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(323, 48)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox5.Size = New System.Drawing.Size(259, 20)
        Me.TextBox5.TabIndex = 116
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(375, 120)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(44, 13)
        Me.LabelControl10.TabIndex = 112
        Me.LabelControl10.Text = "Contacto"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(254, 120)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(20, 13)
        Me.LabelControl9.TabIndex = 111
        Me.LabelControl9.Text = "RFC"
        '
        'Textbox1
        '
        Me.Textbox1.Location = New System.Drawing.Point(9, 139)
        Me.Textbox1.Name = "Textbox1"
        Me.Textbox1.Properties.Mask.EditMask = "f0"
        Me.Textbox1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.Textbox1.Properties.MaxLength = 10
        Me.Textbox1.Size = New System.Drawing.Size(100, 20)
        Me.Textbox1.TabIndex = 113
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(126, 120)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(18, 13)
        Me.LabelControl8.TabIndex = 110
        Me.LabelControl8.Text = "Fax"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 120)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(42, 13)
        Me.LabelControl7.TabIndex = 109
        Me.LabelControl7.Text = "Telefono"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(471, 74)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(65, 13)
        Me.LabelControl6.TabIndex = 108
        Me.LabelControl6.Text = "Código Postal"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(264, 75)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(35, 13)
        Me.LabelControl5.TabIndex = 107
        Me.LabelControl5.Text = "Colonia"
        '
        'ComboBox1
        '
        Me.ComboBox1.Location = New System.Drawing.Point(8, 92)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox1.Size = New System.Drawing.Size(122, 20)
        Me.ComboBox1.TabIndex = 117
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(136, 92)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox2.Size = New System.Drawing.Size(122, 20)
        Me.ComboBox2.TabIndex = 118
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(136, 75)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl4.TabIndex = 106
        Me.LabelControl4.Text = "Ciudad"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(7, 75)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
        Me.LabelControl3.TabIndex = 105
        Me.LabelControl3.Text = "Estado"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(323, 32)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl2.TabIndex = 104
        Me.LabelControl2.Text = "Calle y número"
        '
        'textbox7
        '
        Me.textbox7.Location = New System.Drawing.Point(126, 139)
        Me.textbox7.Name = "textbox7"
        Me.textbox7.Properties.Mask.EditMask = "f0"
        Me.textbox7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.textbox7.Properties.MaxLength = 10
        Me.textbox7.Size = New System.Drawing.Size(100, 20)
        Me.textbox7.TabIndex = 114
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(254, 139)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox6.TabIndex = 121
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(375, 139)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox14.Size = New System.Drawing.Size(207, 20)
        Me.TextBox14.TabIndex = 122
        '
        'ComboBox4
        '
        Me.ComboBox4.Location = New System.Drawing.Point(471, 92)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox4.Size = New System.Drawing.Size(111, 20)
        Me.ComboBox4.TabIndex = 120
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(8, 32)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(60, 13)
        Me.LabelControl1.TabIndex = 103
        Me.LabelControl1.Text = "Razón Social"
        '
        'ComboBox3
        '
        Me.ComboBox3.Location = New System.Drawing.Point(264, 92)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox3.Size = New System.Drawing.Size(170, 20)
        Me.ComboBox3.TabIndex = 119
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(8, 48)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Size = New System.Drawing.Size(309, 20)
        Me.TextBox2.TabIndex = 115
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.Button3)
        Me.GroupControl2.Controls.Add(Me.ComboBox7)
        Me.GroupControl2.Controls.Add(Me.Button4)
        Me.GroupControl2.Location = New System.Drawing.Point(9, 12)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(602, 58)
        Me.GroupControl2.TabIndex = 108
        Me.GroupControl2.Text = "Localizar y mostrar para edición"
        '
        'RazonesSociales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(627, 560)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.PanelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RazonesSociales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Control de Razones Sociales"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.ComboBox7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.TextBox5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Textbox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textbox7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBox3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Button1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBox7 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents Button2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Button4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextBox5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Textbox1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBox1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBox2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents textbox7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextBox14 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboBox4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBox3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextBox2 As DevExpress.XtraEditors.TextEdit
End Class
