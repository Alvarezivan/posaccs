﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Drawing2D

Public Class Plazos

    Private Sub Plazos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)

        Me.Text = " [Plazos] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        carga()
        Splash(False)
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click

        Dim a, p As Boolean
        Dim fechadel, fechal, qry As String
        fechadel = Date.Parse(dtinicial.Value).Date.ToString("yyyyMMdd")
        fechal = Date.Parse(dtfinal.Value).Date.ToString("yyyyMMdd")
        Dim empleado As Integer = oLogin.pUserId

        If tbnombre.Text = "" Then
            MsgBox("Debe asignar Nombre", MsgBoxStyle.Information)
            tbnombre.Focus()
            Exit Sub
        ElseIf tbplazo.Text = "" Then
            MsgBox("Debe asignar un Plazo en dias", MsgBoxStyle.Information)
            tbplazo.Focus()
            Exit Sub
        ElseIf tblibre.Text = "" Then
            MsgBox("Debe asignar los dias libre de interes", MsgBoxStyle.Information)
            tblibre.Focus()
            Exit Sub
        ElseIf tbpagos.Text = "" Then
            MsgBox("Debe asignar el No. de pagos", MsgBoxStyle.Information)
            tbpagos.Focus()
            Exit Sub
        ElseIf tbinteres.Text = "" Then
            MsgBox("Debe asiganar la tasa de interes", MsgBoxStyle.Information)
            tbinteres.Focus()
            Exit Sub
        ElseIf tbminimo.Text = "" Then
            MsgBox("Debe asignar un minimo de venta", MsgBoxStyle.Information)
            tbminimo.Focus()
            Exit Sub
        End If

        If chkactivo.Checked Then
            a = 1
        Else
            a = 0
        End If

        If chkpermanente.Checked Then
            p = 1
        Else
            p = 0
        End If

        qry = "Insert into [plazos] ([NOMBRE], [plazodias], [diaslibre], [pagos],[interes],[minimoventa], [fechainicial], [fechafinal], [permanente], [activo], " & _
            "[usuarioalta]) values ('" & tbnombre.Text.Trim.ToUpper & "', '" _
            & tbplazo.Text.Trim & "', '" & tblibre.Text.Trim & "', '" & tbpagos.Text.Trim.ToUpper & "','" & tbinteres.Text.Trim & "','" & tbminimo.Text.Trim.ToUpper & "', '" _
            & fechadel & "', '" & fechal & "', '" & p & "', '" & a & "', '" _
             & empleado & "')"
        regresa = bdBase.bdExecute(conexion, qry)

        If String.IsNullOrEmpty(regresa) Then
            MsgBox("El plazo ha sido guardado con exito", MsgBoxStyle.Information)
            carga()
            XtraTabControl1.SelectedTabPage = XtraTabPage2
        End If

    End Sub

    Private Sub carga()
        Dim qry As String = " select Nombre,PlazoDias,DiasLibre,Pagos,Interes,MinimoVenta,FechaInicial,FechaFinal,Permanente,Activo from plazos "
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        'GridView2.Columns("Unidades").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.HorzScrollVisibility = True
        GridView2.VertScrollVisibility = True
        GridView2.BestFitColumns()
    End Sub

 
End Class