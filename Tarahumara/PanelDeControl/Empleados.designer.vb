﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Empleados
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblMail = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txSueldo = New DevExpress.XtraEditors.TextEdit()
        Me.nuCComison = New DevExpress.XtraEditors.SpinEdit()
        Me.nuPComision = New DevExpress.XtraEditors.SpinEdit()
        Me.cbCobranza = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbRuta = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ckActivo = New DevExpress.XtraEditors.CheckEdit()
        Me.txClave = New DevExpress.XtraEditors.TextEdit()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.TimeEdit2 = New DevExpress.XtraEditors.TimeEdit()
        Me.TimeEdit1 = New DevExpress.XtraEditors.TimeEdit()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txBono = New DevExpress.XtraEditors.TextEdit()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btRegistraHuella = New DevExpress.XtraEditors.SimpleButton()
        Me.txUsuario = New DevExpress.XtraEditors.TextEdit()
        Me.txCompV = New DevExpress.XtraEditors.TextEdit()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txPrima = New DevExpress.XtraEditors.TextEdit()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.deVAl = New DevExpress.XtraEditors.DateEdit()
        Me.deVDel = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.lblAntiguedad = New DevExpress.XtraEditors.LabelControl()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.rgTurno = New DevExpress.XtraEditors.RadioGroup()
        Me.rgTipoEmp = New DevExpress.XtraEditors.RadioGroup()
        Me.txCompensa = New DevExpress.XtraEditors.TextEdit()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txInfonavit = New DevExpress.XtraEditors.TextEdit()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cbDepartamento = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.cbDescanso = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.btPermisos = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.cbTienda = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboBox7 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.btModificar = New DevExpress.XtraEditors.SimpleButton()
        Me.btMostrar = New DevExpress.XtraEditors.SimpleButton()
        Me.btGuardar = New DevExpress.XtraEditors.SimpleButton()
        Me.btImagen = New DevExpress.XtraEditors.SimpleButton()
        Me.txClabe = New DevExpress.XtraEditors.TextEdit()
        Me.txNomina = New DevExpress.XtraEditors.TextEdit()
        Me.txImss = New DevExpress.XtraEditors.TextEdit()
        Me.txeMail = New DevExpress.XtraEditors.TextEdit()
        Me.txCurp = New DevExpress.XtraEditors.TextEdit()
        Me.txRfc = New DevExpress.XtraEditors.TextEdit()
        Me.deIngreso = New DevExpress.XtraEditors.DateEdit()
        Me.deNacimiento = New DevExpress.XtraEditors.DateEdit()
        Me.txTel2 = New DevExpress.XtraEditors.TextEdit()
        Me.txTel1 = New DevExpress.XtraEditors.TextEdit()
        Me.cbPC = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbColonia = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbCiudad = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.cbEstado = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.txCalle = New DevExpress.XtraEditors.TextEdit()
        Me.txNombre = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.btImprimir = New DevExpress.XtraEditors.SimpleButton()
        Me.ckFotos = New DevExpress.XtraEditors.CheckEdit()
        Me.btMostrarR = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ckTodasT = New DevExpress.XtraEditors.CheckEdit()
        Me.dePDel = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.dePAl = New DevExpress.XtraEditors.DateEdit()
        Me.cbTiendasP = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.btCalcular = New DevExpress.XtraEditors.SimpleButton()
        Me.btGuardaPre = New DevExpress.XtraEditors.SimpleButton()
        Me.btExportP = New DevExpress.XtraEditors.SimpleButton()
        Me.ckExtras = New DevExpress.XtraEditors.CheckEdit()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.lblMsj = New DevExpress.XtraEditors.LabelControl()
        Me.btBuscaP = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.ckTodasP = New DevExpress.XtraEditors.CheckEdit()
        Me.deDelP = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.deAlP = New DevExpress.XtraEditors.DateEdit()
        Me.cbTiendaPP = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.GridControl4 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.btExpPP = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txSueldo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuCComison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nuPComision.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCobranza.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbRuta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckActivo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txBono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txUsuario.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCompV.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txPrima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deVAl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deVAl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deVDel.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deVDel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTurno.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgTipoEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCompensa.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txInfonavit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbDepartamento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbDescanso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTienda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.ComboBox7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txClabe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNomina.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txImss.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txeMail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCurp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txRfc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIngreso.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deIngreso.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deNacimiento.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deNacimiento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTel2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTel1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbColonia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCiudad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCalle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txNombre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ckFotos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckTodasT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dePDel.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dePDel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dePAl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dePAl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTiendasP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckExtras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.ckTodasP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDelP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deDelP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deAlP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deAlP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbTiendaPP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(59, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(92, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Nombre Completo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(59, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Estado"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(201, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Ciudad"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(336, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Colonia"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(471, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Código Postal"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(375, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Calle y número"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(305, 119)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(27, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "RFC"
        '
        'lblMail
        '
        Me.lblMail.AutoSize = True
        Me.lblMail.Location = New System.Drawing.Point(62, 358)
        Me.lblMail.Name = "lblMail"
        Me.lblMail.Size = New System.Drawing.Size(78, 13)
        Me.lblMail.TabIndex = 33
        Me.lblMail.Text = "Usuario (eMail)"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(723, 64)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "Fecha Nacimiento"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(364, 173)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 13)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "Sueldo Diario"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(432, 119)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(30, 13)
        Me.Label20.TabIndex = 13
        Me.Label20.Text = "Curp"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(583, 119)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 13)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "IMSS"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(59, 119)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Telefono"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(182, 119)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 13)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Telefono2"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(607, 64)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(76, 13)
        Me.Label29.TabIndex = 7
        Me.Label29.Text = "Fecha Ingreso"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(59, 173)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(80, 13)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Cuenta Nómina"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(165, 173)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(72, 13)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Cuenta Clabe"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(705, 111)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(121, 150)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 89
        Me.PictureBox1.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "imágenes (*.jpg) | *jpg"
        Me.OpenFileDialog1.Title = "Buscar Imágenes"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'txSueldo
        '
        Me.txSueldo.EditValue = "0"
        Me.txSueldo.Location = New System.Drawing.Point(367, 189)
        Me.txSueldo.Name = "txSueldo"
        Me.txSueldo.Properties.Mask.EditMask = "c"
        Me.txSueldo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txSueldo.Size = New System.Drawing.Size(87, 20)
        Me.txSueldo.TabIndex = 52
        '
        'nuCComison
        '
        Me.nuCComison.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nuCComison.Location = New System.Drawing.Point(579, 272)
        Me.nuCComison.Name = "nuCComison"
        Me.nuCComison.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nuCComison.Size = New System.Drawing.Size(74, 20)
        Me.nuCComison.TabIndex = 63
        '
        'nuPComision
        '
        Me.nuPComision.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nuPComision.Location = New System.Drawing.Point(495, 272)
        Me.nuPComision.Name = "nuPComision"
        Me.nuPComision.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.nuPComision.Size = New System.Drawing.Size(76, 20)
        Me.nuPComision.TabIndex = 62
        '
        'cbCobranza
        '
        Me.cbCobranza.Location = New System.Drawing.Point(344, 272)
        Me.cbCobranza.Name = "cbCobranza"
        Me.cbCobranza.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCobranza.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCobranza.Size = New System.Drawing.Size(142, 20)
        Me.cbCobranza.TabIndex = 61
        '
        'cbRuta
        '
        Me.cbRuta.Location = New System.Drawing.Point(206, 272)
        Me.cbRuta.Name = "cbRuta"
        Me.cbRuta.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbRuta.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbRuta.Size = New System.Drawing.Size(128, 20)
        Me.cbRuta.TabIndex = 60
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(577, 257)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(69, 13)
        Me.Label32.TabIndex = 25
        Me.Label32.Text = "Com. Unid. $"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(491, 257)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(71, 13)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Comisión (%)"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(340, 257)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(80, 13)
        Me.Label36.TabIndex = 27
        Me.Label36.Text = "Zona Cobranza"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(203, 256)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(58, 13)
        Me.Label23.TabIndex = 26
        Me.Label23.Text = "Zona/Ruta"
        '
        'ckActivo
        '
        Me.ckActivo.EditValue = True
        Me.ckActivo.Location = New System.Drawing.Point(339, 375)
        Me.ckActivo.Name = "ckActivo"
        Me.ckActivo.Properties.Caption = "Activo"
        Me.ckActivo.Size = New System.Drawing.Size(75, 19)
        Me.ckActivo.TabIndex = 71
        '
        'txClave
        '
        Me.txClave.Location = New System.Drawing.Point(229, 375)
        Me.txClave.Name = "txClave"
        Me.txClave.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClave.Size = New System.Drawing.Size(88, 20)
        Me.txClave.TabIndex = 70
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(227, 358)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(53, 13)
        Me.Label31.TabIndex = 34
        Me.Label31.Text = "Password"
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage3
        Me.XtraTabControl2.Size = New System.Drawing.Size(894, 542)
        Me.XtraTabControl2.TabIndex = 100
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage1})
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.TimeEdit2)
        Me.XtraTabPage3.Controls.Add(Me.TimeEdit1)
        Me.XtraTabPage3.Controls.Add(Me.Label25)
        Me.XtraTabPage3.Controls.Add(Me.Label26)
        Me.XtraTabPage3.Controls.Add(Me.txBono)
        Me.XtraTabPage3.Controls.Add(Me.Label12)
        Me.XtraTabPage3.Controls.Add(Me.btRegistraHuella)
        Me.XtraTabPage3.Controls.Add(Me.txUsuario)
        Me.XtraTabPage3.Controls.Add(Me.txCompV)
        Me.XtraTabPage3.Controls.Add(Me.Label21)
        Me.XtraTabPage3.Controls.Add(Me.txPrima)
        Me.XtraTabPage3.Controls.Add(Me.Label24)
        Me.XtraTabPage3.Controls.Add(Me.deVAl)
        Me.XtraTabPage3.Controls.Add(Me.deVDel)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl6)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl5)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl4)
        Me.XtraTabPage3.Controls.Add(Me.lblAntiguedad)
        Me.XtraTabPage3.Controls.Add(Me.Label18)
        Me.XtraTabPage3.Controls.Add(Me.Label14)
        Me.XtraTabPage3.Controls.Add(Me.rgTurno)
        Me.XtraTabPage3.Controls.Add(Me.rgTipoEmp)
        Me.XtraTabPage3.Controls.Add(Me.txCompensa)
        Me.XtraTabPage3.Controls.Add(Me.Label11)
        Me.XtraTabPage3.Controls.Add(Me.txInfonavit)
        Me.XtraTabPage3.Controls.Add(Me.Label10)
        Me.XtraTabPage3.Controls.Add(Me.nuCComison)
        Me.XtraTabPage3.Controls.Add(Me.ckActivo)
        Me.XtraTabPage3.Controls.Add(Me.nuPComision)
        Me.XtraTabPage3.Controls.Add(Me.cbDepartamento)
        Me.XtraTabPage3.Controls.Add(Me.cbCobranza)
        Me.XtraTabPage3.Controls.Add(Me.txClave)
        Me.XtraTabPage3.Controls.Add(Me.cbRuta)
        Me.XtraTabPage3.Controls.Add(Me.Label32)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl3)
        Me.XtraTabPage3.Controls.Add(Me.Label19)
        Me.XtraTabPage3.Controls.Add(Me.Label31)
        Me.XtraTabPage3.Controls.Add(Me.Label36)
        Me.XtraTabPage3.Controls.Add(Me.cbDescanso)
        Me.XtraTabPage3.Controls.Add(Me.Label23)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl2)
        Me.XtraTabPage3.Controls.Add(Me.btPermisos)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl1)
        Me.XtraTabPage3.Controls.Add(Me.cbTienda)
        Me.XtraTabPage3.Controls.Add(Me.GroupControl4)
        Me.XtraTabPage3.Controls.Add(Me.btGuardar)
        Me.XtraTabPage3.Controls.Add(Me.btImagen)
        Me.XtraTabPage3.Controls.Add(Me.txClabe)
        Me.XtraTabPage3.Controls.Add(Me.txNomina)
        Me.XtraTabPage3.Controls.Add(Me.txImss)
        Me.XtraTabPage3.Controls.Add(Me.txeMail)
        Me.XtraTabPage3.Controls.Add(Me.txCurp)
        Me.XtraTabPage3.Controls.Add(Me.txRfc)
        Me.XtraTabPage3.Controls.Add(Me.deIngreso)
        Me.XtraTabPage3.Controls.Add(Me.deNacimiento)
        Me.XtraTabPage3.Controls.Add(Me.txTel2)
        Me.XtraTabPage3.Controls.Add(Me.txTel1)
        Me.XtraTabPage3.Controls.Add(Me.cbPC)
        Me.XtraTabPage3.Controls.Add(Me.cbColonia)
        Me.XtraTabPage3.Controls.Add(Me.cbCiudad)
        Me.XtraTabPage3.Controls.Add(Me.cbEstado)
        Me.XtraTabPage3.Controls.Add(Me.txCalle)
        Me.XtraTabPage3.Controls.Add(Me.txNombre)
        Me.XtraTabPage3.Controls.Add(Me.PictureBox1)
        Me.XtraTabPage3.Controls.Add(Me.txSueldo)
        Me.XtraTabPage3.Controls.Add(Me.Label17)
        Me.XtraTabPage3.Controls.Add(Me.Label16)
        Me.XtraTabPage3.Controls.Add(Me.Label2)
        Me.XtraTabPage3.Controls.Add(Me.Label3)
        Me.XtraTabPage3.Controls.Add(Me.Label29)
        Me.XtraTabPage3.Controls.Add(Me.Label4)
        Me.XtraTabPage3.Controls.Add(Me.Label5)
        Me.XtraTabPage3.Controls.Add(Me.Label9)
        Me.XtraTabPage3.Controls.Add(Me.Label6)
        Me.XtraTabPage3.Controls.Add(Me.Label7)
        Me.XtraTabPage3.Controls.Add(Me.Label1)
        Me.XtraTabPage3.Controls.Add(Me.Label8)
        Me.XtraTabPage3.Controls.Add(Me.lblMail)
        Me.XtraTabPage3.Controls.Add(Me.Label13)
        Me.XtraTabPage3.Controls.Add(Me.Label15)
        Me.XtraTabPage3.Controls.Add(Me.Label22)
        Me.XtraTabPage3.Controls.Add(Me.Label20)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(888, 514)
        Me.XtraTabPage3.Text = "Control de Empleados"
        '
        'TimeEdit2
        '
        Me.TimeEdit2.EditValue = New Date(2013, 9, 26, 21, 0, 0, 0)
        Me.TimeEdit2.Location = New System.Drawing.Point(554, 233)
        Me.TimeEdit2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TimeEdit2.Name = "TimeEdit2"
        Me.TimeEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit2.Size = New System.Drawing.Size(102, 20)
        Me.TimeEdit2.TabIndex = 59
        '
        'TimeEdit1
        '
        Me.TimeEdit1.EditValue = New Date(2013, 9, 26, 9, 0, 0, 0)
        Me.TimeEdit1.Location = New System.Drawing.Point(435, 233)
        Me.TimeEdit1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TimeEdit1.Name = "TimeEdit1"
        Me.TimeEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.TimeEdit1.Size = New System.Drawing.Size(102, 20)
        Me.TimeEdit1.TabIndex = 58
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(551, 217)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(61, 13)
        Me.Label25.TabIndex = 188
        Me.Label25.Text = "Hora Salida"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(433, 217)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(71, 13)
        Me.Label26.TabIndex = 187
        Me.Label26.Text = "Hora Entrada"
        '
        'txBono
        '
        Me.txBono.EditValue = "0"
        Me.txBono.Location = New System.Drawing.Point(593, 323)
        Me.txBono.Name = "txBono"
        Me.txBono.Properties.Mask.EditMask = "c"
        Me.txBono.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txBono.Size = New System.Drawing.Size(87, 20)
        Me.txBono.TabIndex = 68
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(591, 307)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 91
        Me.Label12.Text = "Bono $"
        '
        'btRegistraHuella
        '
        Me.btRegistraHuella.Enabled = False
        Me.btRegistraHuella.Location = New System.Drawing.Point(536, 358)
        Me.btRegistraHuella.Name = "btRegistraHuella"
        Me.btRegistraHuella.Size = New System.Drawing.Size(150, 23)
        Me.btRegistraHuella.TabIndex = 90
        Me.btRegistraHuella.Text = "Registrar huella digital"
        '
        'txUsuario
        '
        Me.txUsuario.Location = New System.Drawing.Point(62, 375)
        Me.txUsuario.Name = "txUsuario"
        Me.txUsuario.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txUsuario.Size = New System.Drawing.Size(146, 20)
        Me.txUsuario.TabIndex = 69
        '
        'txCompV
        '
        Me.txCompV.EditValue = "0"
        Me.txCompV.Location = New System.Drawing.Point(488, 323)
        Me.txCompV.Name = "txCompV"
        Me.txCompV.Properties.Mask.EditMask = "c"
        Me.txCompV.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txCompV.Size = New System.Drawing.Size(87, 20)
        Me.txCompV.TabIndex = 67
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(484, 307)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(91, 13)
        Me.Label21.TabIndex = 32
        Me.Label21.Text = "Comp. Vacacional"
        '
        'txPrima
        '
        Me.txPrima.EditValue = "0"
        Me.txPrima.Location = New System.Drawing.Point(382, 323)
        Me.txPrima.Name = "txPrima"
        Me.txPrima.Properties.Mask.EditMask = "c"
        Me.txPrima.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txPrima.Size = New System.Drawing.Size(87, 20)
        Me.txPrima.TabIndex = 66
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(380, 307)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(86, 13)
        Me.Label24.TabIndex = 31
        Me.Label24.Text = "Prima vacacional"
        '
        'deVAl
        '
        Me.deVAl.EditValue = Nothing
        Me.deVAl.Location = New System.Drawing.Point(287, 323)
        Me.deVAl.Name = "deVAl"
        Me.deVAl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deVAl.Properties.DisplayFormat.FormatString = "dd-MMM"
        Me.deVAl.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deVAl.Properties.EditFormat.FormatString = "dd-MMM"
        Me.deVAl.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deVAl.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deVAl.Size = New System.Drawing.Size(75, 20)
        Me.deVAl.TabIndex = 65
        '
        'deVDel
        '
        Me.deVDel.EditValue = Nothing
        Me.deVDel.Location = New System.Drawing.Point(206, 323)
        Me.deVDel.Name = "deVDel"
        Me.deVDel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deVDel.Properties.DisplayFormat.FormatString = "dd-MMM"
        Me.deVDel.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deVDel.Properties.EditFormat.FormatString = "dd-MMM"
        Me.deVDel.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deVDel.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deVDel.Size = New System.Drawing.Size(75, 20)
        Me.deVDel.TabIndex = 64
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 7.25!)
        Me.LabelControl6.Location = New System.Drawing.Point(321, 310)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(9, 12)
        Me.LabelControl6.TabIndex = 30
        Me.LabelControl6.Text = "Al"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 7.25!)
        Me.LabelControl5.Location = New System.Drawing.Point(237, 310)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(14, 12)
        Me.LabelControl5.TabIndex = 29
        Me.LabelControl5.Text = "Del"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(254, 294)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(53, 13)
        Me.LabelControl4.TabIndex = 28
        Me.LabelControl4.Text = "Vacaciones"
        '
        'lblAntiguedad
        '
        Me.lblAntiguedad.Appearance.Font = New System.Drawing.Font("Tahoma", 7.25!)
        Me.lblAntiguedad.Location = New System.Drawing.Point(609, 100)
        Me.lblAntiguedad.Name = "lblAntiguedad"
        Me.lblAntiguedad.Size = New System.Drawing.Size(67, 12)
        Me.lblAntiguedad.TabIndex = 8
        Me.lblAntiguedad.Text = "Sin antigüedad"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(59, 218)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(35, 13)
        Me.Label18.TabIndex = 21
        Me.Label18.Text = "Turno"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(550, 167)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(76, 13)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "Tipo empleado"
        '
        'rgTurno
        '
        Me.rgTurno.Location = New System.Drawing.Point(62, 231)
        Me.rgTurno.Name = "rgTurno"
        Me.rgTurno.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Completo"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "½ Turno"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Fin de Semana")})
        Me.rgTurno.Size = New System.Drawing.Size(128, 89)
        Me.rgTurno.TabIndex = 55
        '
        'rgTipoEmp
        '
        Me.rgTipoEmp.Location = New System.Drawing.Point(553, 180)
        Me.rgTipoEmp.Name = "rgTipoEmp"
        Me.rgTipoEmp.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Fijo"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Temporal")})
        Me.rgTipoEmp.Size = New System.Drawing.Size(133, 29)
        Me.rgTipoEmp.TabIndex = 54
        '
        'txCompensa
        '
        Me.txCompensa.EditValue = "0"
        Me.txCompensa.Location = New System.Drawing.Point(460, 189)
        Me.txCompensa.Name = "txCompensa"
        Me.txCompensa.Properties.Mask.EditMask = "c"
        Me.txCompensa.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.txCompensa.Size = New System.Drawing.Size(87, 20)
        Me.txCompensa.TabIndex = 53
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(457, 173)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Comp. Diaria"
        '
        'txInfonavit
        '
        Me.txInfonavit.Location = New System.Drawing.Point(274, 189)
        Me.txInfonavit.Name = "txInfonavit"
        Me.txInfonavit.Size = New System.Drawing.Size(87, 20)
        Me.txInfonavit.TabIndex = 51
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(271, 173)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(89, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Cuenta Infonavit"
        '
        'cbDepartamento
        '
        Me.cbDepartamento.Location = New System.Drawing.Point(321, 233)
        Me.cbDepartamento.Name = "cbDepartamento"
        Me.cbDepartamento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbDepartamento.Size = New System.Drawing.Size(100, 20)
        Me.cbDepartamento.TabIndex = 57
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(321, 218)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Departamento"
        '
        'cbDescanso
        '
        Me.cbDescanso.Location = New System.Drawing.Point(206, 233)
        Me.cbDescanso.Name = "cbDescanso"
        Me.cbDescanso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbDescanso.Properties.Items.AddRange(New Object() {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"})
        Me.cbDescanso.Size = New System.Drawing.Size(100, 20)
        Me.cbDescanso.TabIndex = 56
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(206, 218)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(78, 13)
        Me.LabelControl2.TabIndex = 22
        Me.LabelControl2.Text = "Día de descanso"
        '
        'btPermisos
        '
        Me.btPermisos.Location = New System.Drawing.Point(62, 429)
        Me.btPermisos.Name = "btPermisos"
        Me.btPermisos.Size = New System.Drawing.Size(202, 36)
        Me.btPermisos.TabIndex = 71
        Me.btPermisos.Text = "Permisos y Perfiles"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(650, 21)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl1.TabIndex = 2
        Me.LabelControl1.Text = "Tienda"
        '
        'cbTienda
        '
        Me.cbTienda.Location = New System.Drawing.Point(650, 37)
        Me.cbTienda.Name = "cbTienda"
        Me.cbTienda.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTienda.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTienda.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTienda.Size = New System.Drawing.Size(176, 20)
        Me.cbTienda.TabIndex = 37
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.ComboBox7)
        Me.GroupControl4.Controls.Add(Me.btModificar)
        Me.GroupControl4.Controls.Add(Me.btMostrar)
        Me.GroupControl4.Location = New System.Drawing.Point(330, 407)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(496, 76)
        Me.GroupControl4.TabIndex = 93
        Me.GroupControl4.Text = "Busqueda para edición"
        '
        'ComboBox7
        '
        Me.ComboBox7.Location = New System.Drawing.Point(5, 38)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBox7.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBox7.Size = New System.Drawing.Size(320, 20)
        Me.ComboBox7.TabIndex = 118
        '
        'btModificar
        '
        Me.btModificar.Location = New System.Drawing.Point(413, 35)
        Me.btModificar.Name = "btModificar"
        Me.btModificar.Size = New System.Drawing.Size(75, 23)
        Me.btModificar.TabIndex = 120
        Me.btModificar.Text = "Modificar"
        '
        'btMostrar
        '
        Me.btMostrar.Location = New System.Drawing.Point(331, 35)
        Me.btMostrar.Name = "btMostrar"
        Me.btMostrar.Size = New System.Drawing.Size(75, 23)
        Me.btMostrar.TabIndex = 119
        Me.btMostrar.Text = "Mostrar"
        '
        'btGuardar
        '
        Me.btGuardar.Location = New System.Drawing.Point(705, 333)
        Me.btGuardar.Name = "btGuardar"
        Me.btGuardar.Size = New System.Drawing.Size(113, 48)
        Me.btGuardar.TabIndex = 92
        Me.btGuardar.Text = "&Guardar"
        '
        'btImagen
        '
        Me.btImagen.Location = New System.Drawing.Point(699, 267)
        Me.btImagen.Name = "btImagen"
        Me.btImagen.Size = New System.Drawing.Size(127, 23)
        Me.btImagen.TabIndex = 91
        Me.btImagen.Text = "Imagen.."
        '
        'txClabe
        '
        Me.txClabe.Location = New System.Drawing.Point(168, 189)
        Me.txClabe.Name = "txClabe"
        Me.txClabe.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txClabe.Size = New System.Drawing.Size(100, 20)
        Me.txClabe.TabIndex = 50
        '
        'txNomina
        '
        Me.txNomina.Location = New System.Drawing.Point(62, 189)
        Me.txNomina.Name = "txNomina"
        Me.txNomina.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNomina.Size = New System.Drawing.Size(100, 20)
        Me.txNomina.TabIndex = 49
        '
        'txImss
        '
        Me.txImss.Location = New System.Drawing.Point(586, 135)
        Me.txImss.Name = "txImss"
        Me.txImss.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txImss.Size = New System.Drawing.Size(100, 20)
        Me.txImss.TabIndex = 48
        '
        'txeMail
        '
        Me.txeMail.Location = New System.Drawing.Point(62, 375)
        Me.txeMail.Name = "txeMail"
        Me.txeMail.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txeMail.Properties.NullValuePrompt = "mail@mail.com"
        Me.txeMail.Properties.NullValuePromptShowForEmptyValue = True
        Me.txeMail.Size = New System.Drawing.Size(146, 20)
        Me.txeMail.TabIndex = 69
        '
        'txCurp
        '
        Me.txCurp.Location = New System.Drawing.Point(435, 135)
        Me.txCurp.Name = "txCurp"
        Me.txCurp.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCurp.Properties.NullValuePrompt = "AAAA123456AAAAAA12"
        Me.txCurp.Properties.NullValuePromptShowForEmptyValue = True
        Me.txCurp.Size = New System.Drawing.Size(124, 20)
        Me.txCurp.TabIndex = 47
        '
        'txRfc
        '
        Me.txRfc.Location = New System.Drawing.Point(308, 135)
        Me.txRfc.Name = "txRfc"
        Me.txRfc.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txRfc.Properties.NullValuePrompt = "XAXX010101000"
        Me.txRfc.Size = New System.Drawing.Size(100, 20)
        Me.txRfc.TabIndex = 46
        '
        'deIngreso
        '
        Me.deIngreso.EditValue = Nothing
        Me.deIngreso.Location = New System.Drawing.Point(610, 80)
        Me.deIngreso.Name = "deIngreso"
        Me.deIngreso.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deIngreso.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deIngreso.Size = New System.Drawing.Size(100, 20)
        Me.deIngreso.TabIndex = 42
        '
        'deNacimiento
        '
        Me.deNacimiento.EditValue = Nothing
        Me.deNacimiento.Location = New System.Drawing.Point(726, 80)
        Me.deNacimiento.Name = "deNacimiento"
        Me.deNacimiento.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deNacimiento.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deNacimiento.Size = New System.Drawing.Size(100, 20)
        Me.deNacimiento.TabIndex = 43
        '
        'txTel2
        '
        Me.txTel2.Location = New System.Drawing.Point(185, 135)
        Me.txTel2.Name = "txTel2"
        Me.txTel2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTel2.Properties.MaxLength = 50
        Me.txTel2.Properties.NullValuePrompt = "1234567890"
        Me.txTel2.Properties.NullValuePromptShowForEmptyValue = True
        Me.txTel2.Size = New System.Drawing.Size(100, 20)
        Me.txTel2.TabIndex = 45
        '
        'txTel1
        '
        Me.txTel1.Location = New System.Drawing.Point(62, 135)
        Me.txTel1.Name = "txTel1"
        Me.txTel1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txTel1.Properties.MaxLength = 50
        Me.txTel1.Properties.NullValuePrompt = "1234567890"
        Me.txTel1.Properties.NullValuePromptShowForEmptyValue = True
        Me.txTel1.Size = New System.Drawing.Size(100, 20)
        Me.txTel1.TabIndex = 44
        '
        'cbPC
        '
        Me.cbPC.Location = New System.Drawing.Point(474, 80)
        Me.cbPC.Name = "cbPC"
        Me.cbPC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPC.Size = New System.Drawing.Size(121, 20)
        Me.cbPC.TabIndex = 41
        '
        'cbColonia
        '
        Me.cbColonia.Location = New System.Drawing.Point(339, 80)
        Me.cbColonia.Name = "cbColonia"
        Me.cbColonia.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbColonia.Size = New System.Drawing.Size(121, 20)
        Me.cbColonia.TabIndex = 40
        '
        'cbCiudad
        '
        Me.cbCiudad.Location = New System.Drawing.Point(204, 80)
        Me.cbCiudad.Name = "cbCiudad"
        Me.cbCiudad.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCiudad.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbCiudad.Size = New System.Drawing.Size(121, 20)
        Me.cbCiudad.TabIndex = 39
        '
        'cbEstado
        '
        Me.cbEstado.Location = New System.Drawing.Point(62, 80)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEstado.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbEstado.Size = New System.Drawing.Size(128, 20)
        Me.cbEstado.TabIndex = 38
        '
        'txCalle
        '
        Me.txCalle.Location = New System.Drawing.Point(378, 37)
        Me.txCalle.Name = "txCalle"
        Me.txCalle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txCalle.Size = New System.Drawing.Size(266, 20)
        Me.txCalle.TabIndex = 36
        '
        'txNombre
        '
        Me.txNombre.Location = New System.Drawing.Point(62, 37)
        Me.txNombre.Name = "txNombre"
        Me.txNombre.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txNombre.Size = New System.Drawing.Size(310, 20)
        Me.txNombre.TabIndex = 35
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.GroupControl3)
        Me.XtraTabPage4.Controls.Add(Me.GroupControl2)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(888, 514)
        Me.XtraTabPage4.Text = "Reporte de Empleados"
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.GridControl1)
        Me.GroupControl3.Location = New System.Drawing.Point(0, 58)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(888, 458)
        Me.GroupControl3.TabIndex = 1
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(2, 22)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(884, 434)
        Me.GridControl1.TabIndex = 93
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView1.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsBehavior.ReadOnly = True
        Me.GridView1.OptionsCustomization.AllowRowSizing = True
        Me.GridView1.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView1.OptionsFind.AlwaysVisible = True
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.btImprimir)
        Me.GroupControl2.Controls.Add(Me.ckFotos)
        Me.GroupControl2.Controls.Add(Me.btMostrarR)
        Me.GroupControl2.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(819, 74)
        Me.GroupControl2.TabIndex = 0
        '
        'btImprimir
        '
        Me.btImprimir.Appearance.Font = New System.Drawing.Font("Calibri", 14.0!)
        Me.btImprimir.Appearance.Options.UseFont = True
        Me.btImprimir.Location = New System.Drawing.Point(613, 26)
        Me.btImprimir.Name = "btImprimir"
        Me.btImprimir.Size = New System.Drawing.Size(199, 32)
        Me.btImprimir.TabIndex = 8
        Me.btImprimir.Text = "Imprimir Exportar"
        '
        'ckFotos
        '
        Me.ckFotos.Location = New System.Drawing.Point(214, 33)
        Me.ckFotos.Name = "ckFotos"
        Me.ckFotos.Properties.Caption = "Mostrar con  Fotos"
        Me.ckFotos.Size = New System.Drawing.Size(119, 19)
        Me.ckFotos.TabIndex = 7
        '
        'btMostrarR
        '
        Me.btMostrarR.Appearance.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMostrarR.Appearance.Options.UseFont = True
        Me.btMostrarR.Location = New System.Drawing.Point(5, 23)
        Me.btMostrarR.Name = "btMostrarR"
        Me.btMostrarR.Size = New System.Drawing.Size(75, 32)
        Me.btMostrarR.TabIndex = 6
        Me.btMostrarR.Text = "mostrar"
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.XtraTabControl1)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(888, 514)
        Me.XtraTabPage1.Text = "Prenómina"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage2
        Me.XtraTabControl1.Size = New System.Drawing.Size(888, 514)
        Me.XtraTabControl1.TabIndex = 40
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage2, Me.XtraTabPage5})
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LabelControl7)
        Me.XtraTabPage2.Controls.Add(Me.GridControl2)
        Me.XtraTabPage2.Controls.Add(Me.RadioGroup1)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl8)
        Me.XtraTabPage2.Controls.Add(Me.ckTodasT)
        Me.XtraTabPage2.Controls.Add(Me.dePDel)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl9)
        Me.XtraTabPage2.Controls.Add(Me.dePAl)
        Me.XtraTabPage2.Controls.Add(Me.cbTiendasP)
        Me.XtraTabPage2.Controls.Add(Me.btCalcular)
        Me.XtraTabPage2.Controls.Add(Me.btGuardaPre)
        Me.XtraTabPage2.Controls.Add(Me.btExportP)
        Me.XtraTabPage2.Controls.Add(Me.ckExtras)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(882, 486)
        Me.XtraTabPage2.Text = "Crear Nueva Prenómina"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(9, 9)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl7.TabIndex = 0
        Me.LabelControl7.Text = "Del"
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.Location = New System.Drawing.Point(-1, 77)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(883, 408)
        Me.GridControl2.TabIndex = 10
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView2.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView2.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView2.OptionsFind.AlwaysVisible = True
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(488, 21)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Calcular comisiones usando %"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Calcular comisiones usando cant.")})
        Me.RadioGroup1.Size = New System.Drawing.Size(385, 24)
        Me.RadioGroup1.TabIndex = 39
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(98, 9)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(9, 13)
        Me.LabelControl8.TabIndex = 1
        Me.LabelControl8.Text = "Al"
        '
        'ckTodasT
        '
        Me.ckTodasT.Location = New System.Drawing.Point(185, 46)
        Me.ckTodasT.Name = "ckTodasT"
        Me.ckTodasT.Properties.Caption = "Todas las tiendas"
        Me.ckTodasT.Size = New System.Drawing.Size(106, 19)
        Me.ckTodasT.TabIndex = 5
        '
        'dePDel
        '
        Me.dePDel.EditValue = Nothing
        Me.dePDel.Location = New System.Drawing.Point(9, 25)
        Me.dePDel.Name = "dePDel"
        Me.dePDel.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dePDel.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dePDel.Size = New System.Drawing.Size(83, 20)
        Me.dePDel.TabIndex = 2
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(187, 9)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl9.TabIndex = 38
        Me.LabelControl9.Text = "Tienda"
        '
        'dePAl
        '
        Me.dePAl.EditValue = Nothing
        Me.dePAl.Location = New System.Drawing.Point(98, 24)
        Me.dePAl.Name = "dePAl"
        Me.dePAl.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dePAl.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dePAl.Size = New System.Drawing.Size(83, 20)
        Me.dePAl.TabIndex = 3
        '
        'cbTiendasP
        '
        Me.cbTiendasP.Location = New System.Drawing.Point(187, 25)
        Me.cbTiendasP.Name = "cbTiendasP"
        Me.cbTiendasP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTiendasP.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTiendasP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTiendasP.Size = New System.Drawing.Size(176, 20)
        Me.cbTiendasP.TabIndex = 4
        '
        'btCalcular
        '
        Me.btCalcular.Location = New System.Drawing.Point(488, 48)
        Me.btCalcular.Name = "btCalcular"
        Me.btCalcular.Size = New System.Drawing.Size(108, 23)
        Me.btCalcular.TabIndex = 7
        Me.btCalcular.Text = "Calcular"
        '
        'btGuardaPre
        '
        Me.btGuardaPre.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btGuardaPre.Location = New System.Drawing.Point(701, 48)
        Me.btGuardaPre.Name = "btGuardaPre"
        Me.btGuardaPre.Size = New System.Drawing.Size(72, 23)
        Me.btGuardaPre.TabIndex = 8
        Me.btGuardaPre.Text = "Guardar"
        Me.btGuardaPre.Visible = False
        '
        'btExportP
        '
        Me.btExportP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btExportP.Location = New System.Drawing.Point(779, 48)
        Me.btExportP.Name = "btExportP"
        Me.btExportP.Size = New System.Drawing.Size(94, 23)
        Me.btExportP.TabIndex = 9
        Me.btExportP.Text = "Exportar a Excel"
        Me.btExportP.Visible = False
        '
        'ckExtras
        '
        Me.ckExtras.EditValue = True
        Me.ckExtras.Location = New System.Drawing.Point(369, 26)
        Me.ckExtras.Name = "ckExtras"
        Me.ckExtras.Properties.Caption = "Contemplar Extras"
        Me.ckExtras.Size = New System.Drawing.Size(113, 19)
        Me.ckExtras.TabIndex = 6
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.lblMsj)
        Me.XtraTabPage5.Controls.Add(Me.btBuscaP)
        Me.XtraTabPage5.Controls.Add(Me.LabelControl10)
        Me.XtraTabPage5.Controls.Add(Me.LabelControl11)
        Me.XtraTabPage5.Controls.Add(Me.ckTodasP)
        Me.XtraTabPage5.Controls.Add(Me.deDelP)
        Me.XtraTabPage5.Controls.Add(Me.LabelControl12)
        Me.XtraTabPage5.Controls.Add(Me.deAlP)
        Me.XtraTabPage5.Controls.Add(Me.cbTiendaPP)
        Me.XtraTabPage5.Controls.Add(Me.GridControl4)
        Me.XtraTabPage5.Controls.Add(Me.btExpPP)
        Me.XtraTabPage5.Controls.Add(Me.GridControl3)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.Size = New System.Drawing.Size(882, 486)
        Me.XtraTabPage5.Text = "Consultar Prenómina"
        '
        'lblMsj
        '
        Me.lblMsj.Location = New System.Drawing.Point(738, 30)
        Me.lblMsj.Name = "lblMsj"
        Me.lblMsj.Size = New System.Drawing.Size(142, 13)
        Me.lblMsj.TabIndex = 47
        Me.lblMsj.Text = "Clic para mostrar el contenido"
        Me.lblMsj.Visible = False
        '
        'btBuscaP
        '
        Me.btBuscaP.Location = New System.Drawing.Point(490, 15)
        Me.btBuscaP.Name = "btBuscaP"
        Me.btBuscaP.Size = New System.Drawing.Size(102, 23)
        Me.btBuscaP.TabIndex = 46
        Me.btBuscaP.Text = "Buscar"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(15, 13)
        Me.LabelControl10.TabIndex = 39
        Me.LabelControl10.Text = "Del"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(92, 3)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(9, 13)
        Me.LabelControl11.TabIndex = 40
        Me.LabelControl11.Text = "Al"
        '
        'ckTodasP
        '
        Me.ckTodasP.Location = New System.Drawing.Point(363, 20)
        Me.ckTodasP.Name = "ckTodasP"
        Me.ckTodasP.Properties.Caption = "Todas las tiendas"
        Me.ckTodasP.Size = New System.Drawing.Size(106, 19)
        Me.ckTodasP.TabIndex = 44
        '
        'deDelP
        '
        Me.deDelP.EditValue = Nothing
        Me.deDelP.Location = New System.Drawing.Point(3, 19)
        Me.deDelP.Name = "deDelP"
        Me.deDelP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deDelP.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deDelP.Size = New System.Drawing.Size(83, 20)
        Me.deDelP.TabIndex = 41
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(181, 3)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 13)
        Me.LabelControl12.TabIndex = 45
        Me.LabelControl12.Text = "Tienda"
        '
        'deAlP
        '
        Me.deAlP.EditValue = Nothing
        Me.deAlP.Location = New System.Drawing.Point(92, 18)
        Me.deAlP.Name = "deAlP"
        Me.deAlP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deAlP.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.deAlP.Size = New System.Drawing.Size(83, 20)
        Me.deAlP.TabIndex = 42
        '
        'cbTiendaPP
        '
        Me.cbTiendaPP.Location = New System.Drawing.Point(181, 19)
        Me.cbTiendaPP.Name = "cbTiendaPP"
        Me.cbTiendaPP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbTiendaPP.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.cbTiendaPP.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.cbTiendaPP.Size = New System.Drawing.Size(176, 20)
        Me.cbTiendaPP.TabIndex = 43
        '
        'GridControl4
        '
        Me.GridControl4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl4.Location = New System.Drawing.Point(0, 259)
        Me.GridControl4.MainView = Me.GridView4
        Me.GridControl4.Name = "GridControl4"
        Me.GridControl4.Size = New System.Drawing.Size(883, 226)
        Me.GridControl4.TabIndex = 13
        Me.GridControl4.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.GridControl4
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView4.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView4.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView4.OptionsView.ColumnAutoWidth = False
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        '
        'btExpPP
        '
        Me.btExpPP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btExpPP.Location = New System.Drawing.Point(785, 233)
        Me.btExpPP.Name = "btExpPP"
        Me.btExpPP.Size = New System.Drawing.Size(94, 23)
        Me.btExpPP.TabIndex = 12
        Me.btExpPP.Text = "Exportar a Excel"
        Me.btExpPP.Visible = False
        '
        'GridControl3
        '
        Me.GridControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl3.Location = New System.Drawing.Point(0, 45)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(883, 185)
        Me.GridControl3.TabIndex = 11
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView3.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridView3.OptionsBehavior.AutoExpandAllGroups = True
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsDetail.AllowExpandEmptyDetails = True
        Me.GridView3.OptionsView.ColumnAutoWidth = False
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        '
        'Empleados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 542)
        Me.Controls.Add(Me.XtraTabControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MinimizeBox = False
        Me.Name = "Empleados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txSueldo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuCComison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nuPComision.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCobranza.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbRuta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckActivo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.TimeEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txBono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txUsuario.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCompV.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txPrima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deVAl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deVAl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deVDel.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deVDel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTurno.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgTipoEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCompensa.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txInfonavit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbDepartamento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbDescanso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTienda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.ComboBox7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txClabe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNomina.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txImss.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txeMail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCurp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txRfc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIngreso.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deIngreso.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deNacimiento.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deNacimiento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTel2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTel1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbColonia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCiudad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEstado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCalle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txNombre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.ckFotos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckTodasT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dePDel.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dePDel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dePAl.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dePAl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTiendasP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckExtras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        Me.XtraTabPage5.PerformLayout()
        CType(Me.ckTodasP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDelP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deDelP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deAlP.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deAlP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbTiendaPP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblMail As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents txSueldo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents cbPC As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbColonia As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbCiudad As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbEstado As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents txCalle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txNombre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txClabe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txNomina As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txImss As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txeMail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCurp As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txRfc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents deIngreso As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deNacimiento As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txTel2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTel1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btModificar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btMostrar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ComboBox7 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents btGuardar As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btImagen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ckActivo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txClave As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nuCComison As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents nuPComision As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents cbCobranza As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents cbRuta As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents btMostrarR As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btImprimir As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ckFotos As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbTienda As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents btPermisos As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbDescanso As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbDepartamento As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblAntiguedad As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents rgTurno As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents rgTipoEmp As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents txCompensa As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txInfonavit As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txCompV As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txPrima As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents deVAl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents deVDel As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txUsuario As DevExpress.XtraEditors.TextEdit
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btExportP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btCalcular As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dePAl As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dePDel As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ckExtras As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents btGuardaPre As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btRegistraHuella As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txBono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ckTodasT As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbTiendasP As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GridControl4 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btExpPP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents btBuscaP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ckTodasP As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents deDelP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents deAlP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbTiendaPP As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lblMsj As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TimeEdit2 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents TimeEdit1 As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
End Class
