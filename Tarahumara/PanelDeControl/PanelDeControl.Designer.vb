﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PanelDeControl
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement3 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement4 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement5 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement6 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement7 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement8 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement9 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement10 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement11 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.TileControl1 = New DevExpress.XtraEditors.TileControl()
        Me.TileGroup2 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem1 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem2 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem3 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem4 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem5 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem6 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup3 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem7 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem8 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem9 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem10 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem11 = New DevExpress.XtraEditors.TileItem()
        Me.SuspendLayout()
        '
        'TileControl1
        '
        Me.TileControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TileControl1.Groups.Add(Me.TileGroup2)
        Me.TileControl1.Groups.Add(Me.TileGroup3)
        Me.TileControl1.Location = New System.Drawing.Point(0, 0)
        Me.TileControl1.MaxId = 11
        Me.TileControl1.Name = "TileControl1"
        Me.TileControl1.Size = New System.Drawing.Size(659, 421)
        Me.TileControl1.TabIndex = 0
        Me.TileControl1.Text = "TileControl1"
        '
        'TileGroup2
        '
        Me.TileGroup2.Items.Add(Me.TileItem1)
        Me.TileGroup2.Items.Add(Me.TileItem2)
        Me.TileGroup2.Items.Add(Me.TileItem3)
        Me.TileGroup2.Items.Add(Me.TileItem4)
        Me.TileGroup2.Items.Add(Me.TileItem5)
        Me.TileGroup2.Items.Add(Me.TileItem6)
        Me.TileGroup2.Name = "TileGroup2"
        Me.TileGroup2.Text = Nothing
        '
        'TileItem1
        '
        TileItemElement1.Text = "Tiendas"
        Me.TileItem1.Elements.Add(TileItemElement1)
        Me.TileItem1.Id = 0
        Me.TileItem1.Name = "TileItem1"
        '
        'TileItem2
        '
        TileItemElement2.Text = "Razones Sociales"
        Me.TileItem2.Elements.Add(TileItemElement2)
        Me.TileItem2.Id = 1
        Me.TileItem2.Name = "TileItem2"
        '
        'TileItem3
        '
        TileItemElement3.Text = "Empleados"
        Me.TileItem3.Elements.Add(TileItemElement3)
        Me.TileItem3.Id = 2
        Me.TileItem3.Name = "TileItem3"
        '
        'TileItem4
        '
        TileItemElement4.Text = "Plazos"
        Me.TileItem4.Elements.Add(TileItemElement4)
        Me.TileItem4.Id = 3
        Me.TileItem4.Name = "TileItem4"
        '
        'TileItem5
        '
        TileItemElement5.Text = "TileItem5"
        Me.TileItem5.Elements.Add(TileItemElement5)
        Me.TileItem5.Id = 4
        Me.TileItem5.Name = "TileItem5"
        '
        'TileItem6
        '
        TileItemElement6.Text = "TileItem6"
        Me.TileItem6.Elements.Add(TileItemElement6)
        Me.TileItem6.Id = 5
        Me.TileItem6.Name = "TileItem6"
        '
        'TileGroup3
        '
        Me.TileGroup3.Items.Add(Me.TileItem7)
        Me.TileGroup3.Items.Add(Me.TileItem8)
        Me.TileGroup3.Items.Add(Me.TileItem9)
        Me.TileGroup3.Items.Add(Me.TileItem10)
        Me.TileGroup3.Items.Add(Me.TileItem11)
        Me.TileGroup3.Name = "TileGroup3"
        Me.TileGroup3.Text = Nothing
        '
        'TileItem7
        '
        TileItemElement7.Text = "Formas de Pago"
        Me.TileItem7.Elements.Add(TileItemElement7)
        Me.TileItem7.Id = 6
        Me.TileItem7.Name = "TileItem7"
        '
        'TileItem8
        '
        TileItemElement8.Text = "Tipos de Gastos"
        Me.TileItem8.Elements.Add(TileItemElement8)
        Me.TileItem8.Id = 7
        Me.TileItem8.Name = "TileItem8"
        '
        'TileItem9
        '
        TileItemElement9.Text = "Bancos"
        Me.TileItem9.Elements.Add(TileItemElement9)
        Me.TileItem9.Id = 8
        Me.TileItem9.Name = "TileItem9"
        '
        'TileItem10
        '
        TileItemElement10.Text = "Cuentas Bancarias"
        Me.TileItem10.Elements.Add(TileItemElement10)
        Me.TileItem10.Id = 9
        Me.TileItem10.Name = "TileItem10"
        '
        'TileItem11
        '
        TileItemElement11.Text = "Valores en Caja"
        Me.TileItem11.Elements.Add(TileItemElement11)
        Me.TileItem11.Id = 10
        Me.TileItem11.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem11.Name = "TileItem11"
        '
        'PanelDeControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 421)
        Me.Controls.Add(Me.TileControl1)
        Me.Name = "PanelDeControl"
        Me.Text = "PanelDeControl"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TileControl1 As DevExpress.XtraEditors.TileControl
    Friend WithEvents TileGroup2 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem1 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem2 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem3 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem4 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem5 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem6 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup3 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem7 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem8 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem9 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem10 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem11 As DevExpress.XtraEditors.TileItem
End Class
