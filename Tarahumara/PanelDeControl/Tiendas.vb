﻿Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.LookAndFeel

Public Class Tiendas
    'Dim conexion As String = "Data Source=" & oConfig.pServidor & ";Initial Catalog=" & _
    '  oConfig.pBase & ";Persist Security Info=True;User ID=" & oLogin.pUsrNombre & _
    '  ";password=" & oConfig.pClave
    Dim idm, idm2, idm3, idm4, idm5 As Integer
    Dim regresa As Boolean = False
    Dim hayfoto As Boolean = False
    Dim bEsNuevo As Boolean = True
    ''cargado dependiendo del TabControl

#Region "tiendas"
    Public Sub cargacombostiendas()
        ''llena combo tiendas
        LlenaCombobox(ComboBox15, "Select distinct nombre,numero from tiendas order by nombre", "nombre", "numero", conexion)
        'llena estados
        LlenaCombobox(ComboBox17, "Select distinct estado,codestado from sepomex order by estado", "Estado", "codestado", conexion)
        If ComboBox17.Properties.Items.Count = 0 Then
            MessageBox.Show(IdiomaMensajes(694, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        ''llena zonas tienda
        LlenaCombobox(ComboBox16, "Select distinct nombre,id from zonatiendas order by nombre", "nombre", "id", conexion)
        If ComboBox16.Properties.Items.Count = 0 Then
            MessageBox.Show(IdiomaMensajes(695, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        LlenaCombobox(ComboBox32, "Select distinct nombre,id from razones order by nombre", "nombre", "id", conexion)
        If ComboBox32.Properties.Items.Count = 0 Then
            MessageBox.Show(IdiomaMensajes(696, Mensaje.Texto), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        LlenaCombobox(ComboBox33, "Select distinct nombre,numero from tiendas  where tipo<>1 and numero <> " & My.Settings.TiendaActual & " order by nombre", "nombre", "numero", conexion)
        LlenaCombobox(cbAlmacenInv, "Select distinct nombre,numero from tiendas order by nombre", "nombre", "numero", conexion)
    End Sub
    'carga datos

    '´´inserta tienda
    'Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Not bEsNuevo Then
            Button6.PerformClick()
            Exit Sub
        End If

        Splash(True)
        If TimeEdit1.Time = TimeEdit2.Time Then
            DevExpress.XtraEditors.XtraMessageBox.Show("Las horas de apertura y cierre no pueden ser la misma!", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            TimeEdit1.Focus()
            Exit Sub
        End If

        validacombostienda()
        If regresacombo = True Then
        Else
            Exit Sub
        End If
        If TextBox28.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(697, Mensaje.Texto), IdiomaMensajes(697, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            TextBox28.Focus()
            Exit Sub
        End If
        If textbox59.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(701, Mensaje.Texto), IdiomaMensajes(701, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            textbox59.Focus()
            Exit Sub
        End If
        If TextBox27.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(698, Mensaje.Texto), IdiomaMensajes(698, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            TextBox27.Focus()
            Exit Sub
        End If
        Dim qry As String = "Select nombre from tiendas where nombre='" & TextBox28.Text.Trim & "' and borrada=0 "
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(699, Mensaje.Texto), IdiomaMensajes(699, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Sub
            Else
                'continua
            End If
        End If


        Dim idhermano, TipoTienda As Integer
        Dim x As String
        If ComboBox33.Properties.Items.Count = 0 Or ComboBox33.SelectedIndex = -1 Then
            idhermano = -1
        Else
            idhermano = CType(ComboBox33.SelectedItem, cInfoCombo).ID
        End If

        TipoTienda = ComboBox34.SelectedIndex + 1
        'If ComboBox34.Text = "Tienda que Vende" Then
        '    TipoTienda = 1
        'Else
        '    TipoTienda = 0
        'End If

        'qry = "exec  insertatienda2 " & _
        ' "'" & TextBox28.Text.Trim.ToUpper & " '," & _
        ' "'" & textbox23.Text.Trim.ToUpper & "'," & _
        '  "'" & textbox22.Text.Trim.ToUpper & "'," & _
        '  "'" & TextBox27.Text.Trim.ToUpper & "'," & _
        ' "'" & CType(ComboBox20.SelectedItem, cInfoCombo).ID & "'," & _
        ' "'" & CType(ComboBox32.SelectedItem, cInfoCombo).ID & "'," & _
        ' "'" & TipoTienda & "'," & _
        ' "'" & idhermano & "'," & _
        '"'" & CType(ComboBox16.SelectedItem, cInfoCombo).ID & "'," & _
        '   "'" & textbox59.Text.Trim.ToUpper & "'," & _
        '          "'" & TextBox32.Text.Trim.ToUpper & "'"
        'bdBase.bdExecute(conexion, qry)
        'If hayfoto Then
        '    Dim cnCon As New SqlConnection(conexion)
        '    cnCon.Open()
        '    Dim imagen As Byte()
        '    imagen = Image2Bytes(Me.PictureBox1.Image)
        '    Dim cmdImg As New SqlCommand("UPDATE tiendas SET logo = @Foto WHERE nombre = '" & TextBox28.Text.Trim.ToUpper & "'", cnCon)
        '    cmdImg.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = imagen  ' Image2Bytes(Me.PictureBox1.Image)
        '    cmdImg.CommandTimeout = 0
        '    cmdImg.ExecuteNonQuery()
        '    cnCon.Close()
        'End If        
        If ChkAlmacenInventario.Checked Then
            qry = "INSERT INTO [tiendas] ([NOMBRE], [TEL], [FAX], [CALLE], [IdCOLONIA], [idRAZON], [TIPO], [HERMANA], [ZONA], [IVA], [franquiciatario], [logo], " & _
              "[ESTADO], [CIUDAD], [COLONIA], [CP], [RAZON], [BORRADA], [llave], HoraAp, HoraCie, Tolerancia, ToleraComida, TiempoComida, Retardo) " & _
              "VALUES (@NOMBRE, @TEl, @FAX, @CALLE, @IdCOLONIA, @idRAZON, @TIPO, @HERMANA, @ZONA, @IVA, @franquiciatario, @logo, @ESTADO, @CIUDAD, @COLONIA, " & _
              "@CP, @RAZON, 0, @llave, @HoraAp, @HoraCie, @Tolerancia, @ToleraComida, @TiempoComida, @Retardo) "
        Else
            qry = "INSERT INTO [tiendas] ([NOMBRE], [TEL], [FAX], [CALLE], [IdCOLONIA], [idRAZON], [TIPO], [HERMANA], [ZONA], [IVA], [franquiciatario], [logo], " & _
              "[ESTADO], [CIUDAD], [COLONIA], [CP], [RAZON], [BORRADA], [llave], AlmacenInv, HoraAp, HoraCie, Tolerancia, ToleraComida, TiempoComida, Retardo) " & _
              "VALUES (@NOMBRE, @TEl, @FAX, @CALLE, @IdCOLONIA, @idRAZON, @TIPO, @HERMANA, @ZONA, @IVA, @franquiciatario, @logo, @ESTADO, @CIUDAD, @COLONIA, " & _
              "@CP, @RAZON, 0, @llave, @AlmacenInv, @HoraAp, @HoraCie, @Tolerancia, @ToleraComida, @TiempoComida, @Retardo) "
        End If       
        Dim cmd As New SqlCommand
        cmd.CommandText = qry
        cmd.Parameters.Add(New SqlParameter("NOMBRE", SqlDbType.VarChar)).Value = TextBox28.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("TEL", SqlDbType.VarChar)).Value = textbox23.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("FAX", SqlDbType.VarChar)).Value = textbox22.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("CALLE", SqlDbType.VarChar)).Value = TextBox27.Text.Trim.ToUpper
        If ComboBox20.SelectedIndex <> -1 Then
            cmd.Parameters.Add(New SqlParameter("IdCOLONIA", SqlDbType.Decimal)).Value = CType(ComboBox20.SelectedItem, cInfoCombo).ID
        Else
            cmd.Parameters.Add(New SqlParameter("IdCOLONIA", SqlDbType.Decimal)).Value = ComboBox20.Tag
            ComboBox20.Tag = ""
        End If
        cmd.Parameters.Add(New SqlParameter("idRAZON", SqlDbType.BigInt)).Value = CType(ComboBox32.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("RAZON", SqlDbType.BigInt)).Value = CType(ComboBox32.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("TIPO", SqlDbType.Decimal)).Value = TipoTienda
        cmd.Parameters.Add(New SqlParameter("HERMANA", SqlDbType.Decimal)).Value = idhermano
        cmd.Parameters.Add(New SqlParameter("ZONA", SqlDbType.Decimal)).Value = CType(ComboBox16.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("IVA", SqlDbType.Decimal)).Value = textbox59.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("franquiciatario", SqlDbType.VarChar)).Value = TextBox32.Text.Trim.ToUpper
        If hayfoto Then
            Dim imagen As Byte()
            imagen = Image2Bytes(Me.PictureBox1.Image)
            cmd.Parameters.Add(New SqlParameter("@logo", SqlDbType.Image)).Value = imagen
        Else
            cmd.Parameters.Add(New SqlParameter("@logo", SqlDbType.Image)).Value = DBNull.Value
        End If
        cmd.Parameters.Add(New SqlParameter("ESTADO", SqlDbType.VarChar)).Value = ComboBox17.Text.Trim
        cmd.Parameters.Add(New SqlParameter("CIUDAD", SqlDbType.VarChar)).Value = ComboBox18.Text.Trim
        cmd.Parameters.Add(New SqlParameter("COLONIA", SqlDbType.VarChar)).Value = ComboBox19.Text.Trim
        cmd.Parameters.Add(New SqlParameter("CP", SqlDbType.VarChar)).Value = ComboBox20.Text.Trim
        cmd.Parameters.Add(New SqlParameter("llave", SqlDbType.UniqueIdentifier)).Value = Guid.NewGuid
        If Not ChkAlmacenInventario.Checked Then            
            cmd.Parameters.Add(New SqlParameter("AlmacenInv", SqlDbType.Decimal)).Value = CType(cbAlmacenInv.SelectedItem, cInfoCombo).ID
        End If

        cmd.Parameters.Add(New SqlParameter("HoraAp", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit1.Time)
        cmd.Parameters.Add(New SqlParameter("HoraCie", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit2.Time)
        cmd.Parameters.Add(New SqlParameter("Tolerancia", SqlDbType.Decimal)).Value = seTolera.Value
        cmd.Parameters.Add(New SqlParameter("ToleraComida", SqlDbType.Decimal)).Value = seTolComida.Value
        cmd.Parameters.Add(New SqlParameter("TiempoComida", SqlDbType.Decimal)).Value = seComida.Value
        cmd.Parameters.Add(New SqlParameter("Retardo", SqlDbType.Decimal)).Value = seRetardo.Value

        Dim regresa As String = bdBase.bdExecute(conexion, cmd)
        Dim drTienda As SqlDataReader = bdBase.bdDataReader(conexion, "select max(numero) numero from tiendas")
        drTienda.Read()
        bdBase.bdExecute(conexion, "update tiendas set AlmacenInv = " + drTienda("numero").ToString + " where numero = " + drTienda("numero").ToString + "")
        Splash(False)
        If String.IsNullOrWhiteSpace(regresa) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(692, Mensaje.Texto), IdiomaMensajes(692, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            limpiatienda()
            cargacombostiendas()
            llenagrid()
            Cursor.Current = Cursors.Default
        Else
            cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmd)
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(700, Mensaje.Texto), IdiomaMensajes(700, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub limpiatienda()
        bEsNuevo = True
        Cursor.Current = Cursors.WaitCursor
        TextBox28.Text = ""
        TextBox27.Text = ""
        TextBox32.Text = ""
        ComboBox17.SelectedIndex = -1
        ComboBox18.SelectedIndex = -1
        ComboBox19.SelectedIndex = -1
        ComboBox20.SelectedIndex = -1
        textbox59.Text = ""
        textbox23.Text = ""
        textbox22.Text = ""
        ComboBox32.SelectedIndex = -1
        ComboBox34.SelectedIndex = -1
        ComboBox33.SelectedIndex = -1
        ComboBox16.SelectedIndex = -1
        ComboBox15.SelectedIndex = -1
        cbAlmacenInv.SelectedIndex = -1
        ChkAlmacenInventario.Checked = False
        Me.PictureBox1.Image = Nothing
        Cursor.Current = Cursors.Default
    End Sub

    'Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

        Splash(True)


        If bEsNuevo Then
            Button5.PerformClick()
            Exit Sub
        End If

        validacombostienda()
        If regresacombo = False Then
            Exit Sub
        End If


        If TextBox28.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(697, Mensaje.Texto), IdiomaMensajes(697, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            TextBox28.Focus()
            Exit Sub
        End If
        If textbox59.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(701, Mensaje.Texto), IdiomaMensajes(701, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            textbox59.Focus()
            Exit Sub
        End If
        If TextBox27.Text.Trim = "" Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(698, Mensaje.Texto), IdiomaMensajes(698, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            TextBox27.Focus()
            Exit Sub



        End If
        Dim qry As String
        Dim idhermano, TipoTienda As Integer
        Dim x As String
        If ComboBox33.Properties.Items.Count = 0 Or ComboBox33.SelectedIndex = -1 Then
            idhermano = -1
        Else
            idhermano = CType(ComboBox33.SelectedItem, cInfoCombo).ID
        End If

        TipoTienda = ComboBox34.SelectedIndex + 1
        'If ComboBox34.Text = "Tienda que Vende" Then
        '    TipoTienda = 1
        'Else
        '    TipoTienda = 0
        'End If

        'qry = "exec  actualizatienda2 " & _
        ' "'" & TextBox28.Text.Trim.ToUpper & " '," & _
        ' "'" & textbox23.Text.Trim.ToUpper & "'," & _
        '  "'" & textbox22.Text.Trim.ToUpper & "'," & _
        '  "'" & TextBox27.Text.Trim.ToUpper & "'," & _
        ' "'" & CType(ComboBox20.SelectedItem, cInfoCombo).ID & "'," & _
        ' "'" & CType(ComboBox32.SelectedItem, cInfoCombo).ID & "'," & _
        ' "'" & TipoTienda & "'," & _
        ' "'" & idhermano & "'," & _
        '"'" & CType(ComboBox16.SelectedItem, cInfoCombo).ID & "'," & _
        ' "'" & textbox59.Text.Trim.ToUpper & "'," & _
        ' "'" & TextBox32.Text.Trim.ToUpper & "'," & _
        ' "'" & CType(ComboBox15.SelectedItem, cInfoCombo).ID & "'"
        'Dim regresa As String = bdBase.bdExecute(conexion, qry)
        'LlenaCombobox(ComboBox15, "Select distinct id,nombre from tiendas order by nombre", "nombre", "id", conexion)
        'If hayfoto Then
        '    Dim cnCon As New SqlConnection(conexion)
        '    cnCon.Open()
        '    Dim imagen As Byte()
        '    imagen = Image2Bytes(Me.PictureBox1.Image)
        '    Dim cmdImg As New SqlCommand("UPDATE tiendas SET logo = @Foto WHERE nombre = '" & TextBox28.Text.Trim.ToUpper & "'", cnCon)
        '    cmdImg.Parameters.Add(New SqlParameter("@Foto", SqlDbType.Image)).Value = imagen  ' Image2Bytes(Me.PictureBox1.Image)
        '    cmdImg.CommandTimeout = 0
        '    cmdImg.ExecuteNonQuery()
        '    cnCon.Close()
        'End If

        qry = "UPDATE tiendas SET [Nombre] = @NOMBRE, [Tel] = @TEL, [Fax] =@FAX, [Calle] = @CALLE, [IdCOLONIA] =@IdCOLONIA, [idRazon] =@idRAZON, " & _
              "[Tipo] = @TIPO, [Hermana] = @HERMANA, [franquiciatario] = @franquiciatario, [ZONA] = @ZONA, [IVA] = @IVA, [logo] = @logo, " & _
              "[ESTADO] = @ESTADO, [CIUDAD] = @CIUDAD, [COLONIA] = @COLONIA, [CP] = @CP, [RAZON] = @RAZON, AlmacenInv = @AlmacenInv, " & _
              "HoraAp = @HoraAp, HoraCie = @HoraCie, Tolerancia = @Tolerancia, ToleraComida = @ToleraComida, TiempoComida = @TiempoComida, " & _
              "Retardo = @Retardo WHERE numero=@numero "
        Dim cmd As New SqlCommand
        cmd.CommandText = qry
        cmd.Parameters.Add(New SqlParameter("NOMBRE", SqlDbType.VarChar)).Value = TextBox28.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("TEL", SqlDbType.VarChar)).Value = textbox23.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("FAX", SqlDbType.VarChar)).Value = textbox22.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("CALLE", SqlDbType.VarChar)).Value = TextBox27.Text.Trim.ToUpper
        If ComboBox20.SelectedIndex <> -1 Then
            cmd.Parameters.Add(New SqlParameter("IdCOLONIA", SqlDbType.Decimal)).Value = CType(ComboBox20.SelectedItem, cInfoCombo).ID
        Else
            cmd.Parameters.Add(New SqlParameter("IdCOLONIA", SqlDbType.Decimal)).Value = ComboBox20.Tag
            ComboBox20.Tag = ""
        End If
        cmd.Parameters.Add(New SqlParameter("idRAZON", SqlDbType.BigInt)).Value = CType(ComboBox32.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("RAZON", SqlDbType.BigInt)).Value = CType(ComboBox32.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("TIPO", SqlDbType.Decimal)).Value = TipoTienda
        cmd.Parameters.Add(New SqlParameter("HERMANA", SqlDbType.Decimal)).Value = idhermano
        cmd.Parameters.Add(New SqlParameter("franquiciatario", SqlDbType.VarChar)).Value = TextBox32.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("ZONA", SqlDbType.Decimal)).Value = CType(ComboBox16.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("IVA", SqlDbType.Decimal)).Value = textbox59.Text.Trim.ToUpper
        cmd.Parameters.Add(New SqlParameter("ESTADO", SqlDbType.VarChar)).Value = ComboBox17.Text.Trim
        cmd.Parameters.Add(New SqlParameter("CIUDAD", SqlDbType.VarChar)).Value = ComboBox18.Text.Trim
        cmd.Parameters.Add(New SqlParameter("COLONIA", SqlDbType.VarChar)).Value = ComboBox19.Text.Trim
        cmd.Parameters.Add(New SqlParameter("CP", SqlDbType.VarChar)).Value = ComboBox20.Text.Trim
        cmd.Parameters.Add(New SqlParameter("numero", SqlDbType.BigInt)).Value = CType(ComboBox15.SelectedItem, cInfoCombo).ID
        If hayfoto Then
            Dim imagen As Byte()
            imagen = Image2Bytes(Me.PictureBox1.Image)
            cmd.Parameters.Add(New SqlParameter("@logo", SqlDbType.Image)).Value = imagen
        Else
            cmd.Parameters.Add(New SqlParameter("@logo", SqlDbType.Image)).Value = DBNull.Value
        End If
        cmd.Parameters.Add(New SqlParameter("AlmacenInv", SqlDbType.Decimal)).Value = CType(cbAlmacenInv.SelectedItem, cInfoCombo).ID
        cmd.Parameters.Add(New SqlParameter("HoraAp", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit1.Time)
        cmd.Parameters.Add(New SqlParameter("HoraCie", SqlDbType.VarChar)).Value = String.Format("{0:HH:mm:ss}", TimeEdit2.Time)
        cmd.Parameters.Add(New SqlParameter("Tolerancia", SqlDbType.Decimal)).Value = seTolera.Value
        cmd.Parameters.Add(New SqlParameter("ToleraComida", SqlDbType.Decimal)).Value = seTolComida.Value
        cmd.Parameters.Add(New SqlParameter("TiempoComida", SqlDbType.Decimal)).Value = seComida.Value
        cmd.Parameters.Add(New SqlParameter("Retardo", SqlDbType.Decimal)).Value = seRetardo.Value
        Dim regresa2 As String = bdBase.bdExecute(conexion, cmd)
        Splash(False)
        If String.IsNullOrWhiteSpace(regresa2) Then
            limpiatienda()
            cargacombostiendas()
            llenagrid()
            ChkAlmacenInventario.Enabled = True
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(693, Mensaje.Texto), IdiomaMensajes(693, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            cError.ReportaError(regresa, oLogin.pEmpId, oLogin.pUserId, "", cmd)
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(700, Mensaje.Texto), IdiomaMensajes(700, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

#End Region




#Region "Valida Combos"
    Private Function validacombostienda()
        regresacombo = False
        Dim regresa As Boolean = False
        Dim oInfo As cInfoCombo
        oInfo = CType(Me.ComboBox17.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(220, Mensaje.Texto), IdiomaMensajes(220, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If
        oInfo = CType(Me.ComboBox18.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(222, Mensaje.Texto), IdiomaMensajes(222, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If

        If ComboBox19.SelectedIndex <> -1 Then
            oInfo = CType(Me.ComboBox19.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(ComboBox19.Text.Trim) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(223, Mensaje.Texto), IdiomaMensajes(223, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            End If
        End If

        If ComboBox20.SelectedIndex <> -1 Then
            oInfo = CType(Me.ComboBox20.SelectedItem, cInfoCombo)
            If Not IsNothing(oInfo) Then
                regresa = True
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        Else
            If Not String.IsNullOrEmpty(ComboBox20.Text.Trim) Then
                If ComboBox20.Text.Trim.Length = 5 Then
                    regresa = GuardaElementoSepomex
                    'regresa = True
                Else
                    DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(225, Mensaje.Texto), IdiomaMensajes(225, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Return False
                    Exit Function
                End If
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(224, Mensaje.Texto), IdiomaMensajes(224, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False
                Exit Function
            End If
        End If

        oInfo = CType(Me.ComboBox16.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(654, Mensaje.Texto), IdiomaMensajes(654, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If

        oInfo = CType(Me.ComboBox32.SelectedItem, cInfoCombo)
        If Not IsNothing(oInfo) Then
            regresa = True
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(702, Mensaje.Texto), IdiomaMensajes(702, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        End If

        If ComboBox34.SelectedIndex = -1 Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(703, Mensaje.Texto), IdiomaMensajes(703, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Hand)
            Exit Function
        Else
            regresa = True
        End If
        If ChkAlmacenInventario.Checked = False Then
            If cbAlmacenInv.SelectedIndex = -1 Then
                DevExpress.XtraEditors.XtraMessageBox.Show("Debe seleccionar un almacén de inventario", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Exit Function
            Else
                regresa = True
            End If
        End If
        regresacombo = True

    End Function

    Private Function GuardaElementoSepomex() As Boolean
        Dim regresar As Boolean = False
        If Not String.IsNullOrEmpty(ComboBox19.Text.Trim) And Not String.IsNullOrEmpty(ComboBox20.Text.Trim) Then
            Dim sQuery As String = "INSERT INTO sepomex (cp, colonia, municipio, ciudad, estado, codEstado, codigoPais, llave) VALUES ('" & _
                                           ComboBox20.Text.Trim & "', '" & ComboBox19.Text.Trim & "', '" & ComboBox18.Text.Trim & "', '" & ComboBox18.Text.Trim & "', '" & _
                                           ComboBox17.Text.Trim & "', '" & CType(ComboBox17.SelectedItem, cInfoCombo).ID & "', 'MX', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER)) SELECT @@IDENTITY AS id"
            Dim regresa As String = bdBase.bdExecute(conexion, sQuery, False, True)
            If Not String.IsNullOrEmpty(regresa) Then
                If IsNumeric(regresa) Then
                    ComboBox20.Tag = regresa
                    regresar = True
                Else
                    ComboBox20.Tag = "-1"
                End If
            End If
        End If
        Return regresar
    End Function
#End Region
    ''combos de ciudad.estado,colonia,cp
#Region "combos"

    'Private Sub ComboBox17_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox17.SelectedIndexChanged
    Private Sub ComboBox17_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox17.SelectedIndexChanged
        LlenaCombobox(ComboBox18, "Select distinct municipio,codestado from sepomex where estado='" & ComboBox17.Text & "' order by municipio", "municipio", "codestado", conexion)
    End Sub

    'Private Sub ComboBox18_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox18.SelectedIndexChanged
    Private Sub ComboBox18_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox18.SelectedIndexChanged
        LlenaCombobox(ComboBox19, "Select distinct colonia,codestado from sepomex where estado='" & ComboBox17.Text & "' and municipio='" & ComboBox18.Text & "' order by colonia", "colonia", "codestado", conexion)
    End Sub

    'Private Sub ComboBox19_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox19.SelectedIndexChanged
    Private Sub ComboBox19_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox19.SelectedIndexChanged
        LlenaCombobox(ComboBox20, "Select distinct cp,id from sepomex where estado='" & ComboBox17.Text & _
                "' and municipio='" & ComboBox18.Text & "' and colonia='" & ComboBox19.Text & "' order by cp", "cp", "id", conexion)
    End Sub
#End Region

    Private Sub utilerias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        'IdiomaTextos(Me)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        ' DevExpress.UserSkins.OfficeSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)

        cargacombostiendas()
        llenagrid()

        RegistraAcceso(conexion, "Utilerías, Control de tienas [utilerias.frm]")
    End Sub


    'Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl4)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub llenagrid()
        Dim qry As String
        qry = " SELECT     tiendas.NUMERO, tiendas.Nombre, tiendas.Tel AS Telefono, tiendas.Fax, tiendas.Calle,  " & _
                      " tiendas.Tipo, tiendas.franquiciatario, tiendas.ZONA, tiendas.IVA, razones.Nombre AS RazonSocial, tiendas.estado, tiendas.ciudad, tiendas.colonia,   " & _
                      " tiendas.cp, tiendas_1.Nombre AS AlmacenVirtual, tiendas_2.Nombre AS AlmacenInventarios, tiendas.HoraAp AS HoraApertura, " & _
                      "tiendas.HoraCie AS HoraCierre, tiendas.Tolerancia  " & _
" FROM         tiendas INNER JOIN  " & _
            "          razones ON tiendas.idRazon = razones.id left JOIN  " & _
           "           sepomex ON tiendas.IdCOLONIA = sepomex.id LEFT OUTER JOIN " & _
          "            tiendas AS tiendas_1 ON tiendas.Hermana = tiendas_1.NUMERO LEFT OUTER JOIN " & _
          " tiendas AS tiendas_2 ON tiendas.AlmacenInv = tiendas_2.NUMERO " & _
        " WHERE (Tiendas.BORRADA = 0)  "
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl4.DataSource = dr2.Tables(0)
        GridView4.PopulateColumns()
        GridView4.BestFitColumns()
    End Sub

    'Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If IsNumeric(DataGridView2.CurrentRow.Cells(e.ColumnIndex).Value) Then
            Cursor.Current = Cursors.WaitCursor
            Dim qry As String = "update  tiendas set Meta='" & DataGridView1.CurrentRow.Cells(2).Value.ToString.Trim.ToUpper & "'," & _
                                " metamin='" & DataGridView1.CurrentRow.Cells(3).Value.ToString.Trim.ToUpper & "'," & _
                                " matesat='" & DataGridView1.CurrentRow.Cells(4).Value.ToString.Trim.ToUpper & "'," & _
                                " metasob='" & DataGridView1.CurrentRow.Cells(5).Value.ToString.Trim.ToUpper & "', " & _
                                " comiMetaMin = '" & DataGridView1.CurrentRow.Cells(9).Value.ToString.Trim.ToUpper & "' " & _
                                " where numero='" & DataGridView1.CurrentRow.Cells(6).Value & "'"
            Dim regresa As String = bdBase.bdExecute(conexion, qry)
            Cursor.Current = Cursors.Default
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(704, Mensaje.Texto), IdiomaMensajes(704, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub

    'Private Sub DataGridView2_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellValueChanged
    Private Sub DataGridView2_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellValueChanged
        If IsNumeric(DataGridView2.CurrentRow.Cells(e.ColumnIndex).Value) Then
            Cursor.Current = Cursors.WaitCursor
            Dim qry As String = "update  tiendas set renta='" & DataGridView2.CurrentRow.Cells(2).Value.ToString.Trim.ToUpper & "'," & _
                                " gastos='" & DataGridView2.CurrentRow.Cells(3).Value.ToString.Trim.ToUpper & "', " & _
                                " volumen='" & DataGridView2.CurrentRow.Cells(4).Value.ToString.Trim.ToUpper & "' " & _
                                " where numero='" & DataGridView2.CurrentRow.Cells(5).Value & "'"
            Dim regresa As String = bdBase.bdExecute(conexion, qry)
            Cursor.Current = Cursors.Default
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(704, Mensaje.Texto), IdiomaMensajes(704, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub

    Private Sub FillCombo(ByVal elemento As String, ByVal id As String, ByVal combo As ComboBox, ByVal tabla As String)
        If id = "" Or id = "-1" Or id = "-1.00" Then
        Else

            Dim qry As String
            qry = "SELECT      " & elemento & _
               " FROM   " & tabla & _
               " WHERE     (id ='" & id & "')"
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            dr2.Read()
            Dim contador As Integer = 0
            For contador = 0 To combo.Items.Count - 1
                If combo.Items.Item(contador).ToString.Trim = dr2(elemento).ToString.Trim Then
                    combo.SelectedIndex = contador
                    Exit For
                End If
            Next
        End If
    End Sub

    'Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Cursor.Current = Cursors.WaitCursor

        If ComboBox15.SelectedIndex = -1 Then

            MsgBox("Debe seleccionar una tienda", MsgBoxStyle.Information)

        Else

            Dim qry As String = "Select * from tiendas where numero='" & CType(ComboBox15.SelectedItem, cInfoCombo).ID & "'  "
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            dr.Read()
            bEsNuevo = False
            If Not IsDBNull(dr("nombre")) Then TextBox28.Text = dr("nombre").Trim.ToUpper Else TextBox28.Text = ""
            If Not IsDBNull(dr("calle")) Then TextBox27.Text = dr("calle").Trim.ToUpper Else TextBox27.Text = ""
            If Not IsDBNull(dr("tel")) Then textbox23.Text = dr("tel").Trim.ToUpper Else textbox23.Text = ""
            If Not IsDBNull(dr("fax")) Then textbox22.Text = dr("fax").Trim.ToUpper Else textbox22.Text = ""
            If Not IsDBNull(dr("franquiciatario")) Then TextBox32.Text = dr("franquiciatario").Trim.ToUpper Else TextBox32.Text = ""
            If Not IsDBNull(dr("IVA")) Then textbox59.Text = dr("IVA") Else textbox59.Text = ""
            'TextBox6.Text = dr("rfc").Trim.ToUpper
            'TextBox14.Text = dr("Contacto").Trim.ToUpper

            ' Buscar datos sepomex
            If Not IsDBNull(dr("idColonia")) Then
                If dr("idColonia") <> -1 Then
                    qry = "SELECT cp, colonia, municipio, estado FROM sepomex WHERE id = " & dr("idColonia")
                    Dim drCp As SqlDataReader = bdBase.bdDataReader(conexion, qry)
                    If Not drCp Is Nothing Then
                        If drCp.HasRows Then
                            drCp.Read()
                            BuscaEnComboDx(ComboBox17, drCp("Estado").ToString.Trim)
                            BuscaEnComboDx(ComboBox18, drCp("municipio").ToString.Trim)
                            BuscaEnComboDx(ComboBox19, drCp("colonia").ToString.Trim)
                            BuscaEnComboDx(ComboBox20, drCp("cp").ToString.Trim)
                        Else
                            ComboBox17.SelectedIndex = -1
                            ComboBox18.SelectedIndex = -1
                            ComboBox19.SelectedIndex = -1
                            ComboBox20.SelectedIndex = -1
                        End If
                        drCp.Close()
                    End If
                Else
                    BuscaEnComboDx(ComboBox17, dr("Estado").ToString.Trim)
                    BuscaEnComboDx(ComboBox18, dr("Ciudad").ToString.Trim)
                    ComboBox19.Text = dr("colonia").ToString.Trim
                    ComboBox20.Text = dr("cp").ToString.Trim

                End If
            End If

            If Not IsDBNull(dr("idRazon")) Then BuscaEnComboDx(ComboBox32, dr("idRazon"), True) Else ComboBox32.SelectedIndex = -1
            If Not IsDBNull(dr("Tipo")) Then BuscaEnComboDx(ComboBox34, dr("Tipo"), False, dr("Tipo")) Else ComboBox34.SelectedIndex = -1
            If Not IsDBNull(dr("Hermana")) Then BuscaEnComboDx(ComboBox33, dr("Hermana"), True) Else ComboBox33.SelectedIndex = -1
            If Not IsDBNull(dr("Zona")) Then BuscaEnComboDx(ComboBox16, dr("Zona"), True) Else ComboBox16.SelectedIndex = -1
            If Not IsDBNull(dr("AlmacenInv")) Then BuscaEnComboDx(cbAlmacenInv, dr("AlmacenInv"), True) Else cbAlmacenInv.SelectedIndex = -1
            If Not IsDBNull(dr("HoraAp")) Then TimeEdit1.Time = Convert.ToDateTime(dr("HoraAp")) Else TimeEdit1.Time = Convert.ToDateTime("09:00:00")
            If Not IsDBNull(dr("HoraCie")) Then TimeEdit2.Time = Convert.ToDateTime(dr("HoraCie")) Else TimeEdit1.Time = Convert.ToDateTime("21:00:00")
            If Not IsDBNull(dr("Tolerancia")) Then seTolera.Value = dr("Tolerancia") Else seTolera.Value = 10
            If Not IsDBNull(dr("ToleraComida")) Then seTolComida.Value = dr("ToleraComida") Else seTolComida.Value = 5
            If Not IsDBNull(dr("TiempoComida")) Then seComida.Value = dr("TiempoComida") Else seComida.Value = 60
            If Not IsDBNull(dr("Retardo")) Then seRetardo.Value = dr("Retardo") Else seRetardo.Value = 5

            'FillCombo("nombre", dr("idrazon").ToString.Trim, ComboBox32, "razones")

            If Not IsDBNull(dr("logo")) Then
                Dim img As Image = Bytes2Image(CType(dr("logo"), Byte()))
                If img IsNot Nothing Then
                    PictureBox1.Image = img
                    hayfoto = True
                End If
            Else
                PictureBox1.Image = Nothing
                hayfoto = False
            End If

            'Button3.Visible = True
            ChkAlmacenInventario.Enabled = False
        End If

        Cursor.Current = Cursors.Default
    End Sub

    Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanged
        Select Case XtraTabControl1.SelectedTabPageIndex
            Case 1
                Dim x As Integer = 0
                Dim qry As String = "SELECT NUMERO, Nombre as Tienda,  ISNULL(Meta, 0) as MetaEnPares, ISNULL(MetaMin, 0) as MetaMinima, ISNULL(MateSat, 0) as MetaSatisfactoria, " & _
                                    "ISNULL(MetaSob, 0) as MetaSobresaliente, ID, 0 as MaximoInventario,0 as RotacionMinima, ISNULL(comiMetaMin, 0) AS ComisionMetaMinima " & _
                                    " FROM Tiendas where tipo=1 order by NUMERO "
                Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
                DataGridView1.DataSource = ds.Tables(0)
                For x = 0 To 9
                    DataGridView1.Columns(x).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Next
                DataGridView1.Columns(0).ReadOnly = True
                DataGridView1.Columns(1).ReadOnly = True
                DataGridView1.Columns(6).Visible = False
            Case 2
                Dim x As Integer = 0
                Dim qry As String = " SELECT     NUMERO, Nombre as Tienda,  ISNULL(Renta, 0) as RentaMensual, ISNULL(Gastos, 0) as GastosFijos, ISNULL(Volumen, 0) as VolumenTienda, ID " & _
                " FROM Tiendas where tipo=1 " & _
                 " order by NUMERO "
                Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
                DataGridView2.DataSource = ds.Tables(0)
                For x = 0 To 5
                    DataGridView2.Columns(x).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                Next
                DataGridView2.Columns(0).ReadOnly = True
                DataGridView2.Columns(1).ReadOnly = True
                DataGridView2.Columns(5).Visible = False
        End Select
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            PictureBox1.Image = System.Drawing.Image.FromFile(OpenFileDialog1.FileName)
            hayfoto = True
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Zonas.ShowDialog()
    End Sub

    Private Sub ChkAlmacenInventario_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles ChkAlmacenInventario.CheckedChanged
        If ChkAlmacenInventario.Checked = True Then
            cbAlmacenInv.Enabled = False
        Else
            cbAlmacenInv.Enabled = True
        End If
    End Sub
End Class