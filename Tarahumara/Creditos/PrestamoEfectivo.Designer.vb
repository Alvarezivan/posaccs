﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrestamoEfectivo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.PCredito = New DevExpress.XtraEditors.PanelControl()
        Me.cbsubcliente = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.lbdisp = New DevExpress.XtraEditors.LabelControl()
        Me.pbestatus = New System.Windows.Forms.PictureBox()
        Me.lblc = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.lbrfc = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.lbemail = New DevExpress.XtraEditors.LabelControl()
        Me.lbdireccion = New DevExpress.XtraEditors.LabelControl()
        Me.lbtel = New DevExpress.XtraEditors.LabelControl()
        Me.lbnombre = New DevExpress.XtraEditors.LabelControl()
        Me.lbcliente = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.btnClientes = New DevExpress.XtraEditors.SimpleButton()
        Me.lbcajero = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.lbcaja = New DevExpress.XtraEditors.LabelControl()
        Me.lbtienda = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.cbvendedor = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.tbtotal = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PCredito, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PCredito.SuspendLayout()
        CType(Me.cbsubcliente.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbestatus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbvendedor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl2.Location = New System.Drawing.Point(12, 12)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage3
        Me.XtraTabControl2.Size = New System.Drawing.Size(891, 137)
        Me.XtraTabControl2.TabIndex = 1
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage3})
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.PCredito)
        Me.XtraTabPage3.Controls.Add(Me.lbrfc)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl47)
        Me.XtraTabPage3.Controls.Add(Me.lbemail)
        Me.XtraTabPage3.Controls.Add(Me.lbdireccion)
        Me.XtraTabPage3.Controls.Add(Me.lbtel)
        Me.XtraTabPage3.Controls.Add(Me.lbnombre)
        Me.XtraTabPage3.Controls.Add(Me.lbcliente)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl25)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl24)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage3.Controls.Add(Me.btnClientes)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(885, 109)
        Me.XtraTabPage3.Text = "Datos &Cliente Actual"
        '
        'PCredito
        '
        Me.PCredito.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PCredito.Appearance.BackColor = System.Drawing.Color.LightGray
        Me.PCredito.Appearance.Options.UseBackColor = True
        Me.PCredito.Controls.Add(Me.cbsubcliente)
        Me.PCredito.Controls.Add(Me.LabelControl7)
        Me.PCredito.Controls.Add(Me.LabelControl8)
        Me.PCredito.Controls.Add(Me.LabelControl6)
        Me.PCredito.Controls.Add(Me.lbdisp)
        Me.PCredito.Controls.Add(Me.pbestatus)
        Me.PCredito.Controls.Add(Me.lblc)
        Me.PCredito.Controls.Add(Me.LabelControl5)
        Me.PCredito.Location = New System.Drawing.Point(452, 5)
        Me.PCredito.Name = "PCredito"
        Me.PCredito.Size = New System.Drawing.Size(267, 92)
        Me.PCredito.TabIndex = 21
        Me.PCredito.Visible = False
        '
        'cbsubcliente
        '
        Me.cbsubcliente.Location = New System.Drawing.Point(98, 67)
        Me.cbsubcliente.Name = "cbsubcliente"
        Me.cbsubcliente.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbsubcliente.Size = New System.Drawing.Size(164, 20)
        Me.cbsubcliente.TabIndex = 16
        '
        'LabelControl7
        '
        Me.LabelControl7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl7.Location = New System.Drawing.Point(12, 68)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(73, 16)
        Me.LabelControl7.TabIndex = 21
        Me.LabelControl7.Text = "SubCliente:"
        '
        'LabelControl8
        '
        Me.LabelControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl8.Location = New System.Drawing.Point(12, 9)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(94, 16)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "Limite Credito:"
        '
        'LabelControl6
        '
        Me.LabelControl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl6.Location = New System.Drawing.Point(12, 28)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl6.TabIndex = 13
        Me.LabelControl6.Text = "Disponible:"
        '
        'lbdisp
        '
        Me.lbdisp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbdisp.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbdisp.Location = New System.Drawing.Point(98, 26)
        Me.lbdisp.Name = "lbdisp"
        Me.lbdisp.Size = New System.Drawing.Size(0, 20)
        Me.lbdisp.TabIndex = 14
        '
        'pbestatus
        '
        Me.pbestatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbestatus.Location = New System.Drawing.Point(208, 31)
        Me.pbestatus.Name = "pbestatus"
        Me.pbestatus.Size = New System.Drawing.Size(54, 23)
        Me.pbestatus.TabIndex = 18
        Me.pbestatus.TabStop = False
        '
        'lblc
        '
        Me.lblc.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblc.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblc.Location = New System.Drawing.Point(112, 5)
        Me.lblc.Name = "lblc"
        Me.lblc.Size = New System.Drawing.Size(0, 20)
        Me.lblc.TabIndex = 16
        '
        'LabelControl5
        '
        Me.LabelControl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl5.Location = New System.Drawing.Point(208, 14)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(54, 16)
        Me.LabelControl5.TabIndex = 17
        Me.LabelControl5.Text = "Estatus:"
        '
        'lbrfc
        '
        Me.lbrfc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbrfc.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbrfc.Location = New System.Drawing.Point(259, 11)
        Me.lbrfc.Name = "lbrfc"
        Me.lbrfc.Size = New System.Drawing.Size(0, 20)
        Me.lbrfc.TabIndex = 12
        '
        'LabelControl47
        '
        Me.LabelControl47.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl47.Location = New System.Drawing.Point(223, 11)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(28, 16)
        Me.LabelControl47.TabIndex = 11
        Me.LabelControl47.Text = "RFC:"
        '
        'lbemail
        '
        Me.lbemail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbemail.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbemail.Location = New System.Drawing.Point(268, 77)
        Me.lbemail.Name = "lbemail"
        Me.lbemail.Size = New System.Drawing.Size(0, 20)
        Me.lbemail.TabIndex = 10
        '
        'lbdireccion
        '
        Me.lbdireccion.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbdireccion.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbdireccion.Location = New System.Drawing.Point(84, 58)
        Me.lbdireccion.Name = "lbdireccion"
        Me.lbdireccion.Size = New System.Drawing.Size(0, 20)
        Me.lbdireccion.TabIndex = 9
        '
        'lbtel
        '
        Me.lbtel.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbtel.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbtel.Location = New System.Drawing.Point(84, 78)
        Me.lbtel.Name = "lbtel"
        Me.lbtel.Size = New System.Drawing.Size(0, 20)
        Me.lbtel.TabIndex = 8
        '
        'lbnombre
        '
        Me.lbnombre.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbnombre.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbnombre.Location = New System.Drawing.Point(84, 34)
        Me.lbnombre.Name = "lbnombre"
        Me.lbnombre.Size = New System.Drawing.Size(0, 20)
        Me.lbnombre.TabIndex = 7
        '
        'lbcliente
        '
        Me.lbcliente.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbcliente.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbcliente.Location = New System.Drawing.Point(103, 5)
        Me.lbcliente.Name = "lbcliente"
        Me.lbcliente.Size = New System.Drawing.Size(0, 19)
        Me.lbcliente.TabIndex = 6
        '
        'LabelControl27
        '
        Me.LabelControl27.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl27.Location = New System.Drawing.Point(223, 76)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(37, 16)
        Me.LabelControl27.TabIndex = 5
        Me.LabelControl27.Text = "Email:"
        '
        'LabelControl26
        '
        Me.LabelControl26.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl26.Location = New System.Drawing.Point(18, 76)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(60, 16)
        Me.LabelControl26.TabIndex = 4
        Me.LabelControl26.Text = "Telefono:"
        '
        'LabelControl25
        '
        Me.LabelControl25.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl25.Location = New System.Drawing.Point(14, 56)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(64, 16)
        Me.LabelControl25.TabIndex = 3
        Me.LabelControl25.Text = "Direccion:"
        '
        'LabelControl24
        '
        Me.LabelControl24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl24.Location = New System.Drawing.Point(24, 8)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(73, 16)
        Me.LabelControl24.TabIndex = 2
        Me.LabelControl24.Text = "No. Cliente:"
        '
        'LabelControl19
        '
        Me.LabelControl19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl19.Location = New System.Drawing.Point(24, 36)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(54, 16)
        Me.LabelControl19.TabIndex = 1
        Me.LabelControl19.Text = "Nombre:"
        '
        'btnClientes
        '
        Me.btnClientes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClientes.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClientes.Appearance.Options.UseFont = True
        Me.btnClientes.Location = New System.Drawing.Point(725, 12)
        Me.btnClientes.Name = "btnClientes"
        Me.btnClientes.Size = New System.Drawing.Size(155, 77)
        Me.btnClientes.TabIndex = 0
        Me.btnClientes.Text = "Busqueda de Clientes"
        '
        'lbcajero
        '
        Me.lbcajero.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbcajero.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbcajero.Location = New System.Drawing.Point(673, 180)
        Me.lbcajero.Name = "lbcajero"
        Me.lbcajero.Size = New System.Drawing.Size(133, 16)
        Me.lbcajero.TabIndex = 20
        Me.lbcajero.Text = "xxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl51
        '
        Me.LabelControl51.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl51.Location = New System.Drawing.Point(623, 180)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(43, 16)
        Me.LabelControl51.TabIndex = 19
        Me.LabelControl51.Text = "Cajero:"
        '
        'lbcaja
        '
        Me.lbcaja.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbcaja.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbcaja.Location = New System.Drawing.Point(870, 166)
        Me.lbcaja.Name = "lbcaja"
        Me.lbcaja.Size = New System.Drawing.Size(20, 19)
        Me.lbcaja.TabIndex = 18
        Me.lbcaja.Text = "xx"
        '
        'lbtienda
        '
        Me.lbtienda.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbtienda.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbtienda.Location = New System.Drawing.Point(673, 157)
        Me.lbtienda.Name = "lbtienda"
        Me.lbtienda.Size = New System.Drawing.Size(133, 16)
        Me.lbtienda.TabIndex = 17
        Me.lbtienda.Text = "xxxxxxxxxxxxxxxxxxx"
        '
        'LabelControl30
        '
        Me.LabelControl30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl30.Location = New System.Drawing.Point(826, 170)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(31, 16)
        Me.LabelControl30.TabIndex = 16
        Me.LabelControl30.Text = "Caja:"
        '
        'LabelControl29
        '
        Me.LabelControl29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl29.Location = New System.Drawing.Point(623, 157)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(44, 16)
        Me.LabelControl29.TabIndex = 15
        Me.LabelControl29.Text = "Tienda:"
        '
        'cbvendedor
        '
        Me.cbvendedor.Location = New System.Drawing.Point(473, 166)
        Me.cbvendedor.Name = "cbvendedor"
        Me.cbvendedor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbvendedor.Size = New System.Drawing.Size(143, 20)
        Me.cbvendedor.TabIndex = 14
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl3.Location = New System.Drawing.Point(350, 173)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(117, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Seleccione Vendedor"
        '
        'tbtotal
        '
        Me.tbtotal.Location = New System.Drawing.Point(316, 209)
        Me.tbtotal.Name = "tbtotal"
        Me.tbtotal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbtotal.Properties.Appearance.Options.UseFont = True
        Me.tbtotal.Size = New System.Drawing.Size(151, 30)
        Me.tbtotal.TabIndex = 21
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(13, 212)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(281, 33)
        Me.LabelControl1.TabIndex = 22
        Me.LabelControl1.Text = "Total Prestamo Efectivo"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(765, 205)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(128, 40)
        Me.SimpleButton1.TabIndex = 23
        Me.SimpleButton1.Text = "&Guardar"
        '
        'PrestamoEfectivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 257)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.tbtotal)
        Me.Controls.Add(Me.lbcajero)
        Me.Controls.Add(Me.LabelControl51)
        Me.Controls.Add(Me.lbcaja)
        Me.Controls.Add(Me.lbtienda)
        Me.Controls.Add(Me.LabelControl30)
        Me.Controls.Add(Me.LabelControl29)
        Me.Controls.Add(Me.cbvendedor)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.XtraTabControl2)
        Me.Name = "PrestamoEfectivo"
        Me.Text = "PrestamoEfectivo"
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.PCredito, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PCredito.ResumeLayout(False)
        Me.PCredito.PerformLayout()
        CType(Me.cbsubcliente.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbestatus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbvendedor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents PCredito As DevExpress.XtraEditors.PanelControl
    Friend WithEvents cbsubcliente As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbdisp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents pbestatus As System.Windows.Forms.PictureBox
    Public WithEvents lblc As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbrfc As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbemail As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbdireccion As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbtel As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbnombre As DevExpress.XtraEditors.LabelControl
    Public WithEvents lbcliente As DevExpress.XtraEditors.LabelControl
    Public WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnClientes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbcajero As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbcaja As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbtienda As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents cbvendedor As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbtotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
