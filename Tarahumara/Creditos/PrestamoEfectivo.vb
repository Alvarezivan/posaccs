﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports System.Drawing
Imports System.IO
Imports System.Drawing.Drawing2D

Public Class PrestamoEfectivo

    Dim socio As Integer
    Dim cfecha As String
    Dim disponible As Decimal

    Private Sub btnClientes_Click(sender As System.Object, e As System.EventArgs) Handles btnClientes.Click
        Dim clientes As New IngresoClientes
        Preefectivo = True
        If clientes.ShowDialog() = DialogResult.OK Then

            If clientes.tbrfc.Text = "" Then

            Else
                lbnombre.Text = clientes.tbnombre.Text.Trim
                lbcliente.Text = clientes.lbnumero.Text.Trim
                lbdireccion.Text = clientes.tbcalle.Text & " " & clientes.tbnext.Text & "-" & clientes.tbnint.Text & "  " & clientes.tbciudad.Text & ", " & clientes.tbestado.Text & ", CP " & clientes.tbcp.Text
                lbtel.Text = clientes.tbtelefono.Text
                lbemail.Text = clientes.tbemail.Text
                lbrfc.Text = clientes.tbrfc.Text
                PCredito.Visible = True
                verificadisponible()

                btnClientes.Enabled = False

                If Val(lbdisp.Text) <= 0 Then
                    pbestatus.BackColor = Color.Red
                    MsgBox("Solo se pueden realizar Ventas de Contado al Cliente")
                Else
                    pbestatus.BackColor = Color.Green
                    Dim qry As String = "select numero, nombre from subsocios where activo = 1 and idsocio = " & socio
                    LlenaCombobox(cbsubcliente, qry, "nombre", "numero", conexion)
                End If
            End If

        End If

    End Sub

    Private Sub verificadisponible()
        Dim bHabilita As Boolean = True
        Dim qry, qry1 As String
        Dim limite, saldo As Decimal
        socio = lbcliente.Text

        qry = " SELECT  CASE WHEN socios.LIMITEcreditoEfe IS NULL THEN 0 ELSE socios.LIMITEcreditoEfe END as Limite from socios WHERE id = '" & socio & "'"
        qry1 = " SELECT CASE WHEN SUM(TOTAL) IS NULL THEN 0 ELSE SUM(TOTAL) END AS Saldo from creditos WHERE socio = '" & socio & "' and efectivo = 1"


        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        ds.Read()
        lblc.Text = ds("Limite")
        limite = ds("Limite")
        ds.Close()

        Dim ds1 As SqlDataReader = bdBase.bdDataReader(conexion, qry1)
        ds1.Read()
        saldo = ds1("saldo")
        ds1.Close()
        lbdisp.Text = limite - saldo
        disponible = limite - saldo
    End Sub

    Private Sub Registraventa()

        Dim ticket As String = GeneraFolio()
        Dim qry As String

        Dim tienda As Integer = My.Settings.TiendaActual
        Dim caja As Integer = My.Settings.TiendaActual
        Dim vendedor As Integer = CType(cbvendedor.SelectedItem, cInfoCombo).ID
        Dim total As Decimal = tbtotal.Text
        Dim empleado As Integer = oLogin.pUserId
        Dim subcliente As Integer = 0

        If disponible < total Then
            MsgBox("Cliente no cuenta con disponible suficiente", MsgBoxStyle.Information)
            Exit Sub
        End If



        If cbsubcliente.SelectedIndex = -1 Then
            subcliente = -1
        Else
            subcliente = CType(cbsubcliente.SelectedItem, cInfoCombo).ID
        End If




        Dim x As Integer
        Dim iEfectivo, iCheque, iTarjetas, iNc, iVales, iCupon, iStatus, iGiro, iNVale, iNumvale, iVisa, iCredito As Decimal
        Dim mastercard, amex, idmov, idvale, idcupon, idprecios, idnotacre, idvaleut, descto, Factura, idfactura, monut, mongen, PrestamoE, d1, d2, d3, d4, d5, d6, d7, d8, d9, d0, cf, vd, vi, gremove As Decimal
        Dim e1, e2, e3, e4, e5, e6, e7, e8, e9, e0 As Decimal
        Dim x0, x1, x2, x3, x4, x5, x6, x7, x8, x9 As Decimal
        iStatus = 0
        iEfectivo = 0
        iCheque = 0
        iNc = 0
        iVales = 0
        iTarjetas = 0
        iCupon = 0
        PrestamoE = total
        iGiro = 0
        iCredito = 0
        vd = 0
        cf = 0
        vi = 0
        d1 = 0
        d2 = 0
        d3 = 0
        d4 = 0
        d5 = 0
        d6 = 0
        d7 = 0
        d8 = 0
        d9 = 0
        d0 = 0
        cf = 0
        vd = 0
        vi = 0
        gremove = 0
        e1 = 0
        e2 = 0
        e3 = 0
        e4 = 0
        e5 = 0
        e6 = 0
        e7 = 0
        e8 = 0
        e9 = 0
        e0 = 0
        x0 = 0
        x1 = 0
        x2 = 0
        x3 = 0
        x4 = 0
        x5 = 0
        x6 = 0
        x7 = 0
        x8 = 0
        x9 = 0
        
        

        qry = "Insert Into fma (ticket,efectivo,cheques,tarjetas,nc,vales,cupon,credito,Status,fecha,tienda,caja,socio,Total,nvale,numvale,pagare,visa," & _
         "mastercard,amex,idvale,idcupon,idprecios,idnotacre,idvaleut,descto,factura,monut,mongen,PrestamoE,idfactura,d1,d2,d3,d4,d5,d6,d7,d8,d9,d0,cf,vd,vi,gremove," & _
         "e1,e2,e3,e4,e5,e6,e7,e8,e9,e0,giro,otras,aprobacion, llave, fechaope,observaciones,tipocam,subcliente)" & _
         " values ('" & ticket & "'," & iEfectivo & "," & _
         iCheque & "," & iTarjetas & "," & iNc & "," & iVales & "," & _
         iCupon & "," & iCredito & "," & iStatus & ",'" & cfecha & "'," & My.Settings.TiendaActual & "," & _
         My.Settings.CajaActual & "," & socio & "," & total & "," & iNVale & "," & _
         iNumvale & ",'" & ticket & "','" & iVisa & "','" & mastercard & "','" & amex & _
         "'," & 1 & "," & idcupon & "," & idprecios & _
         "," & idnotacre & "," & idvaleut & "," & descto & ",'" & Factura & "','" & monut & "','" & _
         mongen & "','" & _
         PrestamoE & "','" & _
         idfactura & "','" & _
         d1 & "','" & _
         d2 & "','" & _
         d3 & "','" & _
         d4 & "','" & _
         d5 & "','" & _
         d6 & "','" & _
         d7 & "','" & _
         d8 & "','" & _
         d9 & "','" & _
         d0 & "','" & _
         cf & "','" & _
         vd & "','" & _
         vi & "','" & _
         gremove & "','" & _
         e1 & "','" & _
         e2 & "','" & _
         e3 & "','" & _
         e4 & "','" & _
         e5 & "','" & _
         e6 & "','" & _
         e7 & "','" & _
         e8 & "','" & _
         e9 & "','" & _
         e0 & "','" & _
         0 & "','" & _
         0 & "','" & _
         1 & "', newid() ,getdate(),'" & 1 & "','" & My.Settings.tipoCambio & "','" & subcliente & "')"

        bdBase.bdExecute(conexion, qry)

       

            Dim plazo As Integer = 0
            Dim interes As Decimal = 0



            qry = "select plazodias,tasainteres from socios where numero = " & socio

            Dim dr As SqlDataReader

            dr = bdBase.bdDataReader(conexion, qry)

            If dr.HasRows Then
                dr.Read()
            plazo = dr.Item(0)
            interes = dr.Item(1)
                dr.Close()
            End If

            Dim hoy As DateTime = Now.Date
            Dim fecha1 As DateTime = hoy.AddDays(plazo)
            Dim vence As String = Format(fecha1, "yyyyMMdd")

        qry = "insert into creditos (tienda,numero,cliente,subcliente,total,inicial,fecha,pares,status,empleado,vence, " & _
            "plazo,caja,socio,finiint,foraneo,pagos,secuencia,interes,corte,id,llave,efectivo) values ( '" & tienda & "','" & ticket & "','" & _
                         socio & "','" & subcliente & "','" & total & "','" & total & "',getdate(),1,0,'" & vendedor & "','" & vence & "','" & plazo & _
                         "','" & caja & "','" & socio & "','" & vence & "','0','1','1','" & interes & "','1',(select max(id) from creditos) + 1 , newid(),1)"

            bdBase.bdExecute(conexion, qry)



        qry = "select idproducto from productos where codigo = 'Efectivo'"

        Dim idproducto As Integer
        Dim dr1 As SqlDataReader
        dr1 = bdBase.bdDataReader(conexion, qry)

        If dr1.HasRows Then
            dr1.Read()
            idproducto = dr.Item(0)
            dr1.Close()
        Else
            qry = "INSERT INTO [Productos] ([Codigo], [CodigoAlterno], [llave], [Estado], [FechaAlta], [UsuarioAlta], " & _
         "[UltimaModificacion], [UsuarioModificacion], [idFamilia], [idSubfamilia], [idMarca], [idTemporada], " & _
        "[idProveedor], [Costo], [Costo2], [CostoPrimer], [Precio], [Precio2], [PrecioPrimer], [Descripcion], [Observaciones], " & _
       "[Basico], [Resurtible],[Descuento],[Descuento2], [Iva],[idClasificacion]) values ( 'Efectivo', 'Efectivo', newid(), 1, getdate(), '" _
       & oLogin.pUserId.ToString.Trim & "',  getdate(), '" & oLogin.pUserId.ToString.Trim & "', '" & 1 & "', '" _
        & 1 & "', '" & 1 & "', '" & 1 _
        & "', '" & 1 & "', '" & 1 & "', '" & 1 & "', '" _
        & 1 & "', '" & 1 & "', '" & 1 & "', '" & 1 & "', 'PRESTAMO EFECTIVO', 'PRESTAMO EFECTIVO', " & 1 & " , " & 1 & ",'EFECTIVO','" _
        & 0 & "','" & 0 & "','" & 1 & "') "

            bdBase.bdExecute(conexion, qry)
            qry = "select idproducto from productos where codigo = 'Efectivo'"
            Dim dr2 As SqlDataReader
            dr2 = bdBase.bdDataReader(conexion, qry)
            If dr2.HasRows Then
                dr2.Read()
                idproducto = dr2.Item(0)
                dr2.Close()
            End If
        End If

        qry = "insert into detnotas (tienda,caja,numero,idproducto,cantidad, " & _
            "precio,descuen,empleado,fecha,llave) values ('" & tienda & "','" & caja & "','" & _
         ticket & "','" & idproducto & _
         "','" & 1 & "','" & total & "','" & _
          0 & "','" & vendedor & "','" & cfecha & "',newid())"
        bdBase.bdExecute(conexion, qry)

        MsgBox("Prestamo Registrado con Exito", MsgBoxStyle.Information)
        Me.Close()
    End Sub


    Private Function GeneraFolio()

        Dim qry As String = String.Empty
        Dim NumeroNota As String
        Dim FechaActual As String
        qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
        Dim conexionTicketVenta As String = conexion

        Dim ds As SqlDataReader = bdBase.bdDataReader(conexionTicketVenta, qry)
        ds.Read()
        FechaActual = ds("fecha")
        ds.Close()

        'qry = "SELECT CASE WHEN ticket IS NULL THEN 0 ELSE MAX(RIGHT(CONVERT(nchar(15), ticket), 5)) END AS Consecutivo " & _
        '      " FROM fma WHERE (left(CONVERT(nchar(20), ticket), 10) = '" & _
        '      My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "')"
        '
        'qry = "SELECT CASE WHEN MAX(SUBSTRING(CONVERT(nchar(15), ticket), LEN(CONVERT(nchar(15), ticket)) - 4, 4)) IS NULL THEN 0 ELSE " & _
        '      "MAX(SUBSTRING(CONVERT(nchar(15), ticket), LEN(CONVERT(nchar(15), ticket)) - 4, 4)) END AS Consecutivo " & _
        '      "FROM fma WHERE CONVERT(nchar(10), ticket) = '" & _
        '      My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "'"
        Dim nLen As Integer = Len(My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0"))
        Dim nLen2 As Integer = nLen + 1
        qry = "SELECT CASE WHEN MAX(SUBSTRING(CONVERT(nchar, ticket), " & nLen2 & ", LEN(ticket) - " & nLen & ")) IS NULL THEN 0 ELSE " & _
              "MAX(SUBSTRING(CONVERT(nchar, ticket), " & nLen2 & ", LEN(ticket) - " & nLen & ")) END AS Consecutivo " & _
              "FROM fma WHERE ticket LIKE '" & _
              My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & "%'"

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexionTicketVenta, qry)
        dr.Read()
        Dim conse As Integer
        conse = dr("Consecutivo") + 1
        dr.Close()
        NumeroNota = My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & conse.ToString.PadLeft(4, "0")
        Return NumeroNota
    End Function


    Private Sub asiste()
        Dim qry As String = ""
        Dim fechasql As String
        qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        ds.Read()
        fechasql = ds("fecha")
        ds.Close()

        ' Crear temporales
        qry = "CREATE TABLE [empleadoc3] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[FECHA] datetime NULL, [tipoope] nchar(14) NULL)"
        Dim regresa As String = bdBase.bdExecute(conexion, qry)
        qry = "CREATE TABLE [empleadoc2] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[ultimao] datetime NULL)"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Insertar datos 
        qry = "INSERT INTO empleadoc3 (numero, nombre, Usuario, fecha, tipoope) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], logdia.fecha, logdia.tipo FROM empleado " & _
              "inner join logdia on logdia.nombre= empleado.nombre " & _
              "WHERE logdia.TIENDA = '" & My.Settings.Item("TiendaActual") & "' AND  CONVERT(nCHAR(8), logdia.fecha , 112)= '" & fechasql & "' "
        regresa = bdBase.bdExecute(conexion, qry)
        qry = "INSERT INTO empleadoc2 (numero, nombre, Usuario, ultimao) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], MAX(logdia.fecha) from empleado " & _
              "inner join logdia on logdia.nombre = empleado.nombre and DATEPART(year, logdia.fecha)= DATEPART(year, getdate()) " & _
              "and DATEPART(month, logdia.fecha)=DATEPART(month, getdate()) and DATEPART(day, logdia.fecha)=DATEPART(day, getdate()) and logdia.tienda='" & _
              My.Settings.Item("TiendaActual") & "' group by empleado.numero, empleado.nombre, empleado.[USER]  order BY [user]"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Query final
        qry = "Select empleadoc3.numero, empleadoc3.nombre, empleadoc3.usuario From empleadoc2 " & _
              "INNER Join empleadoc3 On empleadoc2.numero= empleadoc3.numero And empleadoc2.ultimao= empleadoc3.fecha " & _
              "Where empleadoc3.tipoope like 'ENTRADA%'"
        LlenaCombobox(cbvendedor, qry, "nombre", "numero", conexion)
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc3') BEGIN DROP TABLE empleadoc3 END")
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc2') BEGIN DROP TABLE empleadoc2 END")

        ' *****************************************************
        ' **** terminado
        ' *****************************************************
        If cbvendedor.Properties.Items.Count >= 1 Then
            cbvendedor.SelectedIndex = 0
        Else
            cbvendedor.SelectedIndex = -1
        End If

        'addtabs()       
    End Sub

    Private Sub PrestamoEfectivo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        asiste()
        inicializa()



    End Sub

    Private Sub inicializa()
        Dim theDate As Date
        Dim qry As String
        qry = "Select getDate() as FechaHora "
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dr.Read()
        theDate = dr("FechaHora")
        dr.Close()
        cfecha = theDate.ToString("yyyyMMdd")

        qry = " SELECT     tiendas.NUMERO, tiendas.NOMBRE AS Tienda, razones.NOMBRE AS RazonSocial, razones.RFC,Razones.Calle,Razones.Ciudad,Razones.Colonia, Razones.Estado,Razones.Cp, " & _
                          " Tiendas.Calle as CalleTienda, Tiendas.Ciudad as CiudadTienda,Tiendas.estado as EstadoTienda FROM         tiendas INNER JOIN " & _
                          " razones ON tiendas.RAZON = razones.ID " & _
                          " WHERE Tiendas.numero='" & My.Settings.Item("TiendaActual") & _
                          "' ORDER BY Tienda "
        Dim ds2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        Dim bTieneRen As Boolean = False
        While ds2.Read
            bTieneRen = True
            'Label8.Text = ds2("RazonSocial").ToString.Trim
            lbtienda.Text = ds2("Tienda").ToString.Trim
            lbcaja.Text = My.Settings.CajaActual.ToString.Trim
            'Label21.Text = "RFC: " & ds2("RFC").ToString.Trim
            lbcajero.Text = oLogin.pUsrNombre
        End While
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Registraventa()
    End Sub
End Class