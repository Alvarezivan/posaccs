﻿Imports System
Imports System.IO
Imports System.Collections
Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.String
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic
Imports DevExpress.XtraEditors
Imports System.Collections.Generic
Imports System.Data
Imports DevExpress.XtraGrid.Columns
Imports System.Text
Imports System.Text.RegularExpressions
Imports Microsoft.Reporting.WinForms
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraGrid.Views.Base
Imports System.Globalization
Imports DevExpress.Utils
'Imports System.Data.SqlServerCe

Public Class Creditos
    Dim ciUSA As CultureInfo = New CultureInfo("en-US")
    Dim iSocio As String
    Dim SaldoAFavor As Boolean = False

    Private Sub Button52_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button52.Click
        Cursor = Cursors.WaitCursor
        Dim qry As String
        qry = "Select socios.id, ltrim(rtrim(socios.nombre)) as nombre,socios.numero,cbarras as [Codigo de barras] From socios Where socios.nombre like '%" & TextBox12.Text & "%' and limitecredito > 0   order by nombre "
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl17.DataSource = dr2.Tables(0)
        GridView17.PopulateColumns(dr2.Tables(0))
        GridView17.BestFitColumns()
        If TextBox1.Text.Trim.Length > 0 Then
            clickbutton1()
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub GridControl17_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl17.DoubleClick
        Dim qry As String = ""
        Dim row As System.Data.DataRow = GridView17.GetDataRow(GridView17.FocusedRowHandle)
        Dim id As String = row(2)
        oClientePago = id
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim saldopos, saldoneg As Decimal
        qry = "select CASE WHEN sum(inicial) IS NULL THEN 0 ELSE sum(inicial) END as Saldo from creditos where socio='" & oClientePago & "'"
        qry = "select CASE WHEN sum(inicial) IS NULL THEN 0 ELSE sum(inicial) / (select count(*) from creditos where socio='" & oClientePago & "' and total != 0) END as Saldo from creditos where socio='" & oClientePago & "'"
        qry = "select isnull(inicial,0) as saldo from creditos where socio ='" & oClientePago & "'  group by numero,inicial"
        qry = "select isnull(inicial,0) as saldo from creditos where socio ='" & oClientePago & "' and fecha " _
            & "between '" & (New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).ToString("yyyy-MM-dd") & "' and '" & (New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") & "' group by numero,inicial"
        qry = "select isnull(sum(total),0) as saldo from creditos where socio='" & oClientePago & "'"
        qry = "select isnull(sum(total),0) as saldo from creditos where socio='" & oClientePago & "' and total > 0"
        qry = "select  (cr.INICIAL -sum(isnull(ab.total,0)))AS SALDO from creditos as cr left join abonos as ab on cr.numero = ab.numero inner join socios as " _
            & "soc on cr.socio = soc.numero where soc.numero = '" & oClientePago & "' group by cr.numero,soc.NOMBRE, cr.INICIAL having (cr.INICIAL -sum(isnull(ab.total,0)) > 0)"
        qry = "select  (cr.INICIAL )AS SALDO from creditos as cr left join abonos as ab on cr.numero = ab.numero inner join socios as " _
            & "soc on cr.socio = soc.numero where soc.numero = '" & oClientePago & "' group by cr.numero,soc.NOMBRE, cr.INICIAL having (cr.INICIAL -sum(isnull(ab.total,0)) > 0)"
        qry = "select cr.numero,cr.inicial,cr.total as credito, isnull(sum(ab.TOTAL),0) as abonos, (cr.inicial -" _
            & "isnull(sum(ab.TOTAL),0)) as saldo from creditos as cr left join abonos as ab on cr.numero = ab.numero and cr.socio = ab.socio where cr.total > " _
            & "0 and cr.cliente = '" & oClientePago & "'  group by cr.numero,cr.inicial,cr.total having ((cr.inicial -isnull(sum(ab.TOTAL),0)) >= 0)"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        TextBox6.Text = 0
        saldopos = 0
        Do While dr.Read()
            TextBox6.Text += dr("Saldo")
            saldopos += dr("Saldo")
        Loop

        dr.Close()

        TextBox7.Text = 0
        saldoneg = 0
        qry = "select CASE WHEN sum(abono) IS NULL THEN 0 ELSE sum(abono) END as Saldo from creditos where socio='" & oClientePago & "' and abono>0"
        'qry = "select CASE WHEN sum(abono) IS NULL THEN 0 ELSE sum(abono) END as Saldo from creditos where socio='" & oClientePago & "' and abono>0 and fecha " _
        '    & "between '" & (New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).ToString("yyyy-MM-dd") & "' and '" & (New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd") & "'"
        'qry = "select isnull(sum(total),0) as saldo from abonos where cliente='" & oClientePago & "' and numero >0 "
        'qry = "select isnull(sum(total),0) as saldo from creditos where cliente='" & oClientePago & "' and numero >0 "
        dr = bdBase.bdDataReader(conexion, qry)
        While dr.Read()
            TextBox7.Text = dr("Saldo")
            saldoneg = dr("Saldo")
        End While

        dr.Close()
        TextBox8.Text = TextBox6.Text - TextBox7.Text
        TextBox8.Text = saldopos.ToString
        iSaldo = TextBox8.Text
        TextBox6.Text = FormatCurrency(TextBox6.Text)
        TextBox7.Text = FormatCurrency(TextBox7.Text)
        TextBox8.Text = FormatCurrency(TextBox8.Text)
        'If saldopos > 0 And saldoneg > 0 Then
        '    'se autoabona
        '    Dim saldoarepartir As Decimal = saldoneg
        '    qry = "select id,total from creditos where total>0 and socio='" & oClientePago & "' "
        '    Dim drpagares As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        '    Dim SaldoARedimir As Decimal = 0
        '    If drpagares.HasRows Then
        '        Do While drpagares.Read
        '            If drpagares("total") <= saldoarepartir Then
        '                If saldoarepartir >= drpagares("total") Then
        '                    SaldoARedimir = drpagares("total")
        '                Else
        '                    SaldoARedimir = saldoarepartir
        '                End If
        '                saldoarepartir = saldoarepartir - SaldoARedimir
        '                qry = "update creditos  set total=total-'" & SaldoARedimir & "' where id='" & drpagares("id") & "'"
        '                bdBase.bdExecute(conexion, qry)
        '                qry = "select id, saldoabono from creditos where socio='" & oClientePago & "' and saldoabono>0 "
        '                dr = bdBase.bdDataReader(conexion, qry)
        '                Dim abonoaquitar As Decimal = 0
        '                abonoaquitar = SaldoARedimir
        '                Do While dr.Read

        '                    If abonoaquitar > 0 Then

        '                        qry = "update creditos  set saldoabono=saldoabono-'" & abonoaquitar & "' where id='" & dr("id") & "'"
        '                        bdBase.bdExecute(conexion, qry)
        '                        If dr("saldoabono") >= abonoaquitar Then
        '                            abonoaquitar = 0
        '                        Else
        '                            abonoaquitar = abonoaquitar - dr("saldoabono")
        '                        End If

        '                    End If
        '                Loop
        '            End If
        '        Loop
        '    Else
        '        'falsa alarma.
        '    End If
        '    drpagares.Close()
        'End If


        'Abonos.ShowDialog()
        TextEdit6.Text = row(1).ToString.Trim.ToUpper

        qry = "SELECT DISTINCT creditos.NUMERO, creditos.FECHA, (creditos.INICIAL/creditos.pagos) AS Total, " & _
              "CASE WHEN vence IS NULL THEN 0 ELSE DATEDIFF(DD, vence, getdate()) END as DiasVencido, " & _
              "CASE WHEN creditos.saldoint IS NULL THEN 0 ELSE creditos.saldoint END as InteresAnterior, " & _
              "((creditos.Total*((socios.tasainteres/100))/30))*(CASE WHEN vence IS NULL THEN 0 ELSE DATEDIFF(DD, vence, getdate()) END ) as Interes, " & _
              "creditos.total AS Saldo, creditos.VENCE, creditos.SECUENCIA, creditos.SOCIO, tiendas.NOMBRE AS Tienda " & _
              "FROM creditos INNER JOIN tiendas ON creditos.TIENDA = tiendas.NUMERO INNER Join " & _
              "socios ON creditos.socio = socios.NUMERO WHERE creditos.SOCIO ='" & id & "'  and total>0  " & _
              " order by creditos.vence,creditos.NUMERO  "
        qry = "SELECT  creditos.NUMERO, creditos.FECHA, (creditos.INICIAL/creditos.pagos) AS Total, CASE WHEN vence IS NULL THEN 0 " _
            & "ELSE DATEDIFF(DD, vence, getdate()) END as DiasVencido, CASE WHEN creditos.saldoint IS NULL THEN 0 ELSE creditos.saldoint END as " _
            & "InteresAnterior, ((creditos.Total*((socios.tasainteres/100))/30))*(CASE WHEN vence IS NULL THEN 0 ELSE DATEDIFF(DD, vence, getdate()) END ) " _
            & "as Interes, creditos.total AS Saldo, creditos.VENCE, creditos.SECUENCIA, creditos.SOCIO, tiendas.NOMBRE AS Tienda, CASE WHEN sc.nombre IS NULL THEN 'TITULAR' ELSE sc.nombre END as SubCliente, creditos.efectivo as Efectivo FROM creditos INNER JOIN tiendas ON " _
            & "creditos.TIENDA = tiendas.NUMERO INNER Join socios ON creditos.socio = socios.NUMERO left join abonos as ab on creditos.numero = ab.numero and creditos.socio = ab.socio left join subsocios sc on creditos.subcliente = sc.numero WHERE " _
            & "creditos.SOCIO ='" & id & "'  and creditos.total>0 group by creditos.Efectivo, creditos.INICIAL,creditos.NUMERO,sc.nombre ,creditos.FECHA,creditos.pagos,creditos.VENCE,creditos.SALDOINT," _
            & "creditos.TOTAL,socios.TasaInteres,creditos.SECUENCIA,creditos.SOCIO,tiendas.Nombre having (creditos.INICIAL -sum(isnull(ab.total,0)) > 0) order by creditos.vence,creditos.NUMERO  "
        iSocio = id
        'and creditos.total<>0
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        DataGridView3.Rows.Clear()
        Dim pos As Integer = 0
        'If dr2.HasRows Then
        While dr2.Read()
            DataGridView3.Rows.Add()
            DataGridView3.Item(0, DataGridView3.Rows.Count - 1).Value = dr2("NUMERO")
            DataGridView3.Item(1, DataGridView3.Rows.Count - 1).Value = FormatDateTime(dr2("fecha"), DateFormat.ShortDate)
            DataGridView3.Item(2, DataGridView3.Rows.Count - 1).Value = dr2("Saldo")
            DataGridView3.Item(3, DataGridView3.Rows.Count - 1).Value = dr2("DiasVencido")
            DataGridView3.Item(4, DataGridView3.Rows.Count - 1).Value = dr2("InteresAnterior")
            If dr2("Interes") < 0 Then
                DataGridView3.Item(5, DataGridView3.Rows.Count - 1).Value = 0
            Else
                DataGridView3.Item(5, DataGridView3.Rows.Count - 1).Value = dr2("Interes")
            End If
            If dr2("Interes") + dr2("InteresAnterior") < 0 Then
                DataGridView3.Item(6, DataGridView3.Rows.Count - 1).Value = 0
            Else
                DataGridView3.Item(6, DataGridView3.Rows.Count - 1).Value = dr2("Interes") + dr2("InteresAnterior")
            End If

            DataGridView3.Item(7, DataGridView3.Rows.Count - 1).Value = DataGridView3.Item(2, DataGridView3.Rows.Count - 1).Value + DataGridView3.Item(6, DataGridView3.Rows.Count - 1).Value
            DataGridView3.Item(8, DataGridView3.Rows.Count - 1).Value = FormatDateTime(dr2("VENCE"), DateFormat.ShortDate)
            DataGridView3.Item(9, DataGridView3.Rows.Count - 1).Value = dr2("SECUENCIA")
            DataGridView3.Item(10, DataGridView3.Rows.Count - 1).Value = dr2("Tienda")
            DataGridView3.Item(12, DataGridView3.Rows.Count - 1).Value = dr2("SubCliente")
            DataGridView3.Item(13, DataGridView3.Rows.Count - 1).Value = dr2("Efectivo")
        End While
        dr2.Close()

        'End If
        'DataGridView3.DataSource = dr2.Tables(0)
        DataGridView3.AutoResizeColumns()
        Dim total As Double = 0
        Dim interes, interesnuevo, saldototal As Double
        interes = 0
        interesnuevo = 0
        saldototal = 0

        For i As Integer = 0 To DataGridView3.RowCount - 1
            total = total + CDbl(DataGridView3.Item(DataGridView3.Columns(7).Index, i).Value)
            interes = interes + CDbl(DataGridView3.Item(DataGridView3.Columns(4).Index, i).Value)
            interesnuevo = interesnuevo + CDbl(DataGridView3.Item(DataGridView3.Columns(5).Index, i).Value)
            saldototal = saldototal + CDbl(DataGridView3.Item(DataGridView3.Columns(2).Index, i).Value)
        Next
        TextBox2.Text = String.Format("{0:f2}", Convert.ToDouble(total))
        TextBox3.Text = String.Format("{0:f2}", Convert.ToDouble(interes))
        TextBox5.Text = String.Format("{0:f2}", Convert.ToDouble(interesnuevo))
        TextBox4.Text = String.Format("{0:f2}", Convert.ToDouble(saldototal))
        DataGridView2.DataSource = Nothing

        XtraTabControl4.SelectedTabPageIndex = 1


        Me.Cursor = Cursors.Default
    End Sub




    Private Sub Button53_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button53.Click
        If Val(TextEdit3.Text) = 0 Then
            Exit Sub
        End If
        If TextEdit3.Text = "" Then
            Exit Sub

        End If
        If Val(TextEdit3.Text) > Val(TextEdit2.Text) Then
            MessageBox.Show(IdiomaMensajes(437, Mensaje.Texto), IdiomaMensajes(437, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub

        End If
        Dim x As Integer = 0
        Dim suma As Decimal = 0
        For x = 0 To DataGridView2.Rows.Count - 1
            If DataGridView2.Item(0, x).Value = TextEdit1.Text And TextEdit8.Text = DataGridView2.Item(3, x).Value Then
                MessageBox.Show(IdiomaMensajes(438, Mensaje.Texto), IdiomaMensajes(438, Mensaje.Titulo), MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next
        DataGridView2.Rows.Add()
        DataGridView2.Item(0, DataGridView2.Rows.Count - 1).Value = TextEdit1.Text
        DataGridView2.Item(1, DataGridView2.Rows.Count - 1).Value = FormatNumber(TextEdit2.Text, 2)
        DataGridView2.Item(2, DataGridView2.Rows.Count - 1).Value = FormatNumber(TextEdit3.Text, 2)
        DataGridView2.Item(3, DataGridView2.Rows.Count - 1).Value = TextEdit8.Text
        DataGridView2.Item(4, DataGridView2.Rows.Count - 1).Value = TextEdit9.Text
        DataGridView2.Item(5, DataGridView2.Rows.Count - 1).Value = TextEdit10.Text
        Dim capital As Decimal
        capital = Convert.ToDecimal(TextEdit9.Text) + Convert.ToDecimal(TextEdit10.Text)
        capital = TextEdit3.Text - capital
        DataGridView2.Item(6, DataGridView2.Rows.Count - 1).Value = capital

        For x = 0 To DataGridView2.Rows.Count - 1
            suma = suma + DataGridView2.Item(2, x).Value
        Next
        TextBox9.Text = suma
    End Sub
    Dim iDiferencia, iSaldo, iPago As Decimal
    Private Sub Button54_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button54.Click
        If Val(TextEdit5.Text) <> Val(TextEdit7.Text) Then
            MessageBox.Show(IdiomaMensajes(439, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Button54.Enabled = False
        iDiferencia = 0
        If Val(TextEdit4.Text) <= 0 Then
            MessageBox.Show(IdiomaMensajes(440, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If Val(TextEdit5.Text) <= 0 Then
            MessageBox.Show(IdiomaMensajes(441, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Dim pagado As Decimal = TextEdit7.Text
        If iSaldo < 0 Then
            iSaldo = 0
        End If
        If pagado > iSaldo Then
            iDiferencia = pagado - iSaldo
            Dim msg As String = IdiomaMensajes(793, Mensaje.Texto)
            msg = String.Format(msg, FormatCurrency(iDiferencia))
            If vbYes = DevExpress.XtraEditors.XtraMessageBox.Show(msg, IdiomaMensajes(793, Mensaje.Titulo), MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                If iSaldo > 0 Then
                    iDiferencia = pagado - iSaldo
                Else
                    iDiferencia = pagado - iSaldo
                End If
            Else
                Exit Sub

            End If
        End If

        Me.Cursor = Cursors.WaitCursor
        pagar()
        DataGridView2.Rows.Clear()
        TextEdit1.Text = 0
        TextEdit2.Text = 0
        TextEdit3.Text = 0
        TextEdit4.Text = 0
        TextEdit1.Text = 0
        Cursor = Cursors.Default
    End Sub
    Private Sub DataGridView3_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView3.CellContentClick
        TextEdit1.Text = DataGridView3.Item(0, DataGridView3.CurrentRow.Index).Value
        TextEdit2.Text = DataGridView3.Item(7, DataGridView3.CurrentRow.Index).Value
        TextEdit3.Text = TextEdit2.Text
        TextEdit8.Text = DataGridView3.Item(9, DataGridView3.CurrentRow.Index).Value
        TextEdit9.Text = DataGridView3.Item(4, DataGridView3.CurrentRow.Index).Value
        TextEdit10.Text = DataGridView3.Item(5, DataGridView3.CurrentRow.Index).Value

    End Sub
    Private Sub pagar()
        Dim qry, cfecha, regresa As String
        Dim operacion, idapartado, x As String
        Dim theDate As Date
        qry = "SELECT CASE WHEN operacion IS NULL THEN 0 ELSE operacion END as operacion from tiendas where numero ='" & My.Settings.TiendaActual & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        Dim bTieneRen As Boolean = False
        Dim iOpX As Integer = 1
        While dr.Read
            bTieneRen = True
            'operacion = dr("operacion") + 1
            iOpX = dr("operacion") + 1
            operacion = GetOperacion(dr("operacion")).ToString
        End While
        If Not bTieneRen Then
            'operacion = 1
            iOpX = 1
            operacion = GetOperacion(0)
        End If
        'dr.Read()
        'operacion = dr("operacion") + 1
        dr.Close()
        qry = "update tiendas set operacion='" & iOpX & "' where numero='" & My.Settings.TiendaActual & "'"
        regresa = bdBase.bdExecute(conexion, qry)
        If conexion = conexion Then
            regresa = bdBase.bdExecute(conexion, qry)
        End If
        qry = "Select getDate() as FechaHora "
        dr = bdBase.bdDataReader(conexion, qry)
        dr.Read()
        theDate = dr("FechaHora")
        cfecha = theDate.ToString("yyyyMMdd")
        dr.Close()
        Dim interesactual As Decimal = 0
        Dim sumaabono As Decimal = 0
        Dim amonedero As Decimal = 0

        For xx As Integer = 0 To DataGridView2.RowCount - 1
            Dim montoabono, ksaldo As String
            Dim abonado, saldado As Decimal
            sumaabono = sumaabono + Convert.ToDecimal(DataGridView2.Item(6, xx).Value)
            interesactual = interesactual + Convert.ToDecimal(DataGridView2.Item(4, xx).Value)
            interesactual = interesactual + Convert.ToDecimal(DataGridView2.Item(5, xx).Value)
            montoabono = DataGridView2.Item(2, xx).Value
            montoabono = montoabono.Replace(",", "")
            qry = "Insert into abonos (tienda, numero, cliente, fecha, total, caja, corte, socio,operacion,interes, llave) " & _
             " values ('" & My.Settings.TiendaActual & "','" & DataGridView2.Item(0, xx).Value.ToString & "','" & iSocio & "', '" & cfecha & "','" & _
              montoabono + "','" & My.Settings.CajaActual & "','1','" & iSocio &
             "','" & operacion & "','0', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER) )"
            If SaldoAFavor = False Then
                regresa = bdBase.bdExecute(conexion, qry)
                If conexion = conexion Then
                    regresa = bdBase.bdExecute(conexion, qry)
                End If
            End If
            'montoabono = DataGridView2.Item(6, x).Value
            If SaldoAFavor = False Then
                qry = "Update creditos Set Total = Total - '" & montoabono & "', saldoint='" & DataGridView2.Item(4, xx).Value & "'  Where socio ='" & iSocio & "' And numero = '" & DataGridView2.Item(0, xx).Value & "' and secuencia='" & DataGridView2.Item(3, xx).Value & "'"
                regresa = bdBase.bdExecute(conexion, qry)
                If conexion = conexion Then
                    regresa = bdBase.bdExecute(conexion, qry)
                End If
            Else
                abonado = montoabono
                saldado = Convert.ToDecimal(DataGridView2.Item(1, xx).Value)
                If abonado >= saldado Then
                    qry = "Update creditos Set Total = Total - '" & abonado & "'  Where socio ='" & iSocio & "' And numero = '" & DataGridView2.Item(0, xx).Value & "' and secuencia='" & DataGridView2.Item(3, xx).Value & "'"
                    regresa = bdBase.bdExecute(conexion, qry)
                    If conexion = conexion Then
                        regresa = bdBase.bdExecute(conexion, qry)
                    End If
                End If
            End If

            iSaldo = TextBox8.Text - TextEdit7.Text
            If iSaldo <= 0 Then
                qry = "Update creditos Set Total =0  Where socio ='" & iSocio & "' "
                regresa = bdBase.bdExecute(conexion, qry)
                If conexion = conexion Then
                    regresa = bdBase.bdExecute(conexion, qry)
                End If
            End If

        Next


        If SaldoAFavor = True Then
            'ínserto en creditos
            Dim montonegativo As Decimal
            montonegativo = TextEdit7.Text
            If iDiferencia > 0 Then
                qry = "Update creditos set saldoabono=0 where saldoabono>0"
                regresa = bdBase.bdExecute(conexion, qry)
                If conexion = conexion Then
                    regresa = bdBase.bdExecute(conexion, qry)
                End If
            End If
            qry = "INSERT INTO creditos (tienda,numero,cliente,socio,fecha,total,inicial,iva,pares, " & _
               "status,empleado,plazo,caja,corte,foraneo,interes,pagos,secuencia,vence,finiint,operacion,abono,tipo,saldoabono, llave) " & _
     "VALUES (" & My.Settings.TiendaActual & ",-6,'" & iSocio & "','" & iSocio & "','" & cfecha & "','" & 0 & "'" & _
     ",'" & 0 & "','0','1','0'," & oLogin.pUserId & ",'300','" & _
            My.Settings.CajaActual & "','1','0','0','1','1','" & cfecha & "','" & cfecha & "','" & operacion & "','" & montonegativo & "','1','" & iDiferencia & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER) )"
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
            qry = "Insert into abonos (tienda, numero, cliente, fecha, total, caja, corte, socio,operacion,interes, llave) " & _
      " values ('" & My.Settings.TiendaActual & "','-5','" & iSocio & "', '" & cfecha & "','" & _
       iDiferencia & "','" & My.Settings.CajaActual & "','1','" & iSocio &
      "','" & operacion & "','0', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER) )"
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
        Else
            Dim montonegativo As Decimal
            montonegativo = TextEdit7.Text
            If iDiferencia > 0 Then
                qry = "Update creditos set saldoabono=0 where saldoabono>0"
                regresa = bdBase.bdExecute(conexion, qry)
                If conexion = conexion Then
                    regresa = bdBase.bdExecute(conexion, qry)
                End If
            End If
            qry = "INSERT INTO creditos (tienda,numero,cliente,socio,fecha,total,inicial,iva,pares, " & _
               "status,empleado,plazo,caja,corte,foraneo,interes,pagos,secuencia,vence,finiint,operacion,abono,tipo,saldoabono, llave) " & _
     "VALUES (" & My.Settings.TiendaActual & ",-6,'" & iSocio & "','" & iSocio & "','" & cfecha & "','" & 0 & "'" & _
     ",'" & 0 & "','0','1','0'," & oLogin.pUserId & ",'300','" & _
            My.Settings.CajaActual & "','1','0','0','1','1','" & cfecha & "','" & cfecha & "','" & operacion & "','" & montonegativo & "','2','" & iDiferencia & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER) )"
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
            qry = "Insert into abonos (tienda, numero, cliente, fecha, total, caja, corte, socio,operacion,interes, llave) " & _
" values ('" & My.Settings.TiendaActual & "','-6','" & iSocio & "', '" & cfecha & "','" & _
montonegativo & "','" & My.Settings.CajaActual & "','1','" & iSocio &
"','" & operacion & "','0', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER) )"
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
        End If
        '   If TextEdit7.Text > TextEdit4.Text Then
        '       Dim montonegativo As Decimal
        '       montonegativo = TextEdit7.Text - TextEdit4.Text
        '       montonegativo = montonegativo * -1
        '       qry = "INSERT INTO creditos (tienda,numero,cliente,socio,fecha,total,inicial,iva,pares, " & _
        '          "status,empleado,plazo,caja,corte,foraneo,interes,pagos,secuencia,vence,finiint,operacion) " & _
        '"VALUES (" & My.Settings.TiendaActual & ",-2,'" & iSocio & "','" & iSocio & "','" & cfecha & "','" & montonegativo & "'" & _
        '",'" & montonegativo & "','0','1','0'," & oLogin.pUserId & ",'300','" & _
        '       My.Settings.CajaActual & "','1','0','0','1','1','" & cfecha & "','" & cfecha & "','" & operacion & "' )"
        '       bdBase.bdExecute(conexion, qry)
        '   End If


        If interesactual > 0 Then
        Else
            amonedero = sumaabono * 0.02
            qry = "INSERT INTO monedero  (tienda,ticket,fecha,total,empleado,caja,socio,operacion, llave) VALUES ('" & _
            My.Settings.TiendaActual & "','-1','" & cfecha + "','" & amonedero & _
            "','-1','" & My.Settings.CajaActual & "','" & iSocio & "','0', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER))"
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
        End If

        If sumaabono < Val(TextEdit7.Text) Then
            Dim nuevosaldo As Decimal
            nuevosaldo = Val(TextEdit7.Text) - sumaabono
            regresa = bdBase.bdExecute(conexion, qry)
            If conexion = conexion Then
                regresa = bdBase.bdExecute(conexion, qry)
            End If
        End If



        Dim x0, x1, x2, x3, x4, x5, x6, x7, x8, x9 As Decimal
        Dim d1, d2, d3, d4, d5, d6, d7, d8, d9, d0, e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, CF, CH, CU, EF, TC, VC, VD, VI As Decimal
        d1 = 0
        d2 = 0
        d3 = 0
        d4 = 0
        d5 = 0
        d6 = 0
        d7 = 0
        d8 = 0
        d9 = 0
        d0 = 0
        e1 = 0
        e2 = 0
        e3 = 0
        e4 = 0
        e5 = 0
        e6 = 0
        e7 = 0
        e8 = 0
        e9 = 0
        e0 = 0
        CF = 0
        CH = 0
        CU = 0
        EF = 0
        TC = 0
        VC = 0
        VD = 0
        VI = 0
        x0 = 0
        x1 = 0
        x2 = 0
        x3 = 0
        x4 = 0
        x5 = 0
        x6 = 0
        x7 = 0
        x8 = 0
        x9 = 0
        For c As Integer = 0 To DataGridView4.RowCount - 1
            Select Case DataGridView4.Item(0, c).Value.ToString.Trim
                Case "D1"
                    d1 = DataGridView4.Item(2, c).Value
                Case "D2"
                    d2 = DataGridView4.Item(2, c).Value
                Case "D3"
                    d3 = DataGridView4.Item(2, c).Value
                Case "D4"
                    d4 = DataGridView4.Item(2, c).Value
                Case "D5"
                    d5 = DataGridView4.Item(2, c).Value
                Case "D6"
                    d6 = DataGridView4.Item(2, c).Value
                Case "D7"
                    d7 = DataGridView4.Item(2, c).Value
                Case "D8"
                    d8 = DataGridView4.Item(2, c).Value
                Case "D9"
                    d9 = DataGridView4.Item(2, c).Value
                Case "D0"
                    d0 = DataGridView4.Item(2, c).Value
                Case "E1"
                    e1 = DataGridView4.Item(2, c).Value
                Case "E2"
                    e2 = DataGridView4.Item(2, c).Value
                Case "E3"
                    e3 = DataGridView4.Item(2, c).Value
                Case "E4"
                    e4 = DataGridView4.Item(2, c).Value
                Case "E5"
                    e5 = DataGridView4.Item(2, c).Value
                Case "E6"
                    e6 = DataGridView4.Item(2, c).Value
                Case "E7"
                    e7 = DataGridView4.Item(2, c).Value
                Case "E8"
                    e8 = DataGridView4.Item(2, c).Value
                Case "E9"
                    e9 = DataGridView4.Item(2, c).Value
                Case "E0"
                    e0 = DataGridView4.Item(2, c).Value
                Case "CF"
                    CF = DataGridView4.Item(2, c).Value
                Case "CH"
                    CH = DataGridView4.Item(2, c).Value
                Case "CU"
                    CU = DataGridView4.Item(2, c).Value
                Case "EF"
                    EF = DataGridView4.Item(2, c).Value
                Case "TC"
                    TC = DataGridView4.Item(2, c).Value
                Case "VC"
                    VC = DataGridView4.Item(2, c).Value
                Case "VD"
                    VD = DataGridView4.Item(2, c).Value
                Case "VI"
                    VI = DataGridView4.Item(2, c).Value
                Case Is = "X0"
                    x0 = DataGridView4.Item(2, c).Value
                Case Is = "X1"
                    x1 = DataGridView4.Item(2, c).Value
                Case Is = "X2"
                    x2 = DataGridView4.Item(2, c).Value
                Case Is = "X3"
                    x3 = DataGridView4.Item(2, c).Value
                Case Is = "X4"
                    x4 = DataGridView4.Item(2, c).Value
                Case Is = "X5"
                    x5 = DataGridView4.Item(2, c).Value
                Case Is = "X6"
                    x6 = DataGridView4.Item(2, c).Value
                Case Is = "X7"
                    x7 = DataGridView4.Item(2, c).Value
                Case Is = "X8"
                    x8 = DataGridView4.Item(2, c).Value
                Case Is = "X9"
                    x9 = DataGridView4.Item(2, c).Value
            End Select
        Next

        Dim formasPagoExtraFields As String = ""
        Dim formasPagoExtraValues As String = ""
        qry = "IF COL_LENGTH('fma','X0') IS NULL BEGIN select 'NO' END else select 'SI'"
        Using drExistenFormasPagoExtra As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If drExistenFormasPagoExtra IsNot Nothing Then
                If drExistenFormasPagoExtra.HasRows Then
                    drExistenFormasPagoExtra.Read()
                    If drExistenFormasPagoExtra(0) = "SI" Then
                        formasPagoExtraFields = ",x0,x1,x2,x3,x4,x5,x6,x7,x8,x9"
                        formasPagoExtraValues = "," & x0 & "," & x1 & "," & x2 & "," & x3 & "," & x4 & "," & x5 & "," & x6 & "," & x7 & "," & x8 & "," & x9 & ""
                    End If
                End If
            End If
        End Using

        qry = " Insert Into fmacre (caja, fecha,efectivo,tienda,idmov,d1,d2,d3,d4,d5,d6,d7,d8,d9,d0,cf,vd,vi,e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, llave,tipocam,cheques" & formasPagoExtraFields & ") values('" & _
          My.Settings.CajaActual & "','" & _
          cfecha & "','" & _
          EF & "','" & _
          My.Settings.TiendaActual & "','" & _
        operacion & "','" & _
        d1 & "','" & _
        d2 & "','" & _
        d3 & "','" & _
        d4 & "','" & _
        d5 & "','" & _
        d6 & "','" & _
        d7 & "','" & _
        d8 & "','" & _
        d9 & "','" & _
        d0 & "','" & _
        CF & "','" & _
        VD & "','" & _
        VI & "','" & _
        e0 & "','" & _
        e1 & "','" & _
        e2 & "','" & _
        e3 & "','" & _
        e4 & "','" & _
        e5 & "','" & _
        e6 & "','" & _
        e7 & "','" & _
        e8 & "','" & _
        e9 & "', CAST('" & Guid.NewGuid().ToString & "' AS UNIQUEIDENTIFIER),'" & My.Settings.tipoCambio & "','" & CH & "'" & formasPagoExtraValues & ")"
        regresa = bdBase.bdExecute(conexion, qry)

        Dim ds As New DataSet()
        Dim tabla As DataTable = New DataTable()
        tabla.Columns.Add("ID", GetType(Integer))
        tabla.Columns("ID").AutoIncrement = True
        tabla.Columns("ID").AutoIncrementSeed = 1
        tabla.Columns.Add("Tienda", GetType(String))
        tabla.Columns.Add("RazonSocial", GetType(String))
        tabla.Columns.Add("RFC", GetType(String))
        tabla.Columns.Add("CalleRazon", GetType(String))
        tabla.Columns.Add("CiudadRazon", GetType(String))
        tabla.Columns.Add("ColoniaRazon", GetType(String))
        tabla.Columns.Add("EstadoRazon", GetType(String))
        tabla.Columns.Add("CP", GetType(String))
        tabla.Columns.Add("Calle", GetType(String))
        tabla.Columns.Add("Ciudad", GetType(String))
        tabla.Columns.Add("Estado", GetType(String))
        tabla.Columns.Add("Cantidad", GetType(Decimal))
        tabla.Columns.Add("Descripcion", GetType(String))
        tabla.Columns.Add("Destino", GetType(String))
        tabla.Columns.Add("Anticipo", GetType(String))
        tabla.Columns.Add("Vence", GetType(String))
        tabla.Columns.Add("Numero", GetType(String))
        tabla.Columns.Add("Indicaciones", GetType(String))
        tabla.Columns.Add("Colonia", GetType(String))
        tabla.Columns.Add("SaldoCapital", GetType(String))
        tabla.Columns.Add("InteresGenerado", GetType(String))
        tabla.Columns.Add("InteresAnterior", GetType(String))
        tabla.Columns.Add("Interes", GetType(String))
        tabla.PrimaryKey = New DataColumn() {tabla.Columns("ID")}
        ds.Tables.Add(tabla)

        qry = " SELECT     tiendas.NOMBRE AS Tienda, tiendas.colonia as col, tiendas.CIUDAD as CiudadTienda, tiendas.ESTADO as EstadoTienda, tiendas.CALLE as CalleTienda, tiendas.COLONIA, razones.NOMBRE AS RazonSocial, razones.RFC, " & _
                      " razones.CALLE AS CalleRazon, razones.COLONIA AS ColoniaRazon, razones.CIUDAD AS CiudadRazon, razones.ESTADO AS EstadoRazon, razones.CP " & _
" FROM         tiendas INNER JOIN " & _
                      " razones ON tiendas.RAZON = razones.ID " & _
" WHERE     (tiendas.NUMERO = '" & My.Settings.TiendaActual & "')"
        Dim dsr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dsr.Read()
        qry = "select ltrim(rtrim(nombre)) as nombre from socios where numero='" & iSocio & "'"
        Dim dsr1 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        dsr1.Read()
        Dim saldointeres, interesanterior, capital As Decimal
        qry = "SELECT CASE WHEN sum(creditos.saldoint) IS NULL THEN 0 ELSE sum(creditos.saldoint) END as InteresAnterior, " & _
              "CASE WHEN sum(((creditos.Total*((socios.tasainteres/100))/30))*(DATEDIFF(DD, vence, getdate()))) IS NULL THEN " & _
              "0 ELSE sum(((creditos.Total*((socios.tasainteres/100))/30))*(DATEDIFF(DD, vence, getdate()))) END as Interes, " & _
              "CASE WHEN sum(creditos.TOTAL) IS NULL THEN 0 ELSE sum(creditos.TOTAL) END AS Saldo " & _
              "FROM creditos INNER Join  socios ON creditos.socio = socios.NUMERO " & _
              "WHERE creditos.SOCIO ='" & iSocio & "' and creditos.total>0    "
        Dim drg As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        'If drg.HasRows Then
        drg.Read()
        If IsDBNull(drg("interes")) Then
            saldointeres = 0
        Else
            saldointeres = drg("interes")

        End If

        If IsDBNull(drg("InteresAnterior")) Then

            interesanterior = 0
        Else

            interesanterior = drg("InteresAnterior")

        End If

        If IsDBNull(drg("saldo")) Then
            capital = 0
        Else
            capital = drg("Saldo")
        End If
        drg.Close()

        'End If
        If DataGridView2.Rows.Count > 0 Then
            For i As Integer = 0 To DataGridView2.Rows.Count - 1
                Dim row As DataRow = tabla.NewRow()
                row("Cantidad") = DataGridView2.Item(2, i).Value
                row("Descripcion") = " Pago a Pagaré:" & DataGridView2.Item(0, i).Value.ToString.Trim
                row("Tienda") = dsr("Tienda")
                row("RazonSocial") = dsr("RazonSocial")
                row("RFC") = dsr("RFC")
                row("CalleRazon") = dsr("CalleRazon")
                row("CiudadRazon") = dsr("CiudadRazon")
                row("ColoniaRazon") = dsr("ColoniaRazon")
                row("EstadoRazon") = dsr("EstadoRazon")
                row("CP") = dsr("CP")
                row("Calle") = dsr("CalleTienda")
                row("Ciudad") = dsr("Ciudadtienda")
                row("Colonia") = dsr("col")
                row("Estado") = dsr("EstadoTienda")
                row("Destino") = dsr1("nombre").ToString.Trim.ToUpper
                row("Anticipo") = operacion
                row("Vence") = Date.Now.AddDays(My.Settings.ValidezApartado)
                row("Numero") = 0
                'row("Indicaciones") = FormatCurrency((TextBox2.Text - TextEdit5.Text), 2)
                iSaldo = TextBox8.Text
                row("Indicaciones") = FormatCurrency((iSaldo - TextEdit7.Text), 2)
                row("SaldoCapital") = capital
                row("InteresGenerado") = saldointeres
                row("InteresAnterior") = interesanterior
                row("Interes") = interesactual
                tabla.Rows.Add(row)
            Next
        Else
            Dim row As DataRow = tabla.NewRow()
            row("Cantidad") = TextEdit5.Text
            row("Descripcion") = " Anticipo de Crédito:"
            row("Tienda") = dsr("Tienda")
            row("RazonSocial") = dsr("RazonSocial")
            row("RFC") = dsr("RFC")
            row("CalleRazon") = dsr("CalleRazon")
            row("CiudadRazon") = dsr("CiudadRazon")
            row("ColoniaRazon") = dsr("ColoniaRazon")
            row("EstadoRazon") = dsr("EstadoRazon")
            row("CP") = dsr("CP")
            row("Calle") = dsr("CalleTienda")
            row("Ciudad") = dsr("Ciudadtienda")
            row("Colonia") = dsr("col")
            row("Estado") = dsr("EstadoTienda")
            row("Destino") = dsr1("nombre").ToString.Trim.ToUpper
            row("Anticipo") = operacion
            row("Vence") = Date.Now.AddDays(My.Settings.ValidezApartado)
            row("Numero") = 0
            'row("Indicaciones") = FormatCurrency((TextBox2.Text - TextEdit5.Text), 2)
            iSaldo = TextBox8.Text
            row("Indicaciones") = FormatCurrency((iSaldo - TextEdit7.Text), 2)
            row("SaldoCapital") = capital
            row("InteresGenerado") = saldointeres
            row("InteresAnterior") = interesanterior
            row("Interes") = interesactual
            tabla.Rows.Add(row)
        End If
        dsr.Close()
        dsr1.Close()

        ReportViewer2.Visible = True
        ReportViewer2.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
        ReportViewer2.LocalReport.ReportPath = "C:\saz\saz\tickets\PagoPagare.rdlc"
        ReportViewer2.RefreshReport()
        ReportViewer2.LocalReport.EnableExternalImages = True
        Dim params As ReportParameter() = New ReportParameter(0) {}
        params(0) = New ReportParameter("logotipo", "file:///" & "C:\Saz\logos\logo.jpg")
        'params(1) = New ReportParameter("Cajero", oLogin.pUsrNombre)
        ReportViewer2.LocalReport.SetParameters(params)
        ReportViewer2.LocalReport.DataSources.Clear()
        ReportViewer2.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", ds.Tables(0)))
        ReportViewer2.DocumentMapCollapsed = True
        ReportViewer2.RefreshReport()

        'Dim impresion As PrintReport
        'impresion = New PrintReport(ReportViewer2.LocalReport)
        ''impresion.PrinterSettings.Copies = My.Settings.Traspaso
        'For xy As Integer = 1 To My.Settings.Apartado
        '    impresion.Print()
        'Next

    End Sub

    Private Sub DataGridView4_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView4.CellValueChanged
        Dim x As Integer
        Dim tota, totnotas As Decimal
        For x = 0 To DataGridView4.RowCount - 1
            If Not IsDBNull(DataGridView4.Item(2, x).Value) Then
                tota = tota + ((Convert.ToDecimal(DataGridView4.Item(2, x).Value)))
            End If
        Next
        TextEdit5.Text = Val(tota)
    End Sub

    Private Sub llenafmas()
        Dim qry As String = "SELECT Clave,Nombre,0.00 as Monto from formaspago where status=1 and clave<>'VC' and clave <>'NC' and tienda='" & My.Settings("TiendaActual") & "'ORDER BY nombre"
        Dim dsl As DataSet = bdBase.bdDataset(conexion, qry)
        DataGridView4.DataSource = dsl.Tables(0)
        DataGridView4.Columns(0).Visible = False
        DataGridView4.Columns(1).ReadOnly = True
        Dim col As DataGridViewColumn = DataGridView4.Columns(2)
        ' Le establecemos un formato decimal.
        'col.DefaultCellStyle.Format = "N2"
        'DataGridView3.Columns(2).DefaultCellStyle.Format = "C"
        DataGridView4.Columns(0).Visible = False
        DataGridView4.AutoResizeColumns()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        clickbutton1()
    End Sub

    Private Sub clickbutton1()
        Dim qry As String
        qry = "Select socios.id, ltrim(rtrim(socios.nombre)) as nombre,socios.numero From socios Where socios.nombre like '%" & TextBox1.Text & "%' and limitecredito > 0   order by nombre "
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl1.DataSource = dr2.Tables(0)
        GridView1.PopulateColumns(dr2.Tables(0))
        GridView1.BestFitColumns()
    End Sub

    Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
        Cursor = Cursors.WaitCursor
        Dim fechadel, fechaal, qry As String
        fechadel = Date.Parse(DateTimePicker2.Value).Date.ToString("yyyyMMdd")
        fechaal = Date.Parse(DateTimePicker1.Value).Date.ToString("yyyyMMdd")
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim id As String = row(2)
        iSocio = id
        'saco los pagos
        '        qry = " SELECT        tiendas.NOMBRE AS Tienda, abonos.NUMERO AS Pagare, abonos.FECHA, abonos.TOTAL, abonos.OPERACION, empleado.NOMBRE AS Cajero " & _
        '                " FROM            abonos INNER JOIN " & _
        '                " tiendas ON abonos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN " & _
        '                " empleado ON abonos.EMPLEADO = empleado.numero " & _
        '" WHERE        (abonos.CLIENTE = '" & iSocio & "')  and    (abonos.FECHA >='" & fechadel & "' AND abonos.FECHA <= '" & fechaal & "') " & _
        '" ORDER BY abonos.FECHA "

        qry = " SELECT        tiendas.NOMBRE AS Tienda, creditos.NUMERO AS Pagare, creditos.FECHA, creditos.abono as Total, creditos.OPERACION, empleado.NOMBRE AS Cajero " & _
        " FROM            creditos INNER JOIN " & _
        " tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN " & _
        " empleado ON creditos.EMPLEADO = empleado.numero " & _
" WHERE        (creditos.socio = '" & iSocio & "')  and    (creditos.FECHA >='" & fechadel & "' AND creditos.FECHA <= '" & fechaal & "') and creditos.abono>0 " & _
" ORDER BY creditos.FECHA "

        qry = "select ti.Nombre as TIENDA,ab.numero as PAGARE,ab.FECHA,sum(ab.total) as TOTAL,em.nombre AS CAJERO " _
            & "from abonos as ab inner join tiendas as ti on ti.numero = ab.tienda inner join creditos as cr on ab.numero = " _
            & "cr.numero and cr.socio = ab.cliente left join empleado as em on cr.empleado = em.numero WHERE        (ab.cliente = '" & iSocio & "')  and    (ab.FECHA " _
            & ">='" & fechadel & "' AND ab.FECHA <= '" & fechaal & "') and ab.numero > 0 and cr.secuencia = '1' group by ab.numero,ab.fecha,ti.nombre,em.nombre"
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        DataGridView6.DataSource = dr2.Tables(0)
        DataGridView6.AutoResizeColumns()
        Dim highlightCellStyle As New DataGridViewCellStyle


        Dim currencyCellStyle As New DataGridViewCellStyle
        currencyCellStyle.Format = "C"

        With Me.DataGridView6
            .Columns("Total").DefaultCellStyle = currencyCellStyle
        End With

        'saco las compras
        qry = "SELECT        tiendas.Nombre AS Tienda, creditos.NUMERO AS NumeroDeVenta, cast((creditos.INICIAL/creditos.pagos) as decimal(20,2)) AS Monto, creditos.TOTAL AS Saldo,  " & _
                        " creditos.PARES AS Pares, creditos.OPERACION AS Operacion, creditos.FECHA AS Fecha, (creditos.vence) AS Vencimiento, empleado.NOMBRE AS Empleado " & _
" FROM            creditos INNER JOIN " & _
                         " tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN " & _
                         " empleado ON creditos.EMPLEADO = empleado.numero " & _
" WHERE        (creditos.SOCIO = '" & iSocio & "') AND (creditos.FECHA >='" & fechadel & "' AND creditos.FECHA <= '" & fechaal & "') and creditos.inicial>0 " & _
" ORDER BY creditos.Fecha "
        dr2 = bdBase.bdDataset(conexion, qry)
        DataGridView1.DataSource = dr2.Tables(0)
        DataGridView1.AutoResizeColumns()
        DataGridView5.Rows.Clear()

        ' saco el saldo
        qry = " select isnull(sum(inicial/pagos),0) as saldo from creditos  where socio='" & iSocio & "' and fecha < '" & fechadel & "'"
        qry = "select CASE WHEN sum(inicial) IS NULL THEN 0 ELSE sum(inicial) END as comprado from creditos  where socio='" & iSocio & "' and fecha < '" & fechadel & "'"
        qry = " select isnull(sum(inicial),0) as comprado from creditos  where socio='" & iSocio & "' and fecha between '" & fechadel & "' and '" & fechaal & "'"
        qry = " select isnull(sum(inicial),0) as comprado from creditos  where socio='" & iSocio & "' and fecha < '" & fechadel & "'"
        Dim drsaldo As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        Dim qry2 As String = "SELECT CASE WHEN sum(total) IS NULL THEN 0 ELSE sum(total) END as abonado FROM abonos WHERE socio='" & iSocio & "'  and DATEADD(DD, 0, DATEDIFF(DD, 0, FECHA)) < '" & fechadel & "'"
        qry2 = "SELECT CASE WHEN sum(ab.total) IS NULL THEN 0 ELSE sum(ab.total) END as abonado FROM abonos as ab inner join " _
            & "creditos as cr on ab.NUMERO = cr.NUMERO and cr.socio = ab.cliente WHERE cr.socio='" & iSocio & "'  " _
            & "and cr.fecha between '" & fechadel & "' and '" & fechaal & "'"
        qry2 = "SELECT CASE WHEN sum(ab.total) IS NULL THEN 0 ELSE sum(ab.total) END as abonado FROM abonos as ab inner join " _
           & "creditos as cr on ab.NUMERO = cr.NUMERO and cr.socio = ab.cliente WHERE cr.socio='" & iSocio & "'  " _
           & "and cr.fecha between '" & fechadel & "' and '" & fechaal & "' and cr.numero > 0"
        qry2 = "SELECT CASE WHEN sum(ab.total) IS NULL THEN 0 ELSE sum(ab.total) END as abonado FROM abonos as ab inner join " _
           & "creditos as cr on ab.NUMERO = cr.NUMERO and cr.socio = ab.socio WHERE cr.socio='" & iSocio & "'  " _
           & "and cr.fecha < '" & fechadel & "' and cr.numero > 0"
        Dim drsaldo2 As SqlDataReader = bdBase.bdDataReader(conexion, qry2)

        Dim saldoi, saldof, saldoa As Decimal

        saldoi = 0
        While drsaldo.Read()
            saldoi = drsaldo("comprado")
        End While
        drsaldo.Close()
        saldof = saldoi
        While drsaldo2.Read()
            saldof = saldoi - drsaldo2("abonado")
        End While
        drsaldo2.Close()

        'HAGO EL CARDEX DE CREDITO

        '        qry = "SELECT       'Venta' AS Tipo, tiendas.Nombre AS Tienda, creditos.NUMERO AS NumeroDeVenta, creditos.FECHA AS Fecha,  creditos.INICIAL/creditos.pagos AS Compra, 0 as Abono,  " & _
        '                       "  creditos.VENCE AS Vencimiento, empleado.NOMBRE AS Empleado, 2 as NC,creditos.operacion  " & _
        '" FROM            creditos INNER JOIN " & _
        '                      "   tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN " & _
        '                       "  empleado ON creditos.EMPLEADO = empleado.numero " & _
        '" WHERE        (creditos.SOCIO = '" & iSocio & "') AND (creditos.FECHA >= '" & fechadel & "') AND (creditos.FECHA <= '" & fechaal & "') and creditos.inicial>0 " & _
        '  "      union  " & _
        '" SELECT     'Abono' AS Tipo, tiendas.Nombre AS Tienda, 0 as   NumeroDeVenta, creditos.FECHA AS Fecha, 0 AS Compra, SUM(creditos.abono) AS abono, " & _
        '                   "   creditos.FECHA AS Vencimiento, ISNULL(empleado.NOMBRE, '') AS Empleado, ISNULL(creditos.nc, 0) AS NC, creditos.operacion " & _
        '" FROM         creditos INNER JOIN " & _
        '                   "   tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN " & _
        '                   "    empleado ON creditos.EMPLEADO = empleado.numero " & _
        '" WHERE        (creditos.SOCIO = '" & iSocio & "') AND (creditos.FECHA >= '" & fechadel & "') AND (creditos.FECHA <= '" & fechaal & "') and creditos.abono>0 " & _
        '" GROUP BY creditos.OPERACION,tiendas.Nombre ,creditos.FECHA ,empleado.NOMBRE ,creditos.nc  order by creditos.fecha,creditos.operacion   "

        qry = "SELECT 'Venta' AS Tipo, tiendas.Nombre AS Tienda, creditos.NUMERO AS NumeroDeVenta, creditos.FECHA AS Fecha, " & _
              "creditos.INICIAL/creditos.pagos AS Compra, 0 as Abono, creditos.VENCE AS Vencimiento, empleado.NOMBRE AS Empleado,fma.Observaciones, 2 as NC,creditos.operacion " & _
              "FROM creditos INNER JOIN tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN empleado ON creditos.EMPLEADO = empleado.numero " & _
              " inner join fma on creditos.numero = fma.ticket WHERE (creditos.SOCIO = '" & iSocio & "') AND (creditos.FECHA >= '" & fechadel & "') AND (creditos.FECHA <= '" & fechaal & "') and creditos.inicial>0 " & _
              "union " & _
              "SELECT 'Abono' AS Tipo, tiendas.Nombre AS Tienda, 0 as   NumeroDeVenta, creditos.FECHA AS Fecha, 0 AS Compra, SUM(creditos.abono) AS abono, " & _
              "creditos.FECHA AS Vencimiento, CASE WHEN empleado.NOMBRE IS NULL THEN '' ELSE empleado.NOMBRE END AS Empleado,'' as Observaciones, " & _
              "CASE WHEN creditos.nc IS NULL THEN 0 ELSE creditos.nc END AS NC, creditos.operacion FROM creditos INNER JOIN " & _
              "tiendas ON creditos.TIENDA = tiendas.NUMERO LEFT OUTER JOIN empleado ON creditos.EMPLEADO = empleado.numero " & _
              "WHERE (creditos.SOCIO = '" & iSocio & "') AND (creditos.FECHA >= '" & fechadel & "') AND (creditos.FECHA <= '" & fechaal & "') and creditos.abono>0 " & _
              "GROUP BY creditos.OPERACION,tiendas.Nombre ,creditos.FECHA ,empleado.NOMBRE ,creditos.nc  order by creditos.fecha,creditos.operacion"


        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)


        DataGridView5.Rows.Add()
        DataGridView5.Item(0, DataGridView5.Rows.Count - 1).Value = "Saldo Anterior."
        DataGridView5.Item(6, DataGridView5.Rows.Count - 1).Value = saldof

        Dim dt As DataTable = ds.Tables(0)
        Dim elsaldo As Decimal = saldof
        Dim LosAbonos, LasCompras As Decimal
        LosAbonos = 0
        LasCompras = 0
        If Not DataGridView5.Columns.Contains("Observaciones") Then
            DataGridView5.Columns.Add("Observaciones", "Observaciones")
        End If
        'DataGridView5.Columns("Observaciones").DisplayIndex = 4
        For Each renglon As DataRow In dt.Rows
            DataGridView5.Rows.Add()
            If renglon("Tipo") = "Abono" Then
                If renglon("NC") = 0 Then
                    DataGridView5.Item(0, DataGridView5.Rows.Count - 1).Value = renglon("Tipo")
                Else
                    DataGridView5.Item(0, DataGridView5.Rows.Count - 1).Value = "Devolución"
                End If
            Else

                DataGridView5.Item(0, DataGridView5.Rows.Count - 1).Value = "Venta"

            End If
            DataGridView5.Item(1, DataGridView5.Rows.Count - 1).Value = renglon("Tienda")
            If renglon("NumeroDeVenta") = 0 Then
            Else
                DataGridView5.Item(2, DataGridView5.Rows.Count - 1).Value = renglon("NumeroDeVenta")

            End If
            DataGridView5.Item(3, DataGridView5.Rows.Count - 1).Value = renglon("Fecha")
            If renglon("Compra") <> 0 Then
                DataGridView5.Item(4, DataGridView5.Rows.Count - 1).Value = renglon("Compra")
                elsaldo = elsaldo + renglon("Compra")
                LasCompras = LasCompras + renglon("Compra")
            End If
            If renglon("Abono") > 0 Then
                DataGridView5.Item(5, DataGridView5.Rows.Count - 1).Value = renglon("Abono")
                elsaldo = elsaldo - renglon("Abono")
                LosAbonos = LosAbonos + renglon("Abono")
            End If
            DataGridView5.Item(6, DataGridView5.Rows.Count - 1).Value = elsaldo
            DataGridView5.Item(7, DataGridView5.Rows.Count - 1).Value = renglon("operacion")
            DataGridView5.Item(8, DataGridView5.Rows.Count - 1).Value = renglon("observaciones")
        Next

        DataGridView5.Sort(DataGridView5.Columns(7), ListSortDirection.Ascending)
        DataGridView5.Rows.Add()
        DataGridView5.Item(4, DataGridView5.Rows.Count - 1).Value = LasCompras
        DataGridView5.Item(5, DataGridView5.Rows.Count - 1).Value = LosAbonos

        For y As Integer = 0 To DataGridView5.Rows.Count - 1
            If DataGridView5.Item(0, y).Value = "Venta" And DataGridView5.Item(4, y).Value < 0 And DataGridView5.Item(2, y).Value <> 1 Then
                If DataGridView5.Item(2, y).Value = -2 Then
                Else
                    Dim operacion As Decimal = DataGridView5.Item(7, y).Value
                    Dim valor As Decimal = Math.Abs(DataGridView5.Item(4, y).Value)
                    Dim valornegativo = DataGridView5.Item(4, y).Value
                    For w As Integer = 0 To DataGridView5.Rows.Count - 1
                        If DataGridView5.Item(7, w).Value = operacion Then
                            DataGridView5.Item(5, w).Value = DataGridView5.Item(5, w).Value + valor
                            'DataGridView5.Item(6, w).Value = 
                            If DataGridView5.Item(4, w).Value = valornegativo Then
                                DataGridView5.Item(0, w).Value = "borrar"
                            End If
                        End If
                    Next
                End If
            End If

        Next

        Dim aBorrar As New List(Of DataGridViewRow)
        For Each renglon As DataGridViewRow In DataGridView5.Rows
            If renglon.Cells(0).Value = "borrar" Then
                aBorrar.Add(renglon)
            End If
        Next
        For Each renglon As DataGridViewRow In aBorrar
            DataGridView5.Rows.Remove(renglon)
        Next


        Dim saldo As Decimal = DataGridView5.Item(6, 0).Value
        Dim compras, abonos As Decimal
        For y As Integer = 1 To DataGridView5.Rows.Count - 2
            If DataGridView5.Item(4, y).Value < 0 And DataGridView5.Item(0, y).Value = "Venta" Then
                If DataGridView5.Item(2, y).Value = -2 Then
                    DataGridView5.Item(0, y).Value = "Excedente"
                Else
                    DataGridView5.Item(0, y).Value = "Devolucion"
                End If

                DataGridView5.Item(5, y).Value = Math.Abs(DataGridView5.Item(4, y).Value)
                DataGridView5.Item(4, y).Value = ""
            End If
            saldo = saldo + (Val(DataGridView5.Item(4, y).Value) - Val(DataGridView5.Item(5, y).Value))
            DataGridView5.Item(6, y).Value = saldo
            compras += Val(DataGridView5.Item(4, y).Value)
            abonos += Val(DataGridView5.Item(5, y).Value)

        Next

        DataGridView5.Item(4, DataGridView5.Rows.Count - 1).Value = compras
        DataGridView5.Item(5, DataGridView5.Rows.Count - 1).Value = abonos

        DataGridView5.AutoResizeColumns()


        XtraTabControl2.SelectedTabPageIndex = 3
        Dim dsEstadoCuenta As New DataSet("dsEstadoCuenta")
        qry = "select numero,ltrim(rtrim(nombre)) as nombre,telefono,calle,colonia,ciudad,estado from socios where numero = '" & iSocio & "'"
        Using tblCliente As DataTable = bdBase.dbDataTable(conexion, qry)
            dsEstadoCuenta.Tables.Add(tblCliente)
        End Using
        Using tblEstado As New DataTable("tblEstado")
            For Each col As DataGridViewColumn In DataGridView5.Columns
                If col.HeaderText = "Venta" Or col.HeaderText = "Abono" Or col.HeaderText = "Saldo" Then
                    tblEstado.Columns.Add(col.HeaderText, GetType(Decimal))
                Else
                    tblEstado.Columns.Add(col.HeaderText)
                End If
            Next
            For Each rwEstado As DataGridViewRow In DataGridView5.Rows
                Dim arrVal(rwEstado.Cells.Count - 1) As String
                For Each cell As DataGridViewTextBoxCell In rwEstado.Cells
                    If Not IsDBNull(cell.Value) Then
                        If cell.ColumnIndex = 4 Or cell.ColumnIndex = 5 Or cell.ColumnIndex = 6 Then
                            arrVal(cell.ColumnIndex) = Math.Round(cell.Value, 2)
                        Else
                            arrVal(cell.ColumnIndex) = cell.Value
                        End If
                    Else
                        arrVal(cell.ColumnIndex) = ""
                    End If
                Next
                tblEstado.Rows.Add(arrVal)
            Next
            dsEstadoCuenta.Tables.Add(tblEstado)
        End Using
        ReportViewer1.Reset()
        ReportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
        ReportViewer1.LocalReport.ReportPath = "C:\saznt\rpts\EstadoCuentaCliente.rdlc"
        ReportViewer1.RefreshReport()
        ReportViewer1.LocalReport.EnableExternalImages = True
        Dim params As ReportParameter() = New ReportParameter(6) {}
        params(0) = New ReportParameter("logotipo", "file:///" & "C:\Saz\logos\logo.jpg")
        params(1) = New ReportParameter("del", DateTimePicker2.Value.ToShortDateString.ToString)
        params(2) = New ReportParameter("al", DateTimePicker1.Value.ToShortDateString.ToString)
        params(3) = New ReportParameter("resumenVenta", "$ " & dsEstadoCuenta.Tables(1).Rows(dsEstadoCuenta.Tables(1).Rows.Count - 1).Item(4).ToString)
        params(4) = New ReportParameter("resumenAbono", "$ " & dsEstadoCuenta.Tables(1).Rows(dsEstadoCuenta.Tables(1).Rows.Count - 1).Item(5).ToString)
        params(5) = New ReportParameter("resumenSaldo", "$ " & dsEstadoCuenta.Tables(1).Rows(dsEstadoCuenta.Tables(1).Rows.Count - 2).Item(6).ToString)
        params(6) = New ReportParameter("saldoAnt", "$ " & dsEstadoCuenta.Tables(1).Rows(0).Item(6).ToString)
        ReportViewer1.LocalReport.SetParameters(params)
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dsEstadoCuenta.Tables(0)))
        ReportViewer1.LocalReport.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", dsEstadoCuenta.Tables(1)))
        ReportViewer1.DocumentMapCollapsed = True
        ReportViewer1.RefreshReport()

        'Dim impresion As PrintReport
        'impresion = New PrintReport(ReportViewer1.LocalReport)
        Me.Cursor = Cursors.Default
    End Sub



    Private Sub XtraTabControl4_SelectedPageChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl4.SelectedPageChanged
        If XtraTabControl4.SelectedTabPageIndex = 1 Then
            DataGridView2.Rows.Clear()
            TextEdit1.Text = 0
            TextEdit2.Text = 0
            TextEdit3.Text = 0
            TextEdit4.Text = 0
            TextEdit1.Text = 0
        End If
    End Sub



    'Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
    '    Dim qry As String
    '    qry = "SELECT tiendas.Nombre AS Tienda, detnotas.BARCODE, detnotas.ESTILO, colores.COLOR, acabados.ACABADO, marcas.MARCA, articulo.DESCRI, " & _
    '          "identificadores.TEXTO as Talla, detnotas.CANTIDAD, detnotas.PRECIO, detnotas.DESCUEN, CASE WHEN empleado.NOMBRE IS NULL THEN '' ELSE empleado.NOMBRE END as Empleado, " & _
    '          "detnotas.OPERACION, detnotas.hora, detnotas.CAJA FROM detnotas INNER JOIN tiendas ON detnotas.TIENDA = tiendas.NUMERO INNER JOIN " & _
    '          "colores ON detnotas.COLOR = colores.NUMERO INNER JOIN acabados ON detnotas.ACABADO = acabados.NUMERO INNER JOIN " & _
    '          "articulo ON detnotas.BARCODE = articulo.BARCODE INNER JOIN marcas ON articulo.MARCA = marcas.NUMERO LEFT OUTER JOIN " & _
    '          "identificadores ON articulo.LINEA = identificadores.LINEA AND detnotas.PUNTO = identificadores.talla LEFT OUTER JOIN " & _
    '          "empleado ON detnotas.EMPLEADO = empleado.numero where  (detnotas.NUMERO = '" & DataGridView1.Item(1, DataGridView1.CurrentRow.Index).Value & "') "
    '    Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
    '    DataGridView7.DataSource = dr2.Tables(0)
    '    DataGridView7.AutoResizeColumns()
    '    Cursor = Cursors.Default
    'End Sub

    Private Sub XtraTabControl2_SelectedPageChanged(ByVal sender As Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl2.SelectedPageChanged

        If XtraTabControl2.TabIndex = 7 Then
            llenag()
        End If

    End Sub


    Sub llenag()
        Dim qry, fechaal As String
        fechaal = Date.Parse(DateTimePicker3.Value).Date.ToString("yyyyMMdd")
        '        qry = "SELECT     temp.SOCIO,  ltrim(rtrim(socios.NOMBRE)) as Cliente, SUM(temp.vigente) AS '0-30', SUM(temp.dias3060) AS '30-60', SUM(temp.dias6090) AS '61-90', SUM(temp.Over90dias) AS '>90', " & _
        '                "  SUM(temp.iTotal) AS Total " & _
        '" FROM         (SELECT     SOCIO, CASE WHEN Datediff([d], creditos.fecha, getdate()) < 30 THEN SUM(total) ELSE 0 END AS vigente, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
        '                                          " >= 30 AND Datediff([d], creditos.fecha, getdate()) < 60 THEN SUM(total) ELSE 0 END AS dias3060, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
        '                                          " >= 60 AND Datediff([d], creditos.fecha, getdate()) <= 90 THEN SUM(total) ELSE 0 END AS dias6090, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
        '                                          " >= 90 THEN SUM(total) ELSE 0 END AS Over90dias, SUM(TOTAL) AS iTotal " & _
        '        " FROM creditos " & _
        '        "  where total<>0 and fecha<='" & fechaal & "'" & _
        '                   " GROUP BY SOCIO, FECHA) AS temp INNER JOIN " & _
        '                  " socios ON temp.SOCIO = socios.NUMERO " & _
        '" GROUP BY temp.SOCIO, socios.NOMBRE "

        qry = "SELECT socios.numero as id, ltrim(rtrim(socios.NOMBRE)) AS Cliente, " & _
              "CASE WHEN SUM(creditos.inicial) IS NULL THEN 0 ELSE SUM(creditos.inicial) END - " & _
              "CASE WHEN sum(creditos.total) IS NULL THEN 0 ELSE sum(creditos.total) END AS Saldo " & _
              "FROM creditos INNER JOIN  socios ON creditos.SOCIO = socios.NUMERO " & _
              "where  fecha<='" & fechaal & "'" & _
              " GROUP BY socios.NUMERO, socios.NOMBRE " & _
              " ORDER BY   Cliente "

        qry = "SELECT socios.numero as id, ltrim(rtrim(socios.NOMBRE)) AS Cliente, " & _
              "CASE WHEN sum(creditos.total) IS NULL THEN 0 ELSE sum(creditos.total) END AS Saldo " & _
              "FROM creditos INNER JOIN  socios ON creditos.SOCIO = socios.NUMERO " & _
              "where  fecha<='" & fechaal & "'" & _
              " GROUP BY socios.NUMERO, socios.NOMBRE " & _
              " ORDER BY  Cliente "


        Splash(True)
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        'GridView2.Columns("0-30").DisplayFormat(0) = "c"
        'GridView2.Columns("0-30").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Saldo").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("Saldo").DisplayFormat
        'Me.GridView2.Columns.Add(New GridViewDecimalColumn("Currency"))
        'Me.GridView2.Columns("Saldo").FormatString = "{0:C}"

        'GridView2.Columns("30-60").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns("61-90").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView2.Columns(">90").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        'GridView3.Columns("0-30").Summary.Item(0).DisplayFormat = "C"
        Dim item1 As GridColumnSummaryItem = New GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Saldo", "{0:C2}")
        GridView2.Columns("Saldo").Summary.Add(item1)
        GridView2.Columns("Saldo").DisplayFormat.FormatType = FormatType.Numeric
        GridView2.Columns("Saldo").DisplayFormat.FormatString = "c2"


        GridControl2.Refresh()
        GridView2.BestFitColumns()
        Splash(False)
    End Sub

    'Private Sub GridView2_CustomColumnDisplayText(ByVal sender As Object, ByVal e As CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
    '    Dim View As ColumnView = sender
    '    If e.Column.FieldName = "Saldo" Then
    '        Dim currencyType As Integer = View.GetRowCellValue(e.RowHandle, _
    '            View.Columns("CurrencyType"))
    '        Dim price As Decimal = View.GetRowCellValue(e.RowHandle, View.Columns("Saldo"))
    '        ' Conditional formatting: 
    '        e.DisplayText = String.Format(ciUSA, "{0:c}", Saldo)

    '    End If
    'End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl2)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If ComponentPrinter.IsPrintingAvailable(True) Then
            Dim printer As New ComponentPrinter(GridControl3)
            printer.ShowPreview(Me, New DefaultLookAndFeel().LookAndFeel)
        Else
            DevExpress.XtraEditors.XtraMessageBox.Show("XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub


    Private Sub GridControl2_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl2.DoubleClick
        Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
        Dim id As String = row(0)
        iSocio = id
        oClientePago = id
        '     = "SELECT    'Factura' as Operacion,socios.NOMBRE,creditos.NUMERO, creditos.FECHA,creditos.VENCE , Datediff([d], creditos.fecha, getdate()) as Dias, creditos.TOTAL AS Balance " & _
        '" FROM         creditos INNER JOIN " & _
        '                     " socios ON creditos.SOCIO = socios.NUMERO " & _
        '     " WHERE     (creditos.TOTAL > 0) AND (creditos.SOCIO = '" & id & "') "
        Dim qry As String
        qry = "SELECT ltrim(rtrim(socios.NOMBRE)) AS Cliente,  creditos.FECHA,creditos.numero,CASE WHEN inicial > 0 THEN 'Factura' ELSE 'Pago/Abono' END AS Tipo, " & _
              "creditos.INICIAL AS Cargo, CASE WHEN creditos.Abono IS NULL THEN 0 ELSE creditos.Abono END AS Abono,  0.00 as Saldo,fma.Observaciones " & _
              "FROM creditos INNER JOIN socios ON creditos.SOCIO = socios.NUMERO inner join fma on creditos.numero = fma.ticket where (creditos.SOCIO = '" & id & "') " & _
              " ORDER BY creditos.FECHA "


        Dim saldo As Decimal = 0.0
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        For Each renglon As DataRow In dr2.Tables(0).Rows
            saldo = saldo + renglon("Cargo") - renglon("Abono")
            renglon("saldo") = saldo
            If renglon("numero") = -3 Then
                renglon("Tipo") = "Devolución"
                'renglon("numero") = ""
            End If
            'If renglon("numero") = -6 Then
            '    renglon("numero") = ""
            'End If

        Next

        GridControl3.DataSource = dr2.Tables(0)
        GridView3.PopulateColumns()

        XtraTabControl2.SelectedTabPageIndex = 8

        Dim item1 As GridColumnSummaryItem = New GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Cargo", "{0:C2}")
        GridView3.Columns("Cargo").Summary.Add(item1)
        Dim item2 As GridColumnSummaryItem = New GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Abono", "{0:C2}")
        GridView3.Columns("Abono").Summary.Add(item2)
        'Dim item3 As GridColumnSummaryItem = New GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Cargo" - "Abono", "{0:C2}")
        'GridView3.Columns("Saldo").Summary.Add(item3)
        'Dim item1 As GridColumnSummaryItem = New GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Saldo", "{0:C2}")
        'GridView2.Columns("Saldo").Summary.Add(item1)
        GridView3.Columns("Cargo").DisplayFormat.FormatType = FormatType.Numeric
        GridView3.Columns("Cargo").DisplayFormat.FormatString = "c2"
        GridView3.Columns("Abono").DisplayFormat.FormatType = FormatType.Numeric
        GridView3.Columns("Abono").DisplayFormat.FormatString = "c2"
        GridView3.Columns("Saldo").DisplayFormat.FormatType = FormatType.Numeric
        GridView3.Columns("Saldo").DisplayFormat.FormatString = "c2"
        GridView3.BestFitColumns()




        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If String.IsNullOrEmpty(TextEdit7.Text) Then Exit Sub
        DataGridView2.Rows.Clear()
        Dim saldo As Decimal = TextBox2.Text
        Dim totalabono As Decimal = 0
        totalabono = TextEdit7.Text

        If saldo > 0 Then
            SaldoAFavor = False
            Dim x As Integer
            For x = 0 To DataGridView3.Rows.Count - 1
                If totalabono > 0 Then
                    If totalabono >= DataGridView3.Item(7, x).Value And DataGridView3.Item(7, x).Value > 0 Then
                        DataGridView3.Item(11, x).Value = 1
                        TextEdit1.Text = DataGridView3.Item(0, DataGridView3.Rows(x).Index).Value
                        TextEdit2.Text = DataGridView3.Item(7, DataGridView3.Rows(x).Index).Value
                        TextEdit3.Text = TextEdit2.Text
                        TextEdit8.Text = DataGridView3.Item(9, DataGridView3.Rows(x).Index).Value
                        TextEdit9.Text = DataGridView3.Item(4, DataGridView3.Rows(x).Index).Value
                        TextEdit10.Text = DataGridView3.Item(5, DataGridView3.Rows(x).Index).Value
                        DataGridView2.Rows.Add()
                        DataGridView2.Item(0, DataGridView2.Rows.Count - 1).Value = DataGridView3.Item(0, x).Value.ToString
                        DataGridView2.Item(1, DataGridView2.Rows.Count - 1).Value = FormatNumber(DataGridView3.Item(7, x).Value, 2)
                        DataGridView2.Item(2, DataGridView2.Rows.Count - 1).Value = FormatNumber(DataGridView3.Item(7, x).Value, 2)
                        DataGridView2.Item(3, DataGridView2.Rows.Count - 1).Value = DataGridView3.Item(9, x).Value
                        totalabono = totalabono - DataGridView3.Item(7, x).Value

                        DataGridView2.Item(3, DataGridView2.Rows.Count - 1).Value = TextEdit8.Text
                        DataGridView2.Item(4, DataGridView2.Rows.Count - 1).Value = TextEdit9.Text
                        DataGridView2.Item(5, DataGridView2.Rows.Count - 1).Value = TextEdit10.Text
                        Dim capital As Decimal
                        capital = Convert.ToDecimal(TextEdit9.Text) + Convert.ToDecimal(TextEdit10.Text)
                        capital = TextEdit3.Text - capital
                        DataGridView2.Item(6, DataGridView2.Rows.Count - 1).Value = (capital)

                        'For x = 0 To DataGridView2.Rows.Count - 1
                        ' suma = suma + DataGridView2.Item(2, x).Value
                        ' Next
                        ' TextEdit4.Text = suma

                    Else
                        If DataGridView3.Item(7, x).Value > 0 Then
                            DataGridView2.Rows.Add()
                            DataGridView2.Item(0, DataGridView2.Rows.Count - 1).Value = DataGridView3.Item(0, x).Value
                            DataGridView2.Item(1, DataGridView2.Rows.Count - 1).Value = FormatNumber(DataGridView3.Item(7, x).Value, 2)
                            DataGridView2.Item(2, DataGridView2.Rows.Count - 1).Value = FormatNumber(totalabono, 2)
                            DataGridView2.Item(3, DataGridView2.Rows.Count - 1).Value = DataGridView3.Item(9, x).Value
                            DataGridView2.Item(4, DataGridView2.Rows.Count - 1).Value = 0
                            DataGridView2.Item(5, DataGridView2.Rows.Count - 1).Value = 0
                            DataGridView2.Item(6, DataGridView2.Rows.Count - 1).Value = 0

                            totalabono = 0
                            Exit For
                        End If

                    End If
                Else
                    Exit For
                End If

            Next
            Dim s As Integer
            Dim suma As Decimal
            For s = 0 To DataGridView2.Rows.Count - 1
                suma = suma + DataGridView2.Item(2, s).Value
            Next
            TextEdit4.Text = suma
        Else
            SaldoAFavor = True
            If DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(442, Mensaje.Texto), IdiomaMensajes(442, Mensaje.Titulo), MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(443, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(444, Mensaje.Texto), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            TextEdit4.Text = TextEdit7.Text
        End If
        TextBox9.Text = FormatCurrency(TextEdit4.Text)
    End Sub

    Private Sub Button8_Click(sender As System.Object, e As System.EventArgs)
        Dim qry As String

        qry = "SELECT     temp.SOCIO,  ltrim(rtrim(socios.NOMBRE)) as Cliente, SUM(temp.vigente) AS Vigente, SUM(temp.dias3060) AS '30-60', SUM(temp.dias6090) AS '61-90', SUM(temp.Over90dias) AS '>90', " & _
                "  SUM(temp.iTotal) AS Total " & _
" FROM         (SELECT     SOCIO, CASE WHEN Datediff([d], creditos.fecha, getdate()) < 30 THEN SUM(total) ELSE 0 END AS vigente, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
                                          " >= 30 AND Datediff([d], creditos.fecha, getdate()) < 60 THEN SUM(total) ELSE 0 END AS dias3060, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
                                          " >= 60 AND Datediff([d], creditos.fecha, getdate()) <= 90 THEN SUM(total) ELSE 0 END AS dias6090, CASE WHEN Datediff([d], creditos.fecha, getdate())  " & _
                                          " >= 90 THEN SUM(total) ELSE 0 END AS Over90dias, SUM(TOTAL) AS iTotal " & _
        " FROM creditos " & _
        " WHERE(TOTAL > 0) " & _
                   " GROUP BY SOCIO, FECHA) AS temp INNER JOIN " & _
                  " socios ON temp.SOCIO = socios.NUMERO " & _
" GROUP BY temp.SOCIO, socios.NOMBRE "


        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        GridControl2.DataSource = dr2.Tables(0)
        GridView2.PopulateColumns()
        GridView2.BestFitColumns()
        GridView2.Columns("Vigente").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.Columns("30-60").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.Columns("61-90").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
        GridView2.Columns(">90").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
    End Sub

   



    Private Sub TextBox12_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox12.TextChanged
        TextBox1.Text = TextBox12.Text
    End Sub



    Private Sub DateTimePicker3_ValueChanged(sender As System.Object, e As System.EventArgs) Handles DateTimePicker3.ValueChanged
        llenag()
    End Sub

    Private Sub PagosCredito_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
       
        IdiomaTextos(Me)
        If String.IsNullOrEmpty(conexion) Then
            DevExpress.XtraEditors.XtraMessageBox.Show(IdiomaMensajes(307, Mensaje.Texto), IdiomaMensajes(307, Mensaje.Texto), MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If
        DateTimePicker2.Value = DateValue("01/" & Now.Month & "/" & Now.Year)
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        DefaultLookAndFeel1.LookAndFeel.SetSkinStyle(My.Settings.skin)
        RegistraAcceso(conexion, "Creditos [Créditos.frm]")
        llenafmas()
        Me.ReportViewer1.RefreshReport()
    End Sub

End Class