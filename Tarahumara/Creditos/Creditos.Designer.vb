﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Creditos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.tabAplicarPag = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl4 = New DevExpress.XtraTab.XtraTabControl()
        Me.tabSelCliente = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl17 = New DevExpress.XtraGrid.GridControl()
        Me.GridView17 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Button52 = New System.Windows.Forms.Button()
        Me.tabAplicarPago = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.XtraTabControl3 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage7 = New DevExpress.XtraTab.XtraTabPage()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Pagare = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Saldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Secuencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IntAnterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IntGenerado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AbonoCapital = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.XtraTabPage8 = New DevExpress.XtraTab.XtraTabPage()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.Button54 = New System.Windows.Forms.Button()
        Me.GroupControl11 = New DevExpress.XtraEditors.GroupControl()
        Me.ReportViewer2 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.Button53 = New System.Windows.Forms.Button()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.NoPagare = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Inicial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiasVencido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InteresAnterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InteresGenerado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoSinInteres = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoActual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vence = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NSecuencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tiendai = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.subcliente1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.efectivo1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.tabHistorial = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.tabSelClienteH = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tabResAbonos = New DevExpress.XtraTab.XtraTabPage()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.DataGridView6 = New System.Windows.Forms.DataGridView()
        Me.tabResVentas = New DevExpress.XtraTab.XtraTabPage()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.DataGridView7 = New System.Windows.Forms.DataGridView()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tabMovMes = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl5 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tienda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Compra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Abono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Saldos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Operacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.tabSaldos = New DevExpress.XtraTab.XtraTabPage()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.tabEstadoCuenta = New DevExpress.XtraTab.XtraTabPage()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GridControl3 = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.tabAplicarPag.SuspendLayout()
        CType(Me.XtraTabControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl4.SuspendLayout()
        Me.tabSelCliente.SuspendLayout()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.GridControl17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabAplicarPago.SuspendLayout()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl3.SuspendLayout()
        Me.XtraTabPage7.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage8.SuspendLayout()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabHistorial.SuspendLayout()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.tabSelClienteH.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabResAbonos.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabResVentas.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMovMes.SuspendLayout()
        CType(Me.XtraTabControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl5.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        Me.tabSaldos.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabEstadoCuenta.SuspendLayout()
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.tabAplicarPag
        Me.XtraTabControl1.Size = New System.Drawing.Size(953, 618)
        Me.XtraTabControl1.TabIndex = 3
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabAplicarPag, Me.tabHistorial})
        '
        'tabAplicarPag
        '
        Me.tabAplicarPag.Controls.Add(Me.XtraTabControl4)
        Me.tabAplicarPag.Name = "tabAplicarPag"
        Me.tabAplicarPag.Size = New System.Drawing.Size(947, 590)
        Me.tabAplicarPag.Text = "Aplicar Pagos"
        '
        'XtraTabControl4
        '
        Me.XtraTabControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl4.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl4.Name = "XtraTabControl4"
        Me.XtraTabControl4.SelectedTabPage = Me.tabSelCliente
        Me.XtraTabControl4.Size = New System.Drawing.Size(947, 590)
        Me.XtraTabControl4.TabIndex = 6
        Me.XtraTabControl4.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSelCliente, Me.tabAplicarPago})
        '
        'tabSelCliente
        '
        Me.tabSelCliente.Controls.Add(Me.GroupControl9)
        Me.tabSelCliente.Name = "tabSelCliente"
        Me.tabSelCliente.Size = New System.Drawing.Size(941, 562)
        Me.tabSelCliente.Text = "Selección de Cliente"
        '
        'GroupControl9
        '
        Me.GroupControl9.Controls.Add(Me.GridControl17)
        Me.GroupControl9.Controls.Add(Me.Label12)
        Me.GroupControl9.Controls.Add(Me.TextBox12)
        Me.GroupControl9.Controls.Add(Me.Button52)
        Me.GroupControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl9.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(941, 562)
        Me.GroupControl9.TabIndex = 5
        Me.GroupControl9.Text = "Busqueda de Clientes"
        '
        'GridControl17
        '
        Me.GridControl17.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl17.Location = New System.Drawing.Point(17, 64)
        Me.GridControl17.MainView = Me.GridView17
        Me.GridControl17.Name = "GridControl17"
        Me.GridControl17.Size = New System.Drawing.Size(910, 493)
        Me.GridControl17.TabIndex = 19
        Me.GridControl17.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView17})
        '
        'GridView17
        '
        Me.GridView17.GridControl = Me.GridControl17
        Me.GridView17.Name = "GridView17"
        Me.GridView17.OptionsBehavior.Editable = False
        Me.GridView17.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        Me.GridView17.OptionsView.ColumnAutoWidth = False
        Me.GridView17.OptionsView.ShowAutoFilterRow = True
        Me.GridView17.OptionsView.ShowFooter = True
        Me.GridView17.OptionsView.ShowGroupedColumns = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(179, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Ingrese Nombre del cliente a Buscar"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(17, 38)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(208, 21)
        Me.TextBox12.TabIndex = 2
        '
        'Button52
        '
        Me.Button52.Location = New System.Drawing.Point(245, 38)
        Me.Button52.Name = "Button52"
        Me.Button52.Size = New System.Drawing.Size(75, 23)
        Me.Button52.TabIndex = 1
        Me.Button52.Text = "Buscar"
        Me.Button52.UseVisualStyleBackColor = True
        '
        'tabAplicarPago
        '
        Me.tabAplicarPago.Controls.Add(Me.GroupControl10)
        Me.tabAplicarPago.Name = "tabAplicarPago"
        Me.tabAplicarPago.Size = New System.Drawing.Size(941, 562)
        Me.tabAplicarPago.Text = "Aplicar Pago"
        '
        'GroupControl10
        '
        Me.GroupControl10.Controls.Add(Me.TextBox8)
        Me.GroupControl10.Controls.Add(Me.Label11)
        Me.GroupControl10.Controls.Add(Me.Label13)
        Me.GroupControl10.Controls.Add(Me.TextBox9)
        Me.GroupControl10.Controls.Add(Me.Label10)
        Me.GroupControl10.Controls.Add(Me.Label9)
        Me.GroupControl10.Controls.Add(Me.TextBox7)
        Me.GroupControl10.Controls.Add(Me.TextBox6)
        Me.GroupControl10.Controls.Add(Me.TextEdit10)
        Me.GroupControl10.Controls.Add(Me.TextEdit9)
        Me.GroupControl10.Controls.Add(Me.XtraTabControl3)
        Me.GroupControl10.Controls.Add(Me.TextBox5)
        Me.GroupControl10.Controls.Add(Me.Label8)
        Me.GroupControl10.Controls.Add(Me.TextBox4)
        Me.GroupControl10.Controls.Add(Me.Label7)
        Me.GroupControl10.Controls.Add(Me.TextBox3)
        Me.GroupControl10.Controls.Add(Me.Label6)
        Me.GroupControl10.Controls.Add(Me.LabelControl6)
        Me.GroupControl10.Controls.Add(Me.TextEdit8)
        Me.GroupControl10.Controls.Add(Me.Label5)
        Me.GroupControl10.Controls.Add(Me.TextEdit7)
        Me.GroupControl10.Controls.Add(Me.Button7)
        Me.GroupControl10.Controls.Add(Me.TextBox2)
        Me.GroupControl10.Controls.Add(Me.TextEdit6)
        Me.GroupControl10.Controls.Add(Me.Label4)
        Me.GroupControl10.Controls.Add(Me.Label3)
        Me.GroupControl10.Controls.Add(Me.LabelControl5)
        Me.GroupControl10.Controls.Add(Me.LabelControl4)
        Me.GroupControl10.Controls.Add(Me.TextEdit5)
        Me.GroupControl10.Controls.Add(Me.TextEdit4)
        Me.GroupControl10.Controls.Add(Me.Button54)
        Me.GroupControl10.Controls.Add(Me.GroupControl11)
        Me.GroupControl10.Controls.Add(Me.Button53)
        Me.GroupControl10.Controls.Add(Me.LabelControl3)
        Me.GroupControl10.Controls.Add(Me.LabelControl2)
        Me.GroupControl10.Controls.Add(Me.LabelControl1)
        Me.GroupControl10.Controls.Add(Me.TextEdit3)
        Me.GroupControl10.Controls.Add(Me.TextEdit2)
        Me.GroupControl10.Controls.Add(Me.TextEdit1)
        Me.GroupControl10.Controls.Add(Me.DataGridView3)
        Me.GroupControl10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl10.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(941, 562)
        Me.GroupControl10.TabIndex = 7
        '
        'TextBox8
        '
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox8.Enabled = False
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(657, 108)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(263, 46)
        Me.TextBox8.TabIndex = 88
        Me.TextBox8.Text = "0"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.Label11.Location = New System.Drawing.Point(660, 78)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 27)
        Me.Label11.TabIndex = 87
        Me.Label11.Text = "Balance:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 16.0!)
        Me.Label13.Location = New System.Drawing.Point(660, 172)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(104, 27)
        Me.Label13.TabIndex = 86
        Me.Label13.Text = "Pagando:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBox9
        '
        Me.TextBox9.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox9.Enabled = False
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(663, 202)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(263, 46)
        Me.TextBox9.TabIndex = 85
        Me.TextBox9.Text = "0"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(508, 29)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 83
        Me.Label10.Text = "A Favor"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label10.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(514, 31)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(39, 13)
        Me.Label9.TabIndex = 82
        Me.Label9.Text = "Saldo  "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label9.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox7.Enabled = False
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(485, 47)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(108, 19)
        Me.TextBox7.TabIndex = 80
        Me.TextBox7.Text = "0"
        Me.TextBox7.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox6.Enabled = False
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.Location = New System.Drawing.Point(457, 43)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(108, 19)
        Me.TextBox6.TabIndex = 79
        Me.TextBox6.Text = "0"
        Me.TextBox6.Visible = False
        '
        'TextEdit10
        '
        Me.TextEdit10.Location = New System.Drawing.Point(637, 48)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Mask.EditMask = "c"
        Me.TextEdit10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit10.Size = New System.Drawing.Size(60, 20)
        Me.TextEdit10.TabIndex = 78
        Me.TextEdit10.Visible = False
        '
        'TextEdit9
        '
        Me.TextEdit9.Location = New System.Drawing.Point(621, 53)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Mask.EditMask = "c"
        Me.TextEdit9.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit9.Size = New System.Drawing.Size(10, 20)
        Me.TextEdit9.TabIndex = 77
        Me.TextEdit9.Visible = False
        '
        'XtraTabControl3
        '
        Me.XtraTabControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.XtraTabControl3.Location = New System.Drawing.Point(5, 312)
        Me.XtraTabControl3.Name = "XtraTabControl3"
        Me.XtraTabControl3.SelectedTabPage = Me.XtraTabPage7
        Me.XtraTabControl3.Size = New System.Drawing.Size(618, 250)
        Me.XtraTabControl3.TabIndex = 76
        Me.XtraTabControl3.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage7, Me.XtraTabPage8})
        '
        'XtraTabPage7
        '
        Me.XtraTabPage7.Controls.Add(Me.DataGridView2)
        Me.XtraTabPage7.Name = "XtraTabPage7"
        Me.XtraTabPage7.Size = New System.Drawing.Size(612, 222)
        Me.XtraTabPage7.Text = "Resumen de Pagos en esta Sesión"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Pagare, Me.Saldo, Me.Pago, Me.Secuencia, Me.IntAnterior, Me.IntGenerado, Me.AbonoCapital})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView2.DefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.Size = New System.Drawing.Size(612, 222)
        Me.DataGridView2.TabIndex = 7
        '
        'Pagare
        '
        Me.Pagare.HeaderText = "Pagare"
        Me.Pagare.Name = "Pagare"
        Me.Pagare.ReadOnly = True
        Me.Pagare.Width = 66
        '
        'Saldo
        '
        Me.Saldo.HeaderText = "Saldo"
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        Me.Saldo.Width = 58
        '
        'Pago
        '
        Me.Pago.HeaderText = "Pago"
        Me.Pago.Name = "Pago"
        Me.Pago.ReadOnly = True
        Me.Pago.Width = 56
        '
        'Secuencia
        '
        Me.Secuencia.HeaderText = "Secuencia"
        Me.Secuencia.Name = "Secuencia"
        Me.Secuencia.ReadOnly = True
        Me.Secuencia.Width = 80
        '
        'IntAnterior
        '
        Me.IntAnterior.HeaderText = "InteresAnterior"
        Me.IntAnterior.Name = "IntAnterior"
        Me.IntAnterior.ReadOnly = True
        Me.IntAnterior.Width = 106
        '
        'IntGenerado
        '
        Me.IntGenerado.HeaderText = "Interés Generado"
        Me.IntGenerado.Name = "IntGenerado"
        Me.IntGenerado.ReadOnly = True
        Me.IntGenerado.Width = 107
        '
        'AbonoCapital
        '
        Me.AbonoCapital.HeaderText = "AbonoCapital"
        Me.AbonoCapital.Name = "AbonoCapital"
        Me.AbonoCapital.ReadOnly = True
        Me.AbonoCapital.Width = 96
        '
        'XtraTabPage8
        '
        Me.XtraTabPage8.Controls.Add(Me.DataGridView4)
        Me.XtraTabPage8.Name = "XtraTabPage8"
        Me.XtraTabPage8.Size = New System.Drawing.Size(612, 222)
        Me.XtraTabPage8.Text = "Forma de Pago"
        '
        'DataGridView4
        '
        Me.DataGridView4.AllowUserToAddRows = False
        Me.DataGridView4.AllowUserToDeleteRows = False
        Me.DataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView4.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.DataGridView4.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView4.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView4.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.Size = New System.Drawing.Size(612, 222)
        Me.DataGridView4.TabIndex = 55
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox5.Enabled = False
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(559, 27)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(92, 19)
        Me.TextBox5.TabIndex = 75
        Me.TextBox5.Text = "0"
        Me.TextBox5.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(534, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 13)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Interés Generado"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label8.Visible = False
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox4.Enabled = False
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(510, 27)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(92, 19)
        Me.TextBox4.TabIndex = 73
        Me.TextBox4.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(499, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 13)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Saldo Actual"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label7.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox3.Enabled = False
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(559, 22)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(82, 19)
        Me.TextBox3.TabIndex = 71
        Me.TextBox3.Text = "0"
        Me.TextBox3.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(547, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 70
        Me.Label6.Text = "Interés Anterior"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label6.Visible = False
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(198, 271)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl6.TabIndex = 68
        Me.LabelControl6.Text = "Secuencia"
        '
        'TextEdit8
        '
        Me.TextEdit8.Enabled = False
        Me.TextEdit8.Location = New System.Drawing.Point(202, 290)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Mask.EditMask = "c"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit8.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit8.TabIndex = 67
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(124, 13)
        Me.Label5.TabIndex = 66
        Me.Label5.Text = "Ingrese Monto a Abonar"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextEdit7
        '
        Me.TextEdit7.Location = New System.Drawing.Point(186, 54)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Mask.EditMask = "c2"
        Me.TextEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit7.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit7.TabIndex = 65
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(291, 51)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 64
        Me.Button7.Text = "Repartir"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(510, 30)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(108, 19)
        Me.TextBox2.TabIndex = 63
        Me.TextBox2.Visible = False
        '
        'TextEdit6
        '
        Me.TextEdit6.Enabled = False
        Me.TextEdit6.Location = New System.Drawing.Point(65, 21)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.TextEdit6.Properties.Appearance.Options.UseBackColor = True
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.TextEdit6.Properties.Mask.EditMask = "\p{Lu}+"
        Me.TextEdit6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit6.Size = New System.Drawing.Size(376, 28)
        Me.TextEdit6.TabIndex = 62
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(443, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 13)
        Me.Label4.TabIndex = 61
        Me.Label4.Text = "Saldo Capital"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label4.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(19, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "Cliente"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(520, 272)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(69, 13)
        Me.LabelControl5.TabIndex = 59
        Me.LabelControl5.Text = "Monto Pagado"
        Me.LabelControl5.Visible = False
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(419, 272)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl4.TabIndex = 58
        Me.LabelControl4.Text = "Monto Seleccionado"
        Me.LabelControl4.Visible = False
        '
        'TextEdit5
        '
        Me.TextEdit5.Enabled = False
        Me.TextEdit5.Location = New System.Drawing.Point(520, 290)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Size = New System.Drawing.Size(95, 20)
        Me.TextEdit5.TabIndex = 57
        Me.TextEdit5.Visible = False
        '
        'TextEdit4
        '
        Me.TextEdit4.Enabled = False
        Me.TextEdit4.Location = New System.Drawing.Point(419, 290)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Size = New System.Drawing.Size(95, 20)
        Me.TextEdit4.TabIndex = 56
        Me.TextEdit4.Visible = False
        '
        'Button54
        '
        Me.Button54.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button54.Location = New System.Drawing.Point(723, 21)
        Me.Button54.Name = "Button54"
        Me.Button54.Size = New System.Drawing.Size(175, 44)
        Me.Button54.TabIndex = 16
        Me.Button54.Text = "Guardar Pagos"
        Me.Button54.UseVisualStyleBackColor = True
        '
        'GroupControl11
        '
        Me.GroupControl11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl11.Controls.Add(Me.ReportViewer2)
        Me.GroupControl11.Location = New System.Drawing.Point(655, 270)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(281, 294)
        Me.GroupControl11.TabIndex = 15
        '
        'ReportViewer2
        '
        Me.ReportViewer2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ReportViewer2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportViewer2.Location = New System.Drawing.Point(2, 22)
        Me.ReportViewer2.Name = "ReportViewer2"
        Me.ReportViewer2.Size = New System.Drawing.Size(277, 270)
        Me.ReportViewer2.TabIndex = 0
        '
        'Button53
        '
        Me.Button53.Location = New System.Drawing.Point(359, 287)
        Me.Button53.Name = "Button53"
        Me.Button53.Size = New System.Drawing.Size(45, 23)
        Me.Button53.TabIndex = 14
        Me.Button53.Text = "OK"
        Me.Button53.UseVisualStyleBackColor = True
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(274, 271)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(70, 13)
        Me.LabelControl3.TabIndex = 13
        Me.LabelControl3.Text = "Monto a Pagar"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(130, 270)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(26, 13)
        Me.LabelControl2.TabIndex = 12
        Me.LabelControl2.Text = "Saldo"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(24, 270)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl1.TabIndex = 11
        Me.LabelControl1.Text = "Pagaré"
        '
        'TextEdit3
        '
        Me.TextEdit3.Location = New System.Drawing.Point(278, 290)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Mask.EditMask = "c"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit3.Size = New System.Drawing.Size(70, 20)
        Me.TextEdit3.TabIndex = 10
        '
        'TextEdit2
        '
        Me.TextEdit2.Enabled = False
        Me.TextEdit2.Location = New System.Drawing.Point(121, 290)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(76, 20)
        Me.TextEdit2.TabIndex = 9
        '
        'TextEdit1
        '
        Me.TextEdit1.Enabled = False
        Me.TextEdit1.Location = New System.Drawing.Point(13, 290)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 8
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.AllowUserToDeleteRows = False
        Me.DataGridView3.AllowUserToOrderColumns = True
        Me.DataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NoPagare, Me.FechaDoc, Me.Inicial, Me.DiasVencido, Me.InteresAnterior, Me.InteresGenerado, Me.SaldoSinInteres, Me.SaldoActual, Me.Vence, Me.NSecuencia, Me.Tiendai, Me.Sel, Me.subcliente1, Me.efectivo1})
        Me.DataGridView3.Location = New System.Drawing.Point(10, 80)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(641, 186)
        Me.DataGridView3.TabIndex = 6
        '
        'NoPagare
        '
        Me.NoPagare.HeaderText = "Pagaré"
        Me.NoPagare.Name = "NoPagare"
        Me.NoPagare.ReadOnly = True
        Me.NoPagare.Width = 66
        '
        'FechaDoc
        '
        Me.FechaDoc.HeaderText = "Fecha"
        Me.FechaDoc.Name = "FechaDoc"
        Me.FechaDoc.ReadOnly = True
        Me.FechaDoc.Width = 61
        '
        'Inicial
        '
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.Inicial.DefaultCellStyle = DataGridViewCellStyle3
        Me.Inicial.HeaderText = "Total"
        Me.Inicial.Name = "Inicial"
        Me.Inicial.ReadOnly = True
        Me.Inicial.Width = 56
        '
        'DiasVencido
        '
        Me.DiasVencido.HeaderText = "DiasVencido"
        Me.DiasVencido.Name = "DiasVencido"
        Me.DiasVencido.ReadOnly = True
        Me.DiasVencido.Width = 89
        '
        'InteresAnterior
        '
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.InteresAnterior.DefaultCellStyle = DataGridViewCellStyle4
        Me.InteresAnterior.HeaderText = "InteresAnterior"
        Me.InteresAnterior.Name = "InteresAnterior"
        Me.InteresAnterior.ReadOnly = True
        Me.InteresAnterior.Visible = False
        Me.InteresAnterior.Width = 106
        '
        'InteresGenerado
        '
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.InteresGenerado.DefaultCellStyle = DataGridViewCellStyle5
        Me.InteresGenerado.HeaderText = "InteresGenerado"
        Me.InteresGenerado.Name = "InteresGenerado"
        Me.InteresGenerado.ReadOnly = True
        Me.InteresGenerado.Visible = False
        Me.InteresGenerado.Width = 114
        '
        'SaldoSinInteres
        '
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.SaldoSinInteres.DefaultCellStyle = DataGridViewCellStyle6
        Me.SaldoSinInteres.HeaderText = "SaldoInteres"
        Me.SaldoSinInteres.Name = "SaldoSinInteres"
        Me.SaldoSinInteres.ReadOnly = True
        Me.SaldoSinInteres.Visible = False
        Me.SaldoSinInteres.Width = 93
        '
        'SaldoActual
        '
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = "0"
        Me.SaldoActual.DefaultCellStyle = DataGridViewCellStyle7
        Me.SaldoActual.HeaderText = "SaldoActual"
        Me.SaldoActual.Name = "SaldoActual"
        Me.SaldoActual.ReadOnly = True
        Me.SaldoActual.Width = 88
        '
        'Vence
        '
        Me.Vence.HeaderText = "Vence"
        Me.Vence.Name = "Vence"
        Me.Vence.ReadOnly = True
        Me.Vence.Width = 61
        '
        'NSecuencia
        '
        Me.NSecuencia.HeaderText = "Secuencia"
        Me.NSecuencia.Name = "NSecuencia"
        Me.NSecuencia.ReadOnly = True
        Me.NSecuencia.Visible = False
        Me.NSecuencia.Width = 80
        '
        'Tiendai
        '
        Me.Tiendai.HeaderText = "Tienda"
        Me.Tiendai.Name = "Tiendai"
        Me.Tiendai.ReadOnly = True
        Me.Tiendai.Width = 64
        '
        'Sel
        '
        Me.Sel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Sel.HeaderText = "Abonar"
        Me.Sel.Name = "Sel"
        Me.Sel.Width = 48
        '
        'subcliente1
        '
        Me.subcliente1.HeaderText = "SubCliente"
        Me.subcliente1.Name = "subcliente1"
        Me.subcliente1.ReadOnly = True
        Me.subcliente1.Width = 83
        '
        'efectivo1
        '
        Me.efectivo1.HeaderText = "Efectivo"
        Me.efectivo1.Name = "efectivo1"
        Me.efectivo1.ReadOnly = True
        Me.efectivo1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.efectivo1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.efectivo1.Width = 71
        '
        'tabHistorial
        '
        Me.tabHistorial.Controls.Add(Me.XtraTabControl2)
        Me.tabHistorial.Name = "tabHistorial"
        Me.tabHistorial.Size = New System.Drawing.Size(947, 590)
        Me.tabHistorial.Text = "Historial "
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.tabSelClienteH
        Me.XtraTabControl2.Size = New System.Drawing.Size(947, 590)
        Me.XtraTabControl2.TabIndex = 7
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tabSelClienteH, Me.tabResAbonos, Me.tabResVentas, Me.tabMovMes, Me.tabSaldos, Me.tabEstadoCuenta})
        '
        'tabSelClienteH
        '
        Me.tabSelClienteH.Controls.Add(Me.GroupControl1)
        Me.tabSelClienteH.Name = "tabSelClienteH"
        Me.tabSelClienteH.Size = New System.Drawing.Size(941, 562)
        Me.tabSelClienteH.Text = "Selección de Cliente"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.DateTimePicker1)
        Me.GroupControl1.Controls.Add(Me.DateTimePicker2)
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.TextBox1)
        Me.GroupControl1.Controls.Add(Me.Button1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(941, 562)
        Me.GroupControl1.TabIndex = 5
        Me.GroupControl1.Text = "Busqueda de Clientes"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(39, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Periodo de Fechas"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(127, 38)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 21)
        Me.DateTimePicker1.TabIndex = 21
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(17, 37)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(104, 21)
        Me.DateTimePicker2.TabIndex = 20
        '
        'GridControl1
        '
        Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl1.Location = New System.Drawing.Point(17, 64)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(910, 493)
        Me.GridControl1.TabIndex = 19
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(237, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(179, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Ingrese Nombre del cliente a Buscar"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(240, 37)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(208, 21)
        Me.TextBox1.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(454, 35)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tabResAbonos
        '
        Me.tabResAbonos.Controls.Add(Me.Button2)
        Me.tabResAbonos.Controls.Add(Me.GroupControl2)
        Me.tabResAbonos.Name = "tabResAbonos"
        Me.tabResAbonos.Size = New System.Drawing.Size(941, 562)
        Me.tabResAbonos.Text = "Resumen de Abonos"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(697, -1)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Excel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupControl2
        '
        Me.GroupControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl2.Controls.Add(Me.DataGridView6)
        Me.GroupControl2.Location = New System.Drawing.Point(0, 24)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(941, 530)
        Me.GroupControl2.TabIndex = 7
        '
        'DataGridView6
        '
        Me.DataGridView6.AllowUserToAddRows = False
        Me.DataGridView6.AllowUserToDeleteRows = False
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        Me.DataGridView6.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView6.Location = New System.Drawing.Point(2, 21)
        Me.DataGridView6.Name = "DataGridView6"
        Me.DataGridView6.ReadOnly = True
        Me.DataGridView6.Size = New System.Drawing.Size(937, 507)
        Me.DataGridView6.TabIndex = 6
        '
        'tabResVentas
        '
        Me.tabResVentas.Controls.Add(Me.GroupControl5)
        Me.tabResVentas.Controls.Add(Me.Button3)
        Me.tabResVentas.Controls.Add(Me.GroupControl3)
        Me.tabResVentas.Name = "tabResVentas"
        Me.tabResVentas.Size = New System.Drawing.Size(941, 562)
        Me.tabResVentas.Text = "Resumen de Ventas"
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.DataGridView7)
        Me.GroupControl5.Location = New System.Drawing.Point(5, 318)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(941, 249)
        Me.GroupControl5.TabIndex = 10
        '
        'DataGridView7
        '
        Me.DataGridView7.AllowUserToAddRows = False
        Me.DataGridView7.AllowUserToDeleteRows = False
        Me.DataGridView7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView7.Location = New System.Drawing.Point(2, 21)
        Me.DataGridView7.Name = "DataGridView7"
        Me.DataGridView7.ReadOnly = True
        Me.DataGridView7.Size = New System.Drawing.Size(937, 226)
        Me.DataGridView7.TabIndex = 6
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(697, 0)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Excel"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'GroupControl3
        '
        Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl3.Controls.Add(Me.DataGridView1)
        Me.GroupControl3.Location = New System.Drawing.Point(3, 26)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(941, 288)
        Me.GroupControl3.TabIndex = 9
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(2, 21)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(937, 265)
        Me.DataGridView1.TabIndex = 6
        '
        'tabMovMes
        '
        Me.tabMovMes.Controls.Add(Me.XtraTabControl5)
        Me.tabMovMes.Controls.Add(Me.Button4)
        Me.tabMovMes.Name = "tabMovMes"
        Me.tabMovMes.Size = New System.Drawing.Size(941, 562)
        Me.tabMovMes.Text = "Movimientos del Mes"
        '
        'XtraTabControl5
        '
        Me.XtraTabControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl5.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl5.Name = "XtraTabControl5"
        Me.XtraTabControl5.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl5.Size = New System.Drawing.Size(941, 562)
        Me.XtraTabControl5.TabIndex = 12
        Me.XtraTabControl5.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.DataGridView5)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(935, 534)
        Me.XtraTabPage1.Text = "Movimientos"
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = False
        Me.DataGridView5.AllowUserToDeleteRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.DataGridView5.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Tipo, Me.Tienda, Me.Numero, Me.Fecha, Me.Compra, Me.Abono, Me.Saldos, Me.Operacion})
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(31, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView5.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridView5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView5.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = True
        Me.DataGridView5.Size = New System.Drawing.Size(935, 534)
        Me.DataGridView5.TabIndex = 11
        '
        'Tipo
        '
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tipo.DefaultCellStyle = DataGridViewCellStyle11
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Width = 52
        '
        'Tienda
        '
        Me.Tienda.HeaderText = "Tienda"
        Me.Tienda.Name = "Tienda"
        Me.Tienda.ReadOnly = True
        Me.Tienda.Width = 64
        '
        'Numero
        '
        Me.Numero.HeaderText = "Numero"
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Width = 69
        '
        'Fecha
        '
        DataGridViewCellStyle12.Format = "d"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle12
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 61
        '
        'Compra
        '
        DataGridViewCellStyle13.Format = "C2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.Compra.DefaultCellStyle = DataGridViewCellStyle13
        Me.Compra.HeaderText = "Venta"
        Me.Compra.Name = "Compra"
        Me.Compra.ReadOnly = True
        Me.Compra.Width = 60
        '
        'Abono
        '
        DataGridViewCellStyle14.Format = "C2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Abono.DefaultCellStyle = DataGridViewCellStyle14
        Me.Abono.HeaderText = "Abono"
        Me.Abono.Name = "Abono"
        Me.Abono.ReadOnly = True
        Me.Abono.Width = 63
        '
        'Saldos
        '
        DataGridViewCellStyle15.Format = "C2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.Saldos.DefaultCellStyle = DataGridViewCellStyle15
        Me.Saldos.HeaderText = "Saldo"
        Me.Saldos.Name = "Saldos"
        Me.Saldos.ReadOnly = True
        Me.Saldos.Width = 58
        '
        'Operacion
        '
        Me.Operacion.HeaderText = "Operacion"
        Me.Operacion.Name = "Operacion"
        Me.Operacion.ReadOnly = True
        Me.Operacion.Width = 81
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.ReportViewer1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(935, 534)
        Me.XtraTabPage2.Text = "Estado de cuenta"
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(935, 534)
        Me.ReportViewer1.TabIndex = 0
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(697, 0)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Excel"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'tabSaldos
        '
        Me.tabSaldos.Controls.Add(Me.DateTimePicker3)
        Me.tabSaldos.Controls.Add(Me.Button5)
        Me.tabSaldos.Controls.Add(Me.GridControl2)
        Me.tabSaldos.Name = "tabSaldos"
        Me.tabSaldos.Size = New System.Drawing.Size(941, 562)
        Me.tabSaldos.Text = "Saldos "
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker3.Location = New System.Drawing.Point(249, 6)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(104, 21)
        Me.DateTimePicker3.TabIndex = 22
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(0, 4)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(177, 23)
        Me.Button5.TabIndex = 21
        Me.Button5.Text = "Imprimir/Exportar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'GridControl2
        '
        Me.GridControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl2.Location = New System.Drawing.Point(0, 33)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(941, 528)
        Me.GridControl2.TabIndex = 20
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        Me.GridView2.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowFooter = True
        Me.GridView2.OptionsView.ShowGroupedColumns = True
        '
        'tabEstadoCuenta
        '
        Me.tabEstadoCuenta.Controls.Add(Me.Button6)
        Me.tabEstadoCuenta.Controls.Add(Me.GridControl3)
        Me.tabEstadoCuenta.Name = "tabEstadoCuenta"
        Me.tabEstadoCuenta.Size = New System.Drawing.Size(941, 562)
        Me.tabEstadoCuenta.Text = "Estado de Cuenta"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(10, 0)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(177, 23)
        Me.Button6.TabIndex = 22
        Me.Button6.Text = "Imprimir/Exportar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'GridControl3
        '
        Me.GridControl3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridControl3.Location = New System.Drawing.Point(0, 28)
        Me.GridControl3.MainView = Me.GridView3
        Me.GridControl3.Name = "GridControl3"
        Me.GridControl3.Size = New System.Drawing.Size(941, 518)
        Me.GridControl3.TabIndex = 21
        Me.GridControl3.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.GridControl = Me.GridControl3
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsFilter.ShowAllTableValuesInFilterPopup = True
        Me.GridView3.OptionsView.ColumnAutoWidth = False
        Me.GridView3.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowFooter = True
        Me.GridView3.OptionsView.ShowGroupedColumns = True
        '
        'Creditos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(953, 618)
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Name = "Creditos"
        Me.Text = "Creditos"
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.tabAplicarPag.ResumeLayout(False)
        CType(Me.XtraTabControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl4.ResumeLayout(False)
        Me.tabSelCliente.ResumeLayout(False)
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.GridControl17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabAplicarPago.ResumeLayout(False)
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl3.ResumeLayout(False)
        Me.XtraTabPage7.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage8.ResumeLayout(False)
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabHistorial.ResumeLayout(False)
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.tabSelClienteH.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabResAbonos.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabResVentas.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMovMes.ResumeLayout(False)
        CType(Me.XtraTabControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl5.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.tabSaldos.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabEstadoCuenta.ResumeLayout(False)
        CType(Me.GridControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabAplicarPag As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabControl4 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabSelCliente As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl17 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView17 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Button52 As System.Windows.Forms.Button
    Friend WithEvents tabAplicarPago As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents XtraTabControl3 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage7 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Pagare As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Secuencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntAnterior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IntGenerado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbonoCapital As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents XtraTabPage8 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DataGridView4 As System.Windows.Forms.DataGridView
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Button54 As System.Windows.Forms.Button
    Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ReportViewer2 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Button53 As System.Windows.Forms.Button
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents tabHistorial As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tabSelClienteH As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tabResAbonos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataGridView6 As System.Windows.Forms.DataGridView
    Friend WithEvents tabResVentas As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataGridView7 As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents tabMovMes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabControl5 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DataGridView5 As System.Windows.Forms.DataGridView
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tienda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Compra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Abono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Operacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents tabSaldos As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tabEstadoCuenta As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GridControl3 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents NoPagare As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Inicial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiasVencido As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InteresAnterior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InteresGenerado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoSinInteres As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoActual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vence As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NSecuencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tiendai As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents subcliente1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents efectivo1 As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
