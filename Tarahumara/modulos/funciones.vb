﻿Imports DevExpress.XtraSplashScreen
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.NetworkInformation
Imports System.Text

Module funciones
    Public regresarazon As Integer
    Public Function Bytes2Image(ByVal bytes() As Byte) As Image
        If bytes Is Nothing Then Return Nothing
        '
        Dim ms As New MemoryStream(bytes)
        Dim bm As Bitmap = Nothing
        Try
            bm = New Bitmap(ms)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.Message)
        End Try
        Return bm
    End Function
    Public Sub IdiomaTextos(ByVal formulario As Form)
        Dim view As New DataView(dtTextos)
        Dim idioma As String = ""
        If (My.Settings.idiomaES = True) Then
            idioma = "Espanol"
        Else
            idioma = "Ingles"
        End If
        Dim filter As String = "Formulario='{0}' AND Control='{1}'"
        filter = String.Format(filter, formulario.Name, formulario.Name)
        view.RowFilter = filter
        If (view.Count > 0) Then
            If (view(0)(idioma) IsNot DBNull.Value) Then
                formulario.Text = view(0)(idioma)
            End If
        End If

        RecorrerControles(formulario.Controls, view, formulario.Name, idioma)
    End Sub

    Private Sub RecorrerControles(ByVal controls As Control.ControlCollection, ByVal view As DataView, ByVal nombre_formulario As String, ByVal idioma As String)
        Dim filter As String = ""
        For Each ctrl As Control In controls
            If (ctrl.GetType.Name = "RadioGroup") Then
                Dim radio As DevExpress.XtraEditors.RadioGroup = CType(ctrl, DevExpress.XtraEditors.RadioGroup)
                For Each item As DevExpress.XtraEditors.Controls.RadioGroupItem In radio.Properties.Items
                    filter = "Formulario='{0}' AND Control='{1}' AND Espanol='{2}'"
                    filter = String.Format(filter, nombre_formulario, ctrl.Name, item.Description)
                    view.RowFilter = filter
                    If (view.Count > 0) Then
                        If (view(0)(idioma) IsNot DBNull.Value) Then
                            item.Description = view(0)(idioma).ToString().Trim()
                        End If
                    End If
                Next
            End If
            filter = "Formulario='{0}' AND Control='{1}'"
            filter = String.Format(filter, nombre_formulario, ctrl.Name)
            If (ctrl.Name = "XtraTabPage9") Then
                Dim c As Integer = 0
            End If
            view.RowFilter = filter
            If (view.Count > 0) Then
                If (view(0)(idioma) IsNot DBNull.Value) Then
                    ctrl.Text = view(0)(idioma).ToString().Trim()
                End If
            End If
            RecorrerControles(ctrl.Controls, view, nombre_formulario, idioma)
        Next
    End Sub

    Enum Mensaje
        Titulo = 0
        Texto = 1
    End Enum

    Public Function IdiomaMensajes(ByVal ID As Integer, ByVal tipo As Mensaje) As String
        dvMensajes.RowFilter = "Numero = " & ID
        Dim regresa As String = ""
        If (dvMensajes.Count > 0) Then
            If (tipo = Mensaje.Titulo) Then 'Titulo
                If (My.Settings.idiomaES = True) Then
                    If (dvMensajes(0)("Titulo Espanol") IsNot DBNull.Value) Then
                        Return dvMensajes(0)("Titulo Espanol").ToString()
                    End If
                Else
                    If (dvMensajes(0)("Titulo Ingles") IsNot DBNull.Value) Then
                        Return dvMensajes(0)("Titulo Ingles").ToString()
                    End If
                End If
            ElseIf (tipo = Mensaje.Texto) Then 'Texto
                If (My.Settings.idiomaES = True) Then
                    If (dvMensajes(0)("Texto Espanol") IsNot DBNull.Value) Then
                        Return dvMensajes(0)("Texto Espanol").ToString()
                    End If
                Else
                    If (dvMensajes(0)("Texto Ingles") IsNot DBNull.Value) Then
                        Return dvMensajes(0)("Texto Ingles").ToString()
                    End If
                End If
            End If
        End If
        Return regresa
    End Function
    Public Function Image2Bytes(ByVal img As Image) As Byte()
        Dim sTemp As String = Path.GetTempFileName()
        Dim fs As New FileStream(sTemp, FileMode.OpenOrCreate, FileAccess.ReadWrite)
        img.Save(fs, System.Drawing.Imaging.ImageFormat.Png)
        fs.Position = 0
        '
        Dim imgLength As Integer = CInt(fs.Length)
        Dim bytes(0 To imgLength - 1) As Byte
        fs.Read(bytes, 0, imgLength)
        fs.Close()
        Return bytes
    End Function
    Public Sub BuscaEnComboDx(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal valor As String, Optional ByVal buscarEnId As Boolean = False, Optional ByVal Absoluto As Integer = 0)
        Dim posicion As Integer = -1
        Dim bContinua As Boolean = True
        If IsNumeric(valor) Then
            If valor <= -1 Then bContinua = False
        End If
        If bContinua Then
            If Absoluto = 0 Then
                For i As Integer = 0 To combo.Properties.Items.Count - 1
                    Dim oInfoCombo As cInfoCombo = CType(combo.Properties.Items.Item(i), cInfoCombo)
                    If buscarEnId Then
                        If oInfoCombo.ID = valor Then
                            posicion = i
                            Exit For
                        End If
                    Else
                        If oInfoCombo.Text = valor Then
                            posicion = i
                            Exit For
                        End If
                    End If
                Next
            Else
                posicion = Absoluto - 1
            End If
        End If
        combo.SelectedIndex = posicion
    End Sub
    Public Sub LlenaComboDx(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal myId As String, ByVal campo As String, ByVal tabla As String, ByVal conn As String)
        combo.Properties.Items.Clear()
        Dim dr As SqlDataReader = bdBase.bdDataReader(conn, "SELECT " & myId & ", " & campo & " FROM " & tabla & " ORDER BY " & campo)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                Do While dr.Read
                    If Not IsDBNull(dr(myId)) And Not IsDBNull(dr(campo)) Then
                        combo.Properties.Items.Add(New cInfoCombo(CInt(dr(myId)), dr(campo).ToString.Trim))
                        'Else
                        '    DevExpress.XtraEditors.XtraMessageBox.Show(dr(campo).ToString.Trim & " sin " & myId, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    End If
                Loop
            End If
            dr.Close()
            combo.SelectedIndex = -1
        End If
    End Sub
    Public Function FechaHoraSvr() As String
        Dim regresa As String = String.Empty
        Dim cfecha As String
        Dim theDate As Date
        Dim iqry As String = "Select getDate() as FechaHora "
        Dim druk As SqlDataReader = bdBase.bdDataReader(conexion, iqry)
        druk.Read()
        theDate = druk("FechaHora")
        druk.Close()
        cfecha = theDate.ToString("yyyyMMdd")
        regresa = cfecha
        Return regresa
    End Function
    Public Sub Splash(Muestra As Boolean)
        If Not IsNothing(SplashScreenManager.Default) Then
            If SplashScreenManager.Default.IsSplashFormVisible Then SplashScreenManager.CloseForm(False)
        End If
        If Muestra Then SplashScreenManager.ShowForm(GetType(WaitForm1), False, False)
    End Sub
    Public Sub RegistraAcceso(conexion As String, Opcion As String)
        Try
            Dim pingSender As New Ping
            Dim options As New PingOptions
            options.DontFragment = True
            Dim data As String = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            Dim buffer() As Byte = Encoding.ASCII.GetBytes(data)
            Dim timeout As Integer = 120
            Dim reply As PingReply = pingSender.Send("merlot.dnsalias.com", timeout, buffer, options)
            If reply.Status = IPStatus.Success Then
                Dim usuario As Integer = 0
                usuario = oLogin.pUserId
                Dim sQuery As String = "INSERT INTO log (Usuario, Fecha, idTienda, Opcion) VALUES (" & usuario & ", GetDate(), " & My.Settings.TiendaActual & ", '" & Opcion & "')"
                Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
                If Not String.IsNullOrWhiteSpace(regresa) Then
                    'ReportaError("Registra acceso: " & regresa & vbCrLf & sQuery)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LlenaCombo(ByVal combo As ComboBox, ByVal qry As String, ByVal campo As String, ByVal myId As String, ByVal cnCon As String)
        combo.Items.Clear()
        Dim dr As SqlDataReader = Nothing
        Dim cnCon2 As New SqlConnection(cnCon)
        Dim cmd As New SqlCommand(qry, cnCon2)
        Try
            cnCon2.Open()

            cmd.CommandType = CommandType.Text
            dr = cmd.ExecuteReader
            If dr.HasRows Then
                While dr.Read
                    If Not IsDBNull(dr(myId)) And Not IsDBNull(dr(campo)) Then

                        combo.Items.Add(New cInfoCombo((dr(myId)).ToString.Trim, dr(campo).ToString.Trim))


                    End If
                End While
            End If
        Catch ex As Exception
            'ReportaError(cmd, ex.Message)
            MsgBox(ex.Message)
        Finally
            cmd.Dispose()
            If Not IsNothing(dr) Then dr.Close()
            cnCon2.Close()
        End Try
    End Sub

    Public Sub LlenaCombobox(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal dr As SqlDataReader, ByVal campo As String, ByVal myId As String)
        combo.Properties.Items.Clear()
        Try

            If dr.HasRows Then
                While dr.Read
                    If Not IsDBNull(dr(myId)) And Not IsDBNull(dr(campo)) Then

                        combo.Properties.Items.Add(New cInfoCombo(CInt(dr(myId)), dr(campo)))


                    End If
                End While
            End If
        Catch ex As Exception

            MsgBox(ex.Message)
        Finally

            If Not IsNothing(dr) Then dr.Close()

        End Try
    End Sub

    Public Function TipoEntrada(apertura As String, entrada As String, salida As String, tolerancia As String, retardo As String, tiempoComida As String, toleraComida As String, fecha As DateTime) As Integer
        Dim regresa As Integer = 0
        If Not String.IsNullOrEmpty(apertura) And Not String.IsNullOrEmpty(entrada) And Not String.IsNullOrEmpty(salida) And Not String.IsNullOrEmpty(tolerancia) And _
            Not String.IsNullOrEmpty(retardo) And Not String.IsNullOrEmpty(tiempoComida) And Not String.IsNullOrEmpty(toleraComida) Then
            Dim diferencia As Integer = DateDiff(DateInterval.Minute, Convert.ToDateTime(entrada), fecha)
            Dim tiempo As Integer = Convert.ToInt32(tolerancia) + Convert.ToInt32(retardo)
            If diferencia <= Convert.ToInt32(tolerancia) Then regresa = 0
            If diferencia <= tiempo Then regresa = 1 Else regresa = 2
        End If
        Return regresa
    End Function
    Public Sub LlenaCombobox(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal qry As String, ByVal campo As String, ByVal myId As String, ByVal cnCon As String)
        combo.Properties.Items.Clear()
        Try
            Dim dr As SqlDataReader = bdBase.bdDataReader(cnCon, qry)
            If dr.HasRows Then
                While dr.Read
                    If Not IsDBNull(dr(myId)) And Not IsDBNull(dr(campo)) Then
                        combo.Properties.Items.Add(New cInfoCombo(CInt(dr(myId)), dr(campo)))
                    End If
                End While
            End If
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
        End Try
    End Sub
    Public Sub LlenaComboboxLlave(ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal qry As String, ByVal campo As String, ByVal myId As String, ByVal cnCon As String)
        combo.Properties.Items.Clear()
        Try
            Dim dr As SqlDataReader = bdBase.bdDataReader(cnCon, qry)
            If dr.HasRows Then
                While dr.Read
                    If Not IsDBNull(dr(myId)) And Not IsDBNull(dr(campo)) Then
                        combo.Properties.Items.Add(New cInfoCombo((dr(myId)), dr(campo)))
                    End If
                End While
            End If
            dr.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
        End Try
    End Sub
    Public Function BuscaIdTienda(nombreTienda As String) As String
        Dim regresa As String = String.Empty
        Dim sQuery As String = "SELECT id FROM Tiendas WHERE UPPER(LTRIM(RTRIM(Nombre))) = '" & nombreTienda.Trim.ToUpper & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr("id")
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Function efectivodeldia(Optional pTienda As String = "", Optional pFecha As DateTime = #12:00:00 AM#, Optional espere As Boolean = True) As Double
        If espere Then Splash(True)
        Dim sTienda As String
        Dim sCaja(9) As String
        Dim dfecha As DateTime
        If Not String.IsNullOrEmpty(pTienda) Then
            sTienda = BuscaIdTienda(pTienda)
            dfecha = pFecha
            For i As Integer = 0 To 9
                sCaja(i) = String.Empty
            Next
        Else
            'sTienda = My.Settings.TiendaActual
            'sCaja(0) = " and detnotas.caja='" & My.Settings.CajaActual & "'"
            'sCaja(1) = " and caja='" & My.Settings.CajaActual & "'  "
            'sCaja(2) = " and creditos.caja='" & My.Settings.CajaActual & "' "
            'sCaja(3) = " and abonos.caja='" & My.Settings.CajaActual & "'  "
            'sCaja(4) = " and fma.caja='" & My.Settings.CajaActual & "'"
            'sCaja(5) = " and fma.caja='" & My.Settings.CajaActual & "'"
            'sCaja(6) = " and vales.caja='" & My.Settings.CajaActual & "' "
            'sCaja(7) = " and valeut.caja='" & My.Settings.CajaActual & "'   "
            'sCaja(8) = " and detgasto.caja='" & My.Settings.CajaActual & "'  "
            'sCaja(9) = " and liquidaciones.caja='" & My.Settings.CajaActual & "'"
            'dfecha = DateTime.Now
        End If

        Dim rider As SqlDataReader
        Dim efectivoactual As Decimal = 0
        Dim fechadel, qry As String
        fechadel = Date.Parse(dfecha).Date.ToString("yyyyMMdd")

        qry = " select isnull ( cast( Sum(precio * (1 - descuen / 100)) as decimal(16,2) ),0) As MontoVendido    " & _
              " from detnotas WHERE     (detnotas.FECHA = '" & fechadel & "') and detnotas.tienda='" & _
              sTienda & "' " & sCaja(0)

        rider = bdBase.bdDataReader(conexion, qry)
        If rider.HasRows Then
            rider.Read()
            efectivoactual = rider("MontoVendido")
        End If

        qry = "select isnull(sum(importe),0) as Monto  from pagosapa   WHERE     ( FECHA = '" & fechadel & "') and  tienda='" & sTienda & "' " & sCaja(1)
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual + dr("Monto")
        End If
        dr.Close()

        qry = "select isnull(sum(inicial/pagos),0) as Total from creditos WHERE creditos.FECHA = '" & fechadel & "'  and creditos.tienda='" & sTienda & "' " & sCaja(2)
        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual - dr("Total")
        End If
        dr.Close()

        qry = "select isnull(sum(total),0) as Total from abonos WHERE abonos.FECHA = '" & fechadel & "'  and abonos.tienda='" & sTienda & "' " & sCaja(3)
        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual + dr("Total")
        End If
        dr.Close()
        ' aqui pongo todas las formas de pago.
        qry = "select clave,nombre,tipo from formaspago where tienda='" & sTienda & "' and status=1"
        Dim dr2 As DataSet = bdBase.bdDataset(conexion, qry)
        Dim dt As DataTable = dr2.Tables(0)

        For Each renglon As DataRow In dt.Rows
            If (renglon("Clave").ToString.Trim.ToUpper.Substring(0, 1) = "E" Or renglon("Clave").ToString.Trim.ToUpper.Substring(0, 1) = "D") And IsNumeric(renglon("Clave").ToString.Trim.ToUpper.Substring(1, 1)) Or renglon("Clave").ToString.Trim.ToUpper.Substring(0, 2) = "VD" Then
                'Dim caja As String = " and fma.caja='" & sCaja & "'"

                qry = "select isnull(sum(" & renglon("Clave").ToString.Trim.ToUpper & "),0) as idFma  from fma   WHERE      fma.FECHA = '" & fechadel & _
                      "' and fma.tienda='" & sTienda & "' and (status<>2 or status is null) " & sCaja(4)
                dr = bdBase.bdDataReader(conexion, qry)
                If dr.HasRows Then
                    dr.Read()
                    If dr("idFma") > 0 Then
                        efectivoactual = efectivoactual - dr("idFma")
                    End If
                End If
                dr.Close()
            End If
        Next
        qry = " select isnull(sum(tarjetas),0)  as Tarjetas from fma WHERE     (fma.FECHA = '" & fechadel & "') and fma.tienda='" & sTienda & "' and fma.tarjetas>0 " & sCaja(5)
        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual - dr("Tarjetas")
        End If
        dr.Close()

        qry = "select isnull(sum(total),0) as Total from vales  WHERE vales.FECHA = '" & fechadel & "'  and vales.tienda='" & sTienda & "' " & sCaja(6)

        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual + dr("Total")
        End If
        dr.Close()

        qry = "select isnull(sum(total),0) as Total from valeut  WHERE valeut.FECHA = '" & fechadel & "'  and valeut.tienda='" & sTienda & "' " & sCaja(7)
        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual - dr("Total")
        End If
        dr.Close()
        qry = "select isnull(sum(importe),0) as Total from detgasto  WHERE detgasto.FECHA = '" & fechadel & "'  and detgasto.tienda='" & sTienda & "' " & sCaja(8)
        dr = bdBase.bdDataReader(conexion, qry)
        If dr.HasRows Then
            dr.Read()
            efectivoactual = efectivoactual - dr("Total")
        End If
        dr.Close()

        If String.IsNullOrEmpty(pTienda) Then
            qry = "select isnull(sum(monto),0) as Total from liquidaciones  WHERE DATEADD(DD, 0, DATEDIFF(DD, 0, liquidaciones.FECHA )) = '" & fechadel & _
              "'  and liquidaciones.idtienda='" & sTienda & "' " & sCaja(9)
            dr = bdBase.bdDataReader(conexion, qry)
            If dr.HasRows Then
                dr.Read()
                efectivoactual = efectivoactual - dr("Total")
            End If
            dr.Close()
        End If

        If espere Then Splash(False)
        Return efectivoactual
    End Function

    Public Function ObtieneNoTiendaSaz(ByVal conexionSaz As String, ByVal conexionFacturador As String, ByVal tienda As Integer)
        Dim regresa As Integer = -1
        If tienda = 269 Then
            regresa = 1
        Else
            Dim sQuery As String = "SELECT numero, NOMBRE FROM tiendas WHERE tiendasaz = " & tienda
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexionSaz, sQuery)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    'sQuery = "SELECT Numero FROM tiendas WHERE tiendasaz = '" & dr("NOMBRE").ToString.Trim & "'"
                    'Dim drF As SqlDataReader = bdBase.bdDataReader(conexionFacturador, sQuery)
                    'If Not drF Is Nothing Then
                    '    If drF.HasRows Then
                    '        drF.Read()
                    regresa = dr("Numero")
                    '    End If
                    '    drF.Close()
                    'End If
                End If
                dr.Close()
            End If
        End If
        Return regresa
    End Function

    Public Sub FillCombobox(ByVal elemento As String, ByVal id As String, ByVal combo As DevExpress.XtraEditors.ComboBoxEdit, ByVal tabla As String, Optional campo As String = "numero", Optional qryOpcional As String = "")
        Dim qry As String
        If String.IsNullOrWhiteSpace(id) Or id = "-1" Then
            combo.SelectedIndex = 0
        Else
            If qryOpcional <> "" Then
                qry = qryOpcional
            Else
                qry = "SELECT      " & elemento & _
                   " FROM   " & tabla & _
                   " WHERE     (" & campo & " ='" & id & "')"
            End If
            Dim dr2 As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If dr2.HasRows Then
                dr2.Read()
                Dim aux As String = dr2(elemento).ToString.Trim
                For x As Integer = 0 To combo.Properties.Items.Count
                    If combo.Properties.Items(x).ToString.Trim.ToUpper = aux.ToString.Trim.ToUpper Then
                        combo.SelectedIndex = x
                        Exit For

                    End If
                    'If x = (combo.Properties.Items.Count - 1) Then
                    '    Exit For
                    'End If
                Next

            End If
            dr2.Close()
        End If
    End Sub

    Public Sub ObtieneNumeroRazon(ByVal tienda As Integer)

        Dim sQuery As String = "SELECT razon  FROM tiendas WHERE numero = '" & tienda & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                regresarazon = dr("razon")
            End If
            dr.Close()
        End If
    End Sub

    Public Function getBarcode(id As String) As String
        Dim regresa As String = String.Empty
        Dim talla As String = "0"
        id = id.PadLeft(11, "0")

        Dim dll As New etiqueta.Class1
        regresa = dll.EAN13(id & talla)

        Return regresa
    End Function

    Public Sub LlenaComboAsistencia(combo As Object)
       Dim qry As String = ""
        Dim fechasql As String
        qry = "SELECT CONVERT(nCHAR(8), GETDATE() , 112) as Fecha"
        Dim ds As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        ds.Read()
        fechasql = ds("fecha")
        ds.Close()

        ' Crear temporales
        qry = "CREATE TABLE [empleadoc3] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[FECHA] datetime NULL, [tipoope] nchar(14) NULL)"
        Dim regresa As String = bdBase.bdExecute(conexion, qry)
        qry = "CREATE TABLE [empleadoc2] ([numero] numeric(18,0) NULL, [NOMBRE] nvarchar(200) NULL, [Usuario] nvarchar(100) NULL, " & _
              "[ultimao] datetime NULL)"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Insertar datos 
        qry = "INSERT INTO empleadoc3 (numero, nombre, Usuario, fecha, tipoope) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], logdia.fecha, logdia.tipo FROM empleado " & _
              "inner join logdia on logdia.nombre= empleado.nombre " & _
              "WHERE logdia.TIENDA = '" & My.Settings.Item("TiendaActual") & "' AND  CONVERT(nCHAR(8), logdia.fecha , 112)= '" & fechasql & "' "
        regresa = bdBase.bdExecute(conexion, qry)
        qry = "INSERT INTO empleadoc2 (numero, nombre, Usuario, ultimao) " & _
              "SELECT empleado.numero, empleado.nombre, empleado.[USER], MAX(logdia.fecha) from empleado " & _
              "inner join logdia on logdia.nombre = empleado.nombre and DATEPART(year, logdia.fecha)= DATEPART(year, getdate()) " & _
              "and DATEPART(month, logdia.fecha)=DATEPART(month, getdate()) and DATEPART(day, logdia.fecha)=DATEPART(day, getdate()) and logdia.tienda='" & _
              My.Settings.Item("TiendaActual") & "' group by empleado.numero, empleado.nombre, empleado.[USER]  order BY [user]"
        regresa = bdBase.bdExecute(conexion, qry)
        ' Query final
        qry = "Select empleadoc3.numero, empleadoc3.nombre, empleadoc3.usuario From empleadoc2 " & _
              "INNER Join empleadoc3 On empleadoc2.numero= empleadoc3.numero And empleadoc2.ultimao= empleadoc3.fecha " & _
              "Where empleadoc3.tipoope like 'ENTRADA%'"
        LlenaCombobox(combo, qry, "nombre", "numero", conexion)
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc3') BEGIN DROP TABLE empleadoc3 END")
        regresa = bdBase.bdExecute(conexion, "IF EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='empleadoc2') BEGIN DROP TABLE empleadoc2 END")

        ' *****************************************************
        ' **** terminado
        ' *****************************************************
        If combo.Properties.Items.Count >= 1 Then
            combo.SelectedIndex = 0
        Else
            combo.SelectedIndex = -1
        End If
        'addtabs()       
    End Sub
    Public Function GetOperacion(valor As Long, Optional tipoDoc As String = "todos") As String
        Dim regresa As String = String.Empty
        Dim consecutivo As Long = valor + 1
        Dim theDate As DateTime, qry As String = "", cfecha As String
        Dim dr As SqlDataReader = Nothing

        'Select Case tipo.ToLower
        '    Case "operacion"
        '        consecutivo = Convert.ToInt64(My.Settings.syncOperacion) + 1
        '    Case "devolucion"
        '        consecutivo = Convert.ToInt64(My.Settings.syncDevolucion) + 1
        'End Select
        qry = "Select getDate() as FechaHora "
                dr = bdBase.bdDataReader(conexion, qry)
        dr.Read()
        theDate = dr("FechaHora")
        cfecha = String.Format("{0:yyyyMMddHHmmss}", theDate)
        'Dim FechaActual As String = String.Format("{0:yyyyMMddHHmmss}", System.DateTime.Now)
        Dim FechaActual As String = cfecha
        If tipoDoc = "todos" Then
            regresa = My.Settings.CajaActual.ToString.PadLeft(1, "0") & FechaActual.Substring(2, 6) & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & consecutivo.ToString.PadLeft(4, "0")
        ElseIf tipoDoc = "apartado" Then
            regresa = FechaActual.Substring(2, 6) & My.Settings.CajaActual.ToString.PadLeft(1, "0") & My.Settings.TiendaActual.ToString.PadLeft(3, "0") & consecutivo.ToString.PadLeft(4, "0")
        End If
        Return regresa
    End Function
End Module
