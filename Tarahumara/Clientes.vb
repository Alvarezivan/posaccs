﻿
Imports System.Data.SqlClient

Public Class Clientes


    Dim idsub As Integer
    Dim Socio As Double
    Dim limite As Decimal
    Private Sub Clientes_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Splash(True)

        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        LookAndFeel.SetSkinStyle(My.Settings.skin)


        Me.Text = " [Clientes] " & My.Application.Info.Version.ToString & " - " & oLogin.pEmpNombre
        GroupControl2.Enabled = False
        Splash(False)
    End Sub

    Private Sub Button36_Click(sender As System.Object, e As System.EventArgs) Handles Button36.Click
        Me.Cursor = Cursors.WaitCursor
        Dim filtroEstatus As String = String.Empty
        If RdgSocioEstatusFiltro.SelectedIndex = 0 Then
            filtroEstatus = " and estatus = 1 "
        ElseIf RdgSocioEstatusFiltro.SelectedIndex = 1 Then
            filtroEstatus = " and isnull(estatus,0) = 0 "
        ElseIf RdgSocioEstatusFiltro.SelectedIndex = 2 Then

        End If
        Dim qry As String = " select id,ltrim(rtrim(nombre)) as nombre,ltrim(rtrim(calle))+','+ltrim(rtrim(NumeroExt))+'-'+rtrim(ltrim(NumeroInt))+','+ltrim(rtrim(colonia)) as Direccion, " & _
"ltrim(rtrim(Ciudad))+','+ltrim(rtrim(Municipio))+','+ltrim(rtrim(estado))+','+CodigoPostal as ciudad, 'RFC:'+ ltrim(rtrim(isnull(RFC,'')))+',Tel.'+ltrim(rtrim(isnull(Telefono,'')))+',Email'+ltrim(rtrim(isnull(correo,'')))   as rfc " & _
",isnull(desccontado,0) as DescSocio   from socios where Nombre like '%" & TextBox8.Text.Trim & "%' {0} order by nombre"
        qry = String.Format(qry, filtroEstatus)
        Dim ds As DataSet = bdBase.bdDataset(conexion, qry)
        DataGridView1.DataSource = ds.Tables(0)
        DataGridView1.AutoResizeColumns()
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick

        Socio = DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value.ToString.Trim


        Dim qry As String = " select id,ltrim(rtrim(nombre)) as nombre,ltrim(rtrim(calle)) as Calle,ltrim(rtrim(NumeroExt)) as NumeroExt,rtrim(ltrim(NumeroInt)) as NumeroInt,ltrim(rtrim(colonia)) as Colonia, " & _
"ltrim(rtrim(Ciudad)) as Ciudad,ltrim(rtrim(Municipio)) as municipio,ltrim(rtrim(estado)) as estado,CodigoPostal , ltrim(rtrim(isnull(RFC,''))) as RFC,ltrim(rtrim(isnull(Telefono,''))) as telefono,ltrim(rtrim(isnull(correo,'')))   as correo " & _
",isnull(DescContado,0) as DescSocio, isnull(Limitecredito,0) as LimiteCredito, isnull(PlazoDias,0) as " _
& "PlazoDias,isnull(birthday,getdate()) as birthday,isnull(numpagos,0) as numpagos, isnull(tasainteres,0) as interes,isnull(idtiposocio,-1) as idtiposocio " _
& "  from socios where id=' " & DataGridView1.Item(0, DataGridView1.CurrentRow.Index).Value.ToString.Trim & "'"

        qry = "select socios.id,ltrim(rtrim(socios.nombre)) as nombre,ltrim(rtrim(calle)) as Calle,ltrim(rtrim(NumeroExt)) as " _
            & "NumeroExt,rtrim(ltrim(NumeroInt)) as NumeroInt,ltrim(rtrim(colonia)) as Colonia, ltrim(rtrim(Ciudad)) as " _
            & "Ciudad,ltrim(rtrim(Municipio)) as municipio,ltrim(rtrim(estado)) as estado,CodigoPostal , ltrim(rtrim(isnull(RFC,''))) " _
            & "as RFC,ltrim(rtrim(isnull(Telefono,''))) as telefono,ltrim(rtrim(isnull(correo,'')))   as correo ,isnull(DescContado,0) " _
            & "as DescSocio, isnull(Limitecredito,0) as LimiteCredito, isnull(PlazoDias,0) as PlazoDias,isnull(birthday,getdate()) as " _
            & "birthday,isnull(numpagos,0) as numpagos, isnull(tasainteres,0) as interes,isnull(Fecalta,getdate()) as Falta,isnull(Modificado,getdate()) as Modificado,isnull(ingresos,0) as Ingresos,rtrim(ltrim(Comentarios)) as " _
            & "Comentarios,rtrim(ltrim(Refn1)) as Rn1,rtrim(ltrim(Refn2)) as Rn2,rtrim(ltrim(Refn3)) as Rn3,rtrim(ltrim(Reft1)) as Rt1,rtrim(ltrim(Reft2)) as " _
            & "Rt2,rtrim(ltrim(Reft3)) as Rt3,rtrim(ltrim(Refcn1)) as Rcn1,rtrim(ltrim(Refcn2)) as Rcn2,rtrim(ltrim(Refcn3)) as Rcn3,rtrim(ltrim(Refct1)) as " _
            & "Rct1,rtrim(ltrim(Refct2)) as Rct2,rtrim(ltrim(Refct3)) as Rct3, rtrim(ltrim(Calificacion)) as Calificacion " _
            & ",isnull(estatus,0) as estatus, rtrim(ltrim(ActivarEfectivo)) as AE from socios where socios.id='" & Socio & "'"

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()



                If Not IsDBNull(dr("id")) Then tbid.Text = dr("id")
                If Not IsDBNull(dr("nombre")) Then tbnombre.Text = dr("nombre").ToString
                If Not IsDBNull(dr("calle")) Then tbcalle.Text = dr("calle").ToString
                If Not IsDBNull(dr("numeroext")) Then tbnumero.Text = dr("numeroext").ToString
                If Not IsDBNull(dr("telefono")) Then tbtelefono.Text = dr("telefono").ToString
                If Not IsDBNull(dr("numeroint")) Then tbnumeroint.Text = dr("NumeroInt").ToString
                If Not IsDBNull(dr("colonia")) Then tbcolonia.Text = dr("colonia").ToString
                If Not IsDBNull(dr("ciudad")) Then tbciudad.Text = dr("ciudad").ToString
                If Not IsDBNull(dr("municipio")) Then tbmunicipio.Text = dr("municipio").ToString
                If Not IsDBNull(dr("estado")) Then tbestado.Text = dr("estado").ToString
                If Not IsDBNull(dr("codigopostal")) Then tbcp.Text = dr("CodigoPostal").ToString
                If Not IsDBNull(dr("rfc")) Then tbrfc.Text = dr("rfc").ToString
                If Not IsDBNull(dr("correo")) Then tbemail.Text = dr("correo").ToString
                If Not IsDBNull(dr("DescSocio")) Then tbdesc.Text = dr("DescSocio").ToString
                If Not IsDBNull(dr("LimiteCredito")) Then tblimite.Text = dr("LimiteCredito").ToString
                If Not IsDBNull(dr("LimiteCredito")) Then limite = dr("LimiteCredito")
                If Not IsDBNull(dr("PlazoDias")) Then tbplazodias.Text = dr("PlazoDias").ToString
                If Not IsDBNull(dr("interes")) Then tbinteres.Text = dr("interes").ToString
                If Not IsDBNull(dr("birthday")) Then dtpnacimiento.Value = dr("birthday")
                If Not IsDBNull(dr("Ingresos")) Then tbingresos.Text = dr("Ingresos").ToString
                If Not IsDBNull(dr("Comentarios")) Then tbcomentarios.Text = dr("Comentarios").ToString
                If Not IsDBNull(dr("Rn1")) Then tbrefn1.Text = dr("Rn1").ToString
                If Not IsDBNull(dr("Rn2")) Then tbrefn2.Text = dr("Rn2").ToString
                If Not IsDBNull(dr("Rn3")) Then tbrefn3.Text = dr("Rn3").ToString
                If Not IsDBNull(dr("Rt1")) Then tbreft1.Text = dr("Rt1").ToString
                If Not IsDBNull(dr("Rt2")) Then tbreft2.Text = dr("Rt2").ToString
                If Not IsDBNull(dr("Rt3")) Then tbreft3.Text = dr("Rt3").ToString
                If Not IsDBNull(dr("Rcn1")) Then tbrefcn1.Text = dr("Rcn1").ToString
                If Not IsDBNull(dr("Rcn2")) Then tbrefcn2.Text = dr("Rcn2").ToString
                If Not IsDBNull(dr("Rct1")) Then tbrefct1.Text = dr("Rct1").ToString
                If Not IsDBNull(dr("Rct2")) Then tbrefct2.Text = dr("Rct2").ToString
                If Not IsDBNull(dr("falta")) Then dtpalta.Value = dr("falta")
                If Not IsDBNull(dr("Modificado")) Then dtpmodificacion.Value = dr("Modificado")
                If Not IsDBNull(dr("Calificacion")) Then tbcalificacion.Text = dr("Calificacion").ToString
                If Not IsDBNull(dr("estatus")) Then ChkEstatusSocio.Checked = dr("estatus")
                If Not IsDBNull(dr("AE")) Then chkae.Checked = dr("AE")
             
                End If
                Button35.Enabled = True
                GroupControl2.Enabled = True
            End If
            dr.Close()


        LlenaSubSocios(Socio)


    End Sub

    Private Sub Button35_Click(sender As System.Object, e As System.EventArgs) Handles Button35.Click

        Dim qry As String

        Dim limit As Decimal = tblimite.Text.Trim

        If limite <> limit Then
            qry = "update socios set modificado = getdate() where socios.id='" & tbid.Text & "'"
            bdBase.bdExecute(conexion, qry)
        End If

        qry = " update [socios]  set [Nombre]=' " & tbnombre.Text & "'" & _
                 "  ,[Calle]='" & tbcalle.Text & "'" & _
                 " ,[NumeroExt]='" & tbnumero.Text & "'" & _
                 ",[NumeroInt]='" & tbnumeroint.Text & "'" & _
                 ",[Colonia]='" & tbcolonia.Text & "'" & _
                 ",[Ciudad]=' " & tbciudad.Text & "'" & _
                 "   ,[Municipio]='" & tbmunicipio.Text & "'" & _
                 ",[Estado]='" & tbestado.Text & "'" & _
                 ",[CodigoPostal]='" & tbcp.Text & "'" & _
                 ",[RFC]='" & tbrfc.Text & "'" & _
                 ",[Correo]=' " & tbemail.Text & "'" & _
                 "   ,[Telefono]='" & tbtelefono.Text & "'" & _
                 ",[birthday]='" & String.Format("{0:yyyyMMdd}", dtpnacimiento.Value) & "'" & _
                ",[DescContado]='" & tbdesc.Text & "'" & _
                ",[LimiteCredito]='" & tblimite.Text & "'" & _
                ",[PlazoDias]='" & tbplazodias.Text & "'" & _
                  ",[tasainteres]='" & tbinteres.Text & "'" & _
                  ",[comentarios]='" & tbcomentarios.Text & "'" & _
                  ",[Refn1]='" & tbrefn1.Text & "'" & _
                  ",[Refn2]='" & tbrefn2.Text & "'" & _
                  ",[Refn3]='" & tbrefn3.Text & "'" & _
                  ",[Reft1]='" & tbreft1.Text & "'" & _
                  ",[Reft2]='" & tbreft2.Text & "'" & _
                  ",[Reft3]='" & tbreft3.Text & "'" & _
        ",[Refcn1]='" & tbrefcn1.Text & "'" & _
        ",[Refcn2]='" & tbrefcn2.Text & "'" & _
        ",[Refct1]='" & tbrefct1.Text & "'" & _
        ",[Refct2]='" & tbrefct2.Text & "'" & _
        ",[Ingresos]='" & tbingresos.Text & "'" & _
                    ",[TiendaAlta]=' " & My.Settings.Item("TiendaActual").ToString & "',[estatus] = " & IIf(ChkEstatusSocio.Checked, 1, 0) & ", activarefectivo = " & IIf(chkae.Checked, 1, 0) & " where socios.id='" & tbid.Text & "'"
        bdBase.bdExecute(conexion, qry)
        MsgBox("Cliente actualizado con exito", MsgBoxStyle.Information)

    End Sub

    Private Sub LlenaSubSocios(ByVal idsocio)
        dgvsubclientes.Rows.Clear()
        Dim id As Integer
        Dim qry As String = " Select numero from SubSocios where idsocio = '" & idsocio & "'"
        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
        If Not dr Is Nothing Then
            If dr.HasRows Then
                While dr.Read()
                    If Not IsDBNull(dr("numero")) Then id = dr("numero").ToString.Trim
                    cargaSubsocio(id)
                End While
            End If
        End If
        dr.Close()
    End Sub

    Private Sub cargaSubsocio(ByVal id)
        Dim qry As String

        qry = " Select numero,nombre,limitecredito,Activo from Subsocios where numero = '" & id & "' "

        Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)

        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                dgvsubclientes.Rows.Add(dr("numero").ToString.Trim, dr("nombre").ToString.Trim, dr("limitecredito").ToString.Trim, _
                                       dr("Activo").ToString.Trim)
            End If

        End If
        dr.Close()
    End Sub


    Private Sub dgvsubclientes_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvsubclientes.CellContentDoubleClick


        Dim x As Integer = e.RowIndex
        idsub = dgvsubclientes.Item(0, x).Value.ToString.Trim

        Dim qry As String = " Select nombre as n,Limitecredito as l,Activo as a from SubSocios where numero = '" & idsub & "'"
        Try
            Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, qry)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    If Not IsDBNull(dr("n")) Then tbsubnombre.Text = dr("n").ToString.Trim
                    If Not IsDBNull(dr("l")) Then tbsublimite.Text = dr("l").ToString.Trim
                    If Not IsDBNull(dr("a")) Then chksubactivo.Checked = dr("a").ToString.Trim

                End If
            End If
            dr.Close()
            bguardarsub.Text = "Actualizar"
            tbsubnombre.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub bguardarsub_Click(sender As System.Object, e As System.EventArgs) Handles bguardarsub.Click
        Try
            If bguardarsub.Text = "Actualizar" Then


                Dim qry As String = "update subsocios set limitecredito = '" & tbsublimite.Text.Trim & "', activo = '" & IIf(chksubactivo.Checked, 1, 0) & "' where numero = " & idsub
                bdBase.bdExecute(conexion, qry)
                MsgBox("Subcliente actualizado con exito", MsgBoxStyle.Information)
                tbsubnombre.Text = ""
                tbsublimite.Text = ""
                chksubactivo.Checked = False
                tbsubnombre.ReadOnly = False
                bguardarsub.Text = "Guardar"
                LlenaSubSocios(Socio)
        Else
                Dim qry As String = "Insert into subsocios (Nombre,idsocio,limitecredito,activo,llave) values ('" & tbsubnombre.Text.Trim.ToUpper & "','" & Socio & "','" & tbsublimite.Text.Trim & "','" & IIf(chksubactivo.Checked, 1, 0) & "', newid())"
        bdBase.bdExecute(conexion, qry)
                MsgBox("Subcliente guardado con exito", MsgBoxStyle.Information)
        tbsubnombre.Text = ""
        tbsublimite.Text = ""
                chksubactivo.Checked = False
                LlenaSubSocios(Socio)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub tbsublimite_TextChanged(sender As System.Object, e As System.EventArgs) Handles tbsublimite.LostFocus

        If Val(tbsublimite.Text) > Val(tblimite.Text) Then
            MsgBox("Limite mayor al del Cliente", MsgBoxStyle.Critical)
            tbsublimite.Text = ""
            tbsublimite.Focus()
        End If

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim form2 As Form

        form2 = New CapturaFoto

        form2.Show()

    End Sub
End Class