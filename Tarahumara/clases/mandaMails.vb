﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Net
Imports System.Data.SqlClient
Imports System.IO

Public Class mandaMails
#Region "Propiedades"
    Private conexion As String = String.Empty
    Private para As String = String.Empty
    Private asunto As String = String.Empty
    Private cuerpo As String = String.Empty
    Private adjunto1 As String = String.Empty
    Private adjunto2 As String = String.Empty
    Private esHtml As Boolean = False
    Private regresa As String = String.Empty

    Public Property pConexion As String
        Get
            Return conexion
        End Get
        Set(value As String)
            conexion = value
        End Set
    End Property

    Public Property pPara As String
        Get
            Return para
        End Get
        Set(value As String)
            para = value
        End Set
    End Property

    Public Property pAsunto As String
        Get
            Return asunto
        End Get
        Set(value As String)
            asunto = value
        End Set
    End Property

    Public Property pCuerpo As String
        Get
            Return cuerpo
        End Get
        Set(value As String)
            cuerpo = value
        End Set
    End Property

    Public Property pAdjunto1 As String
        Get
            Return adjunto1
        End Get
        Set(value As String)
            adjunto1 = value
        End Set
    End Property

    Public Property pAdjunto2 As String
        Get
            Return adjunto2
        End Get
        Set(value As String)
            adjunto2 = value
        End Set
    End Property

    Public Property pEsHtml As Boolean
        Get
            Return esHtml
        End Get
        Set(value As Boolean)
            esHtml = value
        End Set
    End Property

    Public Property pRegresa As String
        Get
            Return regresa
        End Get
        Set(value As String)
            regresa = value
        End Set
    End Property
#End Region

    Public Sub MandaMail()
        Dim des As New cTripleDES
        ' "soportesaz@gmail.com|soportesaz|chihuahua@|smtp.gmail.com|587|True"
        Dim cadenaDatos As String = des.Decrypt(My.Settings.datosMail)
        Try
            Dim dr As SqlDataReader = bdBase.bdDataReader(des.Decrypt(conexion), "SELECT mailCuenta, mailUsuario, mailClave, mailHost, mailPuerto, mailSsl " & _
                                                          "FROM Configuracion")
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    If Not IsDBNull(dr("mailCuenta")) Then
                        cadenaDatos = dr("mailCuenta").ToString.Trim & "|"
                        cadenaDatos += dr("mailUsuario").ToString.Trim & "|"
                        cadenaDatos += des.Decrypt(dr("mailClave").ToString.Trim) & "|"
                        cadenaDatos += dr("mailHost").ToString.Trim & "|"
                        cadenaDatos += dr("mailPuerto").ToString.Trim & "|"
                        cadenaDatos += dr("mailSsl").ToString.Trim
                    End If
                End If
                dr.Close()
            End If
        Catch ex As Exception
        End Try

        Dim datosMail() As String = cadenaDatos.Split("|")
        ' Enviar reporte por mail
        Dim oMail As New Mail.MailMessage
        oMail.From = New Mail.MailAddress(datosMail(0))
        oMail.To.Add(New Mail.MailAddress(para))
        'oMail.To.Add(New Mail.MailAddress("info@tufacturador.com"))
        oMail.Subject = asunto
        oMail.Body = cuerpo
        If Not String.IsNullOrEmpty(adjunto1) Then
            If File.Exists(adjunto1) Then oMail.Attachments.Add(New Mail.Attachment(adjunto1))
        End If
        If Not String.IsNullOrEmpty(adjunto2) Then
            If File.Exists(adjunto2) Then oMail.Attachments.Add(New Mail.Attachment(adjunto2))
        End If
        oMail.IsBodyHtml = esHtml
        oMail.Priority = MailPriority.Normal
        Dim smtp As New Mail.SmtpClient
        smtp.Host = datosMail(3)
        smtp.Port = datosMail(4)
        smtp.EnableSsl = Convert.ToBoolean(datosMail(5))
        smtp.UseDefaultCredentials = False
        smtp.DeliveryMethod = SmtpDeliveryMethod.Network
        smtp.Credentials = New NetworkCredential(datosMail(1), datosMail(2))
        smtp.Timeout = 20000
        Try
            smtp.Send(oMail)
            regresa = "Su Documento Electrónico ha sido enviado"
        Catch ex As Exception
            regresa = "Error: " & ex.Message
        End Try
    End Sub
End Class
