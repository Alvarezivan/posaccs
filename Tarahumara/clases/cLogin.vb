﻿Imports System.Data.SqlClient
'Imports Microsoft.Win32

Public Class cLogin
    Private empId As Integer = -9
    Private empNombre As String = String.Empty
    Private empFactNo As Integer = -9
    Private empConexion As String = String.Empty
    Private empConexionL As String = String.Empty
    'Private empConexionActiva As String = String.Empty
    Private tienePrivacidad As Boolean = False
    '
    Private usrId As Integer = -9
    Private usrNombre As String = String.Empty
    Private usrNombreC As String = String.Empty
    Private usrTipo As Integer = -9
    Private usrTienda As Integer = -9
    Private usrCaja As Integer = -9
    Private usrCambiarClave As Boolean = False
    Private loginStatus As Boolean = False
    Private usrDemo As Boolean = True
    Private superUser As Boolean = False
    Private passExpira As DateTime

    Private mensaje As String = String.Empty
    Private tmpClave As String = String.Empty

#Region "Propiedades de Empresa"
    Public ReadOnly Property pEmpId() As Integer
        Get
            Return empId
        End Get
    End Property

    Public ReadOnly Property pEmpNombre() As String
        Get
            Return empNombre
        End Get
    End Property

    Public ReadOnly Property pEmpFactNo() As Integer
        Get
            Return empFactNo
        End Get
    End Property

    Public ReadOnly Property pEmpConexion As String
        Get
            Return empConexion
        End Get
    End Property

    Public ReadOnly Property pEmpConexionL As String
        Get
            Return empConexionL
        End Get
    End Property

    'Public ReadOnly Property pEmpConexionActiva As String
    '    Get
    '        Return empConexionActiva
    '    End Get
    'End Property

    Public ReadOnly Property pTienePrivacidad() As Boolean
        Get
            Return tienePrivacidad
        End Get
    End Property
#End Region

#Region "Propiedades de empleado"
    Public ReadOnly Property pUserId() As Integer
        Get
            Return usrId
        End Get
    End Property

    Public ReadOnly Property pUsrNombre() As String
        Get
            Return usrNombre
        End Get
    End Property

    Public ReadOnly Property pUsrNombreC() As String
        Get
            Return usrNombreC
        End Get
    End Property

    Public ReadOnly Property pUsrTipo() As Integer
        Get
            Return usrTipo
        End Get
    End Property

    Public ReadOnly Property pUsrTienda() As Integer
        Get
            Return usrTienda
        End Get
    End Property

    Public ReadOnly Property pUsrCaja() As Integer
        Get
            Return usrCaja
        End Get
    End Property

    Public ReadOnly Property pUsrCambiarClave As Boolean
        Get
            Return usrCambiarClave
        End Get
    End Property

    Public ReadOnly Property pLoginStatus() As Boolean
        Get
            Return loginStatus
        End Get
    End Property

    Public ReadOnly Property pUsrDemo() As Boolean
        Get
            Return usrDemo
        End Get
    End Property

    Public ReadOnly Property pUsrSuper() As Boolean
        Get
            Return superUser
        End Get
    End Property

    Public ReadOnly Property pPassExpira As String
        Get
            Return passExpira
        End Get
    End Property
#End Region

    Public ReadOnly Property pMensaje As String
        Get
            Return mensaje
        End Get
    End Property

    Public Function doLoginUser(usuario As String, clave As String, Optional tEmp As Integer = -1) As Integer
        If String.IsNullOrEmpty(usuario) Or String.IsNullOrEmpty(clave) Then
            Return -1
        End If
        ' Bitacora.SaveTextToFile("Inicia doLoginUser", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim oActiva As New datosActivacion
        Dim regresa As Integer = -1
        Dim des As New cTripleDES
        Dim sQuery As String = String.Empty
        If usuario.Trim.ToLower = "soporte@elsaz.com" Then
            'Dim mKey As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO", True)
            'If Not My.Settings.modoOffline Then
            sQuery = "SELECT admin FROM configuracion where admin = '" & clave & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), sQuery)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    usrNombre = usuario
                    usrNombreC = "SAZ"
                    superUser = True
                    regresa = tEmp
                    empId = tEmp
                    oActiva.AgregaActivacion("", "", "", "", "", "", clave, empId)
                    'mKey.SetValue("Data7", des.Encrypt(clave))
                    'mKey.SetValue("Data8", empId)
                Else
                    loginStatus = False
                    mensaje = "Usuario/Clave incorrectos"
                End If
                dr.Close()
            Else
                loginStatus = False
                mensaje = "Error en conexion. Reintente más tarde."
            End If
            'Else
            '    If des.Encrypt(clave) = oActiva.pEClave Then
            '        usrNombre = usuario
            '        usrNombreC = "SAZ"
            '        superUser = True
            '        regresa = tEmp
            '        empId = tEmp
            '    Else
            '        loginStatus = False
            '        mensaje = "Usuario/Clave incorrectos"
            '    End If
            '    'If clave = des.Decrypt(mKey.GetValue("Data7")) Then
            '    '    usrNombre = usuario
            '    '    usrNombreC = "SAZ"
            '    '    superUser = True
            '    '    regresa = tEmp
            '    '    empId = tEmp
            '    'Else
            '    '    loginStatus = False
            '    '    mensaje = "Usuario/Clave incorrectos"
            '    'End If
            'End If
        Else
            ' Usuario normal
            'Dim mKey As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios", True)
            'If IsNothing(mKey) Then
            '    My.Computer.Registry.CurrentUser.CreateSubKey("Software\\Saz\\AIO\\Usuarios")
            'End If
            'If Not My.Settings.modoOffline Then
            sQuery = "SELECT id, Empresa, Activo, Suspendido, Accesos, Expira FROM Rlogin WHERE Usuario = '" & usuario & "' AND Clave = '" & des.Encrypt(clave) & "'"
            Dim dr As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), sQuery)
            If Not dr Is Nothing Then
                If dr.HasRows Then
                    dr.Read()
                    ' Buscar dato de empleado en el registro
                    ''Dim terminado As Boolean = False
                    'Dim hallado As Boolean = False

                    'Dim rkU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios", True)
                    'Dim Usuarios() As String = rkU.GetSubKeyNames
                    'Dim rkUser As RegistryKey
                    'For iCual As Integer = 0 To Usuarios.Length - 1
                    '    Dim rkUU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\" & Usuarios(iCual), True)
                    '    If Not String.IsNullOrEmpty(rkUU.GetValue("User")) Then
                    '        Dim sTmp As String = des.Decrypt(rkUU.GetValue("User"))
                    '        If sTmp.Trim.ToLower = usuario.Trim.ToLower Then
                    '            rkUser = rkUU
                    '            hallado = True
                    '            Exit For
                    '        End If
                    '    End If
                    'Next
                    'If Not hallado Then rkUser = My.Computer.Registry.CurrentUser.CreateSubKey("Software\\Saz\\AIO\\Usuarios\\Usuario" & Usuarios.Length + 1)

                    oActiva.RecuperaDatosEmpleado(usuario.Trim)

                    ' Checar, ectivo, suspendido, accesos, expira
                    If dr("Activo") Then
                        If Not dr("Suspendido") Then
                            If Not IsDBNull(dr("Expira")) Then
                                passExpira = dr("Expira")
                                Dim iDiff As Integer = DateDiff(DateInterval.Day, Now, passExpira)
                                Select Case iDiff
                                    Case Is > 0
                                    Case Else
                                        Dim fCambio As New cambiaPass
                                        fCambio.pAnterior = clave
                                        fCambio.pIdUsuario = dr("id")
                                        fCambio.pNomUsuario = usuario
                                        fCambio.ShowDialog()
                                        If fCambio.pCambiado Then
                                            usrCambiarClave = True
                                            clave = fCambio.pNuevaClave
                                            tmpClave = fCambio.pNuevaClave
                                            passExpira = fCambio.pFechaExp
                                        Else
                                            loginStatus = False
                                            mensaje = "Clave expirada"
                                            Return -999
                                            Exit Function
                                        End If
                                        fCambio = Nothing
                                End Select
                            Else
                                usrCambiarClave = True
                                loginStatus = False
                                mensaje = "Clave expirada"
                                sQuery = "UPDATE Rlogin SET Expira = @Expira WHERE Usuario = '" & usuario & "' AND Clave = '" & des.Encrypt(clave) & "'"
                                Dim cmd As New SqlCommand
                                cmd.CommandText = sQuery
                                cmd.Parameters.Add(New SqlParameter("Expira", SqlDbType.DateTime)).Value = Now.AddDays(-1)
                                Dim regresaU As String = bdBase.bdExecute(des.Decrypt(My.Settings.conLogins), cmd)
                                Return -999
                                Exit Function
                            End If

                            usrNombre = usuario
                            regresa = dr("Empresa")
                            empId = regresa
                            superUser = False
                            'Dim mRKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO", True)
                            'mRKey.SetValue("Data8", empId)
                            'rkUser.SetValue("User", des.Encrypt(usuario))
                            'rkUser.SetValue("Pwd", des.Encrypt(clave))
                            'rkUser.SetValue("Data1", des.Encrypt("Activo"))
                            'rkUser.SetValue("Data5", des.Encrypt(passExpira))
                            oActiva.AgregaUsuario("Activo", "", "", "", passExpira.ToString, usuario, clave, oActiva.pEId)
                        Else
                            'rkUser.SetValue("Data1", des.Encrypt("Suspendido"))
                            oActiva.AgregaUsuario("Suspendido", "", "", "", "", "", "", oActiva.pEId)
                            loginStatus = False
                            mensaje = "Cuenta suspendida"
                        End If
                    Else
                        'rkUser.SetValue("Data1", des.Encrypt("Inactivo"))
                        oActiva.AgregaUsuario("Inactivo", "", "", "", "", "", "", oActiva.pEId)
                        loginStatus = False
                        mensaje = "Usuario inactivo"
                    End If
                Else
                    loginStatus = False
                    mensaje = "Usuario/Clave incorrectos"
                End If
                dr.Close()
            Else
                loginStatus = False
                mensaje = "Error en conexion. Reintente más tarde."
            End If
            'Else
            ' Modo offline
            'Dim terminado As Boolean = False
            'Dim hallado As Boolean = False
            'Dim i As Integer = 1
            'Do While Not terminado
            '    Dim rk As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\Usuario" & i)
            '    If Not IsNothing(rk) Then
            '        Dim sTmp As String = des.Decrypt(rk.GetValue("User"))
            '        If sTmp.Trim.ToLower = usuario.Trim.ToLower Then
            '            usrNombre = des.Decrypt(rk.GetValue("User"))
            '            superUser = False
            '            Dim mRKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO")
            '            empId = mRKey.GetValue("Data8")
            '            regresa = empId
            '            terminado = True
            '            hallado = True
            '        End If
            '    Else
            '        terminado = True
            '    End If
            '    i += 1
            'Loop
            'If Not hallado Then
            '    loginStatus = False
            '    mensaje = "Usuario/Clave incorrecto"
            'End If

            oActiva.RecuperaDatosEmpleado(usuario.Trim)
            usrNombre = oActiva.pENombreUsr
            superUser = False
            empId = oActiva.pANoEmpresa
            regresa = empId
            'End If
        End If
        ' Bitacora.SaveTextToFile("Termina doLoginUser", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Return regresa
    End Function

    Public Sub doLoginEmp(empresa As Integer)
        ' Bitacora.SaveTextToFile("Inicia doLoginEmp", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim oActiva As New datosActivacion
        'If Not My.Settings.modoOffline Then
        Dim des As New cTripleDES
        Dim sQuery As String = "SELECT Empresa, Server, UsuarioSvr, PassSvr, BasedeDatos, " & _
                 "svrLocal, usrLocal, pwdLocal, bddLocal, Facturador FROM logins WHERE idEmpresa = " & _
                 empresa & " AND Status = 1"
        Dim dr2 As SqlDataReader = bdBase.bdDataReader(des.Decrypt(My.Settings.conLogins), sQuery)
        If Not dr2 Is Nothing Then
            If dr2.HasRows Then
                dr2.Read()
                ' Datos de empresa
                empId = empresa
                If Not IsDBNull(dr2("Empresa")) Then empNombre = dr2("Empresa").ToString.Trim
                If Not IsDBNull(dr2("Server")) And Not IsDBNull(dr2("UsuarioSvr")) And Not IsDBNull(dr2("PassSvr")) And Not IsDBNull(dr2("BasedeDatos")) Then
                    empConexion = "Data Source=" & dr2("Server").ToString.Trim & ";Initial Catalog=" & dr2("BasedeDatos").ToString.Trim & _
                                  ";Persist Security Info=True;User ID=" & dr2("UsuarioSvr").ToString.Trim & _
                                  ";password=" & dr2("PassSvr").ToString.Trim
                    My.Settings.conexion = empConexion
                    My.Settings.Save()
                End If
                If Not IsDBNull(dr2("svrLocal")) And Not IsDBNull(dr2("usrLocal")) And Not IsDBNull(dr2("pwdLocal")) And Not IsDBNull(dr2("bddLocal")) Then
                    empConexionL = "Data Source=" & dr2("svrLocal").ToString.Trim & ";Initial Catalog=" & dr2("bddLocal").ToString.Trim & _
                                   ";Persist Security Info=True;User ID=" & dr2("usrLocal").ToString.Trim & ";password=" & _
                                   dr2("pwdLocal").ToString.Trim
                    My.Settings.conexionLocal = pEmpConexionL
                    My.Settings.Save()
                End If
                If Not IsDBNull(dr2("Facturador")) Then empFactNo = dr2("Facturador")
                'If Not My.Settings.modoOffline Then
                conexion = empConexion
                'Else
                '    conexion = empConexionL
                'End If
                ' complemento empleado
                If usrCambiarClave Then
                    sQuery = "UPDATE empleado SET [PASSWORD] = '" & tmpClave & "' WHERE user = '" & oLogin.pUsrNombre & "'"
                    Dim regresa As String = bdBase.bdExecute(conexion, sQuery)
                End If
                If usrNombre <> "soporte@elsaz.com" Then
                    sQuery = "SELECT id, Nombre, Tipo, Tienda, eMail FROM empleado WHERE UPPER([user]) = '" & usrNombre.ToUpper & "'"
                    Dim dr As SqlDataReader = bdBase.bdDataReader(conexion, sQuery)
                    If Not IsNothing(dr) Then
                        If dr.HasRows Then
                            dr.Read()
                            usrId = dr("id")
                            ' usrNombre = 
                            usrNombreC = dr("Nombre").ToString.Trim
                            usrTipo = dr("Tipo")
                            usrTienda = dr("Tienda")

                            'Dim terminado As Boolean = False
                            'Dim i As Integer = 0
                            'Do While Not terminado
                            '    i += 1
                            '    Dim rk As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\Usuario" & i, True)
                            '    If Not IsNothing(rk) Then
                            '        Dim sTmp As String = des.Decrypt(rk.GetValue("User"))
                            '        If sTmp.Trim.ToLower = usrNombre.Trim.ToLower Then
                            '            rk.SetValue("Data2", usrNombreC)
                            '            rk.SetValue("Data3", usrTipo)
                            '            rk.SetValue("Data4", usrTienda)
                            '            Exit Do
                            '        End If
                            '    Else
                            '        terminado = True
                            '    End If
                            'Loop

                            oActiva.RecuperaDatosEmpleado(usrNombre)
                            oActiva.AgregaUsuario("", usrNombreC, usrTipo, usrTienda, "", "", "", oActiva.pEId)
                        Else
                            empId = -9
                            loginStatus = False
                        End If
                        dr.Close()
                    End If
                Else
                    usrId = -1
                    usrNombreC = "SAZ"
                    usrTipo = "1"
                    usrTienda = "1"
                End If
                '
                If empId <> -9 Then
                    loginStatus = True
                    mensaje = ""
                Else
                    mensaje = "Usuario o clave incorrectos."
                End If
            Else
                loginStatus = False
                mensaje = "Empresa inactiva. Contacte a ventas@elsaz.com"
            End If
            dr2.Close()
        Else
            loginStatus = False
            mensaje = "Error en conexion. Reintente más tarde."
        End If
        'Else
        '    'Dim terminado As Boolean = False
        '    'Dim i As Integer = 0
        '    'Do While Not terminado
        '    '    i += 1
        '    '    Dim rk As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\Usuario" & i)
        '    '    If Not IsNothing(rk) Then
        '    '        Dim des As New cTripleDES
        '    '        Dim sTmp As String = des.Decrypt(rk.GetValue("User"))
        '    '        If sTmp.Trim.ToLower = usrNombre.Trim.ToLower Then
        '    '            If des.Encrypt(rk.GetValue("Data1")) = "Activo" Then
        '    '                usrNombreC = rk.GetValue("Data2")
        '    '                usrTipo = rk.GetValue("Data3")
        '    '                usrTienda = rk.GetValue("Data4")
        '    '            Else
        '    '                loginStatus = False
        '    '                mensaje = "Usuario inactivo"
        '    '            End If
        '    '            Exit Do
        '    '        End If
        '    '    Else
        '    '        terminado = True
        '    '    End If
        '    'Loop
        '    oActiva.RecuperaDatosEmpleado(usrNombreC)
        '    If Not String.IsNullOrEmpty(oActiva.pEEstado) Then
        '        Dim des As New cTripleDES
        '        If des.Encrypt("Activo") = oActiva.pEEstado Then
        '            usrNombreC = oActiva.pENombreUsr
        '            usrTipo = oActiva.pETipo
        '            usrTienda = oActiva.pETienda
        '        Else
        '            loginStatus = False
        '            mensaje = "Usuario inactivo"
        '        End If
        '    End If
        'End If
        ' Bitacora.SaveTextToFile("Termina doLoginEmp", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub
End Class
