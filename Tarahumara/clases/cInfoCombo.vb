Public Class cInfoCombo
    Private myId As Integer
    Private myText As String

    ' constructor
    Sub New(ByVal ID As Integer, ByVal tx As String)
        myID = ID
        myText = tx
    End Sub

    Public Property Text() As String
        Get
            Text = myText
        End Get
        Set(ByVal tx As String)
            myText = tx
        End Set
    End Property

    Public Property ID() As Integer
        Get
            ID = myId
        End Get
        Set(ByVal id As Integer)
            myId = id
        End Set
    End Property

    Public Overrides Function ToString() As String
        ToString = myText
    End Function
End Class
