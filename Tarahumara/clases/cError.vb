﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Net
Imports System.Data.SqlClient
Imports System.IO


Public Class cError
    Public Shared Sub ReportaError(ByVal tError As String, ByVal Empresa As Integer, ByVal Usuario As String, Optional ByVal tQuery As String = "", Optional cmd As SqlCommand = Nothing, Optional archivo As String = "", Optional perdidos As Boolean = False)
        Dim errores As Boolean = False
        Try
            'If Not My.Settings.modoOffline Then
            Dim des As New cTripleDES
            Dim enviaCorreo As New mandaMails
            enviaCorreo.pConexion = des.Decrypt(My.Settings.conLogins)
            enviaCorreo.pPara = "licencias@allinone.mx"
            enviaCorreo.pAsunto = "Error en AllInOne " & My.Application.Info.Version.ToString
            Dim cadena As String = tError & vbCrLf & vbCrLf & Empresa & vbCrLf & Usuario & vbCrLf & vbCrLf & tQuery
            If Not cmd Is Nothing Then
                cadena = cadena & vbCrLf & vbCrLf & cmd.CommandText & vbCrLf & vbCrLf
                For Each parametro As SqlParameter In cmd.Parameters
                    cadena = cadena & parametro.ParameterName & " :: " & parametro.Value & vbCrLf
                Next
            End If
            enviaCorreo.pCuerpo = cadena
            If Not String.IsNullOrEmpty(archivo) Then
                ' Guardar xml a archivo para envio
                Dim sFecha As String = String.Format("{0:yyyyMMdd}", System.DateTime.Now)
                Dim sArchivo As String = Path.Combine(Path.GetTempPath(), Empresa & "_" & sFecha & ".txt")
                If File.Exists(sArchivo) Then
                    Try
                        File.Delete(sArchivo)

                        Dim oWrite As New StreamWriter(sArchivo)
                        oWrite.Write(archivo)
                        oWrite.Close()
                    Catch ex As Exception

                    End Try
                Else
                    Dim oWrite As New StreamWriter(sArchivo)
                    oWrite.Write(archivo)
                    oWrite.Close()
                End If
                enviaCorreo.pAdjunto1 = sArchivo
            End If
            If perdidos Then
                If File.Exists(My.Settings.RutaPerdidos & "perdidos.txt") Then enviaCorreo.pAdjunto2 = My.Settings.RutaPerdidos & "perdidos.txt"
            End If
            enviaCorreo.pEsHtml = False
            enviaCorreo.MandaMail()
            If enviaCorreo.pRegresa.Contains("Error:") Then
                DevExpress.XtraEditors.XtraMessageBox.Show("Error al enviar mensaje: " & enviaCorreo.pRegresa, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                errores = True
            End If
            Try
                If Not errores And perdidos Then
                    If File.Exists(My.Settings.RutaPerdidos & "perdidos.txt") Then File.Delete(My.Settings.RutaPerdidos & "perdidos.txt")
                End If
            Catch ex As Exception
            End Try
            'Else
            'MsgBox("No puedo hacer el reporte del error. Por favor capture esta pantalla y mándela a soporte: " & tError)
            'End If
        Catch ex As Exception
        End Try
    End Sub
End Class
