﻿Imports System.IO
Imports Microsoft.Win32

Public Class datosActivacion
#Region "Propiedades"
    Dim dsActivacion As New DataSet
    Dim dsUsuarios As New DataSet

    Dim aCpu As String
    Dim aBios As String
    Dim aMotherboard As String
    Dim aFingerprint As String
    Dim aComplemento As String
    Dim aIdRegistro As String
    Dim aClaveSaz As String
    Dim aNoEmpresa As String

    Dim eId As Integer = -1
    Dim eEstado As String
    Dim eNombreUsr As String
    Dim eTipo As String
    Dim eTienda As String
    Dim eExpira As String
    Dim eUsuario As String
    Dim eClave As String

    Public ReadOnly Property pACpu As String
        Get
            Return aCpu
        End Get
    End Property

    Public ReadOnly Property pABios As String
        Get
            Return aBios
        End Get
    End Property

    Public ReadOnly Property pAMotherboard As String
        Get
            Return aMotherboard
        End Get
    End Property

    Public ReadOnly Property pAFingerPrint As String
        Get
            Return aFingerprint
        End Get
    End Property

    Public ReadOnly Property pAComplemento As String
        Get
            Return aComplemento
        End Get
    End Property

    Public ReadOnly Property pAIdRegistro As String
        Get
            Return aIdRegistro
        End Get
    End Property

    Public ReadOnly Property pAClaveSaz As String
        Get
            Return aClaveSaz
        End Get
    End Property

    Public ReadOnly Property pANoEmpresa As String
        Get
            Return aNoEmpresa
        End Get
    End Property

    Public ReadOnly Property pEId As Integer
        Get
            Return eId
        End Get
    End Property

    Public ReadOnly Property pEEstado As String
        Get
            Return eEstado
        End Get
    End Property

    Public ReadOnly Property pENombreUsr As String
        Get
            Return eNombreUsr
        End Get
    End Property

    Public ReadOnly Property pETipo As String
        Get
            Return eTipo
        End Get
    End Property

    Public ReadOnly Property pETienda As String
        Get
            Return eTienda
        End Get
    End Property

    Public ReadOnly Property pEExpira As String
        Get
            Return eExpira
        End Get
    End Property

    Public ReadOnly Property pEUsuario As String
        Get
            Return eUsuario
        End Get
    End Property

    Public ReadOnly Property pEClave As String
        Get
            Return eClave
        End Get
    End Property
#End Region

    Public Sub New()
        dsActivacion = New DataSet()
        Dim tabla As DataTable = New DataTable()
        tabla.Columns.Add("ID", GetType(Integer))
            tabla.Columns("ID").AutoIncrement = True
            tabla.Columns("ID").AutoIncrementSeed = 1
        tabla.Columns.Add("Data1", GetType(String))
        tabla.Columns.Add("Data2", GetType(String))
        tabla.Columns.Add("Data3", GetType(String))
        tabla.Columns.Add("Data4", GetType(String))
        tabla.Columns.Add("Data5", GetType(String))
        tabla.Columns.Add("Data6", GetType(String))
        tabla.Columns.Add("Data7", GetType(String))
        tabla.Columns.Add("Data8", GetType(String))
            tabla.PrimaryKey = New DataColumn() {tabla.Columns("ID")}
        dsActivacion.Tables.Add(tabla)

        dsUsuarios = New DataSet()
        Dim tabla2 As DataTable = New DataTable()
        tabla2.Columns.Add("ID", GetType(Integer))
            tabla2.Columns("ID").AutoIncrement = True
            tabla2.Columns("ID").AutoIncrementSeed = 1
        tabla2.Columns.Add("Data1", GetType(String))
        tabla2.Columns.Add("Data2", GetType(String))
        tabla2.Columns.Add("Data3", GetType(String))
        tabla2.Columns.Add("Data4", GetType(String))
        tabla2.Columns.Add("Data5", GetType(String))
        tabla2.Columns.Add("User", GetType(String))
        tabla2.Columns.Add("Pwd", GetType(String))
            tabla2.PrimaryKey = New DataColumn() {tabla2.Columns("ID")}
        dsUsuarios.Tables.Add(tabla2)

        RecuperaInfo()
        LeeDatos()
        If dsActivacion.Tables(0).Rows.Count > 0 Then RecuperaDatosActivacion()
    End Sub

    Public Sub AgregaActivacion(Dato1 As String, Dato2 As String, Dato3 As String, Dato4 As String, Dato5 As String, Dato6 As String, Dato7 As String, Dato8 As String, Optional encripta As Boolean = True)
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.AgregaActivacion", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim des As New cTripleDES
        If encripta Then
            If Not String.IsNullOrEmpty(Dato1) Then Dato1 = des.Encrypt(Dato1)
            If Not String.IsNullOrEmpty(Dato2) Then Dato2 = des.Encrypt(Dato2)
            If Not String.IsNullOrEmpty(Dato3) Then Dato3 = des.Encrypt(Dato3)
            If Not String.IsNullOrEmpty(Dato4) Then Dato4 = des.Encrypt(Dato4)
            If Not String.IsNullOrEmpty(Dato5) Then
                If Dato5 <> "00" Then Dato5 = des.Encrypt(Dato5)
            End If
            If Not String.IsNullOrEmpty(Dato6) Then Dato6 = des.Encrypt(Dato6)
            If Not String.IsNullOrEmpty(Dato7) Then Dato7 = des.Encrypt(Dato7)
        End If
        Dim tabla As DataTable = dsActivacion.Tables(0)
        If tabla.Rows.Count = 0 Then
            Dim row As DataRow = tabla.NewRow()
            If Not String.IsNullOrEmpty(Dato1) Then row("Data1") = Dato1 ' CPU
            If Not String.IsNullOrEmpty(Dato2) Then row("Data2") = Dato2 ' Bios
            If Not String.IsNullOrEmpty(Dato3) Then row("Data3") = Dato3 ' Motherboard
            If Not String.IsNullOrEmpty(Dato4) Then row("Data4") = Dato4 ' Fingerprint
            If Not String.IsNullOrEmpty(Dato5) Then row("Data5") = Dato5 ' Complemento / 00
            If Not String.IsNullOrEmpty(Dato6) Then row("Data6") = Dato6 ' id registro Rlic / "Desactivado"
            If Not String.IsNullOrEmpty(Dato7) Then row("Data7") = Dato7 ' clave saz
            row("Data8") = Dato8    ' No empresa
            tabla.Rows.Add(row)
        Else
            Dim row As DataRow = tabla.Rows(0)
            If Not String.IsNullOrEmpty(Dato1) Then row("Data1") = Dato1 ' CPU
            If Not String.IsNullOrEmpty(Dato2) Then row("Data2") = Dato2 ' Bios
            If Not String.IsNullOrEmpty(Dato3) Then row("Data3") = Dato3 ' Motherboard
            If Not String.IsNullOrEmpty(Dato4) Then row("Data4") = Dato4 ' Fingerprint
            If Not String.IsNullOrEmpty(Dato5) Then row("Data5") = Dato5 ' Complemento / 00
            If Not String.IsNullOrEmpty(Dato6) Then row("Data6") = Dato6 ' id registro Rlic / "Desactivado"
            If Not String.IsNullOrEmpty(Dato7) Then row("Data7") = Dato7 ' clave saz
            row("Data8") = Dato8    ' No empresa
            tabla.AcceptChanges()
        End If
        GuardaDatos("A")
        ' Bitacora.SaveTextToFile("Termina datosActivacion.AgregaActivacion", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Public Sub AgregaUsuario(Dato1 As String, Dato2 As String, Dato3 As String, Dato4 As String, Dato5 As String, Dato6 As String, Dato7 As String, Optional id As Integer = -1, Optional encripta As Boolean = True)
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.AgregaUsuario", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim des As New cTripleDES
        If encripta Then
            If Not String.IsNullOrEmpty(Dato1) Then Dato1 = des.Encrypt(Dato1)
            If Not String.IsNullOrEmpty(Dato5) Then Dato5 = des.Encrypt(Dato5)
            If Not String.IsNullOrEmpty(Dato6) Then Dato6 = des.Encrypt(Dato6)
            If Not String.IsNullOrEmpty(Dato7) Then Dato7 = des.Encrypt(Dato7)
        End If
        Dim tabla As DataTable = dsUsuarios.Tables(0)
        If id = -1 Then
            Dim row As DataRow = tabla.NewRow()
            If Not String.IsNullOrEmpty(Dato1) Then row("Data1") = Dato1 ' "Activo" / "Suspendido" / "Inactivo"
            If Not String.IsNullOrEmpty(Dato2) Then row("Data2") = Dato2 ' Nombre completo usuario
            If Not String.IsNullOrEmpty(Dato3) Then row("Data3") = Dato3 ' Tipo
            If Not String.IsNullOrEmpty(Dato4) Then row("Data4") = Dato4 ' Tienda
            If Not String.IsNullOrEmpty(Dato5) Then row("Data5") = Dato5 ' Expira
            If Not String.IsNullOrEmpty(Dato6) Then row("User") = Dato6 ' Usuario
            If Not String.IsNullOrEmpty(Dato7) Then row("Pwd") = Dato7 ' Clave
            tabla.Rows.Add(row)
        Else
            Dim row As DataRow = tabla.Rows(id)
            If Not String.IsNullOrEmpty(Dato1) Then row("Data1") = Dato1 ' "Activo" / "Suspendido" / "Inactivo"
            If Not String.IsNullOrEmpty(Dato2) Then row("Data2") = Dato2 ' Nombre completo usuario
            If Not String.IsNullOrEmpty(Dato3) Then row("Data3") = Dato3 ' Tipo
            If Not String.IsNullOrEmpty(Dato4) Then row("Data4") = Dato4 ' Tienda
            If Not String.IsNullOrEmpty(Dato5) Then row("Data5") = Dato5 ' Expira
            If Not String.IsNullOrEmpty(Dato6) Then row("User") = Dato6 ' Usuario
            If Not String.IsNullOrEmpty(Dato7) Then row("Pwd") = Dato7 ' Clave
            tabla.AcceptChanges()
        End If
        GuardaDatos("U")
        ' Bitacora.SaveTextToFile("Termina datosActivacion.AgregaUsuario", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Private Function BuscaUsuario(nombre As String) As Integer
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.BuscaUsuario", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim regresa As Integer = -1
        Dim des As New cTripleDES
        nombre = des.Encrypt(nombre.Trim)
        Dim conta As Integer = 0
        For Each renglon As DataRow In dsUsuarios.Tables(0).Rows
            If renglon("User").ToString.ToLower = nombre.Trim.ToLower Then
                regresa = conta   ' renglon("ID")
                Exit For
            End If
            conta += 1
        Next
        ' Bitacora.SaveTextToFile("Termina datosActivacion.BuscaUsuario", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Return regresa
    End Function

    Public Sub RecuperaDatosActivacion()
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.RecuperaDatosActivacion", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim renglon As DataRow = dsActivacion.Tables(0).Rows(0)
        If Not IsDBNull(renglon("Data1")) Then aCpu = renglon("Data1") Else aCpu = ""
        If Not IsDBNull(renglon("Data2")) Then aBios = renglon("Data2") Else aBios = ""
        If Not IsDBNull(renglon("Data3")) Then aMotherboard = renglon("Data3") Else aMotherboard = ""
        If Not IsDBNull(renglon("Data4")) Then aFingerprint = renglon("Data4") Else aFingerprint = ""
        If Not IsDBNull(renglon("Data5")) Then aComplemento = renglon("Data5") Else aComplemento = ""
        If Not IsDBNull(renglon("Data6")) Then aIdRegistro = renglon("Data6") Else aIdRegistro = ""
        If Not IsDBNull(renglon("Data7")) Then aClaveSaz = renglon("Data7") Else aClaveSaz = ""
        If Not IsDBNull(renglon("Data8")) Then aNoEmpresa = renglon("Data8") Else aNoEmpresa = ""
        ' Bitacora.SaveTextToFile("Termina datosActivacion.RecuperaDatosActivacion", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Public Sub RecuperaDatosEmpleado(nombre As String)
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.RecuperaDatosEmpleado", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Dim idEmp As Integer = BuscaUsuario(nombre)
        If idEmp <> -1 Then
            Dim renglon As DataRow = dsUsuarios.Tables(0).Rows(idEmp)
            eId = idEmp
            If Not IsDBNull(renglon("Data1")) Then eEstado = renglon("Data1") Else eEstado = ""
            If Not IsDBNull(renglon("Data2")) Then eNombreUsr = renglon("Data2") Else eNombreUsr = ""
            If Not IsDBNull(renglon("Data3")) Then eTipo = renglon("Data3") Else eTipo = ""
            If Not IsDBNull(renglon("Data4")) Then eTienda = renglon("Data4") Else eTienda = ""
            If Not IsDBNull(renglon("Data5")) Then eExpira = renglon("Data5") Else eExpira = ""
            If Not IsDBNull(renglon("User")) Then eUsuario = renglon("User") Else eUsuario = ""
            If Not IsDBNull(renglon("Pwd")) Then eClave = renglon("Pwd") Else eClave = ""
        Else
            eId = -1
            eEstado = ""
            eNombreUsr = ""
            eTipo = ""
            eTienda = ""
            eExpira = ""
            eUsuario = ""
            eClave = ""
        End If
        ' Bitacora.SaveTextToFile("Termina datosActivacion.RecuperaDatosEmpleado", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Public Sub GuardaDatos(tipo As String)
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.GuardaDatos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        'If tipo = "A" Then
        '    If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data001.xml") Then File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data001.xml")
        '    dsActivacion.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data001.xml")
        'End If
        'If tipo = "U" Then
        '    If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml") Then File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml")
        '    dsUsuarios.WriteXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml")
        'End If
        ' Bitacora.SaveTextToFile("Termina datosActivacion.GuardaDatos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        ' Bitacora.SaveTextToFile("Ruta: " & Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Private Sub LeeDatos()
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.LeeDatos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data001.xml") Then
            dsActivacion.Tables(0).PrimaryKey = Nothing
            dsActivacion.ReadXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data001.xml")
        End If
        If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml") Then
            dsUsuarios.Tables(0).PrimaryKey = Nothing
            dsUsuarios.ReadXml(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\data002.xml")
        End If
        ' Bitacora.SaveTextToFile("Termina datosActivacion.LeeDatos", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub

    Private Sub RecuperaInfo()
        ' Bitacora.SaveTextToFile("Inicia datosActivacion.RecuperaInfo", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
        Try
            Dim guarda As Boolean = False
            Dim guarda2 As Boolean = False
            ' Datos Activacion
            Dim mKey As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO", True)
            If Not IsNothing(mKey) Then
                AgregaActivacion(mKey.GetValue("Data1"), mKey.GetValue("Data2"), mKey.GetValue("Data3"), mKey.GetValue("Data4"), _
                                 mKey.GetValue("Data5"), mKey.GetValue("Data6"), mKey.GetValue("Data7"), mKey.GetValue("Data8"), False)
                guarda = True
            End If
            ' Datos Usuarios
            Dim rkU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios", True)
            If Not IsNothing(rkU) Then
                Dim Usuarios() As String = rkU.GetSubKeyNames
                For iCual As Integer = 0 To Usuarios.Length - 1
                    Dim rkUU As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz\\AIO\\Usuarios\\" & Usuarios(iCual), True)
                    AgregaUsuario(rkUU.GetValue("Data1"), rkUU.GetValue("Data2"), rkUU.GetValue("Data3"), rkUU.GetValue("Data4"), _
                                  rkUU.GetValue("Data5"), rkUU.GetValue("User"), rkUU.GetValue("Pwd"), -1, False)
                    guarda2 = True
                Next
            End If
            If guarda And guarda2 Then
                GuardaDatos("A")
                GuardaDatos("U")
                Dim mKeyD As RegistryKey = Registry.CurrentUser.OpenSubKey("Software\\Saz", True)
                If Not IsNothing(mKeyD) Then
                    mKeyD.DeleteSubKeyTree("AIO", False)
                End If
            End If
        Catch ex As Exception

        End Try
        ' Bitacora.SaveTextToFile("Termina datosActivacion.RecuperaInfo", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\AIOlog.txt")
    End Sub
End Class
