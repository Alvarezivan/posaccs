﻿Imports System.IO
Imports Ionic.Zip

Module scriptsSql
    Public scripts As New List(Of String)

    Public Sub InicializaCreate(tabla As String)
        scripts.Clear()
        'AddScript("IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE xtype='u' AND name='colors') BEGIN " & _
        '          "CREATE TABLE [colors]([id] [numeric](18, 0) IDENTITY(1,1) NOT NULL, " & _
        '          "[numero] [varchar] (50) NULL, [Color] [varchar](50) NULL " & _
        '          ") ON [PRIMARY] END", tabla)
    End Sub

    Public Sub InicializaAlterTable(tabla As String)
        scripts.Clear()
        'AddScript("IF COL_LENGTH('colors', 'miColumna') IS NULL" & vbCrLf & "BEGIN" & vbCrLf & _
        '          "ALTER TABLE colors ADD miColumna [varchar](10)" & vbCrLf & "END", tabla)
    End Sub

    Public Sub InicializaAlterColumn(tabla As String)
        scripts.Clear()
        'AddScript("ALTER TABLE colors ALTER COLUMN miColumna VARCHAR(50)", tabla)
    End Sub

    Public Sub InicializaVista(tabla As String)
        Dim regresa As String = String.Empty
        If Not File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\" & tabla & ".zip") Then
            Dim ofd As New OpenFileDialog
            ofd.Title = "Seleccione el archivo de scripts"
            ofd.InitialDirectory = Application.StartupPath
            ofd.ShowDialog()
            File.Copy(ofd.FileName, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\" & tabla & ".zip")
        End If
        Select Case tabla.ToLower
            Case "conscortes3"
                If Not File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\conscortes3.sql") Then
                    If File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\conscortes3.zip") Then
                        Using zip As ZipFile = ZipFile.Read(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\conscortes3.zip")
                            AddHandler zip.ExtractProgress, New EventHandler(Of ExtractProgressEventArgs)(AddressOf zip_ExtractProgress)
                            zip.ExtractAll(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz", Ionic.Zip.ExtractExistingFileAction.OverwriteSilently)
                        End Using
                    End If
                End If
                regresa = bdBase.bdExecuteScriptGo(conexion, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) & "\Saz\conscortes3.sql")
        End Select
    End Sub

    Public Sub InicializaTrigger()
        'AddScript("IF NOT OBJECT_ID('sociost', 'TR') IS NOT NULL" & vbCrLf & "BEGIN" & vbCrLf & _
        '          "EXEC('create trigger sociost on dbo.socios after insert as update socios set numero=id where id=(Select id From Inserted)')" & _
        '          "END", "todas")
    End Sub

    Private Sub AddScript(script As String, tabla As String)
        Dim sTmp As String = script.Replace("  ", "")
        If tabla.Trim.ToLower <> "todas" Then
            If (sTmp.ToUpper.Contains(tabla.ToUpper())) Then
                scripts.Add(script)
            End If
            If sTmp.ToUpper.Contains("TABLE " & tabla) Or sTmp.ToUpper.Contains("TABLE [" & tabla & "]") Or sTmp.ToUpper.Contains("TABLE [DBO].[" & tabla & "]") Then scripts.Add(script)
        Else
            scripts.Add(script)
        End If
    End Sub

    Private Sub zip_ExtractProgress(ByVal sender As Object, ByVal e As ExtractProgressEventArgs)
        If (e.EventType = Ionic.Zip.ZipProgressEventType.Extracting_AfterExtractEntry) Then
            ''
        ElseIf (e.EventType = ZipProgressEventType.Extracting_BeforeExtractAll) Then
            '' do nothing
        End If
    End Sub
End Module
