﻿Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Windows.Forms.Form

Public Class Validation
#Region "Public Shared Methods"

    Public Shared Function FormIsValid(ByRef objForm As Form) As Boolean

        Dim Valid As Boolean = True
        Validate(objForm, objForm.Controls, Valid)

        objForm.Focus()
        If Not objForm.Validate Then Valid = False

        Return Valid

    End Function

    Public Shared Function FormIsValid(ByRef objform As Form, ByRef TopLevelControl As Control) As Boolean

        Dim Valid As Boolean = True
        Validate(objform, TopLevelControl.Controls, Valid)

        objform.Focus()
        If Not objform.Validate Then Valid = False

        Return Valid

    End Function

    Private Shared Sub Validate(ByRef objForm As Form, ByRef objControls As System.Windows.Forms.Control.ControlCollection, ByRef Valid As Boolean)

        For Each objControl As Control In objControls

            If Not TypeOf objControl Is RadioButton Then

                objControl.Focus()
                If Not objForm.Validate() Then Valid = False



            End If

        Next

    End Sub

#End Region
End Class
