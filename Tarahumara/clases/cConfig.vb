Public Class cConfig
    Private Servidor As String
    Private Nombre As String
    Private Base As String
    Private Usuario As String
    Private UsuarioId As Integer
    Private Clave As String
    Private Existe As Boolean
    Private Conectado As Boolean
    Private Version As String
    Private Tienda As String
    Private Nivel As String
    Private Tecnico As String
    Private Empresa As String
    Private NoEmpresa As String
    Private NombreTienda As String = "(Ninguna)"
    ' Emisi�n de vales
    Private Dias As String
    Private Monto As String
    Private Emite As Boolean
    Private uSocio As Boolean

    Public Property pSocio() As String
        Get
            Return uSocio
        End Get
        Set(ByVal value As String)
            uSocio = value
        End Set
    End Property

    Public Property pServidor() As String
        Get
            Return Servidor
        End Get
        Set(ByVal value As String)
            Servidor = value
        End Set
    End Property

    Public Property pNombre() As String
        Get
            Return Nombre
        End Get
        Set(ByVal value As String)
            Nombre = value
        End Set
    End Property

    Public Property pBase() As String
        Get
            Return Base
        End Get
        Set(ByVal value As String)
            Base = value
        End Set
    End Property

    Public Property pUsuario() As String
        Get
            Return Usuario
        End Get
        Set(ByVal value As String)
            Usuario = value
        End Set
    End Property

    Public Property pUsuarioId() As Integer
        Get
            Return UsuarioId
        End Get
        Set(ByVal value As Integer)
            UsuarioId = value
        End Set
    End Property

    Public Property pClave() As String
        Get
            Return Clave
        End Get
        Set(ByVal value As String)
            Clave = value
        End Set
    End Property

    Public Property pExiste() As Boolean
        Get
            Return Existe
        End Get
        Set(ByVal value As Boolean)
            Existe = value
        End Set
    End Property

    Public Property pConectado() As Boolean
        Get
            Return Conectado
        End Get
        Set(ByVal value As Boolean)
            Conectado = value
        End Set
    End Property

    Public Property pVersion() As String
        Get
            Return Version
        End Get
        Set(ByVal value As String)
            Version = value
        End Set
    End Property

    Public Property pTienda() As String
        Get
            Return Tienda
        End Get
        Set(ByVal value As String)
            Tienda = value
        End Set
    End Property

    Public Property pNivel() As String
        Get
            Return Nivel
        End Get
        Set(ByVal value As String)
            Nivel = value
        End Set
    End Property

    Public Property pTecnico() As String
        Get
            Return Tecnico
        End Get
        Set(ByVal value As String)
            Tecnico = value
        End Set
    End Property

    Public Property pEmpresa() As String
        Get
            Return Empresa
        End Get
        Set(ByVal value As String)
            Empresa = value
        End Set
    End Property

    Public Property pNoEmpresa() As String
        Get
            Return NoEmpresa
        End Get
        Set(ByVal value As String)
            NoEmpresa = value
        End Set
    End Property

    Public Property pNombreTienda() As String
        Get
            Return NombreTienda
        End Get
        Set(ByVal value As String)
            NombreTienda = value
        End Set
    End Property

    Public Property pDias() As String
        Get
            Return Dias
        End Get
        Set(ByVal value As String)
            Dias = value
        End Set
    End Property

    Public Property pMonto() As String
        Get
            Return Monto
        End Get
        Set(ByVal value As String)
            Monto = value
        End Set
    End Property

    Public Property pEmite() As Boolean
        Get
            Return Emite
        End Get
        Set(ByVal value As Boolean)
            Emite = value
        End Set
    End Property
End Class
