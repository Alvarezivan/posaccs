﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions

Public Class bdBase
    Public Shared Function bdDataReader(ByVal cadConexion As String, ByVal comando As String, Optional retry As Boolean = True) As SqlDataReader
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(comando, cnX)
        Dim dr As SqlDataReader = Nothing
        Dim drTmp As SqlDataReader = Nothing
        Try
            cnX.Open()
            cmd.CommandTimeout = 0
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As SqlException
            If retry Then
                ProcesaError(ex.Number, ex.Message, cadConexion, NombreTabla(comando))
                drTmp = bdDataReader(cadConexion, comando, False)
            Else
                'Throw ex
                Return Nothing
            End If
        Finally
        End Try
        If retry Then
            If dr Is Nothing Then
                If Not drTmp Is Nothing Then
                    dr = drTmp
                    'drTmp.Close()
                End If
            End If
        End If
        Return dr
    End Function

    Public Shared Function bdDataset(ByVal cadConexion As String, ByVal cadQuery As String, Optional ByVal esSP As Boolean = False, Optional retry As Boolean = True) As DataSet
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(cadQuery, cnX)
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Try
            cnX.Open()
            If esSP Then cmd.CommandType = CommandType.StoredProcedure
            da.SelectCommand.CommandTimeout = 0
            da.Fill(ds)
        Catch ex As SqlException
            If retry Then
                ProcesaError(ex.Number, ex.Message, cadConexion, NombreTabla(cmd.CommandText))
                ds = bdDataset(cadConexion, cadQuery, esSP, False)
            Else
                Throw ex
                Return Nothing
            End If
        Finally
            da.Dispose()
            cmd.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return ds
    End Function

    Public Shared Function bdDataset(ByVal cadConexion As String, ByVal oCommand As SqlCommand, Optional ByVal esSP As Boolean = False, Optional retry As Boolean = True) As DataSet
        Dim cnX As New SqlConnection(cadConexion)
        oCommand.Connection = cnX
        If esSP Then oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandTimeout = 0
        Dim da As New SqlDataAdapter(oCommand)
        Dim ds As New DataSet
        Try
            cnX.Open()
            da.SelectCommand.CommandTimeout = 0
            da.Fill(ds)
        Catch ex As SqlException
            If retry Then
                ProcesaError(ex.Number, ex.Message, cadConexion, NombreTabla(oCommand.CommandText))
                ds = bdDataset(cadConexion, oCommand, esSP, False)
            Else
                Throw ex
                Return Nothing
            End If
        Finally
            da.Dispose()
            oCommand.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return ds
    End Function

    Public Shared Function dbDataTable(ByVal cadConexion As String, ByVal cadQuery As String, Optional ByVal esSP As Boolean = False) As DataTable
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(cadQuery, cnX)
        Dim da As New SqlDataAdapter(cmd)
        Dim table As New DataTable
        Try
            cnX.Open()
            If esSP Then cmd.CommandType = CommandType.StoredProcedure
            da.SelectCommand.CommandTimeout = 0
            da.Fill(table)
        Catch ex As Exception
            Throw ex
            Return Nothing
        Finally
            da.Dispose()
            cmd.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return table
    End Function

    Public Shared Function bdExecute(ByVal cadConexion As String, ByVal cadQuery As String, Optional ByVal esSP As Boolean = False, Optional ByVal regresanumero As Boolean = False, Optional ByVal esfactura As Boolean = False, Optional retry As Boolean = True) As String
        Dim regresa As String = ""
        Dim cnX As New SqlConnection(cadConexion)
        Dim cmd As New SqlCommand(cadQuery, cnX)
        Try
            cnX.Open()
            cmd.CommandTimeout = 0
            If esSP Then cmd.CommandType = CommandType.StoredProcedure
            Dim resultado As Long = 0
            If regresanumero Then
                resultado = cmd.ExecuteScalar
                regresa = resultado
            Else
                cmd.ExecuteNonQuery()
            End If
            'If Not esfactura Then
            '    Dim qry2 As String
            '    qry2 = cadQuery.ToUpper.Trim
            '    If qry2.Contains("UPDATE") Or qry2.Contains("DELETE") Or qry2.Contains("INSERT") Then
            '        Dim cmd2 As New SqlCommand
            '        cmd2.Connection = cnX
            '        cmd2.CommandText = "insert into eventos (query, status) values(@txtqry,'0')"
            '        cmd2.Parameters.Add(New SqlParameter("txtqry", SqlDbType.Text)).Value = qry2
            '        cmd2.ExecuteNonQuery()
            '    End If
            'End If
        Catch ex As SqlException
            'regresa = "ERR. " & ex.Message
            'Throw ex
            If retry Then
                ProcesaError(ex.Number, ex.Message, cadConexion, NombreTabla(cadQuery))
                Dim regresaExtra As String = bdExecute(cadConexion, cadQuery, esSP, regresanumero, False, False)
                regresa = regresaExtra
            Else
                ' cError.ReportaError(ex.Message, oLogin.pEmpId, oLogin.pUsrNombre, cadQuery, , , True)
            End If
        Finally
            cmd.Dispose()
            cnX.Close()
            cnX.Dispose()
        End Try
        Return regresa
    End Function

    Public Shared Function bdExecute(ByVal cadConexion As String, ByVal oCommand As SqlCommand, Optional ByVal regresaId As Boolean = False, Optional retry As Boolean = True) As String
        Dim regresa As String = ""
        Dim cnX As New SqlConnection(cadConexion)
        oCommand.Connection = cnX
        oCommand.CommandTimeout = 0
        Try
            cnX.Open()
            If regresaId Then
                regresa = oCommand.ExecuteScalar
            Else
                oCommand.ExecuteNonQuery()
            End If
        Catch ex As SqlException
            'regresa = ex.Message
            'Throw ex
            If retry Then
                ProcesaError(ex.Number, ex.Message, cadConexion, NombreTabla(oCommand.CommandText))
                Dim regresaExtra As String = bdExecute(cadConexion, oCommand, regresaId, False)
                regresa = regresaExtra
            Else
                '   cError.ReportaError(ex.Message, oLogin.pEmpId, oLogin.pUsrNombre, oCommand.CommandText, oCommand, , True)
                regresa = ex.Message
            End If
        Finally
            cnX.Close()
            cnX.Dispose()
        End Try
        Return regresa
    End Function

    Public Shared Function bdExecuteScriptGo(ByVal cadConexion As String, scriptFilePath As String) As String
        Dim regresa As String = String.Empty
        Dim fileStream As New FileStream(scriptFilePath, FileMode.Open, FileAccess.Read)
        Dim streamReader As New StreamReader(fileStream)
        streamReader.BaseStream.Seek(0, SeekOrigin.Begin)
        Dim allSqlString As String = streamReader.ReadToEnd()
        streamReader.Close()
        fileStream.Close()
        streamReader.Dispose()
        fileStream.Dispose()
        ' Create segments of the string called "lines" each separated by "GO" 
        Dim regex As Regex = New Regex("GO\r\n", RegexOptions.IgnoreCase)
        Dim lines As String() = regex.Split(allSqlString)
        Dim connection As SqlConnection = New SqlConnection(cadConexion)
        Dim cmd As SqlCommand = New SqlCommand()
        cmd.Connection = connection
        connection.Open()
        ' Execute as a transaction, roll back if it fails
        Dim transaction As SqlTransaction = connection.BeginTransaction()
        cmd.Transaction = transaction
        For i As Integer = 0 To lines.Length - 1
            If lines(i).Length > 0 Then
                Try
                    cmd.CommandText = lines(i)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    regresa = ex.Message & lines(i)
                    allSqlString = String.Empty
                    transaction.Rollback()
                    connection.Close()
                    connection.Dispose()
                    Return regresa
                End Try
            End If
        Next
        allSqlString = String.Empty
        transaction.Commit()
        connection.Close()
        connection.Dispose()
        Return regresa
    End Function

    Public Shared Function bdFechaSvr(ByVal cadConexion As String) As Date
        Dim regresa As Date
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT Getdate()")
        If Not IsNothing(dr) Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr.GetDateTime(0).Date
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Shared Function bdHoraSvr(ByVal cadConexion As String) As Date
        Dim regresa As Date
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT {fn current_time()}")
        If Not IsNothing(dr) Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr(0)
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Shared Function bdFechaHoraSvr(ByVal cadConexion As String) As DateTime
        Dim regresa As DateTime
        Dim dr As SqlDataReader = bdDataReader(cadConexion, "SELECT getdate()")
        If Not dr Is Nothing Then
            If dr.HasRows Then
                dr.Read()
                regresa = dr(0)
            End If
            dr.Close()
        End If
        Return regresa
    End Function

    Public Shared Function NombreTabla(sQuery As String) As String
        Dim regresa As String = String.Empty
        sQuery = sQuery.ToUpper
        Select Case True
            Case sQuery.Contains("INSERT")
                regresa = sQuery.Substring(sQuery.IndexOf("INTO") + 5)
                regresa = regresa.Trim
            Case sQuery.Contains("UPDATE")
                regresa = sQuery.Substring(7)
                regresa = regresa.Trim
            Case sQuery.Contains("DELETE")
                regresa = sQuery.Substring(sQuery.IndexOf("FROM") + 5)
                regresa = regresa.Trim
            Case sQuery.Contains("SELECT")
                regresa = sQuery.Substring(sQuery.IndexOf("FROM") + 5)
                regresa = regresa.Trim
        End Select
        If regresa.Contains(" ") Then regresa = regresa.Substring(0, regresa.IndexOf(" "))
        If regresa.Contains("[") Then regresa = regresa.Replace("[", "")
        If regresa.Contains("(") Then regresa = regresa.Replace("(", "")
        If regresa.Contains("]") Then regresa = regresa.Replace("]", "")
        If regresa.Contains(")") Then regresa = regresa.Replace(")", "")
        If regresa.Contains("DBO.") Or regresa.Contains("dbo.") Then regresa = regresa.Substring(4)
        Return regresa.Trim
    End Function

    Public Shared Sub ProcesaError(noError As Int32, Mensaje As String, conexion As String, tabla As String)
        Select Case noError
            Case 207   ' Invalid column name
                InicializaAlterTable(tabla.ToUpper)
                For Each script As String In scripts
                    Dim regresa As String = bdExecute(conexion, script)
                Next
            Case 208   ' Invalid object name
                InicializaCreate(tabla.ToUpper)
                For Each script As String In scripts
                    Dim regresa As String = bdExecute(conexion, script)
                Next
            Case 8152    ' String or binary data would be truncated.
                InicializaAlterColumn(tabla.ToUpper)
                For Each script As String In scripts
                    Dim regresa As String = bdExecute(conexion, script)
                Next
        End Select
    End Sub
End Class
